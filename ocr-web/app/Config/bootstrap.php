<?php
/**
 * This file is loaded automatically by the app/webroot/index.php file after core.php
 *
 * This file should load/create any application wide configuration settings, such as
 * Caching, Logging, loading additional configuration files.
 *
 * You should also use this file to include any files that provide global functions/constants
 * that your application uses.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.10.8.2117
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

// Setup a 'default' cache configuration for use in the application.
Cache::config('default', array('engine' => 'File'));

/**
 * The settings below can be used to set additional paths to models, views and controllers.
 *
 * App::build(array(
 *     'Model'                     => array('/path/to/models/', '/next/path/to/models/'),
 *     'Model/Behavior'            => array('/path/to/behaviors/', '/next/path/to/behaviors/'),
 *     'Model/Datasource'          => array('/path/to/datasources/', '/next/path/to/datasources/'),
 *     'Model/Datasource/Database' => array('/path/to/databases/', '/next/path/to/database/'),
 *     'Model/Datasource/Session'  => array('/path/to/sessions/', '/next/path/to/sessions/'),
 *     'Controller'                => array('/path/to/controllers/', '/next/path/to/controllers/'),
 *     'Controller/Component'      => array('/path/to/components/', '/next/path/to/components/'),
 *     'Controller/Component/Auth' => array('/path/to/auths/', '/next/path/to/auths/'),
 *     'Controller/Component/Acl'  => array('/path/to/acls/', '/next/path/to/acls/'),
 *     'View'                      => array('/path/to/views/', '/next/path/to/views/'),
 *     'View/Helper'               => array('/path/to/helpers/', '/next/path/to/helpers/'),
 *     'Console'                   => array('/path/to/consoles/', '/next/path/to/consoles/'),
 *     'Console/Command'           => array('/path/to/commands/', '/next/path/to/commands/'),
 *     'Console/Command/Task'      => array('/path/to/tasks/', '/next/path/to/tasks/'),
 *     'Lib'                       => array('/path/to/libs/', '/next/path/to/libs/'),
 *     'Locale'                    => array('/path/to/locales/', '/next/path/to/locales/'),
 *     'Vendor'                    => array('/path/to/vendors/', '/next/path/to/vendors/'),
 *     'Plugin'                    => array('/path/to/plugins/', '/next/path/to/plugins/'),
 * ));
 *
 */

/**
 * Custom Inflector rules can be set to correctly pluralize or singularize table, model, controller names or whatever other
 * string is passed to the inflection functions
 *
 * Inflector::rules('singular', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 * Inflector::rules('plural', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 *
 */

/**
 * Plugins need to be loaded manually, you can either load them one by one or all of them in a single call
 * Uncomment one of the lines below, as you need. Make sure you read the documentation on CakePlugin to use more
 * advanced ways of loading plugins
 *
 * CakePlugin::loadAll(); // Loads all plugins at once
 * CakePlugin::load('DebugKit'); //Loads a single plugin named DebugKit
 *
 */

/**
 * To prefer app translation over plugin translation, you can set
 *
 * Configure::write('I18n.preferApp', true);
 */

/**
 * You can attach event listeners to the request lifecycle as Dispatcher Filter. By default CakePHP bundles two filters:
 *
 * - AssetDispatcher filter will serve your asset files (css, images, js, etc) from your themes and plugins
 * - CacheDispatcher filter will read the Cache.check configure variable and try to serve cached content generated from controllers
 *
 * Feel free to remove or add filters as you see fit for your application. A few examples:
 *
 * Configure::write('Dispatcher.filters', array(
 *		'MyCacheFilter', //  will use MyCacheFilter class from the Routing/Filter package in your app.
 *		'MyCacheFilter' => array('prefix' => 'my_cache_'), //  will use MyCacheFilter class from the Routing/Filter package in your app with settings array.
 *		'MyPlugin.MyFilter', // will use MyFilter class from the Routing/Filter package in MyPlugin plugin.
 *		array('callable' => $aFunction, 'on' => 'before', 'priority' => 9), // A valid PHP callback type to be called on beforeDispatch
 *		array('callable' => $anotherMethod, 'on' => 'after'), // A valid PHP callback type to be called on afterDispatch
 *
 * ));
 */
Configure::write('Dispatcher.filters', array(
	'AssetDispatcher',
	'CacheDispatcher'
));

/**
 * Configures default file logging options
 */
App::uses('CakeLog', 'Log');
CakeLog::config('debug', array(
	'engine' => 'File',
	'types' => array('notice', 'info', 'debug'),
	'file' => 'debug',
));
CakeLog::config('error', array(
	'engine' => 'File',
	'types' => array('warning', 'error', 'critical', 'alert', 'emergency'),
	'file' => 'error',
));

// Load Plugin
CakePlugin::loadAll(); // Loads all plugins
// API Access Keys
Configure::write('API_ACCESS_KEY', array(
        'and7ce162b32be4f65de44619574c7a29d7',
        'ios884b6a4de8ba1cbf7ec3edee2e2f71b7',
        'medicbleep884b6a4de8ba1cbf7ec3edee2e2f71b7',
        'mbweb70bdb66f5b3f29b306b5f34f595072cd',
    'ocrweb70bdb66f5b3f29b306b5f34f595072cd'
));

// Twilio keys
Configure::write("twilio_details", array(
        // 'account_sid'=>"AC34d2cc35e37d1f2b6e6bee19307b55a6",
        'account_sid'=>"AC34d2cc35e37d1f2b6e6bee19307b55a6",
        'auth_token'=>"bec2ed36cb6605910edaee0775b1a1b9",
        // 'from' => "+441591372012"
        'from' => "Medic Bleep"
));

// Define image path
define('USER_PROFILE_PATH', 'profile');
define('USER_ARTICLE_PATH', 'article');

// Paging Constants
define('PAGE_SIZE', 100);
define('DEFAULT_PAGE_SIZE', 15);
define('ADMIN_PAGINATION', 10);

define('ANALYTICS_ENVIRONMENT', 'development');

// Server settings AWS Keys
define('BUCKET', 'ocr1freez');
define('THUMBNAILBUCKET', 'mbbetaresized');
define('SECRETKEY', 'rwCRd7bSqXDWdf3TcrwEAXzWMbyoFVM6UQL6sBLl');
define('ACCESSKEY', 'AKIAIN3KMCJMDSHC76KA');
// define('AMAZON_PATH', 'https://ocr1freez.s3-us-west-2.amazonaws.com/');
// define('PROFILE_PICTURE_THUMBNAIL_IMAGE_PATH', 'https://'.THUMBNAILBUCKET.'.s3-eu-west-2.amazonaws.com/');
define('AMAZON_PATH', 'https://ocr1freez.s3-us-west-2.amazonaws.com/');
define('PROFILE_PICTURE_THUMBNAIL_IMAGE_PATH', 'https://ocr1freezresized.s3-us-west-2.amazonaws.com/');


//define('SECRETKEY', 'NtU4WEWZY12rZMX9TCF5aJRVBjqG4Z/9xo4a4kPP');
//define('ACCESSKEY', 'AKIAIQ7QXPSAVRI6EB3A');
//define('AMAZON_PATH', 'https://ocr1dev.s3-us-west-2.amazonaws.com/');

// Base Url
define('BASE_URL', Router::url('/nursing_home_app/ocr-web/', true)); // Staging setting
// define('BASE_URL', Router::url('/mb_dev_merger_code/ocr-web/', true)); // Staging setting
//define('BASE_URL', Router::url('/', true)); // Local setting

// Email A/C
define('NO_REPLY_EMAIL', 'ehteram333@gmail.com');
// define('NO_REPLY_EMAIL', 'noreply@theoncallroom.com');
define('ADMIN_EMAIL', 'admin@theoncallroom.com');

// Image path
define('SPECILITY_ICON_PATH', BASE_URL.'img/specilities/');
// Guest User image path
define('GUEST_USER_PROFILE_IMG', BASE_URL);


//mandrill account detail
define('MEDICBLEEP_BASE_URL','http://13.57.122.250/medicsite_staging/web-medicbleep/');
define('MANDRILL_API_KEY','XQn2s73atKErgy0GptNdlg');

define('MANDRILL_SUBACCOUNT','MB_QA');
define('ENVIRONMENT', 'development');
define('PUSH_END_POINT_ANDROID','https://fcm.googleapis.com/fcm/send');
define('PUSH_END_POINT_IOS','ssl://gateway.sandbox.push.apple.com:2195');
define('PUSH_END_POINT_IOS_V2','https://api.sandbox.push.apple.com:443');
define('APP_BUNDLE_ID_V2','com.medicbleep.v2app');
define('VOIP_CERTIFICATE_IOS','CertificatesVOIP.pem');
define('PUSH_CERTIFICATE_IOS_V2','apns_certificate.pem');
define('PUSH_CERTIFICATE_KEY_V2','apns_key.pem');
define('GCM_ACCESS_KEY','AAAAo67ODOU:APA91bHzFgRrcrFds6gXIpkr-Ocqr4Cg8D6GtLRJjuRK-1pQoYFuhhEgsRnGBW_LYPQz7W0SCde9gYe43hYmUj0BwddGTLA4gjPAfhe2wgxhqNp872oXBrIHtvftPtMY24nlOOR1jhOf');
define('GCM_ACCESS_KEY_V1','AAAAKXxFQ1Y:APA91bE4s_5f4s8L2MlyeaJO28VxHOeKCVDJghM_EGDstTN_1bagCB9Le-vhyO39T3imbFJIKuqW0W4sfR0TU-yevcRItdPLbJT7wZ_dr1jrZCIyQr1gaG373qC-OowpLbe8kg8TpDaN');

// Load Error Code file
include_once('error_code.php');

// Load Graphql Code file
include_once('graphql_queries.php');
include_once('medicbleep_v2.php');

// Load Medic bleep file
include_once('quickblox_credentials.php');
