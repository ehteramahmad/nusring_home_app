<?php
	define('ERROR_200', "OK");
	define('SUCCESS_200', "OK");
	define('ERROR_601', "Information not provided by app");
	// define('ERROR_602', "You are now signed out as you signed into another device");
	define('ERROR_602', "Either your session has been expired or you have logged-in on another device");
	define('ERROR_603', "Login Credentials Blank");
	define('ERROR_604', "Invalid login credentials! Please check entered Email ID/ Password and try again.");
	define('ERROR_605', "Your Email ID is not verified yet. Please check your email and click on the link to verify your account.");
	define('ERROR_606',	"Old password does not match");
	define('ERROR_607', "Email not instantiated");
	define('ERROR_608', "Some technical error occurred. Please try again");
	define('ERROR_609',	"Looks like you are already registered.  Tap on 'Yes' to login using existing credentials or tap on 'No' to register with different Email ID.");
	define('ERROR_610',	"Sorry, this Email ID is not registered with us. Please provide your registered Email ID.");
	define('ERROR_611',	"Data not posted");
	define('ERROR_612',	"Invalid credential supplied");
	define('ERROR_613',	"No result found.");
	define('ERROR_614',	"Your Email ID is not verified yet. Please check your email and click on the link to verify your account.");
	define('ERROR_615',	"Some technical error occurred. Please try again");
	define('ERROR_616', "Device token or invalid Access Key supplied");
	define('ERROR_617', "Invalid Token OR already logout.");
	define('ERROR_618', "User Id not exists.");
	define('ERROR_619', "User Post(Article) Id not exists.");
	define('ERROR_620', "You Can't Share Self Post");
	define('ERROR_621', "Consent Id Not Found");
	define('ERROR_622', "Consent Not Belongs to you");
	define('ERROR_623', "Followed By User Not Found");
	define('ERROR_624', "Followed To User Not Found");
	define('ERROR_625', "No Contact found");
	define('ERROR_626', "User is already in your Contact list");
	define('ERROR_627', "Already Marked Post Spam");
	define('ERROR_628', "Device Registration already added");
	define('ERROR_629', "User is not in your Contact list");
	define('ERROR_630', "You've already received Contact Request from this user. Please accept or reject contact request.");
	define('ERROR_631', "Spam id not selected.");
	define('ERROR_632', "Beat Not Belongs to you.");
	define('ERROR_633', "Your account is waiting for admin review. You will recieve an email when your account is approved. Thanks for your patience!");
	define('ERROR_634', "Please select your profession");
	define('ERROR_635', "This beat is hidden.");
	define('ERROR_636', "Restricted Access! Looks like you are traveling outside the country! To comply with regulations related to Protected Healthcare Information(PHI), you can’t access Chat till you are back in your country.");
	define('ERROR_637', "Feedback already added.");
	define('ERROR_638', "Please enter QB id.");
	define('ERROR_639', "Country is required");
	define('ERROR_640', "Please update your app to latest version and start using many exciting features including the one you are trying now. Please visit App store to download latest Medic Bleep.");
	define('ERROR_641', "Dear TheOCR user, you can now make colleagues using our new HIPAA compliant messaging app - Medic Bleep. Please download it from App Store and enjoy messaging with your Colleagues.");
	define('ERROR_642', "Both of you should be from same country.");
	define('ERROR_643', "Not permitted to add livestream post");
	define('ERROR_644', "Not permitted to send message on public group");
	define('ERROR_645', "Some technical error occurred. Please try again");
	define('ERROR_646', "Something went wrong. Please try again");
	define('ERROR_647', "Inactive user");
	define('ERROR_648', "Phone verification failed. Please try again");
	define('ERROR_649', "Please use your NHS or Institutional Email Address");
	define('ERROR_650', "Invalid login credentials! Please check entered Username/ Password and try again.");
	define('ERROR_651', "Your account is no more active. Please contact support@medicbleep.com for any further assistance.");
	define('ERROR_652', "User is inactive.");
	define('ERROR_653', "User is deleted.");
	define('ERROR_654', "This user is no more active.");
	define('ERROR_655', "When your institution has confirmed you are a member of their organisation, you will have access to their directory.");
	define('ERROR_656', "This user doesn't belong to your institution anymore.");
	define('ERROR_657', "Invalid request parameter/value.");
	define('ERROR_658', "User is removed by trust admin.");
	define('ERROR_659', "Device id is empty.");
	define('ERROR_660', "Your Medic Bleep account has been blocked due to suspicious login activity. Please contact support@medicbleep.com or try again after 30 minutes.");
	define('ERROR_661', "Your password must include at least 8 characters. Use at least One UPPERCASE letter, One lowercase letter, One number and at least One special character.");
	/*------- Success Messages ------*/
	define('SUCCESS_601', "Profile updated successfully.");
	define('SUCCESS_602', "Looks like you are already registered.  Tap on 'Yes' to login using existing credentials or tap on 'No' to register with different Email ID.");
	define('SUCCESS_603', "Sorry.. we are unable to export the chat as you are currently registered with an unsecured Email ID.");
	// define('SUCCESS_604', "Please check your email and click on the link to verify your account.");
	define('SUCCESS_604', "Please sign in to complete your verification.");
	define('SUCCESS_605', "Thanks for sharing your feedback which is extremely valuable to us");
	define('SUCCESS_606', "A reset password link has been sent, please check your email now.");
	define('SUCCESS_607', "Privacy settings updated successfully.");
	define('SUCCESS_608', "Congratulations! Your password has been changed successfully.");
	define('SUCCESS_609', "Chat history sent to your registered email address.");
	define('SUCCESS_610', "Phone verification successful ");
	define('SUCCESS_611', "Either your session has been expired or you have logged-in on another device.");
	define('SUCCESS_612', "Verify your account.");
	define('SUCCESS_613', "Success!");
	define('SUCCESS_615', "Please check your email and click on the link to verify your account.");
	define('MESSAGE_001', " We have changed our password policy to make Medic Bleep more secure. Please update your password.");
	define('MESSAGE_002', "Your password must include minimum 8 characters. Use at least One UPPERCASE letter, One lowercase letter, One number and at least One special character.");

	//Qr code error number
	define('ERROR_662', "Something went wrong.");
	// define('ERROR_663', "Sorry... this code is already been used. Please contact IT admin of your institute OR Contact Medic Bleep Support.");
	// define('ERROR_664', "Invalid QR code. Please contact IT admin of your institute OR Contact Medic Bleep Support.");

	define('ERROR_663', "This QR code seems to already have been used. Please contact support.");
	define('ERROR_664', "This QR code is invalid. Please contact support.");

	define('ERROR_665', "Your subscription has expired. Thank you for using Medic Bleep!.");
	define('ERROR_666', "Your account is waiting for admin review.You will receive an email when your account is approved. For immediate access to Medic Bleep please contact us.");
	define('ERROR_667', "Image type not supported.");


	//** Basic days/month settings
	define('ENTERPRISES_USER_TRIAL_PERIOD','30');
	define('ENTERPRISES_USER_GRACE_PERIOD','15');

	//***Role Exchange Code
	define('ROLEEX_001', "You have a Baton Role transfer request from ");
	define('ROLEEX_002', " has accepted your Baton Role request.");
	define('ROLEEX_003', " has rejected your Baton Role request.");
	define('ROLEEX_004', " has accepted your Baton Role transfer request.");
	define('ROLEEX_005', " has rejected your Baton Role transfer request.");
	define('ROLEEX_006', " has revoked a Baton Role request from you.");
	define('ROLEEX_007', "Your Baton Role request has expired.");
	define('ROLEEX_008', "Switchboard has accepted your Baton Role request.");
	define('ROLEEX_009', "Switchboard has rejected your Baton Role request.");
	define('ROLEEX_010', "You can not revoke this request as this is already accepted or rejected. Please refresh your Baton Role list.");
	define('ROLEEX_011', "Switchboard has assigned you a Baton Role.");
	define('ROLEEX_012', "Switchboard has removed a Baton Role.");
	define('ROLEEX_013', "Switchboard has accepted your Baton Role transfer request.");
	define('ROLEEX_014', "Switchboard has rejected your Baton Role transfer request.");
	define('ROLEEX_015', "You are not authorized to perform this action now.");
	define('ROLEEX_016', "Switchboard has deleted a role assigned to you.");
	define('ROLEEX_017', "Switchboard has unassigned a role assigned to you.");
	define('ROLEEX_018', "Role successfully transfered to switchboard");
	define('ROLEEX_019', "Switchboard has requested to transfer you a Baton Role.");

	//**Permanent Role Code
	define('PROLEEX_001', "You have a Permanent Role transfer request from ");
	define('PROLEEX_002', " has accepted your Permanent Role transfer request.");
	define('PROLEEX_003', " has rejected your Permanent Role transfer request.");
	define('PROLEEX_004', " has revoked a Permanent Role request from you.");
	define('PROLEEX_007', "Your Permanent Role request has expired.");
