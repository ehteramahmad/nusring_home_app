<?php
    define('GET_DIALOGS', '
        query dialogs($input:DialogFilterInput!){
            dialogs(filter:$input) {
                createdAt
                customParams
                id
                institutionId
                lastMessage
                lastMessageFromUserId
                lastMessageCreatedAt
                name
                occupantsIds
                ownerUserId
                patientInfo
                photoUrl
                type
                opponentUserId
                lastMessageBodyType
            }
        }
    ');
    define('GET_DIALOG', '
        query dialog($id: String!) {
            dialog(id: $id, userId: "0") {
                createdAt
                customParams
                id
                institutionId
                lastMessage
                lastMessageFromUserId
                lastMessageCreatedAt
                name
                occupantsIds
                ownerUserId
                patientInfo
                photoUrl
                type
                opponentUserId
                lastMessageBodyType
            }
        }
    ');
    define('GET_MESSAGES', '
        query messages($input:MessageFilterInput!){
            messages(filter:$input) {
                acks{
                    fromUserId
                    readAt
                    receivedAt
                  }
                  attachment{
                    filename
                    size
                    url
                  }
                  body
                  bodyType
                  createdAt
                  customParams
                  dialogId
                  forwardReplyParams{
                    attachment{
                      filename
                      id
                      size
                      url
                    }
                    avatarUrl
                    body
                    createdAt
                    bodyType
                    fromUserId
                    name
                    type
                    composedAt
                    customParams
                  }
                  fromUserId
                  id
                  institutionId
                  occupantsIds
                  readAt
                  receivedAt
                  readIds
                  receivedIds
                  sentAt
                  state
                  toUserId
                  type
                  composedAt
                  senderName
            }
        }
    ');

    define('GET_GROUP_EVENT_ANALYTICS', '
        query groupEventsAnalytics($inputVariable){
            applicationType
            fromUserId
            id
            institutionId
            occupantsIds
            patientInfo
            type
            name
        }
    ');
?>