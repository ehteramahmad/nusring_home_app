<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	public $uses = array('UserDevice', 'User', 'Mbapi.VoipPushNotification', 'Mbapi.UserDndStatus','UserOneTimeToken','UserDutyLog','Mbapi.UserBatonRole','Mbapi.BatonRole','Mbapi.UserProfile','Mbapiv2.UserPermanentRole','Mbapiv1.UserLoginTransaction','Mbapiv1.UserDeviceInfo','Mbapiv2.UserBatonRoleTransaction','Mbapiv3.AvailableAndOncallTransaction','Mbapiv3.DndTransaction','Mbapiv4.UserDutyLog','Mbapiv4.UserDndStatus','Mbapiv4.UserBatonRole','Mbapiv4.DepartmentsBatonRole','Mbapiv4.CacheLastModifiedUser');
	public $components = array('Common','Session', 'Quickblox','Medicbleep');

	public function beforeFilter() {
		header("Access-Control-Allow-Origin: *");
    	header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization, If-Modified-Since, Cache-Control, Pragma, access_key, token");
	}

	/*
	On: 05-08-2015
	I/P: NA
	O/P: true/false, if access key valid/invalid
	Desc: Checks valid access key provided by API requester.
	*/
	public function accesskeyCheck() {
		$header = getallheaders();
		$accessKey = $this->Common->decryptData($header['access_key']);
		if(in_array($accessKey, Configure::read('API_ACCESS_KEY'))) {
			return true;
		}else{
			return false;
		}

	}

/*
________________________________________________
Method Name:=>getAppType
Purpose:Used to get the application name which is accesing the services ie:MB,TheOCR,Web
Detail:
created by :Developer Shashi
Created date:07-06-16
Params:NULL
_________________________________________________
*/
	public function getAppType() {
		$header = getallheaders();
		$applicationType = $header['application_type'];
		if($applicationType!='') {
			return $applicationType;
		}else{
			return '';
		}

	}

	/*
	On: 10-08-2015
	I/P: token (passed by headers)
	O/P: true/false
	Desc: It checks user logged in token and if validate it will return true else false
	*/
	public function tokenValidate() {
		$header = getallheaders();
		$token = $this->Common->decryptData($header['token']);
		$params['status'] = 1;
		$params['token'] = $token;

		if( !empty($params) ) {
			$cnt = $this->UserDevice->find('count', array('conditions'=> $params));
			if( $cnt > 0 ) {
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}

	}

	/*
	On:
	I/P: NA
	O/P: returns array()
	Desc: returns all header values.
	*/
	public function getHeaderValues() {
		return getallheaders();
	}
/*
________________________________________________
Method Name:checkAdminSession
Purpose:Check the admin session
Detail: Get the Admin id and return true/false
created by :Developer
Created date:31-08-15
Params:None

_________________________________________________
*/
    public function checkBeatUserSession() {
		$value = $this->Session->read('beatPostUserName.email');
		if($value!='') {
			return $value;

		}else{
			return false;
		}
    }
/*
________________________________________________
Method Name:adminAuthorisation
Purpose:Provide admin access to authoried page
Detail:
created by :Developer
Created date:31-08-15
Params:None
_________________________________________________
*/
  public function beatUserAuthorisation() {
		if($this->checkBeatUserSession()) {
			return true;
		}else{
			$this->Session->setFlash(Configure::read('ERROR_617'));
			$this->redirect(array('plugin'=>'institution','controller'=>'institutionindex','action'=>'index'));

		}
    }
/*
________________________________________________
Method Name:=>getDeviceType
Purpose:Used to get the Device Type(Ex:Web,App etc)
Detail:
created by :
Created date:04-05-17
Params:NULL
_________________________________________________
*/
	public function getDeviceType() {
		$header = getallheaders();
		$deviceType = $header['device_type'];
		if($deviceType!='') {
			return $deviceType;
		}else{
			return '';
		}

	}

	/*
	________________________________________________
	Method Name:=>getUserProfileStatus
	Purpose:Used to get the User Status(inactive, deleted etc.)
	Detail:
	created by :
	Created date:05-07-17
	Params:NULL
	_________________________________________________
	*/

	public function getUserProfileStatus($userId, $loggedInUserId) {
		$responceMassage = array();
		//$getstatus = $this->User->find("first", array("conditions"=> array("id"=> $userId)));
		$getstatus = $this->User->find('first', array('conditions'=> array("User.id"=> $userId)));
		if(($userId == $loggedInUserId) && ($getstatus['User']['status'] == 0 || $getstatus['User']['status'] == 2))
		{
			$responceMassage['message'] =  ERROR_651;
			$responceMassage['response_code'] = 651;
		}
		else
		{
			if($getstatus['User']['status'] == 0)
			{
				$responceMassage['message'] =  ERROR_652;
				$responceMassage['response_code'] = 652;

			}
			else if($getstatus['User']['status'] == 2)
			{
				$responceMassage['message'] =  ERROR_653;
				$responceMassage['response_code'] = 653;
			}
			else
			{
				$responceMassage['message'] =  ERROR_654;
				$responceMassage['response_code'] = 654;
			}
		}
		return $responceMassage;
	}

	/*
	________________________________________________
	Method Name:=>getUserStatus
	Purpose:Used to get the User Status(inactive, deleted etc.)
	Detail:
	created by :
	Created date:05-07-17
	Params:NULL
	_________________________________________________
	*/

	public function getUserStatus($userId, $loggedInUserId) {
		$responceMassage = array();
		$getstatus = $this->User->find('first', array('conditions'=> array("User.id"=> $userId)));
		if(($userId == $loggedInUserId) && ($getstatus['User']['status'] == 0 || $getstatus['User']['status'] == 2))
		{
			$responceMassage['message'] =  ERROR_651;
			$responceMassage['response_code'] = 651;
		}
		else
		{
			if($getstatus['User']['status'] == 0)
			{
				$responceMassage['message'] =  ERROR_652;
				$responceMassage['response_code'] = 652;

			}
			else if($getstatus['User']['status'] == 2)
			{
				$responceMassage['message'] =  ERROR_653;
				$responceMassage['response_code'] = 653;
			}
			else
			{
				$responceMassage['message'] =  ERROR_653;
				$responceMassage['response_code'] = 653;
			}
		}
		return $responceMassage;
	}

	public function getUserProfileStatuscheck($userId, $iscurrntUser)
	{
		$responceMassage = array();
		$getstatus = $this->User->find('first', array('conditions'=> array("User.id"=> $userId)));
		if($iscurrntUser == 1)
		{
			$responceMassage['message'] =  ERROR_651;
			$responceMassage['response_code'] = 651;
		}
		else
		{
			if($getstatus['User']['approved'] == 0)
			{
				$responceMassage['message'] =  ERROR_652;
				$responceMassage['response_code'] = 652;
			}
			if($getstatus['User']['status'] == 2)
			{
				$responceMassage['message'] =  ERROR_653;
				$responceMassage['response_code'] = 653;
			}
		}
		return $responceMassage;
	}

	/*
	--------------------------------------------------------------------
	On:
	I/P:
	O/P:
	Desc:
	--------------------------------------------------------------------
	*/
	public function checkEtagNotModified($params = array()){
		$header = getallheaders();
		// $eTag = (isset($header['Etag']) && !empty($header['Etag'])) ? $header['Etag'] : 0;
		$eTag = (isset($header['Etag']) && !empty($header['Etag'])) ? $header['Etag'] : 0;

		if($eTag == 0){
			$eTag = (isset($header['etag']) && !empty($header['etag'])) ? $header['etag'] : 0;
		}
		$etagVals = array();
		$cacObj = $this->Cache->redisConn();
		if(($cacObj['connection']) && empty($cacObj['errors'])){
						if($cacObj['robj']->exists($params['key'])){
							$etagVals = $cacObj['robj']->hgetall($params['key']);
						}
		}

		if($etagVals['eTag'] == (string) $eTag){
			$etagVals['etagHeader'] = $eTag;
			$etagVals['code'] = 304;
			return $etagVals;
		}elseif(empty($eTag)){
			$etagVals['etagHeader'] = 0;
			return $etagVals;
		}else{
				if($cacObj['robj']->exists('institutionDirectory:'.$params['institution_key'])){
					$etagVals['etagHeader'] = $etagVals['eTag'];
					$etagVals['code'] = 200;
					return $etagVals;
				}
				else{
					$etagVals['etagHeader'] = $eTag;
					return $etagVals;
				}
		}
	}

	/*
	--------------------------------------------------------------------
	On:
	I/P:
	O/P:
	Desc:
	--------------------------------------------------------------------
	*/
	public function checkEtagNotModifiedDynamicDirectory($params = array()) {
		$header = getallheaders();
		// $eTag = (isset($header['Etag']) && !empty($header['Etag'])) ? $header['Etag'] : 0;
		$eTag = (isset($header['Etag']) && !empty($header['Etag'])) ? $header['Etag'] : 0;

		if($eTag === 0) {
			$eTag = (isset($header['etag']) && !empty($header['etag'])) ? $header['etag'] : 0;
		}
		$etagVals = array();
		$cacObj = $this->Cache->redisConn();
		if(($cacObj['connection']) && empty($cacObj['errors'])) {
			if($cacObj['robj']->exists($params['key'])) {
				$etagVals = $cacObj['robj']->hgetall($params['key']);
			}
		}
		$previousDateModified = explode("~",$eTag);

		if($etagVals['eTag'] == $previousDateModified[0]) {
			$etagVals['etagHeader'] = $eTag;
			$etagVals['code'] = 304;
			return $etagVals;
		}elseif(empty($eTag)) {
			$etagVals['etagHeader'] = 0;
			return $etagVals;
		}else{
			$etagVals['etagHeader'] = $eTag;
			return $etagVals;
		}
	}

	/*
	--------------------------------------------------------------------
	On:
	I/P:
	O/P:
	Desc:
	--------------------------------------------------------------------
	*/
	public function updateStaticEtagData($params = array()) {
		$cacObjEtag = $this->Cache->redisConn();
		if(($cacObjEtag['connection']) && empty($cacObjEtag['errors'])) {
				$etagLastModified = strtotime(date('Y-m-d H:i:s'));
				$eTagNewVal = md5($params['key'] . rand(1,10000) . $etagLastModified);
				$cacObjEtag['robj']->hset($params['key'],'eTag' , $eTagNewVal);
				$cacObjEtag['robj']->hset($params['key'],'lastmodified' , $etagLastModified);
		}
	}

	/*
	--------------------------------------------------------------------
	On:
	I/P:
	O/P:
	Desc:
	--------------------------------------------------------------------
	*/
	public function updateDynamicEtagData($params = array()) {
		$cacObjEtag = $this->Cache->redisConn();
		if(($cacObjEtag['connection']) && empty($cacObjEtag['errors'])) {
				//$dynamicPreviousModifiedDate = $cacObjEtag['robj']->hget($params['key'],'lastmodified');
				//$cacObjEtag['robj']->hset($params['key'],'previousLastmodified' , $dynamicPreviousModifiedDate);
				$etagLastModified = strtotime($params['lastmodified']);
				$eTagNewVal = md5($params['key'] . rand(1,10000) . $etagLastModified);
				$cacObjEtag['robj']->hset($params['key'],'eTag' , $eTagNewVal);
				$cacObjEtag['robj']->hset($params['key'],'lastmodified' , $etagLastModified);
		}
	}

	/*
	--------------------------------------------------------------------
	On: 09-05-2018
	I/P: Rsa encrypted token
	O/P: Return true or false if token exist or not
	Desc:
	--------------------------------------------------------------------
	*/

	public function tokenValidation() {
		$header = getallheaders();
		if(file_exists(BASE_URL.'/sslKeys/'.'abcc.tmp'))
		{
			$pkGeneratePrivate = file_get_contents(BASE_URL.'/sslKeys/'.'abcc.tmp');
			$pkImport = openssl_pkey_get_private($pkGeneratePrivate);
			$pkImportDetails = openssl_pkey_get_details($pkImport);
			$pkImportPublic = $pkImportDetails['key'];
			openssl_pkey_free($pkImport);
		}
		else
		{
			$pkGeneratePrivate = file_get_contents(BASE_URL.'/sslKeys/'.'abcc.tmp');
			$pkImport = openssl_pkey_get_private($pkGeneratePrivate);
			$pkImportDetails = openssl_pkey_get_details($pkImport);
			$pkImportPublic = $pkImportDetails['key'];
			openssl_pkey_free($pkImport);
		}
		$paramsData = $header['token'];
		openssl_private_decrypt(base64_decode($paramsData), $decrptedData, $pkGeneratePrivate);
		$params['status'] = 1;
		$params['token'] = $decrptedData;
		if( !empty($params) ) {
			$cnt = $this->UserDevice->find('count', array('conditions'=> $params));
			if( $cnt > 0 ) {
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}

	}

	/*
	--------------------------------------------------------------------
	On: 09-05-2018
	I/P: Rsa encrypted access key
	O/P: Return true or false if token exist or not
	Desc:
	--------------------------------------------------------------------
	*/

	public function accesskeyCheckValidation() {
		$header = getallheaders();
		if(file_exists(BASE_URL.'/sslKeys/'.'abcc.tmp'))
		{
			$pkGeneratePrivate = file_get_contents(BASE_URL.'/sslKeys/'.'abcc.tmp');
			$pkImport = openssl_pkey_get_private($pkGeneratePrivate);
			$pkImportDetails = openssl_pkey_get_details($pkImport);
			$pkImportPublic = $pkImportDetails['key'];
			openssl_pkey_free($pkImport);
		}
		else
		{
			$pkGeneratePrivate = file_get_contents(BASE_URL.'/sslKeys/'.'abcc.tmp');
			$pkImport = openssl_pkey_get_private($pkGeneratePrivate);
			$pkImportDetails = openssl_pkey_get_details($pkImport);
			$pkImportPublic = $pkImportDetails['key'];
			openssl_pkey_free($pkImport);
		}
		$paramsData = $header['access_key'];
		openssl_private_decrypt(base64_decode($paramsData), $decrptedData, $pkGeneratePrivate);
		// $accessKey = $this->Common->decryptData($header['access_key']);
		if(in_array($decrptedData, Configure::read('API_ACCESS_KEY'))) {
			return true;
		}else{
			return false;
		}

	}


	/*
	--------------------------------------------------------------------
	On: 12-06-18
	I/P: token in header
	O/P: true/false if token exist or not
	Desc: This function is use to check the user access for the plane api Mbapi
	--------------------------------------------------------------------
	*/

	public function validateToken() {
		$header = getallheaders();
		$token = $header['token'];
		$params['status'] = 1;
		$params['token'] = $token;
		if( !empty($params) ) {
			$cnt = $this->UserDevice->find('count', array('conditions'=> $params));
			if( $cnt > 0 ) {
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}

	}

	/*
	--------------------------------------------------------------------
	On: 12-06-18
	I/P: access_key in header
	O/P: true/false if access_key exist or not
	Desc: This function is use to check the user access for the plane api Mbapi
	--------------------------------------------------------------------
	*/

	public function validateAccessKey() {
		$header = getallheaders();
		$accessKey = $header['access_key'];
		if(in_array($accessKey, Configure::read('API_ACCESS_KEY'))) {
			return true;
		}else{
			return false;
		}

	}

	/*
	--------------------------------------------------------------------
	On: 22-10-18
	I/P: user_id in header
	O/P:
	Desc:
	--------------------------------------------------------------------
	*/

	public function genrateRandomToken($userId=NULL)
	{
		if(!empty($userId))
		{
			$userInfo = $userId.rand(1000,100000);
			$options = [
			    'cost' => 4,
			];
			$token = md5(password_hash($userInfo, PASSWORD_BCRYPT, $options));
			// $token = md5(crypt($userInfo));
		}
		return $token;
	}

	/*
	----------------------------------------------------------------------------------------------
	On: 13-06-2019
	I/P: user id's(from and to)
	O/P: true or false
	Desc: This function will update user permanent role
	----------------------------------------------------------------------------------------------
	*/

	public function getUserPermanentRolesData($params = array())
	{
		if(!empty($params))
		{
			if($params['user_id'] != 0){
				$fromUserPermanentRole = $this->getUserPermanentRoles($params['user_id'],$params['institute_id']);
				$fromUserData = array("UserProfile.custom_permanent_role"=> "'". json_encode($fromUserPermanentRole) ."'");
				$toroleStatusResult = $this->UserProfile->updateAll($fromUserData, array("UserProfile.user_id"=> $params['user_id']));
			}

			if($params['other_user_id'] != 0){
				$toUserPermanentRole = $this->getUserPermanentRoles($params['other_user_id'],$params['institute_id']);
				$toUserData = array("UserProfile.custom_permanent_role"=> "'". json_encode($toUserPermanentRole) ."'");
				$fromroleStatusResult = $this->UserProfile->updateAll($toUserData, array("UserProfile.user_id"=> $params['other_user_id']));
			}
			return true;
		}
	}

	public function getUserPermanentRoles($userId, $instituteId)
	{
		if(!empty($userId))
		{
			$userpermanentRolestatus = array(1,2);
			$permanentRoleDetail = $this->UserPermanentRole->find("all", array("conditions"=> array("user_id"=> $userId, "status"=>$userpermanentRolestatus,"active"=>1, 'institute_id'=>$instituteId)));

			if(!empty($permanentRoleDetail))
			{
				foreach ($permanentRoleDetail as  $value) {
					$permanentRoleDataList[] =  json_decode($value['UserPermanentRole']['role']);
				}
			}
			else
			{
				$permanentRoleDataList = array();
			}

			return $permanentRoleDataList;
		}
	}

		/*
		--------------------------------------------------------------------------
		ON: 12-02-2019
		I/P:
		O/P: True or False
		Desc: This function will send a voip/fcm push notification to dissmiss
			  the Dnd status options.
		--------------------------------------------------------------------------
		*/

		public function dissmissDndOnOcrAndQb($params = array(),$message)
		{
			if(!empty($params))
			{
				$userId = $params['user_id'];
				// DK: Save the DND transaction------->>>>>>>>
				$dnd_conditions = array(
					'UserDndStatus.is_active' => 1,
					'UserDndStatus.user_id' => $userId
				);
				$options = array(
						'conditions'=>$dnd_conditions
					);
				$userDndStatus = $this->UserDndStatus->find('first',$options);
				// DK: Save the DND transaction------->>>>>>>>

				if(!empty($params['email'])) {
					$params['user_email'] = $params['email'];
				}else{
					$getUserDetail = $this->User->find('first', array('conditions'=> array("User.id"=> $userId)));
					$params['user_email'] = $getUserDetail['User']['email'];
				}
				//***********Update Dnd Status On Ocr
				$this->UserDndStatus->updateAll(array("is_active"=> 0, "duration"=> 0), array('user_id'=>$userId));
				//***********Update Dnd Status On Qb
				$oneTimeTokenForDnd = $this->genrateRandomToken($userId);
				$oneTimeTokenDataDnd = array("user_id"=> $userId, "token"=> $oneTimeTokenForDnd);
				$this->UserOneTimeToken->saveAll($oneTimeTokenDataDnd);
				$params['one_time_token'] = $oneTimeTokenForDnd;
				$tokenDetails = $this->Quickblox->quickLogin($params['user_email'], $params['one_time_token']);
				$token = $tokenDetails->session->token;
				$user_id = $tokenDetails->session->user_id;
				$user_details = $this->Quickblox->getuserDetailsbyId($token,$user_id);
				$custom_data_from_api = json_decode($user_details->user->custom_data);
				$customData['userRoleStatus'] = $custom_data_from_api->status;
				$customData['userProfileImgPath'] = $custom_data_from_api->avatar_url;
				$customData['isImport'] = isset($is_import) ? $is_import : "true";
				$customData['at_work'] = $custom_data_from_api->at_work;
				$customData['on_call'] = $custom_data_from_api->on_call;
				$customData['is_dnd_active'] = 0;
				if(!empty($token)) {
					$qbResponce = $this->Quickblox->updateCustomDataOnQbLite($token, $user_id,$customData);
					$res = json_decode(json_encode($qbResponce));
					if(!empty($res['errors'])) {
						$response['qberror'] = $qbResponce;
					}else{
						$response['qbsuccess'] = 1;
						if(isset($custom_data_from_api->dnd_status)){
							$this->DndTransaction->saveAll(array(
								'user_id'=>$userDndStatus['UserDndStatus']['user_id'],
								'institute_id'=>$userDndStatus['UserDndStatus']['institute_id'],
								"dnd_status_id"=>$userDndStatus['UserDndStatus']['dnd_status_id'],
								"start_time"=>$userDndStatus['UserDndStatus']['start_time'],
								"end_time"=>$userDndStatus['UserDndStatus']['end_time'],
								"duration"=>$userDndStatus['UserDndStatus']['duration'],
								"is_active"=>"0",
								"other_user_id"=>$userDndStatus['UserDndStatus']['other_user_id'],
								"note"=>$userDndStatus['UserDndStatus']['note'],
								"custom_data"=>$message ? $message : 'DND Dissmiss',
								'created'=>date("Y-m-d H:i:s")
							));
						}
					}
				}else{
					$response['qberror'] = $tokenDetails;
				}
				return $response;
			}
		}

		/*
		----------------------------------------------------------------------------------------------
		On: 12-03-2019
		I/P: JSON
		O/P: JSON
		Desc: Update at work status for user on qb
		----------------------------------------------------------------------------------------------
		*/

		public function updateOnCallStatusRoleExchange($userId)
		{
			$response = array();
			$customData = array();
			$companyId = $userId['company_id'];
			$is_import = "true";
			$userData = $this->User->find('first', array('conditions'=> array("User.id"=> $userId['user_id'])));
			$userEmail = $userData['User']['email'];

			//** One time token to validate for QB[START]
			$oneTimeTokenForOnCall = $this->genrateRandomToken($userId['user_id']);
			$oneTimeTokenDataOnCall = array("user_id"=> $userId['user_id'], "token"=> $oneTimeTokenForOnCall);
			$this->UserOneTimeToken->saveAll($oneTimeTokenDataOnCall);
			$password = $oneTimeTokenForOnCall;
			//** One time token to validate for QB[END]

			$tokenDetails = $this->Quickblox->quickLogin($userEmail, $password);
			$token = $tokenDetails->session->token;
			$user_id = $tokenDetails->session->user_id;
			$user_details = $this->Quickblox->getuserDetailsbyId($token,$user_id);
			$custom_data_from_api = json_decode($user_details->user->custom_data);

			if($userId['on_call_value'] == 0) {
				$duty = (isset($custom_data_from_api->on_call) && (string)$custom_data_from_api->on_call == '1')?'1':"0";
				$at_work = "1";
			}else{
				$duty = "1";
				$at_work = "1";
			}

			$customData['userRoleStatus'] = $custom_data_from_api->status;
			$customData['userProfileImgPath'] = $custom_data_from_api->avatar_url;
			$customData['isImport'] = isset($is_import) ? $is_import : "true";
			$customData['at_work'] = $at_work;
			$customData['on_call'] = $duty;
			$customData['dnd_status'] = isset($custom_data_from_api->dnd_status)?$custom_data_from_api->dnd_status:[];
			if(!empty($customData['dnd_status']))
			{
				$customData['is_dnd_active'] = 1;
			}
			if(!empty($token)) {
				$qbResponce = $this->Quickblox->updateCustomDataOnQbLite($token, $user_id,$customData);
				$res = json_decode(json_encode($qbResponce));
				if(!empty($res['errors'])) {
					$response['qberror'] = $qbResponce;
				}
				else
				{
					$response['qbsuccess'] = 1;
					// ----------
						$this->UserDutyLog->updateAll(array("status"=> $duty, "atwork_status"=>$at_work), array("user_id"=>$userId['user_id'],"hospital_id"=>$userId['company_id']));
						// DK: Save the On call transaction------->>>>>>>>
						$transactionData = array();
						if(isset($custom_data_from_api->on_call) && (string)$custom_data_from_api->on_call == '0' && $duty == '1'){
							$transactionData[] = array(
								'user_id'=>$userId['user_id'],
								'company_id'=>$companyId,
								'type'=>'OnCall',
								'status'=>$duty,
								'device_type'=>'Institution_pannel',
								'platform'=>'web'
							);
						}

						// DK: Save the Available transaction------->>>>>>>>
						if(isset($custom_data_from_api->at_work) && (string)$custom_data_from_api->at_work == '0' && $at_work == '1'){
							$transactionData[] = array(
								'user_id'=>$userId['user_id'],
								'company_id'=>$companyId,
								'type'=>'Available',
								'status'=>$at_work,
								'device_type'=>'Institution_pannel',
								'platform'=>'web'
							);
						}
						$this->AvailableAndOncallTransaction->saveAll($transactionData);
					// ----------
				}
			}
			else{
				$response['qberror'] = $tokenDetails;
			}
			return $response;
		}

		public function getUserBatonRolesData($params = array())
		{
			if($params['to_user'] != 0)
			{
				$touserData = $this->getUserBatonRoles($params['to_user']);
				$touser = array("UserProfile.custom_baton_roles"=> "'". json_encode($touserData) ."'");
				$toroleStatusResult = $this->UserProfile->updateAll($touser, array("UserProfile.user_id"=> $params['to_user']));
			}
			if($params['from_user'] != 0)
			{
				$fromUserData = $this->getUserBatonRoles($params['from_user']);
				$fromuser = array("UserProfile.custom_baton_roles"=> "'". json_encode($fromUserData) ."'");
				$fromroleStatusResult = $this->UserProfile->updateAll($fromuser, array("UserProfile.user_id"=> $params['from_user']));
			}
			return true;
		}

		public function getUserBatonRoles($userId)
		{
			$batonRoleDataList = array();
			if(!empty($userId))
			{
				$userbatonRolestatus = array(1,2,5,7);
				$batonRoleDetail = $this->UserBatonRole->find("all", array("conditions"=> array("from_user"=> $userId,"status"=>$userbatonRolestatus,"is_active"=>1),"group"=>array('UserBatonRole.role_id'),'order'=>array('UserBatonRole.updated_at')));
				foreach ($batonRoleDetail as  $value) {

					$batonRoleName = $this->BatonRole->find("first", array("conditions"=> array("id"=> $value['UserBatonRole']['role_id'])));
					$batonRoleDataList[] = array(
								'role_id'=> isset($value['UserBatonRole']['role_id']) ? $value['UserBatonRole']['role_id'] : "",
								'is_active'=>	isset($value['UserBatonRole']['is_active']) ? $value['UserBatonRole']['is_active'] : 0,
								'role_name'=>	isset($batonRoleName['BatonRole']['baton_roles']) ? $batonRoleName['BatonRole']['baton_roles'] : "",
								'type'=>isset($value['UserBatonRole']['status']) ? $value['UserBatonRole']['status'] : "0",
								);
				}

				return $batonRoleDataList;
			}
		}

		public function sendCustomVoipPush($params =array()) {
			if(!empty($params)) {
				if($params['user_id']) {
					$userVoipData = $this->VoipPushNotification->find('all', array('conditions' => array('VoipPushNotification.user_id' => $params['user_id'])));
				}else if($params['voip_tokens']) {
					$userVoipData[] = $params['voip_tokens'];
				}
				if(!empty($userVoipData)) {
					foreach ($userVoipData as $value) {
						$params['device_token'] = $value['VoipPushNotification']['device_token'];
						$params['fcm_voip_token'] = $value['VoipPushNotification']['fcm_voip_token'];
						$sendNotification = $this->sendPushOnIos($params);
						$sendNotification = $this->sendPushOnAndroid($params);
					}
				}else{
					return "Data is empty";
				}
			}else{
				return "Data is empty";
			}
		}

		public function sendCustomVoipPushv2($params =array()) {
			if(!empty($params)) {
				if($params['user_id']) {
					$userVoipData = $this->VoipPushNotification->find('all', array('conditions' => array('VoipPushNotification.user_id' => $params['user_id'])));
				}else if($params['voip_tokens']) {
					$userVoipData[] = $params['voip_tokens'];
				}
				if(!empty($userVoipData)) {
					foreach ($userVoipData as $value) {
						$params['device_token'] = $value['VoipPushNotification']['device_token'];
						$sendNotification = $this->sendPushOnIosv2($params);
						$sendNotification = $this->sendPushOnAndroidv2($params);
					}
				}else{
					return "Data is empty";
				}
			}else{
				return "Data is empty";
			}
		}

		/*
		----------------------------------------------------------------------------------------------
		On: 28-05-2019
		I/P: $to, $message, $serviceId
		O/P: Json Data
		Desc: Request Api to send sms from twillo
		----------------------------------------------------------------------------------------------
		*/

		public function sendSms($to, $message, $serviceId) {

				$fields = array(
						'Body' => urlencode($message),
						'MessagingServiceSid' => urlencode($serviceId),
						'To' => urlencode($to),
				);
				$fields_string ='';

				foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
				rtrim($fields_string, '&');

				$ch = curl_init();

				curl_setopt($ch, CURLOPT_URL, "https://api.twilio.com/2010-04-01/Accounts/".Configure::read("twilio_details.account_sid")."/Messages.json");
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
				curl_setopt($ch, CURLOPT_USERPWD, Configure::read("twilio_details.account_sid") . ":" . Configure::read("twilio_details.auth_token"));

				$result = curl_exec($ch);
				if($result){
						return json_encode($result);
				}
				if (curl_errno($ch)) {
						return 'Error:' . curl_error($ch);
				}
				curl_close ($ch);

		}

		/*
		----------------------------------------------------------------------------------------------
		On: 28-05-2019
		I/P: $sid(Request Id)
		O/P: Success, Failure
		Desc: Get Response from twillo
		----------------------------------------------------------------------------------------------
		*/

		public function getSmsResponse($sid) {

				$ch = curl_init();

				curl_setopt($ch, CURLOPT_URL, "https://api.twilio.com/2010-04-01/Accounts/".Configure::read("twilio_details.account_sid")."/Messages/".$sid.".json");
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_POST, 0);
				curl_setopt($ch, CURLOPT_USERPWD, Configure::read("twilio_details.account_sid") . ":" . Configure::read("twilio_details.auth_token"));

				$result = curl_exec($ch);
				if($result){
						return json_encode($result);
				}
				if (curl_errno($ch)) {
						return 'Error:' . curl_error($ch);
				}
				curl_close ($ch);

		}

		///*************************Multiple Device login for medicbleep version v1[START]****************///

		/*
		--------------------------------------------------------------------------
		ON: 22-02-2019
		I/P:
		O/P: True or False
		Desc: This function will send a voip/fcm push notification to dissmiss
			  the Dnd status options.
		--------------------------------------------------------------------------
		*/

		public function sendLogoutPushNotification($params =array())
		{
			if(!empty($params)) {
				$userVoipData = $this->VoipPushNotification->find('all', array('conditions' => array('VoipPushNotification.user_id' => $params['user_id'])));
				if(!empty($userVoipData))
				{
					$counter = 0;
					foreach ($userVoipData as $key => $value) {
						$params['device_token'] = $value['VoipPushNotification']['device_token'];
						$params['device_token'] = $value['VoipPushNotification']['device_token'];
						$params['is_dnd_active'] = $params['is_dnd_active'];
						$params['user_id'] = $params['user_id'];
						if($params['device_id'] != $value['VoipPushNotification']['device_id'])
						{

							$sendNotification = $this->sendLogoutPushNotificationOnIosOne($params);
							if($sendNotification == 1)
							{
								$deleteRecords = $this->VoipPushNotification->deleteAll(array("user_id"=>$params['user_id'], "device_id !="=>$params['device_id']));
							}
							$sendNotificationAndroid = $this->sendLogoutPushNotificationAndroidOne($params);
							if($sendNotificationAndroid == 1)
							{
								$deleteRecords = $this->VoipPushNotification->deleteAll(array("user_id"=>$params['user_id'], "device_id !="=>$params['device_id']));
							}
							$counter++;
						}
					}
					return $counter;
				}

			}else{
				return "Data is empty";
			}
		}

		public function sendLogoutPushNotificationOnIosOne($params = array())
		{
			if(!empty($params))
			{

				$deviceToken = $params['device_token'];
				$fcmVoipToken = $params['device_token'];
				$userId = isset($params['user_id']) ? $params['user_id'] : "";
				$passphrase = 'password';
				$message = 'Dissmiss Dnd';
				$pemFile = WWW_ROOT.'/voipssl/'.VOIP_CERTIFICATE_IOS;
				$ctx = stream_context_create();
				stream_context_set_option($ctx, 'ssl', 'local_cert', $pemFile);
				stream_context_set_option($ctx, 'ssl', 'password', $passphrase);

				// Open a connection to the APNS server
				$fp = stream_socket_client(
					PUSH_END_POINT_IOS, $err,
					$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
				if (!$fp)
					exit("Failed to connect: $err $errstr" . PHP_EOL);

				// echo 'Connected to APNS' . PHP_EOL;

				$body = array("aps"=> array("alert"=> $message, "sound"=> "notification_sound.mp3"),
	                        "VOIPLogoutStatus"=>1,
	                        "message"=>ERROR_602,
	                        "user_id"=>$userId
				);

				$payload = json_encode($body);

				// Build the binary notification
				$msg = chr(0) . pack('n', 32) . pack('H*', $fcmVoipToken) . pack('n', strlen($payload)) . $payload;

				// Send it to the server
				$result = fwrite($fp, $msg, strlen($msg));

				if (!$result)
					// $var = 'Message not delivered' . PHP_EOL;
					// echo 'Message not delivered' . PHP_EOL;
					$var = 0;
				else
					// $var = 'Message successfully delivered' . PHP_EOL;
					// echo 'Message successfully delivered' . PHP_EOL;
					$var = 1;

				// Close the connection to the server
				fclose($fp);
				return $var;
			}
		}

		public function sendLogoutPushNotificationAndroidOne($params = array()) {
			if(!empty($params))
			{
				$responseData = $this->sendLogoutPushNotificationOnAndroid($params);
				return $responseData;
			}
		}

		public function sendLogoutPushNotificationOnAndroid($params = array())
		{
			if(!empty($params['device_token'])) {
				$dataUserDevice = isset($params['device_token']) ? $params['device_token']:"";
				$tokenRegistrationIds = array($dataUserDevice);
				$userId = isset($params['user_id']) ? $params['user_id'] : "";
				$voipPushMessage = array
						(
							'VOIPLogoutStatus'=> 1,
							'VOIPCall'=> 1,
							'message'=>ERROR_602,
							'user_id'=>$userId
						);
				$postFeilds = array
				(
					'registration_ids' 	=> $tokenRegistrationIds,
					'data'			=> $voipPushMessage,
					'priority' => 'high',
					'notification' => $voipPushMessage
				);
				$headers = array
				(
					'Authorization: key=' . GCM_ACCESS_KEY,
					'Content-Type: application/json'
				);
				// echo "<pre>";print_r(GCM_ACCESS_KEY);exit();
				$ch = curl_init();
				curl_setopt( $ch,CURLOPT_URL, PUSH_END_POINT_ANDROID );
				curl_setopt( $ch,CURLOPT_POST, true );
				curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
				curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
				curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $postFeilds ) );
				$result = curl_exec($ch );
				curl_close( $ch );
				return $result;
			}
			else
			{
				return $result;
			}

		}

		/*
		--------------------------------------------------------------------------
		ON: 22-02-2019
		I/P:
		O/P: True or False
		Desc: Baton Role Exchange push notification
		--------------------------------------------------------------------------
		*/

		public function sendRoleExchangePushNotification($params)
		{
			if(!empty($params))
			{
				$sendRoleExchangePushNotification = $this->VoipPushNotification->find('first', array('conditions' => array('VoipPushNotification.user_id' => $params['user_id'])));
				if($params['from_user_id'] == 0)
				{
					$userData['UserProfile']['first_name'] = "Switch";
					$userData['UserProfile']['last_name'] = "Borad";
				}
				else
				{
					$userData = $this->User->find('first', array('conditions'=> array("User.id"=> $params['from_user_id'])));
				}
				if($params['user_id'] == 0)
				{
					$userDataTo['UserProfile']['first_name'] = "Switch";
					$userDataTo['UserProfile']['last_name'] = "Borad";
				}
				else
				{
					$userDataTo = $this->User->find('first', array('conditions'=> array("User.id"=> $params['user_id'])));
				}
				if(!empty($sendRoleExchangePushNotification))
				{
					$param['device_token'] = $sendRoleExchangePushNotification['VoipPushNotification']['device_token'];
					$param['device_type'] = $sendRoleExchangePushNotification['VoipPushNotification']['device_type'];
					$param['request_type'] = $params['request_type'];
					$param['first_name'] = $userData['UserProfile']['first_name'];
					$param['last_name'] = $userData['UserProfile']['last_name'];
					$param['push_on'] = $params['push_on'];
					$param['role_id'] = $params['role_id'];
					$param['is_qr_request'] = isset($params['is_qr_request']) ? $params['is_qr_request'] : 0;
					$param['from_user_id'] = $params['from_user_id'];
					$param['to_user'] = $params['user_id'];

					$getBatonRoleName = $this->BatonRole->find('first', array('conditions'=> array("BatonRole.id"=> $param['role_id'])));
					$param['role_name'] = $getBatonRoleName['BatonRole']['baton_roles'];
					$param['to_user_first_name'] = $userDataTo['UserProfile']['first_name'];
					$param['to_user_last_name'] = $userDataTo['UserProfile']['last_name'];


					if($param['device_type'] == 'ios' || $param['device_type'] == 'iOS')
					{
						$sendNotification = $this->roleExchangePushNotificationOnIos($param);
					}
					else if($param['device_type'] == 'android' || $param['device_type'] == 'Android')
					{
						$sendNotification = $this->roleExchangePushNotificationOnAndroid($param);
					}
				}
			}
		}

		public function roleExchangePushNotificationOnIos($params = array())
		{
			if(!empty($params))
			{
				$var = "";
				$requestTypeVal = 0;
				if($params['request_type'] == 'transfer')
				{
					// $messageVal = $params['first_name']." ".$params['last_name'] ." Transfer role to you.";
					$messageVal = ROLEEX_001.$params['first_name']." ".$params['last_name'].".";
					if($params['to_user'] != 0){
						$custMessage = $params['first_name']." ".$params['last_name']." Transfer " .$params['role_name']. " role to ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
					else{
						$custMessage = $params['first_name']." ".$params['last_name']." Transfer ".$params['role_name']." to Switch Borad ";
					}
				}
				elseif ($params['request_type'] == 'transfer_accept') {
					$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_004;
					$var = "Accepted";
					if($params['to_user'] != 0){
						$custMessage = "Transfer accepted by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
					else{
						$custMessage = "Transfer accepted by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
				}
				elseif ($params['request_type'] == 'transfer_rejected') {
					$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_005;
					$var = "Rejected";
					$custMessage = "Transfer rejected by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				elseif ($params['request_type'] == 'request') {
					$messageVal = ROLEEX_001.$params['first_name']." ".$params['last_name'];
					$requestTypeVal = $params['push_on'];
					if($params['to_user'] != 0){
						$custMessage = $params['first_name']." ".$params['last_name']." requested ".$params['role_name']." from ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
					else{
						$custMessage = $params['first_name']." ".$params['last_name']." requested ".$params['role_name']." from ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
				}
				elseif ($params['request_type'] == 'request_accept') {
					$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_002;
					$var = "Accepted";
					$requestTypeVal = $params['push_on'];
					if($params['to_user'] != 0){
						$custMessage = "Request for role ".$params['role_name']." accepted by ".$params['first_name']." ".$params['last_name']." sent form ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
					else{
						$custMessage = "Request for role ".$params['role_name']." accepted by ".$params['first_name']." ".$params['last_name']." sent form ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
				}
				elseif ($params['request_type'] == 'request_reject') {
					$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_003;
					$var = "Rejected";
					if($params['to_user'] != 0){
						$custMessage = "Request for role ".$params['role_name']." rejected by ".$params['first_name']." ".$params['last_name']." sent form ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
					else{
						$custMessage = "Request for role ".$params['role_name']." rejected by ".$params['first_name']." ".$params['last_name']." sent form ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
				}
				elseif ($params['request_type'] == 'revoke_request') {
					$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_006;
					$var = "Revoked";
					$custMessage = "Request revoked by ".$params['first_name']." ".$params['last_name']." Sent to ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				elseif ($params['request_type'] == 'revoke_transfer') {
					$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_006;
					$var = "Revoked";
					$custMessage = "Transfer revoked by ".$params['first_name']." ".$params['last_name']." Sent to ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				elseif ($params['request_type'] == 'timeout') {
					$var = "Timeout";
					$messageVal = ROLEEX_007;
				}
				elseif ($params['request_type'] == 'swichborad_accept') {
					$var = "Accepted";
					$messageVal = ROLEEX_008;
					$requestTypeVal = $params['push_on'];
					$custMessage = "Request accepted by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." requested by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				elseif ($params['request_type'] == 'swichborad_rejected') {
					$var = "Rejected";
					$messageVal = ROLEEX_009;
					$custMessage = "Request rejected by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." requested by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				//TO do this will be removed when auto accept functionality goes live
				elseif ($params['request_type'] == 'swichborad_transfered') {
					$var = "Requested";
					$messageVal = ROLEEX_019;
					$custMessage = "Switch Borad transfer ".$params['role_name']." to ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				// elseif ($params['request_type'] == 'swichborad_added') {
				elseif ($params['request_type'] == 'swichborad_assigned') {
					$var = "Assigned";
					$messageVal = ROLEEX_011;
					$requestTypeVal = $params['push_on'];
					$custMessage = "Switch Borad transfer ".$params['role_name']." to ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				elseif ($params['request_type'] == 'swichborad_removed') {
					$var = "Rejected";
					$messageVal = ROLEEX_012;
				}
				elseif ($params['request_type'] == 'swichborad_transfer_accept') {
					$var = "Accepted";
					$messageVal = ROLEEX_013;
					$custMessage = "Transfer accepted by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				elseif ($params['request_type'] == 'swichborad_transfer_rejected') {
					$var = "Rejected";
					$messageVal = ROLEEX_014;
					$custMessage = "Transfer rejected by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered by ".$params['to_user_first_name']." ".$params['to_user_last_name'];

				}
				elseif ($params['request_type'] == 'swichborad_unassign') {
					$var = "Unassigned";
					$messageVal = ROLEEX_017;
				}
				elseif ($params['request_type'] == 'swichborad_deleted') {
					$var = "Deleted";
					$messageVal = ROLEEX_016;
				}
				elseif ($params['request_type'] == 'qr_transfer') {
					$custMessage = "Transfer accepted by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered  through qr code by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}

				// $transactionParams['request_type'] = $params['request_type'];
				// $transactionParams['from_user'] = $params['from_user_id'];
				// $transactionParams['to_user'] = $params['to_user'];
				// $transactionParams['role_id'] = $params['role_id'];
				// $transactionParams['custom_message'] = $custMessage;

				// $saveRoleExchangeTransactions = $this->UserBatonRoleTransaction->saveRoleExchangeTransactions($transactionParams);

				$fcmVoipToken = $params['device_token'];
				$passphrase = 'password';
				$message = 'User Role Exchange';
				$pemFile = WWW_ROOT.'/voipssl/'.VOIP_CERTIFICATE_IOS;
				$ctx = stream_context_create();
				stream_context_set_option($ctx, 'ssl', 'local_cert', $pemFile);
				stream_context_set_option($ctx, 'ssl', 'password', $passphrase);

				// Open a connection to the APNS server
				$fp = stream_socket_client(
					PUSH_END_POINT_IOS, $err,
					$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
				if (!$fp)
					exit("Failed to connect: $err $errstr" . PHP_EOL);

				// echo 'Connected to APNS' . PHP_EOL;

				$body = array("aps"=> array("alert"=> $message, "sound"=> "notification_sound.mp3"),
	                        "VOIPRoleExchange"=>1,
	                        "role_type"=>2,
	                        "message"=>$messageVal,
	                        'title'=>"Baton Role ".$var,
	                        'push_on'=>$requestTypeVal,
	                        'role_id'=>$params['role_id'],
	                        'is_qr_request'=>$params['is_qr_request']
				);

				$payload = json_encode($body);

				// Build the binary notification
				$msg = chr(0) . pack('n', 32) . pack('H*', $fcmVoipToken) . pack('n', strlen($payload)) . $payload;

				// Send it to the server
				$result = fwrite($fp, $msg, strlen($msg));

				if (!$result)
					// $var = 'Message not delivered' . PHP_EOL;
					// echo 'Message not delivered' . PHP_EOL;
					$var = 0;
				else
					// $var = 'Message successfully delivered' . PHP_EOL;
					// echo 'Message successfully delivered' . PHP_EOL;
					$var = 1;

				// Close the connection to the server
				fclose($fp);
				return $var;
			}
		}

		public function roleExchangePushNotificationOnAndroid($params = array())
		{
			if(!empty($params['device_token'])){
				$var = "";
				$requestTypeVal = 0;
				if($params['request_type'] == 'transfer')
				{
					// $messageVal = $params['first_name']." ".$params['last_name'] ." Transfer role to you.";
					$messageVal = ROLEEX_001.$params['first_name']." ".$params['last_name'].".";
					if($params['to_user'] != 0){
						$custMessage = $params['first_name']." ".$params['last_name']." Transfer " .$params['role_name']. " role to ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
					else{
						$custMessage = $params['first_name']." ".$params['last_name']." Transfer ".$params['role_name']." to Switch Borad ";
					}
				}
				elseif ($params['request_type'] == 'transfer_accept') {
					$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_004;
					$var = "Accepted";
					if($params['to_user'] != 0){
						$custMessage = "Transfer accepted by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
					else{
						$custMessage = "Transfer accepted by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
				}
				elseif ($params['request_type'] == 'transfer_rejected') {
					$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_005;
					$var = "Rejected";
					$custMessage = "Transfer rejected by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				elseif ($params['request_type'] == 'request') {
					$messageVal = ROLEEX_001.$params['first_name']." ".$params['last_name'];
					$requestTypeVal = $params['push_on'];
					if($params['to_user'] != 0){
						$custMessage = $params['first_name']." ".$params['last_name']." requested ".$params['role_name']." from ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
					else{
						$custMessage = $params['first_name']." ".$params['last_name']." requested ".$params['role_name']." from ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
				}
				elseif ($params['request_type'] == 'request_accept') {
					$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_002;
					$var = "Accepted";
					$requestTypeVal = $params['push_on'];
					if($params['to_user'] != 0){
						$custMessage = "Request for role ".$params['role_name']." accepted by ".$params['first_name']." ".$params['last_name']." sent form ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
					else{
						$custMessage = "Request for role ".$params['role_name']." accepted by ".$params['first_name']." ".$params['last_name']." sent form ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
				}
				elseif ($params['request_type'] == 'request_reject') {
					$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_003;
					$var = "Rejected";
					if($params['to_user'] != 0){
						$custMessage = "Request for role ".$params['role_name']." rejected by ".$params['first_name']." ".$params['last_name']." sent form ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
					else{
						$custMessage = "Request for role ".$params['role_name']." rejected by ".$params['first_name']." ".$params['last_name']." sent form ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
				}

				elseif ($params['request_type'] == 'revoke_request') {
					$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_006;
					$var = "Revoked";
					$custMessage = "Request revoked by ".$params['first_name']." ".$params['last_name']." Sent to ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				elseif ($params['request_type'] == 'revoke_transfer') {
					$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_006;
					$var = "Revoked";
					$custMessage = "Transfer revoked by ".$params['first_name']." ".$params['last_name']." Sent to ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				elseif ($params['request_type'] == 'timeout') {
					$var = "Timeout";
					$messageVal = ROLEEX_007;
				}
				elseif ($params['request_type'] == 'swichborad_accept') {
					$var = "Accepted";
					$messageVal = ROLEEX_008;
					$requestTypeVal = $params['push_on'];
					$custMessage = "Request accepted by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." requested by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				elseif ($params['request_type'] == 'swichborad_rejected') {
					$var = "Rejected";
					$messageVal = ROLEEX_009;
					$custMessage = "Request rejected by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." requested by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				//TO do this will be removed when auto accept functionality goes live
				elseif ($params['request_type'] == 'swichborad_transfered') {
					$var = "Requested";
					$messageVal = ROLEEX_019;
					$custMessage = "Switch Borad transfer ".$params['role_name']." to ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				// elseif ($params['request_type'] == 'swichborad_added') {
				elseif ($params['request_type'] == 'swichborad_assigned') {
					$var = "Assigned";
					$messageVal = ROLEEX_011;
					$requestTypeVal = $params['push_on'];
					$custMessage = "Switch Borad transfer ".$params['role_name']." to ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				elseif ($params['request_type'] == 'swichborad_removed') {
					$var = "Rejected";
					$messageVal = ROLEEX_012;
				}elseif ($params['request_type'] == 'swichborad_transfer_accept') {
					$var = "Accepted";
					$messageVal = ROLEEX_013;
					$custMessage = "Transfer accepted by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				elseif ($params['request_type'] == 'swichborad_transfer_rejected') {
					$var = "Rejected";
					$messageVal = ROLEEX_014;
					$custMessage = "Transfer rejected by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				elseif ($params['request_type'] == 'swichborad_unassign') {
					$var = "Unassigned";
					$messageVal = ROLEEX_017;
				}
				elseif ($params['request_type'] == 'swichborad_deleted') {
					$var = "Deleted";
					$messageVal = ROLEEX_016;
				}
				elseif ($params['request_type'] == 'qr_transfer') {
					$custMessage = "Transfer accepted by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered  through qr code by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				// $transactionParams['request_type'] = $params['request_type'];
				// $transactionParams['from_user'] = $params['from_user_id'];
				// $transactionParams['to_user'] = $params['to_user'];
				// $transactionParams['role_id'] = $params['role_id'];
				// $transactionParams['is_qr_request'] = $params['is_qr_request'];
				// $transactionParams['custom_message'] = $custMessage;

				// $saveRoleExchangeTransactions = $this->UserBatonRoleTransaction->saveRoleExchangeTransactions($transactionParams);
				$dataUserDevice = isset($params['device_token']) ? $params['device_token']:"";
				$tokenRegistrationIds = array($dataUserDevice);
				$voipPushMessage = array
						(
							'VOIPRoleExchange'=> 1,
							'VOIPCall'=> 1,
							'role_type'=>2,
							'type_name'=>'PushRoleExchange',
							'message'=>$messageVal,
							'title'=> "Baton Role ".$var,
							'push_on'=>$requestTypeVal,
							'role_id'=>$params['role_id'],
	                        'is_qr_request'=>$params['is_qr_request']
						);
				$postFeilds = array
				(
					'registration_ids' 	=> $tokenRegistrationIds,
					'data'			=> $voipPushMessage,
					'priority' => 'high'
				);
				$headers = array
				(
					'Authorization: key=' . GCM_ACCESS_KEY,
					'Content-Type: application/json'
				);
				// echo "<pre>";print_r(GCM_ACCESS_KEY);exit();
				$ch = curl_init();
				curl_setopt( $ch,CURLOPT_URL, PUSH_END_POINT_ANDROID );
				curl_setopt( $ch,CURLOPT_POST, true );
				curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
				curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
				curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $postFeilds ) );
				$result = curl_exec($ch );
				curl_close( $ch );
				return $var = 1;
			}
			else
			{
				return $var = 0;
			}

		}

		/*
		----------------------------------------------------------------------------------------------
		On: 12-06-2019
		I/P: user id's(from and to)
		O/P: Push Success or Failure
		Desc: Permanent Role Exchange
		----------------------------------------------------------------------------------------------
		*/

		public function sendPermanentRoleExchangePushNotification($params)
		{
			if(!empty($params))
			{
				$sendRoleExchangePushNotification = $this->VoipPushNotification->find('first', array('conditions' => array('VoipPushNotification.user_id' => $params['other_user_id'])));
				$userData = $this->User->find('first', array('conditions'=> array("User.id"=> $params['user_id'])));

				if(!empty($sendRoleExchangePushNotification))
				{
					$param['device_token'] = $sendRoleExchangePushNotification['VoipPushNotification']['device_token'];
					$param['request_type'] = $params['request_type'];
					$param['first_name'] = $userData['UserProfile']['first_name'];
					$param['last_name'] = $userData['UserProfile']['last_name'];
					$sendNotification = $this->permanentRoleExchangePushNotificationOnIos($param);
					$sendNotification = $this->permanentroleExchangePushNotificationOnAndroid($param);
				}
			}
		}

		public function permanentRoleExchangePushNotificationOnIos($params = array())
		{
			if(!empty($params))
			{
				$var = "";
				if($params['request_type'] == 'transfer')
				{
					// $messageVal = $params['first_name']." ".$params['last_name'] ." Transfer role to you.";
					$messageVal = PROLEEX_001.$params['first_name']." ".$params['last_name'].".";
				}
				elseif ($params['request_type'] == 'transfer_accept') {
					$messageVal = $params['first_name']." ".$params['last_name'].PROLEEX_002;
					$var = "Accepted";
				}
				elseif ($params['request_type'] == 'transfer_rejected') {
					$messageVal = $params['first_name']." ".$params['last_name'].PROLEEX_003;
					$var = "Rejected";
				}
				elseif ($params['request_type'] == 'revoke_transfer') {
					$messageVal = $params['first_name']." ".$params['last_name'].PROLEEX_004;
					$var = "Revoked";
				}
				elseif ($params['request_type'] == 'timeout') {
					$var = "Timeout";
					$messageVal = PROLEEX_007;
				}
				$fcmVoipToken = $params['device_token'];
				$passphrase = 'password';
				$message = 'User Role Exchange';
				$pemFile = WWW_ROOT.'/voipssl/'.VOIP_CERTIFICATE_IOS;
				$ctx = stream_context_create();
				stream_context_set_option($ctx, 'ssl', 'local_cert', $pemFile);
				stream_context_set_option($ctx, 'ssl', 'password', $passphrase);

				// Open a connection to the APNS server
				$fp = stream_socket_client(
					PUSH_END_POINT_IOS, $err,
					$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
				if (!$fp)
					exit("Failed to connect: $err $errstr" . PHP_EOL);

				// echo 'Connected to APNS' . PHP_EOL;

				$body = array("aps"=> array("alert"=> $message, "sound"=> "notification_sound.mp3"),
	                        "VOIPRoleExchange"=>1,
	                        "role_type"=>1,
	                        "message"=>$messageVal,
	                        'title'=>"Permanent Role ".$var
				);

				$payload = json_encode($body);

				// Build the binary notification
				$msg = chr(0) . pack('n', 32) . pack('H*', $fcmVoipToken) . pack('n', strlen($payload)) . $payload;

				// Send it to the server
				$result = fwrite($fp, $msg, strlen($msg));

				if (!$result)
					// $var = 'Message not delivered' . PHP_EOL;
					// echo 'Message not delivered' . PHP_EOL;
					$var = 0;
				else
					// $var = 'Message successfully delivered' . PHP_EOL;
					// echo 'Message successfully delivered' . PHP_EOL;
					$var = 1;

				// Close the connection to the server
				fclose($fp);
				return $var;
			}
		}

		public function permanentroleExchangePushNotificationOnAndroid($params = array())
		{
			if(!empty($params['device_token'])) {
				$var = "";
				if($params['request_type'] == 'transfer')
				{
					// $messageVal = $params['first_name']." ".$params['last_name'] ." Transfer role to you.";
					$messageVal = PROLEEX_001.$params['first_name']." ".$params['last_name'].".";
				}
				elseif ($params['request_type'] == 'transfer_accept') {
					$messageVal = $params['first_name']." ".$params['last_name'].PROLEEX_002;
					$var = "Accepted";
				}
				elseif ($params['request_type'] == 'transfer_rejected') {
					$messageVal = $params['first_name']." ".$params['last_name'].PROLEEX_003;
					$var = "Rejected";
				}
				elseif ($params['request_type'] == 'revoke_transfer') {
					$messageVal = $params['first_name']." ".$params['last_name'].PROLEEX_004;
					$var = "Revoked";
				}
				elseif ($params['request_type'] == 'timeout') {
					$var = "Timeout";
					$messageVal = PROLEEX_007;
				}
				$dataUserDevice = isset($params['device_token']) ? $params['device_token']:"";
				$tokenRegistrationIds = array($dataUserDevice);
				$voipPushMessage = array
						(
							'VOIPRoleExchange'=> 1,
							'role_type'=>1,
							'message'=>$messageVal,
							'title'=> "Permanent Role".$var
						);
				$postFeilds = array
				(
					'registration_ids' 	=> $tokenRegistrationIds,
					'data'			=> $voipPushMessage
				);
				$headers = array
				(
					'Authorization: key=' . GCM_ACCESS_KEY,
					'Content-Type: application/json'
				);
				// echo "<pre>";print_r(GCM_ACCESS_KEY);exit();
				$ch = curl_init();
				curl_setopt( $ch,CURLOPT_URL, PUSH_END_POINT_ANDROID );
				curl_setopt( $ch,CURLOPT_POST, true );
				curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
				curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
				curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $postFeilds ) );
				$result = curl_exec($ch );
				curl_close( $ch );
				return $var = 1;
			}
			else
			{
				return $var = 0;
			}

		}
		///*************************Multiple Device login for medicbleep version v1[END]******************///


		///*************************Multiple Device login for medicbleep version v2[START]****************///

		/*
		--------------------------------------------------------------------------
		ON: 03-02-2020
		I/P:
		O/P: True or False
		Desc: This function will send a push notification to multiple login event
		--------------------------------------------------------------------------
		*/

		public function sendMultipleDeviceLoginPushNotification($params =array())
		{
			if(!empty($params)) {
				$userVoipData = $this->VoipPushNotification->find('all', array('conditions' => array('VoipPushNotification.user_id' => $params['user_id'], 'status'=>1)));
				if(!empty($userVoipData))
				{
					$counter = 0;
					foreach ($userVoipData as $key => $value) {
						$params['device_token'] = $value['VoipPushNotification']['device_token'];
						$params['user_id'] = $params['user_id'];
						if($params['device_id'] != $value['VoipPushNotification']['device_id'])
						{

							$sendNotification = $this->sendMultipleDeviceLoginPushNotificationOnIosv2($params);
							if($sendNotification == 200)
							{
								// $deleteRecords = $this->VoipPushNotification->deleteAll(array("user_id"=>$params['user_id'], "device_id !="=>$params['device_id']));

								$deregisterData = array("user_id"=> $params['user_id'] , "device_id !="=>$params['device_id'] );
								$this->VoipPushNotification->updateAll( array("status"=> 0), $deregisterData );
							}
							$sendNotificationAndroid = $this->sendLogoutPushNotificationAndroidv2($params);
							if($sendNotificationAndroid == 1)
							{
								// $deleteRecords = $this->VoipPushNotification->deleteAll(array("user_id"=>$params['user_id'], "device_id !="=>$params['device_id']));
								$deregisterData = array("user_id"=>$params['user_id'], "device_id !="=>$params['device_id'] );
								$this->VoipPushNotification->updateAll( array("status"=> 0), $deregisterData );
							}
							$counter++;
						}
					}
					return $counter;
				}

			}else{
				return "Data is empty";
			}
		}

		public function sendMultipleDeviceLoginPushNotificationOnIosv2($params = array())
		{
			if(!empty($params))
			{

				$http2_server = PUSH_END_POINT_IOS_V2;
				$device_token = $params['device_token'];
				$url = "{$http2_server}/3/device/{$device_token}";
				$app_bundle_id = APP_BUNDLE_ID_V2;
				$http2ch = curl_init();
				$type = 1;
				$message = '{
				   "aps":{
				     "alert":"Either your session has been expired or you have logged-in on another device.",
				     "priority":10,
				     "sound":{
				       "critical":1,
				       "name":"notification_sound.mp3",
				       "volume":0.1
				     },
				     "mutable-content":1
				   },
				   "data":{
				     "type_name":"PushMultipleDeviceLogin",
				     "message":"Either your session has been expired or you have logged-in on another device.",
				     "title":"Multiple Device Login"
				   }
				 }';

				// certificate
				$push_cert = WWW_ROOT.'/voipssl/'.PUSH_CERTIFICATE_IOS_V2;
				$push_cert_key = WWW_ROOT.'/voipssl/'.PUSH_CERTIFICATE_KEY_V2;

				// headers
				$headers = array(
				    "apns-topic: {$app_bundle_id}",
				    "User-Agent: My Sender",
				);

				// other curl options
				curl_setopt_array($http2ch, array(
				    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_2_0,
				    CURLOPT_URL => $url,
				    CURLOPT_PORT => 443,
				    CURLOPT_HTTPHEADER => $headers,
				    CURLOPT_POST => TRUE,
				    CURLOPT_POSTFIELDS => $message,
				    CURLOPT_RETURNTRANSFER => TRUE,
				    CURLOPT_TIMEOUT => 30,
				    CURLOPT_SSL_VERIFYPEER => false,
				    CURLOPT_SSLCERT => $push_cert,
				    CURLOPT_SSLKEY => $push_cert_key,
				    CURLOPT_HEADER => 1
				));

				// go...
				$result = curl_exec($http2ch);
				if ($result === FALSE) {
				  throw new Exception("Curl failed: " .  curl_error($http2ch));
				}

				$result;

				// get response
				$status = curl_getinfo($http2ch, CURLINFO_HTTP_CODE);

				$status;
				return $status;
			}
		}

		public function sendLogoutPushNotificationAndroidv2($params = array()) {
			if(!empty($params))
			{
				$responseData = $this->sendLogoutPushNotificationOnAndroidv2($params);
				return $responseData;
			}
		}

		public function sendLogoutPushNotificationOnAndroidv2($params = array())
		{
			if(!empty($params['device_token'])) {
				$dataUserDevice = isset($params['device_token']) ? $params['device_token']:"";
				$tokenRegistrationIds = array($dataUserDevice);
				$userId = isset($params['user_id']) ? $params['user_id'] : "";
				$sound_file = "notification_sound";
				$voipPushMessage = array
						(
							'message'=>ERROR_602,
							'user_id'=>$userId,
							'type_name'=>'PushMultipleDeviceLogin',
							'sound'=>$sound_file,
							'channel_id'=>"Medic Bleep Force Logout"
						);
				$postFeilds = array
				(
					'registration_ids' 	=> $tokenRegistrationIds,
					'data'			=> $voipPushMessage,
					'priority' => 'high'
				);
				$headers = array
				(
					'Authorization: key=' . GCM_ACCESS_KEY,
					'Content-Type: application/json'
				);
				// echo "<pre>";print_r(GCM_ACCESS_KEY);exit();
				$ch = curl_init();
				curl_setopt( $ch,CURLOPT_URL, PUSH_END_POINT_ANDROID );
				curl_setopt( $ch,CURLOPT_POST, true );
				curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
				curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
				curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $postFeilds ) );
				$result = curl_exec($ch );
				curl_close( $ch );
				return $var = 1;
			}
			else
			{
				return $var = 0;
			}

		}

		/*
		--------------------------------------------------------------------------
		ON: 03-02-2020
		I/P:
		O/P: True or False
		Desc: This function will send a push notification to multiple login event
		--------------------------------------------------------------------------
		*/

		public function sendRoleExchangePushNotificationv2($params)
		{
			if(!empty($params))
			{
				$sendRoleExchangePushNotification = $this->VoipPushNotification->find('first', array('conditions' => array('VoipPushNotification.user_id' => $params['user_id'], 'status'=>'1')));
				// return $sendRoleExchangePushNotification;
				if($params['from_user_id'] == 0)
				{
					$userData['UserProfile']['first_name'] = "Switch";
					$userData['UserProfile']['last_name'] = "Borad";
				}
				else
				{
					$userData = $this->User->find('first', array('conditions'=> array("User.id"=> $params['from_user_id'])));
				}
				if($params['user_id'] == 0)
				{
					$userDataTo['UserProfile']['first_name'] = "Switch";
					$userDataTo['UserProfile']['last_name'] = "Borad";
				}
				else
				{
					$userDataTo = $this->User->find('first', array('conditions'=> array("User.id"=> $params['user_id'])));
				}
				if(!empty($sendRoleExchangePushNotification))
				{
					$param['device_token'] = $sendRoleExchangePushNotification['VoipPushNotification']['device_token'];
					$param['device_type'] = $sendRoleExchangePushNotification['VoipPushNotification']['device_type'];
					$param['request_type'] = $params['request_type'];
					$param['first_name'] = $userData['UserProfile']['first_name'];
					$param['last_name'] = $userData['UserProfile']['last_name'];
					$param['push_on'] = $params['push_on'];
					$param['role_id'] = $params['role_id'];
					$param['is_qr_request'] = isset($params['is_qr_request']) ? $params['is_qr_request'] : 0;
					$param['from_user_id'] = $params['from_user_id'];
					$param['to_user'] = $params['user_id'];

					$getBatonRoleName = $this->BatonRole->find('first', array('conditions'=> array("BatonRole.id"=> $param['role_id'])));
					$param['role_name'] = $getBatonRoleName['BatonRole']['baton_roles'];
					$param['to_user_first_name'] = $userDataTo['UserProfile']['first_name'];
					$param['to_user_last_name'] = $userDataTo['UserProfile']['last_name'];

					$param['is_keep_bleeping'] = $params["is_keep_bleeping"];
					$param['volume'] = $params["volume"];
					$param['is_user_available'] = $params["is_user_available"];
					$param['is_dnd_active'] = $params["is_dnd_active"];

					
					if($param['device_type'] == 'ios' || $param['device_type'] == 'iOS')
					{
						$sendNotification = $this->roleExchangePushNotificationOnIosv2($param);
					}
					else if($param['device_type'] == 'android' || $param['device_type'] == 'Android')
					{
						$sendNotification = $this->roleExchangePushNotificationOnAndroidv2($param);
					}
				}
			}
		}

		public function roleExchangePushNotificationOnIosv2($params = array())
		{
			if(!empty($params))
			{
				$var = "";
				$requestTypeVal = 0;
				if($params['request_type'] == 'transfer')
				{
					// $messageVal = $params['first_name']." ".$params['last_name'] ." Transfer role to you.";
					$messageVal = ROLEEX_001.$params['first_name']." ".$params['last_name'].".";
					if($params['to_user'] != 0){
						$custMessage = $params['first_name']." ".$params['last_name']." Transfer " .$params['role_name']. " role to ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
					else{
						$custMessage = $params['first_name']." ".$params['last_name']." Transfer ".$params['role_name']." to Switch Borad ";
					}
				}
				elseif ($params['request_type'] == 'transfer_accept') {
					$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_004;
					$var = "Accepted";
					if($params['to_user'] != 0){
						$custMessage = "Transfer accepted by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
					else{
						$custMessage = "Transfer accepted by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
				}
				elseif ($params['request_type'] == 'transfer_rejected') {
					$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_005;
					$var = "Rejected";
					$custMessage = "Transfer rejected by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				elseif ($params['request_type'] == 'request') {
					$messageVal = ROLEEX_001.$params['first_name']." ".$params['last_name'];
					$requestTypeVal = $params['push_on'];
					if($params['to_user'] != 0){
						$custMessage = $params['first_name']." ".$params['last_name']." requested ".$params['role_name']." from ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
					else{
						$custMessage = $params['first_name']." ".$params['last_name']." requested ".$params['role_name']." from ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
				}
				elseif ($params['request_type'] == 'request_accept') {
					$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_002;
					$var = "Accepted";
					$requestTypeVal = $params['push_on'];
					if($params['to_user'] != 0){
						$custMessage = "Request for role ".$params['role_name']." accepted by ".$params['first_name']." ".$params['last_name']." sent form ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
					else{
						$custMessage = "Request for role ".$params['role_name']." accepted by ".$params['first_name']." ".$params['last_name']." sent form ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
				}
				elseif ($params['request_type'] == 'request_reject') {
					$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_003;
					$var = "Rejected";
					if($params['to_user'] != 0){
						$custMessage = "Request for role ".$params['role_name']." rejected by ".$params['first_name']." ".$params['last_name']." sent form ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
					else{
						$custMessage = "Request for role ".$params['role_name']." rejected by ".$params['first_name']." ".$params['last_name']." sent form ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
				}
				elseif ($params['request_type'] == 'revoke_request') {
					$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_006;
					$var = "Revoked";
					$custMessage = "Request revoked by ".$params['first_name']." ".$params['last_name']." Sent to ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				elseif ($params['request_type'] == 'revoke_transfer') {
					$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_006;
					$var = "Revoked";
					$custMessage = "Transfer revoked by ".$params['first_name']." ".$params['last_name']." Sent to ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				elseif ($params['request_type'] == 'timeout') {
					$var = "Timeout";
					$messageVal = ROLEEX_007;
				}
				elseif ($params['request_type'] == 'swichborad_accept') {
					$var = "Accepted";
					$messageVal = ROLEEX_008;
					$requestTypeVal = $params['push_on'];
					$custMessage = "Request accepted by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." requested by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				elseif ($params['request_type'] == 'swichborad_rejected') {
					$var = "Rejected";
					$messageVal = ROLEEX_009;
					$custMessage = "Request rejected by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." requested by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				//TO do this will be removed when auto accept functionality goes live
				elseif ($params['request_type'] == 'swichborad_transfered') {
					$var = "Requested";
					$messageVal = ROLEEX_019;
					$custMessage = "Switch Borad transfer ".$params['role_name']." to ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				// elseif ($params['request_type'] == 'swichborad_added') {
				elseif ($params['request_type'] == 'swichborad_assigned') {
					$var = "Assigned";
					$messageVal = ROLEEX_011;
					$requestTypeVal = $params['push_on'];
					$custMessage = "Switch Borad transfer ".$params['role_name']." to ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				elseif ($params['request_type'] == 'swichborad_removed') {
					$var = "Rejected";
					$messageVal = ROLEEX_012;
				}
				elseif ($params['request_type'] == 'swichborad_transfer_accept') {
					$var = "Accepted";
					$messageVal = ROLEEX_013;
					$custMessage = "Transfer accepted by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				elseif ($params['request_type'] == 'swichborad_transfer_rejected') {
					$var = "Rejected";
					$messageVal = ROLEEX_014;
					$custMessage = "Transfer rejected by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered by ".$params['to_user_first_name']." ".$params['to_user_last_name'];

				}
				elseif ($params['request_type'] == 'swichborad_unassign') {
					$var = "Unassigned";
					$messageVal = ROLEEX_017;
				}
				elseif ($params['request_type'] == 'swichborad_deleted') {
					$var = "Deleted";
					$messageVal = ROLEEX_016;
				}
				elseif ($params['request_type'] == 'qr_transfer') {
					$messageVal = "Transfer accepted by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered  through qr code by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}

				$http2_server = PUSH_END_POINT_IOS_V2;
				$device_token = $params['device_token'];
				$url = "{$http2_server}/3/device/{$device_token}";
				$app_bundle_id = APP_BUNDLE_ID_V2;
				$http2ch = curl_init();
				$type = 1;
				$sound_file = '"silent.wav"';
				$batonRoleExchangeTitle = "Baton Role ".$var;
				$batonRoleExchangeTitle =$batonRoleExchangeTitle;
				$is_qr_request = $params['is_qr_request'];
				if($params['is_dnd_active'] == "1" || $params['is_user_available'] == "0")
				{
					$sound_file = '"silent.wav"';
				}
				else if($params['is_dnd_active'] == "0" || $params['is_user_available'] == "1")
				{
					if($params['is_keep_bleeping'] == "1")
					{
						$volume = $params['volume'];
						$sound_file = '{"critical":1,
	       								"name":"notification_sound.mp3",
	       								"volume":"' . $volume . '"}';
					}
					else{
						$sound_file = '"notification_sound.mp3"';
					}
				}
				$message = '{
				   "aps":{
				     "alert":"' . $messageVal . '",
				     "priority":10,
				     "sound":' . $sound_file . ',
				     "mutable-content":1
				   },
				   "data":{
				     "type_name":"PushRoleExchange",
				     "message":"' . $messageVal . '",
				     "role_type": "2",
				     "is_qr_request":"' . $is_qr_request . '",
				     "title":"' . $batonRoleExchangeTitle . '"
				   }
				 }';

				// certificate
				$push_cert = WWW_ROOT.'/voipssl/'.PUSH_CERTIFICATE_IOS_V2;
				$push_cert_key = WWW_ROOT.'/voipssl/'.PUSH_CERTIFICATE_KEY_V2;

				// headers
				$headers = array(
				    "apns-topic: {$app_bundle_id}",
				    "User-Agent: My Sender",
				);

				// other curl options
				curl_setopt_array($http2ch, array(
				    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_2_0,
				    CURLOPT_URL => $url,
				    CURLOPT_PORT => 443,
				    CURLOPT_HTTPHEADER => $headers,
				    CURLOPT_POST => TRUE,
				    CURLOPT_POSTFIELDS => $message,
				    CURLOPT_RETURNTRANSFER => TRUE,
				    CURLOPT_TIMEOUT => 30,
				    CURLOPT_SSL_VERIFYPEER => false,
				    CURLOPT_SSLCERT => $push_cert,
				    CURLOPT_SSLKEY => $push_cert_key,
				    CURLOPT_HEADER => 1
				));

				// go...
				$result = curl_exec($http2ch);
				if ($result === FALSE) {
				  throw new Exception("Curl failed: " .  curl_error($http2ch));
				}

				// echo 'RESULT ==> ' . $result;

				// get response
				$status = curl_getinfo($http2ch, CURLINFO_HTTP_CODE);

				// echo '\n\nSTATUS ==> ' . $status;
				return $status;

				
			}
		}

		public function roleExchangePushNotificationOnAndroidv2($params = array())
		{
			if(!empty($params['device_token'])){
				$var = "";
				$requestTypeVal = 0;
				if($params['request_type'] == 'transfer')
				{
					// $messageVal = $params['first_name']." ".$params['last_name'] ." Transfer role to you.";
					$messageVal = ROLEEX_001.$params['first_name']." ".$params['last_name'].".";
					if($params['to_user'] != 0){
						$custMessage = $params['first_name']." ".$params['last_name']." Transfer " .$params['role_name']. " role to ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
					else{
						$custMessage = $params['first_name']." ".$params['last_name']." Transfer ".$params['role_name']." to Switch Borad ";
					}
				}
				elseif ($params['request_type'] == 'transfer_accept') {
					$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_004;
					$var = "Accepted";
					if($params['to_user'] != 0){
						$custMessage = "Transfer accepted by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
					else{
						$custMessage = "Transfer accepted by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
				}
				elseif ($params['request_type'] == 'transfer_rejected') {
					$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_005;
					$var = "Rejected";
					$custMessage = "Transfer rejected by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				elseif ($params['request_type'] == 'request') {
					$messageVal = ROLEEX_001.$params['first_name']." ".$params['last_name'];
					$requestTypeVal = $params['push_on'];
					if($params['to_user'] != 0){
						$custMessage = $params['first_name']." ".$params['last_name']." requested ".$params['role_name']." from ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
					else{
						$custMessage = $params['first_name']." ".$params['last_name']." requested ".$params['role_name']." from ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
				}
				elseif ($params['request_type'] == 'request_accept') {
					$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_002;
					$var = "Accepted";
					$requestTypeVal = $params['push_on'];
					if($params['to_user'] != 0){
						$custMessage = "Request for role ".$params['role_name']." accepted by ".$params['first_name']." ".$params['last_name']." sent form ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
					else{
						$custMessage = "Request for role ".$params['role_name']." accepted by ".$params['first_name']." ".$params['last_name']." sent form ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
				}
				elseif ($params['request_type'] == 'request_reject') {
					$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_003;
					$var = "Rejected";
					if($params['to_user'] != 0){
						$custMessage = "Request for role ".$params['role_name']." rejected by ".$params['first_name']." ".$params['last_name']." sent form ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
					else{
						$custMessage = "Request for role ".$params['role_name']." rejected by ".$params['first_name']." ".$params['last_name']." sent form ".$params['to_user_first_name']." ".$params['to_user_last_name'];
					}
				}

				elseif ($params['request_type'] == 'revoke_request') {
					$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_006;
					$var = "Revoked";
					$custMessage = "Request revoked by ".$params['first_name']." ".$params['last_name']." Sent to ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				elseif ($params['request_type'] == 'revoke_transfer') {
					$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_006;
					$var = "Revoked";
					$custMessage = "Transfer revoked by ".$params['first_name']." ".$params['last_name']." Sent to ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				elseif ($params['request_type'] == 'timeout') {
					$var = "Timeout";
					$messageVal = ROLEEX_007;
				}
				elseif ($params['request_type'] == 'swichborad_accept') {
					$var = "Accepted";
					$messageVal = ROLEEX_008;
					$requestTypeVal = $params['push_on'];
					$custMessage = "Request accepted by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." requested by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				elseif ($params['request_type'] == 'swichborad_rejected') {
					$var = "Rejected";
					$messageVal = ROLEEX_009;
					$custMessage = "Request rejected by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." requested by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				//TO do this will be removed when auto accept functionality goes live
				elseif ($params['request_type'] == 'swichborad_transfered') {
					$var = "Requested";
					$messageVal = ROLEEX_019;
					$custMessage = "Switch Borad transfer ".$params['role_name']." to ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				// elseif ($params['request_type'] == 'swichborad_added') {
				elseif ($params['request_type'] == 'swichborad_assigned') {
					$var = "Assigned";
					$messageVal = ROLEEX_011;
					$requestTypeVal = $params['push_on'];
					$custMessage = "Switch Borad transfer ".$params['role_name']." to ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				elseif ($params['request_type'] == 'swichborad_removed') {
					$var = "Rejected";
					$messageVal = ROLEEX_012;
				}elseif ($params['request_type'] == 'swichborad_transfer_accept') {
					$var = "Accepted";
					$messageVal = ROLEEX_013;
					$custMessage = "Transfer accepted by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				elseif ($params['request_type'] == 'swichborad_transfer_rejected') {
					$var = "Rejected";
					$messageVal = ROLEEX_014;
					$custMessage = "Transfer rejected by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				elseif ($params['request_type'] == 'swichborad_unassign') {
					$var = "Unassigned";
					$messageVal = ROLEEX_017;
				}
				elseif ($params['request_type'] == 'swichborad_deleted') {
					$var = "Deleted";
					$messageVal = ROLEEX_016;
				}
				elseif ($params['request_type'] == 'qr_transfer') {
					$messageVal = "Transfer accepted by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered  through qr code by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				}
				// $transactionParams['request_type'] = $params['request_type'];
				// $transactionParams['from_user'] = $params['from_user_id'];
				// $transactionParams['to_user'] = $params['to_user'];
				// $transactionParams['role_id'] = $params['role_id'];
				// $transactionParams['is_qr_request'] = $params['is_qr_request'];
				// $transactionParams['custom_message'] = $custMessage;

				// $saveRoleExchangeTransactions = $this->UserBatonRoleTransaction->saveRoleExchangeTransactions($transactionParams);


				$sound_file = "silent";
				$channel_id = '';
				if($params['is_dnd_active'] == "1" || $params['is_user_available'] == "0")
				{
					$sound_file = "silent";
					$channel_id = "Medic Bleep Baton Role Silent";
				}
				else if($params['is_dnd_active'] == "0" || $params['is_user_available'] == "1")
				{
					$sound_file = "notification_sound";
					$channel_id = "Medic Bleep Baton Role";
				}
				$dataUserDevice = isset($params['device_token']) ? $params['device_token']:"";
				$tokenRegistrationIds = array($dataUserDevice);
				$voipPushMessage = array
						(
							'role_type'=>2,
							'type_name'=>'PushRoleExchange',
							'message'=>$messageVal,
							'title'=> "Baton Role ".$var,
							'role_id'=>$params['role_id'],
							'sound'=>$sound_file,
	                        'is_qr_request'=>$params['is_qr_request'],
	                        'channel_id'=>$channel_id
						);
				$postFeilds = array
				(
					'registration_ids' 	=> $tokenRegistrationIds,
					'data'			=> $voipPushMessage,
					'priority' => 'high'
				);
				$headers = array
				(
					'Authorization: key=' . GCM_ACCESS_KEY,
					'Content-Type: application/json'
				);
				// echo "<pre>";print_r(GCM_ACCESS_KEY);exit();
				$ch = curl_init();
				curl_setopt( $ch,CURLOPT_URL, PUSH_END_POINT_ANDROID );
				curl_setopt( $ch,CURLOPT_POST, true );
				curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
				curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
				curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $postFeilds ) );
				$result = curl_exec($ch );
				curl_close( $ch );
				return $var = 1;
			}
			else
			{
				return $var = 0;
			}

		}



		/*
		--------------------------------------------------------------------------
		ON: 03-02-2020
		I/P:
		O/P: True or False
		Desc: This function will send a push notification to multiple login event
		--------------------------------------------------------------------------
		*/

		public function sendRoleExchangePushNotificationWebv2($params)
		{
			if(!empty($params))
			{
				if($params['from_user_id'] == 0)
				{
					$userData['UserProfile']['first_name'] = "Switch";
					$userData['UserProfile']['last_name'] = "Borad";
				}
				else
				{
					$userData = $this->User->find('first', array('conditions'=> array("User.id"=> $params['from_user_id'])));
				}
				if($params['user_id'] == 0)
				{
					$userDataTo['UserProfile']['first_name'] = "Switch";
					$userDataTo['UserProfile']['last_name'] = "Borad";
				}
				else
				{
					$userDataTo = $this->User->find('first', array('conditions'=> array("User.id"=> $params['user_id'])));
				}
				if(!empty($params))
				{
					$param['request_type'] = $params['request_type'];
					$param['first_name'] = $userData['UserProfile']['first_name'];
					$param['last_name'] = $userData['UserProfile']['last_name'];
					$param['is_qr_request'] = isset($params['is_qr_request']) ? $params['is_qr_request'] : 0;
					$param['from_user_id'] = $params['from_user_id'];
					$param['to_user'] = $params['user_id'];
					$param['role_id'] = $params['role_id'];
					$customMessage = $this->roleExchangePushNotificationOnWebv2($param);
					return $customMessage;
				}
			}
		}

		public function roleExchangePushNotificationOnWebv2($params = array())
		{
			if(!empty($params))
			{
				$var = "";
				$requestTypeVal = 0;
				if($params['request_type'] == 'transfer')
				{
					// $messageVal = $params['first_name']." ".$params['last_name'] ." Transfer role to you.";
					$messageVal = ROLEEX_001.$params['first_name']." ".$params['last_name'].".";
				}
				elseif ($params['request_type'] == 'transfer_accept') {
					$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_004;
				}
				elseif ($params['request_type'] == 'transfer_rejected') {
					$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_005;
					$var = "Rejected";
				}
				elseif ($params['request_type'] == 'request') {
					$messageVal = ROLEEX_001.$params['first_name']." ".$params['last_name'];
					$var = "Request";
				}
				elseif ($params['request_type'] == 'request_accept') {
					$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_002;
					$var = "Accepted";
				}
				elseif ($params['request_type'] == 'request_reject') {
					$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_003;
					$var = "Rejected";
				}
				elseif ($params['request_type'] == 'revoke_request') {
					$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_006;
					$var = "Revoked";
				}
				elseif ($params['request_type'] == 'revoke_transfer') {
					$messageVal = $params['first_name']." ".$params['last_name'].ROLEEX_006;
					$var = "Revoked";
				}
				elseif ($params['request_type'] == 'timeout') {
					$var = "Timeout";
					$messageVal = ROLEEX_007;
				}
				elseif ($params['request_type'] == 'swichborad_accept') {
					$var = "Accepted";
					$messageVal = ROLEEX_008;
				}
				elseif ($params['request_type'] == 'swichborad_rejected') {
					$var = "Rejected";
					$messageVal = ROLEEX_009;
				}
				//TO do this will be removed when auto accept functionality goes live
				elseif ($params['request_type'] == 'swichborad_transfered') {
					$var = "Requested";
					$messageVal = ROLEEX_019;
				}
				// elseif ($params['request_type'] == 'swichborad_added') {
				elseif ($params['request_type'] == 'swichborad_assigned') {
					$var = "Assigned";
					$messageVal = ROLEEX_011;
				}
				elseif ($params['request_type'] == 'swichborad_removed') {
					$var = "Rejected";
					$messageVal = ROLEEX_012;
				}
				elseif ($params['request_type'] == 'swichborad_transfer_accept') {
					$var = "Accepted";
					$messageVal = ROLEEX_013;
				}
				elseif ($params['request_type'] == 'swichborad_transfer_rejected') {
					$var = "Rejected";
					$messageVal = ROLEEX_014;

				}
				elseif ($params['request_type'] == 'swichborad_unassign') {
					$var = "Unassigned";
					$messageVal = ROLEEX_017;
				}
				elseif ($params['request_type'] == 'swichborad_deleted') {
					$var = "Deleted";
					$messageVal = ROLEEX_016;
				}
				// elseif ($params['request_type'] == 'qr_transfer') {
				// 	$messageVal = "Transfer accepted by ".$params['first_name']." ".$params['last_name']." for role ".$params['role_name']." transfered  through qr code by ".$params['to_user_first_name']." ".$params['to_user_last_name'];
				// }

				return $messageVal;
				
			}
		}


		/*
		----------------------------------------------------------------------------------------------
		On: 12-06-2019
		I/P: user id's(from and to)
		O/P: Push Success or Failure
		Desc: Permanent Role Exchange push notification
		----------------------------------------------------------------------------------------------
		*/

		public function sendPermanentRoleExchangePushNotificationv2($params)
		{
			if(!empty($params))
			{
				$sendRoleExchangePushNotification = $this->VoipPushNotification->find('first', array('conditions' => array('VoipPushNotification.user_id' => $params['other_user_id'], 'status'=>'1')));
				$userData = $this->User->find('first', array('conditions'=> array("User.id"=> $params['user_id'])));

				if(!empty($sendRoleExchangePushNotification))
				{
					$param['device_token'] = $sendRoleExchangePushNotification['VoipPushNotification']['device_token'];
					$param['device_type'] = $sendRoleExchangePushNotification['VoipPushNotification']['device_type'];
					$param['request_type'] = $params['request_type'];
					$param['first_name'] = $userData['UserProfile']['first_name'];
					$param['last_name'] = $userData['UserProfile']['last_name'];

					$param['is_keep_bleeping'] = $params["is_keep_bleeping"];
					$param['volume'] = $params["volume"];
					$param['is_user_available'] = $params["is_user_available"];
					$param['is_dnd_active'] = $params["is_dnd_active"];

					if($param['device_type'] == 'ios' || $param['device_type'] == 'iOS')
					{
						$sendNotification = $this->permanentRoleExchangePushNotificationOnIosv2($param);
					}
					else if($param['device_type'] == 'android' || $param['device_type'] == 'Android')
					{
						$sendNotification = $this->permanentroleExchangePushNotificationOnAndroidv2($param);
					}
				}
			}
		}

		public function permanentRoleExchangePushNotificationOnIosv2($params = array())
		{
			if(!empty($params))
			{
				$var = "";
				if($params['request_type'] == 'transfer')
				{
					// $messageVal = $params['first_name']." ".$params['last_name'] ." Transfer role to you.";
					$messageVal = PROLEEX_001.$params['first_name']." ".$params['last_name'].".";
				}
				elseif ($params['request_type'] == 'transfer_accept') {
					$messageVal = $params['first_name']." ".$params['last_name'].PROLEEX_002;
					$var = "Accepted";
				}
				elseif ($params['request_type'] == 'transfer_rejected') {
					$messageVal = $params['first_name']." ".$params['last_name'].PROLEEX_003;
					$var = "Rejected";
				}
				elseif ($params['request_type'] == 'revoke_transfer') {
					$messageVal = $params['first_name']." ".$params['last_name'].PROLEEX_004;
					$var = "Revoked";
				}
				elseif ($params['request_type'] == 'timeout') {
					$var = "Timeout";
					$messageVal = PROLEEX_007;
				}
				$http2_server = PUSH_END_POINT_IOS_V2;
				$device_token = $params['device_token'];
				$url = "{$http2_server}/3/device/{$device_token}";
				$app_bundle_id = APP_BUNDLE_ID_V2;
				$http2ch = curl_init();
				$type = 1;
				$sound_file = '"silent.wav"';
				$permanentRoleExchangeTitle = "Permanent Role ".$var;
				$permanentRoleExchangeTitle =$permanentRoleExchangeTitle;
				if($params['is_dnd_active'] == "1" || $params['is_user_available'] == "0")
				{
					$sound_file = '"silent.wav"';
				}
				else if($params['is_dnd_active'] == "0" || $params['is_user_available'] == "1")
				{
					if($params['is_keep_bleeping'] == "1")
					{
						$volume = $params['volume'];
						$sound_file = '{"critical":1,
	       								"name":"notification_sound.mp3",
	       								"volume":"' . $volume . '"}';
					}
					else{
						$sound_file = '"notification_sound.mp3"';
					}
				}
				$message = '{
				   "aps":{
				     "alert":"' . $messageVal . '",
				     "priority":10,
				     "sound":' . $sound_file . ',
				     "mutable-content":1
				   },
				   "data":{
				     "type_name":"PushRoleExchange",
				     "message":"' . $messageVal . '",
				     "role_type": "1",
				     "title":"' . $permanentRoleExchangeTitle . '"
				   }
				 }';

				// certificate
				$push_cert = WWW_ROOT.'/voipssl/'.PUSH_CERTIFICATE_IOS_V2;
				$push_cert_key = WWW_ROOT.'/voipssl/'.PUSH_CERTIFICATE_KEY_V2;

				// headers
				$headers = array(
				    "apns-topic: {$app_bundle_id}",
				    "User-Agent: My Sender",
				);

				// other curl options
				curl_setopt_array($http2ch, array(
				    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_2_0,
				    CURLOPT_URL => $url,
				    CURLOPT_PORT => 443,
				    CURLOPT_HTTPHEADER => $headers,
				    CURLOPT_POST => TRUE,
				    CURLOPT_POSTFIELDS => $message,
				    CURLOPT_RETURNTRANSFER => TRUE,
				    CURLOPT_TIMEOUT => 30,
				    CURLOPT_SSL_VERIFYPEER => false,
				    CURLOPT_SSLCERT => $push_cert,
				    CURLOPT_SSLKEY => $push_cert_key,
				    CURLOPT_HEADER => 1
				));

				// go...
				$result = curl_exec($http2ch);
				if ($result === FALSE) {
				  throw new Exception("Curl failed: " .  curl_error($http2ch));
				}

				// echo 'RESULT ==> ' . $result;

				// get response
				$status = curl_getinfo($http2ch, CURLINFO_HTTP_CODE);

				// echo '\n\nSTATUS ==> ' . $status;
				return $status;
			}
		}

		public function permanentroleExchangePushNotificationOnAndroidv2($params = array())
		{
			if(!empty($params['device_token'])) {
				$var = "";
				if($params['request_type'] == 'transfer')
				{
					// $messageVal = $params['first_name']." ".$params['last_name'] ." Transfer role to you.";
					$messageVal = PROLEEX_001.$params['first_name']." ".$params['last_name'].".";
				}
				elseif ($params['request_type'] == 'transfer_accept') {
					$messageVal = $params['first_name']." ".$params['last_name'].PROLEEX_002;
					$var = "Accepted";
				}
				elseif ($params['request_type'] == 'transfer_rejected') {
					$messageVal = $params['first_name']." ".$params['last_name'].PROLEEX_003;
					$var = "Rejected";
				}
				elseif ($params['request_type'] == 'revoke_transfer') {
					$messageVal = $params['first_name']." ".$params['last_name'].PROLEEX_004;
					$var = "Revoked";
				}
				elseif ($params['request_type'] == 'timeout') {
					$var = "Timeout";
					$messageVal = PROLEEX_007;
				}
				$dataUserDevice = isset($params['device_token']) ? $params['device_token']:"";
				$tokenRegistrationIds = array($dataUserDevice);
				$sound_file = "silent";
				$channel_id = '';
				if($params['is_dnd_active'] == "1" || $params['is_user_available'] == "0")
				{
					$sound_file = "silent";
					$channel_id = "Medic Bleep Permanent Role Silent";
				}
				else if($params['is_dnd_active'] == "0" || $params['is_user_available'] == "1")
				{
					$sound_file = "notification_sound";
					$channel_id = "Medic Bleep Permanent Role";
				}
				$voipPushMessage = array
						(
							'role_type'=>1,
							'message'=>$messageVal,
							'type_name'=>"PushRoleExchange",
							'title'=> "Permanent Role".$var,
							'sound'=>$sound_file,
							'channel_id'=>$channel_id
						);
				$postFeilds = array
				(
					'registration_ids' 	=> $tokenRegistrationIds,
					'data'			=> $voipPushMessage,
					'priority' => 'high'
				);
				$headers = array
				(
					'Authorization: key=' . GCM_ACCESS_KEY,
					'Content-Type: application/json'
				);
				// echo "<pre>";print_r(GCM_ACCESS_KEY);exit();
				$ch = curl_init();
				curl_setopt( $ch,CURLOPT_URL, PUSH_END_POINT_ANDROID );
				curl_setopt( $ch,CURLOPT_POST, true );
				curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
				curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
				curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $postFeilds ) );
				$result = curl_exec($ch );
				curl_close( $ch );
				return $var = 1;
			}
			else
			{
				return $var = 0;
			}

		}


		/*
		----------------------------------------------------------------------------------------------
		On: 03-04-2019
		I/P: user id's(from and to)
		O/P: Push Success or Failure
		Desc: Permanent Role Exchange push notification for web
		----------------------------------------------------------------------------------------------
		*/

		public function sendPermanentRoleExchangePushNotificationWebv2($params)
		{
			if(!empty($params))
			{
				$userData = $this->User->find('first', array('conditions'=> array("User.id"=> $params['user_id'])));

				if(!empty($userData))
				{
					$param['request_type'] = $params['request_type'];
					$param['first_name'] = $userData['UserProfile']['first_name'];
					$param['last_name'] = $userData['UserProfile']['last_name'];

					$param['is_keep_bleeping'] = $params["is_keep_bleeping"];
					$param['volume'] = $params["volume"];
					$param['is_user_available'] = $params["is_user_available"];
					$param['is_dnd_active'] = $params["is_dnd_active"];

					$sendNotification = $this->permanentRoleExchangePushNotificationOnWebv2($param);
					return $sendNotification;
				}
			}
		}

		public function permanentRoleExchangePushNotificationOnWebv2($params = array())
		{
			if(!empty($params))
			{
				$var = "";
				if($params['request_type'] == 'transfer')
				{
					// $messageVal = $params['first_name']." ".$params['last_name'] ." Transfer role to you.";
					$messageVal = PROLEEX_001.$params['first_name']." ".$params['last_name'].".";
				}
				elseif ($params['request_type'] == 'transfer_accept') {
					$messageVal = $params['first_name']." ".$params['last_name'].PROLEEX_002;
					$var = "Accepted";
				}
				elseif ($params['request_type'] == 'transfer_rejected') {
					$messageVal = $params['first_name']." ".$params['last_name'].PROLEEX_003;
					$var = "Rejected";
				}
				elseif ($params['request_type'] == 'revoke_transfer') {
					$messageVal = $params['first_name']." ".$params['last_name'].PROLEEX_004;
					$var = "Revoked";
				}
				elseif ($params['request_type'] == 'timeout') {
					$var = "Timeout";
					$messageVal = PROLEEX_007;
				}
				return $messageVal;
			}
		}

		/*
		--------------------------------------------------------------------------
		ON: 05-03-2020
		I/P:
		O/P: True or False
		Desc: This function will send a push notification when user is unapproved from trust panel
		--------------------------------------------------------------------------
		*/

		public function sendLogoutPushOnDeviceOnUnapproved($params =array())
		{
			if(!empty($params)) {
				$userVoipData = $this->VoipPushNotification->find('all', array('conditions' => array('VoipPushNotification.user_id' => $params['user_id'], 'status'=>1)));
				if(!empty($userVoipData))
				{
					$response = 0;
					foreach ($userVoipData as $key => $value) {
						$params['device_token'] = $value['VoipPushNotification']['device_token'];
						$params['user_id'] = $params['user_id'];
						$params['device_type'] =  $value['VoipPushNotification']['device_type'];
						if($params['device_type'] == "iOS" || $params['device_type'] == "ios")
						{
							$sendNotification = $this->sendLogoutPushOnIosFromTrustPanelv2($params);
							if($sendNotification == 200)
							{
								$deregisterData = array("user_id"=> $params['user_id']);
								$this->VoipPushNotification->updateAll( array("status"=> 0), $deregisterData );
							}
							$response = "success";
						}
						else if($params['device_type'] == "Android" || $params['device_type'] == "android"){

							$sendNotificationAndroid = $this->sendLogoutPushOnAndroidFromTrustPanelv2($params);
							if($sendNotificationAndroid == 1)
							{
								$deregisterData = array("user_id"=>$params['user_id']);
								$this->VoipPushNotification->updateAll( array("status"=> 0), $deregisterData );
							}
							$response = "success";
						}
						$updateUserCurrentStatus = $this->updateUserCurrentRoleStatusv2($params['user_id'], $params['device_type']);
					}
					return $response;
				}

			}else{
				return "Data is empty";
			}
		}


		public function sendLogoutPushOnIosFromTrustPanelv2($params = array())
		{
			if(!empty($params))
			{

				$http2_server = PUSH_END_POINT_IOS_V2;
				$device_token = $params['device_token'];
				$url = "{$http2_server}/3/device/{$device_token}";
				$app_bundle_id = APP_BUNDLE_ID_V2;
				$http2ch = curl_init();
				$type = 1;
				$message = '{
				   "aps":{
				     "alert":"Your subscription has expired. Thank you for using Medic Bleep!.",
				     "priority":10,
				     "sound":{
				       "critical":1,
				       "name":"notification_sound.mp3",
				       "volume":0.1
				     },
				     "mutable-content":1
				   },
				   "data":{
				     "type_name":"UserUnApproved",
				     "message":"Your subscription has expired. Thank you for using Medic Bleep!.",
				     "title":"User UnApproved"
				   }
				 }';

				// certificate
				$push_cert = WWW_ROOT.'/voipssl/'.PUSH_CERTIFICATE_IOS_V2;
				$push_cert_key = WWW_ROOT.'/voipssl/'.PUSH_CERTIFICATE_KEY_V2;

				// headers
				$headers = array(
				    "apns-topic: {$app_bundle_id}",
				    "User-Agent: My Sender",
				);

				// other curl options
				curl_setopt_array($http2ch, array(
				    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_2_0,
				    CURLOPT_URL => $url,
				    CURLOPT_PORT => 443,
				    CURLOPT_HTTPHEADER => $headers,
				    CURLOPT_POST => TRUE,
				    CURLOPT_POSTFIELDS => $message,
				    CURLOPT_RETURNTRANSFER => TRUE,
				    CURLOPT_TIMEOUT => 30,
				    CURLOPT_SSL_VERIFYPEER => false,
				    CURLOPT_SSLCERT => $push_cert,
				    CURLOPT_SSLKEY => $push_cert_key,
				    CURLOPT_HEADER => 1
				));

				// go...
				$result = curl_exec($http2ch);
				if ($result === FALSE) {
				  throw new Exception("Curl failed: " .  curl_error($http2ch));
				}

				$result;

				// get response
				$status = curl_getinfo($http2ch, CURLINFO_HTTP_CODE);

				$status;
				return $status;
			}
		}


		public function sendLogoutPushOnAndroidFromTrustPanelv2($params = array())
		{
			if(!empty($params['device_token'])) {
				$dataUserDevice = isset($params['device_token']) ? $params['device_token']:"";
				$tokenRegistrationIds = array($dataUserDevice);
				$userId = isset($params['user_id']) ? $params['user_id'] : "";
				$sound_file = "notification_sound";
				$voipPushMessage = array
						(
							'message'=>ERROR_665,
							'user_id'=>$userId,
							'type_name'=>'UserUnApproved',
							'sound'=>$sound_file,
							'channel_id'=>"Medic Bleep User UnApproved"
						);
				$postFeilds = array
				(
					'registration_ids' 	=> $tokenRegistrationIds,
					'data'			=> $voipPushMessage,
					'priority' => 'high'
				);
				$headers = array
				(
					'Authorization: key=' . GCM_ACCESS_KEY,
					'Content-Type: application/json'
				);
				// echo "<pre>";print_r(GCM_ACCESS_KEY);exit();
				$ch = curl_init();
				curl_setopt( $ch,CURLOPT_URL, PUSH_END_POINT_ANDROID );
				curl_setopt( $ch,CURLOPT_POST, true );
				curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
				curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
				curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $postFeilds ) );
				$result = curl_exec($ch );
				curl_close( $ch );
				return $result;
			}
			else
			{
				return $result;
			}

		}


	/*
	--------------------------------------------------------------------------
	ON: 19-03-2020
	I/P:
	O/P: True or False
	Desc: This function will send a push notification when user is Deactivated from superadmin
	--------------------------------------------------------------------------
	*/

	public function sendLogoutPushOnDeviceOnDeactivated($params =array())
	{
		if(!empty($params)) {
			$userVoipData = $this->VoipPushNotification->find('all', array('conditions' => array('VoipPushNotification.user_id' => $params['user_id'], 'status'=>1)));
			if(!empty($userVoipData))
			{
				$response = 0;
				foreach ($userVoipData as $key => $value) {
					$params['device_token'] = $value['VoipPushNotification']['device_token'];
					$params['user_id'] = $params['user_id'];
					$params['device_type'] =  $value['VoipPushNotification']['device_type'];
					if($params['device_type'] == "iOS" || $params['device_type'] == "ios")
					{
						$sendNotification = $this->sendLogoutPushOnIosFromSuperadminv2($params);
						if($sendNotification == 200)
						{
							$deregisterData = array("user_id"=> $params['user_id']);
							$this->VoipPushNotification->updateAll( array("status"=> 0), $deregisterData );
						}
						$response = "success";
					}
					else if($params['device_type'] == "Android" || $params['device_type'] == "android"){

						$sendNotificationAndroid = $this->sendLogoutPushOnAndroidFromSuperadminv2($params);
						if($sendNotificationAndroid == 1)
						{
							$deregisterData = array("user_id"=>$params['user_id']);
							$this->VoipPushNotification->updateAll( array("status"=> 0), $deregisterData );
						}
						$response = "success";
					}
					$updateUserCurrentStatus = $this->updateUserCurrentRoleStatusv2($params['user_id'], $params['device_type']);
				}
				return $response;
			}

		}else{
			return "Data is empty";
		}
	}

	public function sendLogoutPushOnIosFromSuperadminv2($params = array())
		{
			if(!empty($params))
			{

				$http2_server = PUSH_END_POINT_IOS_V2;
				$device_token = $params['device_token'];
				$url = "{$http2_server}/3/device/{$device_token}";
				$app_bundle_id = APP_BUNDLE_ID_V2;
				$http2ch = curl_init();
				$type = 1;
				$message = '{
				   "aps":{
				     "alert":"Your account is no more active. Please contact support@medicbleep.com for any further assistance.",
				     "priority":10,
				     "sound":{
				       "critical":1,
				       "name":"notification_sound.mp3",
				       "volume":0.1
				     },
				     "mutable-content":1
				   },
				   "data":{
				     "type_name":"UserUnApproved",
				     "message":"Your account is no more active. Please contact support@medicbleep.com for any further assistance.",
				     "title":"User Deactivated"
				   }
				 }';

				// certificate
				$push_cert = WWW_ROOT.'/voipssl/'.PUSH_CERTIFICATE_IOS_V2;
				$push_cert_key = WWW_ROOT.'/voipssl/'.PUSH_CERTIFICATE_KEY_V2;

				// headers
				$headers = array(
				    "apns-topic: {$app_bundle_id}",
				    "User-Agent: My Sender",
				);

				// other curl options
				curl_setopt_array($http2ch, array(
				    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_2_0,
				    CURLOPT_URL => $url,
				    CURLOPT_PORT => 443,
				    CURLOPT_HTTPHEADER => $headers,
				    CURLOPT_POST => TRUE,
				    CURLOPT_POSTFIELDS => $message,
				    CURLOPT_RETURNTRANSFER => TRUE,
				    CURLOPT_TIMEOUT => 30,
				    CURLOPT_SSL_VERIFYPEER => false,
				    CURLOPT_SSLCERT => $push_cert,
				    CURLOPT_SSLKEY => $push_cert_key,
				    CURLOPT_HEADER => 1
				));

				// go...
				$result = curl_exec($http2ch);
				if ($result === FALSE) {
				  throw new Exception("Curl failed: " .  curl_error($http2ch));
				}

				$result;

				// get response
				$status = curl_getinfo($http2ch, CURLINFO_HTTP_CODE);

				$status;
				return $status;
			}
		}

		public function sendLogoutPushOnAndroidFromSuperadminv2($params = array())
		{
			if(!empty($params['device_token'])) {
				$dataUserDevice = isset($params['device_token']) ? $params['device_token']:"";
				$tokenRegistrationIds = array($dataUserDevice);
				$userId = isset($params['user_id']) ? $params['user_id'] : "";
				$sound_file = "notification_sound";
				$voipPushMessage = array
						(
							'message'=>ERROR_651,
							'user_id'=>$userId,
							'type_name'=>'UserDeactivated',
							'sound'=>$sound_file,
							'channel_id'=>"Medic Bleep User Deactivated"
						);
				$postFeilds = array
				(
					'registration_ids' 	=> $tokenRegistrationIds,
					'data'			=> $voipPushMessage,
					'priority' => 'high'
				);
				$headers = array
				(
					'Authorization: key=' . GCM_ACCESS_KEY,
					'Content-Type: application/json'
				);
				// echo "<pre>";print_r(GCM_ACCESS_KEY);exit();
				$ch = curl_init();
				curl_setopt( $ch,CURLOPT_URL, PUSH_END_POINT_ANDROID );
				curl_setopt( $ch,CURLOPT_POST, true );
				curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
				curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
				curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $postFeilds ) );
				$result = curl_exec($ch );
				curl_close( $ch );
				return $result;
			}
			else
			{
				return $result;
			}

		}

	/*
	------------------------------------------------------------------------------
	ON: 09-01-2020
	I/P: JSON
	O/P: JSON
	Desc:
	------------------------------------------------------------------------------
	*/

	public function updateUserCurrentRoleStatusv2($user_id, $device_type)
	{
		if(!empty($user_id))
		{
			$conditions = array(
				'User.id' => $user_id
			);
			$joins = array(
					array(
						'table' => 'user_duty_logs',
						'alias' => 'UserDutyLog',
						'type' => 'left',
						'conditions'=> array('User.id = UserDutyLog.user_id')
					),
					array(
						'table' => 'user_dnd_statuses',
						'alias' => 'UserDndStatus',
						'type' => 'left',
						'conditions'=> array('User.id = UserDndStatus.user_id')
					)
				);
			$options = array(
					'joins' => $joins,
					'conditions' => $conditions,
					'fields' => array('User.email','User.id','UserDutyLog.status', 'UserDutyLog.atwork_status', 'UserDutyLog.hospital_id','UserDndStatus.is_active', 'UserDndStatus.duration','UserDndStatus.dnd_status_id'),
				);
			$userData = $this->User->find('first',$options);

		    $params['email'] = $userData['User']['email'];
		    $params['user_id'] = $userData['User']['id'];
		    $params['is_dnd_active'] =  $userData['UserDndStatus']['is_active'];
		    $params['duration'] =  $userData['UserDndStatus']['duration'];
		    $params['is_oncall'] =  $userData['UserDutyLog']['status'];
		    $params['is_available'] =  $userData['UserDutyLog']['atwork_status'];
		    $btn_userid = $params['user_id'];
		    $params['is_cache_modified'] = 0;
		    $params['company_id'] = $userData['UserDutyLog']['hospital_id'];
		    $params['dnd_status_id'] = $userData['UserDndStatus']['dnd_status_id'];

		    // Code to update oncall, available,dnd and baton role
		    $updateUserCurrentAvailablityStatus = $this->updateUserCurrentAvailablityStatus($params);
		   //  $params['is_cache_modified'] = $updateUserCurrentAvailablityStatus;
		   //  $getUserBatonRoleDetail = $this->UserBatonRole->getUserBatonRoleDetail($btn_userid);
		   //  if(!empty($getUserBatonRoleDetail))
		   //  {
		   //  	foreach ($getUserBatonRoleDetail as $value) {
		    		
				 //    try{
					//     $params['role_id'] = $value['UserBatonRole']['role_id'];
					//     $params['from_user'] = $value['UserBatonRole']['from_user'];
					//     $params['to_user'] = $value['UserBatonRole']['to_user'];
					//     $removeUserBatonRole = $this->removeUserBatonRole($params);
				 //    }catch( Exception $e ){
					// 	$responseData = array('method_name'=> 'updateUserCurrentRoleStatusv2', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615);
					// }
				 //    $params['is_cache_modified'] = $removeUserBatonRole;
		   //  	}
		   //  }
		 //    $updateCacheOnLogout = $this->updateCacheOnLogout($params);

		 //    $deviceType =  $device_type;
			// $transactionParams['user_id'] = $params['user_id'];
			// $transactionParams['company_id'] = $params['company_id'];
			// $transactionParams['type'] = "Available";
			// $transactionParams['status'] = "0";
			// $transactionParams['device_type'] = $deviceType;
			// $transactionParams['platform'] = "app";
			// // $saveAvailableTransaction = $this->saveAvailableAndOncallTransactions($transactionParams);


			// $transactionParams['user_id'] = $params['user_id'];
			// $transactionParams['company_id'] = $params['company_id'];
			// $transactionParams['type'] = "OnCall";
			// $transactionParams['status'] = "0";
			// $transactionParams['device_type'] = $deviceType;
			// $transactionParams['platform'] = "app";
			// $saveAvailableTransaction = $this->saveAvailableAndOncallTransactions($transactionParams);

			//Inactive OCR token
			$this->UserDevice->updateAll(array('status'=>0), array('user_id'=> $user_id));
		}
		return true;
	}

	public function updateUserCurrentAvailablityStatus($params = array())
	{
		// return $params;
		//** One time token to validate for QB[START]
		$returnResponse = 0;
		$isCacheModified = 0;
		
		if(!empty($params )){
			$isCacheModified = 1;
			$this->UserDutyLog->updateAll(array("atwork_status"=> "0", "status"=> "0"), array("user_id"=> $params['user_id']));
			$returnResponse = 0;
		}

		//Dismiss Dnd
		// $params['is_dnd_active'] = 0;
		// $startTime = time();
		// $duration = $params['duration'];
		// $endTime = $startTime + $duration*60;
		// $params['end_time'] = ($params['is_dnd_active'] == 1) ? $endTime : "";
		// $params['dnd_name'] = ($params['is_dnd_active'] == 1) ? $params['dnd_name'] : "";
		// $params['dnd_status_id'] = ($params['dnd_status_id'] == 1) ? $params['dnd_status_id'] : "";
		// $params['icon_selected'] = ($params['is_dnd_active'] == 1) ? $params['icon_selected'] : "";
		// $params['note'] = "";
		// $params['other_user_id'] = 0;

		// $updateUserDndStatus = $this->UserDndStatus->updateAll(array("dnd_status_id"=> "'" . $params['dnd_status_id'] . "'", "start_time"=> "'" . $startTime . "'", "end_time"=>"'" . $endTime . "'", "duration"=>"'" . $duration . "'", "other_user_id"=>$params['other_user_id'], "note"=>'"'.$params['note'].'"', "is_active"=>$params['is_dnd_active']), array('UserDndStatus.user_id'=>$params['user_id']));

		return $isCacheModified;
	}

	public function removeUserBatonRole($params = array())
	{
		$isCacheModified = 0;
		if(!empty($params))
		{
			$currentDateFormate = date('Y-m-d H:i:s');
			$note = isset($params['note']) ? $params['note'] : "";
			$this->DepartmentsBatonRole->updateAll(array('DepartmentsBatonRole.is_occupied'=>0),array('DepartmentsBatonRole.role_id'=>$params['role_id']));

			$data = array('from_user_status'=>0, "to_user_status"=>1,"from_user_is_active"=>0,"to_user_is_active"=>1);

			$getFromUserData = $this->UserBatonRole->find("first", array("conditions"=> array("from_user"=> $params['from_user'],'to_user'=>$params['to_user'], "role_id"=>$params['role_id'],'status'=>1,'is_active'=>1)));
			if(!empty($getFromUserData))
			{
				$this->UserBatonRole->updateAll(array("status"=> $data['from_user_status'], "is_active"=>$data['from_user_is_active'], "notes"=>"'" . $note . "'", "updated_at"=> "'" . $currentDateFormate . "'"), array("from_user"=>$params['from_user'],"to_user"=>$params['to_user'],"role_id"=>$params['role_id']));

				$cusParams['to_user'] = $params['to_user'];
				$cusParams['from_user'] = $params['from_user'];
				$updateCustomBatonRole = $this->getUserBatonRolesData($cusParams);
			}

			$getToUserData = $this->UserBatonRole->find("first", array("conditions"=> array("from_user"=> $params['to_user'],'to_user'=>$params['from_user'], "role_id"=>$params['role_id'],'status !='=>1)));
			if(!empty($getToUserData))
			{
				$this->UserBatonRole->updateAll(array("status"=> $data['to_user_status'], "is_active"=>$data['to_user_is_active'], "notes"=>"'" . $note . "'", "updated_at"=> "'" . $currentDateFormate . "'"), array("from_user"=>$params['to_user'],"to_user"=>$params['from_user'],"role_id"=>$params['role_id'],'status !='=>1));
			}
			else
			{
				$saveToUserBatonRole = array("from_user"=>$params['to_user'], "to_user"=>$params['from_user'], "role_id"=> $params['role_id'], "status"=>$data['to_user_status'],"is_active"=>$data['to_user_is_active'], "notes"=>$note, "created_at"=> date("Y-m-d H:i:s"),  "updated_at"=> date("Y-m-d H:i:s"),  "deleted_at"=> date("Y-m-d H:i:s"));
				$this->UserBatonRole->saveAll($saveToUserBatonRole);
			}

			$isCacheModified = 1;
		}
		return $isCacheModified;
	}

	public function updateCacheOnLogout($params = array())
	{
		// return $params;
		$iscacheUpdated = 0;
		if(!empty($params))
		{
			$isDynamicDataExist = $this->CacheLastModifiedUser->find("first", array("conditions"=> array("user_id"=> $params['user_id'], "company_id"=> $params['company_id'], "type"=>'dynamic_directory_changed'), "order"=> array("id DESC")));

			if(!empty($isDynamicDataExist))
			{

				$this->CacheLastModifiedUser->updateLastModifiedDateDynamicDirectory($params['user_id'], $params['company_id'], 'dynamic_directory_changed',$isDynamicDataExist['CacheLastModifiedUser']['id']);
			}
			else
			{
				$saveCacheLastModifiedData = array("user_id"=>$params['user_id'], "company_id"=> $params['company_id'], "type"=>"dynamic_directory_changed", "status"=>"1", "last_modified"=>date('Y-m-d H:i:s'));
				$this->CacheLastModifiedUser->saveAll($saveCacheLastModifiedData);
			}
			//** Get last modified date dynamic directory[START]
			$getDynamicDirectoryLastModified = $this->CacheLastModifiedUser->getLastModifiedDateDynamicDirectory($params['company_id']);
			if(!empty($getDynamicDirectoryLastModified)){
				$paramsDynamicEtagUpdate['lastmodified'] = $getDynamicDirectoryLastModified[0]['cache_last_modified_users']['last_modified'];
				$paramsDynamicEtagUpdate['key'] = 'dynamicDirectoryEtag:'.$params['company_id'];
				$this->updateDynamicEtagData($paramsDynamicEtagUpdate);
			}
			$cacheUpdated = 1;
		}
		return $iscacheUpdated;
	}


	///*************************Multiple Device login for medicbleep version v2[END]******************///


	///*************************Miscellaneous function to send [START]******************///

	public function sendPushOnIos($params = array()) {
		if(!empty($params)) {

			$deviceToken = $params['device_token'];
			$fcmVoipToken = $params['fcm_voip_token'];
			$passphrase = 'password';
			$message = $params['notificationMessage'];
			$pemFile = WWW_ROOT.'/voipssl/'.VOIP_CERTIFICATE_IOS;
			// $pemFile = WWW_ROOT.'/voipssl/'.'apns-dist-cert.pem';
			$ctx = stream_context_create();
			stream_context_set_option($ctx, 'ssl', 'local_cert', $pemFile);
			stream_context_set_option($ctx, 'ssl', 'password', $passphrase);

			$fp = stream_socket_client(
				PUSH_END_POINT_IOS, $err,
				$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx
			);

			if (!$fp) {
				exit("Failed to connect: $err $errstr" . PHP_EOL);
			}

			$body = array(
				"aps"=> array(
					"alert"=> $message,
					"sound"=> "notification_sound.mp3"
				),
        "ios_voip"=> 1,
				'VOIPCall'=> 1,
				"is_broadcast"=> 1,
				"show_alert"=>1,
				"tap_to_reply" => 0
			);

			$payload = json_encode($body);

			// Build the binary notification
			$msg = chr(0) . pack('n', 32) . pack('H*', $fcmVoipToken) . pack('n', strlen($payload)) . $payload;

			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));
			// print_r($result);
			if(!$result) {
				$var = 0;
			}else{
				$var = 1;
			}
			// Close the connection to the server
			fclose($fp);
			return $var;
		}
	}

	public function sendPushOnAndroid($params = array()) {
		if(!empty($params['device_token'])) {
			$dataUserDevice = isset($params['device_token']) ? $params['device_token']:"";
			$tokenRegistrationIds = array($dataUserDevice);
			$message = $params['notificationMessage'];

			$voipPushMessage = array(
				"message" => $message,
				'VOIPCall'=> 1,
				"is_broadcast"=> 1,
				"show_alert"=>1,
				"tap_to_reply" => 0
			);

			$postFeilds = array(
				'registration_ids' => $tokenRegistrationIds,
				'data' => $voipPushMessage,
				'priority' => 'high'
			);
			$headers = array(
				'Authorization: key=' . GCM_ACCESS_KEY_V1,
				'Content-Type: application/json'
			);

			$ch = curl_init();
			// curl_setopt( $ch,CURLOPT_URL, PUSH_END_POINT_ANDROID );
			curl_setopt( $ch,CURLOPT_URL, PUSH_END_POINT_ANDROID );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $postFeilds ) );
			$result = curl_exec($ch );
			// print_r($result);
			curl_close( $ch );
			return $var = 1;
		}else{
			return $var = 0;
		}
	}

	public function sendPushOnIosv2($params = array()) {
		if(!empty($params)) {
			$http2_server = PUSH_END_POINT_IOS_V2;
			$device_token = $params['device_token'];
			$message = $params['notificationMessage'];
			$url = "{$http2_server}/3/device/{$device_token}";
			$app_bundle_id = APP_BUNDLE_ID_V2;
			$http2ch = curl_init();
			$type = 1;

			$message = '{
				"aps":{
					"alert":"'.$message.'",
					"priority":10,
					"sound": "notification_sound.mp3",
					"mutable-content":1
				},
				"data":{
					"type_name":"PushBroadcast",
					"message":"'.$message.'",
					"title":"Institution Broadcast Message"
				}
			}';

			// certificate
			$push_cert = WWW_ROOT.'/voipssl/'.PUSH_CERTIFICATE_IOS_V2;
			$push_cert_key = WWW_ROOT.'/voipssl/'.PUSH_CERTIFICATE_KEY_V2;

			// headers
			$headers = array(
				"apns-topic: {$app_bundle_id}",
				"User-Agent: My Sender",
			);

			// other curl options
			curl_setopt_array($http2ch, array(
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_2_0,
				CURLOPT_URL => $url,
				CURLOPT_PORT => 443,
				CURLOPT_HTTPHEADER => $headers,
				CURLOPT_POST => TRUE,
				CURLOPT_POSTFIELDS => $message,
				CURLOPT_RETURNTRANSFER => TRUE,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_SSL_VERIFYPEER => false,
				CURLOPT_SSLCERT => $push_cert,
				CURLOPT_SSLKEY => $push_cert_key,
				CURLOPT_HEADER => 1
			));

			// go...
			$result = curl_exec($http2ch);
			if ($result === FALSE) {
				throw new Exception("Curl failed: " .  curl_error($http2ch));
			}

			$result;

			// get response
			$status = curl_getinfo($http2ch, CURLINFO_HTTP_CODE);

			$status;
			return $status;
		}
	}

	public function sendPushOnAndroidv2($params = array()) {
		if(!empty($params['device_token'])) {
			$dataUserDevice = isset($params['device_token']) ? $params['device_token']:"";
			$tokenRegistrationIds = array($dataUserDevice);
			$message = $params['notificationMessage'];

			$voipPushMessage = array(
				"type_name" => "PushBroadcast",
				"message" =>$message,
				"sound" => "notification_sound",
				"title" => "Institution Broadcast Message",
				'channel_id'=>"Medic Bleep Broadcast"
			);

			$postFeilds = array(
				'registration_ids' => $tokenRegistrationIds,
				'data' => $voipPushMessage,
				'priority' => 'high'
			);
			$headers = array(
				'Authorization: key=' . GCM_ACCESS_KEY,
				'Content-Type: application/json'
			);

			$ch = curl_init();
			// curl_setopt( $ch,CURLOPT_URL, PUSH_END_POINT_ANDROID );
			curl_setopt( $ch,CURLOPT_URL, PUSH_END_POINT_ANDROID );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $postFeilds ) );
			$result = curl_exec($ch );
			// print_r($result);
			curl_close( $ch );
			return $var = 1;
		}else{
			return $var = 0;
		}
	}

	/*
	--------------------------------------------------------------------
	On: 25-02-2019
	I/P:
	O/P:
	Desc:
	--------------------------------------------------------------------
	*/

	public function sendVoipPushNotification($params =array())
	{
		if(!empty($params)) {
			$userVoipData = $this->VoipPushNotification->find('all', array('conditions' => array('VoipPushNotification.user_id' => $params['user_id'])));
			if(!empty($userVoipData))
			{
				foreach ($userVoipData as $key => $value) {
					$params['device_token'] = $value['VoipPushNotification']['device_token'];
					$params['device_token'] = $value['VoipPushNotification']['device_token'];
					$params['is_dnd_active'] = $params['is_dnd_active'];
					$params['push_on'] = $params['push_on'];
					$params['dnd_data'] = $params['dnd_data'];
					$sendNotification = $this->sendPushNotificationOnIosOne($params);
					$sendNotification = $this->sendPushNotificationAndroidOne($params);
				}
			}

		}else{
			return "Data is empty";
		}
	}

	/*
	--------------------------------------------------------------------
	On: 25-02-2019
	I/P:
	O/P:
	Desc:
	--------------------------------------------------------------------
	*/

	public function sendPushNotificationOnIosOne($params = array())
	{
		if(!empty($params))
		{

			$deviceToken = $params['device_token'];
			$fcmVoipToken = $params['device_token'];
			$isDndActive = $params['is_dnd_active'];
			$pushOn = $params['push_on'];
			$dndData = $params['dnd_data'];
			$passphrase = 'password';
			$message = 'Dissmiss Dnd';
			$pemFile = WWW_ROOT.'/voipssl/'.VOIP_CERTIFICATE_IOS;
			$ctx = stream_context_create();
			stream_context_set_option($ctx, 'ssl', 'local_cert', $pemFile);
			stream_context_set_option($ctx, 'ssl', 'password', $passphrase);

			// Open a connection to the APNS server
			$fp = stream_socket_client(
				PUSH_END_POINT_IOS, $err,
				$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
			if (!$fp)
				exit("Failed to connect: $err $errstr" . PHP_EOL);

			// echo 'Connected to APNS' . PHP_EOL;

			$body = array('VOIPCall' => '1',
				"aps"=> array("alert"=> $message, "sound"=> "notification_sound.mp3"),
                        "VOIPDndStatus"=>$isDndActive,
                        "push_on"=>$pushOn,
                        "dnd_data"=>$dndData
			);

			$payload = json_encode($body);

			// Build the binary notification
			$msg = chr(0) . pack('n', 32) . pack('H*', $fcmVoipToken) . pack('n', strlen($payload)) . $payload;

			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));

			if (!$result)
				// $var = 'Message not delivered' . PHP_EOL;
				// echo 'Message not delivered' . PHP_EOL;
				$var = 0;
			else
				// $var = 'Message successfully delivered' . PHP_EOL;
				// echo 'Message successfully delivered' . PHP_EOL;
				$var = 1;

			// Close the connection to the server
			fclose($fp);
			return $var;
		}
	}
	/*
	--------------------------------------------------------------------
	On: 25-02-2019
	I/P:
	O/P:
	Desc:
	--------------------------------------------------------------------
	*/

	public function sendPushNotificationAndroidOne($params = array()) {
		if(!empty($params))
		{
			$responseData = $this->sendDndPushNotificationOnAndroid($params);
			return $responseData;
		}
	}

	/*
	--------------------------------------------------------------------
	On: 25-02-2019
	I/P:
	O/P:
	Desc:
	--------------------------------------------------------------------
	*/
	public function sendDndPushNotificationOnAndroid($params = array())
	{
		if(!empty($params['device_token'])) {
			$dataUserDevice = isset($params['device_token']) ? $params['device_token']:"";
			$tokenRegistrationIds = array($dataUserDevice);
			$isDndActive = $params['is_dnd_active'];
			$pushOn = $params['push_on'];
			$voipPushMessage = array
					(
						'VOIPDndStatus'=> $isDndActive,
						"push_on"=>$pushOn,
						'VOIPCall'=> 1
					);
			$postFeilds = array
			(
				'registration_ids' 	=> $tokenRegistrationIds,
				'data'			=> $voipPushMessage,
				'priority' => 'high'
			);
			$headers = array
			(
				'Authorization: key=' . GCM_ACCESS_KEY,
				'Content-Type: application/json'
			);
			// echo "<pre>";print_r(GCM_ACCESS_KEY);exit();
			$ch = curl_init();
			curl_setopt( $ch,CURLOPT_URL, PUSH_END_POINT_ANDROID );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $postFeilds ) );
			$result = curl_exec($ch );
			curl_close( $ch );
			return $var = 1;
		}
		else
		{
			return $var = 0;
		}

	}


	//Test Function To send push notification On andriod
	public function sendRoleExchangePushNotificationTwo()
	{
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			try{
				$params['device_token'] = "c15FQcdhLCI:APA91bGldAF8ByhwwmE8Qgsdg1bKmeUoAcSpIWlUKki0ZHMBsfXdhPU7pOPVWnnHJMq14H_TqBEN7yEFQjVP2LM080K0aqCEj5iHb29MzVqi9CSzKkQia3OgTU4HmEYgrDrUrhgR6QOT";
				$userData = $this->sendRoleExchangePushNotificationAndroidTwo($params);
				$responseData = array('method_name'=> 'userBatonRolesDetail', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, 'data'=> $userData);
			}catch( Exception $e ) {
				$responseData = array('method_name'=> 'userBatonRolesDetail', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
			}
		}else{
			$responseData = array('method_name'=> 'userBatonRolesDetail', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		exit;
	}

	public function sendRoleExchangePushNotificationAndroidTwo($params = array()) {
		if(!empty($params))
		{
			$responseData = $this->sendRoleExchangePushNotificationOnAndroid($params);
			return $responseData;
		}
	}

	public function sendRoleExchangePushNotificationOnAndroid($params = array())
	{
		$params['device_token'] ="c1IrzNa9Bhk:APA91bFazdP2jPtp7XRe9Er9-8FNwzFIW3zBKLcsujF8JR_ixYaOsOleEeATxHuILoJ41sEpBESI8wWnsBlRNSbaY0duljKDg6i7lDq6YV5CTb9hvuqFJpqC5iQSRGTYFNd1NLIHWD70";
		if(!empty($params['device_token'])) {
			$dataUserDevice = isset($params['device_token']) ? $params['device_token']:"";
			$tokenRegistrationIds = array($dataUserDevice);
			$isDndActive = $params['is_dnd_active'];
			$voipPushMessage = array
					(
						'VOIPLogoutStatus'=> 1,
						'VOIPCall'=> 1,
						'message'=>ERROR_602
					);
			$postFeilds = array
			(
				'registration_ids' 	=> $tokenRegistrationIds,
				'data'			=> $voipPushMessage
			);
			$headers = array
			(
				'Authorization: key=' . GCM_ACCESS_KEY,
				'Content-Type: application/json'
			);
			// echo "<pre>";print_r(GCM_ACCESS_KEY);exit();
			$ch = curl_init();
			curl_setopt( $ch,CURLOPT_URL, PUSH_END_POINT_ANDROID );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $postFeilds ) );
			$result = curl_exec($ch );
			curl_close( $ch );
			return $result;
		}
		else
		{
			return $result;
		}

	}

	/*
	--------------------------------------------------------------------
	On: 25-02-2019
	I/P:
	O/P:
	Desc:
	--------------------------------------------------------------------
	*/

	public function sendAtWorkPushNotification($params =array())
	{
		if(!empty($params)) {
			$userVoipData = $this->VoipPushNotification->find('all', array('conditions' => array('VoipPushNotification.user_id' => $params['user_id'])));

			$userVersionInfo =  $this->UserLoginTransaction->find('first', array('conditions' =>
		                array('UserLoginTransaction.user_id' => $params['user_id'], 'UserLoginTransaction.device_type !=' => 'MBWEB'),'order'=>array('UserLoginTransaction.id DESC')
		                ,'limit'=> 1));

			$dataArr =  json_decode($userVersionInfo['UserLoginTransaction']['device_info']);
			$version = $dataArr->version;
			$deviceType = $dataArr->device_type;

			if($deviceType == "iOS"){
				if($version > "1.8.7"){
					if(!empty($userVoipData))
					{
						foreach ($userVoipData as $key => $value) {
							$params['device_token'] = $value['VoipPushNotification']['device_token'];
							$params['device_token'] = $value['VoipPushNotification']['device_token'];
							$params['at_work_status'] = $params['at_work_status'];
							$params['push_on'] = $params['push_on'];
							$sendNotification = $this->sendAtWorkPushNotificationOnIosOne($params);
						}
					}
				}
			}

			if($deviceType == "android" || $deviceType == "Android"){
				// if($version > "1.9.8"){
					if(!empty($userVoipData))
					{

						foreach ($userVoipData as $key => $value) {
							$params['device_token'] = $value['VoipPushNotification']['device_token'];
							$params['device_token'] = $value['VoipPushNotification']['device_token'];
							$params['at_work_status'] = $params['at_work_status'];
							$params['push_on'] = $params['push_on'];
							$sendNotification = $this->sendAtWorkPushNotificationAndroidOne($params);
						}
					}
				// }
			}

		}else{
			return "Data is empty";
		}
	}

	/*
	--------------------------------------------------------------------
	On: 25-02-2019
	I/P:
	O/P:
	Desc:
	--------------------------------------------------------------------
	*/

	public function sendAtWorkPushNotificationOnIosOne($params = array())
	{
		if(!empty($params))
		{

			$deviceToken = $params['device_token'];
			$fcmVoipToken = $params['device_token'];
			$atworkStatus = $params['at_work_status'];
			$pushOn = $params['push_on'];
			$passphrase = 'password';
			$message = 'Update Available Status';
			$pemFile = WWW_ROOT.'/voipssl/'.VOIP_CERTIFICATE_IOS;
			$ctx = stream_context_create();
			stream_context_set_option($ctx, 'ssl', 'local_cert', $pemFile);
			stream_context_set_option($ctx, 'ssl', 'password', $passphrase);

			// Open a connection to the APNS server
			$fp = stream_socket_client(
				PUSH_END_POINT_IOS, $err,
				$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
			if (!$fp)
				exit("Failed to connect: $err $errstr" . PHP_EOL);

			// echo 'Connected to APNS' . PHP_EOL;

			$body = array("aps"=> array("alert"=> $message, "sound"=> "notification_sound.mp3"),
                        "at_work_status"=>$atworkStatus,
                        "push_on"=>$pushOn
			);

			$payload = json_encode($body);

			// Build the binary notification
			$msg = chr(0) . pack('n', 32) . pack('H*', $fcmVoipToken) . pack('n', strlen($payload)) . $payload;

			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));

			if (!$result)
				// $var = 'Message not delivered' . PHP_EOL;
				// echo 'Message not delivered' . PHP_EOL;
				$var = 0;
			else
				// $var = 'Message successfully delivered' . PHP_EOL;
				// echo 'Message successfully delivered' . PHP_EOL;
				$var = 1;

			// Close the connection to the server
			fclose($fp);
			return $var;
		}
	}
	/*
	--------------------------------------------------------------------
	On: 25-02-2019
	I/P:
	O/P:
	Desc:
	--------------------------------------------------------------------
	*/

	public function sendAtWorkPushNotificationAndroidOne($params = array()) {
		if(!empty($params))
		{
			$responseData = $this->sendAtWorkPushNotificationOnAndroid($params);
			return $responseData;
		}
	}

	/*
	--------------------------------------------------------------------
	On: 25-02-2019
	I/P:
	O/P:
	Desc:
	--------------------------------------------------------------------
	*/
	public function sendAtWorkPushNotificationOnAndroid($params = array())
	{
		if(!empty($params['device_token'])) {
			$dataUserDevice = isset($params['device_token']) ? $params['device_token']:"";
			$tokenRegistrationIds = array($dataUserDevice);
			$atworkStatus = $params['at_work_status'];
			$pushOn = $params['push_on'];
			$voipPushMessage = array
					(
						'at_work_status'=> $atworkStatus,
						'push_on'=>$pushOn,
						'VOIPCall'=> 1
					);
			$postFeilds = array
			(
				'registration_ids' 	=> $tokenRegistrationIds,
				'data'			=> $voipPushMessage,
				'priority' => 'high'
			);
			$headers = array
			(
				'Authorization: key=' . GCM_ACCESS_KEY,
				'Content-Type: application/json'
			);
			// echo "<pre>";print_r(GCM_ACCESS_KEY);exit();
			$ch = curl_init();
			curl_setopt( $ch,CURLOPT_URL, PUSH_END_POINT_ANDROID );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $postFeilds ) );
			$result = curl_exec($ch );
			curl_close( $ch );
			return $var = 1;
		}
		else
		{
			return $var = 0;
		}

	}

	/*
	--------------------------------------------------------------------
	On: 23-07-2019
	I/P:
	O/P:
	Desc:
	--------------------------------------------------------------------
	*/

	public function getRoleTagForCustomDimension($userId,$companyId){
		if(isset($userId)){
			$return_array = array();
			$perm_data = $this->UserPermanentRole->find('all',array(
				'conditions'=>array(
					'UserPermanentRole.user_id' => $userId,
					'UserPermanentRole.institute_id' => $companyId
				),
				'fields' => array('
					UserPermanentRole.role
				'),
				'order' => 'UserPermanentRole.created_at ASC'
			));
			if(!empty($perm_data)){
				foreach ($perm_data as $perm_val) {
					$permanet_data = json_decode($perm_val['UserPermanentRole']['role'],true);
					if(isset($permanet_data['role'])){
						foreach ($permanet_data['role'] as $value) {
							if(count($value['values']) > 0){
								if(!isset($return_array[$value['tag']]) || $return_array[$value['tag']] == ''){
									$return_array = array_merge($return_array,array($value['tag'] => $value['values'][0]));
								}
							}
						}
					}
				}
			}
			if(!empty($return_array)){
				return json_encode(array($return_array));
			}else{
				return json_encode($return_array);
			}
		}
	}

	/*
	--------------------------------------------------------------------
	On: 23-07-2019
	I/P:
	O/P:
	Desc:
	--------------------------------------------------------------------
	*/

	public function saveTransaction($params = array() ){
		$response = 0;
		if(!empty($params))
		{	$params['note'] = "";
			$duration = (isset($duration)) ? $duration : 0;
			if($params['type'] == "Available")
			{
				$transactionParams['user_id'] = $params['user_id'];
				$transactionParams['company_id'] = $params['company_id'];
				$transactionParams['type'] = "Available";
				$transactionParams['status'] = $params['at_work'];
				$transactionParams['device_type'] = "Institution_pannel";
				$transactionParams['platform'] = "web";
      	$this->AvailableAndOncallTransaction->saveAll($saveTransactionData);
      	// if($params['start_time'] && $params['at_work'] == "0")
      	// {
      	// 	$this->DndTransaction->saveAll(array(
				// 		'user_id'=>$params['user_id'],
				// 		'institute_id'=>$params['company_id'],
				// 		"dnd_status_id"=>$params['dnd_status_id'],
				// 		"start_time"=>$params['start_time'],
				// 		"end_time"=>$params['end_time'],
				// 		"duration"=>"0","is_active"=>"0",
				// 		"other_user_id"=>"9", "note"=>$params['note'],
				// 		'custom_data'=>'web',
				// 		'created'=>date("Y-m-d H:i:s")
				// 	));
      	// }
			}
			if($params['type'] == "OnCall")
			{
				$transactionParams['user_id'] = $params['user_id'];
				$transactionParams['company_id'] = $params['company_id'];
				$transactionParams['type'] = "OnCall";
				$transactionParams['status'] = $params['oncall'];
				$transactionParams['device_type'] = "Institution_pannel";
				$transactionParams['platform'] = "web";
	      $this->AvailableAndOncallTransaction->saveAll($saveTransactionData);
			}
			$response = 1;
		}
		return $response;
	}

	///*************************Miscellaneous function to send [END]******************///


	///*************************Presence related function for v2 client[START]*******///

	public function getUserDataRoInAddInDirectoryUsingPresence($param)
	{
		$responce = $this->Medicbleep->addUserInDirectory($param);
		return $responce;
	}

	public function getPreviousInstitutionLeavePresence($param)
	{
		$responce = $this->Medicbleep->leaveUserInDirectory($param);
		return $responce;
	}

	public function getBatonRoleExchangeData($param)
	{
		$responce = $this->Medicbleep->batonRoleExchange($param);
		return $responce;
	}


	public function sendPushNotificationOnIosTest($param)
	{
		$http2_server = PUSH_END_POINT_IOS_V2;
		$device_token = "";
		$url = "{$http2_server}/3/device/{$device_token}";
		$app_bundle_id = APP_BUNDLE_ID_V2;
		$http2ch = curl_init();
		$type = 1;
		$message = '{
		   "aps":{
		     "alert":"Either your session has been expired or you have logged-in on another device.",
		     "priority":10,
		     "sound":{
		       "critical":1,
		       "name":"notification_sound.mp3",
		       "volume":0.1
		     },
		     "mutable-content":1
		   },
		   "data":{
		     "type_name":"PushMultipleDeviceLogin",
		     "message":"Either your session has been expired or you have logged-in on another device.",
		     "title":"Multiple Device Login"
		   }
		 }';

		// certificate
		$push_cert = WWW_ROOT.'/voipssl/'.PUSH_CERTIFICATE_IOS_V2;
		$push_cert_key = WWW_ROOT.'/voipssl/'.PUSH_CERTIFICATE_KEY_V2;

		// headers
		$headers = array(
		    "apns-topic: {$app_bundle_id}",
		    "User-Agent: My Sender",
		);

		// other curl options
		curl_setopt_array($http2ch, array(
		    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_2_0,
		    CURLOPT_URL => $url,
		    CURLOPT_PORT => 443,
		    CURLOPT_HTTPHEADER => $headers,
		    CURLOPT_POST => TRUE,
		    CURLOPT_POSTFIELDS => $message,
		    CURLOPT_RETURNTRANSFER => TRUE,
		    CURLOPT_TIMEOUT => 30,
		    CURLOPT_SSL_VERIFYPEER => false,
		    CURLOPT_SSLCERT => $push_cert,
		    CURLOPT_SSLKEY => $push_cert_key,
		    CURLOPT_HEADER => 1
		));

		// go...
		$result = curl_exec($http2ch);
		if ($result === FALSE) {
		  throw new Exception("Curl failed: " .  curl_error($http2ch));
		}

		$result;

		// get response
		$status = curl_getinfo($http2ch, CURLINFO_HTTP_CODE);

		$status;
		return $status;
	}

	public function apiRequestResponceTrackingv2($params)
	{
		if(!empty($params))
		{
			ini_set('display_errors', 1);
			ini_set('display_startup_errors', 1);
			error_reporting(E_ALL);
			$dbhost = '35.177.136.62:3306';
			$dbuser = 'mb_analitics';
			$dbpass = 'Team@Medic@';
			$database = 'medicbleep';
			$conn = mysqli_connect($dbhost, $dbuser, $dbpass);
			if(! $conn )
			{
			die('Could not connect to instance: ' . mysqli_error($conn));
			}

			$db = mysqli_select_db($conn,"medicbleep");
			if(! $db )
			{
			die('Could not Select Database: ' . mysqli_error($conn));
			}
			try{
				$insertData = mysqli_query($conn,"INSERT INTO api_request_responce_tracking (user_id,api_name,request,responce) 
				VALUES ('".$params["user_id"]."','".$params["api_name"]."','".$params["request"]."','".$params["response"]."')");
				return "Data successfully inserted";
			}catch( Exception $e ) {
				return "Error".$e->getMessage();
			}
		}
	}

	///*************************Presence related function for v2 client[END]*********///

}// End class
