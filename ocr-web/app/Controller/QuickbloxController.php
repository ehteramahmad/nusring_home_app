<?php
App::uses('AppController', 'Controller');

class QuickbloxController extends AppController {
	//public $uses = array('User','UserColleague');
	//public $components = array('Quickblox');
	
	public $uses = array('User','UserProfile','Specilities','Profession', 'Country', 'UserSpecilities', 'UserInterest', 'UserFollow',  'UserColleague','UserInstitution','UserEmployment');
	public $components = array('Common', 'Image', 'Email','Quickblox');
	
	public function importUserQuickblox(){
		$this->autoRender=false;
		$allUsers = $this->User->find('all');
		if(count($allUsers) > 0){
			$tokenDetails = $this->Quickblox->quickAuth();
			$token = $tokenDetails->session->token;
			foreach($allUsers as $user){
				$profile_image = !empty($user['UserProfile']['profile_img']) ? AMAZON_PATH . $user['User']['id']. '/profile/' . $user['UserProfile']['profile_img']:'';
				//echo $profile_image;exit();
				$this->Quickblox->quickAddUsers($token,$user['User']['email'],'12345678',$user['User']['email'],$user['User']['id'],$user['UserProfile']['first_name'].' '.$user['UserProfile']['last_name'],$profile_image);
			}
		}
		exit();
	}
	public function importColleagueQuickblox(){
		$senderlist = $this->UserColleague->find('all',
								array(
								'fields'=>'DISTINCT UserColleague.user_id',
								'conditions' => array('UserColleague.status' => 1),
								));
		if(count($senderlist) > 0){
			foreach($senderlist as $senderDetails){
				if($senderDetails['UserColleague']['user_id'] != 2){
				$params = array('User.id'=> $senderDetails['UserColleague']['user_id']);
				$userDetails = $this->User->userDetails( $params);
				$user_email = $userDetails['User']['email'];
				$tokenDetails = $this->Quickblox->quickLogin($user_email,'12345678');
				$token = $tokenDetails->session->token;
				$sender_id = $tokenDetails->session->user_id;
				$colleaguelist = $this->UserColleague->find('all',
								array(
								'conditions' => array('UserColleague.status' => 1,
													  'UserColleague.user_id' => $senderDetails['UserColleague']['user_id'], 
													),
								));
				if(count($colleaguelist) > 0){
					foreach($colleaguelist as $colleague){
						$colleagueDetails = $this->Quickblox->getuserDetails($token,$colleague['UserColleague']['colleague_user_id']);
						$receiver_name = $colleagueDetails->user->full_name;
						$receiver_chat_id = $colleagueDetails->user->id;
						$addDialogue = $this->Quickblox->quickAddDialog($token,$sender_id,$receiver_chat_id, $receiver_name);
						/*$dialogue_array = json_decode($addDialogue);
						$dialog_id = $dialogue_array->_id;
						$message = $this->Quickblox->sendMessage($token,$dialog_id,'Hi '.$receiver_name);
						echo $message;exit();
						$message = $this->Quickblox->sendMessage($token,$receiver_chat_id,'Hi '.$receiver_name);*/
						
					}
				}
			}
		}
		}
		exit();
	}
	
	/*
	-------------------------------------------------------------------------------------
	On: 08-09-2015
	I/P: HEADER: (access_key, token) JSON: {"user_id":""}
	O/P: JSON data
	Desc: Get user profile for any particular user
	-------------------------------------------------------------------------------------
	*/
	public function getUserProfile(){
		$responseData = array();
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true);
				if( $this->tokenValidate() && $this->accesskeyCheck() ){
					if( $this->User->findById( $dataInput['user_id'] ) ){
						$params['User.id'] = $dataInput['user_id'];
						$this->User->recursive = 2;
						$userDataList = $this->User->userDetails( $params ); 
						
						$userData = $this->filterUserFields($userDataList);
						//** Check user follow
						$isFollow = $this->UserFollow->find("count", array("conditions"=> array("followed_by"=> $dataInput['loggedin_user_id'], "followed_to"=> $dataInput['user_id'], "follow_type"=> 1, "status"=> 1)));
						$userData['is_following'] = ( $isFollow > 0 ) ? 1 : 0; 
						//** Check user Colleague
							$colleagueStatus = 0; $requestType = '';
							$myColleague = $this->UserColleague->find("first", array("conditions"=> array("user_id"=> $dataInput['loggedin_user_id'], "colleague_user_id"=> $dataInput['user_id'])));
							$meColleague = $this->UserColleague->find("first", array("conditions"=> array("user_id"=> $dataInput['user_id'], "colleague_user_id"=> $dataInput['loggedin_user_id'])));
							if( !empty($myColleague) ){
								$colleagueStatus = $myColleague['UserColleague']['status'];
								$requestType = "from_me";
							}elseif( !empty($meColleague) ){
								$colleagueStatus = $meColleague['UserColleague']['status'];
								$requestType = "to_me";
							}
					$userData['Colleague'] = array('status' => (int) $colleagueStatus, 'request_by' => $requestType);
					$responseData = array('method_name'=> 'getUserProfile','status'=>'1','response_code'=>'200', 'message'=> ERROR_200, 'data'=> array('User'=> $userData));
			    	}else{
			    		$responseData = array('method_name'=> 'getUserProfile','status'=>'0','response_code'=>'618', 'message'=> ERROR_618);
			    	}
			    }else{
			         $responseData = array('method_name'=> 'getUserProfile','status'=>'0','response_code'=>'602', 'message'=> ERROR_602);
		         }
	       }else{
			$responseData = array('method_name'=> 'getUserProfile','status'=>'0','response_code'=>'611','message'=> ERROR_611);
	    }
    	$encryptedData = $this->Common->encryptData(json_encode($responseData));
	echo json_encode(array("values"=> $encryptedData));
    	exit;
	}
/*
	-------------------------------------------------------------------------------------
	On: 08-09-2015
	I/P: array()
	O/P: array()
	Desc: Get all user profile fields
	-------------------------------------------------------------------------------------
	*/
	public function filterUserFields( $ud = array(),$loggedinUserId = NULL){ 
		$userData = array(); 
		if( !empty($ud) ){
			$userCountry = '';
			if( !empty($ud['UserProfile']['country_id']) ){
				$countryData = $this->Country->findById( $ud['UserProfile']['country_id'] );
				$userCountry = !empty($countryData['Country']['country_name'])?$countryData['Country']['country_name']:'';
			}
			//** User Profession
			$userProfession = '';
			if( !empty($ud['UserProfile']['profession_id']) ){
				$professionData = $this->Profession->findById( $ud['UserProfile']['profession_id'] );
				$userProfession  = !empty($professionData['Profession']['profession_type'])?$professionData['Profession']['profession_type']:'';
			}
			//** Get User Specilities
			$userSpecilities = array();
			if( !empty($ud['UserSpecilities']) ){
				foreach( $ud['UserSpecilities'] as $userSpecility){	//echo "<pre>";print_r($userSpecility);die;
					$userSpecilities[] = array('id'=> !empty($userSpecility['Specilities']['id'])?$userSpecility['Specilities']['id']:"", 'name'=> !empty($userSpecility['Specilities']['name'])?$userSpecility['Specilities']['name']:"", 'img_path'=> !empty($userSpecility['Specilities']['img_name'])?SPECILITY_ICON_PATH . $userSpecility['Specilities']['img_name']:"");
				}
			}
			//** Follow and following count
			$followCount = $this->UserFollow->followCount( $ud['User']['id'] );
			$followingCount = $this->UserFollow->followingCount( $ud['User']['id'] );
			//** User Data	
			
			$userEmail = !empty($ud['User']['email']) ? $ud['User']['email']:'';
			$address = !empty($ud['UserProfile']['address']) ? $ud['UserProfile']['address']:'';
			$dob = (!empty($ud['UserProfile']['dob']) && $ud['UserProfile']['dob'] != "0000-00-00") ? $ud['UserProfile']['dob']:'';
			$contactNum = !empty($ud['UserProfile']['contact_no']) ? $ud['UserProfile']['contact_no']:'';
		
			//** If user is colleague show DOB
			$myColleague = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $loggedinUserId, "colleague_user_id"=> $ud['User']['id'], "status"=> 1)));
			$meColleague = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $ud['User']['id'], "colleague_user_id"=> $loggedinUserId, "status"=> 1)));
			if($myColleague > 0 || $meColleague > 0){
				$dob = (!empty($ud['UserProfile']['dob']) && $ud['UserProfile']['dob'] != "0000-00-00") ? $ud['UserProfile']['dob']:'';
			}
			//** User Education History
				$userEducationData = array();
				$userEducation = $this->UserInstitution->getUserInstitutions( $ud['User']['id'] );
				foreach( $userEducation as $ue ){ 
					$userEducationData[] = array("id"=> !empty($ue['UserInstitution']['id']) ? $ue['UserInstitution']['id'] : '',"from_date"=> (int) $ue['UserInstitution']['from_date'], "to_date"=> (int) $ue['UserInstitution']['to_date'], "institution_id"=> !empty($ue['InstituteName']['id']) ? (int) $ue['InstituteName']['id'] : (int) 0,"institute_name"=> !empty($ue['InstituteName']['institute_name']) ? $ue['InstituteName']['institute_name'] : '',"education_degree_id"=> !empty($ue['EducationDegree']['id']) ? (int) $ue['EducationDegree']['id'] : (int) 0, "education_degree_name"=> !empty($ue['EducationDegree']['degree_name']) ? $ue['EducationDegree']['degree_name'] : '' );
				}
			//** User employment history
				$userEmploymentData = array(); $designationDetails = array();
				$userEmployment = $this->UserEmployment->getUserEmployment( $ud['User']['id'] );
				foreach( $userEmployment as $employment){
					$designationDetails = array("designation_id"=> !empty($employment['Designation']['id']) ? $employment['Designation']['id'] : (int) 0, "designation_name"=> !empty($employment['Designation']['designation_name']) ? $employment['Designation']['designation_name'] : "");
					$userEmploymentData[] = array("id"=> !empty($employment['UserEmployment']['id']) ? (int) $employment['UserEmployment']['id'] : (int) 0, "company_id"=> !empty($employment['Company']['id']) ? $employment['Company']['id'] : (int) 0 ,"company_name"=> !empty($employment['Company']['company_name']) ? $employment['Company']['company_name'] : '',"from_date"=> $employment['UserEmployment']['from_date'], "to_date"=> $employment['UserEmployment']['to_date'],"location"=> $employment['UserEmployment']['location'], "description"=> $employment['UserEmployment']['description'],"is_current"=> $employment['UserEmployment']['is_current'], "Designation"=> $designationDetails);
				}
			$userData = array(
								'user_id'=> !empty($ud['User']['id']) ? $ud['User']['id']:'',
								'email'=>	$userEmail,
								'last_loggedin_date'=> !empty($ud['User']['last_loggedin_date']) ? $ud['User']['last_loggedin_date']:'',
								'status' => !empty($ud['User']['status']) ? (string) $ud['User']['status']:'',
								'registration_date'=> !empty($ud['User']['registration_date']) ? (string) $ud['User']['registration_date']:'',
								'first_name'=> !empty($ud['UserProfile']['first_name']) ? $ud['UserProfile']['first_name']:'',
								'last_name'=> !empty($ud['UserProfile']['last_name']) ? $ud['UserProfile']['last_name']:'',
								'profile_img'=> !empty($ud['UserProfile']['profile_img']) ? AMAZON_PATH . $ud['User']['id']. '/profile/' . $ud['UserProfile']['profile_img']:'',
								'country'=> !empty($userCountry) ? $userCountry:'',
								'county'=> !empty($ud['UserProfile']['county']) ? $ud['UserProfile']['county']:'',
								'city'=> !empty($ud['UserProfile']['city']) ? $ud['UserProfile']['city']:'',
								'Profession'=> array('id'=>!empty($ud['UserProfile']['profession_id'])?$ud['UserProfile']['profession_id']:"", 'profession_type'=> !empty($userProfession) ? $userProfession:''),
								'gmcnumber'=> !empty($ud['UserProfile']['gmcnumber']) ? $ud['UserProfile']['gmcnumber']:'',
								'contact_no'=> $contactNum,
								'address'=> $address,
								'dob'=> $dob,
								'year_of_graduation'=> !empty($ud['UserProfile']['year_of_graduation']) ? $ud['UserProfile']['year_of_graduation']:'',
								'gender'=> !empty($ud['UserProfile']['gender']) ? $ud['UserProfile']['gender']:'',
								'current_employment'=> !empty($ud['UserProfile']['current_employment']) ? $ud['UserProfile']['current_employment']:'',
								'institution_name'=> !empty($ud['UserProfile']['institution_name']) ? $ud['UserProfile']['institution_name']:'',
								'Institutions'=> !empty($userEducationData) ? $userEducationData : array(),
								'Company'=> !empty($userEmploymentData) ? $userEmploymentData : array(),
								'Specilities'=> $userSpecilities,
								'follow'=> $followCount,
								'following'=> $followingCount
							);
		}
		return $userData;
	}
	
	public function getUserDialogue(){
		$params = array('User.id'=> 2);
		$userDetails = $this->User->userDetails( $params);
		$user_email = $userDetails['User']['email'];
		$tokenDetails = $this->Quickblox->quickLogin($user_email,'12345678');
		//print_r($tokenDetails);exit();
		$token = $tokenDetails->session->token;
		$dialogueList = $this->Quickblox->quickGetDialog($token);
		//print_r($dialogueList->items);exit();
		foreach($dialogueList->items as $dialogue){
			print_r($dialogue->occupants_ids);exit();
		}
		
	}
	public function addusertoquickblox(){
		$username = 'shashi@mediccreations.com';
		$password = '12345678';
		$email = 'shashi@mediccreations.com';
		$externalid = 209;
		$fullname = 'Shashi Bhusan Shukla';
		$message = $this->Quickblox->quickAdduserFromOcr($username,$password,$email,$externalid,$fullname);
		echo $message;
		exit();
	}
	public function adddialogue(){
		$sender_id = 210;
		$receiver_id = 17;
		$senderDetails = $this->User->findById($sender_id);
		$receiverDetails = $this->User->findById($receiver_id);
		$this->Quickblox->quickAddRemoveDialog($senderDetails,$receiverDetails,1);
	}
	public function deletedialogue(){
		$sender_id = 210;
		$receiver_id = 17;
		$senderDetails = $this->User->findById($sender_id);
		$receiverDetails = $this->User->findById($receiver_id);
		$message = $this->Quickblox->quickAddRemoveDialog($senderDetails,$receiverDetails,0);
		echo $message;exit();
	}
	public function quickUpdateUserProfile(){
		$email = 'devpupun5@gmail.com';
		$profile_image = 'http://www.acuminoussoftware.com/wp-content/uploads/2014/06/life_cycle1.jpg';
		$profile_details = $this->Quickblox->update_userprofile($email,$profile_image);
		echo $profile_details;exit();
	}
	public function adddemouser(){
		$username = "abc@def.com";
		$password = "12345678";
		$email = "abc@def.com";
		$externalid = 100001;
		$fullname = "xxxxxxx yyyyyy";
		$details = $this->Quickblox->quickAdduserFromOcr($this->request->data['User']['email'],'12345678',$this->request->data['User']['email'],$userId,$this->request->data['UserProfile']['first_name'].' '.$this->request->data['UserProfile']['last_name']);
		echo $details;exit();
	}

public function adddialogueTest(){
		
       $data = $this->UserColleague->find('all',array('conditions'=>array('UserColleague.colleague_user_id'=>110,'UserColleague.status'=>1),'fields'=>array('UserColleague.colleague_user_id','UserColleague.user_id') ));
       pr($data);
       exit();	

       foreach($data as $val){
       		$sender_id  = ""; $receiver_id = "";
       		$senderDetails = array(); $receiverDetails = array();
	       	$sender_id = $val['UserColleague']['colleague_user_id'];
			$receiver_id = $val['UserColleague']['user_id'];
			//echo "Sender: " . $sender_id . " Receiver: " . $receiver_id;
			//echo "<br>";
			$senderDetails = $this->User->findById($sender_id);
			$receiverDetails = $this->User->findById($receiver_id);
			$message = $this->Quickblox->quickAddRemoveDialog($senderDetails,$receiverDetails,1);
			//echo $message;

       }
		exit();
	}


	public function adddialogueTestCreateDialogs(){
		    ini_set('max_execution_time', 4*300);


            $data = $this->UserColleague->find('all',array('conditions'=>array('UserColleague.user_id'=>25,'UserColleague.status'=>1),'fields'=>array('UserColleague.colleague_user_id','UserColleague.user_id') ));
            foreach($data as $val){
       		$sender_id  = ""; $receiver_id = "";
       		$senderDetails = array(); $receiverDetails = array();
	       	$sender_id = $val['UserColleague']['colleague_user_id'];
			$receiver_id = $val['UserColleague']['user_id'];
			//echo "Sender: " . $sender_id . " Receiver: " . $receiver_id;
			//echo "<br>";
			$senderDetails = $this->User->findById($sender_id);
			$receiverDetails = $this->User->findById($receiver_id);
			$message = $this->Quickblox->quickAddRemoveDialog($senderDetails,$receiverDetails,1);
			echo $message.',';

          }
		exit();
	}

	
}
