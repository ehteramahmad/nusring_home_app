<?php

/*
 * Settergetter Component
 * 
 * 
 *
 */
class SettergetterComponent extends Component{ 
	
	/*
		setter user_id
	*/
	public function setterUserId($userId=NULL){

	}

	/*
		getter user_id
	*/
	public function getterUserId($userId=NULL){
		if(isset($userId) && !empty($userId)){
			$userId = (int) $userId;
		}else{
			$userId = (int) 0;
		}
		return $userId;
	}

	/*
		setter email
	*/
	public function setterEmail($email=NULL){

	}

	/*
		getter email
	*/
	public function getterEmail($email=NULL){
		if(isset($email) && !empty($email)){
			$email = stripslashes($email);
		}else{
			$email = "";
		}
		return $email;
	}

	/*
		setter firstName
	*/
	public function setterFirstName($firstName=NULL){

	}

	/*
		getter firstName
	*/
	public function getterFirstName($firstName=NULL){
		if(isset($firstName) && !empty($firstName)){
			$firstName = stripslashes($firstName);
		}else{
			$firstName = "";
		}
		return $firstName;
	}

	/*
		setter lastName
	*/
	public function setterLastName($lastName=NULL){

	}

	/*
		getter lastName
	*/
	public function getterLastName($lastName=NULL){
		if(isset($lastName) && !empty($lastName)){
			$lastName = stripslashes($lastName);
		}else{
			$lastName = "";
		}
		return $lastName;
	}

	/*
		setter profile iamge
	*/
	public function setterProfileImg($profileImg=NULL){

	}

	/*
		getter profile iamge
	*/
	public function getterProfileImg($profileImg=NULL){
		if(isset($profileImg) && !empty($profileImg)){
			$profileImg = $profileImg;
		}else{
			$profileImg = "";
		}
		return $profileImg;
	}

	/*
		getter profile iamge
	*/
	public function getterProfileThumbnailImg($profileImg=NULL){
		if(isset($profileImg) && !empty($profileImg)){
			$profileImg = $profileImg;
		}else{
			$profileImg = "";
		}
		return $profileImg;
	}


	/*
		setter profile iamge
	*/
	public function setterCountryCode($countryCode=NULL){

	}

	/*
		getter profile iamge
	*/
	public function getterCountryCode($countryCode=NULL){
		if(isset($countryCode) && !empty($countryCode)){
			$countryCode = $countryCode;
		}else{
			$countryCode = "";
		}
		return $countryCode;
	}

	/*
		setter on duty
	*/
	public function setterOnduty($onduty=NULL){

	}

	/*
		getter on duty
	*/
	public function getterOnduty($onduty=NULL){
		if(isset($onduty) && !empty($onduty)){
			$onduty =  $onduty;
		}else{
			$onduty = "0";
		
		}
		return $onduty;
	}

	/*
		setter at work
	*/
	public function setterAtWork($atWork=NULL){

	}

	/*
		getter at work
	*/
	public function getterAtWork($atWork=NULL){
		if(isset($atWork) && !empty($atWork)){
			$atWork =  $atWork;
		}else{
			$atWork = "0";
		}
		return $atWork;
	}

	/*
		setter role status
	*/
	public function setterRoleStatus($roleStatus=NULL){

	}

	/*
		getter role status
	*/
	public function getterRoleStatus($roleStatus=NULL){
		if(isset($roleStatus) && !empty($roleStatus)){
			$roleStatus =  $roleStatus;
		}else{
			$roleStatus = "";
		
		}
		return $roleStatus;
	}

}
?>
