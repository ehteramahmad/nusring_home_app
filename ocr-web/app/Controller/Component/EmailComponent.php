<?php

/*
 * Email Component
 * 
 * Contains all related function for email sending
 *
 */

App::uses('CakeEmail', 'Network/Email');

class EmailComponent extends Component{ 
	
	/*
	On: 03-08-2015
	I/P: $params = array()
	O/P: 
	Desc: Sending email to user with template.
	*/	
	public function verifyEmail( $params = array()){ 
		if( !empty($params) ){
			$Email = new CakeEmail();
			//$Email->config('smtp');
			$Email->config('mailchimp');
			$params['subject'] = 'Please verify your email';
			$Email->template('verify_email', '');
			$Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			$replacearray = array();
			$replacearray['NAME'] = $params['name'];
			$replacearray['ACTIVATION_KEY'] = $params['activationLink'];
			$replacearray['BASEURL'] = BASE_URL;
			//$replacearray['SITE_NAME'] = 'The On Call Room Team!';
			/*===========End values to be replaced in the template==========*/
		  	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			$Email->viewVars(array('msg' => $message));
			//$Email->from(array($params['from'] => $params['from']));
			$Email->from(array(NO_REPLY_EMAIL => "The On Call Room"));
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );

			try{
				if($Email->send()){
					$msg = 'mail sent';
				}else{
					$msg = 'mail not sent';
				}
			}catch( Exception $e ){
				$msg = $e->getMessage();
			}
			return $msg;
		}
	}

	/*
	--------------------------------------------------------------------------------
	On: 10-08-2015
	I/P: $params = array()
	O/P: True/False
	Desc: Sending email to user with template for forgot password.
	--------------------------------------------------------------------------------
	*/	
	public function forgotPassword( $params = array()){ 
		if( !empty($params) ){
			$Email = new CakeEmail();
			//$Email->config('smtp');
			$Email->config('mailchimp');
			$params['subject'] = 'Your new password';
			$Email->template('forgot_password', '');
			$Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			$replacearray = array();
			$replacearray['NAME'] = $params['name'];
			$replacearray['PASS'] = $params['password'];
			$replacearray['SITE_NAME'] = 'OCR Team';
			/*===========End values to be replaced in the template==========*/
		  	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			$Email->viewVars(array('msg' => $message));
			$Email->from(array(NO_REPLY_EMAIL => "The On Call Room"));
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );
			try{
				if($Email->send()){
					$msg = "Mail Sent";
				}else{
					$msg = "Mail Not Sent";
				}
			}catch( Exception $e ){
				return $msg = $e->getMessage();
			}
			return $msg;
		}
	}

	/*
	--------------------------------------------------------------------------------
	On: 17-12-2015
	I/P: $params = array()
	O/P: True/False
	Desc: Sending email to user whose beat marked as spam.
	--------------------------------------------------------------------------------
	*/	
	public function markSpamBeatEmailToBeatOwner( $params = array()){ 
		if( !empty($params) ){
			$Email = new CakeEmail();
			//$Email->config('smtp');
			$Email->config('mailchimp');
			$Email->template('mark_spam_beat_to_beatowner', '');
			$Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			$replacearray = array();
			$replacearray['NAME'] = $params['name'];
			$replacearray['BEATNAME'] = $params['beat_title'];
			$replacearray['SITE_NAME'] = 'OCR Team';
			$replacearray['REASON'] = $params['spam_reason'];
			/*===========End values to be replaced in the template==========*/
		  	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			$Email->viewVars(array('msg' => $message));
			//$Email->from(array($params['from'] => $params['from']));
			$Email->from(array(NO_REPLY_EMAIL => "The On Call Room"));
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );
			try{
				if($Email->send()){
					return true;
				}else{
					return false;
				}
			}catch( Exception $e ){
				$msg = $e->getMessage();
			}
		}
	}

	/*
	--------------------------------------------------------------------------------
	On: 17-12-2015
	I/P: $params = array()
	O/P: True/False
	Desc: Sending email to admin beat marked as spam.
	--------------------------------------------------------------------------------
	*/	
	public function markSpamBeatEmailToAdmin( $params = array() ){ 
		if( !empty($params) ){
			$Email = new CakeEmail();
			//$Email->config('smtp');
			$Email->config('mailchimp');
			$Email->template('mark_spam_beat_to_admin', '');
			$Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			$replacearray = array();
			$replacearray['SENDERNAME'] = $params['name'];
			$replacearray['BEATNAME'] = $params['beat_title'];
			$replacearray['SITE_NAME'] = 'OCR Team';
			$replacearray['REASON'] = $params['spam_reason'];
			$replacearray['BEAT_OWNER'] = $params['beat_owner_name'];
			$replacearray['SPAM_DATE'] = $params['spam_date'];
			$replacearray['SITE_REDIRECT_URL'] = $params['site_url_path'];
			/*===========End values to be replaced in the template==========*/
		  	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			$Email->viewVars(array('msg' => $message));
			$Email->from(array($params['from'] => $params['from']));
			$Email->from(array(NO_REPLY_EMAIL => "The On Call Room"));
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );
			try{
				if($Email->send()){
					return true;
				}else{
					return false;
				}
			}catch( Exception $e ){
				$msg = $e->getMessage();
			}
		}
	}

	/*
	--------------------------------------------------------------------------------
	On: 21-12-2015
	I/P: $params = array()
	O/P: True/False
	Desc: Sending email to patient when any doctor send consent.
	--------------------------------------------------------------------------------
	*/	
	public function sendConsentMailToPatient( $params = array() ){
		if( !empty($params) ){
			$Email = new CakeEmail();
			//$Email->config('smtp');
			$Email->config('mailchimp');
			$Email->template('consent_form', '');
			$Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			$replacearray = array();
			$replacearray['NAME'] = $params['patientName'];
			$replacearray['DOCTORNAME'] = $params['doctorName'];
			$replacearray['BASEURL'] = BASE_URL;
			$replacearray['SITE_NAME'] = 'OCR Team';
			/*===========End values to be replaced in the template==========*/
		  	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			$Email->viewVars(array('msg' => $message));
			//$Email->from(array($params['from'] => $params['from']));
			$Email->from(array(NO_REPLY_EMAIL => "The On Call Room"));
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );
			$Email->attachments(array(
				$params['file_name'] => array(
							'file' => $params['file_path'],
							'mimetype' => 'image/png',
							)
				)
			);
			try{
				if($Email->send()){
					return true;
				}else{
					return false;
				}
			}catch( Exception $e ){
				$msg = $e->getMessage();
			}
		}
	}


	/*
	On: 29-12-2015
	I/P: $params = array()
	O/P: 
	Desc: Sending signup welcome email to user with template.
	*/	
	public function signupWelcomeEmail( $params = array()){ 
		if( !empty($params) ){
			$Email = new CakeEmail();
			//$Email->config('smtp');
			$Email->config('mailchimp');
			$params['subject'] = 'Welcome';
			$Email->template('signup_welcome', '');
			$Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			$replacearray = array();
			$replacearray['NAME'] = $params['name'];
			$replacearray['REGARDS'] = $params['regards'];
			/*===========End values to be replaced in the template==========*/
		  	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			$Email->viewVars(array('msg' => $message));
			//$Email->from(array($params['from'] => $params['from']));
			$Email->from(array(NO_REPLY_EMAIL => "The On Call Room"));
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );

			try{
				if($Email->send()){
					$msg = 'Welcome mail sent';
				}else{
					$msg = 'Welcome mail not sent';
				}
			}catch( Exception $e ){
				$msg = $e->getMessage();
			}
			return $msg;
		}
	}

	/*
	--------------------------------------------------------------------
	On: 
	I/P:
	O/P:
	Desc: Replace variable and put values from email template.
	--------------------------------------------------------------------
	*/
	function strreplacetextreplace(&$t, $d) {
		preg_match_all ( '/{\%(\w*)\%\}/' , $t , $matches );
		foreach($matches[1] as $m){
						$pattern = "/{\%".$m."\%\}/";
						$t = preg_replace( $pattern, $d[$m], $t);
		}
		return $t;
    }

    /*
	On: 21-01-2015
	I/P: 
	O/P: 
	Desc: Sending APP download link email to visitor .
	*/	
	public function appDownloadLinkMail( $params = array()){ 
		if( !empty($params) ){
			$Email = new CakeEmail();
			//$Email->config('smtp');
			$Email->config('mailchimp');
			$params['subject'] = 'APP Download Link';
			$Email->template('app_download_link', '');
			$Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			$replacearray = array();
			$replacearray['NAME'] = $params['name'];
			$replacearray['REGARDS'] = $params['regards'];
			/*===========End values to be replaced in the template==========*/
		  	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			$Email->viewVars(array('msg' => $message));
			//$Email->from(array($params['from'] => $params['from']));
			$Email->from(array(NO_REPLY_EMAIL => "The On Call Room"));
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );

			try{
				if($Email->send()){
					$msg = 'Mail sent';
				}else{
					$msg = 'Mail not sent';
				}
			}catch( Exception $e ){
				$msg = $e->getMessage();
			}
			return $msg;
		}
	}

	/*
	On: 21-01-2015
	I/P: 
	O/P: 
	Desc: Sending mail to admin when user fills contact us page.
	*/	
	public function contactUsMail( $params = array()){ 
		if( !empty($params) ){
			$Email = new CakeEmail();
			//$Email->config('smtp');
			$Email->config('mailchimp');
			$Email->template('contact_us', '');
			$Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			$replacearray = array();
			$replacearray['NAME'] = $params['name'];
			$replacearray['SITE'] = "OCR Team";
			$replacearray['EMAIL'] = $params['from'];
			$replacearray['MESSAGE'] = $params['message']; 
			/*===========End values to be replaced in the template==========*/
		  	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			$Email->viewVars(array('msg' => $message));
			//$Email->from(array($params['from'] => $params['from']));
			$Email->from(array(NO_REPLY_EMAIL => "The On Call Room"));
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );

			try{
				if($Email->send()){
					$msg = 'Contact Us Mail sent';
				}else{
					$msg = 'Contact Us Mail not sent';
				}
			}catch( Exception $e ){
				$msg = $e->getMessage();
			}
			return $msg;
		}
	}

	/*
	------------------------------------------------------------------------
	On: 22-01-2015
	I/P: 
	O/P: 
	Desc: Sending mail user when admin approves the user.
	------------------------------------------------------------------------
	*/	
	public function userApproveByAdmin( $params = array()){ 
		if( !empty($params) ){
			$Email = new CakeEmail();
			//$Email->config('smtp');
			$Email->config('mailchimp');
			$params['subject'] = 'Approval Confirmed';
			if($params['registration_source'] == "MB"){
				$Email->template('medicbleep_user_approve_by_admin', '');
			}else{
				$Email->template('user_approve_by_admin', '');
			}
			$Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			$replacearray = array();
			$replacearray['NAME'] = $params['name'];
			//$replacearray['SITE_NAME'] = "OCR Team";
			/*===========End values to be replaced in the template==========*/
		  	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			$Email->viewVars(array('msg' => $message));
			//$Email->from(array($params['from'] => $params['from']));
			$Email->from(array(NO_REPLY_EMAIL => "The On Call Room"));
			$Email->from($params['fromMail']);
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );

			try{
				if($Email->send()){
					$msg = 'Admin Approve User Mail sent';
				}else{
					$msg = 'Admin Approve User Mail not sent';
				}
			}catch( Exception $e ){
				$msg = $e->getMessage();
			}
			return $msg;
		}
	}

	/*
	-----------------------------------------------------------------------------
	On: 26-01-2016
	I/P: 
	O/P: 
	Desc: Sending contact response mail to visitor.
	-----------------------------------------------------------------------------
	*/	
	public function contactUsMailResponseToVisitor( $params = array()){ 
		if( !empty($params) ){
			$Email = new CakeEmail();
			//$Email->config('smtp');
			$Email->config('mailchimp');
			$params['subject'] = 'Contact Us';
			$Email->template('contact_us', '');
			$Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			$replacearray = array();
			$replacearray['NAME'] = $params['name']; 
			$replacearray['SITE'] = "OCR Team";
			/*===========End values to be replaced in the template==========*/
		  	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			$Email->viewVars(array('msg' => $message));
			//$Email->from(array($params['from'] => $params['from']));
			$Email->from(array(NO_REPLY_EMAIL => "The On Call Room"));
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );

			try{
				if($Email->send()){
					$msg = 'Contact Us Response Mail to visitor  sent';
				}else{
					$msg = 'Contact Us Response Mail to visitor not sent';
				}
			}catch( Exception $e ){
				$msg = $e->getMessage();
			}
			return $msg; 
		}
	}

	/*
	-----------------------------------------------------------------------------
	On: 12-02-2016
	I/P: 
	O/P: 
	Desc: Sending promt mail to admin when user clicks on verify link.
	-----------------------------------------------------------------------------
	*/
	public function userActivationPromtToAdmin($params = array()){
		if( !empty($params) ){
			$Email = new CakeEmail();
			//$Email->config('smtp');
			$Email->config('mailchimp');
			$params['subject'] = 'New User Activated Account';
			$Email->template('user_activation_promt_to_admin', '');
			$Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			$replacearray = array();
			$replacearray['NAME'] = $params['name']; 
			$replacearray['EMAIL'] = $params['email']; 
			$replacearray['COUNTRY'] = $params['country']; 
			$replacearray['SITE_URL'] = $params['siteUrl']; 
			/*===========End values to be replaced in the template==========*/
		  	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			$Email->viewVars(array('msg' => $message));
			//$Email->from(array($params['from'] => $params['from']));
			$Email->from(array(NO_REPLY_EMAIL => "The On Call Room"));
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );

			try{
				if($Email->send()){
					$msg = 'New User Active Promt to Admin Sent';
				}else{
					$msg = 'New User Active Promt to Admin Not Sent';
				}
			}catch( Exception $e ){
				$msg = $e->getMessage();
			}
			return $msg; 
		}
	}
	public function unmarkSpamBeatEmailToBeatOwnerByAdmin($params = array()){
	if( !empty($params) ){
			$Email = new CakeEmail();
			//$Email->config('smtp');
			$Email->config('mailchimp');
			$Email->template('mark_spam_beat_to_admin', '');
			$Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			$replacearray = array();
			$replacearray['SENDERNAME'] = $params['name'];
			$replacearray['BEATNAME'] = $params['beat_title'];
			$replacearray['SITE_NAME'] = 'OCR Team';
			$replacearray['NAME'] = $params['name']; 
			$replacearray['BEAT_OWNER'] = $params['beat_owner_name'];
			
			$replacearray['SITE_REDIRECT_URL'] = $params['site_url_path'];
			/*===========End values to be replaced in the template==========*/
		  	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			$Email->viewVars(array('msg' => $message));
			$Email->from(array($params['from'] => $params['from']));
			$Email->from(array(NO_REPLY_EMAIL => "The On Call Room"));
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );
			try{
				if($Email->send()){
					return true;
				}else{
					return false;
				}
			}catch( Exception $e ){
				$msg = $e->getMessage();
			}
		}
	}
	
public function markSpamBeatEmailToBeatOwnerByAdmin($params = array()){
	
	if( !empty($params) ){
			$Email = new CakeEmail();
			//$Email->config('smtp');
			$Email->config('mailchimp');
			$Email->template('mark_spam_beat_to_admin', '');
			$Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			$replacearray = array();
			$replacearray['SENDERNAME'] = $params['name'];
			$replacearray['BEATNAME'] = $params['beat_title'];
			$replacearray['SITE_NAME'] = 'OCR Team';
			$replacearray['REASON'] = $params['spam_reason'];
			$replacearray['BEAT_OWNER'] = $params['beat_owner_name'];
			$replacearray['SPAM_DATE'] = $params['spam_date'];
			$replacearray['SITE_REDIRECT_URL'] = $params['site_url_path'];
			$replacearray['COMMENT'] = $params['comment'];
			$replacearray['NAME'] = $params['name']; 
			/*===========End values to be replaced in the template==========*/
		  	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			$Email->viewVars(array('msg' => $message));
			$Email->from(array($params['from'] => $params['from']));
			$Email->from(array(NO_REPLY_EMAIL => "The On Call Room"));
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );
			try{
				if($Email->send()){
					return true;
				}else{
					return false;
				}
			}catch( Exception $e ){
				$msg = $e->getMessage();
			}
		}
	
	}

	/*
	--------------------------------------------------------------------------------
	On: 16-03-2016
	I/P: $params = array()
	O/P: Message
	Desc: Sending email admin when visitor fills form and send sCV.
	--------------------------------------------------------------------------------
	*/	
	public function visitorJoinTeamToAdmin( $params = array() ){
		if( !empty($params) ){
			$Email = new CakeEmail();
			//$Email->config('smtp');
			$Email->config('mailchimp');
			$Email->template('join_team_to_admin', '');
			$Email->emailFormat('html');
			//** Replace Dynamic variables with values.
			$replacearray = array();
			$replacearray['NAME'] = $params['name'];
			$replacearray['EMAIL'] = $params['email'];
			$replacearray['MESSAGE'] = $params['message'];
			
			/*===========End values to be replaced in the template==========*/
		  	$message = $this->strreplacetextreplace($params['temp'], $replacearray);
			$Email->viewVars(array('msg' => $message));
			$Email->from(array(NO_REPLY_EMAIL => "The On Call Room"));
			$Email->to( $params['toMail'] );
			$Email->subject( $params['subject'] );
			$Email->attachments(array(
				$params['file_name'] => array(
							'file' => $params['file_path'],
							'mimetype' => $params['file_type'],
							)
				)
			);
			try{
				if($Email->send()){
					$msg = "Mail Sent";
				}else{
					$msg = "Mail Not Sent";
				}
			}catch( Exception $e ){
				$msg = "Exception Occured";
			}
		}
		return $msg;
	}


	/*
	-----------------------------------------------------------------------------------------
	On: 26-02-2019
	I/P: 
	O/P: 
	Desc: Sending Mail to admin after contact us form submition
	-----------------------------------------------------------------------------------------
	*/
	public function checkMainDrillEmails($params = array()){
		App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
		$mandrill = new Mandrill($this->mandrillKey);
		$template = "Mb_contactus";
		$template_content = array(
			array(
			'name' => $template,
			'content' => 'Contact Us'
			)
		);

		$to[]= array(
			'email'=> $params['to_mail'],
			'name' => $params['user_name'],
			'type' => 'to'
		);


		$var[]=array(
			'rcpt' => $params['to_mail'],
			'vars' => array(
				array(
					'name' => 'SUBJECT',
					'content' => $params['mailSubject']
				),
				array(
					'name' => 'NAME',
					'content' => $params['user_name']
				),
				array(
					'name' => 'EMAIL',
					'content' => $params['user_email']
				),
				array(
					'name' => 'MESSAGE',
					'content' => $params['user_message']
				),
			)
		);	
		$message = array(
		'html' => 'Contact Us',
		'text' => 'Contact Us',
		'subject' => 'Contact Us',
		'bcc_address' => 'parvinder@mediccreations.com ',
		'from_email' => $params['fromMail'],
		'from_name' => $params['fromName'],
		'to' => $to,
		"merge_vars" => $var,
		"subaccount" => MANDRILL_SUBACCOUNT,
		);
		$async = false;
		$ip_pool = 'Main Pool';
		$send_at = date('d-m-Y');
		$result = $mandrill->messages->sendTemplate($template, $template_content, $message, $async, $ip_pool, $send_at);
	 	return json_encode($result);
	 }
}
?>
