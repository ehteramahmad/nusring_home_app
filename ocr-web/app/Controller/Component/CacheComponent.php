<?php

/*
 * Cache Component
 * 
 * 
 *
 */
class CacheComponent extends Component{ 
	
	/*
	----------------------------------------------------------------------------------------------
	On: 10-11-2017
	I/P: 
	O/P: 
	Desc: Redis connection
	----------------------------------------------------------------------------------------------
	*/
	public function redisConn(){
		$returnVal = array();
		App::import('Vendor','PREDIS',array('file' => 'predis/autoload.php'));
		$redisVars = array(
		"scheme" => "tcp",
		'host'     => "172.31.24.165",
		'port'     => 6379,
		'timeout' => 0.8, // (In seconds) used to connect to a Redis server after which an exception is thrown.
		"password" => "bd88ffcb3c87c99d1fd50ece02f125fa",
		);
		$redis = new Predis\Client($redisVars);
		try {
			$msg =  $redis->ping();
			if($msg = 'PONG'){
				$returnVal = array("connection"=> true, "errors"=> "", "robj"=> $redis);
				return $returnVal;
			}
		} catch (Exception $e) {
			$msg = 'Redis Error : '.$e->getMessage();
			$returnVal = array("connection"=> false, "errors"=> $msg, "robj"=> "");
		}
		return $returnVal;
	}

	/*
	----------------------------------------------------------------------------------------------
	On: 13-11-2017
	I/P: 
	O/P: 
	Desc: Delete cache data of specified key
	----------------------------------------------------------------------------------------------
	*/
	public function delRedisKey($keyName=NULL){
		$keyDel = false;
		if(!empty($keyName)){
			$redisConn = $this->redisConn();
			if(($redisConn['connection']) && empty($redisConn['errors'])){
				if($redisConn['robj']->exists($keyName)){
					$keyDel = $redisConn['robj']->del($keyName);
				}
			}
		}
		return $keyDel;
	}

}
?>
