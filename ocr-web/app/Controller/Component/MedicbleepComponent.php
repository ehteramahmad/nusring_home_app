<?php

/*
 * Medicbleep Component
 * 
 * [Contains all related function to send subscription and presence
 * from trust admin panel and super admin panel to medicbleep version
 * v2 client]
 *
 */
class MedicbleepComponent extends Component{ 
	public function deleteOcrUser($params) {
		$platform_type = $params["platform_type"];
		$test_data = array(
		  'userId' => $params['user_id'],
		  'type' => "USER_DELETED",
		  'platformType'=> $platform_type
		);

		$query = array(
		  'query' => 'mutation ($input: UpdateUserSetting) { updateUserSetting(input: $input) { message customMessage }}',
		  'variables' => array(
		    'input' => $test_data,
		  ),
		);
		$json = json_encode($query);
		// curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		// $json = json_encode(['mutation' => $query, 'variables' => $variables]);
		$authToken = "SFMyNTY.g3QAAAACZAAEZGF0YXQAAAAHZAACYXRkAAZtYl93ZWJkAANkaWRtAAAAD2R1bW15X2RldmljZV9pZGQAAmR0ZAAFbWJ3ZWJkAANleHBiYBT4bWQAA29jcm0AAAAPZHVtbXlfb2NyX3Rva2VuZAACcmxkAAVhZG1pbmQAA3VpZG0AAAABMGQABnNpZ25lZG4GADw_OfpvAQ.V7E0wMEpm5QByIQJwsK8S1yfk7ChYmipOV8BQQHXLGg";
		$chObj = curl_init();
		curl_setopt($chObj, CURLOPT_URL, "http://api.akrd3omizk.xyz/api");
		// curl_setopt($chObj, CURLOPT_URL, "https://medicbleepv2.akrd3omizk.xyz/api");
		curl_setopt($chObj, CURLOPT_RETURNTRANSFER, true);    
		curl_setopt($chObj, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($chObj, CURLOPT_HEADER, true);
		curl_setopt($chObj, CURLOPT_VERBOSE, true);
		curl_setopt($chObj, CURLOPT_POSTFIELDS, $json);
		curl_setopt($chObj, CURLOPT_HTTPHEADER,
		     array(
		            'User-Agent: PHP Script',
		            'Content-Type: application/json;charset=utf-8',
		            'Authorization: Bearer '.$authToken
		        )
		    ); 

		$response = curl_exec($chObj);
		return $response;
	}

	public function userForceLogout($params) {
		$platform_type = $params["platform_type"];
		$test_data = array(
		  'userId' => $params['user_id'],
		  'type' => "FORCE_LOGOUT",
		  'platformType'=> $platform_type
		);

		$query = array(
		  'query' => 'mutation ($input: UpdateUserSetting) { updateUserSetting(input: $input) { message customMessage }}',
		  'variables' => array(
		    'input' => $test_data,
		  ),
		);
		$json = json_encode($query);
		// curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		// $json = json_encode(['mutation' => $query, 'variables' => $variables]);
		$authToken = "SFMyNTY.g3QAAAACZAAEZGF0YXQAAAAHZAACYXRkAAZtYl93ZWJkAANkaWRtAAAAD2R1bW15X2RldmljZV9pZGQAAmR0ZAAFbWJ3ZWJkAANleHBiYBzVLWQAA29jcm0AAAAPZHVtbXlfb2NyX3Rva2VuZAACcmxkAAVhZG1pbmQAA3VpZG0AAAABMGQABnNpZ25lZG4GAASN7xhwAQ.sr06BqWX-GlsUPQZNHYVVMTW7CtrbzLaRZuNAxQo91k";
		$chObj = curl_init();
		curl_setopt($chObj, CURLOPT_URL, "http://api.akrd3omizk.xyz/api");
		// curl_setopt($chObj, CURLOPT_URL, "https://medicbleepv2.akrd3omizk.xyz/api");
		curl_setopt($chObj, CURLOPT_RETURNTRANSFER, true);    
		curl_setopt($chObj, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($chObj, CURLOPT_HEADER, true);
		curl_setopt($chObj, CURLOPT_VERBOSE, true);
		curl_setopt($chObj, CURLOPT_POSTFIELDS, $json);
		curl_setopt($chObj, CURLOPT_HTTPHEADER,
		     array(
		            'User-Agent: PHP Script',
		            'Content-Type: application/json;charset=utf-8',
		            'Authorization: Bearer '.$authToken
		        )
		    ); 

		$response = curl_exec($chObj);
		return $response;
	}

	public function deactivateOcrUser($params) {
		$platform_type = $params["platform_type"];        
		$test_data = array(
		  'userId' => $params['user_id'],
		  'type' => "USER_DEACTIVATED",
		  'platformType'=> $platform_type
		);

		$query = array(
		  'query' => 'mutation ($input: UpdateUserSetting) { updateUserSetting(input: $input) { message customMessage }}',
		  'variables' => array(
		    'input' => $test_data,
		  ),
		);
		$json = json_encode($query);
		// curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		// $json = json_encode(['mutation' => $query, 'variables' => $variables]);
		$authToken = "SFMyNTY.g3QAAAACZAAEZGF0YXQAAAAHZAACYXRkAAZtYl93ZWJkAANkaWRtAAAAD2R1bW15X2RldmljZV9pZGQAAmR0ZAAFbWJ3ZWJkAANleHBiYBT4bWQAA29jcm0AAAAPZHVtbXlfb2NyX3Rva2VuZAACcmxkAAVhZG1pbmQAA3VpZG0AAAABMGQABnNpZ25lZG4GADw_OfpvAQ.V7E0wMEpm5QByIQJwsK8S1yfk7ChYmipOV8BQQHXLGg";
		$chObj = curl_init();
		curl_setopt($chObj, CURLOPT_URL, "http://api.akrd3omizk.xyz/api");
		// curl_setopt($chObj, CURLOPT_URL, "https://medicbleepv2.akrd3omizk.xyz/api");
		curl_setopt($chObj, CURLOPT_RETURNTRANSFER, true);    
		curl_setopt($chObj, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($chObj, CURLOPT_HEADER, true);
		curl_setopt($chObj, CURLOPT_VERBOSE, true);
		curl_setopt($chObj, CURLOPT_POSTFIELDS, $json);
		curl_setopt($chObj, CURLOPT_HTTPHEADER,
		     array(
		            'User-Agent: PHP Script',
		            'Content-Type: application/json;charset=utf-8',
		            'Authorization: Bearer '.$authToken
		        )
		    ); 

		$response = curl_exec($chObj);
		return $response;
	}

	public function userUnapproved($params) {
		$platform_type = $params["platform_type"];
		$test_data = array(
		  'userId' => $params['user_id'],
		  'type' => "USER_UNAPPROVED",
		  'platformType'=> $platform_type
		);

		$query = array(
		  'query' => 'mutation ($input: UpdateUserSetting) { updateUserSetting(input: $input) { message customMessage }}',
		  'variables' => array(
		    'input' => $test_data,
		  ),
		);
		$json = json_encode($query);
		// curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		// $json = json_encode(['mutation' => $query, 'variables' => $variables]);
		$authToken = "SFMyNTY.g3QAAAACZAAEZGF0YXQAAAAHZAACYXRkAAZtYl93ZWJkAANkaWRtAAAAD2R1bW15X2RldmljZV9pZGQAAmR0ZAAFbWJ3ZWJkAANleHBiYBT4bWQAA29jcm0AAAAPZHVtbXlfb2NyX3Rva2VuZAACcmxkAAVhZG1pbmQAA3VpZG0AAAABMGQABnNpZ25lZG4GADw_OfpvAQ.V7E0wMEpm5QByIQJwsK8S1yfk7ChYmipOV8BQQHXLGg";
		$chObj = curl_init();
		curl_setopt($chObj, CURLOPT_URL, "http://api.akrd3omizk.xyz/api");
		// curl_setopt($chObj, CURLOPT_URL, "https://medicbleepv2.akrd3omizk.xyz/api");
		curl_setopt($chObj, CURLOPT_RETURNTRANSFER, true);    
		curl_setopt($chObj, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($chObj, CURLOPT_HEADER, true);
		curl_setopt($chObj, CURLOPT_VERBOSE, true);
		curl_setopt($chObj, CURLOPT_POSTFIELDS, $json);
		curl_setopt($chObj, CURLOPT_HTTPHEADER,
		     array(
		            'User-Agent: PHP Script',
		            'Content-Type: application/json;charset=utf-8',
		            'Authorization: Bearer '.$authToken
		        )
		    ); 

		$response = curl_exec($chObj);
		return $response;
	}

	public function changeAvailableAndOncallStatus($params) {    
		if($params['available'] == "1")
		{
			$available = true;
		}
		else
		{
			$available = false;
		}
		if($params['on_call'] == "1")
		{
			$oncall = true;
		}
		else
		{
			$oncall = false;
		}
		$subscriptionData = array(
		  'userId' => $params['user_id'],
		  'type' => "CHANGE_AVAILABLE_STATUS",
		  'isAvailable'=> $available,
		  'isOnCall'=> $oncall,
		  'institutionId'=> $params['institution_id'],
		  'applicationType'=> "MB"
		);

		$query = array(
		  'query' => 'mutation ($input: OcrTrustAdminActivityInput) { ocrTrustAdminActivity(input: $input) { message }}',
		  'variables' => array(
		    'input' => $subscriptionData,
		  ),
		);
		$json = json_encode($query);
		$authToken = "SFMyNTY.g3QAAAACZAAEZGF0YXQAAAAHZAACYXRkAAZtYl93ZWJkAANkaWRtAAAAD2R1bW15X2RldmljZV9pZGQAAmR0ZAAFbWJ3ZWJkAANleHBiYB4qDWQAA29jcm0AAAAPZHVtbXlfb2NyX3Rva2VuZAACcmxkAAVhZG1pbmQAA3VpZG0AAAABMGQABnNpZ25lZG4GAIQaIx5wAQ.7h-m1MfkaS_NOjhsUw_eLM6CnKsu5RY5umW55Pi60vw";
		$chObj = curl_init();
		curl_setopt($chObj, CURLOPT_URL, "http://api.akrd3omizk.xyz/api");
		// curl_setopt($chObj, CURLOPT_URL, "https://medicbleepv2.akrd3omizk.xyz/api");
		curl_setopt($chObj, CURLOPT_RETURNTRANSFER, true);    
		curl_setopt($chObj, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($chObj, CURLOPT_HEADER, true);
		curl_setopt($chObj, CURLOPT_VERBOSE, true);
		curl_setopt($chObj, CURLOPT_POSTFIELDS, $json);
		curl_setopt($chObj, CURLOPT_HTTPHEADER,
		     array(
		            'User-Agent: PHP Script',
		            'Content-Type: application/json;charset=utf-8',
		            'Authorization: Bearer '.$authToken
		        )
		    ); 

		$response = curl_exec($chObj);
		return $response;
}

	public function changeUserContactNumber($params) {
		        
				$subscriptionData = array(
				  'userId' => $params['user_id'],
				  'type' => "CHANGE_CONTACT",
				  'mobileNo'=> $params['contact_no'],
				  'institutionId'=> $params['institution_id'],
				  'applicationType'=> "MB"

				);

				$query = array(
				  'query' => 'mutation ($input: OcrTrustAdminActivityInput) { ocrTrustAdminActivity(input: $input) { message }}',
				  'variables' => array(
				    'input' => $subscriptionData,
				  ),
				);
				$json = json_encode($query);
				$authToken = "SFMyNTY.g3QAAAACZAAEZGF0YXQAAAAHZAACYXRkAAZtYl93ZWJkAANkaWRtAAAAD2R1bW15X2RldmljZV9pZGQAAmR0ZAAFbWJ3ZWJkAANleHBiYBPGg2QAA29jcm0AAAAPZHVtbXlfb2NyX3Rva2VuZAACcmxkAAVhZG1pbmQAA3VpZG0AAAABMGQABnNpZ25lZG4GALxFjvVvAQ.PvNyo13JdGzBVD2vi4v1_ALbZ4cZVPZVhZkphRqtaSo";
				$chObj = curl_init();
				curl_setopt($chObj, CURLOPT_URL, "http://api.akrd3omizk.xyz/api");
				// curl_setopt($chObj, CURLOPT_URL, "https://medicbleepv2.akrd3omizk.xyz/api");
				curl_setopt($chObj, CURLOPT_RETURNTRANSFER, true);    
				curl_setopt($chObj, CURLOPT_CUSTOMREQUEST, 'POST');
				curl_setopt($chObj, CURLOPT_HEADER, true);
				curl_setopt($chObj, CURLOPT_VERBOSE, true);
				curl_setopt($chObj, CURLOPT_POSTFIELDS, $json);
				curl_setopt($chObj, CURLOPT_HTTPHEADER,
				     array(
				            'User-Agent: PHP Script',
				            'Content-Type: application/json;charset=utf-8',
				            'Authorization: Bearer '.$authToken
				        )
				    ); 

				$response = curl_exec($chObj);
				return $response;
		}

	public function changeUserName($params) {
		        
				$subscriptionData = array(
				  'userId' => $params['user_id'],
				  'type' => "CHANGE_USER_NAME",
				  'firstName'=> $params['first_name'],
				  'lastName'=> $params['last_name'],
				  'institutionId'=> $params['institution_id'],
				  'applicationType'=> "MB"
				);

				$query = array(
				  'query' => 'mutation ($input: OcrTrustAdminActivityInput) { ocrTrustAdminActivity(input: $input) { message }}',
				  'variables' => array(
				    'input' => $subscriptionData,
				  ),
				);
				$json = json_encode($query);
				$authToken = "SFMyNTY.g3QAAAACZAAEZGF0YXQAAAAHZAACYXRkAAZtYl93ZWJkAANkaWRtAAAAD2R1bW15X2RldmljZV9pZGQAAmR0ZAAFbWJ3ZWJkAANleHBiYBTrY2QAA29jcm0AAAAPZHVtbXlfb2NyX3Rva2VuZAACcmxkAAVhZG1pbmQAA3VpZG0AAAABMGQABnNpZ25lZG4GABpQBvpvAQ.2h3doM4sVzNvXio1zyx40UuPxnHYY-8R27Sa4jEiIhs";
				$chObj = curl_init();
				curl_setopt($chObj, CURLOPT_URL, "http://api.akrd3omizk.xyz/api");
				// curl_setopt($chObj, CURLOPT_URL, "https://medicbleepv2.akrd3omizk.xyz/api");
				curl_setopt($chObj, CURLOPT_RETURNTRANSFER, true);    
				curl_setopt($chObj, CURLOPT_CUSTOMREQUEST, 'POST');
				curl_setopt($chObj, CURLOPT_HEADER, true);
				curl_setopt($chObj, CURLOPT_VERBOSE, true);
				curl_setopt($chObj, CURLOPT_POSTFIELDS, $json);
				curl_setopt($chObj, CURLOPT_HTTPHEADER,
				     array(
				            'User-Agent: PHP Script',
				            'Content-Type: application/json;charset=utf-8',
				            'Authorization: Bearer '.$authToken
				        )
				    ); 

				$response = curl_exec($chObj);
				return $response;
		}

	public function userApproved($params) {

		if($params['is_approved'] == "1")
		{
			$is_approved = true;
		}
		else
		{
			$is_approved = false;
		}
		        
		$test_data = array(
		  'userId' => $params['user_id'],
		  'type' => "USER_APPROVED",
		  'isApproved' => $is_approved,
		  'institutionId' => $params['institution_id'],
		  'applicationType'=> "MB"
		);

		$query = array(
		  'query' => 'mutation ($input: OcrTrustAdminActivityInput) { ocrTrustAdminActivity(input: $input) { message customMessage }}',
		  'variables' => array(
		    'input' => $test_data,
		  ),
		);
		$json = json_encode($query);
		// curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		// $json = json_encode(['mutation' => $query, 'variables' => $variables]);
		$authToken = "SFMyNTY.g3QAAAACZAAEZGF0YXQAAAAHZAACYXRkAAZtYl93ZWJkAANkaWRtAAAAD2R1bW15X2RldmljZV9pZGQAAmR0ZAAFbWJ3ZWJkAANleHBiYCYQGGQAA29jcm0AAAAPZHVtbXlfb2NyX3Rva2VuZAACcmxkAAVhZG1pbmQAA3VpZG0AAAABMGQABnNpZ25lZG4GAJCy_TxwAQ.qkcCrOtEppvpoIShqooQA0X9CHrutbH7JURBTZgD0wk";
		$chObj = curl_init();
		curl_setopt($chObj, CURLOPT_URL, "http://api.akrd3omizk.xyz/api");
		// curl_setopt($chObj, CURLOPT_URL, "https://medicbleepv2.akrd3omizk.xyz/api");
		curl_setopt($chObj, CURLOPT_RETURNTRANSFER, true);    
		curl_setopt($chObj, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($chObj, CURLOPT_HEADER, true);
		curl_setopt($chObj, CURLOPT_VERBOSE, true);
		curl_setopt($chObj, CURLOPT_POSTFIELDS, $json);
		curl_setopt($chObj, CURLOPT_HTTPHEADER,
		     array(
		            'User-Agent: PHP Script',
		            'Content-Type: application/json;charset=utf-8',
		            'Authorization: Bearer '.$authToken
		        )
		    ); 

		$response = curl_exec($chObj);
		return $response;
	}


	public function addUserInDirectory($params) {

		if($params['at_work'] == "1")
		{
			$at_work = true;
		}
		else
		{
			$at_work = false;
		}
		$on_call = false;
		$is_dnd_active = false;
		$on_call_access = true;
		$custom_role_value = isset($params['custom_role_value']) ? "'".$params['custom_role_value']."'" : "";
		$custom_role_key = isset($params['custom_role_key']) ? $params['custom_role_key'] : "";
		$permanentRolesData =$params['custom_permanent_role_new'];
		$test_data = array(
		  'userId' => isset($params['user_id']) ? $params['user_id'] : "",
		  'type' => "INSTITUTION_CHANGED",
		  'institutionChanged' => 'added',
		  'institutionId' => isset($params['institution_id']) ? $params['institution_id'] : "",
		  'users' => array(
		  	'countryCode' => "IN",
		  	'dynamic' =>array(
		  		'atWork' => $at_work,
		  		'batonRoles' => [],
		  		'dndStatus' => array(
		  			'duration'=>"",
		  			'emergencyContact' => array(
		  				'note'=>"",
		  				'userId'=> "",
		  				'userName'=>""
		  				),
		  			'endTime'=> "",
		            'iconSelected'=> "",
		            'iconUnselected'=> "",
		            'id'=> '0',
		            'name'=> "",
		            'selectedDuration'=> ""
		  			),
		  		'isDndActive' => $is_dnd_active,
		  		'onCall' => $on_call,
		  		'permanentRoles' => $permanentRolesData,
		  		'profileImg' => isset($params['profile_img']) ? $params['profile_img'] : "",
		  		'roleStatus' => isset($params['role_status']) ? $params['role_status'] : "",
		  		'roleTags' => array(
		  			'key' =>$custom_role_key,
		  			'values' => [$custom_role_value],
		  			),
		  		'thumbnailImg' => isset($params['thumbnail_img']) ? $params['thumbnail_img'] : ""
		  		),
		  		'email' => isset($params['email']) ? $params['email'] : "",
		  		'firstName' => isset($params['first_name']) ? $params['first_name'] : "",
		  		'id' => isset($params['user_id']) ? $params['user_id'] : "",
			  	'lastName' => isset($params['last_name']) ? $params['last_name'] : "",
			  	'onCallAccess' => $on_call_access,
			  	'profession' => array(
		  			'type' =>  isset($params['profession_type']) ? $params['profession_type'] : ""
		  		),
		  	),
		);
		// return $test_data;

		$query = array(
		  'query' => 'mutation ($input: AddUserInDirectoryInput) { addUserInDirectory(input: $input) { message }}',
		  'variables' => array(
		    'input' => $test_data,
		  ),
		);
		$json = json_encode($query);
		// curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		// $json = json_encode(['mutation' => $query, 'variables' => $variables]);
		$authToken = "SFMyNTY.g3QAAAACZAAEZGF0YXQAAAAHZAACYXRkAAZtYl93ZWJkAANkaWRtAAAAD2R1bW15X2RldmljZV9pZGQAAmR0ZAAFbWJ3ZWJkAANleHBiYCyuFGQAA29jcm0AAAAPZHVtbXlfb2NyX3Rva2VuZAACcmxkAAVhZG1pbmQAA3VpZG0AAAABMGQABnNpZ25lZG4GAKzU1lZwAQ.Ft0xEOzizJwSXm235djInQuuFD7WcYwRtDBoI3um0rY";
		$chObj = curl_init();
		curl_setopt($chObj, CURLOPT_URL, "http://api.akrd3omizk.xyz/api");
		// curl_setopt($chObj, CURLOPT_URL, "https://medicbleepv2.akrd3omizk.xyz/api");
		curl_setopt($chObj, CURLOPT_RETURNTRANSFER, true);    
		curl_setopt($chObj, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($chObj, CURLOPT_HEADER, true);
		curl_setopt($chObj, CURLOPT_VERBOSE, true);
		curl_setopt($chObj, CURLOPT_POSTFIELDS, $json);
		curl_setopt($chObj, CURLOPT_HTTPHEADER,
		     array(
		            'User-Agent: PHP Script',
		            'Content-Type: application/json;charset=utf-8',
		            'Authorization: Bearer '.$authToken
		        )
		    ); 

		$response = curl_exec($chObj);
		return $response;
	}


	public function leaveUserInDirectory($param) {
		// return $params;
		$at_work = false;
		$on_call = false;
		$is_dnd_active = false;
		$on_call_access = true;
		$test_data = array(
		  'userId' => $param['user_id'],
		  'type' => "USER_LEFT_INSTITUTION",
		  'institutionChanged' => 'leave',
		  'institutionId' => (string)$param['p_id']
		);

		$query = array(
		  'query' => 'mutation ($input: RemoveUserFromDirectoryInput) { removeUserFromDirectory(input: $input) { message }}',
		  'variables' => array(
		    'input' => $test_data,
		  ),
		);
		$json = json_encode($query);
		// return $json;
		// curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		// $json = json_encode(['mutation' => $query, 'variables' => $variables]);
		$authToken = "SFMyNTY.g3QAAAACZAAEZGF0YXQAAAAHZAACYXRkAAZtYl93ZWJkAANkaWRtAAAAD2R1bW15X2RldmljZV9pZGQAAmR0ZAAFbWJ3ZWJkAANleHBiYCyuFGQAA29jcm0AAAAPZHVtbXlfb2NyX3Rva2VuZAACcmxkAAVhZG1pbmQAA3VpZG0AAAABMGQABnNpZ25lZG4GAKzU1lZwAQ.Ft0xEOzizJwSXm235djInQuuFD7WcYwRtDBoI3um0rY";
		$chObj = curl_init();
		curl_setopt($chObj, CURLOPT_URL, "http://api.akrd3omizk.xyz/api");
		// curl_setopt($chObj, CURLOPT_URL, "https://medicbleepv2.akrd3omizk.xyz/api");
		curl_setopt($chObj, CURLOPT_RETURNTRANSFER, true);    
		curl_setopt($chObj, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($chObj, CURLOPT_HEADER, true);
		curl_setopt($chObj, CURLOPT_VERBOSE, true);
		curl_setopt($chObj, CURLOPT_POSTFIELDS, $json);
		curl_setopt($chObj, CURLOPT_HTTPHEADER,
		     array(
		            'User-Agent: PHP Script',
		            'Content-Type: application/json;charset=utf-8',
		            'Authorization: Bearer '.$authToken
		        )
		    ); 

		$response = curl_exec($chObj);
		return $response;
	}

	public function userDndPresence($params) {
		$params['is_dnd_enabled'] = false;
		$at_work = false;
		$subscriptionData = array(
		  'type' => 'DISMISS_DND',
		  'userId' => $params['user_id'],
		  'isDndEnabled'=> $params['is_dnd_enabled'],
		  'applicationType'=> "MB",
		  'institutionId'=> $params['institution_id'],
	  	  'dndStatus' => array(
	  			'duration'=>null,
	  			'emergencyContact' => array(
	  				'note'=>null,
	  				'userId'=> null,
	  				'userName'=>null
	  				),
	  			'endTime'=> null,
	            'iconSelected'=> null,
	            'iconUnselected'=> null,
	            'id'=> '0',
	            'name'=> null,
	            'selectedDuration'=> null
	  		)
		);

		$query = array(
		  'query' => 'mutation ($input: OcrCronInput) { ocrCron(input: $input) { message }}',
		  'variables' => array(
		    'input' => $subscriptionData,
		  ),
		);
		$json = json_encode($query);
		$authToken = "SFMyNTY.g3QAAAACZAAEZGF0YXQAAAAHZAACYXRkAAZtYl93ZWJkAANkaWRtAAAAD2R1bW15X2RldmljZV9pZGQAAmR0ZAAFbWJ3ZWJkAANleHBiYBTrY2QAA29jcm0AAAAPZHVtbXlfb2NyX3Rva2VuZAACcmxkAAVhZG1pbmQAA3VpZG0AAAABMGQABnNpZ25lZG4GABpQBvpvAQ.2h3doM4sVzNvXio1zyx40UuPxnHYY-8R27Sa4jEiIhs";
		$chObj = curl_init();
		curl_setopt($chObj, CURLOPT_URL, "http://api.akrd3omizk.xyz/api");
		// curl_setopt($chObj, CURLOPT_URL, "https://medicbleepv2.akrd3omizk.xyz/api");
		curl_setopt($chObj, CURLOPT_RETURNTRANSFER, true);    
		curl_setopt($chObj, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($chObj, CURLOPT_HEADER, true);
		curl_setopt($chObj, CURLOPT_VERBOSE, true);
		curl_setopt($chObj, CURLOPT_POSTFIELDS, $json);
		curl_setopt($chObj, CURLOPT_HTTPHEADER,
		     array(
		            'User-Agent: PHP Script',
		            'Content-Type: application/json;charset=utf-8',
		            'Authorization: Bearer '.$authToken
		        )
		    ); 

		$response = curl_exec($chObj);
		return $response;
	}

	public function batonRoleExchange($params) {
		if($params['is_qr_request'] == "1")
		{
			$is_qr_request = true;
		}
		else
		{
			$is_qr_request = false;
		}

        $params['baton_role_status'] = $params["baton_role_status"];
        $params['platform_type'] = $params["platform_type"];
        $params['baton_custom_message'] = isset($params["baton_custom_message"]) ? $params["baton_custom_message"] : "Baton Role exchange";
		$subscriptionData = array(
		  'userId' => $params['user_id'],
		  'type' => "BATON_ROLE_EXCHANGE",
		  'batonRolesStatus'=> $params['baton_role_status'],
		  'platformType'=> $params['platform_type'],
		  'isQrRequest'=> $is_qr_request,
		  'batonCustomMessage'=> $params['baton_custom_message']
		);

		$query = array(
		  'query' => 'mutation ($input:  ManageBatonRoleInput) { manageBatonRole(input: $input) { message }}',
		  'variables' => array(
		    'input' => $subscriptionData,
		  ),
		);
		$json = json_encode($query);
		$authToken = "SFMyNTY.g3QAAAACZAAEZGF0YXQAAAAHZAACYXRkAAZtYl93ZWJkAANkaWRtAAAAD2R1bW15X2RldmljZV9pZGQAAmR0ZAAFbWJ3ZWJkAANleHBiYDh4M2QAA29jcm0AAAAPZHVtbXlfb2NyX3Rva2VuZAACcmxkAAVhZG1pbmQAA3VpZG0AAAABMGQABnNpZ25lZG4GALNc5IRwAQ.0vfAanxPG71euN--fHI_DFkzofGChTSLKx3wcjlW6kM";
		$chObj = curl_init();
		curl_setopt($chObj, CURLOPT_URL, "http://api.akrd3omizk.xyz/api");
		// curl_setopt($chObj, CURLOPT_URL, "https://medicbleepv2.akrd3omizk.xyz/api");
		curl_setopt($chObj, CURLOPT_RETURNTRANSFER, true);    
		curl_setopt($chObj, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($chObj, CURLOPT_HEADER, true);
		curl_setopt($chObj, CURLOPT_VERBOSE, true);
		curl_setopt($chObj, CURLOPT_POSTFIELDS, $json);
		curl_setopt($chObj, CURLOPT_HTTPHEADER,
		     array(
		            'User-Agent: PHP Script',
		            'Content-Type: application/json;charset=utf-8',
		            'Authorization: Bearer '.$authToken
		        )
		    ); 

		$response = curl_exec($chObj);
		return $response;
	}

	public function permanentRoleExchange($params) {
        $params['permanent_role_status'] = $params["permanent_role_status"];
        $params['platform_type'] = $params["platform_type"];
        $params['permanent_custom_message'] = isset($params["permanent_custom_message"]) ? $params["permanent_custom_message"] : "Permanent Role exchange";
		$subscriptionData = array(
		  'userId' => $params['user_id'],
		  'type' => "PERMANENT_ROLE_EXCHANGE",
		  'permanentRolesStatus'=> $params['permanent_role_status'],
		  'platformType'=> $params['platform_type'],
		  'permanentCustomMessage'=> $params['permanent_custom_message']
		);

		$query = array(
		  'query' => 'mutation ($input:  ManagePermanentRoleInput) { managePermanentRole(input: $input) { message }}',
		  'variables' => array(
		    'input' => $subscriptionData,
		  ),
		);
		$json = json_encode($query);
		$authToken = "SFMyNTY.g3QAAAACZAAEZGF0YXQAAAAHZAACYXRkAAZtYl93ZWJkAANkaWRtAAAAD2R1bW15X2RldmljZV9pZGQAAmR0ZAAFbWJ3ZWJkAANleHBiYDi8qWQAA29jcm0AAAAPZHVtbXlfb2NyX3Rva2VuZAACcmxkAAVhZG1pbmQAA3VpZG0AAAABMGQABnNpZ25lZG4GAATL74VwAQ.NF7XLe2KvbbMUaOf6eUcnEEb2MgxzoeFxmOSmetnOtY";
		$chObj = curl_init();
		curl_setopt($chObj, CURLOPT_URL, "http://api.akrd3omizk.xyz/api");
		// curl_setopt($chObj, CURLOPT_URL, "https://medicbleepv2.akrd3omizk.xyz/api");
		curl_setopt($chObj, CURLOPT_RETURNTRANSFER, true);    
		curl_setopt($chObj, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($chObj, CURLOPT_HEADER, true);
		curl_setopt($chObj, CURLOPT_VERBOSE, true);
		curl_setopt($chObj, CURLOPT_POSTFIELDS, $json);
		curl_setopt($chObj, CURLOPT_HTTPHEADER,
		     array(
		            'User-Agent: PHP Script',
		            'Content-Type: application/json;charset=utf-8',
		            'Authorization: Bearer '.$authToken
		        )
		    ); 

		$response = curl_exec($chObj);
		return $response;
	}

	public function getDialogList($params) {
		$token = "SFMyNTY.g3QAAAACZAAEZGF0YXQAAAAHZAACYXRkAAZtYl93ZWJkAANkaWRtAAAAD2R1bW15X2RldmljZV9pZGQAAmR0ZAAFbWJ3ZWJkAANleHBiYDi8qWQAA29jcm0AAAAPZHVtbXlfb2NyX3Rva2VuZAACcmxkAAVhZG1pbmQAA3VpZG0AAAABMGQABnNpZ25lZG4GAATL74VwAQ.NF7XLe2KvbbMUaOf6eUcnEEb2MgxzoeFxmOSmetnOtY";
		$query = array(
		  'query' => GET_DIALOGS,
		  'variables' => array(
		    'input' => $params,
		  ),
		);
		$json = json_encode($query);
		$authToken = $token;
		$chObj = curl_init();
		curl_setopt($chObj, CURLOPT_URL, MEDIC_BLEEP_API_ENDPOINT);
		curl_setopt($chObj, CURLOPT_RETURNTRANSFER, true);    
		curl_setopt($chObj, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($chObj, CURLOPT_HEADER, true);
		curl_setopt($chObj, CURLOPT_VERBOSE, true);
		curl_setopt($chObj, CURLOPT_HEADER, false);
		curl_setopt($chObj, CURLOPT_POSTFIELDS, $json);
		curl_setopt($chObj, CURLOPT_HTTPHEADER,
		     array(
		            'User-Agent: PHP Script',
		            'Content-Type: application/json;charset=utf-8',
		            'Authorization: Bearer '.$authToken
		        )
		    ); 
		
		$response = curl_exec($chObj);
		return $response;
	}

	public function getDialogDetail($id) {
		$token = "SFMyNTY.g3QAAAACZAAEZGF0YXQAAAAHZAACYXRkAAZtYl93ZWJkAANkaWRtAAAAD2R1bW15X2RldmljZV9pZGQAAmR0ZAAFbWJ3ZWJkAANleHBiYDi8qWQAA29jcm0AAAAPZHVtbXlfb2NyX3Rva2VuZAACcmxkAAVhZG1pbmQAA3VpZG0AAAABMGQABnNpZ25lZG4GAATL74VwAQ.NF7XLe2KvbbMUaOf6eUcnEEb2MgxzoeFxmOSmetnOtY";
		$query = array(
		  'query' => GET_DIALOG,
		  'variables' => array(
		    'id' => $id,
		  ),
		);
		$json = json_encode($query);
		$authToken = $token;
		$chObj = curl_init();
		curl_setopt($chObj, CURLOPT_URL, MEDIC_BLEEP_API_ENDPOINT);
		curl_setopt($chObj, CURLOPT_RETURNTRANSFER, true);    
		curl_setopt($chObj, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($chObj, CURLOPT_HEADER, true);
		curl_setopt($chObj, CURLOPT_VERBOSE, true);
		curl_setopt($chObj, CURLOPT_HEADER, false);
		curl_setopt($chObj, CURLOPT_POSTFIELDS, $json);
		curl_setopt($chObj, CURLOPT_HTTPHEADER,
		     array(
		            'User-Agent: PHP Script',
		            'Content-Type: application/json;charset=utf-8',
		            'Authorization: Bearer '.$authToken
		        )
		    ); 
		
		$response = curl_exec($chObj);
		return $response;
	}

	public function getMessageList($params) {
		$token = "SFMyNTY.g3QAAAACZAAEZGF0YXQAAAAHZAACYXRkAAZtYl93ZWJkAANkaWRtAAAAD2R1bW15X2RldmljZV9pZGQAAmR0ZAAFbWJ3ZWJkAANleHBiYDi8qWQAA29jcm0AAAAPZHVtbXlfb2NyX3Rva2VuZAACcmxkAAVhZG1pbmQAA3VpZG0AAAABMGQABnNpZ25lZG4GAATL74VwAQ.NF7XLe2KvbbMUaOf6eUcnEEb2MgxzoeFxmOSmetnOtY";
		$inputVariables = array(
		  'institutionId' => $params['institutionId'],
		  'dialogId' => $params['dialogId'],
		  "after" => $params['after'],
		  "before" => $params['before'],
		  'order' => 'ASC'
		);
		$query = array(
		  'query' => GET_MESSAGES,
		  'variables' => array(
		    'input' => $inputVariables,
		  ),
		);
		$json = json_encode($query);
		$authToken = $token;
		$chObj = curl_init();
		curl_setopt($chObj, CURLOPT_URL, MEDIC_BLEEP_API_ENDPOINT);
		curl_setopt($chObj, CURLOPT_RETURNTRANSFER, true);    
		curl_setopt($chObj, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($chObj, CURLOPT_HEADER, true);
		curl_setopt($chObj, CURLOPT_VERBOSE, true);
		curl_setopt($chObj, CURLOPT_HEADER, false);
		curl_setopt($chObj, CURLOPT_POSTFIELDS, $json);
		curl_setopt($chObj, CURLOPT_HTTPHEADER,
		     array(
		            'User-Agent: PHP Script',
		            'Content-Type: application/json;charset=utf-8',
		            'Authorization: Bearer '.$authToken
		        )
		    ); 
		
		$response = curl_exec($chObj);
		return $response;
	}

	// public function groupEventsAnalytics() {
	// 	$subscriptionData = array(
	// 	  'institutionId' => "86",
	// 	  'after' => "2020-03-25T11:48:14.368762Z",
	// 	  'before'=> "2020-03-27T11:48:14.368762Z"
	// 	);

	// 	$query = array(
	// 	  'query' => 'query ($subscriptionData) { groupEventsAnalytics(input: $input) { applicationType
	// 				    fromUserId
	// 				    id
	// 				    institutionId
	// 				    occupantsIds
	// 				    patientInfo
	// 				    type
	// 				    name }}',
	// 	  'variables' => $subscriptionData,
	// 	);
	// 	$json = json_encode($query);
	// 	$authToken = "SFMyNTY.g3QAAAACZAAEZGF0YXQAAAAHZAACYXRkAAZtYl93ZWJkAANkaWRtAAAAD2R1bW15X2RldmljZV9pZGQAAmR0ZAAFbWJ3ZWJkAANleHBiYDi8qWQAA29jcm0AAAAPZHVtbXlfb2NyX3Rva2VuZAACcmxkAAVhZG1pbmQAA3VpZG0AAAABMGQABnNpZ25lZG4GAATL74VwAQ.NF7XLe2KvbbMUaOf6eUcnEEb2MgxzoeFxmOSmetnOtY";
	// 	$chObj = curl_init();
		// curl_setopt($chObj, CURLOPT_URL, "http://api.akrd3omizk.xyz/api");
		// curl_setopt($chObj, CURLOPT_URL, "https://medicbleepv2.akrd3omizk.xyz/api");
	// 	curl_setopt($chObj, CURLOPT_RETURNTRANSFER, true);    
	// 	curl_setopt($chObj, CURLOPT_CUSTOMREQUEST, 'POST');
	// 	curl_setopt($chObj, CURLOPT_HEADER, true);
	// 	curl_setopt($chObj, CURLOPT_VERBOSE, true);
	// 	curl_setopt($chObj, CURLOPT_POSTFIELDS, $json);
	// 	curl_setopt($chObj, CURLOPT_HTTPHEADER,
	// 	     array(
	// 	            'User-Agent: PHP Script',
	// 	            'Content-Type: application/json;charset=utf-8',
	// 	            'Authorization: Bearer '.$authToken
	// 	        )
	// 	    ); 

	// 	$response = curl_exec($chObj);
	// 	return $response;
	// }


}
?>
