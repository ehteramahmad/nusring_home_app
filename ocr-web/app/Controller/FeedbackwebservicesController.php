<?php
/*
 * Feedback controller.
 *
 * This file will render views from views/Feedbackwebservices/
 *
 
 */

App::uses('AppController', 'Controller');


class FeedbackwebservicesController extends AppController {
	public $uses = array('User','UserFeedback');
	public $components = array('Common');
	
	/*
	-------------------------------------------------------------------------
	ON: 02-12-2015
	I/P: JSON (user_id, feedback content)
	O/P: JSON (success/fail)
	Desc: User can send feedback.
	-------------------------------------------------------------------------
	*/
	public function addUserFeedback(){
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true);
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				if( $this->User->find("count", array("conditions"=> array("User.id"=>$dataInput['user_id']))) > 0){
					//** Add feedback
					$userFeedbackData = array("user_id"=> $dataInput['user_id'], "feedback"=> $dataInput['feedback']);
					try{
						if($this->UserFeedback->save( $userFeedbackData )){
							$responseData = array('method_name'=> 'addUserFeedback', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200);
						}else{
							$responseData = array('method_name'=> 'addUserFeedback', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615);
						}
					}catch( Exception $e ){
						$responseData = array('method_name'=> 'addUserFeedback', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
					}
				}else{
					$responseData = array('method_name'=> 'addUserFeedback', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
				}
			}else{
				$responseData = array('method_name'=> 'addUserFeedback', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'addUserFeedback', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}
}
