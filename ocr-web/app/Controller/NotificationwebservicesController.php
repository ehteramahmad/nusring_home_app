<?php
/*
 * Notification controller.
 *
 * This file will render views from views/Notificationwebservices/
 *
 
 */

App::uses('AppController', 'Controller');

class NotificationwebservicesController extends AppController {
	public $uses = array('User', 'NotificationUser', 'NotificationLog', 'UserNotificationSetting', 'UserPost', 'NotificationLastVisit');
	public $components = array('Common');

	/*
	---------------------------------------------------------------------------------------
	On: 06-11-2015
	I/P:
	O/P:
	Desc: Stores user id with device id and device registration for GCM Notification
	--------------------------------------------------------------------------------------- 
	*/
	public function addUserDevice(){
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true);
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				if( $this->User->find("count", array("conditions"=> array("User.id"=> $dataInput['user_id']))) > 0){
					//** Add user for Notification
					try{
						$notificationData = array("user_id"=> $dataInput['user_id'], "device_id"=> $dataInput['device_id'], "device_reg_id"=> $dataInput['device_reg_id']);
						if( $this->NotificationUser->find("count", array("conditions"=> $notificationData)) == 0 ){
							$notificationDataAdd = $this->NotificationUser->save( $notificationData );
							if( $notificationDataAdd ){
								$responseData = array('method_name'=> 'addUserDevice', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200);
							}else{
								$responseData = array('method_name'=> 'addUserDevice', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615);
							}
						}else{
								$this->NotificationUser->updateAll( array("status"=> 1), $notificationData );
								$responseData = array('method_name'=> 'addUserDevice', 'status'=>"0", 'response_code'=> "628", 'message'=> ERROR_628);	
						}
					}catch( Exception $e ){
						$responseData = array('method_name'=> 'addUserDevice', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
					}
				}else{
					$responseData = array('method_name'=> 'addUserDevice', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
				}
			}else{
				$responseData = array('method_name'=> 'addUserDevice', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'addUserDevice', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	---------------------------------------------------------------------------------------
	On: 09-11-2015
	I/P:
	O/P:
	Desc: Stores user send notification logs
	---------------------------------------------------------------------------------------
	*/
	public function notificationList(){  
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true);
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				if( $this->User->find("count", array("conditions"=> array("User.id"=> $dataInput['user_id']))) > 0){
					//** Notification List Data
					$pageNum = isset($dataInput['page_number']) ? $dataInput['page_number'] : 1;
					$pageSize = isset($dataInput['size']) ? $dataInput['size'] : DEFAULT_PAGE_SIZE;
					$params['user_id'] = $dataInput['user_id'];
					$params['notification_id'] = isset($dataInput['notification_id']) ? $dataInput['notification_id'] : '';
					$sortBy = isset($dataInput['sort']) ? $dataInput['sort'] : 'latest';
					$notiList = $this->NotificationLog->notificationListByUser($pageSize, $pageNum, $params, $sortBy );
					if(!empty($notiList)){  
						$notiListData['Notifications'] = $this->notificationFields( $notiList , $dataInput['user_id']);
						$responseData = array('method_name'=> 'notificationList', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, 'data'=> $notiListData);
					}else{ 
						$responseData = array('method_name'=> 'notificationList', 'status'=>"0", 'response_code'=> "613", 'message'=> ERROR_613);
					}
				}else{
					$responseData = array('method_name'=> 'notificationList', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
				}
			}else{
				$responseData = array('method_name'=> 'notificationList', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'notificationList', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	---------------------------------------------------------------------------------------
	On: 09-11-2015
	I/P:
	O/P:
	Desc: Format notification fields
	---------------------------------------------------------------------------------------
	*/
	public function notificationFields( $notiData = array() , $loggedinUserId = NULL ){
		$notiListData = array();
		if(!empty($notiData)){
			foreach( $notiData as $ud ){
				$userDataList = array(); $notifications = array();
				//** User Profile
					//** Check if user posted as anonymous beat
						$isAnonymous = 0;
						if(in_array($ud['NotificationLog']['notify_type'], array('taggeduser','beatToColleague','beatToFollower'))){
							$postDetails = $this->UserPost->find("first", array("conditions"=> array("id"=> $ud['NotificationLog']['item_id'])));
							if($postDetails['UserPost']['is_anonymous'] == 1){
								$isAnonymous = 1;
							}
						}
						if($isAnonymous == 1){
							$userDataList = array(
								'user_id'=> 0,
								'first_name'=> "OCR",
								'last_name'=> "Member",
								'profile_img'=> "",
								);
						}else{
							$userDataList = array(
								'user_id'=> !empty($ud['UserProfile']['user_id']) ? $ud['UserProfile']['user_id']:'',
								'first_name'=> !empty($ud['UserProfile']['first_name']) ? $ud['UserProfile']['first_name']:'',
								'last_name'=> !empty($ud['UserProfile']['last_name']) ? $ud['UserProfile']['last_name']:'',
								'profile_img'=> !empty($ud['UserProfile']['profile_img']) ? AMAZON_PATH . $ud['User']['id']. '/profile/' . $ud['UserProfile']['profile_img']:'',
								);
						}
				// ** Total Unread Notification Count
				//$conditions = array("to_user_id"=> $loggedinUserId, "send_status"=> 1, "read_status"=> 0);
				//$unreadNotiCount = $this->NotificationLog->find("count", array("conditions"=> $conditions ));
				//$unreadNotiCount = $this->NotificationLog->unreadNotificationCount($loggedinUserId);
				$unreadNotificationsData = $this->NotificationLastVisit->unreadNotificationCountAfterLastVisited($loggedinUserId);
				$unreadNotiCount = $unreadNotificationsData[0][0]['totUnreadNotification'];
				//** Get Post Title
				$notifyType = array('taggeduser','upbeat','downbeat','share','comment','beatToFollower','beatToColleague'); 
				$beatShortTitle = "";
				if(in_array($ud['NotificationLog']['notify_type'], $notifyType)){
					try{
						$postDetails = $this->UserPost->find("first", array("conditions"=> array("id"=> $ud['NotificationLog']['item_id'])));
						$beatShortTitle = !empty($postDetails['UserPost']['title']) ? $postDetails['UserPost']['title'] : '';
					}catch(Exception $e){} 
				}
				$notifications = array(
									'notification_id'=> !empty($ud['NotificationLog']['id']) ? $ud['NotificationLog']['id']:'',
									'notify_type'=> !empty($ud['NotificationLog']['notify_type']) ? $ud['NotificationLog']['notify_type']:'',
									'read_status'=> !empty($ud['NotificationLog']['read_status']) ? (int) $ud['NotificationLog']['read_status']:'0',
									"item_id"=> !empty($ud['NotificationLog']['item_id']) ? $ud['NotificationLog']['item_id']:'',
									"short_title"=> $beatShortTitle,
									"notification_datetime"=> !empty($ud['NotificationLog']['created']) ? $ud['NotificationLog']['created']:'',  
									"unread_notifications"=> $unreadNotiCount,
									"UserProfile" => $userDataList,
									);
				$notiListData[] = $notifications;
			}
		}
		return $notiListData;
	}

	/*
	-------------------------------------------------------------------------------------
	On: 16-11-2015
	I/P: JSON (user_id, notification_id)
	O/P: JSON (success/Failure)
	Desc: value will update when notification will be read by user. 
	-------------------------------------------------------------------------------------
	*/
	public function markReadNotification(){
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true);
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				if( $this->User->find("count", array("conditions"=> array("User.id"=> $dataInput['user_id']))) > 0){
					//** Mark Notification Read
					try{
						$notificationData = array("to_user_id"=> $dataInput['user_id'], "id"=> $dataInput['notification_id']);
							$markAsRead = $this->NotificationLog->updateAll( array("read_status"=> 1), $notificationData );
							if( $markAsRead ){
								$responseData = array('method_name'=> 'markReadNotification', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200);
							}else{
								$responseData = array('method_name'=> 'markReadNotification', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615);
							}
					}catch( Exception $e ){
						$responseData = array('method_name'=> 'markReadNotification', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
					}
				}else{
					$responseData = array('method_name'=> 'markReadNotification', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
				}
			}else{
				$responseData = array('method_name'=> 'markReadNotification', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'markReadNotification', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	On: 28-11-2015
	I/P: JSON (user_id)
	O/P: JSON (user notification setting details)
	Desc: Get notification setting details for any user.
	*/
	public function getNotificationSetting(){
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true);
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				if( $this->User->find("count", array("conditions"=> array("User.id"=> $dataInput['user_id']))) > 0){
					//** Get user Notification
					$notificationSetting = $this->UserNotificationSetting->find("first", array("conditions"=> array("user_id"=> $dataInput['user_id'])));
					if( !empty($notificationSetting) ){
						$notificationSettingData['NotificationSetting'] = array("status"=> (int) $notificationSetting['UserNotificationSetting']['status']);
						$responseData = array('method_name'=> 'getNotificationSetting', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, 'data'=> $notificationSettingData);
					}else{
						$notificationSettingData['NotificationSetting'] = array("status"=> (int) 0);
						$responseData = array('method_name'=> 'getNotificationSetting', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, 'data'=> $notificationSettingData);
					}
				}else{
					$responseData = array('method_name'=> 'getNotificationSetting', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
				}
			}else{
				$responseData = array('method_name'=> 'getNotificationSetting', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'getNotificationSetting', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	------------------------------------------------------------------------------
	On: 10-12-2015
	I/P: JSON (user_id)
	O/P: JSON (user's unread notification count)
	Desc: Get total unread notifications for any user.
	------------------------------------------------------------------------------
	*/
	public function notificationCount(){
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true);
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				if( $this->User->find("count", array("conditions"=> array("User.id"=> $dataInput['user_id']))) > 0){
					//** Get user unread Notifications count
					try{
						$unreadNotifications = 0;
						//$unreadNotifications = $this->NotificationLog->unreadNotificationCount( $dataInput['user_id'] );
						$unreadNotificationsData = $this->NotificationLastVisit->unreadNotificationCountAfterLastVisited($dataInput['user_id']);
						$unreadNotifications = $unreadNotificationsData[0][0]['totUnreadNotification'];
						$unreadNotifyCount["UnreadNotificationCount"] = (int) $unreadNotifications;
						$responseData = array('method_name'=> 'notificationCount', 'status'=>"1", 'response_code'=> "200", 'data'=> $unreadNotifyCount);
					}catch( Exception $e ){
						$responseData = array('method_name'=> 'notificationCount', 'status'=>"0", 'response_code'=> "615", 'system_errors'=> $e->getMessage());
					}
				}else{
					$responseData = array('method_name'=> 'notificationCount', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
				}
			}else{
				$responseData = array('method_name'=> 'notificationCount', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'notificationCount', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	---------------------------------------------------------------------------------------
	On: 12-03-2016
	I/P: $array()
	O/P: JSON Response
	Desc: Set service is forground/bacground according to device call 
	--------------------------------------------------------------------------------------- 
	*/
	public function foregroundBackgroundServiceSet(){
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true);
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				//if( $this->User->find("count", array("conditions"=> array("User.id"=> $dataInput['user_id']))) > 0){
					//** Update user service is foreground/background
					try{
						$notificationData = array("user_id"=> $dataInput['user_id'], "device_id"=> $dataInput['device_id'], "device_reg_id"=> $dataInput['device_reg_id']);
							//** Change service is in background/foreground
							$serviceBackGroundForeground = isset($dataInput['service_background_foreground']) ? (int) $dataInput['service_background_foreground'] : (int) 0;
							//** Update service is in background/foreground
							$updateDeviceService = $this->NotificationUser->updateAll( array("service_background_foreground"=> $serviceBackGroundForeground), $notificationData );
							if( $updateDeviceService ){
								$responseData = array('method_name'=> 'foregroundBackgroundServiceSet', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200);
							}else{
								$responseData = array('method_name'=> 'foregroundBackgroundServiceSet', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615);
							}
					}catch( Exception $e ){
						$responseData = array('method_name'=> 'foregroundBackgroundServiceSet', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
					}
				/*}else{
					$responseData = array('method_name'=> 'foregroundBackgroundServiceSet', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
				}*/
			}else{
				$responseData = array('method_name'=> 'foregroundBackgroundServiceSet', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'foregroundBackgroundServiceSet', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	------------------------------------------------------------------------------
	On: 04-04-2016
	I/P: JSON (user_id)
	O/P: 
	Desc: Update last visited date by user's notification list
	------------------------------------------------------------------------------
	*/
	public function notificationLastVisitedDateUpdate(){
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true);
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				if( $this->User->find("count", array("conditions"=> array("User.id"=> $dataInput['user_id']))) > 0){
					//** Update last visited date
					try{
						$conditions = array("user_id"=> $dataInput['user_id']);
						$checkData = $this->NotificationLastVisit->find("count", array("conditions"=> $conditions));
						if($checkData == 0){
							$data = array("created"=> 'now()', "user_id"=> $dataInput['user_id']);
							$this->NotificationLastVisit->save($data);
						}else{
							$updateData = array("last_visited_date"=> 'now()');
							$updateConditions = array("user_id"=> $dataInput['user_id']);
							$this->NotificationLastVisit->updateAll($updateData, $updateConditions);
						}
						$responseData = array('method_name'=> 'notificationLastVisitedDateUpdate', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200);
					}catch(Exception $e){
						$responseData = array('method_name'=> 'notificationLastVisitedDateUpdate', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
					}
				}else{
					$responseData = array('method_name'=> 'notificationLastVisitedDateUpdate', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
				}
			}else{
				$responseData = array('method_name'=> 'notificationLastVisitedDateUpdate', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'notificationLastVisitedDateUpdate', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}
	
}
