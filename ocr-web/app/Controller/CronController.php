<?php
/*
-----------------------------------------------------------------------------------------------
	Desc: Controller used to do some scheduled tasks.
-----------------------------------------------------------------------------------------------
*/

App::uses('AppController', 'Controller');

class CronController extends AppController {
	public $uses = array('User', 'UserProfile', 'UserPost', 'UserPostSpam', 'ConsentForm', 'ConsentFormAttribute', 'EmailTemplate', 'PostSpamContent', 'NotificationQueque', 'PostUserTag', 'UserColleague', 'UserFollow', 'NotificationLog', 'NotificationLastVisit', 'UserNotificationSetting', 'NotificationUser', 'TempRegistration', 'PartialSignupMailLog', 'PreQualifiedDomain', 'EmailSmsLog','Mbapi.UserOneTimeToken','Mbapi.VoipPushNotification');
	public $components = array('Common', 'Email', 'Mb.MbEmail','Quickblox');
	/*
	-----------------------------------------------------------------------------------------------
	On: 
	I/P: 
	O/P: 
	Desc: Beat will be marked as spam when unique number of users marked as spam (currently 5 users)
	-----------------------------------------------------------------------------------------------
	*/
	public function markBeatSpam(){ 
		$totBeatSpamed = 0;
		$spamBeatList = $this->UserPostSpam->beatListToBeSpam(); //echo "<pre>"; print_r($spamBeatList);die;
		if( !empty($spamBeatList) ){
			foreach( $spamBeatList as $spamBeat ){
				$markSpam = false; $spamContentIds = array(); $spamCount = 0;
				$spamContentIds = explode(',', $spamBeat[0]['spamContentIds']);
				$spamCount = $this->UserPostSpam->find("count", array("conditions"=> array("user_post_id"=> $spamBeat['UserPostSpam']['user_post_id'])));
				if(in_array(2, $spamContentIds)){
					$markSpam = true;
				}else if($spamCount > 4){ //** if spam marked 5 or more
					$markSpam = true;
				}

				if($markSpam){
				//** Update user_posts table
				try{
					$this->UserPost->updateAll( array("UserPost.spam_confirmed"=> (int) 1), array("UserPost.id"=> $spamBeat['UserPostSpam']['user_post_id']) );
					//** Mark spam all child beats means shared by this beat
					$childSpamCount = $this->spamChildBeats($spamBeat['UserPostSpam']['user_post_id']);
					//** Get spam content (reason)
					$spamReason = $this->PostSpamContent->find("first", array("conditions"=> array("id"=> $spamBeat['UserPostSpam']['post_spam_content_id'])));
					//** Send e-mail to user who has post the beat (beat owner)
					$params = array();
					$params['subject'] = 'Your Beat marked as Spam';
					$templateContent = $this->EmailTemplate->find("first", array("conditions"=> array("name"=> 'Mark Spam Beat to beat owner')));
					$params['temp'] = $templateContent['EmailTemplate']['content'];
					$params['toMail'] = $spamBeat['User']['email'];
					$params['from'] = NO_REPLY_EMAIL;
					$params['name'] = $spamBeat['UserProfile']['first_name'] . " " . $spamBeat['UserProfile']['last_name'] ;
					$params['beat_title'] = $spamBeat['UserPost']['title'];
					$params['spam_reason'] = $spamReason['PostSpamContent']['spam_name'];
					try{
						if(!empty($params['toMail'])){
							$this->Email->markSpamBeatEmailToBeatOwner( $params );
						}
					}catch(Exception $e){}
					//** Send e-mail to admin when post marked as spam.
					$paramsAdmin = array();
					$beatSpamByDetails = $this->UserProfile->find('first', array("conditions"=> array("user_id"=> $spamBeat['UserPostSpam']['reported_by'])));
					$paramsAdmin['subject'] = 'Beat marked as Spam';
					$templateContent = $this->EmailTemplate->find("first", array("conditions"=> array("name"=> 'Mark Spam Beat to Admin')));
					$paramsAdmin['temp'] = $templateContent['EmailTemplate']['content'];
					//$paramsAdmin['toMail'] = "contact@theoncallroom.com";
					$paramsAdmin['toMail'] = "sangram@mediccreations.com"; 
					$paramsAdmin['from'] = NO_REPLY_EMAIL;
					$paramsAdmin['sender_name'] = $spamBeat['UserProfile']['first_name'] . " " . $spamBeat['UserProfile']['last_name'] ;
					$paramsAdmin['beat_title'] = $spamBeat['UserPost']['title'];
					$paramsAdmin['spam_reason'] = $spamReason['PostSpamContent']['spam_name'];
					$paramsAdmin['name'] = $beatSpamByDetails['UserProfile']['first_name']. ' ' .$beatSpamByDetails['UserProfile']['last_name'];
					$paramsAdmin['beat_owner_name'] = $params['name'];
					$paramsAdmin['spam_date'] = $spamBeat['UserPostSpam']['created'];
					$paramsAdmin['site_url_path'] = BASE_URL. 'admin';
					try{
						if(!empty($params['toMail'])){
							$this->Email->markSpamBeatEmailToAdmin( $paramsAdmin );
						}
					}catch(Exception $e){}
				}catch(Exception $e){

				}
				//** Update user_post_spams table
			}

				if($markSpam){
					try{
						$this->UserPostSpam->updateAll( array("UserPostSpam.spam_confirmed"=> (int) 1), array("UserPostSpam.user_post_id"=> $spamBeat['UserPostSpam']['user_post_id']) );
					}catch(Exception $e){

					}
					$totBeatSpamed++;
				}
			}
		}
		//echo "Total Beat Spammed:" . $totBeatSpamed;
		//echo "<pre>";print_r($childSpamCount);
		exit;
	}

	/*
	-----------------------------------------------------------------------------------------------
	On: 21-12-2015
	I/P: 
	O/P: 
	Desc: Send consent form to patient by any medical profession.
	-----------------------------------------------------------------------------------------------
	*/
	public function sendConsentForm(){
		$this->layout = '';
		$unsentConsents = $this->ConsentForm->unsentConsent();
		if( !empty($unsentConsents) ){ //echo "<pre>";print_r($unsentConsents);die;
			foreach( $unsentConsents as $consents ){	//echo "<pre>";print_r($consents['ConsentFormAttribute']);die;
				if( !empty($consents['ConsentForm']['patient_email']) ){
					$userId = $consents['ConsentForm']['user_id'];
					$paramsConsent = array();
					$consentFileName = $consents['ConsentForm']['id'] . '_' . strtotime(date('Y-m-d H:i:s')) . '.pdf';
					$consentFileCreate = $this->Common->createConsentPdf($consents['ConsentFormAttribute'], $userId, $consentFileName);
					if(file_exists(APP . '/webroot/consent_pdfs/' . $consentFileName)){
						//** Send Consent mail to patient
						$paramsConsent = array();
						$paramsConsent['subject'] = 'Consent form';
						$paramsConsent['patientName'] = $consents['ConsentForm']['patient_name'];
						$paramsConsent['toMail'] = $consents['ConsentForm']['patient_email'];
						$paramsConsent['doctorName'] = $consents['up']['first_name']. ' ' . $consents['up']['last_name'];
						$paramsConsent['doctorEmail'] = $consents['u']['email'];
						$paramsConsent['from'] = NO_REPLY_EMAIL;
						$paramsConsent['file_name'] = $consentFileName;
						$paramsConsent['file_path'] = APP . '/webroot/consent_pdfs/' . $consentFileName;
						$templateContent = $this->EmailTemplate->find("first", array("conditions"=> array("name"=> 'Send Consent To Patient')));
						$paramsConsent['temp'] = $templateContent['EmailTemplate']['content'];
						$sendMail = $this->Email->sendConsentMailToPatient( $paramsConsent );
						//** If mail send successfully, update table
						if($sendMail){
							$consentSendConfirm = $this->ConsentForm->updateAll(array("consent_send_confirmed"=> 1), array("id"=> $consents['ConsentForm']['id']));
						}
					}
					//** Delete temp consent pdf file (after sending mail) from physical directory
					$this->Common->removeTempFile( APP . '/webroot/consent_pdfs/' . $consentFileName );
				}
			}
		}
		//if($consentSendConfirm) { echo "Consent sent"; } else { echo "Consent Not sent"; }
		exit;
	}

	/*
	----------------------------------------------------------------------
	On: 06-01-2015
	I/P: 
	O/P: 
	Desc: Spam all child beats.
	----------------------------------------------------------------------
	*/
	public function spamChildBeats( $parentBeatId){
		$beatIds = array();
		$childSpamCount = 0;
		if(!empty($parentBeatId)){
			$spamBeat = $this->UserPost->updateAll( array("UserPost.spam_confirmed"=> (int) 1), array("UserPost.parent_post_id"=> $parentBeatId) );
			$beatIds[] = $parentBeatId;//array($parentBeatId);
			$childBeat = $this->UserPost->find("all", array("conditions"=> array("UserPost.parent_post_id"=> $parentBeatId)));
			foreach( $childBeat as $cb ){
				if( !empty($cb['UserPost']['id']) ){
					$beatIds[] = $cb['UserPost']['id'];
					$spamBeat = $this->UserPost->updateAll( array("UserPost.spam_confirmed"=> (int) 1), array("UserPost.parent_post_id"=> $cb['UserPost']['id']) );
					//** Call recursive
					$beatIds[] = $this->spamChildBeats($cb['UserPost']['id']);
					
				}
				//$this->spamChildBeats($childBeat['UserPost']['id']);//** Call recursive
			}
		}
		return $beatIds;
	}


	/*
	----------------------------------------------------------------------
	On: 27-06-2016
	I/P: 
	O/P: 
	Desc: Send notification when any beat posted.
	----------------------------------------------------------------------
	*/
	public function sendNotificationBeatPost(){
		//** Fetch All posts have to send notifications
		$conditions = array("notify_sent_status"=> 0);
		$beatData = $this->NotificationQueque->find("all", array("conditions"=> $conditions));

		if(!empty($beatData)){
			foreach($beatData as $beat){
				try{
						$postId = $beat['NotificationQueque']['item_id'];
						$postedBy = $beat['NotificationQueque']['user_id'];
						//** find Beat Details [START]
						$postTitle = ''; $isAnonymous = 0; $isShared = '';
						$beatConditions = array("id"=> $postId,"status"=> 1);
						$beatDetails = $this->UserPost->find("first", array("conditions"=> $beatConditions));
						$postTitle = $beatDetails['UserPost']['title']; 
						$isAnonymous = (int) $beatDetails['UserPost']['is_anonymous']; 
						$isShared = !empty($beatDetails['UserPost']['parent_post_id']) ? 'shared' : '';
						$parentBeatId = !empty($beatDetails['UserPost']['parent_post_id']) ? $beatDetails['UserPost']['parent_post_id'] : 0;
						//** find Beat Details [END]

						//** Send notification to beat owner[START]
						if(!empty($isShared)){
								$beatOwnerDetails = $this->UserPost->findById($parentBeatId);
								$beatOwnerUserId = $beatOwnerDetails['UserPost']['user_id'];
								$this->shareBeatNotificationToBeatOwner($postId, $beatOwnerUserId , $postedBy , $postTitle);
							}
						//** Send notification to beat owner[END]

						//** Find all tagged user[START]
						$taggedUsers = array();
						$taggedUsers = $this->PostUserTag->find("list", array("fields"=> array("user_id") ,"conditions"=> array("post_id"=> $postId, "status"=> 1)));
						if(!empty($taggedUsers)){
							$this->notificationToTaggedUser( $postId, $taggedUsers , $postedBy , $postTitle , $isAnonymous );
						}
						//** Find all tagged user[END]

						//** List of  Colleagues [START]
						$colleagueIds = array(); $colleagueUserIds = array(); $meColleagues = array(); $myColleagues = array();
						$meColleagues = $this->UserColleague->find("all", array("conditions"=> array("user_id"=> $postedBy, "status"=> 1)));
						foreach($meColleagues as $colleagueIds){
							$colleagueUserIds[] = $colleagueIds['UserColleague']['colleague_user_id'];
						}

						$myColleagues = $this->UserColleague->find("all", array("conditions"=> array("colleague_user_id"=> $postedBy, "status"=> 1)));
						foreach($myColleagues as $colleagueIds){
							$colleagueUserIds[] = $colleagueIds['UserColleague']['user_id'];
						}
						//** List of  Colleagues [END]

						//** List of followers[START]
						$followers = array();
						$followers = $this->UserFollow->find("all", array("conditions"=> array("followed_to"=> $postedBy, "follow_type"=> 1,"status"=> 1)));
						$followerUserIds = array();
						foreach($followers as $followerId){
							$followerUserIds[] = $followerId['UserFollow']['followed_by'];
						}
						//** List of followers[END]
						//** Send consolidated notifications[START]
						$finalUsers = array(); $finalUserRemoveTaggedUsers =array();
						$finalUsers = array_unique(array_merge($colleagueUserIds, $followerUserIds));
						$finalUserRemoveTaggedUsers = array_diff($finalUsers, array_unique($taggedUsers)); //** Don't send tagged user message again
						$this->sendNotificationToFollowers($finalUserRemoveTaggedUsers, $postedBy, $postId, $postTitle, $isAnonymous, $isShared );
						//** Send consolidated notifications[END]
				}catch(Exception $e){}
				//** Update posts not to send notification again[START]
					$this->NotificationQueque->updateAll(array("notify_sent_status"=> 1), array("item_id"=> $postId));
				//** Update posts not to send notification again[END]
			}
		}
		exit;
	}

	/*
	--------------------------------------------------------------------------
	On: 27-06-2016
	I/P:
	O/P:
	Desc: Send notification to all followers when beat added by any user..
	--------------------------------------------------------------------------
	*/
	public function sendNotificationToFollowers($followerUserIds = array(), $userId = NULL, $postId = NULL, $postTitle = '', $isAnonymous = 0, $sharedBeat = NULL){
		if( !empty($postId) ){
			//** Send GCM to colleague or follower when beat posted or shared[START]
					$message = $this->getBeatPostSharedMessage($userId, $sharedBeat, $postTitle, $isAnonymous );
			//** Send GCM to colleague or follower when beat posted or shared[END]
				foreach( $followerUserIds as $uFollow ){
					$followerUserId = $uFollow;
					//** Get User Active gcm registered devices[START]
					$userRegisteredDevices = $this->NotificationUser->getUserGcmRegisteredDevices($followerUserId);
					//** Get User Active gcm registered devices[END]

					//**Check Notification already sent
					$checkNotifyAlreadySent = $this->NotificationLog->find("count", array("conditions"=> array("by_user_id"=> $userId, "to_user_id"=> $followerUserId, "item_id"=> $postId)));
					if($checkNotifyAlreadySent == 0){
						try{
								if(empty($sharedBeat)){ //** Don't show in case of shared beat
									$notifyResponse = "";$notificationId = 0; $totUnreadNotificationCount = 0;
									$notificationSetting = $this->UserNotificationSetting->find("count", array("conditions"=> array("user_id"=> $followerUserId, "status"=> 1 )));
									if( ($notificationSetting == (int) 0) ){ //** Check user notification setting ON/OFF 
										$notifyLogData = array("by_user_id"=> $userId, "to_user_id"=> $followerUserId, "item_id"=> $postId ,"notify_type"=> "beatToFollower" ,"content"=> $message);
										$this->NotificationLog->saveAll( $notifyLogData );
										$notificationId = $this->NotificationLog->getLastInsertId();
										//** Send GCM message[START]
											$unreadNotificationsData = $this->NotificationLastVisit->unreadNotificationCountAfterLastVisited($followerUserId);
											$totUnreadNotificationCount = $unreadNotificationsData[0][0]['totUnreadNotification'];
											$notifyResponse = $this->Common->sendGcmMessage( $userRegisteredDevices, array("notification_id"=> $notificationId, "message"=> $message, "item_id"=> $postId ,"notify_type"=> "beatToFollower", "tot_unread_notification"=> $totUnreadNotificationCount) );
										//** Send GCM message[END]
										//** Update GCM API response[START]
											$this->NotificationLog->updateAll(array("api_response"=> "'". $notifyResponse ."'"), array("id"=> $notificationId));
										//** Update GCM API response[END]
									}else{
										if($userId != $followerUserId){
											$notifyLogData = array("by_user_id"=> $userId, "to_user_id"=> $followerUserId, "item_id"=> $postId ,"notify_type"=> "beatToFollower" ,"content"=> $message ,"api_response"=> "User Notification Setting Off");
											$this->NotificationLog->save( $notifyLogData );
										}
									}
								}
								//** Send New Beat Available GCM Message [START]
									$notifyResponse = $this->Common->sendGcmMessage( $userRegisteredDevices, array("message"=> "New Beat Available", "notify_type"=> "newBeatAvailable" ,"item_id"=> $postId), array("priority"=> "normal", "content_available"=> false) );
								//** Send New Beat Available GCM Message [END]
						}catch( Exception $e ){
							if($userId != $followerUserId){
								$notifyLogData = array("by_user_id"=> $userId, "to_user_id"=> $followerUserId, "item_id"=> $postId ,"notify_type"=> "beatToFollower" ,"content"=> $message ,"api_response"=> $e->getMessage() );
								$this->NotificationLog->save( $notifyLogData );
						}
					}
				}
			}
		}
	}

	/*
	------------------------------------------------------------------------------------------
	On: 27-06-2016
	I/P: $postId = NULL, $taggedUser = array()
	O/P: N/A
	Desc: Send notification to tagged users.
	------------------------------------------------------------------------------------------
	*/
	public function notificationToTaggedUser( $postId = NULL, $taggedUsers = array(), $loggedinUserId = NULL, $postTitle = '', $isAnonymous = 0){
		if( !empty($taggedUsers) && !empty($postId) ){
			foreach( $taggedUsers as $tagUser ){
				if( !empty($tagUser) && !empty($postId) ){
				//** Send Notification [START]
					//** Get User Active gcm registered devices[START]
					$userRegisteredDevices = $this->NotificationUser->getUserGcmRegisteredDevices($tagUser);
					//** Get User Active gcm registered devices[END]
					$notificationSetting = $this->UserNotificationSetting->find("count", array("conditions"=> array("user_id"=> $tagUser, "status"=> 1 )));
					$userDetails = $this->User->findById( $loggedinUserId );
					$message = "";
					$message = $this->taggedUserMessage($loggedinUserId, $postTitle, $isAnonymous);
						//** Initialize variables
						$notifyResponse = "";$notificationId = 0; $totUnreadNotificationCount = 0;
						$notifyLogData = array();
						//**Check Notification already sent
						$checkNotifyAlreadySent = $this->NotificationLog->find("count", array("conditions"=> array("by_user_id"=> $loggedinUserId, "to_user_id"=> $tagUser, "item_id"=> $postId)));
						if($checkNotifyAlreadySent == 0){
							try{
									if($loggedinUserId !=$tagUser){ //** don't save self notification
										$notifyLogData = array("by_user_id"=> $loggedinUserId, "to_user_id"=> $tagUser, "item_id"=> $postId ,"notify_type"=> "taggeduser" ,"content"=> $message );
										$checkExists = $this->NotificationLog->find("count", array("conditions"=> array("by_user_id"=> $loggedinUserId, "to_user_id"=> $tagUser, "item_id"=> $postId ,"notify_type"=> "taggeduser")));
										if( $checkExists == 0 ){
											$this->NotificationLog->saveAll( $notifyLogData );
											$notificationId = $this->NotificationLog->getLastInsertId();
										}
									}
									if( ($notificationSetting == (int) 0) && ($loggedinUserId !=$tagUser) ){ //** Check user notification setting ON/OFF and not to self user
										//$totUnreadNotificationCount = $this->NotificationLog->find("count", array("conditions"=> array("to_user_id"=> $tagUser, "read_status"=> 0)));
										$unreadNotificationsData = $this->NotificationLastVisit->unreadNotificationCountAfterLastVisited($postByUserId);
										$totUnreadNotificationCount = $unreadNotificationsData[0][0]['totUnreadNotification'];
										$notifyResponse = $this->Common->sendGcmMessage( $userRegisteredDevices, array("notification_id"=> $notificationId, "message"=> $message, "notify_type"=> "taggeduser" ,"item_id"=> $postId, "tot_unread_notification"=>$totUnreadNotificationCount) );
									}
										if(!empty($notificationId)){
											$this->NotificationLog->updateAll(array("api_response"=> "'". $notifyResponse ."'"), array("id"=> $notificationId));
										}
									//** Send New Beat Available GCM Message
										if($loggedinUserId !=$tagUser){
											$notifyResponse = $this->Common->sendGcmMessage( $userRegisteredDevices, array("message"=> "New Beat Available", "notify_type"=> "newBeatAvailable" ,"item_id"=> (int) $postId), array("priority"=> "normal", "content_available"=> false) );
										}
									//** Send New Beat Available push to devices
								}catch( Exception $e ){
									$notifyLogData = array("by_user_id"=> $loggedinUserId, "to_user_id"=> $tagUser, "item_id"=> $postId ,"notify_type"=> "taggeduser" ,"content"=> $message ,"api_response"=> $e->getMessage() );
									$checkExists = $this->NotificationLog->find("count", array("conditions"=> array("by_user_id"=> $loggedinUserId, "to_user_id"=> $tagUser, "item_id"=> $postId ,"notify_type"=> "taggeduser")));
									if( $checkExists == 0 ){
										$this->NotificationLog->saveAll( $notifyLogData );
									}
								}
							}			
					//** Send Notification [END]
				}
			}	
		}	
	}

	/*
	------------------------------------------------------------------------------------------
	On: 27-06-2016
	I/P: 
	O/P: $message
	Desc: return message for beat post/share.
	------------------------------------------------------------------------------------------
	*/
	public function getBeatPostSharedMessage($userId = NULL, $sharedBeat = NULL, $postTitle = '', $isAnonymous = 0 ){
		$message ="";
		if(!empty($sharedBeat) && $sharedBeat == 'shared'){
						//** If Post is anonymous don't show beat owner name
						if($isAnonymous == 1){
							if(!empty($postTitle)){
								$message = "OCR Member just shared " . $postTitle;
							}else{
								$message = "OCR Member just shared a Beat";
							}
						}else{
							$userDetails = $this->User->findById( $userId );
							if(!empty($postTitle)){
								$message = $userDetails['UserProfile']['first_name'] . " ". $userDetails['UserProfile']['last_name'] . " just shared " . $postTitle;
							}else{
								$message = $userDetails['UserProfile']['first_name'] . " ". $userDetails['UserProfile']['last_name'] . " just shared a Beat";
							}
						}
					}else{
						//** If Post is anonymous don't show beat owner name
						if($isAnonymous == 1){
							if(!empty($postTitle)){
								$message = "OCR Member just posted " . $postTitle;
							}else{
								$message = "OCR Member just posted a Beat";
							}
						}else{
							$userDetails = $this->User->findById( $userId );
							if(!empty($postTitle)){
								$message = $userDetails['UserProfile']['first_name'] . " ". $userDetails['UserProfile']['last_name'] . " just posted " . $postTitle;
							}else{
								$message = $userDetails['UserProfile']['first_name'] . " ". $userDetails['UserProfile']['last_name'] . " just posted a Beat";
							}
						}
					}
		return $message;
	}

	/*
	------------------------------------------------------------------------------------------
	On: 27-06-2016
	I/P: 
	O/P: $message
	Desc: return message for tagged user.
	------------------------------------------------------------------------------------------
	*/
	public function taggedUserMessage($userId = NULL, $postTitle = NULL, $isAnonymous =0){
		$message = "";
		if($isAnonymous == 1){
				if(!empty($postTitle)){
					$message = "OCR Member tagged you in " . $postTitle;
				}else{
					$message = "OCR Member tagged you in a beat";
				}
			}else{
				$userDetails = $this->User->findById($userId);
				if(!empty($postTitle)){
					$message = $userDetails['UserProfile']['first_name'] . " ". $userDetails['UserProfile']['last_name'] . " tagged you in " . $postTitle;
				}else{
					$message = $userDetails['UserProfile']['first_name'] . " ". $userDetails['UserProfile']['last_name'] . " tagged you in a beat";
				}
		}
		return $message;
	}

	/*
	-----------------------------------------------------------------------------------------
	On: 21-07-2016
	I/P: 
	O/P: 
	Desc: send notification to beat owner when any one shared beat.
	-----------------------------------------------------------------------------------------
	*/
	public function shareBeatNotificationToBeatOwner($postId = NULL, $postByUserId = NULL, $sharedByUserId = NULL, $postTitle = NULL){
		if(!empty($postId) && !empty($postByUserId) && !empty($sharedByUserId)){
		//** Send Notification [START]
			$postDetails = $this->UserPost->findById( $postId );
			$notificationSetting = $this->UserNotificationSetting->find("count", array("conditions"=> array("user_id"=> $postByUserId, "status"=> 1 )));
				$userDetails = $this->User->findById( $sharedByUserId );
				//** Get User Active gcm registered devices [START]
				$userRegisteredDevices = $this->NotificationUser->getUserGcmRegisteredDevices($postByUserId);
				//** Get User Active gcm registered devices [END]
					if(!empty($postTitle)){
						$message = $userDetails['UserProfile']['first_name'] . " ". $userDetails['UserProfile']['last_name'] . " shared ". $postDetails['UserPost']['title'];
					}else{
						$message = $userDetails['UserProfile']['first_name'] . " ". $userDetails['UserProfile']['last_name'] . " shared your beat.";
					}
				$checkNotifyAlreadySent = $this->NotificationLog->find("count", array("conditions"=> array("by_user_id"=> $sharedByUserId, "to_user_id"=> $postByUserId, "item_id"=> $postId)));
				if($checkNotifyAlreadySent == 0){	
					try{
							$notifyResponse = ""; $notificationId = 0; $totUnreadNotificationCount = 0;
							if( ($notificationSetting == (int) 0) ){ //** Check user notification setting ON/OFF and not to self user
								if($dataInput['user_id'] !=$postByUserId){ //** Don't save self notificaton
									$notifyLogData = array("by_user_id"=> $sharedByUserId, "to_user_id"=> $postByUserId, "item_id"=> $postId ,"notify_type"=> "share" ,"content"=> $message);
									$this->NotificationLog->save( $notifyLogData );
									$notificationId = $this->NotificationLog->getLastInsertId();
								
									$unreadNotificationsData = $this->NotificationLastVisit->unreadNotificationCountAfterLastVisited($postByUserId);
									$totUnreadNotificationCount = $unreadNotificationsData[0][0]['totUnreadNotification'];
									$notifyResponse = $this->Common->sendGcmMessage( $userRegisteredDevices, array("notification_id"=> $notificationId, "message"=> $message, "item_id"=> $postId ,"notify_type"=> "share", "tot_unread_notification"=>$totUnreadNotificationCount) );
								}	
								$this->NotificationLog->updateAll(array("api_response"=> "'". $notifyResponse ."'"), array("id"=> $notificationId));
							}else{
								$notifyLogData = array("by_user_id"=> $sharedByUserId, "to_user_id"=> $postByUserId, "item_id"=> $postId ,"notify_type"=> "share" ,"content"=> $message ,"api_response"=> "User Notification Setting Off" );
								$this->NotificationLog->save( $notifyLogData );
							}
						}catch( Exception $e ){
							if($dataInput['user_id'] != $postByUserId){
								$notifyLogData = array("by_user_id"=> $sharedByUserId, "to_user_id"=> $postByUserId, "item_id"=> $postId ,"notify_type"=> "share" ,"content"=> $message ,"api_response"=> $e->getMessage() );
								$this->NotificationLog->save( $notifyLogData );
							}
						}
				}
			//** Send Notification [END]
		}
	}

	/*
	-----------------------------------------------------------------------------------------
	On: 20-12-2016
	I/P: 
	O/P: 
	Desc: Send Emails to Partial Signup Users.
	-----------------------------------------------------------------------------------------
	*/
	public function sendMailToPartialSignupUsers(){
		App::import('model','TempRegistration');
		$TempRegistration = new TempRegistration();
		$partialSignupQuer = "SELECT ut.id, ut.email, ut.version, ut.device_type, ut.created, u.status, psml.email  
		FROM temp_registrations ut LEFT JOIN users u ON  u.email=ut.email LEFT JOIN partial_signup_mail_logs psml 
		ON ut.email = psml.email 
		WHERE u.status is NULL AND psml.email IS NULL GROUP BY ut.email ORDER BY ut.created DESC LIMIT 10";
		$partialRegisterUsers = $TempRegistration->query( $partialSignupQuer );
		if(!empty($partialRegisterUsers)){ 
			foreach($partialRegisterUsers as $partialData){ 
				$userEmail = trim($partialData['ut']['email']);
				//** Check email is preapproved[START]
					$domainNameArr = explode("@", $userEmail);//** Extract Domain name from email
					$domainName = end($domainNameArr);	//** Domain Name
					$rootDomain = $this->Common->getTld($domainName);
					$preQualifiedEmail = $this->PreQualifiedDomain->find("count", array("conditions"=> array("domain_name"=> trim($rootDomain), "status"=>1)));
					$preapproved = 0;
					if($preQualifiedEmail > 0){
						$preapproved = 1;
					}
				//** Check email is preapproved[END]
		 		$templateContent = $this->EmailTemplate->find("first", array("conditions"=> array("name"=> 'Medicbleep Partial Signup')));
				$partialSignupParams['temp'] = $templateContent['EmailTemplate']['content'];
				$partialSignupParams['toMail'] = $userEmail;
				if(strtolower($partialData['ut']['device_type']) == 'ios'){
					$partialSignupParams['partialSignupUrl'] = BASE_URL. "mb/deeplinkwebservices/partialSignupRedirect?email=". $userEmail ."&preapproved=". $preapproved;
				}elseif(strtolower($partialData['ut']['device_type']) == 'android'){
					$partialSignupParams['partialSignupUrl'] = "www.medicbleep.com/partialsignup-an?email=". $userEmail ."&preapproved=". $preapproved;
				}elseif(strtolower($partialData['ut']['device_type']) == 'mbweb'){
					$partialSignupParams['partialSignupUrl'] = "https://web.medicbleep.com/signup" . "?email=". $userEmail ."&preapproved=". $preapproved;
				}
				$partialSignupMailSend = $this->MbEmail->sendMailPartialSignupUser($partialSignupParams);
				//** Store on Log Table[START]
				$partialSingupData['email'] = $userEmail;
				$partialSingupData['msg'] = $partialSignupMailSend;
				$this->PartialSignupMailLog->saveAll($partialSingupData);
				//** Store on Log Table[END]
		 	}
		}
		exit;
	}

	/*
	-----------------------------------------------------------------------------------------
	On: 11-08-2017
	I/P: 
	O/P: 
	Desc: Send Onboarding emails
	-----------------------------------------------------------------------------------------
	*/
	public function sendOnboardingMail(){
		$userDetails = $this->User->find('all', array("conditions"=> array("status"=> 1, "approved"=> 1), "order"=> "registration_date DESC", "limit"=> 10));
		foreach($userDetails as $ud){
			// $params = array();
			// $day1 = date('Y-m-d h:i:00');
			// $day2 = date('Y-m-d H:i:s', strtotime($day1 . ' +2 days'));
			// $day3 = date('Y-m-d H:i:s', strtotime($day2 . ' +2 days'));
			// // $day4 = date('Y-m-d H:i:s', strtotime($day3 . ' +2 days'));
			// // $day5 = date('Y-m-d H:i:s', strtotime($day4 . ' +2 days'));
			// $day6 = date('Y-m-d H:i:s', strtotime($day5 . ' +2 days'));
			// $day7 = date('Y-m-d H:i:s', strtotime($day6 . ' +2 days'));
			// $day8 = date('Y-m-d H:i:s', strtotime($day7 . ' +2 days'));
			// $day9 = date('Y-m-d H:i:s', strtotime($day8 . ' +2 days'));
			// $day10 = date('Y-m-d H:i:s', strtotime($day9 . ' +2 days'));



			// $day1 = date('Y-m-d h:i:00');
			// $day2 = date('Y-m-d H:i:s', strtotime($day1 . ' +2 days'));
			// $day3 = date('Y-m-d H:i:s', strtotime($day2 . ' +2 days'));
			// $day4 = date('Y-m-d H:i:s', strtotime($day3 . ' +2 days'));
			// $day5 = date('Y-m-d H:i:s', strtotime($day4 . ' +2 days'));
			// $day6 = date('Y-m-d H:i:s', strtotime($day3 . ' +2 days'));
			// $day7 = date('Y-m-d H:i:s', strtotime($day6 . ' +2 days'));
			// $day8 = date('Y-m-d H:i:s', strtotime($day7 . ' +2 days'));
			// $day9 = date('Y-m-d H:i:s', strtotime($day8 . ' +2 days'));
			// $day10 = date('Y-m-d H:i:s', strtotime($day9 . ' +2 days'));

			$day1 = date('Y-m-d h:i:00');
			$wday1 = date('D', strtotime($day1));
			if($wday1 == 'Sat' || $wday1 == 'Sun')
			{
			 $day1 = date('Y-m-d h:i:00',strtotime('+3 days'));
			}
			else
			{
			  $day1 = date('Y-m-d h:i:00');
			}
			$day2 = date('Y-m-d H:i:s', strtotime($day1 . ' +2 days'));
			$wday2 = date('D', strtotime($day2));
			if($wday2 == 'Sat' || $wday2 == 'Sun')
			{
			  $day2 = date('Y-m-d h:i:00',strtotime($day1 . '+4 days'));
			 
			}
			else
			{
			  $day2 = date('Y-m-d H:i:s', strtotime($day1 . ' +2 days'));
			}
			$day3 = date('Y-m-d H:i:s', strtotime($day2 . ' +2 days'));
			$wday3 = date('D', strtotime($day3));
			if($wday3 == 'Sat' || $wday3 == 'Sun')
			{
			  $day3 = date('Y-m-d H:i:s', strtotime($day2 . ' +4 days'));
			}
			else
			{
			  $day3 = date('Y-m-d H:i:s', strtotime($day2 . ' +2 days'));
			}
			// $day4 = date('Y-m-d H:i:s', strtotime($day3 . ' +2 days'));
			// $wday4 = date('D', strtotime($day4));
			// if($wday4 == 'Sat' || $wday4 == 'Sun')
			// {
			//  $day4 = date('Y-m-d H:i:s', strtotime($day3 . ' +4 days'));
			// }
			// else
			// {
			//   $day4 = date('Y-m-d H:i:s', strtotime($day3 . ' +2 days'));
			// }
			// $day5 = date('Y-m-d H:i:s', strtotime($day4 . ' +2 days'));
			// $wday5 = date('D', strtotime($day5));
			// if($wday5 == 'Sat' || $wday5 == 'Sun')
			// {
			//  $day5 = date('Y-m-d H:i:s', strtotime($day4 . ' +4 days'));
			// }
			// else
			// {
			//   $day5 = date('Y-m-d H:i:s', strtotime($day4 . ' +2 days'));
			// }
			$day6 = date('Y-m-d H:i:s', strtotime($day3 . ' +2 days'));
			$wday6 = date('D', strtotime($day6));
			if($wday6 == 'Sat' || $wday6 == 'Sun')
			{
			 $day6 = date('Y-m-d H:i:s', strtotime($day3 . ' +4 days'));
			}
			else
			{
			  $day6 = date('Y-m-d H:i:s', strtotime($day3 . ' +2 days'));
			}
			$day7 = date('Y-m-d H:i:s', strtotime($day6 . ' +2 days'));
			$wday7 = date('D', strtotime($day7));
			if($wday7 == 'Sat' || $wday7 == 'Sun')
			{
			 $day7 = date('Y-m-d H:i:s', strtotime($day6 . ' +4 days'));
			}
			else
			{
			  $day7 = date('Y-m-d H:i:s', strtotime($day6 . ' +2 days'));
			}
			$day8 = date('Y-m-d H:i:s', strtotime($day7 . ' +2 days'));
			$wday8 = date('D', strtotime($day8));
			if($wday8 == 'Sat' || $wday8 == 'Sun')
			{
			 $day8 = date('Y-m-d H:i:s', strtotime($day7 . ' +4 days'));
			}
			else
			{
			  $day8 = date('Y-m-d H:i:s', strtotime($day7 . ' +2 days'));
			}
			$day9 = date('Y-m-d H:i:s', strtotime($day8 . ' +2 days'));
			$wday9 = date('D', strtotime($day9));
			if($wday9 == 'Sat' || $wday9 == 'Sun')
			{
			 $day9 = date('Y-m-d H:i:s', strtotime($day8 . ' +4 days'));
			}
			else
			{
			  $day9 = date('Y-m-d H:i:s', strtotime($day8 . ' +2 days'));
			}

			
			$params['toMail'] = $ud['User']['email'];
			$params['name'] = $ud['UserProfile']['first_name'];
			$userId = $ud['User']['id'];
			//** First Mail [START]
			$params['templateName'] = "MB_Welcome_to_Medic_Bleep";
			$params['subject'] = "Welcome to Medic Bleep";
			$params['sendAt'] = $day1;
			if( $this->EmailSmsLog->find("count", array("conditions"=> array("user_id"=> $userId, "notify_type"=> $params['templateName'])))==0 ){
				$onboardingMBWelcomeToMedicBleep = $this->MbEmail->onboardingEmail($params);
				$this->EmailSmsLog->saveAll( array("type"=> "email", "msg"=> $onboardingMBWelcomeToMedicBleep, "user_id"=> $userId, "notify_type"=> $params['templateName']) );
			}
			//** First Mail [END]
			
			//** Second Mail [START]
				//** As disscussed with akshat 03Oct17 not to send this mail
			/*$params['templateName'] = "MB_Add_Colleagues";
			$params['subject'] = "It’s good to talk";
			$params['sendAt'] = $day2;
			if( $this->EmailSmsLog->find("count", array("conditions"=> array("user_id"=> $userId, "notify_type"=> $params['templateName'])))==0 ){
				$onboardingMBAddColleagues = $this->MbEmail->onboardingEmail($params);
				$this->EmailSmsLog->saveAll( array("type"=> "email", "msg"=> $onboardingMBAddColleagues, "user_id"=> $userId, "notify_type"=> $params['templateName']) );
			}*/
			//** Second Mail [END]

			//** Third Mail [START]
			// $params['templateName'] = "MB_Invite_Colleagues";
			// $params['subject'] = "Find the people you want to talk to";
			// $params['sendAt'] = $day2;
			// if( $this->EmailSmsLog->find("count", array("conditions"=> array("user_id"=> $userId, "notify_type"=> $params['templateName'])))==0 ){
			// 	$onboardingMBInviteColleagues = $this->MbEmail->onboardingEmail($params);
			// 	$this->EmailSmsLog->saveAll( array("type"=> "email", "msg"=> $onboardingMBInviteColleagues, "user_id"=> $userId, "notify_type"=> $params['templateName']) );
			// }
			//** Third Mail [END]

			//** Fourth Mail [START]
			$params['templateName'] = "MB_Starting_Group_Chat";
			$params['subject'] = "Keep Everyone in the Loop";
			$params['sendAt'] = $day3;
			if( $this->EmailSmsLog->find("count", array("conditions"=> array("user_id"=> $userId, "notify_type"=> $params['templateName'])))==0 ){
				$onboardingMBStartingGroupChat = $this->MbEmail->onboardingEmail($params);
				$this->EmailSmsLog->saveAll( array("type"=> "email", "msg"=> $onboardingMBStartingGroupChat, "user_id"=> $userId, "notify_type"=> $params['templateName']) );
			}
			//** Fourth Mail [END]

			//** Fifth Mail [START]

			//Commecnted On 20 Dec 2018[Disccuss With sandeep]

			// $params['templateName'] = "MB_Send_Images_and_Documents";
			// $params['subject'] = "Never Lose a Handover Sheet Again!";
			// $params['sendAt'] = $day4;
			// if( $this->EmailSmsLog->find("count", array("conditions"=> array("user_id"=> $userId, "notify_type"=> $params['templateName'])))==0 ){
			// 	$onboardingMBSendImagesAndDocuments = $this->MbEmail->onboardingEmail($params);
			// 	$this->EmailSmsLog->saveAll( array("type"=> "email", "msg"=> $onboardingMBSendImagesAndDocuments, "user_id"=> $userId, "notify_type"=> $params['templateName']) );
			// 	$this->EmailSmsLog->saveAll( array("type"=> "email", "msg"=> "Stop for Wsh", "user_id"=> $userId, "notify_type"=> $params['templateName']) );
			// }

			//Commecnted On 20 Dec 2018[Disccuss With sandeep]
			//** Fifth Mail [END]


			//** Sixth Mail [START]
			//Commecnted On 20 Dec 2018[Disccuss With sandeep]
			// $params['templateName'] = "MB_Export_Chat";
			// $params['subject'] = "How to Export Your Chats";
			// $params['sendAt'] = $day5;
			// if( $this->EmailSmsLog->find("count", array("conditions"=> array("user_id"=> $userId, "notify_type"=> $params['templateName'])))==0 ){
			// 	$onboardingMbExportChat = $this->MbEmail->onboardingEmail($params);
			// 	$this->EmailSmsLog->saveAll( array("type"=> "email", "msg"=> $onboardingMbExportChat, "user_id"=> $userId, "notify_type"=> $params['templateName']) );
			// 	$this->EmailSmsLog->saveAll( array("type"=> "email", "msg"=> "Stop for Wsh", "user_id"=> $userId, "notify_type"=> $params['templateName']) );
			// }
			//Commecnted On 20 Dec 2018[Disccuss With sandeep]
			//** Sixth Mail [END]

			//** Seventh Mail [START]
			$params['templateName'] = "MB_Your_Profile_&_Settings";
			$params['subject'] = "Your Profile. Your Settings.";
			$params['sendAt'] = $day6;
			if( $this->EmailSmsLog->find("count", array("conditions"=> array("user_id"=> $userId, "notify_type"=> $params['templateName'])))==0 ){
				$onboardingMbYourProfileAndSettings = $this->MbEmail->onboardingEmail($params);
				$this->EmailSmsLog->saveAll( array("type"=> "email", "msg"=> $onboardingMbYourProfileAndSettings, "user_id"=> $userId, "notify_type"=> $params['templateName']) );
			}
			//** Seventh Mail [END]

			//** Eighth Mail [START]
			$params['templateName'] = "MB_See_&_Hear_Your_Colleagues";
			$params['subject'] = "See & Hear Your Colleagues";
			$params['sendAt'] = $day7;
			if( $this->EmailSmsLog->find("count", array("conditions"=> array("user_id"=> $userId, "notify_type"=> $params['templateName'])))==0 ){
				$onboardingMbSeeAndHearYourColleagues = $this->MbEmail->onboardingEmail($params);
				$this->EmailSmsLog->saveAll( array("type"=> "email", "msg"=> $onboardingMbSeeAndHearYourColleagues, "user_id"=> $userId, "notify_type"=> $params['templateName']) );
			}
			//** Eighth Mail [END]

			//** Ninth Mail [START]
			$params['templateName'] = "MB_Web_App";
			$params['subject'] = "Which version is right for you?";
			$params['sendAt'] = $day8;
			if( $this->EmailSmsLog->find("count", array("conditions"=> array("user_id"=> $userId, "notify_type"=> $params['templateName'])))==0 ){
				$onboardingMbWebApp = $this->MbEmail->onboardingEmail($params);
				$this->EmailSmsLog->saveAll( array("type"=> "email", "msg"=> $onboardingMbWebApp, "user_id"=> $userId, "notify_type"=> $params['templateName']) );
			}
			//** Ninth Mail [END]

			//** Tenth Mail [START]
			$params['templateName'] = "MB_Secure_Messaging";
			$params['subject'] = "How Secure is Medic Bleep?";
			$params['sendAt'] = $day9;
			if( $this->EmailSmsLog->find("count", array("conditions"=> array("user_id"=> $userId, "notify_type"=> $params['templateName'])))==0 ){
				$onboardingMbSecureMessaging = $this->MbEmail->onboardingEmail($params);
				$this->EmailSmsLog->saveAll( array("type"=> "email", "msg"=> $onboardingMbSecureMessaging, "user_id"=> $userId, "notify_type"=> $params['templateName']) );
			}
			//** Tenth Mail [END]
		}
		exit;
	}
}
