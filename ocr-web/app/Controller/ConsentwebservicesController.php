
<?php
/*
Desc: API data get/post to Webservices realated to Consent Form.
*/

class ConsentwebservicesController extends AppController {

	public $uses = array('User', 'ConsentForm', 'ConsentFormAttribute');
	public $components = array('Common', 'Image', 'Email');

	/*
	-------------------------------------------------------------
	On: 28-09-2015
	I/P: JSON Data
	O/P: JSON data with Message
	Desc: Adding consent form by any doctor on behalf of patient.
	------------------------------------------------------------- 
	*/
	public function addConsent(){
		$responseData = array();
		if($this->request->is('post')) {
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				$dataInput = json_decode($_REQUEST['data'], true);
				$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
				$dataInput = json_decode($encryptedData, true);
				if( $this->User->find("count", array("conditions"=> array("User.id"=> $dataInput['user_id']))) > 0){
				//** Add Consent Form
				$consentData = array("patient_name"=> $dataInput['patient_name'], "patient_email"=> $dataInput['patient_email'], "user_id"=> (int) $dataInput['user_id'], "status"=> 1 );
				try{
					$addConsent = $this->ConsentForm->save( $consentData );
					if( $addConsent ){
						//** Add Consent form attributes which Not includes attachment
						if( !empty($dataInput['attributes']) ){
							foreach( $dataInput['attributes'] as $attributes){
								if( $attributes['attribute_type'] == "text" || $attributes['attribute_type'] == "video" ){
									$consentFormAttributes = array("consent_form_id"=> $this->ConsentForm->id, "attribute_type"=> $attributes['attribute_type'], "content"=> $attributes['content'], "status"=> 1);
									$this->ConsentFormAttribute->saveAll( $consentFormAttributes );
								}
							}
						}
						//** Add consent form images
						if( isset($dataInput['images']) && !empty($dataInput['images']) ){
							foreach( $dataInput['images'] as $imgs){
								$imgName = ''; $imageName = '';
								$imgName = $this->ConsentForm->id.'_'.strtotime(date("Y-m-d H:i:s")).rand(1,1000);
								$imageName = $this->Image->createimage($imgs['img'],WWW_ROOT . 'img/uploader_tmp/', $imgName, $imgs['img_ext']);
								if( !empty($imageName) ){
									$this->Image->moveImageToAmazon( array('img_path'=> WWW_ROOT . 'img/uploader_tmp/'.$imageName, 'img_name'=> $dataInput['user_id'].'/ConsentForm/image/'.$imageName) );
									$consentFormAttributes = array("consent_form_id"=> $this->ConsentForm->id, "attribute_type"=> 'image', "is_signature"=> $imgs['is_signature'], "content"=> $imageName, "status"=> 1);
									$this->ConsentFormAttribute->saveAll( $consentFormAttributes );
								}
								//** Remove physiacal file from Temp Directory
								if( file_exists( WWW_ROOT . 'img/uploader_tmp/'.$imageName )){
									chmod(WWW_ROOT . 'img/uploader_tmp/'.$imageName, 777);
									unlink( WWW_ROOT . 'img/uploader_tmp/'.$imageName );
								}
							}
						}
						//** Add consent form attributes includes attachment
						if( isset($_FILES) && !empty($_FILES) ){
							//** Add doc attributes
							if( isset($dataInput['doc_count']) &&  $dataInput['doc_count']>0 ){
								for($cnt=0; $cnt < $dataInput['doc_count']; $cnt++ ){
									$postAttributes = array();
									$fileNameDoc = $this->Image->uploadFileTempLocation( $dataInput['user_id'].'/ConsentForm/doc/', $_FILES, 'docfileUpload_'.$cnt);
									$fileExt = explode(".", $fileNameDoc);
									$consentFormAttributes = array("consent_form_id"=> $this->ConsentForm->id, "attribute_type"=> end($fileExt), "content"=> $fileNameDoc, "status"=> 1);
									$this->ConsentFormAttribute->saveAll( $consentFormAttributes );
									//** Remove physiacal file from Temp Directory
									$this->Common->removeTempFile( WWW_ROOT . 'img/uploader_tmp/'.$fileNameDoc );
								}
							}
						}
						$responseData = array('method_name'=> 'addConsentForm', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200);
					}else{
						$responseData = array('method_name'=> 'addConsentForm', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615);
					}
				}catch( Exception $e ){
					$responseData = array('method_name'=> 'addConsentForm', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615);
				}
				}else{
					$responseData = array('method_name'=> 'addConsentForm', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
				}
			}else{
				$responseData = array('method_name'=> 'addConsentForm', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'addConsentForm', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		//echo json_encode( $responseData );
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	-------------------------------------------------------------
	On: 29-09-2015
	I/P: JSON Data
	O/P: JSON data with Message
	Desc: Display consents added by user.
	------------------------------------------------------------- 
	*/
	public function listConsents(){
		$responseData = array();
		if($this->request->is('post')) {
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				$dataInput = $this->request->input ( 'json_decode', true) ;
				$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
				$dataInput = json_decode($encryptedData, true);
				if( $this->User->find("count", array("conditions"=> array("User.id"=> $dataInput['user_id']))) > 0){
					$pageSize = (isset($dataInput['size']) && !empty($dataInput['size'])) ? (int) $dataInput['size'] : DEFAULT_PAGE_SIZE;
					$pageNumber = (isset($dataInput['page_number']) && !empty($dataInput['page_number'])) ? (int) $dataInput['page_number'] : 1;
					$consentId = (isset($dataInput['consent_id']) && !empty($dataInput['consent_id'])) ? (int) $dataInput['consent_id'] : '';
					$sortBy = isset($dataInput['sort']) ? $dataInput['sort'] : 'latest';
					$consentListData = $this->ConsentForm->consentList( (int) $dataInput['user_id'], $pageSize, $pageNumber, $consentId, $sortBy );
					if( !empty($consentListData) ){ //echo "<pre>";print_r($consentListData);die;
						$consentData = $this->getFields( $consentListData );
						$responseData = array('method_name'=> 'listConsents', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> array("Consents"=> $consentData));
					}else{
						$responseData = array('method_name'=> 'listConsents', 'status'=>"0", 'response_code'=> "613", 'message'=> ERROR_613);
					}
				}else{
					$responseData = array('method_name'=> 'listConsents', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
				}
			}else{
				$responseData = array('method_name'=> 'listConsents', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'listConsents', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		//echo json_encode( $responseData );
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	-----------------------------------------------------------------------------------------------
	On: 29-09-2015
	I/P:
	O/P:
	Desc: Formatted JSON data for consent form
	-----------------------------------------------------------------------------------------------
	*/
	public function getFields( $consentListData = array() ){
		$consentDataArr = array();
		foreach( $consentListData as $cd ){
		//** Get attribute data
			$attrData = array();
			foreach( $cd['ConsentFormAttribute'] as $consentAttr ){
				if( $consentAttr['attribute_type'] == 'text' || $consentAttr['attribute_type'] == 'video' ){
					$content = $consentAttr['content']; 
				}
				else if( $consentAttr['attribute_type'] == 'image'){ 
					$content = AMAZON_PATH . $cd['ConsentForm']['user_id'].'/ConsentForm/image/'.$consentAttr['content']; 
				}
				else{	
					$content = AMAZON_PATH . $cd['ConsentForm']['user_id'].'/ConsentForm/doc/'.$consentAttr['content'];
				}
				$attrData[] = array(
					"attr_id"=> !empty($consentAttr['id']) ? $consentAttr['id'] : '',
					"attribute_type"=> !empty($consentAttr['attribute_type']) ? $consentAttr['attribute_type'] : '',
					"is_signature"=> (int) $consentAttr['is_signature'],
					"content"=> !empty($content) ? $content : '',
					);
			}
			$consentData["id"] = !empty($cd["ConsentForm"]['id']) ? $cd["ConsentForm"]['id'] : '';
			$consentData["user_id"] = !empty($cd["ConsentForm"]['user_id']) ? $cd["ConsentForm"]['user_id'] : '';
			$consentData["patient_name"] = !empty($cd["ConsentForm"]['patient_name']) ? $cd["ConsentForm"]['patient_name'] : '';
			$consentData["patient_email"] = !empty($cd["ConsentForm"]['patient_email']) ? $cd["ConsentForm"]['patient_email'] : '';
			$consentData["status"] = !empty($cd["ConsentForm"]['status']) ? $cd["ConsentForm"]['status'] : '';
			$consentData["created"] = !empty($cd["ConsentForm"]['created']) ? $cd["ConsentForm"]['created'] : '';
			$consentData["attributes"] = $attrData;
			$consentDataArr[] = $consentData;
		}
		return $consentDataArr;
	}

	/*
	-----------------------------------------------------------------------------------------------
	On:	30-09-2015
	I/P:	
	O/P:	
	Desc: Delete consent form by same user.
	----------------------------------------------------------------------------------------------- 
	*/
	public function deleteConsent(){
		$responseData = array();
		if($this->request->is('post')) {
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				$dataInput = $this->request->input ( 'json_decode', true) ;
				$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
				$dataInput = json_decode($encryptedData, true);
				if( $this->User->find("count", array("conditions"=> array("User.id"=> $dataInput['user_id']))) > 0){
					if( $this->ConsentForm->find("count", array("conditions"=> array("ConsentForm.id"=> $dataInput['consent_id']))) > 0){
						if( $this->ConsentForm->find("count", array("conditions"=> array("ConsentForm.user_id"=> $dataInput['user_id'], "ConsentForm.id"=> $dataInput['consent_id']))) > 0){
						//** Delete consent [START]
						try{
							$updateConsent = $this->ConsentForm->updateAll( array("ConsentForm.status"=> 2), array("ConsentForm.id"=> $dataInput['consent_id'], "ConsentForm.user_id"=> $dataInput['user_id']));
							if( $updateConsent ){
								$responseData = array('method_name'=> 'listConsents', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200);
							}else{
								$responseData = array('method_name'=> 'listConsents', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615);
							}
						}catch( Exception $e){
							$responseData = array('method_name'=> 'listConsents', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615);
						}
						//** Delete consent [END]
						}else{
						$responseData = array('method_name'=> 'listConsents', 'status'=>"0", 'response_code'=> "622", 'message'=> ERROR_622);
					}
					}else{
						$responseData = array('method_name'=> 'listConsents', 'status'=>"0", 'response_code'=> "621", 'message'=> ERROR_621);
					}
				}else{
					$responseData = array('method_name'=> 'listConsents', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
				}
			}else{
				$responseData = array('method_name'=> 'listConsents', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'listConsents', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		//echo json_encode( $responseData );
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}
}
