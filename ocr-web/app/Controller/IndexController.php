<?php


App::uses('AppController', 'Controller');

class IndexController extends AppController {
	public $uses = array('Content', 'User', 'EmailTemplate', 'EmailSmsLog', 'Country');
	public $components = array('Common', 'Image', 'Email', 'Session');
	public $helpers = array('Form', 'Session');
	/*
	-----------------------------------------------------------------------------------------
	On:
	I/P:
	O/P:
	Desc: Main index page
	-----------------------------------------------------------------------------------------
	*/
	public function index(){
		if($_SERVER['SERVER_NAME'] == 'institution.medicbleep.com'){
			header("Location: https://institution.medicbleep.com/institution");
			exit();
		}
		header("Location: https://www.medicbleep.com");
		exit;
		// echo "<pre>";print_r("Hello");exit();
		$this->layout = '';
		$pageContent = $this->Content->find("first", array("conditions"=> array("content_title"=> "index", "content_status"=> 1)));
		$this->set('pageContent', $pageContent);
	}

	/*
	-----------------------------------------------------------------------------------------
	On:
	I/P:
	O/P:
	Desc: FAQ page content
	-----------------------------------------------------------------------------------------
	*/
	public function faq($callFromDevice = NULL){
		$pageTitle = "Frequently Asked Questions";
		if($callFromDevice == "mobile" ){
			$this->layout = 'layout_without_header_footer';
			$pageTitle = "";
		}
		$faqContent = $this->Content->find("first", array("conditions"=> array("content_title"=> "faq", "content_status"=> 1)));
		$this->set('pageTitle', $pageTitle);
		$this->set('faqContent', $faqContent);
	}

	/*
	-----------------------------------------------------------------------------------------
	On:
	I/P:
	O/P:
	Desc: FAQ page content
	-----------------------------------------------------------------------------------------
	*/
	public function faqMobile(){
		$this->layout = 'layout_without_header_footer';
		$pageTitle = "Frequently Asked Questions";
		$this->set('pageTitle', $pageTitle);
	}

	/*
	-----------------------------------------------------------------------------------------
	On:
	I/P:
	O/P:
	Desc: Terms & Conditions page Contents.
	-----------------------------------------------------------------------------------------
	*/
	public function tnc($callFromDevice = NULL){
		$pageTitle = "Terms and Conditions";
		if($callFromDevice == "mobile" ){
			$this->layout = 'layout_without_header_footer';
			$pageTitle = "";
		}
		$tncContent = $this->Content->find("first", array("conditions"=> array("content_title"=> "tnc", "content_status"=> 1)));
		$this->set('pageTitle', $pageTitle);
		$this->set('tncContent', $tncContent);
	}

	/*
	-----------------------------------------------------------------------------------------
	On:
	I/P:
	O/P:
	Desc: Terms & Conditions page Contents.
	-----------------------------------------------------------------------------------------
	*/
	public function tncMobile($callFromDevice = NULL){
		$this->layout = 'layout_without_header_footer';
		$pageTitle = "Terms and Conditions";
		$this->set('pageTitle', $pageTitle);
	}
	

	/*
	-----------------------------------------------------------------------------------------
	On:
	I/P:
	O/P:
	Desc: Privacy Policy page Contents.
	-----------------------------------------------------------------------------------------
	*/
	public function privacyPolicy($callFromDevice = NULL){
		$pageTitle = "Privacy Policy";
		if($callFromDevice == "mobile" ){
			$this->layout = 'layout_without_header_footer';
			$pageTitle = "";
		}
		$privacyContent = $this->Content->find("first", array("conditions"=> array("content_title"=> "privacy", "content_status"=> 1)));
		$this->set('pageTitle', $pageTitle);
		$this->set('privacyContent', $privacyContent);
	}

	/*
	-----------------------------------------------------------------------------------------
	On:
	I/P:
	O/P:
	Desc: Privacy Policy page Contents.
	-----------------------------------------------------------------------------------------
	*/
	public function privacyPolicyMobile(){
		$this->layout = 'layout_without_header_footer';
		$pageTitle = "Privacy Policy";
		$this->set('pageTitle', $pageTitle);
	}

	/*
	-----------------------------------------------------------------------------------------
	On:
	I/P:
	O/P:
	Desc: About Us page Contents.
	-----------------------------------------------------------------------------------------
	*/
	public function about($callFromDevice = NULL){
		$pageTitle = "About Us";
		if($callFromDevice == "mobile" ){
			$this->layout = 'layout_without_header_footer';
			$pageTitle = "";
		}
		$aboutContent = $this->Content->find("first", array("conditions"=> array("content_title"=> "about", "content_status"=> 1)));
		$this->set('pageTitle', $pageTitle);
		$this->set('about', $aboutContent);
	}

	/*
	-----------------------------------------------------------------------------------------
	On:
	I/P:
	O/P:
	Desc: About Us page for mobile Contents.
	-----------------------------------------------------------------------------------------
	*/
	public function aboutMobile(){
		$this->layout = 'layout_without_header_footer';
		$pageTitle = "About Us";
		$this->set('pageTitle', $pageTitle);
		
	}

	/*
	-----------------------------------------------------------------------------------------
	On:
	I/P:
	O/P:
	Desc: Consent terms and conditions page Contents.
	-----------------------------------------------------------------------------------------
	*/
	public function consentTermConditions($callFromDevice = NULL){
		$pageTitle = "Consent";
		if($callFromDevice == "mobile" ){
			$this->layout = 'layout_without_header_footer';
			$pageTitle = "";
		}
		$consentTermContent = $this->Content->find("first", array("conditions"=> array("content_title"=> "consentterms", "content_status"=> 1)));
		$this->set('pageTitle', $pageTitle);
		$this->set('consentTerm', $consentTermContent);
	}

	/*
	-----------------------------------------------------------------------------------------
	On:
	I/P:
	O/P:
	Desc: Consent terms and conditions mobile page Contents.
	-----------------------------------------------------------------------------------------
	*/
	public function consentTermConditionsMobile($callFromDevice = NULL){
		$this->layout = 'layout_without_header_footer';
		$pageTitle = "Consent";
		$this->set('pageTitle', $pageTitle);
	}

	/*
	-----------------------------------------------------------------------------------------
	On:
	I/P:
	O/P:
	Desc: Network Guidelines page Contents.
	-----------------------------------------------------------------------------------------
	*/
	public function networkGuidelines($callFromDevice = NULL){
		$pageTitle = "Network Guidelines";
		if($callFromDevice == "mobile" ){
			$this->layout = 'layout_without_header_footer';
			$pageTitle = "";
		}
		$networkGuidelines = $this->Content->find("first", array("conditions"=> array("content_title"=> "Network Guidelines", "content_status"=> 1)));
		$this->set('pageTitle', $pageTitle);
		$this->set('networkGuidelines', $networkGuidelines);
	}

	/*
	-----------------------------------------------------------------------------------------
	On:
	I/P:
	O/P:
	Desc: Network Guidelines page Contents.
	-----------------------------------------------------------------------------------------
	*/
	public function joinTheTeam($callFromDevice = NULL){
		$pageTitle = "Join the Team";
		if($callFromDevice == "mobile" ){
			$this->layout = 'layout_without_header_footer';
			$pageTitle = "";
		}
		$joinTheTeam = $this->Content->find("first", array("conditions"=> array("content_title"=> "Join The Team", "content_status"=> 1)));
		$this->set('pageTitle', $pageTitle);
		$this->set('joinTheTeam', $joinTheTeam);
	}

	/*
	-----------------------------------------------------------------------------------------
	On:
	I/P:
	O/P:
	Desc: Contact page Contents.
	-----------------------------------------------------------------------------------------
	*/
	public function contact($callFromDevice = NULL){
		//pr($this->params);
		$pageTitle = "Contact Us";
		if($callFromDevice == "mobile" ){
			$this->layout = 'layout_without_header_footer';
			$pageTitle = "";
		}
		$contact = $this->Content->find("first", array("conditions"=> array("content_title"=> "Contact", "content_status"=> 1)));
		$this->set('pageTitle', $pageTitle);
		$this->set('contact', $contact);
	}

	/*
	-------------------------------------------------------------------------------------
	On: 04-08-2015
	I/P: $activationKey
	O/P: Key activation Message
	-------------------------------------------------------------------------------------
	*/
	public function verifyMail( $activationKey = NULL , $encryptedUserId = NULL ){ 
		$this->layout = '';
		$msg = 'Wrong Key';
		if( !empty( $activationKey ) ){
			$activationStatus = $this->User->activationKeyStatus( $activationKey ); 
			if( !empty($activationStatus) ){ 
				if( $activationStatus['User']['status'] == 0 ){ 
					//$msg = 'Email Already Verified.';
				//}else {
					$keyUpdate = $this->User->activateKey( $activationKey );
					if($keyUpdate){
						$msg = "Email Verified";
						$userId = (int) base64_decode($encryptedUserId);
						try{
								$userDetails = $this->User->find('first', array('fields'=> array('User.id, User.email, UserProfile.first_name, UserProfile.last_name, UserProfile.country_id'), 'conditions'=> array('User.id'=> $userId)));
								//echo "<pre>";print_r($userDetails);die;
							}catch(Exception $e){
								//echo $e->getMessage();die;
							}
						//** Send welcome mail
						/*if( !empty($encryptedUserId) ){
							$userId = (int) base64_decode($encryptedUserId);
							try{
								$userDetails = $this->User->find('first', array('fields'=> array('User.id, User.email, UserProfile.first_name, UserProfile.last_name'), 'conditions'=> array('User.id'=> $userId)));
								//echo "<pre>";print_r($userDetails);die;
							}catch(Exception $e){
								//echo $e->getMessage();die;
							}
							$templateContent = $this->EmailTemplate->find("first", array("conditions"=> array("name"=> 'Welcome')));
							$params['temp'] = $templateContent['EmailTemplate']['content'];
							$params['toMail'] = $userDetails['User']['email'];
							$params['from'] = NO_REPLY_EMAIL;
							$params['name'] = $userDetails['UserProfile']['first_name'] . ' ' . $userDetails['UserProfile']['last_name'];
							$params['regards'] = "Dr. Sandeep Bansal";
							try{
								$mailSend = $this->Email->signupWelcomeEmail( $params );
								$this->EmailSmsLog->save( array("type"=> "email", "msg"=> $mailSend, "user_id"=> $userId, "notify_type"=> "Signup Welcome") );
							}catch(Exception $e){}
						}*/
						//** Send mail to admin when user clicks on verify link
						if( !empty($encryptedUserId) ){
							$templateContent = $this->EmailTemplate->find("first", array("conditions"=> array("name"=> 'User Active Promt to Admin')));
							$params['temp'] = $templateContent['EmailTemplate']['content'];
							$params['toMail'] = array('contact@theoncallroom.com', 'shamsher@mediccreations.com', 'anil@mediccreations.com');
							$params['from'] = NO_REPLY_EMAIL;
							$params['name'] = $userDetails['UserProfile']['first_name'] . ' ' . $userDetails['UserProfile']['last_name'];
							$params['email'] = $userDetails['User']['email'];
							$params['siteUrl'] = BASE_URL. 'admin/';
							$params['country'] = '';
							if(!empty($userDetails['UserProfile']['country_id'])){
								$country = $this->Country->find("first", array("conditions"=> array("id"=> $userDetails['UserProfile']['country_id'])));
								$params['country'] = $country['Country']['country_name'];
							}
							try{
								$mailSend = $this->Email->userActivationPromtToAdmin( $params );
								$this->EmailSmsLog->save( array("type"=> "email", "msg"=> $mailSend, "user_id"=> 0, "notify_type"=> "User Active Promt to Admin") );
							}catch(Exception $e){}
						}
					}
				//}
			}
		}
		}
		 $this->set('emailVerifyMsg', $msg);
	}

	/*
	-------------------------------------------------------------------------------------
	On: 04-08-2015
	I/P: $activationKey
	O/P: Key activation Message
	Desc: verify mail by user for pre approved domains (like doctors.net, theoncallroom.com etc)
	-------------------------------------------------------------------------------------
	*/
	public function verifyMailPreApproved( $activationKey = NULL , $encryptedUserId = NULL ){ 
		$this->layout = '';
		$msg = 'Wrong Key';
		if( !empty( $activationKey ) ){
			$activationStatus = $this->User->activationKeyStatus( $activationKey ); 
			if( !empty($activationStatus) ){ 
				if( $activationStatus['User']['status'] == 0 ){
					//$msg = 'Email Already Verified.';
				//}else {
					$keyUpdate = $this->User->activateKey( $activationKey );
					if($keyUpdate){
						$msg = "Email Verified";
						$userId = (int) base64_decode($encryptedUserId);
						//** Approve user [START]
							$conditions = array('User.id'=> $userId);
							$data = array(
								'User.approved'=> 1
							);
							$this->User->updateAll( $data ,$conditions );
						//** Approve user [END]

						//** Send Welcome mail and auto approve by admin
							$userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $userId)));
							$templateContent = $this->EmailTemplate->find("first", array("conditions"=> array("name"=> 'User Approve By Admin')));
							$params['temp'] = $templateContent['EmailTemplate']['content'];
							$params['toMail'] = $userDetails['User']['email'];
							$params['name'] = $userDetails['UserProfile']['first_name'];
							$params['from'] = NO_REPLY_EMAIL;
							$params['regards'] = "SITE_NAME";
							try{
								$mailSend = $this->Email->userApproveByAdmin( $params );
								$this->EmailSmsLog->save( array("type"=> "email", "msg"=> $mailSend, "user_id"=> $userId, "notify_type"=> "User Approve By Admin") );
							}catch(Exception $e){
								$this->EmailSmsLog->save( array("type"=> "email", "msg"=> $e->getMessage(), "user_id"=> $userId, "notify_type"=> "User Approve By Admin") );
							}
					}
				//}
			}
		}
		}
		 $this->set('emailVerifyMsg', $msg);
	}

	/*
	-------------------------------------------------------------------------------------
	On: 21-01-2016
	I/P: 
	O/P: 
	Desc: Send APP download link to visitor
	-------------------------------------------------------------------------------------
	*/
	public function sendAppDownloadLink(  ){ echo "chk here";die;
		if( !empty($visitorEmail) ){
			$templateContent = $this->EmailTemplate->find("first", array("conditions"=> array("name"=> 'APP Download')));
			$params['temp'] = $templateContent['EmailTemplate']['content'];
			$params['toMail'] = $visitorEmail;
			$params['from'] = NO_REPLY_EMAIL;
			$params['regards'] = "SITE_NAME";
			$appLinkEmail = $this->Email->appDownloadLinkMail( $params );
			$this->EmailSmsLog->save( array("type"=> "email", "msg"=> $mailSend, "user_id"=> $userId, "notify_type"=> "App Download") );
			try{
				$data = array("user_email"=> $visitorEmail);
				$appLinkData = $this->AppDownloadSendEmail->save( $data );
			}catch(Exception $e){}
		}
		$this->Session->setFlash('Thanks! APP Link has been sent to your email.');
		$this->redirect(BASE_URL);
	}

	/*
	-------------------------------------------------------------------------------------
	On: 21-01-2016
	I/P: 
	O/P: 
	Desc: Send mail when user fills contact us page
	-------------------------------------------------------------------------------------
	*/
	public function sendContactUs(){ 
			session_start();

if( !empty($this->request->data) && $_SESSION['captcha_code']==$this->request->data['contact']['captcha']){
			$templateContent = $this->EmailTemplate->find("first", array("conditions"=> array("name"=> 'Contact Us')));
			$params['temp'] = $templateContent['EmailTemplate']['content'];
			$params['toMail'] = 'contact@theoncallroom.com';
			//$params['toMail'] = 'shashi@mediccreations.com';
			$params['name'] = $this->request->data['contact']['name'];
			$params['from'] = $this->request->data['contact']['email'];
			$params['subject'] = $this->request->data['contact']['subject'];
			$params['message'] = $this->request->data['contact']['message'];
			$params['regards'] = "SITE_NAME"; 
			$mailSend = $this->Email->contactUsMail( $params );
			$this->EmailSmsLog->save( array("type"=> "email", "msg"=> $mailSend, "user_id"=> (int) 0, "notify_type"=> "Contact Us") );
			//** Contact us response to visitor
			$templateContent = array();
			$templateContent = $this->EmailTemplate->find("first", array("conditions"=> array("name"=> 'contact us to visitor')));
			$paramsVisitor['temp'] = $templateContent['EmailTemplate']['content'];
			$paramsVisitor['toMail'] = $this->request->data['contact']['email'];
			$paramsVisitor['from'] = NO_REPLY_EMAIL;
			$paramsVisitor['name'] = $this->request->data['contact']['name'];
			$paramsVisitor['regards'] = "SITE_NAME";
			$mailSendToVisitor = $this->Email->contactUsMailResponseToVisitor( $paramsVisitor );
			$this->EmailSmsLog->save( array("type"=> "email", "msg"=> $mailSendToVisitor, "user_id"=> (int) 0, "notify_type"=> "Contact Us to Visitor") );
			/*try{
				$data = array("user_email"=> $this->request->data['contact']['email']);
				$appLinkData = $this->AppDownloadSendEmail->save( $data );
			}catch(Exception $e){}*/
		}else{
			//$this->Session->setFlash(__('Please enter right captch value', true));
			$this->redirect(BASE_URL.'contact/?captcha=1');
		}
	}

	/*
	-----------------------------------------------------------------------------------------
	On: 14-03-2016
	I/P:
	O/P:
	Desc: Product Tour Page
	-----------------------------------------------------------------------------------------
	*/
	public function productTour($callFromDevice = NULL){
		$pageTitle = "Product Tour";
		if($callFromDevice == "mobile" ){
			$this->layout = 'layout_without_header_footer';
			$pageTitle = "";
		}
		//$faqContent = $this->Content->find("first", array("conditions"=> array("content_title"=> "Product Tour", "content_status"=> 1)));
		$this->set('pageTitle', $pageTitle);
		//$this->set('faqContent', $faqContent);
	}

	/*
	-----------------------------------------------------------------------------------------
	On: 14-03-2016
	I/P:
	O/P:
	Desc: Quick Start Page
	-----------------------------------------------------------------------------------------
	*/
	public function quickStart($callFromDevice = NULL){
		$pageTitle = "Quick Start";
		if($callFromDevice == "mobile" ){
			$this->layout = 'layout_without_header_footer';
			$pageTitle = "";
		}
		//$faqContent = $this->Content->find("first", array("conditions"=> array("content_title"=> "Quick Start", "content_status"=> 1)));
		$this->set('pageTitle', $pageTitle);
		//$this->set('faqContent', $faqContent);
	}

	/*
	-----------------------------------------------------------------------------------------
	On: 14-03-2016
	I/P:
	O/P:
	Desc: Other Apps Page
	-----------------------------------------------------------------------------------------
	*/
	public function otherApps($callFromDevice = NULL){
		$pageTitle = "Our Other Apps";
		if($callFromDevice == "mobile" ){
			$this->layout = 'layout_without_header_footer';
			$pageTitle = "";
		}
		//$faqContent = $this->Content->find("first", array("conditions"=> array("content_title"=> "Our Other Apps", "content_status"=> 1)));
		$this->set('pageTitle', $pageTitle);
		//$this->set('faqContent', $faqContent);
	}

	/*
	-----------------------------------------------------------------------------------------
	On: 14-03-2016
	I/P:
	O/P:
	Desc: Other Apps Page
	-----------------------------------------------------------------------------------------
	*/
	public function press($callFromDevice = NULL){
		$pageTitle = "Press";
		if($callFromDevice == "mobile" ){
			$this->layout = 'layout_without_header_footer';
			$pageTitle = "";
		}
		//$faqContent = $this->Content->find("first", array("conditions"=> array("content_title"=> "Press", "content_status"=> 1)));
		$this->set('pageTitle', $pageTitle);
		//$this->set('faqContent', $faqContent);
	}

	/*
	-----------------------------------------------------------------------------------------
	On: 14-03-2016
	I/P:
	O/P:
	Desc: Student Ambassador
	-----------------------------------------------------------------------------------------
	*/
	public function studentAmbassador($callFromDevice = NULL){
		$pageTitle = "Become a Student Ambassador";
		if($callFromDevice == "mobile" ){
			$this->layout = 'layout_without_header_footer';
			$pageTitle = "";
		}
		//$faqContent = $this->Content->find("first", array("conditions"=> array("content_title"=> "Become a Student Ambassador", "content_status"=> 1)));
		$this->set('pageTitle', $pageTitle);
		//$this->set('faqContent', $faqContent);
	}

	/*
	-----------------------------------------------------------------------------------------
	On: 14-03-2016
	I/P:
	O/P:
	Desc: Support Page
	-----------------------------------------------------------------------------------------
	*/
	public function support($callFromDevice = NULL){
		$pageTitle = "Support";
		if($callFromDevice == "mobile" ){
			$this->layout = 'layout_without_header_footer';
			$pageTitle = "";
		}
		//$faqContent = $this->Content->find("first", array("conditions"=> array("content_title"=> "Support", "content_status"=> 1)));
		$this->set('pageTitle', $pageTitle);
		//$this->set('faqContent', $faqContent);
	}

	/*
	-----------------------------------------------------------------------------------------
	On: 14-03-2016
	I/P:
	O/P:
	Desc: How you can use page
	-----------------------------------------------------------------------------------------
	*/
	public function howYouCanUseOcr($callFromDevice = NULL){
		$pageTitle = "How You Can Use The OCR";
		if($callFromDevice == "mobile" ){
			$this->layout = 'layout_without_header_footer';
			$pageTitle = "";
		}
		//$faqContent = $this->Content->find("first", array("conditions"=> array("content_title"=> "How You Can Use OCR", "content_status"=> 1)));
		$this->set('pageTitle', $pageTitle);
		//$this->set('faqContent', $faqContent);
	}

	/*
	-----------------------------------------------------------------------------------------
	On: 15-03-2016
	I/P:
	O/P:
	Desc: Send mail when visitors fills join team form
	-----------------------------------------------------------------------------------------
	*/
	public function joinTeam(){ //echo "<pre>"; print_r($_FILES);die;
		if(isset($this->request->data) && !empty($this->request->data)){
			$allowedFileTypes = array('doc','docx','pdf','rtf','odt','txt');
			$fileArr = explode('.', $this->request->data['contact']['file']['name']);
			$fileExt = end($fileArr);
			if(in_array($fileExt, $allowedFileTypes)){
				$params['name'] = $this->request->data['contact']['name'];
				$params['email'] = $this->request->data['contact']['email'];
				$params['toMail'] = array('anil@mediccreations.com', 'yamini@mediccreations.com');
				$params['fromMail'] = $this->request->data['contact']['email'];
				$params['subject'] = "Visitor CV";
				$params['file_name'] = "Visitor CV";
				$params['file_path'] = $_FILES['data']['tmp_name']['contact']['file'];
				$params['file_type'] = $_FILES['data']['type']['contact']['file'];
				//** Get message from DB
				$templateContent = $this->EmailTemplate->find("first", array("conditions"=> array("name"=> 'Join Team To Admin')));
				$params['temp'] = $templateContent['EmailTemplate']['content'];
				//** Send mail to admin
				$msg = $this->Email->visitorJoinTeamToAdmin( $params );
			}else{
				$msg =  "Invalid File Type";
			}
		}
		if($msg == "Mail Sent"){
			$this->Session->setFlash(__('Thanks! Your CV has been sent.', null), 
                            'default', 
                             array('class' => 'flash-message-success'));
		}elseif($msg == "Mail Not Sent"){
			$this->Session->setFlash(__('Sorry! Please try again.', null), 
                            'default', 
                             array('class' => 'flash-message-error'));
		}elseif($msg == "Invalid File Type"){
			$this->Session->setFlash(__('File Type Not supported', null), 
                            'default', 
                             array('class' => 'flash-message-error'));
		}elseif($msg == "Exception Occured"){
			$this->Session->setFlash(__('Some Exception occured. Please try again.', null), 
                            'default', 
                             array('class' => 'flash-message-error'));
		}
		else{
			$this->Session->setFlash(__('Please Try Again.', null), 
                            'default', 
                             array('class' => 'flash-message-error'));
		}
		 $this->redirect(
            array('controller' => 'index', 'action' => 'joinTheTeam')
        );
	}

	/*
	-----------------------------------------------------------------------------------------
	On: 14-03-2016
	I/P:
	O/P:
	Desc: Support Page
	-----------------------------------------------------------------------------------------
	*/
	public function healthcheck(){
		echo "Your instance health check status passed";
		exit();
	}

}
