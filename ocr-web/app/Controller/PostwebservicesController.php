<?php
/*
 * Post controller.
 *
 * This file will render views from views/postwebservices/
 *
 
 */

App::uses('AppController', 'Controller');

class PostwebservicesController extends AppController {
	public $uses = array('User', 'UserPost', 'UserPostAttribute','UserPostSpecilities','Specilities', 'UserPostComment', 'UserPostBeat', 'UserPostShare', 'ConsentForm', 'DebugTest', 'TopStoryPoint', 'UserPostSpam', 'NotificationUser', 'UserNotificationSetting', 'NotificationLog', 'PostUserTag', 'HashTag', 'BeatHashMapping', 'PostSpamContent', 'UserColleague', 'UserFollow', 'EmailTemplate', 'EmailSmsLog', 'UserSpecilities', 'UserInterest', 'NotificationLastVisit', 'NotificationQueque');
	public $components = array('Common', 'Image', 'Email');

	/*
	------------------------------------------------------------------------
	On: 12-08-2015
	I/P: token, user_id, device_id
	O/P: JSON data
	Desc: Method will return JSON data of user posts with post attributes. 
	------------------------------------------------------------------------
	*/
	public function postLists(){
		$this->autoRender = false;
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ; 
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true); 
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				$pageSize = (isset($dataInput['size']) && !empty($dataInput['size'])) ? $dataInput['size'] : DEFAULT_PAGE_SIZE;
				$pageNumber = (isset($dataInput['page_number']) && !empty($dataInput['page_number'])) ? $dataInput['page_number'] : 1;
				$postId = isset($dataInput['post_id']) ? $dataInput['post_id'] : '';
				$sortBy = isset($dataInput['sort']) ? $dataInput['sort'] : 'latest';
				if( isset($dataInput['order_by']) && $dataInput['order_by'] == 'top_stories' ){
					$userPosts = $this->postsByTopStories( $pageSize, $pageNumber ,''); 
				}elseif( isset($dataInput['order_by']) && $dataInput['order_by'] == 'top_followed' ){
					//$userPosts = $this->UserPost->userPostLists( $pageSize, $pageNumber , NULL, $postId, $sortBy, 'top_followed' ); 
					$loggedinUserId = $dataInput['user_id'];
					$userPosts = $this->UserPost->followedPostLists( $pageSize, $pageNumber , $loggedinUserId, $postId, $sortBy, '' );
				}
				else{
					$userPosts = $this->UserPost->userPostLists( $pageSize, $pageNumber , NULL, $postId, $sortBy ); 
				}
				if( !empty($userPosts) ){
					$loggedinUserId = isset($dataInput['user_id']) ? $dataInput['user_id'] : '';//** User id of loggedin user
					$userPostsData = $this->getFields( $userPosts , $loggedinUserId); 
					//** Add shared post information
					if( !empty($userPostsData) ){
						foreach( $userPostsData as $upd){
							$sharedPostsData = array(); $postDetails = array(); $sharedData = array(); $parentPostDetails = array(); $parentPostsData = array();
							$postData = $this->UserPost->findById( $upd["post_id"] );
							//** Original Post information
							if( !empty($postData['UserPost']['original_post_id']) ){
								$postDetails = $this->UserPost->postListById( $postData['UserPost']['original_post_id'] );
								$sharedPostsData = $this->getFields( $postDetails , $loggedinUserId);
							}
							//** Parent post details
							if( !empty($postData['UserPost']['parent_post_id']) ){
								$parentPostDetails = $this->UserPost->postListById( $postData['UserPost']['parent_post_id'] );
								$parentPostsData = $this->getFields( $parentPostDetails , $loggedinUserId);
								//** If post is shared
								$sharedData = array( "original_post"=> !empty($sharedPostsData[0]) ? (object) $sharedPostsData[0] : $sharedPostsData[0], "parent_post"=> !empty($parentPostsData[0]) ? (object) $parentPostsData[0] : $parentPostsData[0] );
								$upd["shared"] = $sharedData;
							}
							//** Final array to create JSON
							$userPostWithSharedData[] = $upd;
							$upd = array();
						}
					}else{
						$userPostWithSharedData = $userPostsData;
					}
					$responseData = array('method_name'=> 'postLists', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> array("Posts"=> $userPostWithSharedData));
				}else{
					$responseData = array('method_name'=> 'postLists', 'status'=>"0", 'response_code'=> "613", 'message'=> ERROR_613);
				}
			}else{
				$responseData = array('method_name'=> 'postLists', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'postLists', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		//echo json_encode($responseData);
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	----------------------------------------
	On: 13-08-2015
	I/P: $userPosts = array()
	O/P: $userPostsData = array()
	Desc: 
	----------------------------------------
	*/
	public function getFields( $userPosts = array() , $loggedinUserId = NULL){ 
		$userPostsData = array();
		if( $loggedinUserId == -1 ){// ** Guest User
			$userPostsData = $this->beatFieldsForGuestUser( $userPosts );
		}else{
			foreach( $userPosts as $up ){
					//** Get attribute data
					$attrData = array();
					foreach( $up['UserPostAttribute'] as $postAttr ){
						if( $postAttr['attribute_type'] == 'text' || $postAttr['attribute_type'] == 'video' ){
							$content = $postAttr['content']; 
						}
						else if( $postAttr['attribute_type'] == 'image'){ 
							$content = AMAZON_PATH . 'posts/'.$postAttr['user_post_id'].'/image/'.$postAttr['content']; 
						}
						else{	
							$content = AMAZON_PATH . 'posts/'.$postAttr['user_post_id'].'/doc/'.$postAttr['content']; 
						}
						/*$content = str_replace(
						 array("\r\n", "\n", "\r"), 
						 '',
						 $content
						);*/
							$content = str_replace(
							 array("<br>", "<br >", "< br >"), 
							 "\n",
							 $content
							);
						$attrData[] = array(
							"attr_id"=> $postAttr['id'],
							"attribute_type"=> $postAttr['attribute_type'],
							"content"=> $content,
							);
					}

					//** Get Beat User Tagged
					$taggedUsers = $this->PostUserTag->postTaggedUsers( $up['UserPost']['id'] );
					$postTaggedUsers = array();
					foreach( $taggedUsers as $tagUser ){
						$postTaggedUsers[] = array(
													"user_id"=> !empty($tagUser['User']['id']) ? $tagUser['User']['id'] : '',
													"first_name"=> !empty($tagUser['UserProfile']['first_name']) ? $tagUser['UserProfile']['first_name'] : '',
													"last_name"=> !empty($tagUser['UserProfile']['last_name']) ? $tagUser['UserProfile']['last_name'] : '',
													"profile_img"=> !empty($tagUser['UserProfile']['profile_img']) ? AMAZON_PATH . $tagUser['UserProfile']['user_id']. '/profile/' . $tagUser['UserProfile']['profile_img'] : ''
												);
					}
					//** Get Specility
					$specilities = $this->Specilities->allSpecilities('','all');
					foreach($specilities as $specVals){
						$specilitiesData[$specVals['Specilities']['id']][] = array('id'=> !empty($specVals['Specilities']['id']) ? $specVals['Specilities']['id'] : '', 'name'=> !empty($specVals['Specilities']['name']) ? $specVals['Specilities']['name'] : '', 'img_name'=> $specVals['Specilities']['img_name']);
					}
					$specilitiesArr = array();
					foreach( $up['UserPostSpecilities'] as $postSpec ){
						if(!empty($specilitiesData[$postSpec['specilities_id']][0]['id'])){
						$specilitiesArr[] = array(
											'id'=> !empty($specilitiesData[$postSpec['specilities_id']][0]['id']) ? $specilitiesData[$postSpec['specilities_id']][0]['id'] : 0,
											'name'=> !empty($specilitiesData[$postSpec['specilities_id']][0]['name']) ? $specilitiesData[$postSpec['specilities_id']][0]['name'] : '',
											'icon'=> SPECILITY_ICON_PATH.$specilitiesData[$postSpec['specilities_id']][0]['img_name']
											);
						}
					}
					//** Get Post up/down beat info
					$upBeatCount = 0; $downBeatCount =0; $shareCount = 0; //** Initialize variables
					$upBeatCount = $this->UserPostBeat->find("count", array("conditions"=> array("post_id"=> $up['UserPost']['id'], "beat_type"=> 1)));
					$downBeatCount = $this->UserPostBeat->find("count", array("conditions"=> array("post_id"=> $up['UserPost']['id'], "beat_type"=> 0)));
					//** Get Beat status of user who has posted Article
					$userBeatData = $this->UserPostBeat->find("first", array("conditions"=> array("post_id"=> $up['UserPost']['id'], "user_id"=> $loggedinUserId)));
					$userBeatAction =  ( isset($userBeatData['UserPostBeat']['beat_type']) && ((int) $userBeatData['UserPostBeat']['beat_type'] == 0 || (int) $userBeatData['UserPostBeat']['beat_type'] == 1 )) ? (int) $userBeatData['UserPostBeat']['beat_type'] : 2;
					//** Share Count
					$shareCount = $this->UserPost->find("count", array("conditions"=> array("parent_post_id"=> $up['UserPost']['id'])));
					$postData['post_id'] = !empty($up['UserPost']['id']) ? $up['UserPost']['id'] : '';
					$postData['post_title'] = !empty($up['UserPost']['title']) ? $up['UserPost']['title'] : '';
					$postData['post_datetime'] = !empty($up['UserPost']['post_date']) ? $up['UserPost']['post_date'] : '';
					$postData['is_anonymous'] = !empty($up['UserPost']['is_anonymous']) ? (int) $up['UserPost']['is_anonymous'] : 0;
					$postData['markBeat'] = array("upBeatcount"=> $upBeatCount, "downBeatcount"=> $downBeatCount, "userAction"=> $userBeatAction);
					//** Get Beat comment count
					$beatCommentCount = $this->UserPostComment->find("count",array("conditions"=> array("user_post_id"=> $up['UserPost']['id'], "status"=> 1)));
					$postData['comment_count'] = $beatCommentCount;
					$postData['share_count'] = $shareCount;
					if( $up['UserPost']['is_anonymous'] == 1 ){
						$postData['user'] = array("user_id"=> 0, "first_name"=> 'OCR', "last_name"=> 'Member' , "profile_img"=> AMAZON_PATH );
					}else{
						$postData['user'] = array("user_id"=> !empty($up['UserProfile']['user_id']) ? $up['UserProfile']['user_id'] : '', "first_name"=> !empty($up['UserProfile']['first_name']) ? $up['UserProfile']['first_name'] : '', "last_name"=> !empty($up['UserProfile']['last_name']) ? $up['UserProfile']['last_name'] :'' , "profile_img"=> !empty($up['UserProfile']['profile_img']) ? AMAZON_PATH . $up['UserProfile']['user_id']. '/profile/' . $up['UserProfile']['profile_img'] : '');
					}
					$postData['attributes'] = $attrData;
					$postData['post_speciality'] = $specilitiesArr;
					$postData['is_shared'] = !empty($up['UserPost']['parent_post_id']) ? 1 : 0;
					$postData['tagged_users'] = $postTaggedUsers;
					//** User action(loggedin user) on beat like beat comment, beat shared ,beat spammed
					$userActionBeatComment = 0; $userActionBeatShared = 0; $userActionBeatSpammed = 0;
					if(!empty($loggedinUserId)){
						//** Check loggedin user posted comment on beat
						$postCommentCount = $this->UserPostComment->find("count", array("conditions"=> array("user_post_id"=> $up['UserPost']['id'], "user_id"=> $loggedinUserId,"status"=> 1)));
						if($postCommentCount > 0){
							$userActionBeatComment = 1;
						}
						//** Check loggedin user shared beat
						$shareBeatByUser = $this->UserPost->find("count", array("conditions"=> array("user_id"=> $loggedinUserId, "parent_post_id"=> $up['UserPost']['id'], "status"=> 1)));
						if($shareBeatByUser > 0){
							$userActionBeatShared = 1;
						}
						//** Check loggedin user spammed beat
						$spamBeatByUser = $this->UserPostSpam->find("count", array("conditions"=> array("reported_by"=> $loggedinUserId, "user_post_id"=> $up['UserPost']['id'])));
						if($spamBeatByUser > 0){
							$userActionBeatSpammed = 1;
						}
					}
					$postData['user_action'] = array("beat_comment"=> $userActionBeatComment, "beat_shared"=> $userActionBeatShared, "beat_spammed"=> $userActionBeatSpammed);
					//** Final Beat Lists
					$userPostsData[] = $postData;
			}
		}
		return $userPostsData;		
	}

	/*
	-----------------------------------------------------------------------------
	On: 21-08-2015
	I/P: Headers: {access_key, token} , 
		 JSON Input {"post_id":"","size":"","page_number":""}
	O/P: JSON data
	Desc: Input JSON as parameters and returns all Post Comments as JSON format.
	-----------------------------------------------------------------------------
	*/
	public function getPostComment(){
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true);
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				$pageSize = (isset($dataInput['size']) && !empty($dataInput['size'])) ? $dataInput['size'] : DEFAULT_PAGE_SIZE;
				$pageNumber = (isset($dataInput['page_number']) && !empty($dataInput['page_number'])) ? $dataInput['page_number'] : 1;
				$postComment = $this->UserPostComment->postComments( $dataInput['post_id'], $pageSize, $pageNumber);	
				$postCommentData = array();
				if( !empty($postComment)){
					foreach( $postComment as $comment ){
						$profile = array(); $postComments = array(); // Initialize variables
						//** Get User profile
						$profile = array('user_id'=> $comment['UserPostComment']['user_id'], 'first_name'=> $comment['UserProfile']['first_name'], 'last_name'=> $comment['UserProfile']['last_name'], 'profile_img'=> !empty($comment['UserProfile']['profile_img']) ? AMAZON_PATH . $comment['UserPostComment']['user_id']. '/profile/' . $comment['UserProfile']['profile_img']:'');
						//** Get Post Comment Info
						$postComments = array('id'=> $comment['UserPostComment']['id'], 'user_post_id'=> $comment['UserPostComment']['user_post_id'],'content'=> $comment['UserPostComment']['content'],'created'=> $comment['UserPostComment']['created']);
						$postCommentData[] = array('UserProfile'=> $profile, 'UserPostComment'=> $postComments);
					}
				}
					//** Get total post comment count
      				$postCommentCount = $this->UserPostComment->find("count", array('conditions'=> array('user_post_id'=> $dataInput['post_id'])));		
					//** Get Upbeat Count
					$upBeatCount = 0;
					$upBeatCount = $this->UserPostBeat->find("count", array("conditions"=> array("post_id"=> $dataInput['post_id'], "beat_type"=> 1)));
					//** Beat Title
					$beatDetails = $this->UserPost->find("first", array("conditions"=>array( "UserPost.id"=> $dataInput['post_id'])));
					$postComments = array("comments"=> $postCommentData, "totalComment"=> $postCommentCount, 'post_title'=> !empty($beatDetails['UserPost']['title']) ? $beatDetails['UserPost']['title'] : '', 'up_beat_count'=>$upBeatCount, "pageSize"=> $pageSize);	
					$responseData = array('method_name'=> 'getPostComment', 'status'=>"1", 'response_code'=> "200", 'data'=> $postComments);
				/*}else{
					$responseData = array('method_name'=> 'getPostComment', 'status'=>"0", 'response_code'=> "613", 'message'=> ERROR_613);	
				}*/
			}else{
				$responseData = array('method_name'=> 'getPostComment', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'getPostComment', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		//echo json_encode($responseData);
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	------------------------------------------------------------------------------------
	On: 07-09-2015
	I/P: HEADER: {"acess_key", "token"}, JSON ("user_id":"","post_id":"", "comment":"") 
	O/P: JSON data
	Desc: If access_key and token validated then user can Add post comment using API.
	------------------------------------------------------------------------------------
	*/
	public function addPostComment(){
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true);
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				if( $this->User->findById( $dataInput['user_id'] )){
					if( $this->UserPost->findById( $dataInput['post_id']) ){
						try{
							$userPostCommentData = array("user_post_id"=> (int) $dataInput['post_id'], "user_id"=> (int) $dataInput['user_id'], "content"=> $dataInput['comment'], "status"=> 1);
							$addComment = $this->UserPostComment->save( $userPostCommentData );
							if( $addComment ){
								//** Get Post Comment added by user
								$comment = $this->UserPostComment->findById( $this->UserPostComment->id );
      							//** Get total post comment count
      							$postCommentCount = $this->UserPostComment->find("count", array('conditions'=> array('user_post_id'=> $dataInput['post_id'])));
								//** Set JSON values
								$postComments = array('id'=> $comment['UserPostComment']['id'], 'post_id'=> $comment['UserPostComment']['user_post_id'], 'content'=> $comment['UserPostComment']['content'],'created'=> $comment['UserPostComment']['created'], 'totalComment'=> $postCommentCount);
								$postCommentData["comments"] = array('UserPostComment'=> $postComments);
								$responseData = array('method_name'=> 'addPostComment', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, 'data'=> $postCommentData);
								//** Send Notification [START]
								$postDetails = $this->UserPost->findById( $dataInput['post_id'] );
								$postByUserId = $postDetails['UserPost']['user_id'];
								$notificationSetting = $this->UserNotificationSetting->find("count", array("conditions"=> array("user_id"=> $postByUserId, "status"=> 1 )));
								
									$userDetails = $this->User->findById( $dataInput['user_id'] );
									$userDevices = $this->NotificationUser->find("all", array("conditions"=> array("user_id"=> $postByUserId, "status"=> 1)));
									$userRegisteredDevices = array();
									foreach( $userDevices as $userDevice ){
										$userRegisteredDevices[] = $userDevice['NotificationUser']['device_reg_id'];
									}
									if(!empty($postDetails['UserPost']['title'])){
										$message = $userDetails['UserProfile']['first_name'] . " ". $userDetails['UserProfile']['last_name'] ." commented on ". $postDetails['UserPost']['title'];
									}else{
										$message = $userDetails['UserProfile']['first_name'] . " ". $userDetails['UserProfile']['last_name'] ." commented on Your beat";
									}
									try{
											$notifyResponse = ""; $notificationId = 0; $totUnreadNotificationCount = 0;
											if($dataInput['user_id'] !=$postByUserId){ //** Don't save self notification
												$notifyLogData = array("by_user_id"=> $dataInput['user_id'], "to_user_id"=> $postByUserId, "item_id"=> $dataInput['post_id'] ,"notify_type"=> "comment" ,"content"=> $message);
												$this->NotificationLog->save( $notifyLogData );
												$notificationId = $this->NotificationLog->getLastInsertId();
											}
											if( ($notificationSetting == (int) 0) && ($dataInput['user_id'] != $postByUserId) ){ //** Check user notification setting ON/OFF and not to self user
												//$totUnreadNotificationCount = $this->NotificationLog->find("count", array("conditions"=> array("to_user_id"=> $postByUserId, "read_status"=> 0)));
												$unreadNotificationsData = $this->NotificationLastVisit->unreadNotificationCountAfterLastVisited($postByUserId);
												$totUnreadNotificationCount = $unreadNotificationsData[0][0]['totUnreadNotification'];
												$notifyResponse = $this->Common->sendGcmMessage( $userRegisteredDevices, array("notification_id"=> $notificationId, "message"=> $message, "item_id"=> $dataInput['post_id'] ,"notify_type"=> "comment", "tot_unread_notification"=> $totUnreadNotificationCount) );
											}
											$this->NotificationLog->updateAll(array("api_response"=> "'". $notifyResponse ."'"), array("id"=> $notificationId));
										}catch( Exception $e ){
											if($dataInput['user_id'] != $postByUserId){
												$notifyLogData = array("by_user_id"=> $dataInput['user_id'], "to_user_id"=> $postByUserId, "item_id"=> $dataInput['post_id'] ,"notify_type"=> "comment" ,"content"=> $message ,"api_response"=> $e->getMessage() );
												$this->NotificationLog->save( $notifyLogData );
											}
										}
								
								//** Send Notification [END]
							}else{
								$responseData = array('method_name'=> 'addPostComment', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615);
							}
						}catch(Exception $e){
							$responseData = array('method_name'=> 'addPostComment', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615);
						}
					}else{
						$responseData = array('method_name'=> 'addPostComment', 'status'=>"0", 'response_code'=> "619", 'message'=> ERROR_619);
					}
				}else{
					$responseData = array('method_name'=> 'addPostComment', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
				}
			}else{
				$responseData = array('method_name'=> 'addPostComment', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'addPostComment', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		//echo json_encode($responseData);
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	-----------------------------------------------------
	On: 09-09-2015
	I/P: array()
	O/P: JSON response
	Desc: Mark a post Up beat/Down Beat by any user
	-----------------------------------------------------
	*/
	public function markBeatUpDown(){
		$responseData = array();
		if($this->request->is('post')) {
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				$dataInput = $this->request->input ( 'json_decode', true) ;
				$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
				$dataInput = json_decode($encryptedData, true);
				if( $this->User->find("count", array("conditions"=>array("User.id"=> (int) $dataInput['user_id']))) > 0){
					if( $this->UserPost->find("count", array("conditions"=>array("UserPost.id"=> (int) $dataInput['post_id']))) ){
						try{
							if( $this->UserPostBeat->find('count', array('conditions'=> array("user_id"=> (int) $dataInput['user_id'], "post_id"=> (int) $dataInput['post_id']))) > 0 ){
								$updateBeat = $this->UserPostBeat->updateAll( array("beat_type"=>(int) $dataInput['beat_type']), array("user_id"=> (int) $dataInput['user_id'], "post_id"=> (int) $dataInput['post_id']));
								$upBeatCount = $this->UserPostBeat->find('count', array('conditions'=>array('post_id'=>(int) $dataInput['post_id'], "beat_type"=>(int) 1)));
								$downBeatCount = $this->UserPostBeat->find('count', array('conditions'=>array('post_id'=>(int) $dataInput['post_id'], "beat_type"=>(int) 0)));
								//** Get Beat status of user who has posted Article
								$userBeatData = $this->UserPostBeat->find("first", array("conditions"=> array("post_id"=> $dataInput['post_id'], "user_id"=> $dataInput['user_id'])));
								$userBeatAction =  ( isset($userBeatData['UserPostBeat']['beat_type']) && ((int) $userBeatData['UserPostBeat']['beat_type'] == 0 || (int) $userBeatData['UserPostBeat']['beat_type'] == 1 )) ? (int) $userBeatData['UserPostBeat']['beat_type'] : 2;
								if( $updateBeat ){
									$responseData = array('method_name'=> 'markBeatUpDown', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, 'data'=> array('markBeat'=> array('upBeatcount'=> $upBeatCount, 'downBeatcount'=> $downBeatCount, "userAction"=> $userBeatAction)));
								}else{
									$responseData = array('method_name'=> 'markBeatUpDown', 'status'=>"1", 'response_code'=> "615", 'message'=> ERROR_615);
								}
							}else{
								$userPostBeat = array("beat_type"=> (int) $dataInput['beat_type'], "user_id"=> (int) $dataInput['user_id'], "post_id"=> (int) $dataInput['post_id'], "status"=> 1);
								$saveBeat = $this->UserPostBeat->save( $userPostBeat );
								//** Get Beat status of user who has posted Article
								$userBeatData = $this->UserPostBeat->find("first", array("conditions"=> array("post_id"=> $dataInput['post_id'], "user_id"=> $dataInput['user_id'])));
								$userBeatAction =  ( isset($userBeatData['UserPostBeat']['beat_type']) && ((int) $userBeatData['UserPostBeat']['beat_type'] == 0 || (int) $userBeatData['UserPostBeat']['beat_type'] == 1 )) ? (int) $userBeatData['UserPostBeat']['beat_type'] : 2;
								if( $saveBeat ){
									$upBeatCount = $this->UserPostBeat->find('count', array('conditions'=>array('post_id'=>(int) $dataInput['post_id'], "beat_type"=>(int) 1)));
									$downBeatCount = $this->UserPostBeat->find('count', array('conditions'=>array('post_id'=>(int) $dataInput['post_id'], "beat_type"=>(int) 0)));
									$responseData = array('method_name'=> 'markBeatUpDown', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, 'data'=> array('markBeat'=> array('upBeatcount'=> $upBeatCount, 'downBeatcount'=> $downBeatCount, "userAction"=> $userBeatAction)));
								}else{
									$responseData = array('method_name'=> 'markBeatUpDown', 'status'=>"1", 'response_code'=> "615", 'message'=> ERROR_615);
								}
							}
						}catch(Exception $e){
							$responseData = array('method_name'=> 'markBeatUpDown', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615);
						}
						//** Send Notification [START]
								$postDetails = $this->UserPost->findById( $dataInput['post_id'] );
								$postByUserId = $postDetails['UserPost']['user_id'];
								$notificationSetting = $this->UserNotificationSetting->find("count", array("conditions"=> array("user_id"=> $postByUserId, "status"=> 1 )));
								
									$userDetails = $this->User->findById( $dataInput['user_id'] );
									$userDevices = $this->NotificationUser->find("all", array("conditions"=> array("user_id"=> $postByUserId, "status"=> (int) 1)));
									$userRegisteredDevices = array();
									foreach( $userDevices as $userDevice ){
										$userRegisteredDevices[] = $userDevice['NotificationUser']['device_reg_id'];
									}
									if( $dataInput['beat_type'] == 1 ){
										if(!empty($postDetails['UserPost']['title'])){
											$message = "Yay! ". $postDetails['UserPost']['title'] ." got an Upbeat from ". $userDetails['UserProfile']['first_name'] . " ". $userDetails['UserProfile']['last_name'] ;
										}else{
											$message = "Yay! Your beat got an Upbeat from ". $userDetails['UserProfile']['first_name'] . " ". $userDetails['UserProfile']['last_name'] ;
										}
										$notifyType = "upbeat";
										$alreadyUpBeatCount = $this->NotificationLog->find("count", array("conditions"=> array("by_user_id"=> $dataInput['user_id'], "item_id"=> $dataInput['post_id'] ,"notify_type"=> $notifyType)));
										if($alreadyUpBeatCount == 0){ // ** Don't send multiple upbeat notification by same user on same beat
											try{
												$notifyResponse = ""; $notificationId = 0; $totUnreadNotificationCount = 0;
												if($dataInput['user_id'] !=$postByUserId){ //** Don't save self notification action
													$notifyLogData = array("by_user_id"=> $dataInput['user_id'], "to_user_id"=> $postByUserId, "item_id"=> $dataInput['post_id'] ,"notify_type"=> $notifyType ,"content"=> $message );
													$this->NotificationLog->save( $notifyLogData );
													$notificationId = $this->NotificationLog->getLastInsertId();
												}
												if( ($notificationSetting == (int) 0) && ($dataInput['user_id'] !=$postByUserId) ){ //** Check user notification setting ON/OFF and not to self user
													//$totUnreadNotificationCount = $this->NotificationLog->find("count", array("conditions"=> array("to_user_id"=> $postByUserId, "read_status"=> 0)));
													$unreadNotificationsData = $this->NotificationLastVisit->unreadNotificationCountAfterLastVisited($postByUserId);
													$totUnreadNotificationCount = $unreadNotificationsData[0][0]['totUnreadNotification'];
													$messagedata = array("notification_id"=> $notificationId,"message"=> $message, "notify_type"=> $notifyType, "item_id"=> $dataInput['post_id'], "tot_unread_notification"=> $totUnreadNotificationCount);
													$notifyResponse = $this->Common->sendGcmMessage( $userRegisteredDevices, $messagedata );
												}
												$this->NotificationLog->updateAll(array("api_response"=> "'". $notifyResponse ."'"), array("id"=> $notificationId));
											}catch( Exception $e ){
												if($dataInput['user_id'] != $postByUserId){
													$notifyLogData = array("by_user_id"=> $dataInput['user_id'], "to_user_id"=> $postByUserId, "item_id"=> $dataInput['post_id'] ,"notify_type"=> $notifyType ,"content"=> $message ,"api_response"=> $e->getMessage() );
													$this->NotificationLog->save( $notifyLogData );
												}
											}
										}
									}else if( $dataInput['beat_type'] == 0 ){
										if(!empty($postDetails['UserPost']['title'])){
											$message = "Uh Oh! ". $postDetails['UserPost']['title'] ." got a Downbeat from ". $userDetails['UserProfile']['first_name'] . " ". $userDetails['UserProfile']['last_name'] ;
										}else{
											$message = "Uh Oh! Your beat got a Downbeat from ". $userDetails['UserProfile']['first_name'] . " ". $userDetails['UserProfile']['last_name'] ;
										}
										$notifyType = "downbeat";
										$alreadyDownBeatCount = $this->NotificationLog->find("count", array("conditions"=> array("by_user_id"=> $dataInput['user_id'], "item_id"=> $dataInput['post_id'] ,"notify_type"=> $notifyType)));
										if($alreadyDownBeatCount == 0){ // ** Don't send multiple downbeat notification by same user on same beat
											try{
												$notifyResponse = "";$notificationId = 0; $totUnreadNotificationCount = 0;
												if($dataInput['user_id'] !=$postByUserId){ //** Don't save self notification action
													$notifyLogData = array("by_user_id"=> $dataInput['user_id'], "to_user_id"=> $postByUserId, "item_id"=> $dataInput['post_id'] ,"notify_type"=> $notifyType ,"content"=> $message );
													$this->NotificationLog->save( $notifyLogData );
													$notificationId = $this->NotificationLog->getLastInsertId();
												}
												if( ($notificationSetting == (int) 0) && ($dataInput['user_id'] !=$postByUserId) ){ //** Check user notification setting ON/OFF and not to self user
													//$totUnreadNotificationCount = $this->NotificationLog->find("count", array("conditions"=> array("to_user_id"=> $postByUserId, "read_status"=> 0)));
													$unreadNotificationsData = $this->NotificationLastVisit->unreadNotificationCountAfterLastVisited($postByUserId);
													$totUnreadNotificationCount = $unreadNotificationsData[0][0]['totUnreadNotification'];
													$messagedata = array("notification_id"=> $notificationId, "message"=> $message, "notify_type"=> $notifyType, "item_id"=> $dataInput['post_id'], "tot_unread_notification"=> $totUnreadNotificationCount);
													$notifyResponse = $this->Common->sendGcmMessage( $userRegisteredDevices, $messagedata );
												}
												$this->NotificationLog->updateAll(array("api_response"=> "'". $notifyResponse ."'"), array("id"=> $notificationId));
											}catch( Exception $e ){
												if($dataInput['user_id'] != $postByUserId){
													$notifyLogData = array("by_user_id"=> $dataInput['user_id'], "to_user_id"=> $postByUserId, "item_id"=> $dataInput['post_id'] ,"notify_type"=> $notifyType ,"content"=> $message ,"api_response"=> $e->getMessage() );
													$this->NotificationLog->save( $notifyLogData );
												}
											}
										}
									}
									
								
								//** Send Notification [END]
					}else{
						$responseData = array('method_name'=> 'markBeatUpDown', 'status'=>"0", 'response_code'=> "619", 'message'=> ERROR_619);
					}
				}else{
					$responseData = array('method_name'=> 'markBeatUpDown', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
				}
			}else{
				$responseData = array('method_name'=> 'markBeatUpDown', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'markBeatUpDown', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		//echo json_encode($responseData);
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	-----------------------------------------------------------------------
	On: 17-09-2015
	I/P: array()
	O/P: JSON data
	Desc: data of userPost list by any particular user
	-----------------------------------------------------------------------
	*/
	public function postListByUser(){
		$this->autoRender = false;
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true); 
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				$sortBy = isset($dataInput['sort']) ? $dataInput['sort'] : 'latest';
				$pageSize = isset($dataInput['size']) ? $dataInput['size'] : 15;
				$pageNum = isset($dataInput['page_number']) ? $dataInput['page_number'] : 1;
				$userPosts = $this->UserPost->userPostLists( $pageSize, $pageNum, $dataInput['user_id'], $dataInput['post_id'], $sortBy );
					if( !empty($userPosts) ){
						$userPostsData = $this->getFieldsInterestListByUser( $userPosts ,$dataInput['loggedin_user_id']);
						//** Add shared post information
					if( !empty($userPostsData) ){
						foreach( $userPostsData as $upd){
							$sharedPostsData = array(); $postDetails = array(); $sharedData = array(); $parentPostDetails = array(); $parentPostsData = array();
							$postData = $this->UserPost->findById( $upd["post_id"] );
							//** Original Post information
							if( !empty($postData['UserPost']['original_post_id']) ){
								$postDetails = $this->UserPost->postListById( $postData['UserPost']['original_post_id'] );
								$sharedPostsData = $this->getFields( $postDetails , $loggedinUserId);
							}
							//** Parent post details
							if( !empty($postData['UserPost']['parent_post_id']) ){
								$parentPostDetails = $this->UserPost->postListById( $postData['UserPost']['parent_post_id'] );
								$parentPostsData = $this->getFields( $parentPostDetails , $loggedinUserId);
								//** If post is shared
								$sharedData = array( "original_post"=> !empty($sharedPostsData[0]) ? (object) $sharedPostsData[0] : $sharedPostsData[0], "parent_post"=> !empty($parentPostsData[0]) ? (object) $parentPostsData[0] : $parentPostsData[0] );
								$upd["shared"] = $sharedData;
							}
							//** Final array to create JSON
							$userPostWithSharedData[] = $upd;
							$upd = array();
						}
					}else{
						$userPostWithSharedData = $userPostsData;
					}
						$responseData = array('method_name'=> 'postListByUser', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> array("Posts"=> $userPostWithSharedData));
				}else{
						$responseData = array('method_name'=> 'postListByUser', 'status'=>"0", 'response_code'=> "613", 'message'=> ERROR_613);
				}
			}else{
				$responseData = array('method_name'=> 'postListByUser', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'postListByUser', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		//echo json_encode($responseData);
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	----------------------------------------------------------------------------------------------
	On: 
	I/P:
	O/P:
	Desc: 
	----------------------------------------------------------------------------------------------
	*/
	public function getFieldsInterestListByUser( $userPosts = array() , $loggedinUserId = NULL){ 
		$userPostsData = array();
		
			foreach( $userPosts as $up ){
					//** Get attribute data
					$attrData = array();
					foreach( $up['UserPostAttribute'] as $postAttr ){
						if( $postAttr['attribute_type'] == 'text' ){
							$content = $postAttr['content']; 
						}
						else if( $postAttr['attribute_type'] == 'image'){ 
							$content = AMAZON_PATH . 'posts/'.$postAttr['user_post_id'].'/image/'.$postAttr['content']; 
						}
						else{	
							$content = AMAZON_PATH . 'posts/'.$postAttr['user_post_id'].'/doc/'.$postAttr['content']; 
						}
						/*$content = str_replace(
						 array("\r\n", "\n", "\r"), 
						 '',
						 $content
						);*/
							$content = str_replace(
							 array("<br>", "<br >", "< br >"), 
							 "\n",
							 $content
							);
						$attrData[] = array(
							"attr_id"=> $postAttr['id'],
							"attribute_type"=> $postAttr['attribute_type'],
							"content"=> $content,
							);
					}

					//** Get Beat User Tagged
					$taggedUsers = $this->PostUserTag->postTaggedUsers( $up['UserPost']['id'] );
					$postTaggedUsers = array();
					foreach( $taggedUsers as $tagUser ){
						$postTaggedUsers[] = array(
													"user_id"=> !empty($tagUser['User']['id']) ? $tagUser['User']['id'] : '',
													"first_name"=> !empty($tagUser['UserProfile']['first_name']) ? $tagUser['UserProfile']['first_name'] : '',
													"last_name"=> !empty($tagUser['UserProfile']['last_name']) ? $tagUser['UserProfile']['last_name'] : '',
													"profile_img"=> !empty($tagUser['UserProfile']['profile_img']) ? AMAZON_PATH . $tagUser['UserProfile']['user_id']. '/profile/' . $tagUser['UserProfile']['profile_img'] : ''
												);
					}
					//** Get Specility
					$specilities = $this->Specilities->allSpecilities('','all');
					foreach($specilities as $specVals){
						$specilitiesData[$specVals['Specilities']['id']][] = array('id'=> !empty($specVals['Specilities']['id']) ? $specVals['Specilities']['id'] : '', 'name'=> !empty($specVals['Specilities']['name']) ? $specVals['Specilities']['name'] : '', 'img_name'=> $specVals['Specilities']['img_name']);
					}
					$specilitiesArr = array();
					foreach( $up['UserPostSpecilities'] as $postSpec ){
						if(!empty($specilitiesData[$postSpec['specilities_id']][0]['id'])){
						$specilitiesArr[] = array(
											'id'=> !empty($specilitiesData[$postSpec['specilities_id']][0]['id']) ? $specilitiesData[$postSpec['specilities_id']][0]['id'] : 0,
											'name'=> !empty($specilitiesData[$postSpec['specilities_id']][0]['name']) ? $specilitiesData[$postSpec['specilities_id']][0]['name'] : '',
											'icon'=> SPECILITY_ICON_PATH.$specilitiesData[$postSpec['specilities_id']][0]['img_name']
											);
						}
					}
					//** Get Post up/down beat info
					$upBeatCount = 0; $downBeatCount =0; $shareCount = 0; //** Initialize variables
					$upBeatCount = $this->UserPostBeat->find("count", array("conditions"=> array("post_id"=> $up['UserPost']['id'], "beat_type"=> 1)));
					$downBeatCount = $this->UserPostBeat->find("count", array("conditions"=> array("post_id"=> $up['UserPost']['id'], "beat_type"=> 0)));
					//** Get Beat status of user who has posted Article
					$userBeatData = $this->UserPostBeat->find("first", array("conditions"=> array("post_id"=> $up['UserPost']['id'], "user_id"=> $loggedinUserId)));
					$userBeatAction =  ( isset($userBeatData['UserPostBeat']['beat_type']) && ((int) $userBeatData['UserPostBeat']['beat_type'] == 0 || (int) $userBeatData['UserPostBeat']['beat_type'] == 1 )) ? (int) $userBeatData['UserPostBeat']['beat_type'] : 2;
					//** Share Count
					$shareCount = $this->UserPost->find("count", array("conditions"=> array("parent_post_id"=> $up['UserPost']['id'])));
					$postData['post_id'] = !empty($up['UserPost']['id']) ? $up['UserPost']['id'] : '';
					$postData['post_title'] = !empty($up['UserPost']['title']) ? $up['UserPost']['title'] : '';
					$postData['post_datetime'] = !empty($up['UserPost']['post_date']) ? $up['UserPost']['post_date'] : '';
					$postData['is_anonymous'] = !empty($up['UserPost']['is_anonymous']) ? (int) $up['UserPost']['is_anonymous'] : 0;
					$postData['markBeat'] = array("upBeatcount"=> $upBeatCount, "downBeatcount"=> $downBeatCount, "userAction"=> $userBeatAction);
					//** Get Beat comment count
					$beatCommentCount = $this->UserPostComment->find("count",array("conditions"=> array("user_post_id"=> $up['UserPost']['id'], "status"=> 1)));
					$postData['comment_count'] = $beatCommentCount;
					$postData['share_count'] = $shareCount;
					if( $up['UserPost']['is_anonymous'] == 1 ){
						$postData['user'] = array("user_id"=> 0, "first_name"=> 'OCR', "last_name"=> 'Member' , "profile_img"=> AMAZON_PATH );
					}else{
						$postData['user'] = array("user_id"=> !empty($up['UserProfile']['user_id']) ? $up['UserProfile']['user_id'] : '', "first_name"=> !empty($up['UserProfile']['first_name']) ? $up['UserProfile']['first_name'] : '', "last_name"=> !empty($up['UserProfile']['last_name']) ? $up['UserProfile']['last_name'] :'' , "profile_img"=> !empty($up['UserProfile']['profile_img']) ? AMAZON_PATH . $up['UserProfile']['user_id']. '/profile/' . $up['UserProfile']['profile_img'] : '');
					}
					$postData['attributes'] = $attrData;
					$postData['post_speciality'] = $specilitiesArr;
					$postData['is_shared'] = !empty($up['UserPost']['parent_post_id']) ? 1 : 0;
					$postData['tagged_users'] = $postTaggedUsers;
					//** User action(loggedin user) on beat like beat comment, beat shared ,beat spammed
					$userActionBeatComment = 0; $userActionBeatShared = 0; $userActionBeatSpammed = 0;
					if(!empty($loggedinUserId)){
						//** Check loggedin user posted comment on beat
						$postCommentCount = $this->UserPostComment->find("count", array("conditions"=> array("user_post_id"=> $up['UserPost']['id'], "user_id"=> $loggedinUserId,"status"=> 1)));
						if($postCommentCount > 0){
							$userActionBeatComment = 1;
						}
						//** Check loggedin user shared beat
						$shareBeatByUser = $this->UserPost->find("count", array("conditions"=> array("user_id"=> $loggedinUserId, "parent_post_id"=> $up['UserPost']['id'], "status"=> 1)));
						if($shareBeatByUser > 0){
							$userActionBeatShared = 1;
						}
						//** Check loggedin user spammed beat
						$spamBeatByUser = $this->UserPostSpam->find("count", array("conditions"=> array("reported_by"=> $loggedinUserId, "user_post_id"=> $up['UserPost']['id'])));
						if($spamBeatByUser > 0){
							$userActionBeatSpammed = 1;
						}
					}
					$postData['user_action'] = array("beat_comment"=> $userActionBeatComment, "beat_shared"=> $userActionBeatShared, "beat_spammed"=> $userActionBeatSpammed);
					//** Final Beat Lists
					//** Check if post is anonymous only loggedin user can see beat
					if( $up['UserPost']['is_anonymous'] == 1){
						if($up['UserPost']['user_id'] == $loggedinUserId){
							$userPostsData[] = $postData;
						}
					}else{
						$userPostsData[] = $postData;
					}
			}
		return $userPostsData;		
	}
	/*
	------------------------------------------------------------
	On: 14-09-2015
	I/P: array()
	O/P: JSON
	Desc: add post with specilities and attributes.
	------------------------------------------------------------ 
	*/
	public function addPost(){
		$this->autoRender = false;
		$responseData = array();
		if($this->request->is('post')) {
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				$dataInput = json_decode($_REQUEST['data'], true); 
				$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
				$dataInput = json_decode($encryptedData, true);
				if( $this->User->find("count", array("conditions"=> array("User.id"=> $dataInput['user_id']))) > 0 ){ 
				//** Add post
				$postData = array("title"=> $dataInput['post_title'], "user_id"=> $dataInput['user_id'], "is_anonymous"=> isset($dataInput['is_anonymous'])?$dataInput['is_anonymous']:0, "consent_id"=> isset($dataInput['consent_id'])?$dataInput['consent_id']:0, "status"=> 1 );
				try{
					$addPost = $this->UserPost->save( $postData );
					$postId = $this->UserPost->id;
					if( $addPost ){
						$responseData = array('method_name'=> 'addPost', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200);
						//** Add post specilities
						if( !empty($dataInput['post_speciality']) ){
							foreach( $dataInput['post_speciality'] as $specility){
								$postSpecilities = array("user_post_id"=> $postId, "specilities_id"=> $specility);
								$this->UserPostSpecilities->saveAll( $postSpecilities );
							}
						}
						//** Add post attributes which Not includes attachment
						if( !empty($dataInput['attributes']) ){
							foreach( $dataInput['attributes'] as $attributes){
								if( $attributes['attribute_type'] == "text" || $attributes['attribute_type'] == "video" ){
									$postAttributes = array("user_post_id"=> $postId, "attribute_type"=> $attributes['attribute_type'], "content"=> $attributes['content'], "status"=> 1);
									$this->UserPostAttribute->saveAll( $postAttributes );
								}
								//** Add Hash Tag
								try{
									$this->addHashTag( $attributes['content'], $postId );
								}catch( Exception $e ){

								}
							}
						}
						//** Add attributes images
						if( isset($dataInput['images']) && !empty($dataInput['images']) ){
							foreach( $dataInput['images'] as $imgs){
								$imgName = ''; $imageName = '';
								$imgName = $postId.'_'.strtotime(date("Y-m-d H:i:s")).rand(1,1000);
								$imageName = $this->Image->createimage($imgs['img'],WWW_ROOT . 'img/uploader_tmp/', $imgName, $imgs['img_ext']);
								if( !empty($imageName) ){
									$this->Image->moveImageToAmazon( array('img_path'=> WWW_ROOT . 'img/uploader_tmp/'.$imageName, 'img_name'=> 'posts/'.$postId.'/image/'.$imageName) );
									$postAttributes = array("user_post_id"=> $postId, "attribute_type"=> 'image', "content"=> $imageName, "status"=> 1);
									$this->UserPostAttribute->saveAll( $postAttributes );
								}
								//** Remove physiacal file from Temp Directory
								if( file_exists( WWW_ROOT . 'img/uploader_tmp/'.$imageName )){
									chmod(WWW_ROOT . 'img/uploader_tmp/'.$imageName, 777);
									unlink( WWW_ROOT . 'img/uploader_tmp/'.$imageName );
								}
							}
						}
						//** Add post attributes includes attachment
						if( isset($_FILES) && !empty($_FILES) ){
							//** Add doc attributes
							$docCount = 1;
							if( isset( $dataInput['doc_count']) &&  $dataInput['doc_count'] > 0 ){
								for($cnt=0; $cnt < $dataInput['doc_count']; $cnt++ ){
									$postAttributes = array();
									$fileNameDoc = $this->Image->uploadFileTempLocation( 'posts/'.$postId.'/doc/', $_FILES, 'docfileUpload_'.$cnt);
									$fileExt = explode(".", $fileNameDoc);
									$postAttributes = array("user_post_id"=> $postId, "attribute_type"=> end($fileExt), "content"=> $fileNameDoc, "status"=> 1);
									$this->UserPostAttribute->saveAll( $postAttributes );
									//** Remove physiacal file from Temp Directory
									$this->Common->removeTempFile( WWW_ROOT . 'img/uploader_tmp/'.$fileNameDoc );
								}
							}
						}		
					}else{
						$responseData = array('method_name'=> 'addPost', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615);
					}
				}catch( Exception $e ){
					$responseData = array('method_name'=> 'addPost', 'status'=>"0", 'response_code'=> "615", 'system_errors'=> $e->getMessage());
				}
				//** Add tagged users
				$dataInput['tagged_users'] = isset($dataInput['tagged_users']) ? $dataInput['tagged_users'] : array();
				try{
					$this->addTaggedUser( $postId, array_unique($dataInput['tagged_users']), $dataInput['user_id'], $dataInput['post_title'], $dataInput['is_anonymous']);
				}catch( Exception $e ){}
				
				//** Add information to send notification[START]
					$notificationData = array(
											"item_id"=> $postId,
											"user_id"=> $dataInput['user_id'],
											"type"=> 'addPost',
										);
					$addNotificationInfo = $this->NotificationQueque->save($notificationData);
				//** Add information to send notification[END]
			}
			else{
				$responseData = array('method_name'=> 'addPost', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
			}
			}else{
				$responseData = array('method_name'=> 'addPost', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'addPost', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		//echo json_encode($responseData);
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	----------------------------------------------------------
	On: 15-09-2015
	I/P: array()
	O/P: JSON data as response
	Desc: User can share any post.
	----------------------------------------------------------
	*/
	public function sharePost(){
		$responseData = array();
		if($this->request->is('post')) {
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				$dataInput = $this->request->input ( 'json_decode', true) ; 
				$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
				$dataInput = json_decode($encryptedData, true);
				if( $this->User->findById( $dataInput['user_id'] )){
					if( $this->UserPost->findById( $dataInput['post_id']) ){
						$postDetails = $this->UserPost->findById( $dataInput['post_id'] ); //echo "<pre>";print_r($postDetails);die;
						//if( (int) $postDetails['UserPost']['user_id'] != (int) $dataInput['user_id']){ // User can't share self post
							$text = (isset($dataInput['text']) && !empty($dataInput['text']) ? $dataInput['text']:'');
							$sharePostData = array("post_id"=> $dataInput['post_id'], "shared_from"=> $dataInput['user_id'],"status"=> 1);
							try{
								if( $this->UserPostShare->save( $sharePostData ) ){
									//** Add into user_post table
									$sharedData = array("user_id"=> $dataInput['user_id'], "title"=> $postDetails['UserPost']['title'], "status"=> 1, "is_anonymous"=> 0, "parent_post_id"=> $dataInput['post_id'], "original_post_id"=> $dataInput['original_post_id']);
									$this->UserPost->save( $sharedData );
									$sharedPostId = $this->UserPost->getLastInsertId();
									//** Add into user_post_attributes table
									if( !empty($text) ){ //** If sharing has text
										$userPostAttributes = array("user_post_id"=> $this->UserPost->id, "attribute_type"=> "text", "content"=> $text, "status"=> 1);
										$this->UserPostAttribute->save( $userPostAttributes );
									}
									//** Count total shared of post
									$totSharedCount = $this->UserPost->find("count", array("conditions"=> array("parent_post_id"=> $dataInput['post_id'])));
									$sharedCount = array('shared_count'=> $totSharedCount);
									$responseData = array('method_name'=> 'sharePost', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, 'data'=> $sharedCount);
									//** Add information to send notification[START]
										$notificationData = array(
										"item_id"=> $sharedPostId,
										"user_id"=> $dataInput['user_id'],
										"type"=> 'sharedPost',
										);
										$addNotificationInfo = $this->NotificationQueque->save($notificationData);
									//** Add information to send notification[END]
									
								}else{
									$responseData = array('method_name'=> 'sharePost', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615);	
								}
							}catch(Exception $e){
								$responseData = array('method_name'=> 'sharePost', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615);	
							}
						/*}else{
							$responseData = array('method_name'=> 'sharePost', 'status'=>"0", 'response_code'=> "620", 'message'=> ERROR_620);
						}*/
					}else{
						$responseData = array('method_name'=> 'sharePost', 'status'=>"0", 'response_code'=> "619", 'message'=> ERROR_619);
					}
				}else{
					$responseData = array('method_name'=> 'sharePost', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
				}
			}else{
				$responseData = array('method_name'=> 'sharePost', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'sharePost', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		//echo json_encode($responseData);
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	------------------------------------------------------------------------
	On: 22-09-2015
	I/P: token, post_id
	O/P: JSON data
	Desc: Method will return JSON data of user posts with post attributes. 
	------------------------------------------------------------------------
	*/
	public function latestPostLists(){
		$this->autoRender = false;
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ; 
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true);
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				$pageSize = (isset($dataInput['size']) && !empty($dataInput['size'])) ? $dataInput['size'] : DEFAULT_PAGE_SIZE; 
				$pageNumber = (isset($dataInput['page_number']) && !empty($dataInput['page_number'])) ? $dataInput['page_number'] : 1;
				$sortBy = isset($dataInput['sort']) ? $dataInput['sort'] : 'latest';
				$userPosts = $this->UserPost->userPostLists( $pageSize, $pageNumber ,NULL, (int) $dataInput['post_id'], $sortBy ); //echo '<pre>';print_r($userPosts);die;
				if( !empty($userPosts) ){
					$loggedinUserId = $dataInput['user_id'];
					$userPostsData = $this->getFields( $userPosts, $loggedinUserId );
					$responseData = array('method_name'=> 'latestPostLists', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> array("Posts"=> $userPostsData));
				}else{
					$responseData = array('method_name'=> 'latestPostLists', 'status'=>"0", 'response_code'=> "613", 'message'=> ERROR_613);
				}
			}else{
				$responseData = array('method_name'=> 'latestPostLists', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'latestPostLists', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		//echo json_encode($responseData);
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	------------------------------------------------------------------------
	On: 09-10-2015
	I/P: $interestid
	O/P: JSON data
	Desc: Method will return JSON data of user posts with post attributes search by interest id. 
	------------------------------------------------------------------------
	*/
	public function postListsByInterest(){
		$this->autoRender = false;
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true); 
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				$pageSize = (isset($dataInput['size']) && !empty($dataInput['size'])) ? $dataInput['size'] : DEFAULT_PAGE_SIZE; 
				$pageNumber = (isset($dataInput['page_number']) && !empty($dataInput['page_number'])) ? $dataInput['page_number'] : 1;
				$interestId = isset($dataInput['interest_id']) ? (int) $dataInput['interest_id'] : '';
				$postId = isset($dataInput['post_id']) ? (int) $dataInput['post_id'] : '';
				$sortBy = isset($dataInput['sort']) ? $dataInput['sort'] : 'latest';
				$orderBySort = isset($dataInput['order_by']) ? $dataInput['order_by'] : '';
				if( isset($dataInput['order_by']) && $dataInput['order_by'] == 'top_stories'){
					$userPosts = $this->postsByTopStories( $pageSize, $pageNumber , $interestId); 
				}elseif( isset($dataInput['order_by']) && $dataInput['order_by'] == 'top_followed' ){
					$userPosts = $this->UserPost->followedPostLists( $pageSize, $pageNumber , $dataInput['user_id'], $postId, $sortBy ,$interestId);
				}else{
					$userPosts = $this->UserPost->userPostListsByInterest( $pageSize, $pageNumber , $interestId, $postId, $sortBy, $orderBySort );
				}
				if( !empty($userPosts) ){
					$loggedinUserId = $dataInput['user_id'];
					$userPostsData = $this->getFields( $userPosts, $loggedinUserId ); 
					$responseData = array('method_name'=> 'postListsByInterest', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> array("Posts"=> $userPostsData));
				}else{
					$responseData = array('method_name'=> 'postListsByInterest', 'status'=>"0", 'response_code'=> "613", 'message'=> ERROR_613);
				}
			}else{
				$responseData = array('method_name'=> 'postListsByInterest', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'postListsByInterest', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		//echo json_encode($responseData);
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	On: 27-10-2015 
	I/P: 
	O/P: 
	Desc: Fetches post list according to top stories
	*/
	public function postsByTopStories( $pageSize = DEFAULT_PAGE_SIZE, $pageNum = 1 , $interestId = NULL){
		$offsetVal = ( $pageNum - 1 ) * $pageSize;
		$userPosts = array();
		//** Post Share Point
		$sharePointData = $this->TopStoryPoint->find("first", array("conditions"=> array("name"=> "Top Shared")));
		$sharePoint = $sharePointData['TopStoryPoint']['point'];
		//** Post Up Beat Point
		$upbeatPointData = $this->TopStoryPoint->find("first", array("conditions"=> array("name"=> "Up Beat")));
		$upbeatPoint = $upbeatPointData['TopStoryPoint']['point'];
		//** Post Top Comment Point
		$topCommentPointData = $this->TopStoryPoint->find("first", array("conditions"=> array("name"=> "Top Comment")));
		$topCommentPoint = $topCommentPointData['TopStoryPoint']['point'];
		//** Post Top Follow Point
		$topFollowPointData = $this->TopStoryPoint->find("first", array("conditions"=> array("name"=> "Top Follow")));
		$topFollowPoint = $topFollowPointData['TopStoryPoint']['point'];
		//** Set Conditions  
		$conditions = "User.status = 1 AND UserPost.status = 1 AND UserPost.spam_confirmed = 0";
		if(isset($interestId) && !empty($interestId)){
			$conditions .= "  AND UserPostSpecilities.specilities_id = " . $interestId;
		}
			$quer = "SELECT UserPost.id,
				UserPost.title,
				UserPost.post_date,
	       		UserPost.parent_post_id,
	       		UserPost.original_post_id,
	       		UserPost.consent_id,
	       		UserProfile.user_id,
	       		UserProfile.first_name,
	       		UserProfile.last_name,
	       		UserProfile.profile_img,
	       		(SELECT count(UserPostShare.post_id)  FROM user_post_shares AS UserPostShare WHERE UserPostShare.post_id = UserPost.id) AS shareCount,
				(SELECT count(UserPostBeat.post_id)  FROM user_post_beats AS UserPostBeat WHERE UserPostBeat.post_id = UserPost.id AND UserPostBeat.beat_type = 1) AS beatCount,
				(SELECT count(UserPostComment.user_post_id)  FROM user_post_comments AS UserPostComment WHERE UserPostComment.user_post_id = UserPost.id AND UserPostComment.status = 1) AS commentCount,
				(
					(SELECT count(UserPostShare.post_id)  FROM user_post_shares AS UserPostShare WHERE UserPostShare.post_id = UserPost.id) * " . $sharePoint . " + 
					(SELECT count(UserPostBeat.post_id)  FROM user_post_beats AS UserPostBeat WHERE UserPostBeat.post_id = UserPost.id AND UserPostBeat.beat_type = 1) * " . $upbeatPoint . " + 
					(SELECT count(UserPostComment.user_post_id)  FROM user_post_comments AS UserPostComment WHERE UserPostComment.user_post_id = UserPost.id AND UserPostComment.status = 1) * " . $topCommentPoint . "
				) AS totPoint   
				FROM ( user_posts AS UserPost )
				LEFT JOIN ( user_post_shares AS UserPostShare )
				ON ( UserPost.id = UserPostShare.post_id )
				LEFT JOIN ( user_post_beats AS UserPostBeat )
				ON ( UserPost.id = UserPostBeat.post_id )
				LEFT JOIN ( user_post_comments AS UserPostComment )
				ON ( UserPost.id = UserPostComment.user_post_id )  
				LEFT JOIN (user_profiles AS UserProfile) 
				ON ( UserPost.user_id = UserProfile.user_id ) 
				LEFT JOIN user_post_specilities AS UserPostSpecilities 
				ON (UserPost.id = UserPostSpecilities.user_post_id)     
				INNER JOIN users AS User 
				ON ( UserPost.user_id = User.id ) 
				WHERE ". $conditions ."   
				GROUP BY UserPost.id ORDER BY totPoint DESC LIMIT " . $offsetVal . "," . $pageSize;
			App::import('model','UserPost');
			$UserPost = new UserPost();
			$userPosts = $UserPost->query( $quer ); 
			$userPostsData = array();
			foreach( $userPosts as $uPost){
				$userPostAttr = array(); $upSpecData = array();
				$userPostAttributes = $this->UserPostAttribute->find("all", array("conditions"=> array("user_post_id"=> $uPost['UserPost']['id'])));
				foreach( $userPostAttributes as $upAttr){ 
					$userPostAttr[] = $upAttr['UserPostAttribute'];
				}
				$userPostSpecilities = $this->UserPostSpecilities->find("all", array("conditions"=> array("user_post_id"=> $uPost['UserPost']['id'])));
				foreach( $userPostSpecilities as $upSpec){
					$upSpecData[] = $upSpec['UserPostSpecilities'];
				}
				$userPostsData[] = array('UserPost'=> $uPost['UserPost'], 'UserProfile'=> $uPost['UserProfile'], 'UserPostAttribute'=> $userPostAttr, 'UserPostSpecilities' => $upSpecData);
			}
			return $userPostsData;
	}
	
	/*
	On: 05-11-2015
	I/P: JSON (post_id, user_id)
	O/P: JSON ()
	Desc: User can mark as spam for any post.
	*/
	public function markPostSpam(){
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true);
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				if( $this->User->find("count", array("conditions"=> array("User.id"=>$dataInput['user_id']))) > 0){
					if( $this->UserPost->find("count", array("conditions"=> array("UserPost.id"=> $dataInput['post_id']))) >0 ){
						if( isset($dataInput['option_id']) && !empty($dataInput['option_id']) ){
							//** Check Spam post
							$text = isset($dataInput['text']) ? $dataInput['text'] : '';
							$alreadySpamCheck = array("user_post_id"=> $dataInput['post_id'], "reported_by"=> $dataInput['user_id']);
							if( $this->UserPostSpam->find("count", array("conditions"=> $alreadySpamCheck)) == 0 ){
								$spamPostData = array("user_post_id"=> $dataInput['post_id'], "reported_by"=> $dataInput['user_id'], "content"=> $text, "post_spam_content_id"=> $dataInput['option_id']);
								try{
									$spamPost = $this->UserPostSpam->save( $spamPostData );
									if($dataInput['option_id'] == 2){
										$this->UserPost->updateAll( array("UserPost.spam_confirmed"=> (int) 1), array("UserPost.id"=> $dataInput['post_id']) );
									}
										//** Mark spam beat
										/*$this->UserPost->updateAll( array("UserPost.spam_confirmed"=> (int) 1), array("UserPost.id"=> $dataInput['post_id']) );
										//** Get spam content (reason)
										$spamReason = $this->PostSpamContent->find("first", array("conditions"=> array("id"=> $dataInput['option_id'])));
										$userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $dataInput['user_id'])));
										$beatDetails = $this->UserPost->find("first", array("conditions"=> array("UserPost.id"=> $dataInput['post_id'])));
										$beatOwnerDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $beatDetails['UserPost']['user_id'])));
										//** Send e-mail to user who has post the beat (beat owner)
										$params = array();
										$params['subject'] = 'Your Beat marked as Spam';
										$templateContent = $this->EmailTemplate->find("first", array("conditions"=> array("name"=> 'Mark Spam Beat to beat owner')));
										$params['temp'] = $templateContent['EmailTemplate']['content'];
										$params['toMail'] = $userDetails['User']['email'];
										$params['from'] = NO_REPLY_EMAIL;
										$params['name'] = $userDetails['UserProfile']['first_name'] . " " . $spamBeat['UserProfile']['last_name'] ;
										$params['beat_title'] = $beatDetails['UserPost']['title'];
										$params['spam_reason'] = $spamReason['PostSpamContent']['spam_name'];
										if(!empty($params['toMail'])){
											$mailsendToBeatOwner = $this->Email->markSpamBeatEmailToBeatOwner( $params );
											$this->EmailSmsLog->save( array("type"=> "email", "msg"=> !empty($mailsendToBeatOwner) ? $mailsendToBeatOwner : '', "user_id"=> 0, "notify_type"=> "Beat Spam to Beat Owner") );
										}
										// ** Send email to admin emails ids when beat marked as spam
										$paramsAdmin = array();
										$paramsAdmin['subject'] = 'Beat marked as Spam';
										$templateContent = $this->EmailTemplate->find("first", array("conditions"=> array("name"=> 'Mark Spam Beat to Admin')));
										$paramsAdmin['temp'] = $templateContent['EmailTemplate']['content'];
										$paramsAdmin['toMail'] = array("contact@theoncallroom.com", "shamsher@mediccreations.com", "anil@mediccreations.com");
										$paramsAdmin['from'] = NO_REPLY_EMAIL;
										$paramsAdmin['name'] = $userDetails['UserProfile']['first_name'] . " " . $userDetails['UserProfile']['last_name'] ;
										$paramsAdmin['beat_title'] = $beatDetails['UserPost']['title'];
										$paramsAdmin['spam_reason'] = $spamReason['PostSpamContent']['spam_name'];
										$paramsAdmin['beat_owner_name'] = $beatOwnerDetails['UserProfile']['first_name'] . " " . $beatOwnerDetails['UserProfile']['last_name'] ;
										$paramsAdmin['spam_date'] = date('Y-m-d H:i:s');
										$paramsAdmin['site_url_path'] = BASE_URL . 'admin/UserPost/userPostLists';
										if(!empty($paramsAdmin['toMail'])){
												$mailSend = $this->Email->markSpamBeatEmailToAdmin( $paramsAdmin );
												$this->EmailSmsLog->save( array("type"=> "email", "msg"=> !empty($mailSend) ? $mailSend :'' , "user_id"=> 0, "notify_type"=> "Beat Spam to Admin") );
										}*/
									if( $spamPost ){
										$responseData = array('method_name'=> 'markPostSpam', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200);
									}else{
										$responseData = array('method_name'=> 'markPostSpam', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615);
									}
								}catch( Exception $e ){
									$responseData = array('method_name'=> 'markPostSpam', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
								}
							}else{
								$responseData = array('method_name'=> 'markPostSpam', 'status'=>"0", 'response_code'=> "627", 'message'=> ERROR_627);
							}
						}else{
							$responseData = array('method_name'=> 'markPostSpam', 'status'=>"0", 'response_code'=> "631", 'message'=> ERROR_631);
						}
					}else{
						$responseData = array('method_name'=> 'markPostSpam', 'status'=>"0", 'response_code'=> "619", 'message'=> ERROR_619);
					}
				}else{
					$responseData = array('method_name'=> 'markPostSpam', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
				}
			}else{
				$responseData = array('method_name'=> 'markPostSpam', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'markPostSpam', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	---------------------------------------------------------------------------------------
	On: 16-11-2015
	I/P: JSON (post_id)
	O/P: JSON (post details)
	Desc: Fetched post details for any particular post id
	---------------------------------------------------------------------------------------
	*/
	public function postDetailsById(){
		$this->autoRender = false;
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ; 
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true); 
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				$postId = isset($dataInput['post_id']) ? $dataInput['post_id'] : 0;
				$checkSpam = $this->UserPost->find("first", array("conditions"=> array("UserPost.id"=> $postId )));
				if( $checkSpam['UserPost']['status'] == 1 && $checkSpam['UserPost']['spam_confirmed'] == 0){
				$userPosts = $this->UserPost->postListById( $postId ); 
				if( !empty($userPosts) ){
					$loggedinUserId = isset($dataInput['user_id']) ? $dataInput['user_id'] : '';//** User id of loggedin user
					$userPostsData = $this->getFields( $userPosts , $loggedinUserId); 
					//** Add shared post information
					if( !empty($userPostsData) ){
						foreach( $userPostsData as $upd){
							$sharedPostsData = array(); $postDetails = array(); $sharedData = array(); $parentPostDetails = array(); $parentPostsData = array();
							$postData = $this->UserPost->findById( $upd["post_id"] );
							//** Original Post information
							if( !empty($postData['UserPost']['original_post_id']) ){
								$postDetails = $this->UserPost->postListById( $postData['UserPost']['original_post_id'] );
								$sharedPostsData = $this->getFields( $postDetails , $loggedinUserId);
							}
							//** Parent post details
							if( !empty($postData['UserPost']['parent_post_id']) ){
								$parentPostDetails = $this->UserPost->postListById( $postData['UserPost']['parent_post_id'] );
								$parentPostsData = $this->getFields( $parentPostDetails , $loggedinUserId);
								//** If post is shared
								$sharedData = array( "original_post"=> !empty($sharedPostsData[0]) ? (object) $sharedPostsData[0] : $sharedPostsData[0], "parent_post"=> !empty($parentPostsData[0]) ? (object) $parentPostsData[0] : $parentPostsData[0] );
								$upd["shared"] = $sharedData;
							}
							//** Final array to create JSON
							$userPostWithSharedData[] = $upd;
							$upd = array();
						}
					}else{
						$userPostWithSharedData = $userPostsData;
					}
					$responseData = array('method_name'=> 'postDetailsById', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> array("Posts"=> $userPostWithSharedData));
				}else{
					$responseData = array('method_name'=> 'postDetailsById', 'status'=>"0", 'response_code'=> "613", 'message'=> ERROR_613);
				}
				}else{
				$responseData = array('method_name'=> 'postDetailsById', 'status'=>"0", 'response_code'=> "635", 'message'=> ERROR_635);
			}
			}else{
				$responseData = array('method_name'=> 'postDetailsById', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'postDetailsById', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	------------------------------------------------------------------------------------------
	On: 18-11-2015 
	I/P: $postId = NULL, $taggedUsers = array()
	O/P: N/A
	Desc: Add tagged users during add post.
	------------------------------------------------------------------------------------------
	*/
	public function addTaggedUser( $postId = NULL, $taggedUsers = array(), $loggedinUserId = NULL, $postTitle = '', $isAnonymous = 0, $accessKey = ''){
		if( !empty($taggedUsers) && !empty($postId) ){
			foreach( $taggedUsers as $tagUser ){
				if( !empty($tagUser) && !empty($postId) ){
					$taggedUserData = array( "post_id"=> $postId, "user_id"=> $tagUser );
					$addTaggedUser = $this->PostUserTag->saveAll( $taggedUserData );
					/*
					//** Send Notification [START]
					$notificationSetting = $this->UserNotificationSetting->find("count", array("conditions"=> array("user_id"=> $tagUser, "status"=> 1 )));
					$userDetails = $this->User->findById( $loggedinUserId );
					$message = "";
					if($isAnonymous == 1){
						if(!empty($postTitle)){
							$message = "OCR Member tagged you in " . $postTitle;
						}else{
							$message = "OCR Member tagged you in a beat";
						}
					}else{
						if(!empty($postTitle)){
							$message = $userDetails['UserProfile']['first_name'] . " ". $userDetails['UserProfile']['last_name'] . " tagged you in " . $postTitle;
						}else{
							$message = $userDetails['UserProfile']['first_name'] . " ". $userDetails['UserProfile']['last_name'] . " tagged you in a beat";
						}
					}
					
						//** Initialize variables
						$notifyResponse = "";$notificationId = 0; $totUnreadNotificationCount = 0;
						$notifyLogData = array();
						try{
								
								if($loggedinUserId !=$tagUser){ //** don't save self notification
									$notifyLogData = array("by_user_id"=> $loggedinUserId, "to_user_id"=> $tagUser, "item_id"=> $postId ,"notify_type"=> "taggeduser" ,"content"=> $message );
									$checkExists = $this->NotificationLog->find("count", array("conditions"=> array("by_user_id"=> $loggedinUserId, "to_user_id"=> $tagUser, "item_id"=> $postId ,"notify_type"=> "taggeduser")));
									if( $checkExists == 0 ){
										$this->NotificationLog->saveAll( $notifyLogData );
										$notificationId = $this->NotificationLog->getLastInsertId();
									}
								}
								if( ($notificationSetting == (int) 0) && ($loggedinUserId !=$tagUser) ){ //** Check user notification setting ON/OFF and not to self user
									//$totUnreadNotificationCount = $this->NotificationLog->find("count", array("conditions"=> array("to_user_id"=> $tagUser, "read_status"=> 0)));
									$unreadNotificationsData = $this->NotificationLastVisit->unreadNotificationCountAfterLastVisited($postByUserId);
									$totUnreadNotificationCount = $unreadNotificationsData[0][0]['totUnreadNotification'];
									$notifyResponse = $this->Common->sendGcmMessage( $userRegisteredDevices, array("notification_id"=> $notificationId, "message"=> $message, "notify_type"=> "taggeduser" ,"item_id"=> $postId, "tot_unread_notification"=>$totUnreadNotificationCount) );
								}
									if(!empty($notificationId)){
										$this->NotificationLog->updateAll(array("api_response"=> "'". $notifyResponse ."'"), array("id"=> $notificationId));
									}
								//** Send New Beat Available GCM Message
									//** Below code is for android only
									if($loggedinUserId !=$tagUser){
										$notifyResponse = $this->Common->sendGcmMessage( $userAndroidDevice, array("message"=> "New Beat Available", "notify_type"=> "newBeatAvailable" ,"item_id"=> (int) $postId), array("priority"=> "normal", "content_available"=> false) );
									}
									//** Send New Beat Available push to devices
							}catch( Exception $e ){
								$notifyLogData = array("by_user_id"=> $loggedinUserId, "to_user_id"=> $tagUser, "item_id"=> $postId ,"notify_type"=> "taggeduser" ,"content"=> $message ,"api_response"=> $e->getMessage() );
								$checkExists = $this->NotificationLog->find("count", array("conditions"=> array("by_user_id"=> $loggedinUserId, "to_user_id"=> $tagUser, "item_id"=> $postId ,"notify_type"=> "taggeduser")));
								if( $checkExists == 0 ){
									$this->NotificationLog->saveAll( $notifyLogData );
								}
							}			
					//** Send Notification [END]
							*/
				}
			}	
		}	
	}

	/*
	------------------------------------------------------------------------------------------
	On: 18-11-2015 
	I/P: $content = NULL
	O/P: N/A
	Desc: Adding Unique Hash tag and hash post mapping.
	------------------------------------------------------------------------------------------
	*/
	public function addHashTag( $contents = NULL, $postId = NULL ){
		if( !empty($contents) ){
			$hashTags = $this->Common->getHashTags( $contents );
			foreach( $hashTags as $hashT ){
				$hashTag = str_replace("#", "", $hashT);
				$checkHashTag = $this->HashTag->find("count", array("conditions"=> array("name"=> ucfirst($hashTag))));
				if( $checkHashTag == 0 ){
					try{
						$this->HashTag->saveAll( array("name"=> ucfirst($hashTag)) );
						//** Save Beat with hash tag mapping
						$hashId = $this->HashTag->id;
						$hashPostData = array("post_id"=> $postId, "hash_id"=> $hashId);
						if( $this->BeatHashMapping->find("count", array("conditions"=> $hashPostData)) == 0 ){
							$this->BeatHashMapping->saveAll( $hashPostData );
						}
					}catch( Exception $e ){

					}
				}else{
					$getHashValue = $this->HashTag->find("first", array("conditions"=> array("name"=> ucfirst($hashTag))));
					//** Save Beat with hash tag mapping
					$hashId = $getHashValue['HashTag']['id'];
					$hashPostData = array("post_id"=> $postId, "hash_id"=> $hashId);
					if( $this->BeatHashMapping->find("count", array("conditions"=> $hashPostData)) == 0 ){
						$this->BeatHashMapping->saveAll( $hashPostData );
					}
				}
			}
		}
	}

	/*
	------------------------------------------------------------------------------------------
	On: 20-11-2015 
	I/P: JSON (textSearch)
	O/P: JSON (POST AND USER DETAILS)
	Desc: Fetching post details  according to text serach.
	------------------------------------------------------------------------------------------
	*/
	public function beatSearch(){
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true);
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				//** Search all posts
				$pageSize = !empty($dataInput['size']) ? $dataInput['size'] : 15;
				$pageNumber = !empty($dataInput['page_number']) ? $dataInput['page_number'] : 1;
				$searchText = !empty($dataInput['text']) ? $dataInput['text'] : '';
				$postId = !empty($dataInput['post_id']) ? $dataInput['post_id'] : '';
				$loggedinUserId = !empty($dataInput['user_id']) ? $dataInput['user_id'] : '';
				$postListBySearch = $this->UserPost->postSearch( $pageSize, $pageNumber, $searchText, $postId );
				$userPostsData = $this->getFields( $postListBySearch , $loggedinUserId);
				//** Add shared post information
					if( !empty($userPostsData) ){
						foreach( $userPostsData as $upd){
							$sharedPostsData = array(); $postDetails = array(); $sharedData = array(); $parentPostDetails = array(); $parentPostsData = array();
							$postData = $this->UserPost->findById( $upd["post_id"] );
							//** Original Post information
							if( !empty($postData['UserPost']['original_post_id']) ){
								$postDetails = $this->UserPost->postListById( $postData['UserPost']['original_post_id'] );
								$sharedPostsData = $this->getFields( $postDetails , $loggedinUserId);
							}
							//** Parent post details
							if( !empty($postData['UserPost']['parent_post_id']) ){
								$parentPostDetails = $this->UserPost->postListById( $postData['UserPost']['parent_post_id'] );
								$parentPostsData = $this->getFields( $parentPostDetails , $loggedinUserId);
								//** If post is shared
								$sharedData = array( "original_post"=> !empty($sharedPostsData[0]) ? (object) $sharedPostsData[0] : $sharedPostsData[0], "parent_post"=> !empty($parentPostsData[0]) ? (object) $parentPostsData[0] : $parentPostsData[0] );
								$upd["shared"] = $sharedData;
							}
							//** Final array to create JSON
							$userPostWithSharedData[] = $upd;
							$upd = array();
						}
						$responseData = array('method_name'=> 'beatSearch', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, 'data'=> array("Posts"=> $userPostWithSharedData));
					}else{
						//$userPostWithSharedData = $userPostsData;
						$responseData = array('method_name'=> 'beatSearch', 'status'=>"0", 'response_code'=> "613", 'message'=> ERROR_613);
					}
				
			}else{
				$responseData = array('method_name'=> 'beatSearch', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'beatSearch', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	----------------------------------------------------------------------------------
	On: 07-12-2015
	I/P: 
	O/P: 
	Desc: Formatting beat fields for guest user.
	----------------------------------------------------------------------------------
	*/
	public function beatFieldsForGuestUser( $userPosts = array() ){
		$userPostsData = array();
		foreach( $userPosts as $up ){
					//** Get attribute data
					$attrData = array();
					foreach( $up['UserPostAttribute'] as $postAttr ){
						if( $postAttr['attribute_type'] == 'text' || $postAttr['attribute_type'] == 'video' ){
							$content = $postAttr['content']; 
						}
						else if( $postAttr['attribute_type'] == 'image'){ 
							$content = AMAZON_PATH . 'posts/'.$postAttr['user_post_id'].'/image/'.$postAttr['content']; 
						}
						else{	
							$content = AMAZON_PATH . 'posts/'.$postAttr['user_post_id'].'/doc/'.$postAttr['content']; 
						}
						$content = str_replace(
							 array("<br>", "<br >", "< br >"), 
							 "\n",
							 $content
							);
						$attrData[] = array(
							"attr_id"=> $postAttr['id'],
							"attribute_type"=> $postAttr['attribute_type'],
							"content"=> $content,
							);
					}

					//** Get Beat User Tagged
					$taggedUsers = $this->PostUserTag->postTaggedUsers( $up['UserPost']['id'] );
					$postTaggedUsers = array();
					foreach( $taggedUsers as $tagUser ){
						$postTaggedUsers[] = array(
													"user_id"=> 0,
													"first_name"=> "OCR",
													"last_name"=> "Member",
													"profile_img"=> GUEST_USER_PROFILE_IMG
												);
					}
					//** Get Specility
					$specilities = $this->Specilities->allSpecilities('','all');
					foreach($specilities as $specVals){
						$specilitiesData[$specVals['Specilities']['id']][] = array('id'=> $specVals['Specilities']['id'], 'name'=> $specVals['Specilities']['name'], 'img_name'=> $specVals['Specilities']['img_name']);
					}
					$specilitiesArr = array();
					foreach( $up['UserPostSpecilities'] as $postSpec ){
						if(!empty($specilitiesData[$postSpec['specilities_id']][0]['id'])){
						$specilitiesArr[] = array(
											'id'=> $specilitiesData[$postSpec['specilities_id']][0]['id'],
											'name'=> $specilitiesData[$postSpec['specilities_id']][0]['name'],
											'icon'=> SPECILITY_ICON_PATH.$specilitiesData[$postSpec['specilities_id']][0]['img_name'],
										);
						}
					}
					//** Get Post up/down beat info
					$upBeatCount = 0; $downBeatCount =0; $shareCount = 0; //** Initialize variables
					$upBeatCount = $this->UserPostBeat->find("count", array("conditions"=> array("post_id"=> $up['UserPost']['id'], "beat_type"=> 1)));
					$downBeatCount = $this->UserPostBeat->find("count", array("conditions"=> array("post_id"=> $up['UserPost']['id'], "beat_type"=> 0)));
					//** Get Beat status of user who has posted Article
					//$userBeatData = $this->UserPostBeat->find("first", array("conditions"=> array("post_id"=> $up['UserPost']['id'], "user_id"=> $loggedinUserId)));
					$userBeatAction =  ( isset($userBeatData['UserPostBeat']['beat_type']) && ((int) $userBeatData['UserPostBeat']['beat_type'] == 0 || (int) $userBeatData['UserPostBeat']['beat_type'] == 1 )) ? (int) $userBeatData['UserPostBeat']['beat_type'] : 2;
					//** Share Count
					$shareCount = $this->UserPost->find("count", array("conditions"=> array("parent_post_id"=> $up['UserPost']['id'])));
					$postData['post_id'] = !empty($up['UserPost']['id']) ? $up['UserPost']['id'] : '';
					$postData['post_title'] = !empty($up['UserPost']['title']) ? $up['UserPost']['title'] : '';
					$postData['post_datetime'] = !empty($up['UserPost']['post_date']) ? $up['UserPost']['post_date'] : '';
					$postData['is_anonymous'] = !empty($up['UserPost']['is_anonymous']) ? (int) $up['UserPost']['is_anonymous'] : 0;
					$postData['markBeat'] = array("upBeatcount"=> $upBeatCount, "downBeatcount"=> $downBeatCount, "userAction"=> $userBeatAction);
					$postData['comment_count'] = count( $up['UserPostComment'] );
					$postData['share_count'] = $shareCount;
					$postData['user'] = array("user_id"=> 0, "first_name"=> 'OCR', "last_name"=> 'Member' , "profile_img"=> GUEST_USER_PROFILE_IMG );
					$postData['attributes'] = $attrData;
					$postData['post_speciality'] = $specilitiesArr;
					$postData['is_shared'] = !empty($up['UserPost']['parent_post_id']) ? 1 : 0;
					$postData['tagged_users'] = $postTaggedUsers;
					//** For guest user user_action will be 0
					$postData['user_action'] = array("beat_comment"=> 0, "beat_shared"=> 0, "beat_spammed"=> 0);
					$userPostsData[] = $postData;
			}
			return $userPostsData;
	}

	/*
	On: 11-12-2015
	I/P: 
	O/P: 
	Desc: Get list of post spam content.
	*/
	public function postSpamContentList(){
		$this->autoRender = false;
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ; 
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true); 
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				//** Get spam content list
				$spamContents = $this->PostSpamContent->spamContentList();
				if( !empty($spamContents) ){
					foreach( $spamContents as $spamC){
						$postSpamData[] = array("id"=> $spamC['PostSpamContent']['id'], "spam_name"=> $spamC['PostSpamContent']['spam_name']);
					}
					$spamContentData['PostSpamContent'] = $postSpamData;
					$responseData = array('method_name'=> 'postSpamContentList', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, 'data'=> $spamContentData);
				}else{
					$responseData = array('method_name'=> 'postSpamContentList', 'status'=>"0", 'response_code'=> "613", 'message'=> ERROR_613);
				}
			}else{
				$responseData = array('method_name'=> 'postSpamContentList', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'postSpamContentList', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	

	/*
	--------------------------------------------------------------------------
	On: 26-01-2016
	I/P: 
	O/P: 
	Desc: Send notification to colleagues when beat added by any user.
	--------------------------------------------------------------------------
	*/
	public function sendNotificationToColleagues($colleagueUserIds = array(), $userId = NULL, $postId = NULL, $postTitle = '', $isAnonymous = 0 ){
		if( !empty($postId) ){
			//** If Post is anonymous don't show beat owner name
			if($isAnonymous == 1){
				$message = "OCR Member has posted a new beat " . $postTitle;
			}else{
				$userDetails = $this->User->findById( $userId );
				$message = $userDetails['UserProfile']['first_name'] . " ". $userDetails['UserProfile']['last_name'] . " has posted a new beat " . $postTitle;
			}
				foreach( $colleagueUserIds as $clg ){
					$userRegisteredDevices = array(); $colleagueuserId = ""; $notificationSetting = "";
					$colleagueuserId = $clg;
					$notificationSetting = $this->UserNotificationSetting->find("count", array("conditions"=> array("user_id"=> $colleagueuserId, "status"=> 1 )));
					$userDevices = $this->NotificationUser->find("all", array("conditions"=> array("user_id"=> $colleagueuserId, "status"=> 1)));
					foreach( $userDevices as $userDevice ){
						$userRegisteredDevices[] = $userDevice['NotificationUser']['device_reg_id'];
					}
					try{
						$notifyResponse = ""; $notificationId = 0; $totUnreadNotificationCount = 0;
						if( ($notificationSetting == (int) 0) ){ //** Check user notification setting ON/OFF 
							$notifyLogData = array("by_user_id"=> $userId, "to_user_id"=> $colleagueuserId, "item_id"=> $postId ,"notify_type"=> "beatToColleague" ,"content"=> $message);
							$this->NotificationLog->saveAll( $notifyLogData );
							$notificationId = $this->NotificationLog->getLastInsertId();
							//**Send GCM Message
							//$totUnreadNotificationCount = $this->NotificationLog->find("count", array("conditions"=> array("to_user_id"=> $colleagueuserId, "read_status"=> 0)));
							$unreadNotificationsData = $this->NotificationLastVisit->unreadNotificationCountAfterLastVisited($postByUserId);
							$totUnreadNotificationCount = $unreadNotificationsData[0][0]['totUnreadNotification'];
							$notifyResponse = $this->Common->sendGcmMessage( $userRegisteredDevices, array("notification_id"=> $notificationId, "message"=> $message, "item_id"=> $postId ,"notify_type"=> "beatToColleague", "tot_unread_notification"=>$totUnreadNotificationCount) );
							//** Update GCM API response
							$this->NotificationLog->updateAll(array("api_response"=> "'". $notifyResponse ."'"), array("id"=> $notificationId));
						}
					}catch( Exception $e ){
						if($userId != $colleagueuserId){
							$notifyLogData = array("by_user_id"=> $userId, "to_user_id"=> $colleagueuserId, "item_id"=> $postId ,"notify_type"=> "beatToColleague" ,"content"=> $message ,"api_response"=> $e->getMessage() );
							$this->NotificationLog->save( $notifyLogData );
					}
				}
			}
		}
	}

	/*
	--------------------------------------------------------------------------
	On: 26-01-2016
	I/P:
	O/P:
	Desc: Send notification to all followers when beat added by any user..
	--------------------------------------------------------------------------
	*/
	public function sendNotificationToFollowers($followerUserIds = array(), $userId = NULL, $postId = NULL, $postTitle = '', $isAnonymous = 0, $sharedBeat = NULL){
		if( !empty($postId) ){
			//** Send GCM to colleague or follower when beat posted or shared[START]
					if(!empty($sharedBeat) && $sharedBeat == 'shared'){
						//** If Post is anonymous don't show beat owner name
						if($isAnonymous == 1){
							if(!empty($postTitle)){
								$message = "OCR Member just shared " . $postTitle;
							}else{
								$message = "OCR Member just shared a Beat";
							}
						}else{
							$userDetails = $this->User->findById( $userId );
							if(!empty($postTitle)){
								$message = $userDetails['UserProfile']['first_name'] . " ". $userDetails['UserProfile']['last_name'] . " just shared " . $postTitle;
							}else{
								$message = $userDetails['UserProfile']['first_name'] . " ". $userDetails['UserProfile']['last_name'] . " just shared a Beat";
							}
						}
					}else{
						//** If Post is anonymous don't show beat owner name
						if($isAnonymous == 1){
							if(!empty($postTitle)){
								$message = "OCR Member just posted " . $postTitle;
							}else{
								$message = "OCR Member just posted a Beat";
							}
						}else{
							$userDetails = $this->User->findById( $userId );
							if(!empty($postTitle)){
								$message = $userDetails['UserProfile']['first_name'] . " ". $userDetails['UserProfile']['last_name'] . " just posted " . $postTitle;
							}else{
								$message = $userDetails['UserProfile']['first_name'] . " ". $userDetails['UserProfile']['last_name'] . " just posted a Beat";
							}
						}
					}
				//** Send GCM to colleague or follower when beat posted or shared[END]
				foreach( $followerUserIds as $uFollow ){
					$userRegisteredDevices = array(); $followerUserId = ""; $notificationSetting = ""; $userDevices = array(); $followerUserId = ""; $userAndroidDevice = array();
					$followerUserId = $uFollow;
					$notificationSetting = $this->UserNotificationSetting->find("count", array("conditions"=> array("user_id"=> $followerUserId, "status"=> 1 )));
					$userDevices = $this->NotificationUser->find("all", array("conditions"=> array("user_id"=> $followerUserId, "status"=> 1)));
					foreach( $userDevices as $userDevice ){
						$userRegisteredDevices[] = $userDevice['NotificationUser']['device_reg_id'];
						//** Get Device id and send New Beat Available to android only
							//if(strlen($userDevice['NotificationUser']['device_id']) < 20 ){
								$userAndroidDevice[] = $userDevice['NotificationUser']['device_reg_id'];
							//}
					}
					try{
							if(empty($sharedBeat)){ //** Don't show in case of shared beat
								$notifyResponse = "";$notificationId = 0; $totUnreadNotificationCount = 0;
								if( ($notificationSetting == (int) 0) ){ //** Check user notification setting ON/OFF 
									$notifyLogData = array("by_user_id"=> $userId, "to_user_id"=> $followerUserId, "item_id"=> $postId ,"notify_type"=> "beatToFollower" ,"content"=> $message);
									$this->NotificationLog->saveAll( $notifyLogData );
									$notificationId = $this->NotificationLog->getLastInsertId();
									//** Send GCM message
									//$totUnreadNotificationCount = $this->NotificationLog->find("count", array("conditions"=> array("to_user_id"=> $followerUserId, "read_status"=> 0)));
									$unreadNotificationsData = $this->NotificationLastVisit->unreadNotificationCountAfterLastVisited($followerUserId);
									$totUnreadNotificationCount = $unreadNotificationsData[0][0]['totUnreadNotification'];
									$notifyResponse = $this->Common->sendGcmMessage( $userRegisteredDevices, array("notification_id"=> $notificationId, "message"=> $message, "item_id"=> $postId ,"notify_type"=> "beatToFollower", "tot_unread_notification"=> $totUnreadNotificationCount) );
									//** Update GCM API response
									try{
										$this->NotificationLog->updateAll(array("api_response"=> "'". $notifyResponse ."'"), array("id"=> $notificationId));
									}catch(Exception $e){
										$notifyResponse = $this->Common->sendGcmMessage( $userAndroidDevice, array("message"=> "New Beat Available", "notify_type"=> "newBeatAvailable" ,"item_id"=> $postId), array("priority"=> "normal", "content_available"=> false) );
									}
								}
							}
							//** Send New Beat Available GCM Message [START]
								//if($userId !=$followerUserId){
										$notifyResponse = $this->Common->sendGcmMessage( $userAndroidDevice, array("message"=> "New Beat Available", "notify_type"=> "newBeatAvailable" ,"item_id"=> $postId), array("priority"=> "normal", "content_available"=> false) );
									//}
								//** Send New Beat Available push to devices
									/*if($userId !=$followerUserId){
										$foregroundDevices = array();
										$foregroundDevices = $this->newBeatAvailableGcmIds($followerUserId);
										if(!empty($foregroundDevices)){
											$notifyResponse = $this->Common->sendGcmMessage( $foregroundDevices, array("message"=> "New Beat Available", "notify_type"=> "newBeatAvailable" ,"item_id"=> (int) $postId, "tot_unread_notification"=> $totUnreadNotificationCount));
										}
									}*/
							//** Send New Beat Available GCM Message [END]
					}catch( Exception $e ){
						if($userId != $followerUserId){
							$notifyLogData = array("by_user_id"=> $userId, "to_user_id"=> $followerUserId, "item_id"=> $postId ,"notify_type"=> "beatToFollower" ,"content"=> $message ,"api_response"=> $e->getMessage() );
							$this->NotificationLog->save( $notifyLogData );
					}
				}
			}
		}
	}

	/*
	------------------------------------------------------------------------------------------
	On: 10-02-2015
	I/P: 
	O/P: 
	Desc: User can hide self beat
	------------------------------------------------------------------------------------------
	*/
	public function hideSelfBeat(){
		$this->autoRender = false;
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ; 
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true); 
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				//** Self beat hide
				if($this->UserPost->find("count", array("conditions"=> array("id"=> $dataInput['post_id'] ,"user_id"=> $dataInput['user_id']))) > 0 ){
					$beatStatusChange = $this->UserPost->updateAll(array("status"=> 0), array("UserPost.id"=> $dataInput['post_id']));
					if($beatStatusChange){
						$responseData = array('method_name'=> 'hideSelfBeat', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200);
					}else{
						$responseData = array('method_name'=> 'hideSelfBeat', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615);
					}
				}else{
					$responseData = array('method_name'=> 'hideSelfBeat', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_632);
				}
			}else{
				$responseData = array('method_name'=> 'hideSelfBeat', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'hideSelfBeat', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	------------------------------------------------------------------------------------------
	On: 19-02-2016
	I/P: $userId
	O/P: array() of user lists
	Desc: Fetches all users which have same specility
	------------------------------------------------------------------------------------------
	*/
	public function getUsersSameSpecility($userId = NULL){
		$userListWithSameSpecilities = array();
		if(!empty($userId)){
			App::import('model', 'UserSpecilities');
			$userSpecilities = new UserSpecilities();
			$query = "SELECT GROUP_CONCAT(user_id) AS user_ids FROM user_specilities WHERE status = 1 AND user_id != ". $userId ."   
						AND specilities_id IN(SELECT GROUP_CONCAT(specilities_id) AS sets 
						from user_specilities WHERE user_id = ". $userId ." AND status = 1)";
			$userLists = $userSpecilities->query($query);
			$userListWithSameSpecilities = explode(',', $userLists[0][0]['user_ids']);
		}
		return $userListWithSameSpecilities;
	}

	/*
	------------------------------------------------------------------------------------------
	On: 19-02-2016
	I/P: $userId
	O/P: array() of user lists
	Desc: Fetches all users which have same interest which added during add beat
	------------------------------------------------------------------------------------------
	*/
	public function getUsersSameBeatInterest($postId = NULL, $userId = NULL){
		$userListWithSamePostInterest = array();
		if(!empty($postId)){
			App::import('model', 'UserPostSpecilities');
			$userPostSpecilities = new UserPostSpecilities();
			$queryInterests = "SELECT GROUP_CONCAT(specilities_id) AS sets  
						FROM user_post_specilities WHERE user_post_id = ". $postId ."";
			$interestLists = $userPostSpecilities->query($queryInterests);
			if(!empty($interestLists[0][0]['sets'])){
				$queryUserLists = "SELECT GROUP_CONCAT(user_id) AS user_ids FROM user_interests WHERE status = 1 AND user_id  != ". $userId ."     
							AND interest_id IN(". $interestLists[0][0]['sets']  .")";
				$usersLists = $userPostSpecilities->query($queryUserLists);
				$userListWithSamePostInterest = explode(',', $usersLists[0][0]['user_ids']);
			}
		}
		return $userListWithSamePostInterest;
	}

	/*
	On: 12-03-2016
	I/P: $userId
	O/P: $foregroundUserRegisteredDevices = array()
	Desc: All device ids of any user if user's application in foreground
	*/
	public function newBeatAvailableGcmIds($userId = NULL){
		$foregroundUserRegisteredDevices = array();
		if(!empty($userId)){
			$userDevices = $this->NotificationUser->find("all", array("conditions"=> array("user_id"=> $userId, "status"=> 1, "service_background_foreground"=> 1)));
			foreach( $userDevices as $userDevice ){
				$foregroundUserRegisteredDevices[] = $userDevice['NotificationUser']['device_reg_id'];	
			}
		}
		return $foregroundUserRegisteredDevices;
	}
	
}
