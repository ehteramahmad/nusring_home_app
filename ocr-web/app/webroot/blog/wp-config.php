<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ocrblog');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'Sde8e4#$@1');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'yUO9shEqad[I-CPJY -5V^s+tGI]jugHU#<_-p%N6fiyAhQ|L]Xt-M,_}eX0nQ,g');
define('SECURE_AUTH_KEY',  '`X.ATCF,j_pD-)C!=E*;t+xP$<<]+60+cXE;caM%65 8eWOVA:R/2b91[*$5/L)$');
define('LOGGED_IN_KEY',    '@fQq%)ZS7>(INureHsCuBF[H6+%_Glm,0|O@Un0-#n4(l0YOz,-|MKc+c{83V1j{');
define('NONCE_KEY',        '};-*AJb2gf|8saqRwXj^`CP5%G/mpk+1DPQ>zp.*|<S(g9^fV2JLUi0ER$g-w!2R');
define('AUTH_SALT',        'v0kw}gQ.^[~W?b|!R5Fi+-Rlj.-+D]00hOk# j746)+st*e#-65%}#^UgN_<CRXo');
define('SECURE_AUTH_SALT', 'L+*5[/]9Z98DW!6Krlc~PK$7-|.+}cyMqyEPN*WbIu ;x!!{.H K>7|@:}K:MG5=');
define('LOGGED_IN_SALT',   'a);s#|o07sJ=(d1p&|oi{oI>z8<$N9M%~xRc5dM&F.U-fv85sZIq9j=Wee(l:r+V');
define('NONCE_SALT',       'JqJRTL*5P`ClMINGBD$X=k8:nM6W{esid^lHMg6z%%xTE*{-_gIX@:ff2=y5 X__');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define( 'FORCE_SSL_ADMIN', true );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
