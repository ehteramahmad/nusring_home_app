<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Stock
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<script src="<?php bloginfo('template_directory'); ?>/js/jquery-1.9.1.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/bootstrap.js"></script>

<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/font-awesome.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/bootstrap.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/simple-line-icons.css" type="text/css" media="screen" />
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'stock' ); ?></a>

	<header><!--header-->
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
      <div class="row">
        <div class="navbar-header"> 
          <!--menu links-->
          <div class="navi-div">
            <ul class="nav navbar-nav navbar-right social-menu">
              <li><a target="_blank" title="LinkedIn" href="https://www.linkedin.com/company/4842839?trk=tyah&trkInfo=idx%3A1-1-1%2CtarId%3A1425467003079%2Ctas%3Athe+on+call+room"><img alt="LinkedIn" src="<?php echo get_template_directory_uri(); ?>/images/linked-icon.png"></a></li>
              <li><a target="_blank" title="Facebook" href="https://www.facebook.com/TheOnCallRoom?ref=br_rs"><img alt="Facebook" src="<?php echo get_template_directory_uri(); ?>/images/fb-icon.png"></a></li>
              <li><a target="_blank" title="Twitter" href="https://twitter.com/theoncallroom"><img alt="Twitter" src="<?php echo get_template_directory_uri(); ?>/images/tweets.png"></a></li>
              <li class="dropdown" style="display:none;"> <a href="javascript:void(0);" class="user-icon" title="Dropdown">Menu</a>
                <div class="user-nav">
                  <ul>
                    <li><a href="javascript:void(0);" title="Get the App">Get the App</a></li>
                    <li><a href="javascript:void(0);" title="Contact Us">Contact Us</a></li>
                    <li><a href="javascript:void(0);" title="About OCR">About OCR</a></li>
                    <li><a href="javascript:void(0);" title="Help and Support">Help and Support</a></li>
                    <li><a href="javascript:void(0);" title="Join the Team">Join the Team</a></li>
                    <li><a href="javascript:void(0);" title="Frequently Asked Questions">Frequently Asked Questions</a></li>
                    <li><a href="javascript:void(0);" title="Network Guidelines">Network Guidelines</a></li>
                    <li><a href="javascript:void(0);" title="Terms & Conditions">Terms &amp; Conditions</a></li>
                    <li><a href="javascript:void(0);" title="Privacy Policy">Privacy Policy</a></li>
                  </ul>
                </div>
              </li>
            </ul>
          </div>
          <!--//menu links--> 
          <a title="The OCR" class="navbar-brand" href="/"><img width="123" height="63" alt="Logo" src="<?php echo get_template_directory_uri(); ?>/images/logo.png"></a>
          <a title="Blog" class="navbar-brand blogSub" target="_blank" href="/blog">Blog</a>
          
      </div>
    </div>
  </nav>
  
  <!--//header--></header>

	<div id="content" class="site-content">
