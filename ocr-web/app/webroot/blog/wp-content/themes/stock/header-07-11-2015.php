<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Stock
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<script src="<?php bloginfo('template_directory'); ?>/js/jquery-1.9.1.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/bootstrap.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/functions.js"></script>

<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/font-awesome.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/bootstrap.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/simple-line-icons.css" type="text/css" media="screen" />
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'stock' ); ?></a>

	<header><!--header-->
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
      <div class="row">
        <div class="navbar-header"> 
          <!--menu links-->
          <div class="navi-div">
          <ul class="nav navbar-nav navbar-right social-menu">
        <li>
          <div class="home-links">
          <ul class="links-nav">
           <li><a href="/about" title="About">About</a></li>
                   <li class="dropdown"><a href="javascript:void(0);" title="Who is it for" data-toggle="dropdown" class="dropdown-toggle">Who is it for
                    <div class="caret"></div>
                    </a>
                      <ul class="dropdown-menu">
                        <li><a href="/who-is-it-for/doctors">Doctors</a></li>
                        <li><a href="/who-is-it-for/nurses">Nurses</a></li>
                        <li><a href="/who-is-it-for/pharmacists">Pharmacists</a></li>
                      </ul>
                    </li>         
                    <li><a href="/faq" title="FAQ">FAQ</a></li>
                    <li><a href="/blog/" title="Blog">Blog</a></li>
          <!-- 
<li><a href="javascript:void(0);" title="Login" class="my_modal_open">Login</a></li>
          <li><a href="/sign-up" title="Sign Up">Sign Up</a></li>
 -->
          </ul>       
        </div>        
        </li>
      
      
              <li>
               <div class="home-links">
         <ul>
          <li class="dropdown"> <a title="Dropdown" class="user-icon" href="javascript:void(0);">Menu</a>
          <div class="user-nav">
             
            <ul>          
              <!-- 
<li class="links-mb who-is-it"><a href="javascript:void(0);" title="Profile" class="who-is-it-for">Who is it for</a>
              <ul class="submenu" style="display: none;">
                <li><a href="/who-is-it-for/doctors">Doctors</a></li>
                <li><a href="/who-is-it-for/nurses">Nurses</a></li>
                <li><a href="/who-is-it-for/pharmacists">Pharmacists</a></li>
              </ul>
              </li>
 -->
              <!-- <li class="links-mb"><a href="/faq" title="FAQ">FAQ</a></li> -->
              <li class="links-mb"><a href="/blog/" title="Blog">Blog</a></li>
              <!-- 
				<li><a href="javascript:void(0);" title="Login" class="my_modal_open">Login</a></li>
                <li><a href="/sign-up" title="Sign Up">Sign Up</a></li> 
 -->             
            <!-- <li><a title="Get the App" href="javascript:void(0);">Get the App</a></li> -->
            <li><a title="Contact Us" href="/about/contact">Contact Us</a></li>
            <li><a title="About OCR" href="/about">About OCR</a></li>
            <li><a title="Help and Support" href="mailto:support@theoncallroom.com">Help and Support</a></li>
            <li><a title="Join the Team" href="/join-the-team">Join the Team</a></li>
            <li><a title="Frequently Asked Questions" href="/faq">Frequently Asked Questions</a></li>
            <li><a title="Network Guidelines" href="/network-guidelines">Network Guidelines</a></li>
            <li><a title="Terms &amp; Conditions" href="/terms-and-conditions">Terms &amp; Conditions</a></li>
            <li><a title="Privacy Policy" href="/privacy-policy">Privacy Policy</a></li>
            </ul>
          </div>
                 </li>
          </ul>
        
               </div>
              </li>
 

            </ul>
          </div>
          <!--//menu links--> 
          <a title="The OCR" class="navbar-brand" href="/"><img width="123" height="63" alt="Logo" src="<?php echo get_template_directory_uri(); ?>/images/logo.png"></a>
          
      </div>
    </div>
  </nav>
  
  <!--//header--></header>

	<div id="content" class="site-content">
