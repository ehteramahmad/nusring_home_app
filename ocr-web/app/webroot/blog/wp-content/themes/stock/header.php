<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Stock
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<script src="<?php bloginfo('template_directory'); ?>/js/jquery-1.9.1.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/bootstrap.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/functions.js"></script>

<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/font-awesome.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/bootstrap.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/simple-line-icons.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/responsive.css" type="text/css" media="screen" />
<?php wp_head(); ?>
<script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');

    fbq('init', '1521228478177347');
    fbq('track', "PageView");
    </script>
  <noscript>
  <img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=1521228478177347&ev=PageView&noscript=1"
  /></noscript>
  <!-- End Facebook Pixel Code -->
<style type="text/css">
.sumome-icon{
  display: none;
}
/*
.sumome-welcomemat-overlay::before{
  content: '';
  display: block;
  background-image: url("https://s3-us-west-1.amazonaws.com/theoncallroom/latest_theme_assets/images/1920.png");
  background: url("https://s3-us-west-1.amazonaws.com/theoncallroom/latest_theme_assets/images/1920.png");
}*/
</style>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'stock' ); ?></a>

	<header><!--header-->
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
      <div class="row">
        <div class="navbar-header"> 
          <!--menu links-->
          <div class="navi-div">
          <ul class="nav navbar-nav navbar-right social-menu">
        <li>
          <div class="home-links" style="margin-top: 6px;">
          <ul class="links-nav">
           <li><a href="/about" title="About">About</a></li>
           <li><a title="Partners" href="/#ourPartners">Our Partners</a></li>
           <li><a title="News" href="/#ourNews">News</a></li>
                   <!-- 
<li class="dropdown"><a href="javascript:void(0);" title="Who is it for" data-toggle="dropdown" class="dropdown-toggle">Who is it for
                    <div class="caret"></div>
                    </a>
                      <ul class="dropdown-menu">
                        <li><a href="/who-is-it-for/doctors">Doctors</a></li>
                        <li><a href="/who-is-it-for/nurses">Nurses</a></li>
                        <li><a href="/who-is-it-for/pharmacists">Pharmacists</a></li>
                      </ul>
                    </li>    
 -->     
                    <li><a href="/faq" title="FAQ">FAQ</a></li>
                    <li><a href="/blog/" title="Blog">Blog</a></li>
          <!-- 
<li><a href="javascript:void(0);" title="Login" class="my_modal_open">Login</a></li>
          <li><a href="/sign-up" title="Sign Up">Sign Up</a></li>
 -->
          </ul>       
        </div>        
        </li>
      
      
              <li>
               <div class="home-links">
         <ul>
          <li class="dropdown"> <a title="Dropdown" class="user-icon" href="javascript:void(0);">Menu</a>
          <div class="user-nav">
             <a title="" class="close-menu" href="javascript:void(0);">Back</a>
            <ul>          
              <!-- 
<li class="links-mb who-is-it"><a href="javascript:void(0);" title="Profile" class="who-is-it-for">Who is it for</a>
              <ul class="submenu" style="display: none;">
                <li><a href="/who-is-it-for/doctors">Doctors</a></li>
                <li><a href="/who-is-it-for/nurses">Nurses</a></li>
                <li><a href="/who-is-it-for/pharmacists">Pharmacists</a></li>
              </ul>
              </li>
 -->
              <!-- <li class="links-mb"><a href="/faq" title="FAQ">FAQ</a></li> -->
              
              <!-- 
				<li><a href="javascript:void(0);" title="Login" class="my_modal_open">Login</a></li>
                <li><a href="/sign-up" title="Sign Up">Sign Up</a></li> 
 -->             
            <!-- <li><a title="Get the App" href="javascript:void(0);">Get the App</a></li> -->
            <li><a title="Blog" href="/blog/">Blog</a></li>
            <li><a title="Contact Us" href="/contact">Contact Us</a></li>
            <li><a title="About OCR" href="/about">About OCR</a></li>
            <li><a href="/#ourPartners" title="Partners">Our Partners</a></li>
            <li><a href="/#ourNews" title="News">News</a></li>
            <li><a title="Help and Support" href="mailto:support@theoncallroom.com">Help and Support</a></li>
            <li><a title="Join the Team" href="/join-the-team">Join the Team</a></li>
            <li><a title="Frequently Asked Questions" href="/faq">Frequently Asked Questions</a></li>
            <li><a title="Network Guidelines" href="/network-guidelines">Network Guidelines</a></li>
            <li><a title="Terms &amp; Conditions" href="/terms-and-conditions">Terms &amp; Conditions</a></li>
            <li><a title="Privacy Policy" href="/privacy-policy">Privacy Policy</a></li>
            </ul>
          </div>
                 </li>
          </ul>
        
               </div>
              </li>
 

            </ul>
          </div>
          <!--//menu links--> 
          <a title="The OCR" class="navbar-brand" href="/"><img width="123" height="63" alt="Logo" src="<?php echo get_template_directory_uri(); ?>/images/logo.png"></a>
          
      </div>
    </div>
  </nav>
  
  <!--//header--></header>

	<div id="content" class="site-content">
