<?php 
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 */
class Country extends AppModel {

	public $name = 'Country';

/*
________________________________________________
Method Name:checkValueExists
Purpose: Search the field value in table via true or false
Detail: Check the field vale in model
created by :Developer
Created date: 06-08-2015
Params:$fieldName = '', $fieldVal = ''

_________________________________________________
*/
    public function checkValueExists( $fieldName = '', $fieldVal = ''){
        
        $countdata = $this->find('count', array('conditions' => array( $fieldName => $fieldVal )));
        
        if($countdata > 0){
            return true;
        }else{
            return false;
        }
    }

 /*
 On: 06-08-2015
 I/P: $fields = array(), $conditions = array()
 O/P: Country details
 Desc: 
 */
 public function countryDetails( $fields = '', $conditions = array() ){
 	if( !empty($fields) && !empty($conditions) ){
 		$countryData = $this->find('all', array( 'fields'=> $fields, 'conditions'=> $conditions));
 	}else{
 		$countryData = $this->find('all');
 	}
 	return $countryData;
 }   


}// End Class

?>
