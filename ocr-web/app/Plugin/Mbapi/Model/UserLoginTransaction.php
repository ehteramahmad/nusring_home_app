<?php


App::uses('AppModel', 'Model');

/**
 * Represents model UserLoginTransaction
 */
class UserLoginTransaction extends AppModel {

	/*
	------------------------------------------------------------------------------
	On: 
	I/P: 
	O/P: 
	Desc: 
	------------------------------------------------------------------------------
	*/

	public function maintainLoginTransaction($params = array())
	{
		$response = 0;
		if( !empty($params) )
		{
			$saveTransactionData = array("user_id"=> $params['user_id'], "email"=> $params['email'], "type"=>$params['type'], "status"=>$params['status'], "device_type"=>$params['device_type'], "application_type"=>$params['application_type']);
	        $this->saveAll($saveTransactionData);
	        $response = 1;
		}
		return $response;
	}
	
}
