<?php 
/*
 Represents model for post spam contents.
*/
App::uses('AppModel', 'Model');

class PostSpamContent extends AppModel {
	public $name = 'PostSpamContent';

	/*
	----------------------------------------------------------------------------------
	On: 11-12-2015
	I/P: 
	O/P: 
	Desc: Fetches all Beat spam contents.
	----------------------------------------------------------------------------------
	*/
	public function spamContentList(){
		$spamLists = array();

		$spamLists = $this->find("all", array("conditions"=> array("status"=> 1)));
		return $spamLists;
	}
}
?>