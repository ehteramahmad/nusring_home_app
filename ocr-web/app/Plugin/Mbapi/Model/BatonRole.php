<?php


App::uses('Model', 'Model');

/**
 * Represents model UserDndStatus
 */
class BatonRole extends Model {

	/*
	-----------------------------------------------------
	On: 31-10-2017
	I/P:
	O/P:
	Desc:
	-----------------------------------------------------
	*/
	public function batonRoleList($params = array()){
		$returnData = array();
		$conditions = array("DepartmentsBatonRole.is_occupied"=> 0 , "DepartmentsBatonRole.is_active"=> 1,"DepartmentsBatonRole.institute_id" =>$params['institute_id']);
		$returnData = $this->find('all', 
							array(
								'joins'=>
								array(
									array(
										'table' => 'departments_baton_roles',
										'alias' => 'DepartmentsBatonRole',
										'type' => 'left',
										'conditions'=> array('BatonRole.id = DepartmentsBatonRole.role_id')
									)
								),
								'fields'=> array("BatonRole.id","BatonRole.baton_roles","DepartmentsBatonRole.on_call_value"),
								'conditions' => $conditions,
							)
						);
		return $returnData;

	}


	public function batonRoleList_pre($params = array()){
		$returnData = array();
		$conditions = array("DepartmentsBatonRole.is_occupied"=> 0 , "DepartmentsBatonRole.is_active"=> 1,"DepartmentsBatonRole.institute_id" =>$params['institute_id']);
		$returnData = $this->find('all', 
							array(
								'joins'=>
								array(
									array(
										'table' => 'departments_baton_roles',
										'alias' => 'DepartmentsBatonRole',
										'type' => 'left',
										'conditions'=> array('BatonRole.id = DepartmentsBatonRole.role_id')
									)
								),
								'fields'=> array("BatonRole.id","BatonRole.baton_roles","DepartmentsBatonRole.on_call_value"),
								"order"=> array("BatonRole.baton_roles"),
								'conditions' => $conditions,
							)
						);
		return $returnData;

	}

	/*
	-----------------------------------------------------
	On: 31-10-2017
	I/P:
	O/P:
	Desc:
	-----------------------------------------------------
	*/
	public function batonRoleList_v1($params = array()){
		$returnData = array();
		$conditions = array("DepartmentsBatonRole.is_occupied"=> 0 , "DepartmentsBatonRole.is_active"=> 1, "DepartmentsBatonRole.institute_id" =>$params['institute_id']);
		$returnData = $this->find('all', 
							array(
								'joins'=>
								array(
									array(
										'table' => 'departments_baton_roles',
										'alias' => 'DepartmentsBatonRole',
										'type' => 'left',
										'conditions'=> array('BatonRole.id = DepartmentsBatonRole.role_id')
									)
								),
								'fields'=> array("BatonRole.id","BatonRole.baton_roles","DepartmentsBatonRole.on_call_value"),
								'conditions' => $conditions,
							)
						);
		return $returnData;

	}

}
