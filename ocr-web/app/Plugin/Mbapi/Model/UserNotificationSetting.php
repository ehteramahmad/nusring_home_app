<?php 
/*
 Represents model for User Notification Setting.
*/
App::uses('AppModel', 'Model');

class UserNotificationSetting extends AppModel {
	public $name = 'UserNotificationSetting';
}
?>