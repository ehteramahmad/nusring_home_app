<?php


App::uses('Model', 'Model');

/**
 * Represents model UserDndStatus
 */
class DndStatusOption extends Model {

	/*
	-----------------------------------------------------------
	On: 13-11-2017
	I/P: $params = array()
	O/P: array() users lists
	Desc: Fetches all user of same hospital with ON/OFF duty (for lesser data)
	-----------------------------------------------------------
	*/
	public function getDndStatusOptionModel($instituteId = NULL){
		$userLists = array();
		if( !empty($instituteId) ){
			$quer = "SELECT DndStatusOption.id,DndStatusOption.dnd_name,
												DndStatusOption.icon_selected,
												DndStatusOption.icon_unselected,DndStatusOption.duration
			FROM dnd_status_options DndStatusOption
			WHERE  DndStatusOption.institute_id = $instituteId AND DndStatusOption.is_active = 1
			ORDER BY DndStatusOption.id ASC";
			$dndStatusOptionList = $this->query($quer);
			
		}
		return $dndStatusOptionList;
	}


	/*
	--------------------------------------------------------------------------
	ON: 12-02-2019
	I/P: data array $dndStatusOptionList
	O/P: array
	Desc: Will return list of all active Do not disturb detail.
	--------------------------------------------------------------------------
	*/

	public function getDndStatusOptions($params = array())
	{
		if(!empty($params))
		{
			$data = $this->find('first',array('conditions'=>array('id'=>$params['dnd_status_id'])));
		}
		return $data;
	}
}
