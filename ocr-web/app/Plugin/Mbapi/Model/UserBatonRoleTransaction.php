<?php


App::uses('Model', 'Model');

/**
 * Represents model UserDndStatus
 */
class UserBatonRoleTransaction extends Model {

	/*
	-----------------------------------------------------------
	On: 06-03-2019
	I/P: $userId
	O/P: array
	Desc: fetch user all baton roles
	-----------------------------------------------------------
	*/
	public function saveRoleExchangeTransactions($params = array())
	{
		if(!empty($params))
		{
			switch ($params['request_type']) {
			    case "transfer":
			        $data = array('from_user_status'=>5, "to_user_status"=>4,"from_user_is_active"=>1,"to_user_is_active"=>1);
			        break;
			    case "transfer_accept":
			        $data = array('from_user_status'=>1, "to_user_status"=>0,"from_user_is_active"=>1,"to_user_is_active"=>0);
			        break;
			    case "transfer_rejected":
			        $data = array('from_user_status'=>0, "to_user_status"=>7,"from_user_is_active"=>0,"to_user_is_active"=>0);
			        break;
			    case "request":
			        $data = array('from_user_status'=>3, "to_user_status"=>2,"from_user_is_active"=>1,"to_user_is_active"=>1);
			        break;
			    case "request_accept":
			        $data = array('from_user_status'=>0, "to_user_status"=>1,"from_user_is_active"=>0,"to_user_is_active"=>1);
			        break;
			    case "request_reject":
			        $data = array('from_user_status'=>6, "to_user_status"=>0,"from_user_is_active"=>0,"to_user_is_active"=>0);
			        break;
			    case "revoke_request":
			        $data = array('from_user_status'=>0, "to_user_status"=>1,"from_user_is_active"=>0,"to_user_is_active"=>1);
			        break;
			    case "revoke_transfer":
			        $data = array('from_user_status'=>1, "to_user_status"=>0,"from_user_is_active"=>1,"to_user_is_active"=>0);
			        break;
			}

			$saveUserBatonRoleTransactions = array("from_user"=>$params['from_user'], "to_user"=>$params['to_user'], "role_id"=> $params['role_id'], "status"=>$data['from_user_status'],"is_active"=>$data['from_user_is_active'],  "created_at"=> date("Y-m-d H:i:s"));
			$this->saveAll($saveUserBatonRoleTransactions);
		}
	}
}
