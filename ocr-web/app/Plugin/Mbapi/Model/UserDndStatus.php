<?php


App::uses('Model', 'Model');

/**
 * Represents model UserDndStatus
 */
class UserDndStatus extends Model {

	/*
	--------------------------------------------------------------------
	On: 13-02-2019
	I/P: $params = array()
	O/P: array()
	Desc: Fetche single user dnd status
	--------------------------------------------------------------------
	*/	
	public function getUserDndStatus($params = array()){
		if(!empty($params))
		{
			$data = $this->find('first',array('conditions'=>array('user_id'=>$params)));
		}
		return $data;
	}
	/*
	--------------------------------------------------------------------
	On: 18-02-2019
	I/P: 
	O/P: $data
	Desc: Fetches all user dnd status.
	--------------------------------------------------------------------
	*/

	public function getAllUserDndStatus(){
		$data = $this->find('all',array('conditions'=>array('is_active'=>"1")));
		return $data;
	}

	/*
	--------------------------------------------------------------------
	On: 24-04-2019
	I/P: $params = array()
	O/P: array()
	Desc: Fetche single user dnd status
	--------------------------------------------------------------------
	*/	
	public function getUserDndStatus_pre($params = array()){
		if(!empty($params))
		{
			$data = $this->find('first',array('conditions'=>array('user_id'=>$params['user_id'])));
		}
		return $data;
	}

	public function getUserDndStatus_v1($params = array()){
		if(!empty($params))
		{
			$data = $this->find('first',array('conditions'=>array('user_id'=>$params['user_id'])));
		}
		return $data;
	}
}
