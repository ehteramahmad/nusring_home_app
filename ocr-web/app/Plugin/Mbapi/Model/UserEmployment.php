<?php 
/*
Add, update, List user employments.
*/
App::uses('AppModel', 'Model');

class UserEmployment extends AppModel {
	
	/*
	--------------------------------------------------------------
	On: 05-01-2015
	I/P: 
	O/P: 
	Desc: Fetches all user employments.
	--------------------------------------------------------------
	*/
	public function getUserEmployment( $userId = NULL, $params = array() ){
		$userEmploymentList = array();
		if( !empty($userId) ){
			$pageNum = isset($params['page_number']) ? $params['page_number'] : 1;
			$pageSize = isset($params['size']) ? $params['size'] : DEFAULT_PAGE_SIZE;
			$offsetVal = ( $pageNum - 1 ) * $pageSize;
			$conditions = array("UserEmployment.user_id"=> $userId, "is_current"=> 1);
			$userEmploymentList = $this->find('all', 
							array(
								'joins'=>
								array(
									array(
										'table' => 'company_names',
										'alias' => 'Company',
										'type' => 'left',
										'conditions'=> array('UserEmployment.company_id = Company.id')
									),
									array(
										'table' => 'designations',
										'alias' => 'Designation',
										'type' => 'left',
										'conditions'=> array('UserEmployment.designation_id	 = Designation.id')
									),
								),
								'conditions' => $conditions,
								'fields'=> array('UserEmployment.id, UserEmployment.company_id, UserEmployment.from_date, UserEmployment.to_date, UserEmployment.location, UserEmployment.description, UserEmployment.is_current, Company.id, Company.is_subscribed, Company.status, Company.company_name, Company.patient_number_type, Designation.id, Designation.designation_name'),
								'limit'=> $pageSize,
								'offset'=> $offsetVal,
								'order'=> 'UserEmployment.id'
							)
						);
		}
		return $userEmploymentList;
	}

	/*
	--------------------------------------------------------------
	On: 04-12-2017
	I/P: $params
	O/P: $contactRequestData
	Desc: Fetch all contact request data
	--------------------------------------------------------------
	*/

	public function getContactRequestsData($params = array())
	{
		//return $params;
		$contactRequestList = array();
		if( !empty($params) ){
			$loggedinUserCompanyId = $params['loggedinUserInstitute'];
			$loggedinUserCountryId = $params['loggedinUserCountry'];
			$loggedinUserId = $params['loggedinUserId'];
			$userStatus = $params['status'];
			if(!empty($loggedinUserCompanyId))
			{
				if($userStatus == 1)
				{
					$conditions = array("UserEmployment.company_id"=> $loggedinUserCompanyId, "UserEmployment.is_current"=> 1, "UserColleagues.colleague_user_id"=>$loggedinUserId," User.status"=>1,"User.approved"=>1,"EnterpriseUserLists.status"=>$userStatus,"UserColleagues.status"=>2);

					//***************Remove Country Check 04-04-18 **********************//
					// $conditions = array("UserEmployment.company_id"=> $loggedinUserCompanyId, "UserEmployment.is_current"=> 1, "UserColleagues.colleague_user_id"=>$loggedinUserId," User.status"=>1,"User.approved"=>1,"EnterpriseUserLists.status"=>$userStatus,"UserColleagues.status"=>2, "UserProfile.country_id"=>$loggedinUserCountryId);	
				}
				else
				{
					$conditions = array("UserEmployment.company_id"=> $loggedinUserCompanyId, "UserEmployment.is_current"=> 1, "UserColleagues.colleague_user_id"=>$loggedinUserId," User.status"=>1,"User.approved"=>1,"UserColleagues.status"=>2);

					//***************Remove Country Check 04-04-18 **********************//
					// $conditions = array("UserEmployment.company_id"=> $loggedinUserCompanyId, "UserEmployment.is_current"=> 1, "UserColleagues.colleague_user_id"=>$loggedinUserId," User.status"=>1,"User.approved"=>1,"UserColleagues.status"=>2, "UserProfile.country_id"=>$loggedinUserCountryId);
				}
				$contactRequestList = $this->find('all', 
							array(
								'joins'=>
								array(
									array(
										'table' => 'users',
										'alias' => 'User',
										'type' => 'left',
										'conditions'=> array('User.id = UserEmployment.user_id')
									),
									array(
										'table' => 'enterprise_user_lists',
										'alias' => 'EnterpriseUserLists',
										'type' => 'left',
										'conditions'=> array('User.email = EnterpriseUserLists.email')
									),
									array(
										'table' => 'user_colleagues',
										'alias' => 'UserColleagues',
										'type' => 'left',
										'conditions'=> array('UserEmployment.user_id = UserColleagues.user_id')
									),
									array(
										'table' => 'user_profiles',
										'alias' => 'UserProfile',
										'type' => 'left',
										'conditions'=> array('UserProfile.user_id = UserEmployment.user_id')
									),
									array(
										'table' => 'professions',
										'alias' => 'Professions',
										'type' => 'left',
										'conditions'=> array('Professions.id = UserProfile.profession_id')
									),
									array(
										'table' => 'user_qb_details',
										'alias' => 'UserQbDetails',
										'type' => 'left',
										'conditions'=> array('UserQbDetails.user_id = UserEmployment.user_id')
									),
									array(
										'table' => 'user_duty_logs',
										'alias' => 'UserDutyLog',
										'type' => 'left',
										'conditions'=> array('UserDutyLog.user_id = UserColleagues.user_id')
									),
								),
								'conditions' => $conditions,
								'fields'=> array('UserEmployment.user_id,UserProfile.first_name, UserProfile.last_name, UserProfile.role_status,UserProfile.profile_img, Professions.profession_type,UserProfile.profession_id,UserQbDetails.qb_id,UserDutyLog.status,UserDutyLog.atwork_status,UserColleagues.message,UserColleagues.updated'),
								'group'=>'UserColleagues.user_id',
								'order'=> 'UserColleagues.user_id'
							)
						);
			}
		}
		return $contactRequestList;
	}

	public function getPendingRequestData($params = array())
	{
		//return $params;
		$contactRequestList = array();
		if( !empty($params) ){
			$loggedinUserCompanyId = $params['loggedinUserInstitute'];
			$loggedinUserCountryId = $params['loggedinUserCountry'];
			$loggedinUserId = $params['loggedinUserId'];
			$userStatus = $params['status'];
			if(!empty($loggedinUserCompanyId))
			{
				if($userStatus == 1)
				{
					$conditions = array("UserEmployment.company_id"=> $loggedinUserCompanyId, "UserEmployment.is_current"=> 1, "UserColleagues.colleague_user_id"=>$loggedinUserId," User.status"=>1,"User.approved"=>1,"EnterpriseUserLists.status"=>$userStatus,"UserColleagues.status"=>2);


					//***************Remove Country Check 04-04-18 **********************//
					// $conditions = array("UserEmployment.company_id"=> $loggedinUserCompanyId, "UserEmployment.is_current"=> 1, "UserColleagues.colleague_user_id"=>$loggedinUserId," User.status"=>1,"User.approved"=>1,"EnterpriseUserLists.status"=>$userStatus,"UserColleagues.status"=>2, "UserProfile.country_id"=>$loggedinUserCountryId);
				}
				else
				{

					$conditions = array("UserEmployment.company_id"=> $loggedinUserCompanyId, "UserEmployment.is_current"=> 1, "UserColleagues.colleague_user_id"=>$loggedinUserId," User.status"=>1,"User.approved"=>1,"UserColleagues.status"=>2);

					//***************Remove Country Check 04-04-18 **********************//
					// $conditions = array("UserEmployment.company_id"=> $loggedinUserCompanyId, "UserEmployment.is_current"=> 1, "UserColleagues.colleague_user_id"=>$loggedinUserId," User.status"=>1,"User.approved"=>1,"UserColleagues.status"=>2, "UserProfile.country_id"=>$loggedinUserCountryId);
				}

				
				$contactRequestList = $this->find('all', 
							array(
								'joins'=>
								array(
									array(
										'table' => 'users',
										'alias' => 'User',
										'type' => 'left',
										'conditions'=> array('User.id = UserEmployment.user_id')
									),
									array(
										'table' => 'enterprise_user_lists',
										'alias' => 'EnterpriseUserLists',
										'type' => 'left',
										'conditions'=> array('User.email = EnterpriseUserLists.email')
									),
									array(
										'table' => 'user_colleagues',
										'alias' => 'UserColleagues',
										'type' => 'left',
										'conditions'=> array('UserEmployment.user_id = UserColleagues.user_id')
									),
									array(
										'table' => 'user_profiles',
										'alias' => 'UserProfile',
										'type' => 'left',
										'conditions'=> array('UserProfile.user_id = UserEmployment.user_id')
									),
								),
								'conditions' => $conditions,
								'fields'=> array('UserEmployment.user_id'),
								'group'=>'UserColleagues.user_id',
								'order'=> 'UserColleagues.user_id'
							)
						);
			}
		}
		return $contactRequestList;
	}

	/*
	--------------------------------------------------------------
	On: 07-12-2017
	I/P: 
	O/P: 
	Desc: Fetches User instituion.
	--------------------------------------------------------------
	*/

	public function getUserInstitution($params=array())
	{
		$data =array();
		if(!empty($params)){
			$userId = $params['user_id'];
			$colleagueUserId = $params['colleague_user_id'];
			$quer = "SELECT ue.`company_id` , ues.`company_id`
					FROM `user_employments` ue
					INNER JOIN `user_employments` ues ON ue.`company_id` = ues.`company_id`
					WHERE ue.`user_id` =$userId AND ue.is_current=1 
					AND ues.`user_id` =$colleagueUserId AND ues.is_current=1";
			$data = $this->query($quer);
    	}
    	return $data;
	}

	/*
	--------------------------------------------------------------
	On: 08-12-2017
	I/P: 
	O/P: 
	Desc: Fetches User instituion.
	--------------------------------------------------------------
	*/

	public function getUserCurrentInstitution($userId)
	{
		if(!empty($userId)){
			$userCurrentInstitute = $this->find("first", array("conditions"=> array("user_id"=> $userId, "is_current"=> 1), "order"=> array("id DESC")));
    	}
    	return $userCurrentInstitute;
	}
}
?>