<?php
/*
 * Feedback controller.
 *
 * This file will render views from views/Mbfeedbackwebservices/
 *
 
 */

App::uses('AppController', 'Controller');


class MbfeedbackwebservicesController extends AppController {
	public $uses = array('Mb.User','Mb.UserFeedback');
	public $components = array('Common');
	
	/*
	-------------------------------------------------------------------------
	ON: 04-07-2016
	I/P: JSON (user_id, feedback content)
	O/P: JSON (success/fail)
	Desc: User can send feedback.
	-------------------------------------------------------------------------
	*/
	public function addUserFeedback(){
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			if( $this->validateToken() && $this->validateAccessKey() ){
				if( $this->User->find("count", array("conditions"=> array("User.id"=>$dataInput['user_id']))) > 0){
					//** Add feedback
					try{
						$this->UserFeedback->recursive = -1;
						$userFeedbackData = array("user_id"=> $dataInput['user_id'], "subject"=> $dataInput['subject'], "feedback"=> $dataInput['feedback'], "feedback_source"=> "MB");
						$checkFeedback = $this->UserFeedback->find("count", array("conditions"=> $userFeedbackData));
						if( $checkFeedback == 0 ){
							if($this->UserFeedback->save( $userFeedbackData )){
								$responseData = array('method_name'=> 'addUserFeedback', 'status'=>"1", 'response_code'=> "200", 'message'=> SUCCESS_605);
							}else{
								$responseData = array('method_name'=> 'addUserFeedback', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615);
							}
						}else{
								$responseData = array('method_name'=> 'addUserFeedback', 'status'=>"0", 'response_code'=> "637", 'message'=> ERROR_637);
						}
					}catch( Exception $e ){
						$responseData = array('method_name'=> 'addUserFeedback', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, 'system_errors'=> $e->getMessage());
					}
				}else{
					$responseData = array('method_name'=> 'addUserFeedback', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
				}
			}else{
				$responseData = array('method_name'=> 'addUserFeedback', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'addUserFeedback', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
		exit;
	}
}
