<?php
/*
Desc: API data get/post to Webservices.
*/

//App::uses('AppController', 'Controller');

class MbapidirectoryController extends AppController {

	public $uses = array('Mbapi.User','Mbapi.UserProfile', 'Mbapi.UserEmployment','Mbapi.EnterpriseUserList','Mbapi.CompanyName', 'Mbapi.UserQbDetail','Mbapi.Profession','Mbapi.UserRoleTag', 'Mbapi.CacheLastModifiedUser', 'Mbapi.InstitutionGroupAdmin','Mbapi.UserDndStatus','Mbapi.DndStatusOption','Mbapi.BatonRole','Mbapi.UserBatonRole','Mbapi.RoleTag');
	public $components = array('Common','Cache','Settergetter');

	/*
	------------------------------------------------------------------------------
	ON: 13-11-2017
	I/P:
	O/P:
	Desc: Get Static directory data
	------------------------------------------------------------------------------
	*/
	public function getStaticDirectoryData(){
		$group_admin = 0;
		if($this->request->is('post'))
		{
			$dataInput = $this->request->input('json_decode', true) ;
			if($this->User->findById( $dataInput['user_id'] ) )
			{
				$loggedinUserId = isset($dataInput['user_id']) ? $dataInput['user_id'] : '';
				$userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $loggedinUserId)));
				if($userDetails['User']['status']==1 && $userDetails['User']['approved']==1)
				{
					if( $this->validateToken() && $this->validateAccessKey() )
					{
						$params['loggedin_user_countryid'] = $userDetails['UserProfile']['country_id'];
						$userCurrentCompany = $this->UserEmployment->find("first", array("conditions"=> array("user_id"=> $loggedinUserId, "is_current"=> 1), "order"=> array("id DESC")));
						$params['user_current_company_id'] = $userCurrentCompany['UserEmployment']['company_id'];
						if($params['user_current_company_id'] != -1)
						{
							$enterpriseUserCheck = $this->EnterpriseUserList->find("count", array("conditions"=> array("email"=> stripslashes($userDetails['User']['email']),"company_id"=> $userCurrentCompany['UserEmployment']['company_id'],"status"=> 1)));
							$companySubscriptionCheck=$this->CompanyName->find("count", array("conditions"=> array("id"=>$userCurrentCompany['UserEmployment']['company_id'], "status"=>1,"is_subscribed"=>1)));

							$companyDetail=$this->CompanyName->find("first", array("conditions"=> array("id"=>$userCurrentCompany['UserEmployment']['company_id'])));

							$adminDetail = $this->InstitutionGroupAdmin->find("first", array("conditions"=> array("institute_id"=>$userCurrentCompany['UserEmployment']['company_id'],'status'=>1)));

							if(!empty($adminDetail)){
								$group_admin = $adminDetail['InstitutionGroupAdmin']['qb_id'];
							}

							$etagChangeParams['key'] = 'staticDirectoryEtag:'.$userCurrentCompany['UserEmployment']['company_id'];
							$etagChangeParams['institution_key'] = $userCurrentCompany['UserEmployment']['company_id'];
							$etagChanged = $this->checkEtagNotModified($etagChangeParams);
							// echo "<pre>";print_r($etagChanged);exit();
							$etagLastModified = strtotime(date('Y-m-d H:i:s'));
							if($etagChanged['code'] !=304 && $etagChanged['code'] !=200){
								$cacObjEtag = $this->Cache->redisConn();
								if(($cacObjEtag['connection']) && empty($cacObjEtag['errors'])){
									// if($cacObjEtag['robj']->exists($etagChangeParams['key'])){
										$etagLastModified = strtotime(date('Y-m-d H:i:s'));
										$eTagNewVal = md5($etagChangeParams['key'] . rand(1,10000) . $etagLastModified);
										$cacObjEtag['robj']->hset($etagChangeParams['key'],'eTag' , $eTagNewVal);
										$cacObjEtag['robj']->hset($etagChangeParams['key'],'lastmodified' , $etagLastModified);
									// }
								}
								$eTagData = $eTagNewVal;
								$eTagDate = date('Y-m-d H:i:s', $etagLastModified);
								$dynamicETagData = $eTagNewVal.'~'.$etagLastModified;
							}else{
								$eTagData = $etagChanged['eTag'];
								$dynamicETagData = $etagChanged['eTag'].'~'.$etagLastModified;
								if(empty($etagChanged['etagHeader'])){
									$eTagDate = '0000-00-00 00:00:00';
								}else{
									$eTagDate = date('Y-m-d H:i:s', $etagChanged['lastmodified']);
								}
							}
							//** Add company details[START]
							$userEmploymentData = array("id"=> $userCurrentCompany['UserEmployment']['id'], "company_id"=> $userCurrentCompany['UserEmployment']['company_id'] ,"company_name"=> $companyDetail['CompanyName']['company_name'], "patient_number_type"=>$companyDetail['CompanyName']['patient_number_type'], "is_current"=> $userCurrentCompany['UserEmployment']['is_current']);
							//** Add company details[END]

							if($companySubscriptionCheck ==1 && $enterpriseUserCheck ==0){
								$userData['diretory_access'] = 655;
								$userData['message'] = ERROR_655;
								if($userCurrentCompany['UserEmployment']['company_id'] == $dataInput['institution_id'])
								{
									$responseData = array('method_name'=> 'getStaticDirectoryData','status'=>'1','response_code'=>'200', 'message'=> ERROR_200,"Etag"=> $eTagData, 'data'=> $userData, 'admin_qb_id' => $group_admin);
								}
								else
								{
									$responseData = array('method_name'=> 'getStaticDirectoryData','status'=>'1','response_code'=>'200', 'message'=> ERROR_200,"Etag"=> $eTagData, 'data'=> $userData, 'Company'=>$userEmploymentData, 'delete_static_directory'=>1,'admin_qb_id' => $group_admin);
								}
							}
							else
							{
								if($etagChanged['code'] ==304){
									$responseData = array('method_name'=> 'getStaticDirectoryData','status'=>'0','response_code'=>'304', 'admin_qb_id' => $group_admin, 'message'=> SUCCESS_304, "Etag"=> $eTagData);
								}
								else
								{
									//** Get Deleted users[START]
									$userListsArr = array();
									$getDeletedUserData=$this->CacheLastModifiedUser->find("all", array("conditions"=> array("company_id"=>$userCurrentCompany['UserEmployment']['company_id'], "type"=>"deleted_user","last_modified > "=> "'".$eTagDate."'", "status"=>1),'group'=>array('CacheLastModifiedUser.user_id'), 'order'=>array('last_modified DESC')));
									//** Get Deleted users[END]
									$cacObjEtag = $this->Cache->redisConn();
									// print_r($cacObj);
									// exit();
									if(($cacObjEtag['connection']) && empty($cacObjEtag['errors'])){
										// echo "<pre>";print_r("TEST12");exit();

										if($userCurrentCompany['UserEmployment']['company_id'] == $dataInput['institution_id'])
										{
											if($cacObjEtag['robj']->exists('institutionDirectory:'.$userCurrentCompany['UserEmployment']['company_id'])){

											
												$dataJson = $cacObjEtag['robj']->get('institutionDirectory:'.$userCurrentCompany['UserEmployment']['company_id']);
												$userData = json_decode($dataJson, true);
											}
											else
											{
												// echo "<pre>";print_r("TEST13");exit();
												$userLists = array();
												if($userCurrentCompany['UserEmployment']['company_id'] != -1){ //** Don't show if user selected company None(i.e.-1)
													//$userLists = $this->User->oncallUserListsLight( $params, $loggedinUserId );
													$userLists = $this->User->getUserDirectoryDataLiteOne( $params, $loggedinUserId );
												}
												$userData['User'] = $this->directoryStaticDataFormatLiteOne($userLists, $loggedinUserId,$userCurrentCompany['UserEmployment']['company_id'],$companySubscriptionCheck);
												$userData['DEtag'] = $dynamicETagData;
												if(count($getDeletedUserData) > 0)
												{
													foreach ($getDeletedUserData as $modifiedUser) {
														$userListsArr[] = array("user_id"=> $modifiedUser['CacheLastModifiedUser']['user_id']);
													}
												}
												$userData['Deleted_user'] = $userListsArr;
												$key = 'institutionDirectory:'.$userCurrentCompany['UserEmployment']['company_id'];
												$cacObjEtag['robj']->set($key,json_encode($userData, true));
											}
											$responseData = array('method_name'=> 'getStaticDirectoryData','status'=>'1','response_code'=>'200', 'message'=> ERROR_200,"Etag"=> $eTagData, 'data'=> $userData,'admin_qb_id' => $group_admin, 'Company'=>$userEmploymentData);
										}
										else
										{
											if($cacObjEtag['robj']->exists('institutionDirectory:'.$userCurrentCompany['UserEmployment']['company_id'])){
											//echo "<pre>";print_r("TEST12");exit();
												$dataJson = $cacObjEtag['robj']->get('institutionDirectory:'.$userCurrentCompany['UserEmployment']['company_id']);
												$userData = json_decode($dataJson, true);
											}
											else
											{
												$userLists = array();
												if($userCurrentCompany['UserEmployment']['company_id'] != -1){ //** Don't show if user selected company None(i.e.-1)
													//$userLists = $this->User->oncallUserListsLight( $params, $loggedinUserId );
													$userLists = $this->User->getUserDirectoryDataLiteOne( $params, $loggedinUserId );
												}
												$userData['User'] = $this->directoryStaticDataFormatLiteOne($userLists, $loggedinUserId,$userCurrentCompany['UserEmployment']['company_id'],$companySubscriptionCheck);
												$userData['DEtag'] = $dynamicETagData;
												if(count($getDeletedUserData) > 0)
												{
													foreach ($getDeletedUserData as $modifiedUser) {
														$userListsArr[] = array("user_id"=> $modifiedUser['CacheLastModifiedUser']['user_id']);
													}
												}
												$userData['Deleted_user'] = $userListsArr;
												$key = 'institutionDirectory:'.$userCurrentCompany['UserEmployment']['company_id'];
												$cacObjEtag['robj']->set($key,json_encode($userData, true));
											}
											$responseData = array('method_name'=> 'getStaticDirectoryData','status'=>'1','response_code'=>'200', 'message'=> ERROR_200,"Etag"=> $eTagData, 'data'=> $userData,'admin_qb_id' => $group_admin, 'Company'=>$userEmploymentData, 'delete_static_directory'=>1);

										}
									}
									else{
										echo "No Connection";exit();
									}
								}
							}
						}else{
							$responseData = array('method_name'=> 'getStaticDirectoryData','status'=>'0','response_code'=>'658', 'message'=> ERROR_658);
						}

					}else{
						$responseData = array('method_name'=> 'getStaticDirectoryData','status'=>'0','response_code'=>'602', 'message'=> ERROR_602);
					}
				}else{
					$isCurrentUser = 1;
					$statusResponse = $this->getUserProfileStatuscheck($dataInput['user_id'],$isCurrentUser);
					$responseData = array('method_name'=> 'getStaticDirectoryData','status'=>'0','response_code'=>$statusResponse['response_code'], 'message'=> $statusResponse['message']);
				}
			}else{
				$responseData = array('method_name'=> 'getStaticDirectoryData','status'=>'0','response_code'=>'618', 'message'=> ERROR_618);
			}
		}else{
		$responseData = array('method_name'=> 'getStaticDirectoryData','status'=>'0','response_code'=>'611','message'=> ERROR_611);
		}
		echo json_encode($responseData);
		exit;
	}

	/*
	--------------------------------------------------------------------------
	On: 13-11-2017
	I/P: JSON
	O/P: JSON, user lists
	Desc: Formatting static directory data
	--------------------------------------------------------------------------
	*/
	public function directoryStaticDataFormat( $userData = array(), $loggedinUserId = NULL, $currentCompanyId=NULL, $checkCompnayIsPaid=0 ){
		$userDataList = array();
		if( !empty($userData) ){
		//** User Data
			foreach( $userData as $ud ){
				//** Check for user from pending institution[START]
				$isApproveByInstitutionCheck = 1;
				$userEnterpriseCheck = $this->EnterpriseUserList->find("first", array("conditions"=> array("company_id"=> $currentCompanyId, "email"=> $ud['User']['email'], "status"=> 1)));
				if(empty($userEnterpriseCheck)){ $isApproveByInstitutionCheck = 0;}
				//** Check for user from pending institution[END]
				if($checkCompnayIsPaid > 0){
				if($isApproveByInstitutionCheck){
					//** Get Profession
					$professionArr = array();
					$professionDetails = $this->Profession->find("first", array("conditions"=> array("id"=> $ud['UserProfile']['profession_id'])));
					$professionId = !empty($professionDetails['Profession']['id']) ? $professionDetails['Profession']['id']:0;
					$professionType = !empty($professionDetails['Profession']['profession_type']) ? $professionDetails['Profession']['profession_type']:'';
					$professionArr = array("profession_type"=>$professionType);
					//** Get Contact Info
					$phone = array(); $isConactFound = 0;
					if(!empty($ud['UserProfile']['contact_no'])){
						$phone = array("mobile_no"=>!empty($ud['UserProfile']['contact_no']) ? $ud['UserProfile']['contact_no']:"");
						$isConactFound = 1;
					}
					//** Get QB details [START]
					$qbInfo = array(); $qbDetails = array();
					$qbDetails = $this->UserQbDetail->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'])));
					$qbId = !empty($qbDetails['UserQbDetail']['qb_id']) ? $qbDetails['UserQbDetail']['qb_id'] : 0;
					$qbInfo = array("id"=> $qbId);
					//** Get QB details [END]
					//** Get role status [START]
					$userRoleStatusData = $this->UserProfile->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'])));
					//** Get role status [END]
					$imgPath = "";
					$imgPath = !empty($ud['UserProfile']['profile_img']) ? AMAZON_PATH . $ud['User']['id']. '/profile/' . $ud['UserProfile']['profile_img']:"";
					$thumbnailimgPath = "";
					$thumbnailimgPath = !empty($ud['UserProfile']['thumbnail_img']) ? PROFILE_PICTURE_THUMBNAIL_IMAGE_PATH . $ud['User']['id']. '/profile/' . $ud['UserProfile']['thumbnail_img']:"";
					if(!empty($qbId)){
					$userDataList[] = array(
									'user_id'=> $this->Settergetter->getterUserId($ud['User']['id']),
									'email'=>	$this->Settergetter->getterEmail($ud['User']['email']),
									'first_name'=> $this->Settergetter->getterFirstName($ud['UserProfile']['first_name']),
									'last_name'=> $this->Settergetter->getterLastName($ud['UserProfile']['last_name']),
									'profile_img'=> $this->Settergetter->getterProfileImg($imgPath),
									'thumbnail_img'=> $this->Settergetter->getterProfileImg($thumbnailimgPath),
									// 'thumbnail_img'=> $thumbnailimgPath,
									'country_code'=> $this->Settergetter->getterCountryCode($ud['Country']['country_code']),
									'profession'=> !empty($professionArr) ? $professionArr : (object) array() ,
									'phone'=> !empty($phone) ? $phone : array("mobile_no"=>""),
									'qb_details'=> !empty($qbInfo) ? $qbInfo : (object) array(),
									);

						}
					}
				}else{
					//** Get Profession
					$professionArr = array();
					$professionDetails = $this->Profession->find("first", array("conditions"=> array("id"=> $ud['UserProfile']['profession_id'])));
					$professionId = !empty($professionDetails['Profession']['id']) ? $professionDetails['Profession']['id']:0;
					$professionType = !empty($professionDetails['Profession']['profession_type']) ? $professionDetails['Profession']['profession_type']:'';
					$professionArr = array("profession_type"=>$professionType);
					//** Get Contact Info
					$phone = array(); $isConactFound = 0;
					if(!empty($ud['UserProfile']['contact_no'])){
						$phone = array("mobile_no"=>!empty($ud['UserProfile']['contact_no']) ? $ud['UserProfile']['contact_no']:"");
						$isConactFound = 1;
					}
					//** Get QB details [START]
					$qbInfo = array(); $qbDetails = array();
					$qbDetails = $this->UserQbDetail->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'])));
					$qbId = !empty($qbDetails['UserQbDetail']['qb_id']) ? $qbDetails['UserQbDetail']['qb_id'] : 0;
					$qbInfo = array("id"=> $qbId);
					//** Get QB details [END]
					//** Get role status [START]
					$userRoleStatusData = $this->UserProfile->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'])));
					//** Get role status [END]
					$imgPath = "";
					$imgPath = !empty($ud['UserProfile']['profile_img']) ? AMAZON_PATH . $ud['User']['id']. '/profile/' . $ud['UserProfile']['profile_img']:"";
					$thumbnailimgPath = "";
					$thumbnailimgPath = !empty($ud['UserProfile']['thumbnail_img']) ? PROFILE_PICTURE_THUMBNAIL_IMAGE_PATH . $ud['User']['id']. '/profile/' . $ud['UserProfile']['thumbnail_img']:"";
					if(!empty($qbId)){
					$userDataList[] = array(
									'user_id'=> $this->Settergetter->getterUserId($ud['User']['id']),
									'email'=>	$this->Settergetter->getterEmail($ud['User']['email']),
									'first_name'=> $this->Settergetter->getterFirstName($ud['UserProfile']['first_name']),
									'last_name'=> $this->Settergetter->getterLastName($ud['UserProfile']['last_name']),
									'profile_img'=> $this->Settergetter->getterProfileImg($imgPath),
									'thumbnail_img'=> $this->Settergetter->getterProfileImg($thumbnailimgPath),
									// 'thumbnail_img'=> $thumbnailimgPath,
									'country_code'=> $this->Settergetter->getterCountryCode($ud['Country']['country_code']),
									'profession'=> !empty($professionArr) ? $professionArr : (object) array() ,
									'phone'=> !empty($phone) ? $phone : array("mobile_no"=>""),
									'qb_details'=> !empty($qbInfo) ? $qbInfo : (object) array(),
									);

						}
				}
			}
		}
		return $userDataList;
	}

	/*
	--------------------------------------------------------------------------
	On: 14-11-2017
	I/P: JSON
	O/P: JSON, user lists
	Desc: Get dynamic directory data
	--------------------------------------------------------------------------
	*/
	public function getDynamicDirectoryData(){
		$startMilliseconds = round(microtime(true) * 1000);
		if($this->request->is('post')){
					$dataInput = $this->request->input('json_decode', true) ;
					if($this->User->findById( $dataInput['user_id'] ) )
					{
					$loggedinUserId = isset($dataInput['user_id']) ? $dataInput['user_id'] : '';
					$newEtag = '';
					$userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $loggedinUserId)));
					if($userDetails['User']['status']==1 && $userDetails['User']['approved']==1)
					{
						if( $this->validateToken() && $this->validateAccessKey() ){
					$userCurrentCompany = $this->UserEmployment->find("first", array("conditions"=> array("user_id"=> $loggedinUserId, "is_current"=> 1), "order"=> array("id DESC")));
					$params['user_current_company_id'] = $userCurrentCompany['UserEmployment']['company_id'];
					$enterpriseUserCheck = $this->EnterpriseUserList->find("count", array("conditions"=> array("email"=> stripslashes($userDetails['User']['email']),"company_id"=> $userCurrentCompany['UserEmployment']['company_id'],"status"=> 1)));
					$companySubscriptionCheck=$this->CompanyName->find("count", array("conditions"=> array("id"=>$userCurrentCompany['UserEmployment']['company_id'], "status"=>1,"is_subscribed"=>1)));

					/*$eTagData = 0;
					//** If static eTag not exists add first time[START]
					$eTagData = array('user_id'=>$loggedinUserId, 'company_id'=>$userCurrentCompany['UserEmployment']['company_id'],'type'=>'dynamic_directory_changed');
					$chkStaticModifiedDateExists = $this->CacheLastModifiedUser->find("count", array("conditions"=> $eTagData));
					if($chkStaticModifiedDateExists==0){
						$this->CacheLastModifiedUser->save($eTagData);
					}

					$eTag = date('Y-m-d H:i:s', $dataInput['eTag']);*/

					$etagChangeParams['key'] = 'dynamicDirectoryEtag:'.$userCurrentCompany['UserEmployment']['company_id'];
					$etagChanged = $this->checkEtagNotModifiedDynamicDirectory($etagChangeParams);
					if($etagChanged['code'] !=304){
						$cacObjEtag = $this->Cache->redisConn();
						if(($cacObjEtag['connection']) && empty($cacObjEtag['errors'])){
							//if($cacObjEtag['robj']->exists($etagChangeParams['key'])){
								$etagLastModified = strtotime(date('Y-m-d H:i:s'));
								$eTagNewVal = md5($etagChangeParams['key'] . rand(1,10000) . $etagLastModified);
								$cacObjEtag['robj']->hset($etagChangeParams['key'],'eTag' , $eTagNewVal);
								$cacObjEtag['robj']->hset($etagChangeParams['key'],'lastmodified' , $etagLastModified);
							//}
						}
						//$eTagData = $eTagNewVal.'~'.$etagChanged['previousLastmodified'];
						$eTagData = $eTagNewVal.'~'.$etagLastModified;
						$previousDateModified = explode("~",$etagChanged['etagHeader']);
						$eTagDate = date('Y-m-d H:i:s', end($previousDateModified));
					}else{
						$eTagData = $etagChanged['etagHeader'];
						if(isset($etagChanged['etagHeader']) && empty($etagChanged['etagHeader'])){
							$eTagDate = '0000-00-00 00:00:00';
						}else{
							$previousDateModified = explode("~",$etagChanged['etagHeader']);
							$eTagDate = date('Y-m-d H:i:s', end($previousDateModified));
						}
					}
					//echo "<pre>"; print_r($etagChanged); die;
					$userListsArr = array();
					$getModifiedUser=$this->CacheLastModifiedUser->find("all", array("conditions"=> array("company_id"=>$userCurrentCompany['UserEmployment']['company_id'], "type"=>"dynamic_directory_changed","last_modified > "=> $eTagDate), 'group'=>array('CacheLastModifiedUser.user_id'),'order'=>array('last_modified DESC')));
					//echo "<pre>";print_r($getModifiedUser); exit();
					if($companySubscriptionCheck ==1 && $enterpriseUserCheck ==0){
						$userData['diretory_access'] = 655;
						$userData['message'] = ERROR_655;
						$responseData = array('method_name'=> 'getDynamicDirectoryData','status'=>'1','response_code'=>'200', 'message'=> ERROR_200, 'data'=> $userData);
					}else{
							//** Directory Users
							//if(isset($dataInput['eTag'])){
								/*if($dataInput['eTag'] == 0)
								{
									$userLists = array();
									if($params['user_current_company_id'] != -1){ //** Don't show if user selected company None(i.e.-1)
										$params['user_current_company_id'] = $userCurrentCompany['UserEmployment']['company_id'];
										$params['loggedin_user_countryid'] = $userDetails['UserProfile']['country_id'];
										$userLists = $this->User->getDynamicUserDirectoryData( $params, $loggedinUserId);
									}
									//$newEtag = $getModifiedUser[0]['CacheLastModifiedUser']['last_modified'];
									$newEtag = $eTagDate;
									$userData['User'] = $this->dynamicDirectoryDataFormat($userLists, $userCurrentCompany['UserEmployment']['company_id'], $companySubscriptionCheck);
									$eTagVal = strtotime($newEtag);
									$responseData = array('method_name'=> 'getDynamicDirectoryData','status'=>'1','response_code'=>'200', 'message'=> ERROR_200, 'data'=> $userData, "eTag"=> $eTagVal);
								}
								else
								{*/
									//echo "<pre>";print_r($getModifiedUser);exit();
									//$newEtag = $getModifiedUser[0]['CacheLastModifiedUser']['last_modified'];
									//$newEtag = $eTagDate;
								if(isset($etagChanged['etagHeader']) && empty($etagChanged['etagHeader'])){
									$userLists = array();
									if($params['user_current_company_id'] != -1){ //** Don't show if user selected company None(i.e.-1)
										$params['user_current_company_id'] = $userCurrentCompany['UserEmployment']['company_id'];
										$params['loggedin_user_countryid'] = $userDetails['UserProfile']['country_id'];
										$userLists = $this->User->getDynamicUserDirectoryData( $params, $loggedinUserId);
									}
									//$newEtag = $getModifiedUser[0]['CacheLastModifiedUser']['last_modified'];
									$newEtag = $eTagDate;
									$userData['User'] = $this->dynamicDirectoryDataFormat($userLists, $userCurrentCompany['UserEmployment']['company_id'], $companySubscriptionCheck);
									//$eTagVal = strtotime($newEtag);
									$responseData = array('method_name'=> 'getDynamicDirectoryData','status'=>'1','response_code'=>'200', 'message'=> ERROR_200, 'data'=> $userData, "Etag"=> $eTagData);
								}else{
									if(!empty($getModifiedUser))
									{
										foreach ($getModifiedUser as $modifiedUser) {
											$userRoleTags = array();
											$getUserData = $this->User->getChangedUserData( $modifiedUser['CacheLastModifiedUser']['user_id'], $userCurrentCompany['UserEmployment']['company_id']);
											if(! empty($getUserData))
											{
												$roleTagParams['user_id'] = $modifiedUser['CacheLastModifiedUser']['user_id'];
												// $userRoleTgasData = $this->UserRoleTag->getUserRoleTag($roleTagParams);
												// foreach($userRoleTgasData as $urt){
												// 	$userRoleTags[] = array("key"=> $urt['RoleTag']['key'],"value"=> array($urt['RoleTag']['value']));
												// }
												if(!empty($getUserData[0]['UserProfile']['custom_role']))
												{
													$userRoleTags = json_decode($getUserData[0]['UserProfile']['custom_role'], true);
												}
												else
												{
													$userRoleTags = array();
												}
												$getUserDndStatus = $this->UserDndStatus->getUserDndStatus($modifiedUser['CacheLastModifiedUser']['user_id']);
												$dndStatusOptions = $this->DndStatusOption->getDndStatusOptions($getUserDndStatus['UserDndStatus']['dnd_status_id']);
												// $getUserBatonRolesDetail = $this->getUserBatonRoles($modifiedUser['CacheLastModifiedUser']['user_id']);
												if(!empty($getUserData[0]['UserProfile']['custom_baton_roles']))
												{
													$getUserBatonRolesDetail = json_decode($getUserData[0]['UserProfile']['custom_baton_roles'], true);
												}
												else
												{
													$getUserBatonRolesDetail = array();
												}
												if($getUserDndStatus['UserDndStatus']['is_active'])
												{
													$userDndDetail = array('dnd_status_id'=>$getUserDndStatus['UserDndStatus']['dnd_status_id'], 'dnd_name'=>$dndStatusOptions['DndStatusOption']['dnd_name'],
													'icon_selected'=>$dndStatusOptions['DndStatusOption']['icon_selected'], 'end_time'=>$getUserDndStatus['UserDndStatus']['end_time']);

												}
												else{
													$userDndDetail = array('dnd_status_id'=>"0");
												}
												$imgPath = "";
												$imgPath = !empty($getUserData[0]['UserProfile']['profile_img']) ? AMAZON_PATH . $getUserData[0]['User']['id']. '/profile/' . $getUserData[0]['UserProfile']['profile_img']:"";
												$thumbnailimgPath = "";
												$thumbnailimgPath = !empty($getUserData[0]['UserProfile']['thumbnail_img']) ? PROFILE_PICTURE_THUMBNAIL_IMAGE_PATH . $getUserData[0]['User']['id']. '/profile/' . $getUserData[0]['UserProfile']['thumbnail_img']:"";
												$userListsArr[] = array(
													"at_work"=> isset($getUserData[0]['UserDutyLog']['atwork_status']) ? $getUserData[0]['UserDutyLog']['atwork_status'] : "0",
													"on_duty"=> isset($getUserData[0]['UserDutyLog']['status']) ? $getUserData[0]['UserDutyLog']['status'] : "0",
													"role_status"=> isset($getUserData[0]['UserProfile']['role_status']) ? $getUserData[0]['UserProfile']['role_status'] : '',
													"role_tags"=> $userRoleTags,
													'profile_img'=> $imgPath,
													'thumbnail_img'=> $thumbnailimgPath,
													"user_id"=> isset($modifiedUser['CacheLastModifiedUser']['user_id']) ? (int) $modifiedUser['CacheLastModifiedUser']['user_id'] : (int) 0,
													'is_dnd_active'=> isset($getUserDndStatus['UserDndStatus']['is_active']) ? (string)$getUserDndStatus['UserDndStatus']['is_active'] : "0",
													'dnd_status'=> !empty($userDndDetail)?$userDndDetail:(object) array(),
													 'baton_roles'=> $getUserBatonRolesDetail
													);

											}
										}
										$userDataArr['User'] = $userListsArr;
										//$userDataArr['User'] = $this->dynamicDirectoryDataFormat($userListsArr);
										//echo "<pre>";print_r($userDataArr);exit();
										//$eTagVal = strtotime($newEtag);
										$responseData = array('method_name'=> 'getDynamicDirectoryData','status'=>'1','response_code'=>'200', 'message'=> ERROR_200, 'data'=> $userDataArr, "Etag"=> $eTagData);
									}
									else
									{
										//echo "<pre>";print_r("TEST14");exit();
										//$newEtag1 = $eTagData; //date('Y-m-d H:i:s');
										//$eTagVal1 = strtotime($newEtag1);
										$responseData = array('method_name'=> 'getDynamicDirectoryData','status'=>'0','response_code'=>'304', 'message'=> SUCCESS_304, "Etag"=> $eTagData);
									}
								}
							//}
						}
					}else{
								        $responseData = array('method_name'=> 'getDynamicDirectoryData','status'=>'0','response_code'=>'602', 'message'=> ERROR_602);
							         }

								}
								else
								{
									$isCurrentUser = 1;
									$statusResponse = $this->getUserProfileStatuscheck($dataInput['user_id'],$isCurrentUser);
									$responseData = array('method_name'=> 'getDynamicDirectoryData','status'=>'0','response_code'=>$statusResponse['response_code'], 'message'=> $statusResponse['message']);
								}
							}
						else
						{
							$responseData = array('method_name'=> 'getDynamicDirectoryData','status'=>'0','response_code'=>'618', 'message'=> ERROR_618);
						}

		}else{
				$responseData = array('method_name'=> 'getDynamicDirectoryData','status'=>'0','response_code'=>'611','message'=> ERROR_611);
		}
		echo json_encode($responseData);
		//** Write API response time on log file[START]
		$endMilliseconds = round(microtime(true) * 1000);
		$this->Common->writeLog($dataInput['user_id'],$startMilliseconds,$endMilliseconds,$userCurrentCompany['UserEmployment']['company_id']);
		//** Write API response time on log file[END]
		exit;
	}

	/*
	--------------------------------------------------------------------------
	On: 14-11-2017
	I/P: JSON
	O/P: JSON, user lists
	Desc: Formatting dynamic directory data
	--------------------------------------------------------------------------
	*/
	public function dynamicDirectoryDataFormat( $userData = array(), $currentCompanyId=NULL, $checkCompnayIsPaid=0){
		$userDataList = array();
		if( !empty($userData) ){
				//** User Data
				foreach( $userData as $ud ){
						//** Check for user from pending institution[START]
				$isApproveByInstitutionCheck = 1;
				$userEnterpriseCheck = $this->EnterpriseUserList->find("first", array("conditions"=> array("company_id"=> $currentCompanyId, "email"=> $ud['User']['email'], "status"=> 1)));
				if(empty($userEnterpriseCheck)){ $isApproveByInstitutionCheck = 0;}
				//** Check for user from pending institution[END]
				if($checkCompnayIsPaid > 0){
				if($isApproveByInstitutionCheck){
						$qbInfo = array(); $qbDetails = array();
						$qbDetails = $this->UserQbDetail->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'])));
						$qbId = !empty($qbDetails['UserQbDetail']['qb_id']) ? $qbDetails['UserQbDetail']['qb_id'] : 0;
						$qbInfo = array("id"=> $qbId);
						//** Get user role tags[START]
						$userRoleTags = array();
						$paramsRoleTag['user_id'] = $ud['User']['id'];
						// $userRoleTgasData = $this->UserRoleTag->getUserRoleTag($paramsRoleTag);
						// foreach($userRoleTgasData as $urt){
						// 	$userRoleTags[] = array("key"=> $urt['RoleTag']['key'],"value"=> array($urt['RoleTag']['value']));
						// }
						if(!empty($ud['UserProfile']['custom_role']))
						{
							$userRoleTags = json_decode($ud['UserProfile']['custom_role'], true);
						}
						else
						{
							$userRoleTags = array();
						}

						//** Get user role tags[END]
						if(!empty($qbId)){
						$getUserDndStatus = $this->UserDndStatus->getUserDndStatus($ud['User']['id']);
						$dndStatusOptions = $this->DndStatusOption->getDndStatusOptions($getUserDndStatus['UserDndStatus']['dnd_status_id']);
						if($getUserDndStatus['UserDndStatus']['is_active'])
						{
							$userDndDetail = array('dnd_status_id'=>$getUserDndStatus['UserDndStatus']['dnd_status_id'], 'dnd_name'=>$dndStatusOptions['DndStatusOption']['dnd_name'],
							'icon_selected'=>$dndStatusOptions['DndStatusOption']['icon_selected'], 'end_time'=>$getUserDndStatus['UserDndStatus']['end_time']);

						}
						else
						{
							$userDndDetail = array('dnd_status_id'=>"0");
						}
						//$getUserBatonRolesDetail = $this->getUserBatonRoles($ud['User']['id']);
						if(!empty($ud['UserProfile']['custom_baton_roles']))
						{
							$getUserBatonRolesDetail = json_decode($ud['UserProfile']['custom_baton_roles'], true);
						}
						else
						{
							$getUserBatonRolesDetail = array();
						}
						$imgPath = "";
						$imgPath = !empty($ud['UserProfile']['profile_img']) ? AMAZON_PATH . $ud['User']['id']. '/profile/' . $ud['UserProfile']['profile_img']:"";
						$thumbnailimgPath = "";
						$thumbnailimgPath = !empty($ud['UserProfile']['thumbnail_img']) ? PROFILE_PICTURE_THUMBNAIL_IMAGE_PATH . $ud['User']['id']. '/profile/' . $ud['UserProfile']['thumbnail_img']:"";
						$userDataList[] = array(
										'user_id'=> $this->Settergetter->getterUserId($ud['User']['id']),
										'on_duty'=>	$this->Settergetter->getterOnduty($ud['UserDutyLog']['status']),
										'at_work'=> $this->Settergetter->getterAtWork($ud['UserDutyLog']['atwork_status']),
										'role_tags'=> $userRoleTags,
										'role_status'=> $this->Settergetter->getterRoleStatus($ud['UserProfile']['role_status']),
										'profile_img'=> $imgPath,
										'thumbnail_img'=> $thumbnailimgPath,
										'is_dnd_active'=> isset($getUserDndStatus['UserDndStatus']['is_active']) ? (string)$getUserDndStatus['UserDndStatus']['is_active'] : "0",
										'dnd_status'=> !empty($userDndDetail)?$userDndDetail:(object) array(),
										'baton_roles'=> $getUserBatonRolesDetail
										);
						}
					}
				}else{
					$qbInfo = array(); $qbDetails = array();
						$qbDetails = $this->UserQbDetail->find("first", array("conditions"=> array("user_id"=> $ud['User']['id'])));
						$qbId = !empty($qbDetails['UserQbDetail']['qb_id']) ? $qbDetails['UserQbDetail']['qb_id'] : 0;
						$qbInfo = array("id"=> $qbId);
						//** Get user role tags[START]
						$userRoleTags = array();
						$paramsRoleTag['user_id'] = $ud['User']['id'];
						// $userRoleTgasData = $this->UserRoleTag->getUserRoleTag($paramsRoleTag);
						// foreach($userRoleTgasData as $urt){
						// 	$userRoleTags[] = array("key"=> $urt['RoleTag']['key'],"value"=> array($urt['RoleTag']['value']));
						// }
						if(!empty($ud['UserProfile']['custom_role']))
						{
							$userRoleTags = json_decode($ud['UserProfile']['custom_role'], true);
						}
						else
						{
							$userRoleTags = array();
						}

						//** Get user role tags[END]
						if(!empty($qbId)){
						$getUserDndStatus = $this->UserDndStatus->getUserDndStatus($ud['User']['id']);
						$dndStatusOptions = $this->DndStatusOption->getDndStatusOptions($getUserDndStatus['UserDndStatus']['dnd_status_id']);
						if($getUserDndStatus['UserDndStatus']['is_active'])
						{
							$userDndDetail = array('dnd_status_id'=>$getUserDndStatus['UserDndStatus']['dnd_status_id'], 'dnd_name'=>$dndStatusOptions['DndStatusOption']['dnd_name'],
							'icon_selected'=>$dndStatusOptions['DndStatusOption']['icon_selected'], 'end_time'=>$getUserDndStatus['UserDndStatus']['end_time']);

						}
						else
						{
							$userDndDetail = array('dnd_status_id'=>"0");
						}
						// $getUserBatonRolesDetail = $this->getUserBatonRoles($ud['User']['id']);

						if(!empty($ud['UserProfile']['custom_baton_roles']))
						{
							$getUserBatonRolesDetail = json_decode($ud['UserProfile']['custom_baton_roles'], true);
						}
						else
						{
							$getUserBatonRolesDetail = array();
						}

						$imgPath = "";
						$imgPath = !empty($ud['UserProfile']['profile_img']) ? AMAZON_PATH . $ud['User']['id']. '/profile/' . $ud['UserProfile']['profile_img']:"";
						$thumbnailimgPath = "";
						$thumbnailimgPath = !empty($ud['UserProfile']['thumbnail_img']) ? PROFILE_PICTURE_THUMBNAIL_IMAGE_PATH . $ud['User']['id']. '/profile/' . $ud['UserProfile']['thumbnail_img']:"";
						$userDataList[] = array(
										'user_id'=> $this->Settergetter->getterUserId($ud['User']['id']),
										'on_duty'=>	$this->Settergetter->getterOnduty($ud['UserDutyLog']['status']),
										'at_work'=> $this->Settergetter->getterAtWork($ud['UserDutyLog']['atwork_status']),
										'role_tags'=> $userRoleTags,
										'role_status'=> $this->Settergetter->getterRoleStatus($ud['UserProfile']['role_status']),
										'profile_img'=> $imgPath,
										'thumbnail_img'=> $thumbnailimgPath,
										'is_dnd_active'=> isset($getUserDndStatus['UserDndStatus']['is_active']) ? (string)$getUserDndStatus['UserDndStatus']['is_active'] : "0",
										'dnd_status'=> !empty($userDndDetail)?$userDndDetail:(object) array(),
										'baton_roles'=> $getUserBatonRolesDetail
										);
						}
				}
				}

		}
		return $userDataList;
	}

	public function getUserBatonRoles($userId)
	{
		$batonRoleDataList = array();
		if(!empty($userId))
		{
			$userbatonRolestatus = array(1,2,5,7);
			$batonRoleDetail = $this->UserBatonRole->find("all", array("conditions"=> array("from_user"=> $userId,"status"=>$userbatonRolestatus,"is_active"=>1),"group"=>array('UserBatonRole.role_id'),'order'=>array('UserBatonRole.updated_at')));
			foreach ($batonRoleDetail as  $value) {

				$batonRoleName = $this->BatonRole->find("first", array("conditions"=> array("id"=> $value['UserBatonRole']['role_id'])));
				$batonRoleDataList[] = array(
							'role_id'=> isset($value['UserBatonRole']['role_id']) ? $value['UserBatonRole']['role_id'] : "",
							'is_active'=>	isset($value['UserBatonRole']['is_active']) ? $value['UserBatonRole']['is_active'] : 0,
							'role_name'=>	isset($batonRoleName['BatonRole']['baton_roles']) ? $batonRoleName['BatonRole']['baton_roles'] : "",
							'type'=>isset($value['UserBatonRole']['status']) ? $value['UserBatonRole']['status'] : "0",
							);
			}

			return $batonRoleDataList;
		}
	}

	/*
	--------------------------------------------------------------------------
	On: 13-11-2017
	I/P: JSON
	O/P: JSON, user lists
	Desc: Formatting static directory data
	--------------------------------------------------------------------------
	*/
	public function directoryStaticDataFormatLiteOne( $userData = array(), $loggedinUserId = NULL, $currentCompanyId=NULL, $checkCompnayIsPaid=0 ){
		$userDataList = array();
		if( !empty($userData) ){
		//** User Data
			foreach( $userData as $ud ){
			//** Get Contact Info

			$professionArr = array();
			$professionArr = array("profession_type"=>$ud['Profession']['profession_type']);
			$userRoleTags = array();
			$userIds = $ud['User']['id'];
			$oncallAccess = 0;
			if($ud[0]['oncallaccess'] && $ud[0]['oncallaccess'] > 0)
			{
			  $oncallAccess = 1;
			}
			if(!empty($ud['UserProfile']['custom_role']))
			{
				$userRoleTags = json_decode($ud['UserProfile']['custom_role'], true);
			}
			else
			{
				$userRoleTags = array();
			}
			if(!empty($ud['UserProfile']['custom_baton_roles']))
			{
				$batonRoleArr = json_decode($ud['UserProfile']['custom_baton_roles'], true);
			}
			else
			{
				$batonRoleArr = array();
			}
			$qbInfo = array();
			$qbId = !empty($ud['UserQbDetail']['qb_id']) ? $ud['UserQbDetail']['qb_id'] : 0;
			$qbInfo = array("id"=> $qbId);
			$phone = array(); $isConactFound = 0;
			if(!empty($ud['UserProfile']['contact_no'])){
				$phone = array("mobile_no"=>!empty($ud['UserProfile']['contact_no']) ? $ud['UserProfile']['contact_no']:"");
				$isConactFound = 1;
			}
			$imgPath = "";
			$imgPath = !empty($ud['UserProfile']['profile_img']) ? AMAZON_PATH . $ud['User']['id']. '/profile/' . $ud['UserProfile']['profile_img']:"";
			$thumbnailimgPath = "";
			$thumbnailimgPath = !empty($ud['UserProfile']['thumbnail_img']) ? PROFILE_PICTURE_THUMBNAIL_IMAGE_PATH . $ud['User']['id']. '/profile/' . $ud['UserProfile']['thumbnail_img']:"";
			if(!empty($ud['UserDndStatuse']['dnd_status_id']))
			{
				$dndarr = array('dnd_status_id'=>$ud['UserDndStatuse']['dnd_status_id'], 'dnd_name'=>$ud['DndStatusOption']['dnd_name'],
			'icon_selected'=>$ud['DndStatusOption']['icon_selected'], 'end_time'=>$ud['UserDndStatuse']['end_time']);
			}
			else
			{
				$dndarr = array('dnd_status_id'=>"0");
			}
			if(!empty($qbId)){
			$userDataList[] = array(
							'user_id'=> $this->Settergetter->getterUserId($ud['User']['id']),
							'email'=>	$this->Settergetter->getterEmail($ud['User']['email']),
							'first_name'=> $this->Settergetter->getterFirstName($ud['UserProfile']['first_name']),
							'last_name'=> $this->Settergetter->getterLastName($ud['UserProfile']['last_name']),
							'profile_img'=> $this->Settergetter->getterProfileImg($imgPath),
							'thumbnail_img'=> $this->Settergetter->getterProfileImg($thumbnailimgPath),
							'country_code'=> $this->Settergetter->getterCountryCode($ud['Country']['country_code']),
							'profession'=> !empty($professionArr) ? $professionArr : (object) array() ,
							'phone'=> !empty($phone) ? $phone : array("mobile_no"=>""),
							'qb_details'=> !empty($qbInfo) ? $qbInfo : (object) array(),
							'on_duty'=> isset($ud['UserDutyLog']['status']) ? $ud['UserDutyLog']['status'] : 0,
							'at_work'=>isset($ud['UserDutyLog']['atwork_status']) ? $ud['UserDutyLog']['atwork_status'] : 0,
							'role_status'=>isset($ud['UserProfile']['role_status']) ? $ud['UserProfile']['role_status'] : "",
							'is_dnd_active'=> isset($ud['UserDndStatuse']['is_active']) ? (string)$ud['UserDndStatuse']['is_active'] : "0",
							'dnd_status'=> !empty($dndarr)?$dndarr:(object) array(),
							'role_tags'=>$userRoleTags,
							'baton_roles'=>$batonRoleArr,
							'oncall_access'=> (int) $oncallAccess
							);

				}
			}
		}
		return $userDataList;
	}


	public function getUserDirectoryData(){
		$group_admin = 0;
		if($this->request->is('post'))
		{
			$dataInput = $this->request->input('json_decode', true) ;
			if($this->User->findById( $dataInput['user_id'] ) )
			{
				$loggedinUserId = isset($dataInput['user_id']) ? $dataInput['user_id'] : '';
				$userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $loggedinUserId)));
				if($userDetails['User']['status']==1 && $userDetails['User']['approved']==1)
				{
					// if( $this->validateToken() && $this->validateAccessKey() )
					// {
						$params['loggedin_user_countryid'] = $userDetails['UserProfile']['country_id'];
						$userCurrentCompany = $this->UserEmployment->find("first", array("conditions"=> array("user_id"=> $loggedinUserId, "is_current"=> 1), "order"=> array("id DESC")));
						$params['user_current_company_id'] = $userCurrentCompany['UserEmployment']['company_id'];
						if($params['user_current_company_id'] != -1)
						{
							$enterpriseUserCheck = $this->EnterpriseUserList->find("count", array("conditions"=> array("email"=> stripslashes($userDetails['User']['email']),"company_id"=> $userCurrentCompany['UserEmployment']['company_id'],"status"=> 1)));
							$companySubscriptionCheck=$this->CompanyName->find("count", array("conditions"=> array("id"=>$userCurrentCompany['UserEmployment']['company_id'], "status"=>1,"is_subscribed"=>1)));

							$companyDetail=$this->CompanyName->find("first", array("conditions"=> array("id"=>$userCurrentCompany['UserEmployment']['company_id'])));

							$adminDetail = $this->InstitutionGroupAdmin->find("first", array("conditions"=> array("institute_id"=>$userCurrentCompany['UserEmployment']['company_id'],'status'=>1)));

							if(!empty($adminDetail)){
								$group_admin = $adminDetail['InstitutionGroupAdmin']['qb_id'];
							}

							$etagChangeParams['key'] = 'staticDirectoryEtag:'.$userCurrentCompany['UserEmployment']['company_id'];
							$etagChangeParams['institution_key'] = $userCurrentCompany['UserEmployment']['company_id'];
							$etagChanged = $this->checkEtagNotModified($etagChangeParams);
							// echo "<pre>";print_r($etagChanged);exit();
							$etagLastModified = strtotime(date('Y-m-d H:i:s'));
							if($etagChanged['code'] !=304 && $etagChanged['code'] !=200){
								$cacObjEtag = $this->Cache->redisConn();
								if(($cacObjEtag['connection']) && empty($cacObjEtag['errors'])){
									// if($cacObjEtag['robj']->exists($etagChangeParams['key'])){
										$etagLastModified = strtotime(date('Y-m-d H:i:s'));
										$eTagNewVal = md5($etagChangeParams['key'] . rand(1,10000) . $etagLastModified);
										$cacObjEtag['robj']->hset($etagChangeParams['key'],'eTag' , $eTagNewVal);
										$cacObjEtag['robj']->hset($etagChangeParams['key'],'lastmodified' , $etagLastModified);
									// }
								}
								$eTagData = $eTagNewVal;
								$eTagDate = date('Y-m-d H:i:s', $etagLastModified);
								$dynamicETagData = $eTagNewVal.'~'.$etagLastModified;
							}else{
								$eTagData = $etagChanged['eTag'];
								$dynamicETagData = $etagChanged['eTag'].'~'.$etagLastModified;
								if(empty($etagChanged['etagHeader'])){
									$eTagDate = '0000-00-00 00:00:00';
								}else{
									$eTagDate = date('Y-m-d H:i:s', $etagChanged['lastmodified']);
								}
							}
							$dataa = $val;
							//** Add company details[START]
							$userEmploymentData = array("id"=> $userCurrentCompany['UserEmployment']['id'], "company_id"=> $userCurrentCompany['UserEmployment']['company_id'] ,"company_name"=> $companyDetail['CompanyName']['company_name'], "patient_number_type"=>$companyDetail['CompanyName']['patient_number_type'], "is_current"=> $userCurrentCompany['UserEmployment']['is_current']);
							//** Add company details[END]

							if($companySubscriptionCheck ==1 && $enterpriseUserCheck ==0){
								$userData['diretory_access'] = 655;
								$userData['message'] = ERROR_655;
								if($userCurrentCompany['UserEmployment']['company_id'] == $dataInput['institution_id'])
								{
									$responseData = array('method_name'=> 'getUserDirectoryData','status'=>'1','response_code'=>'200', 'message'=> ERROR_200,"Etag"=> $eTagData, "EtagCeck"=> "One", "EtagCeckData"=>$eTagDataCheck, 'data'=> $userData, 'admin_qb_id' => $group_admin);
								}
								else
								{
									$responseData = array('method_name'=> 'getUserDirectoryData','status'=>'1','response_code'=>'200', 'message'=> ERROR_200,"Etag"=> $eTagData, 'data'=> $userData, 'Company'=>$userEmploymentData, 'delete_static_directory'=>1,'admin_qb_id' => $group_admin);
								}
							}
							else
							{
								if($etagChanged['code'] ==304){
									$responseData = array('method_name'=> 'getUserDirectoryData','status'=>'0','response_code'=>'304', 'admin_qb_id' => $group_admin, 'message'=> SUCCESS_304, "Etag"=> $eTagData);
								}
								else
								{
									//** Get Deleted users[START]
									$userListsArr = array();
									$getDeletedUserData=$this->CacheLastModifiedUser->find("all", array("conditions"=> array("company_id"=>$userCurrentCompany['UserEmployment']['company_id'], "type"=>"deleted_user","last_modified > "=> "'".$eTagDate."'", "status"=>1),'group'=>array('CacheLastModifiedUser.user_id'), 'order'=>array('last_modified DESC')));
									//** Get Deleted users[END]
									$cacObjEtag = $this->Cache->redisConn();
									// print_r($cacObj);
									// exit();
									if(($cacObjEtag['connection']) && empty($cacObjEtag['errors'])){
										// echo "<pre>";print_r("TEST12");exit();

										if($userCurrentCompany['UserEmployment']['company_id'] == $dataInput['institution_id'])
										{
											if($cacObjEtag['robj']->exists('institutionDirectory:'.$userCurrentCompany['UserEmployment']['company_id'])){

											
												$dataJson = $cacObjEtag['robj']->get('institutionDirectory:'.$userCurrentCompany['UserEmployment']['company_id']);
												$userData = json_decode($dataJson, true);
											}
											else
											{
												// echo "<pre>";print_r("TEST13");exit();
												$userLists = array();
												if($userCurrentCompany['UserEmployment']['company_id'] != -1){ //** Don't show if user selected company None(i.e.-1)
													//$userLists = $this->User->oncallUserListsLight( $params, $loggedinUserId );
													$userLists = $this->User->getUserDirectoryDataLiteOne( $params, $loggedinUserId );
												}
												$userData['User'] = $this->directoryStaticDataFormatLiteOne($userLists, $loggedinUserId,$userCurrentCompany['UserEmployment']['company_id'],$companySubscriptionCheck);
												$userData['DEtag'] = $dynamicETagData;
												if(count($getDeletedUserData) > 0)
												{
													foreach ($getDeletedUserData as $modifiedUser) {
														$userListsArr[] = array("user_id"=> $modifiedUser['CacheLastModifiedUser']['user_id']);
													}
												}
												$userData['Deleted_user'] = $userListsArr;
												$key = 'institutionDirectory:'.$userCurrentCompany['UserEmployment']['company_id'];
												$cacObjEtag['robj']->set($key,json_encode($userData, true));
											}
											$responseData = array('method_name'=> 'getUserDirectoryData','status'=>'1','response_code'=>'200', 'message'=> ERROR_200,"Etag"=> $eTagData, "EtagCeck"=> "Four", "EtagCeckData"=>$eTagDataCheck, 'data'=> $userData,'admin_qb_id' => $group_admin, 'Company'=>$userEmploymentData);
										}
										else
										{
											if($cacObjEtag['robj']->exists('institutionDirectory:'.$userCurrentCompany['UserEmployment']['company_id'])){
											//echo "<pre>";print_r("TEST12");exit();
												$dataJson = $cacObjEtag['robj']->get('institutionDirectory:'.$userCurrentCompany['UserEmployment']['company_id']);
												$userData = json_decode($dataJson, true);
												$checkData = $dataJson;
											}
											else
											{
												$userLists = array();
												if($userCurrentCompany['UserEmployment']['company_id'] != -1){ //** Don't show if user selected company None(i.e.-1)
													//$userLists = $this->User->oncallUserListsLight( $params, $loggedinUserId );
													$userLists = $this->User->getUserDirectoryDataLiteOne( $params, $loggedinUserId );
												}
												$userData['User'] = $this->directoryStaticDataFormatLiteOne($userLists, $loggedinUserId,$userCurrentCompany['UserEmployment']['company_id'],$companySubscriptionCheck);
												$userData['DEtag'] = $dynamicETagData;
												if(count($getDeletedUserData) > 0)
												{
													foreach ($getDeletedUserData as $modifiedUser) {
														$userListsArr[] = array("user_id"=> $modifiedUser['CacheLastModifiedUser']['user_id']);
													}
												}
												$userData['Deleted_user'] = $userListsArr;
												$key = 'institutionDirectory:'.$userCurrentCompany['UserEmployment']['company_id'];
												$cacObjEtag['robj']->set($key,json_encode($userData, true));
											}
											$responseData = array('method_name'=> 'getUserDirectoryData','status'=>'1','response_code'=>'200', 'message'=> ERROR_200,"Etag"=> $eTagData, 'data'=> $userData,'admin_qb_id' => $group_admin, 'Company'=>$userEmploymentData, 'delete_static_directory'=>1);

										}
									}
									else{
										echo "No Connection";exit();
									}
								}
							}
						}else{
							$responseData = array('method_name'=> 'getUserDirectoryData','status'=>'0','response_code'=>'658', 'message'=> ERROR_658);
						}

					// }else{
					// 	$responseData = array('method_name'=> 'getUserDirectoryData','status'=>'0','response_code'=>'602', 'message'=> ERROR_602);
					// }
				}else{
					$isCurrentUser = 1;
					$statusResponse = $this->getUserProfileStatuscheck($dataInput['user_id'],$isCurrentUser);
					$responseData = array('method_name'=> 'getUserDirectoryData','status'=>'0','response_code'=>$statusResponse['response_code'], 'message'=> $statusResponse['message']);
				}
			}else{
				$responseData = array('method_name'=> 'getUserDirectoryData','status'=>'0','response_code'=>'618', 'message'=> ERROR_618);
			}
		}else{
		$responseData = array('method_name'=> 'getUserDirectoryData','status'=>'0','response_code'=>'611','message'=> ERROR_611);
		}
		echo json_encode($responseData);
		exit;
	}


	/*
	--------------------------------------------------------------------------
	On: 07-25-2019
	I/P: JSON
	O/P: JSON, user lists
	Desc: Get dynamic directory data
	--------------------------------------------------------------------------
	*/
	public function getUserDynamicDirectoryData(){
		$startMilliseconds = round(microtime(true) * 1000);
		if($this->request->is('post')){
		$dataInput = $this->request->input('json_decode', true) ;
			if($this->User->findById( $dataInput['user_id'] ) )
			{
			$loggedinUserId = isset($dataInput['user_id']) ? $dataInput['user_id'] : '';
			$newEtag = '';
			$userDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $loggedinUserId)));
			if($userDetails['User']['status']==1 && $userDetails['User']['approved']==1)
			{
				// if( $this->validateToken() && $this->validateAccessKey() ){
					$userCurrentCompany = $this->UserEmployment->find("first", array("conditions"=> array("user_id"=> $loggedinUserId, "is_current"=> 1), "order"=> array("id DESC")));
					$params['user_current_company_id'] = $userCurrentCompany['UserEmployment']['company_id'];
					$enterpriseUserCheck = $this->EnterpriseUserList->find("count", array("conditions"=> array("email"=> stripslashes($userDetails['User']['email']),"company_id"=> $userCurrentCompany['UserEmployment']['company_id'],"status"=> 1)));
					$companySubscriptionCheck=$this->CompanyName->find("count", array("conditions"=> array("id"=>$userCurrentCompany['UserEmployment']['company_id'], "status"=>1,"is_subscribed"=>1)));

					$etagChangeParams['key'] = 'dynamicDirectoryEtag:'.$userCurrentCompany['UserEmployment']['company_id'];
					$etagChanged = $this->checkEtagNotModifiedDynamicDirectory($etagChangeParams);
					if($etagChanged['code'] !=304){
						$cacObjEtag = $this->Cache->redisConn();
						if(($cacObjEtag['connection']) && empty($cacObjEtag['errors'])){
							//if($cacObjEtag['robj']->exists($etagChangeParams['key'])){
								$etagLastModified = strtotime(date('Y-m-d H:i:s'));
								$eTagNewVal = md5($etagChangeParams['key'] . rand(1,10000) . $etagLastModified);
								$cacObjEtag['robj']->hset($etagChangeParams['key'],'eTag' , $eTagNewVal);
								$cacObjEtag['robj']->hset($etagChangeParams['key'],'lastmodified' , $etagLastModified);
							//}
						}
						$eTagData = $eTagNewVal.'~'.$etagLastModified;
						$previousDateModified = explode("~",$etagChanged['etagHeader']);
						$eTagDate = date('Y-m-d H:i:s', end($previousDateModified));
					}else{
						$eTagData = $etagChanged['etagHeader'];
						if(isset($etagChanged['etagHeader']) && empty($etagChanged['etagHeader'])){
							$eTagDate = '0000-00-00 00:00:00';
						}else{
							$previousDateModified = explode("~",$etagChanged['etagHeader']);
							$eTagDate = date('Y-m-d H:i:s', end($previousDateModified));
						}
					}
					$userListsArr = array();
					$getDeletedUserData=$this->CacheLastModifiedUser->find("all", array("conditions"=> array("company_id"=>$userCurrentCompany['UserEmployment']['company_id'], "type"=>"deleted_user","last_modified > "=> $eTagDate, "status"=>1),'group'=>array('CacheLastModifiedUser.user_id'), 'order'=>array('last_modified DESC')));
					$getModifiedUser=$this->CacheLastModifiedUser->find("all", array("conditions"=> array("company_id"=>$userCurrentCompany['UserEmployment']['company_id'], "type"=>"dynamic_directory_changed","last_modified > "=> $eTagDate), 'group'=>array('CacheLastModifiedUser.user_id'),'order'=>array('last_modified DESC')));
					if($companySubscriptionCheck ==1 && $enterpriseUserCheck ==0){
						$userData['diretory_access'] = 655;
						$userData['message'] = ERROR_655;
						$responseData = array('method_name'=> 'getUserDynamicDirectoryData','status'=>'1','response_code'=>'200', 'message'=> ERROR_200, 'data'=> $userData);
					}else{
						if(isset($etagChanged['etagHeader']) && empty($etagChanged['etagHeader'])){
						$userLists = array();
						$newEtag = $eTagDate;
						$responseData = array('method_name'=> 'getUserDynamicDirectoryData','status'=>'1','response_code'=>'200', 'message'=> ERROR_200, 'data'=> $userData, "Etag"=> $eTagData);
					}else{
						if(!empty($getModifiedUser))
						{
							foreach ($getModifiedUser as $modifiedUser) {
								$params['user_id'] = $modifiedUser['CacheLastModifiedUser']['user_id'];
								$params['user_current_company_id'] = $userCurrentCompany['UserEmployment']['company_id'];
								$getUserData = $this->User->getChangedUserDataLite($params);
								if(!empty($getUserData))
								{
									$userRoleTags = array();
									$batonRoleArr = array();
									if(!empty($getUserData[0]['UserProfile']['custom_role']))
									{
										$userRoleTags = json_decode($getUserData[0]['UserProfile']['custom_role'], true);
									}
									else
									{
										$userRoleTags = array();
									}
									if(!empty($getUserData[0]['UserProfile']['custom_baton_roles']))
									{
										$batonRoleArr = json_decode($getUserData[0]['UserProfile']['custom_baton_roles'], true);
									}
									else
									{
										$batonRoleArr = array();
									}
									$professionArr = array();
									$professionArr = array("profession_type"=>$getUserData[0]['Profession']['profession_type']);
									$phone = array(); $isConactFound = 0;
									if(!empty($getUserData[0]['UserProfile']['contact_no'])){
										$phone = array("mobile_no"=>!empty($getUserData[0]['UserProfile']['contact_no']) ? $getUserData[0]['UserProfile']['contact_no']:"");
										$isConactFound = 1;
									}
									if(!empty($ud['UserDndStatuse']['dnd_status_id']))
									{
										$dndarr = array('dnd_status_id'=>$getUserData[0]['UserDndStatuse']['dnd_status_id'], 'dnd_name'=>$getUserData[0]['DndStatusOption']['dnd_name'],
										'icon_selected'=>$getUserData[0]['DndStatusOption']['icon_selected'], 'end_time'=>$getUserData[0]['UserDndStatuse']['end_time']);
									}
									else
									{
										$dndarr = array('dnd_status_id'=>"0");
									}
									$imgPath = "";
									$imgPath = !empty($getUserData[0]['UserProfile']['profile_img']) ? AMAZON_PATH . $getUserData[0]['User']['id']. '/profile/' . $getUserData[0]['UserProfile']['profile_img']:"";
									$thumbnailimgPath = "";
									$thumbnailimgPath = !empty($getUserData[0]['UserProfile']['thumbnail_img']) ? PROFILE_PICTURE_THUMBNAIL_IMAGE_PATH . $getUserData[0]['User']['id']. '/profile/' . $getUserData[0]['UserProfile']['thumbnail_img']:"";

									$qbInfo = array();
									$qbId = !empty($getUserData[0]['UserQbDetail']['qb_id']) ? $getUserData[0]['UserQbDetail']['qb_id'] : 0;
									$qbInfo = array("id"=> $qbId);
									$userListsArr[] = array(
									 	'user_id'=> isset($getUserData[0]['User']['id']) ? $getUserData[0]['User']['id'] : "0",
										'email'=>	isset($getUserData[0]['User']['email']) ? $getUserData[0]['User']['email'] : "0",
										'first_name'=> isset($getUserData[0]['UserProfile']['first_name']) ? $getUserData[0]['UserProfile']['first_name'] : "",
										'last_name'=> isset($getUserData[0]['UserProfile']['last_name']) ? $getUserData[0]['UserProfile']['last_name'] : "",
										'profile_img'=> $imgPath,
										'thumbnail_img'=> $thumbnailimgPath,
										'country_code'=> isset($getUserData[0]['Country']['country_code']) ? $getUserData[0]['Country']['country_code'] : "",
										'profession'=> !empty($professionArr) ? $professionArr : (object) array() ,
										'phone'=> !empty($phone) ? $phone : array("mobile_no"=>""),
										'qb_details'=> !empty($qbInfo) ? $qbInfo : (object) array(),
										'on_duty'=> isset($getUserData[0]['UserDutyLog']['status']) ? $getUserData[0]['UserDutyLog']['status'] : "0",
										'at_work'=>isset($getUserData[0]['UserDutyLog']['atwork_status']) ? $getUserData[0]['UserDutyLog']['atwork_status'] : "0",
										'role_status'=>isset($getUserData[0]['UserProfile']['role_status']) ? $getUserData[0]['UserProfile']['role_status'] : '',
										'is_dnd_active'=> isset($getUserData[0]['UserDndStatuse']['is_active']) ? (string)$getUserData[0]['UserDndStatuse']['is_active'] : "0",
										'dnd_status'=> $dndarr,
										'role_tags'=>$dndarr,
										'baton_roles'=>$batonRoleArr

										);
								}
							}
							$userDataArr['User'] = $userListsArr;
							if(count($getDeletedUserData) > 0)
							{
								foreach ($getDeletedUserData as $modifiedUser) {
									$dUserListsArr[] = array("user_id"=> $modifiedUser['CacheLastModifiedUser']['user_id']);
								}
							}
							else
							{
								$dUserListsArr = array();
							}
							$userDataArr['Deleted_user'] = $dUserListsArr;
							$responseData = array('method_name'=> 'getUserDynamicDirectoryData','status'=>'1','response_code'=>'200', 'message'=> ERROR_200, 'data'=> $userDataArr, "Etag"=> $eTagData);
						}
						else
						{
							$responseData = array('method_name'=> 'getUserDynamicDirectoryData','status'=>'0','response_code'=>'304', 'message'=> SUCCESS_304, "Etag"=> $eTagData);
						}
					}
				}
				// }else{
	   //      	$responseData = array('method_name'=> 'getUserDynamicDirectoryData','status'=>'0','response_code'=>'602', 'message'=> ERROR_602);
    //      		}
			}
			else
			{
				$isCurrentUser = 1;
				$statusResponse = $this->getUserProfileStatuscheck($dataInput['user_id'],$isCurrentUser);
				$responseData = array('method_name'=> 'getUserDynamicDirectoryData','status'=>'0','response_code'=>$statusResponse['response_code'], 'message'=> $statusResponse['message']);
			}
			}
			else
			{
				$responseData = array('method_name'=> 'getUserDynamicDirectoryData','status'=>'0','response_code'=>'618', 'message'=> ERROR_618);
			}
		}else{
				$responseData = array('method_name'=> 'getUserDynamicDirectoryData','status'=>'0','response_code'=>'611','message'=> ERROR_611);
		}
		echo json_encode($responseData);
		//** Write API response time on log file[START]
		$endMilliseconds = round(microtime(true) * 1000);
		$this->Common->writeLog($dataInput['user_id'],$startMilliseconds,$endMilliseconds,$userCurrentCompany['UserEmployment']['company_id']);
		//** Write API response time on log file[END]
		exit;
	}
}// End Class
