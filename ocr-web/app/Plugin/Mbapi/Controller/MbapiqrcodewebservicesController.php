<?php
/*
Desc: API data get/post to Webservices.
*/

//App::uses('AppController', 'Controller');

class MbapiqrcodewebservicesController extends AppController {

	public $uses = array('Mbapi.User','Mbapi.UserProfile', 'Mbapi.GmcUkUser','Mbapi.Specilities','Mbapi.Profession', 'Mbapi.Country', 'Mbapi.UserSpecilities', 'Mbapi.UserInterest', 'Mbapi.UserFollow', 'Mbapi.EmailSmsLog', 'Mbapi.PreQualifiedDomain', 'Mbapi.UserColleague', 'Mbapi.UserNotificationSetting', 'EmailTemplate', 'Mbapi.InstituteName', 'Mbapi.CompanyName', 'Mbapi.EducationDegree', 'Mbapi.Designation', 'Mbapi.UserInstitution', 'Mbapi.UserEmployment', 'Mbapi.NotificationUser', 'Mbapi.InstitutionInformation', 'Mbapi.MedicBleepAppUrl', 'Mbapi.ActivityLog', 'UserQbDetail', 'Mbapi.TempRegistration', 'NpiUsaUser','Mbapi.UserVisibilitySetting', 'Mbapi.UserDutyLog','Mbapi.CipOnOff', 'Mbapi.ForceLogoutSetting', 'Mbapi.ForgotPasswordLinkExpire', 'Mbapi.UserOneTimeToken', 'Mbapi.EnterpriseUserList', 'Mbapi.UserSubscriptionLog', 'Mbapi.ApiRequestResponseTrack', "Mbapi.PaidSubscriptionDomain", "Mbapi.AdminActivityLog", "Mbapi.CompanyGeofenceParameter","Mbapi.RoleTag","Mbapi.UserRoleTag","Mbapi.TourGuideCompletionVisit","Mbapi.OncallAccessProfessions","Mbapi.UserDevice","Mbapi.CacheLastModifiedUser","Mbapi.AvailableAndOncallTransaction","Mbapi.AppDeviceKey","Mbapi.AppServerKey", "Mbapi.LoginAttemptTransaction", "Mbapi.CompanyBranchName", "Mbapi.QrCodeDetail");
	public $components = array('Common', 'Image', 'Mbapi.MbEmail','Quickblox','Cache');

	/*
	-------------------------------------------------------------------------------------
	On: 16-08-18
	I/P: 
	O/P: 
	Desc: 
	-------------------------------------------------------------------------------------
	*/


	public function institutionDetailByQrCode()
	{
		$this->autoRender = false;
		$responseData = array();
		$text = '';
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true);
			$userInfo = $this->UserProfile->find("first", array("conditions"=> array("UserProfile.user_id"=> $dataInput['user_id'])));
			// if($userInfo['User']['status']==1 && $userInfo['User']['approved']==1){
				// if( $this->validateToken() && $this->validateAccessKey() ){
					try{
						$qrCodeDetail = $this->QrCodeDetail->find("first", array("conditions"=> array("batch_number"=> $dataInput['batch_number'], "qr_code_number"=> $dataInput['qr_code_number'], "institute_id"=>$dataInput['institute_id'])));
						if($qrCodeDetail['QrCodeDetail']['is_valid'] == 1)
						{
							if($qrCodeDetail['QrCodeDetail']['is_used'] == 1)
							{
								$companyDetail = $this->CompanyName->find("first", array("conditions"=> array("id"=> $dataInput['institute_id'])));
								if(! empty($qrCodeDetail)){
									$userName = $userInfo['UserProfile']['first_name']." ".$userInfo['UserProfile']['last_name'];
									$validityValue = $qrCodeDetail['QrCodeDetail']['validity'];
									if($validityValue == 1)
									{
										$text = "1 Day";
									}
									else if($validityValue == 2)
									{
										$text = $validityValue." Days";
									}
									else if($validityValue == 7)
									{
										$text = "1 Week";
									}
									else if($validityValue == 14)
									{
										$text = "2 Weeks";
									}
									else if($validityValue == 21)
									{
										$text = "3 Weeks";
									}
									else if($validityValue == 30)
									{
										$text = "1 Month";
									}
									else if($validityValue == 60)
									{
										$text = "2 Months";
									}
									else if($validityValue == 90)
									{
										$text = "3 Months";
									}
									else if($validityValue == 180)
									{
										$text = "6 Months";
									}
									else if($validityValue == 365)
									{
										$text = "1 Year";
									}
									$qrCodeData = array("user_name"=> $userName, "subscription_period"=> $text, "company_id"=>$companyDetail['CompanyName']['id'] ,"company_logo"=>$companyDetail['CompanyName']['company_image'] , "company_name"=>$companyDetail['CompanyName']['company_name'],"qr_code_number"=>$qrCodeDetail['QrCodeDetail']['qr_code_number']);

									$responseData = array('method_name'=> 'institutionDetailByQrCode', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> $qrCodeData);
								}else{
									$responseData = array('method_name'=> 'institutionDetailByQrCode', 'status'=>"0", 'response_code'=> "662", 'message'=> ERROR_663);
								}
							}
							else{
								$responseData = array('method_name'=> 'institutionDetailByQrCode', 'status'=>"0", 'response_code'=> "663", 'message'=>ERROR_663);
							}
						}
						else{
							$responseData = array('method_name'=> 'institutionDetailByQrCode', 'status'=>"0", 'response_code'=> "664", 'message'=> ERROR_664);
						}
					}catch(Exception $e){	
						$response = array('method_name'=> 'institutionDetailByQrCode', 'status'=>"0", 'response_code'=> '615', 'message'=> ERROR_615);
					}
				// }else{
				// 	$responseData = array('method_name'=> 'institutionDetailByQrCode', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
				// }
			// }else{
			// 	$statusResponse = $this->getUserStatus($dataInput['user_id'],$dataInput['user_id']);
			// 	$responseData = array('method_name'=> 'institutionDetailByQrCode','status'=>'0','response_code'=>$statusResponse['response_code'], 'message'=> $statusResponse['message']);
			// }
		}else{
			$responseData = array('method_name'=> 'institutionDetailByQrCode', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		echo json_encode($responseData);
    	exit;
	}
	

}// End Class
