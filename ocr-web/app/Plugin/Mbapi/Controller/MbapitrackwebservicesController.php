<?php
/*
Desc: Track controller .
*/

//App::uses('AppController', 'Controller');

class MbapitrackwebservicesController extends AppController {

	public $uses = array("Mbapi.UserDeviceInfo",'Mbapi.UserLoginTransaction','Mbapi.UserIpSetting','Mbapi.SessionTimeIp','Mbapi.SessionTimeIpRange');
	public $components = array('Common');

	/*
	-------------------------------------------------------------------------------------------------------
	On: 13-09-2017
	I/P: JSON
	O/P: JSOn
	Desc: Get group country list for any user
	-------------------------------------------------------------------------------------------------------
	*/
	public function setDeviceInfo(){
		$responseData = array();
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
				if( $this->validateToken() ){
					//** Store Track record[START]
					try{
						$deviceData = array(
											"user_id"=> $dataInput['device_info']['user_id'],
											"device_info"=> json_encode($dataInput['device_info']),
											"local_server_info"=> $this->Common->getFrontServerInfo()
											);
						$addData = $this->UserDeviceInfo->save($deviceData );
						$responseData = array('method_name'=> 'setDeviceInfo', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200);
						// try {
							$header = getallheaders();
							$token = $header['token'];

							$this->UserLoginTransaction->updateAll(array("UserLoginTransaction.device_info"=> "'".json_encode($dataInput['device_info'])."'"), array('UserLoginTransaction.token'=>$token,'UserLoginTransaction.login_status'=>'1','UserLoginTransaction.user_id'=>$dataInput['device_info']['user_id']));
						// } catch (Exception $e) {
						// 	$responseData = array('method_name'=> 'setDeviceInfo', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, "message"=> $e->getMessage());
						// }
					}catch(Exception $e){
						$responseData = array('method_name'=> 'setDeviceInfo', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, "message"=> $e->getMessage());
					}
					//** Store Track record[END]
			    }else{
			         $responseData = array('method_name'=> 'setDeviceInfo','status'=>'0','response_code'=>'602', 'message'=> ERROR_602);
		         }
	       }else{
			$responseData = array('method_name'=> 'setDeviceInfo','status'=>'0','response_code'=>'611','message'=> ERROR_611);
	    }
		echo json_encode($responseData);
    	exit;
	}

	/*
	-------------------------------------------------------------------------------------------------------
	On: 13-12-2018
	I/P: JSON
	O/P: JSOn
	Desc: Get Local ip address user wise
	-------------------------------------------------------------------------------------------------------
	*/

	public function getDeviceInfo(){
		$responseData = array();
		if($this->request->is('post')){
			$dataInput = $this->request->input('json_decode', true) ;
				if( $this->validateToken() ){
					$dataInput = $this->request->input ( 'json_decode', true);
					if( $this->User->find( "count", array("conditions"=> array("User.id"=>$dataInput['user_id']))) > 0 ){
						try {
							// $device_info = $this->UserIpSetting->find('all',array('conditions'=>array('user_id'=>$dataInput['user_id'],'company_id'=>$dataInput['institution_id'])));

							$userIp = $dataInput['local_ip'];
							$ip_time = 10;
							$companyId = $dataInput['institution_id'];
		          $normal_ips = $this->SessionTimeIp->find('all',array('conditions'=>array('SessionTimeIp.company_id'=> $companyId)));
		          $ip_range = $this->SessionTimeIpRange->find('all',array('conditions'=>array('SessionTimeIpRange.company_id'=> $companyId)));

							if(!empty($normal_ips)){
								foreach ($normal_ips as $value) {
									if((string)$userIp == (string)$value['SessionTimeIp']['ip']){
										$ip_time = $value['SessionTimeIp']['time_span'];
									}
								}
							}

		          if(!empty($ip_range)){
		            foreach ($ip_range as $value) {
									$from_ip = explode(".",$value['SessionTimeIpRange']['ip_from']);
									$to_ip = explode(".",$value['SessionTimeIpRange']['ip_to']);
									$user_ip = explode(".",$userIp);
									if($user_ip[0] == $from_ip[0] && $user_ip[1] == $from_ip[1] && $user_ip[2] == $from_ip[2]){
										if(($user_ip[3] <= $from_ip[3] && $user_ip[3] >= $to_ip[3]) || ($user_ip[3] <= $to_ip[3] && $user_ip[3] >= $from_ip[3])){
											$ip_time = $value['SessionTimeIpRange']['time_span'];
										}
									}
		            }
		          }

							$ip_data = array(
								'time_span' => $ip_time
							);
							$responseData = array('method_name'=> 'getDeviceInfo', 'status'=>"1", 'data'=> $ip_data, 'response_code'=> "200", 'message'=> ERROR_200);
						} catch (Exception $e) {
							$responseData = array('method_name'=> 'getDeviceInfo', 'status'=>"0", 'response_code'=> "615", 'message'=> ERROR_615, "message"=> $e->getMessage());
						}
					}else{
						$responseData = array('method_name'=> 'getDeviceInfo', 'status'=>"0", 'response_code'=> "618", 'message'=> ERROR_618);
					}
			    }else{
			        $responseData = array('method_name'=> 'getDeviceInfo','status'=>'0','response_code'=>'602', 'message'=> ERROR_602);
		        }
	       }else{
			$responseData = array('method_name'=> 'getDeviceInfo','status'=>'0','response_code'=>'611','message'=> ERROR_611);
	    }
		echo json_encode($responseData);
    	exit;
	}
}
