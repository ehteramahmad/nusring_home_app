<?php
/*
Desc: API data get/post to Webservices.
*/

class MbapiprofessionsController extends AppController {

	public $uses = array('Mbapi.Profession','Mbapi.Country');
	public $components = array('Common');

	/*
	-------------------------------------------------------------------------------------
	On: 25-09-2017
	I/P: country_id
	O/P: All Professions
	-------------------------------------------------------------------------------------
	*/
	public function getAllProfessions(){
		$this->autoRender = false;
			$response = array();
			if($this->validateAccessKey()){
			$dataInput = $this->request->input ( 'json_decode', true) ; 	
				try{	
						$params = array();
						$countryData = $data = $this->Country->find("first", array("conditions"=> array("country_code"=> $dataInput['country_code'])));
						$data = $this->Profession->find("all", array("conditions"=> array("country_id"=> $countryData['Country']['id'], "status"=>1),"order"=> array("profession_type")));
						if(!empty($data)){
							foreach($data as $dt){
								$specData[] = array('id'=> $dt['Profession']['id'], 'profession_type'=> $dt['Profession']['profession_type']);
							}
							$response = array('method_name'=> 'getAllProfessions', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, 'data'=> array('Profession'=> $specData));
						}else{
							$response = array('method_name'=> 'getAllProfessions', 'status'=>"0", 'response_code'=> "613", 'message'=> ERROR_613);
						}
					}catch(Exception $e){	
						$response = array('method_name'=> 'getAllProfessions', 'status'=>"0", 'response_code'=> '615', 'message'=> ERROR_615);
					}
			}else{
				$response = array('method_name'=> 'getAllProfessions', 'status'=>"0", 'response_code'=> '602', 'message'=> ERROR_602);	
			}
			echo json_encode($response);
			exit;
	}
}// End Class
