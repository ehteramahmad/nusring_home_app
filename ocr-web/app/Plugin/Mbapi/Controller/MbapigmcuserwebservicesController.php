<?php
/*
Desc: Data Push/Pull related to GMC user.
*/

//App::uses('AppController', 'Controller');

class MbapigmcuserwebservicesController extends AppController {

	public $uses = array('GmcUkUser','User', 'NpiUsaUser', 'Mbapi.PreRoleBasedDomain');
	public $components = array('Common','Mbapi.MbCommon');
	/*
	On: 12-05-2016
	I/P: $params = array()
	O/P: GMC users details
	Desc: Fetch all Gmc Users details according to conditions.
	*/
	public function gmcUsersSearch(){

		$resposneGmcUser = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$params['first_name'] = isset($dataInput['first_name'])?$dataInput['first_name']:'';
			$params['last_name'] = isset($dataInput['last_name'])?$dataInput['last_name']:'';
			$params['size'] = isset($dataInput['size'])?$dataInput['size']:'';
			$params['page_number'] = isset($dataInput['page_number']) ? $dataInput['page_number'] : 1;
			$params['country'] = isset($dataInput['country'])?$dataInput['country']:'';

			$skipGmc = 0;
			$domainNameArr = explode("@", $dataInput['email']);//** Extract Domain name from email
			$domainName = end($domainNameArr);	//** Domain Name
			$rootDomain = $this->MbCommon->getTld($domainName);
			//$allowedDomains = array('nhs.net', 'nhs.uk', 'nhs.co.uk','wsh.nhs.uk','wmas.nhs.uk');
			$allowedDomains = $this->PreRoleBasedDomain->find("count", array("conditions"=> array("domain_name"=> $rootDomain, "status"=> 1)));
			if(in_array($dataInput['profession_id'], array(9,10,11))){
			//** Values for profession Administration,Management [START] 
				if(in_array($dataInput['profession_id'], array(9,10,11)) && $allowedDomains > 0){
						$skipGmc = 1;
						$gmcUsers = array();
						$resposneGmcUser = array('method_name'=> 'gmcUsersSearch', 'status'=>"1", 'response_code'=> "200", 'message'=>'OK', 'data'=> array("skip_gmc"=> $skipGmc,'GmcUser'=> $gmcUsers));
				}else{
					$resposneGmcUser = array('method_name'=> 'gmcUsersSearch', 'status'=>"0", 'response_code'=> "649", 'message'=> ERROR_649);
				}
			//** Values for profession Administration,Management [END] 
			}elseif(in_array($dataInput['profession_id'], array(2,3))){
			//** Values for profession Pharmacist,Nurse [START] 
				if(in_array($dataInput['profession_id'], array(2,3)) && $allowedDomains > 0){
						$skipGmc = 1;
						$gmcUsers = array();
						$resposneGmcUser = array('method_name'=> 'gmcUsersSearch', 'status'=>"1", 'response_code'=> "200", 'message'=>'OK', 'data'=> array("skip_gmc"=> $skipGmc, 'GmcUser'=> $gmcUsers));
				}else{
					$gmcUsers = array();
					$resposneGmcUser = array('method_name'=> 'gmcUsersSearch', 'status'=>"1", 'response_code'=> "200", 'message'=>'OK', 'data'=> array("skip_gmc"=> $skipGmc,'GmcUser'=> $gmcUsers));
				}
			//** Values for profession Administration,Management [END] 
			}elseif(in_array($dataInput['profession_id'], array(8))){
					$data = array();
					if(in_array(strtolower($params['country']), array('united kingdom','uk','gb'))){ //** If user is from uk check uk GMC
						if(in_array($dataInput['profession_id'], array(8))){ //** If profession is Doctor
							$data = $this->GmcUkUser->gmcUserSearch( $params );
						}
					}
					elseif(in_array(strtolower($params['country']), array('United States','us','US'))){
						if(in_array($dataInput['profession_id'], array(8))){ //** If profession is Doctor
							$this->NpiUsaUser->useDbConfig = 'gmc';
							$data = $this->NpiUsaUser->npiUserSearch( $params );
							//echo "<pre>"; print_r($data);die;
						}
					}
					if(!empty($data)){
						foreach($data as $gmcData){
							$gmcUsers[] = array(
											'id'=> $gmcData['GmcUkUser']['id'], 
											'GMCRefNo'=> $gmcData['GmcUkUser']['GMCRefNo'],
											'Surname'=> $gmcData['GmcUkUser']['Surname'],
											'GivenName'=> $gmcData['GmcUkUser']['GivenName'],
											'Gender'=> $gmcData['GmcUkUser']['Gender'],
											'YearOfQualification'=> $gmcData['GmcUkUser']['YearOfQualification'],
											'PlaceofQualificationCountry'=> $gmcData['GmcUkUser']['PlaceofQualificationCountry'],
											);
							}
							$resposneGmcUser = array('method_name'=> 'gmcUsersSearch', 'status'=>"1", 'response_code'=> "200", 'message'=>'OK', 'data'=> array("skip_gmc"=> $skipGmc,'GmcUser'=> $gmcUsers));
						}else{
							$gmcUsers = array();
							$resposneGmcUser = array('method_name'=> 'gmcUsersSearch', 'status'=>"1", 'response_code'=> "200", 'message'=>'OK', 'data'=> array("skip_gmc"=> $skipGmc,'GmcUser'=> $gmcUsers));
						}
			}else{
					$skipGmc = 0;
					$gmcUsers = array();
					$resposneGmcUser = array('method_name'=> 'gmcUsersSearch', 'status'=>"1", 'response_code'=> "200", 'message'=>'OK', 'data'=> array("skip_gmc"=> $skipGmc,'GmcUser'=> $gmcUsers));
			}	
			
		}else{
			$resposneGmcUser = array('method_name'=> 'gmcUsersSearch', 'status'=>"0", 'response_code'=> "601", 'message'=>'Information not provided by app');
		}
		echo json_encode($resposneGmcUser);
		exit;
	}
}
