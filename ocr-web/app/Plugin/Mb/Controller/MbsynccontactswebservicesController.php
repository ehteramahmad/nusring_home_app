

<?php
/*
Desc: Data Push/Pull related to Sync users.
*/

//App::uses('AppController', 'Controller');

class MbsynccontactswebservicesController extends AppController {
	public $uses = array("Mb.SyncContact", "Mb.SyncContactsLastUpdatedtimestamp", "Mb.ApiRequestResponseTrack");
	//public $components = array();

	/*
	-----------------------------------------------------------------------------------------------
	On: 19-05-2016
	I/P: 
	O/P:
	Desc: Fetches last updated sync contacts by any user.
	-----------------------------------------------------------------------------------------------
	*/
	public function getSyncLastUpdatedTimestamp($userId = NULL){
		$this->autoRender = false;
		$responseData = array();
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true);
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				//** Get contact sync updated time stamp
				$conditions = array("user_id"=> $dataInput['user_id'], "device_id"=> $dataInput['device_id'] );
				$lastSyncTimeData = $this->SyncContactsLastUpdatedtimestamp->find("first", array("conditions"=> $conditions));
				if(!empty($lastSyncTimeData)){
					$syncContactsLastUpdatedData['last_updated_sync_timstamp'] = array("contactLastUpdatedTimestamp_device"=> $lastSyncTimeData['SyncContactsLastUpdatedtimestamp']['last_updated_timstamp_device'], "contactLastUpdatedTimestamp_server"=> strtotime($lastSyncTimeData['SyncContactsLastUpdatedtimestamp']['last_updated_timstamp_server']));
					$responseData = array('method_name'=> 'getSyncLastUpdatedTimestamp', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> $syncContactsLastUpdatedData );
				}else{
					$syncContactsLastUpdatedData['last_updated_sync_timstamp'] = array("contactLastUpdatedTimestamp_device"=> 0, "contactLastUpdatedTimestamp_server"=> 0);
					$responseData = array('method_name'=> 'getSyncLastUpdatedTimestamp', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200, "data"=> $syncContactsLastUpdatedData );
				}
				
			}else{
				$responseData = array('method_name'=> 'getSyncLastUpdatedTimestamp', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'getSyncLastUpdatedTimestamp', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}

	/*
	-----------------------------------------------------------------------------------------------
	On: 20-05-2016
	I/P: 
	O/P:
	Desc: Add contacts by any user's device using sync data
	-----------------------------------------------------------------------------------------------
	*/
	public function syncContacts(){
		$this->autoRender = false;
		$responseData = array();
		$startTime = strtotime(date('Y-m-d H:i:s'));//** Used to track API request response
		if($this->request->is('post')) {
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true);
			if( $this->tokenValidate() && $this->accesskeyCheck() ){
				//** Sync data with phone number
				foreach($dataInput['contacts'] as $contacts){
				if(!empty($contacts['phoneNumber'])){
					foreach($contacts['phoneNumber'] as $phoneNumber){
						if(!empty($phoneNumber)){
							$checkConditions = array(); $syncData = array();
							$syncData['user_id'] = $dataInput['loggedin_user_id'];
							$checkConditions = array("user_id"=> $dataInput['loggedin_user_id'], "mobile_no"=> $phoneNumber);
							if($this->SyncContact->find("count", array("conditions"=> $checkConditions)) == 0 ){
								$syncData['name'] = $contacts['name'];
								$syncData['mobile_no'] = $phoneNumber;
								//$syncData['email'] = $contacts['emailAddress'];
								//** Save data
								$this->SyncContact->saveAll($syncData);
							}
						}
					}
					}
				}
				//** Sync data with Email
				foreach($dataInput['contacts'] as $contacts){
				if(!empty($contacts['emailAddress'])){
					foreach($contacts['emailAddress'] as $email){
						if(!empty($email)){
							$checkConditions = array(); $syncData = array();
							$syncData['user_id'] = $dataInput['loggedin_user_id'];
							$checkConditions = array("user_id"=> $dataInput['loggedin_user_id'], "email"=> $email);
							if($this->SyncContact->find("count", array("conditions"=> $checkConditions)) == 0 ){
								$syncData['name'] = $contacts['name'];
								//$syncData['mobile_no'] = $phoneNumber;
								$syncData['email'] = $email;
								//** Save data
								$this->SyncContact->saveAll($syncData);
							}
						}
					}
				}
				}
				//** Update last sync timstamp
					$lastUpdateConditions = array("user_id"=> $dataInput['loggedin_user_id'], "device_id"=> $dataInput['device_id']);
					if($this->SyncContactsLastUpdatedtimestamp->find("count", array("conditions"=> $lastUpdateConditions)) > 0){
						$this->SyncContactsLastUpdatedtimestamp->updateAll(array("last_updated_timstamp_device"=> $dataInput['contactLastUpdatedTimestamp']), $lastUpdateConditions);
					}else{
						$lastUpdatedTimestampData['user_id'] = $dataInput['loggedin_user_id'];
						$lastUpdatedTimestampData['device_id'] = $dataInput['device_id'];
						$lastUpdatedTimestampData['last_updated_timstamp_device'] = $dataInput['contactLastUpdatedTimestamp'];
						$this->SyncContactsLastUpdatedtimestamp->save($lastUpdatedTimestampData);	
					}
				$responseData = array('method_name'=> 'syncContacts', 'status'=>"1", 'response_code'=> "200", 'message'=> ERROR_200);
			}else{
				$responseData = array('method_name'=> 'syncContacts', 'status'=>"0", 'response_code'=> "602", 'message'=> ERROR_602);
			}
		}else{
			$responseData = array('method_name'=> 'syncContacts', 'status'=>"0", 'response_code'=> "601", 'message'=> ERROR_601);
		}
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		//** Track API Request, Response[START]
		$endTime = strtotime(date('Y-m-d H:i:s'));//** Used to track API request response
		$headerVals = $this->getHeaderValues();
		$trackData = array(
							"user_id"=> isset($dataInput['loggedin_user_id']) ? $dataInput['loggedin_user_id'] : 0,
							"request_at"=> $startTime,
							"response_at"=> $endTime,
							"request_val"=> !empty($dataInput) ? json_encode($dataInput) : 0,
							"response_val"=> json_encode($responseData),
							"api_name"=> $_SERVER['REQUEST_URI'],
							"process_time"=> ($endTime - $startTime),
							"device_type"=> $headerVals['device_type']
						);
		try{
			$this->ApiRequestResponseTrack->save($trackData);
		}catch(Exception $e){}
		//** Track API Request, Response[END]
		exit;
	}

	
}
