<?php

/**
 * Image Component
 * 
 * It contains all the common functions related image manipulation
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Netsolutions
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */


class MbImageComponent extends Component{ 
		
	var $sourceFile; // We use this file to create the thumbnail
	var $originalFilename; // We use this to get the extension of the filename
	var $destinationDirectory; // The Directory in question
	var $destinationDirectoryFilename; // The destination filename
	var $createImageFunction = '';
	var $outputImageFunction = '';
	var $failed = false;
	
	/**
	 * Method is used to thumbnail acc. to aspect ratio
	 *
	 * @param sourceFile, originalFilename, destinationDirectory, destinationDirectoryFilename
	 * 
	 * @return void
	 */	
	function generate($sourceFile = "", $originalFilename = "", $destinationDirectory = "", $destinationDirectoryFilename = "", $width = -1, $height = -1){
		if (!empty($sourceFile))
			$this->sourceFile = $sourceFile;
      
		if (!empty($originalFilename))
			$this->originalFilename = $originalFilename;
       
		if (!empty($destinationDirectory))
			$this->destinationDirectory = $destinationDirectory;
       
		if (!empty($destinationDirectoryFilename))
			$this->destinationDirectoryFilename = $destinationDirectoryFilename;
      
		if (!empty($width))
			$this->width = $width;
      
		if (!empty($height))
			$this->height = $height;
		
		list(, ,$this->extension) = array_values(pathinfo($this->originalFilename));
		switch ($this->extension)
		
		{
			case 'gif' : case 'GIF' :
				$createImageFunction = 'imagecreatefromgif';
				$outputImageFunction = 'imagegif';
			  break;
			
			case 'png' : case 'PNG' :
				$createImageFunction = 'imagecreatefrompng';
				$outputImageFunction = 'imagepng';
			  break;
			
			case 'bmp' : case 'BMP' :
				$createImageFunction = 'imagecreatefromwbmp';
				$outputImageFunction = 'imagewbmp';
			  break;
				
			case 'jpg': case 'jpeg': case 'JPG': case 'JPEG':
				$createImageFunction = 'imagecreatefromjpeg';
				$outputImageFunction = 'imagejpeg';
			  break;
			
			default : 
				exit("Sorry: The format '{$this->extension}' is unsuported");
			  break;
		}
			
		$this->img  = $createImageFunction($this->sourceFile);
			
		list($this->org_width, $this->org_height) = getimagesize($this->sourceFile);
			
		if ($this->height == -1)
		{
			$this->height = round($this->org_height * $this->width / $this->org_width);
		}
		if ($this->width == -1)
		{
			$this->width = round($this->org_width * $this->height / $this->org_height);
		}	 
		$this->xoffset = 0;
		$this->yoffset = 0;
		$this->img_new = imagecreatetruecolor($this->width, $this->height);	
		if ($this->img_new)
		{
			imagecopyresampled($this->img_new, $this->img, 0, 0, $this->xoffset, $this->yoffset, $this->width, $this->height, $this->org_width, $this->org_height);
			list(, , , $this->newFilename) = array_values(pathinfo($this->destinationDirectoryFilename));
			$this->fullDestination = ($this->destinationDirectory.'/'.$this->newFilename.'.'.$this->extension);
			$outputImageFunction($this->img_new, $this->fullDestination);
		}
		else
		{
			$this->failed = true;
		}
		if ($this->failed == false)
		{
			return $this->fullDestination;
		}
	}

	/*
	 *
	 * Method is used to create the image in uploader_tmp directory where the images are created in diffternt format
	 * 
	 * @param imageString, path, name, extention
	 * 
	 * @return true/false
	 */	
	function createimage( $imageString = "", $path = NULL, $name = NULL, $extention = "" ){
        $data = base64_decode($imageString); //base64 decoded image data
		$source_img = imagecreatefromstring($data);

        $color = imagecolorallocate($source_img, 255, 0, 0);
        imagesetpixel($source_img, 0, 0, $color);
		$originalFile =  $name . '.'.$extention;
		$imageSave = ($extention=="png")?imagepng($source_img, $path.$originalFile, NULL):imagejpeg($source_img, $path.$originalFile, 100);
		imagedestroy($source_img);// 
		if($imageSave){
			return $originalFile;
		 }else{
			return false;
		}


	}



public function  uploadProfilePicToAmazon($val,$id,$ext = 'png'){
			
	      		    App::uses('S3', 'Vendor');
				    // create image 
		            $originalFile = $this->createimage($val,WWW_ROOT . 'img/uploader_tmp/',md5($id), $ext);
		            
		            if($originalFile!=false){
		            // resize image  
		            $imagePath250 = $this->generate(WWW_ROOT . 'img/uploader_tmp/'.$originalFile,$originalFile,WWW_ROOT . 'img/uploader_tmp','250_'.md5($id).'.png','250');
		            $imagePath125 = $this->generate(WWW_ROOT . 'img/uploader_tmp/'.$originalFile,$originalFile,WWW_ROOT . 'img/uploader_tmp','125_'.md5($id).'.png','125');
					// put image to S3 server 
					$s3 = new S3(ACCESSKEY,SECRETKEY);
					$s3->putObjectFile(
						      WWW_ROOT . 'img/uploader_tmp/'.$originalFile,
						      BUCKET,
						      $id.'/profile/'.md5($id).'.png',
						      S3::ACL_PUBLIC_READ,
						      array(),
						      'image/png');

					$s3->putObjectFile(
						      $imagePath250,
						      BUCKET,
						      $id.'/profile/250_'.md5($id).'.png',
						      S3::ACL_PUBLIC_READ,
						      array(),
						      'image/png');
						    
				    $s3->putObjectFile(
						      $imagePath125,
						      BUCKET,
						      $id.'/profile/125_'.md5($id).'.png',
						      S3::ACL_PUBLIC_READ,
						      array(),
						      'image/png');

				    // Deleting the image from physical existance; to do;
				     return $originalFile;

				      }else{
				   	  return false;
				   }
			}
	/*
	On: 07-08-2015
	I/P: $params = array()
			$params['img_path'] => Destination Image path
			$params['img_name'] => Image name on amazon
	O/P: 
	Desc: Uploading image from real path to amazon server.
	*/
	public function moveImageToAmazon( $params = array() ,$contentType = 'image/png' ){
		App::uses('S3', 'Vendor');
			$s3 = new S3(ACCESSKEY,SECRETKEY);
			$s3->putObjectFile(
			  $params['img_path'],
			  BUCKET,
			  $params['img_name'],
			  S3::ACL_PUBLIC_READ,
			  array(),
			  $contentType);
	}

	/*
	On: 17-09-2015
	I/P: 
	O/P: 
	Desc: 
	*/
	public function uploadFileTempLocation( $amazonPath = '', $files = array(), $fileName = ''){
		$targetPath = WWW_ROOT . 'img/uploader_tmp/';
		$addedFileName = "";
		$temp = explode(".", basename( $files[$fileName]['name']));
		$newfilename = strtotime( date('Y-m-d H:i:s') ). rand(0,1000).'.' . end($temp);
		$addedFileName = $targetPath . $newfilename;
		try{
				if(move_uploaded_file( $files[$fileName]['tmp_name'], $addedFileName )){
					$this->moveImageToAmazon( array('img_path'=> $addedFileName, 'img_name'=> $amazonPath.$newfilename), $files[$fileName]['type'] );
				}
		}catch(Exception $e){}
		return $newfilename;
	}
}
?>
