<?php

/*
 * MedicBleep Component
 * 
 * Related methods for quick blox 
 *
 */
class MedicBleepComponent extends Component{ 
	public function quickAuth() {
		        $nonce = rand();
		        $timestamp = time();
		        $signature_string = "application_id=" . APPLICATION_ID . "&auth_key=" . AUTH_KEY . "&nonce=" . $nonce . "&timestamp=" . $timestamp;
		        $signature = hash_hmac('sha1', $signature_string, AUTH_SECRET);
		        // Build post body
		        $post_body = http_build_query(array(
		            'application_id' => APPLICATION_ID,
		            'auth_key' => AUTH_KEY,
		            'timestamp' => $timestamp,
		            'nonce' => $nonce,
		            'signature' => $signature,
		        ));
		        $curl = curl_init();
		        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT . 'session.json' );
		        curl_setopt($curl, CURLOPT_POST, true); // Use POST
		        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body); // Setup post body
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // Receive server response
		        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // Receive server response
		        $response = curl_exec($curl);
		        $responseVal = json_decode($response, true);
		        if (isset($responseVal['session']['token']) && !empty($responseVal['session']['token'])) {
		            //return json_decode($response);
		            return $responseVal['session']['token'];
		        } else {
		            //$error = curl_error($curl). '(' .curl_errno($curl). ')';
		        	//return $error;
		        	$responseVal['session']['token'] = "";
		        	return $responseVal['session']['token'];
		        }
		        curl_close($curl);
		}
		
	public function quickAddUsers($token,$username,$password,$email,$externalid,$fullname,$profile_image) {
				$user_data = array(
		            'user[login]' => $username,
		            'user[password]' => $password,
		            'user[email]' => $email,
		            'user[full_name]' => $fullname,
		            'user[external_user_id]' => $externalid,
		        );
		        if($profile_image != ""){
		        		$update_array = array(
		        					'avatar_url' => $profile_image ,
  								'status' => "",
  								'is_import' => "true",
  							);
  					$user_data["user"]["custom_data"] = json_encode($update_array);
		        }
		        $post_body = http_build_query($user_data);
		        $curl = curl_init();
		        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'users.json');
		        curl_setopt($curl, CURLOPT_POST, true);
		        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body);
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		            'Accept: application/json',
		            'Content-Type: application/x-www-form-urlencoded',
		            'QuickBlox-REST-API-Version: 0.1.0',
		            'QB-Token: ' . $token
		        ));
		        $response = curl_exec($curl);
		        if ($response) {
		            return $response . "\n";
		        } else {
		            $error = curl_error($curl). '(' .curl_errno($curl). ')';
		            return $error . "\n";
		        }
		        curl_close($curl);
		}
		
	public function quickGetUsers($token) {
		        $curl = curl_init();
		        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'users.json?per_page=20');
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		            'QuickBlox-REST-API-Version: 0.1.0',
		            'QB-Token: ' . $token
		        ));
		        $response = curl_exec($curl);
		        if ($response) {
		                return json_decode($response);
		        } else {
		                echo false;
		        }
		        curl_close($curl);
		}
		
	public function quickGetDialog($token) {
		        $curl = curl_init();
		        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'chat/Dialog.json');
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		            'QuickBlox-REST-API-Version: 0.1.0',
		            'QB-Token: ' . $token
		        ));
		        $response = curl_exec($curl);
		        if ($response) {
		                return json_decode($response);
		        } else {
		                $error = curl_error($curl). '(' .curl_errno($curl). ')';
		                return json_decode($error) ;
		        }
		        curl_close($curl);
		}
		
	public function quickAddDialog($token,$from,$to,$name) {
		        $post_body = http_build_query(array(
		            'type' => 3,
		            'name' => $name,
		            'occupants_ids' => $to,
		        ));
		        $post_body = urldecode($post_body);
		        $curl = curl_init();
		        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'chat/Dialog.json');
		        curl_setopt($curl, CURLOPT_POST, true);
		        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body);
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		            'QuickBlox-REST-API-Version: 0.1.0',
		            'QB-Token: ' . $token
		        ));
		        $response = curl_exec($curl);
		        if ($response) {
		                return $response;
		        } else {
		                $error = curl_error($curl). '(' .curl_errno($curl). ')';
		                return $error . "\n";
		        }
		        curl_close($curl);
		}
		
	public function quickGetMessage($token, $dialogId) {
		        $curl = curl_init();
		        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'chat/Message.json?chat_dialog_id=' . $dialogId);
		
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		            'QuickBlox-REST-API-Version: 0.1.0',
		            'QB-Token: ' . $token
		        ));
		        $response = curl_exec($curl);
		
		        if ($response) {
		                return json_decode($response);
		        } else {
		                echo false;
		        }
		        curl_close($curl);
		}
	public function quickLogin($username,$password){
		$nonce = rand();
        $timestamp = time();
        $signature_string = "application_id=" . APPLICATION_ID . "&auth_key=" . AUTH_KEY . "&nonce=" . $nonce . "&timestamp=" . $timestamp . "&user[login]=" . $username . "&user[password]=" . $password;
        $signature = hash_hmac('sha1', $signature_string, AUTH_SECRET);
        $post_array = array(
            'application_id' => APPLICATION_ID,
            'auth_key' => AUTH_KEY,
            'timestamp' => $timestamp,
            'nonce' => $nonce,
            'signature' => $signature,
            'user[login]' => $username,
            'user[password]' => $password
        );
        $post_body = http_build_query($post_array);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT . 'session.json' );
        curl_setopt($curl, CURLOPT_POST, true); // Use POST
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body); // Setup post body
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // Receive server response
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // Receive server response
        // Execute request and read response
        $response = curl_exec($curl);
        // Close connection
        curl_close($curl);
		if ($response) {
                return json_decode($response);
        } else {
                $error = curl_error($curl). '(' .curl_errno($curl). ')';
		        return $error;
        }   
	}
		
	public function quickLogout($token){
		        //echo $token;exit();
		        $curl = curl_init();
		        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'session.json');
		        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		
		        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		            'QuickBlox-REST-API-Version: 0.1.0',
		            'QB-Token: ' . $token
		        ));
		        $response = curl_exec($curl);
		        if ($response) {
		            return $response;
		        } else {
		            $error = curl_error($curl). '(' .curl_errno($curl). ')';
		            return $error;
		        }
		        curl_close($curl);
		}
	public function quickSessionDetails($token){
		        $curl = curl_init();
		        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'session.json');
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		            'QuickBlox-REST-API-Version: 0.1.0',
		            'QB-Token: ' . $token
		        ));
		        $response = curl_exec($curl);
		
		        if ($response) {
		                return $response;
		        } else {
		                $error = curl_error($curl). '(' .curl_errno($curl). ')';
		                return $error;
		        }
		        curl_close($curl);
		}
	public function getuserDetails($token,$externalid){
		        $curl = curl_init();
		        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'users/external/'.$externalid.'.json');
		
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		            'QuickBlox-REST-API-Version: 0.1.0',
		            'QB-Token: ' . $token
		        ));
		        $response = curl_exec($curl);
		        if ($response) {
		                return json_decode($response);
		        } else {
		                $error = curl_error($curl). '(' .curl_errno($curl). ')';
		                return $error;
		        }
		        curl_close($curl);
		}
	public function sendMessage($token,$dialogId,$message){
		$data = array(
		  'recipient_id' => $dialogId,
		  'message' => $message,
		  'send_to_chat' => 1,
		);
		$request = json_encode($data);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, QB_API_ENDPOINT.'chat/Message.json');  
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		  'Content-Type: application/json',
		  'QuickBlox-REST-API-Version: 0.1.0',
		  'QB-Token: ' . $token
		));
		$resultJSON = curl_exec($ch);
		$pretty = json_encode(json_decode($resultJSON), JSON_PRETTY_PRINT);
		return $pretty;
	}
	public function quickAdduserFromOcr($username,$password,$email,$externalid,$fullname){
		$tokenDetails = $this->quickAuth();
		/*$token_array = json_decode($tokenDetails);
		$token = "";
		if(isset($token_array->session->token) && $token_array->session->token != ""){
			$token = $token_array->session->token;
			return $this->quickAddUsers($token,$username,$password,$email,$externalid,$fullname,'');
		}
		*/
		if(!empty($tokenDetails)){
			$token = $tokenDetails;
			return $this->quickAddUsers($token,$username,$password,$email,$externalid,$fullname,'');
		}else{
			return 'no token created';
		}
		
	}
	public function quickAddRemoveDialog($sender,$receiver,$status){
		/*$status = 0 (for unfriend case)
		$status = 1 (for Accepting friend request case)*/
		$sender_email = $sender['User']['email'];
		$tokenDetails = $this->quickLogin($sender_email,'12345678');
		$token = $tokenDetails->session->token;
		$sender_id = $tokenDetails->session->user_id;
		if($status == 1 ){
			$colleagueDetails = $this->getuserDetails($token,$receiver['User']['id']);
			$receiver_name = $colleagueDetails->user->full_name;
			$receiver_chat_id = $colleagueDetails->user->id;
			$message = $this->quickAddDialog($token,$sender_id,$receiver_chat_id,$receiver_name);
		}else if($status == 0){
			$colleagueDetails = $this->getuserDetails($token,$receiver['User']['id']);
			$receiver_name = $colleagueDetails->user->full_name;
			$receiver_chat_id = $colleagueDetails->user->id;
			$dialogueList = $this->quickGetDialog($token);
			$dialogue_ids = array();
			foreach($dialogueList->items as $dialogue){
				if (in_array($receiver_chat_id, $dialogue->occupants_ids)) {
	    				if($dialogue->type == 3){
	    					$dialogue_ids[] = $dialogue->_id;
	    					$delete_status = $this->delete_dialogue($token,$dialogue_ids);
	    				}else if($dialogue->type == 2){
	    					if($dialogue->user_id == $sender_id){
	    						$update_group_status = $this->update_dialogue($dialogue->_id,$dialogue->name,$receiver_chat_id);
	    					}elseif($dialogue->user_id == $receiver_chat_id){
	    						$update_group_status = $this->update_dialogue($dialogue->_id,$dialogue->name,$sender_id);
	    					}
	    				}
				}
			}
		}
	}
	public function delete_dialogue($token,$dialogue_ids){
		if(count($dialogue_ids) > 0){
			$dialogue_strings = implode(",",$dialogue_ids);
			$dialogue_url = $dialogue_strings.'.json';
			$curl = curl_init();
	        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'chat/Dialog/'.$dialogue_url);
	        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
	        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
	            'QuickBlox-REST-API-Version: 0.1.0',
	            'QB-Token: ' . $token
	        ));
	        $response = curl_exec($curl);
	        if ($response) {
                return json_decode($response);
	        } else {
	                $error = curl_error($curl). '(' .curl_errno($curl). ')';
			        return $error;
	        }   
	        curl_close($curl);
		}
	}
	public function update_dialogue($token,$group_name,$userid){
		$data = array(
  			'name' => $group_name,
			'pull_all'=> array('occupants_ids' =>  array($userid)),
  		);
  		/////////
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'chat/Dialog/'.$token);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        /////////
        $response = curl_exec($curl);
        if ($response) {
                return $response;
        } else {
                $error = curl_error($curl). '(' .curl_errno($curl). ')';
                return $error . "\n";
        }
        curl_close($curl);
	}
	public function update_userprofile($useremail,$profile_image){
		$tokenDetails = $this->quickLogin($useremail,'12345678');
		$token = $tokenDetails->session->token;
		$user_id = $tokenDetails->session->user_id;
		$user_details = $this->getuserDetailsbyId($token,$user_id);
		$custom_data = json_decode($user_details->user->custom_data);
		$status = "";
		$is_import = "true";
		if(isset($custom_data->status) && $custom_data->status != ""){
			$status = $custom_data->status;
		}
		$profile_image_details = $this->update_profile_image($token,$user_id,$profile_image,$status,$is_import);
		return $profile_image_details;
	}
	public function update_profile_image($token,$user_id,$profile_image,$status,$is_import){
  		$update_array = array('avatar_url' => $profile_image ,
  								'status' => $status,
  								'is_import' => $is_import,
  							);
  		$data["user"]["custom_data"] = json_encode($update_array);
  		/////////
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'users/'.$user_id.'.json');
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        //curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
	            'QuickBlox-REST-API-Version: 0.1.0',
	            'QB-Token: ' . $token
	        ));
        /////////
        $response = curl_exec($curl);
        if ($response) {
                return $response;
        } else {
                $error = curl_error($curl). '(' .curl_errno($curl). ')';
                return $error . "\n";
        }
        curl_close($curl);
	}
	
	public function showAllMessages($token,$chatid){
		$curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'chat/Message.json?chat_dialog_id='.$chatid);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'QuickBlox-REST-API-Version: 0.1.0',
            'QB-Token: ' . $token
        ));
        $response = curl_exec($curl);
        if ($response) {
                return $response;
        } else {
                $error = curl_error($curl). '(' .curl_errno($curl). ')';
                return $error;
        }
        curl_close($curl);
	}
	
	public function getuserDetailsbyId($token,$userId){
		        $curl = curl_init();
		        curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'users/'.$userId.'.json');
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		            'QuickBlox-REST-API-Version: 0.1.0',
		            'QB-Token: ' . $token
		        ));
		        $response = curl_exec($curl);
		        if ($response) {
		                return json_decode($response);
		        } else {
		                $error = curl_error($curl). '(' .curl_errno($curl). ')';
		                return $error;
		        }
		        curl_close($curl);
		}
}
?>
