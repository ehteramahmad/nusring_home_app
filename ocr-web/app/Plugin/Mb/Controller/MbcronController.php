<?php
/*
-----------------------------------------------------------------------------------------------
	Desc: Controller used to do some scheduled tasks.
-----------------------------------------------------------------------------------------------
*/

App::uses('AppController', 'Controller');

class MbcronController extends AppController {
	public $uses = array('Mb.User', 'Mb.UserProfile', 'Mb.UserColleague', 'Mb.SyncContact', 'Mb.UserFollow','Mb.EmailSmsLog','Mb.UserSubscriptionLog');
	public $components = array('Mb.MbCommon', 'Mb.MedicBleep','Mb.MbEmail');
	/*
	-----------------------------------------------------------------------------------------------
	On: 23-05-2016
	I/P: 
	O/P: 
	Desc: Auto colleague
	-----------------------------------------------------------------------------------------------
	*/
	public function setColleagueFromSyncListOld(){
		ini_set('max_execution_time', 300);
		$syncData = $this->SyncContact->find("all", array("conditions"=> array("is_sync_done"=> 0), "order"=> "id DESC"));
		foreach($syncData as $sData){
			$contactNo = ""; $email = ""; $userIdSyncBy = "";
			$contactNo = !empty($sData['SyncContact']['mobile_no']) ? $sData['SyncContact']['mobile_no'] : "";
			$email = !empty($sData['SyncContact']['email']) ? $sData['SyncContact']['email'] : "";
			$userIdSyncBy = $sData['SyncContact']['user_id'];
			$userSyncByDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $userIdSyncBy)));
			//** Conditions
			$userData = array();
			//** Check User by mobile/email
			if(!empty($contactNo)){
				$conditions = array(
								"UserProfile.contact_no"=> $contactNo
								);
				$userData = $this->User->find("first", array("conditions"=> $conditions));
			}else if(!empty($email)){
				$conditions = array(
								"User.email"=> $email
								);
				$userData = $this->User->find("first", array("conditions"=> $conditions));
			}
			//echo "<pre>";print_r($userData);die;
			//** If user found from sync list then make colleague
			if(!empty($userData)){
				$userIdToBeSync = $userData['User']['id'];
				$isSyncDone = false;
				if($userData['UserProfile']['country_id'] == $userSyncByDetails['UserProfile']['country_id']){
					//** Set Colleague
					$myColleague = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $userIdSyncBy, "colleague_user_id"=> $userIdToBeSync)));
					$meColleague = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $userIdToBeSync, "colleague_user_id"=> $userIdSyncBy)));
					if($userIdSyncBy != $userIdToBeSync){ //** Don't Make colleague to self user
						if($myColleague == 0 && $meColleague == 0){
							$colleagueData = array("user_id"=> $userIdSyncBy, "colleague_user_id"=> $userIdToBeSync, "status"=> 2);
							$this->UserColleague->saveAll($colleagueData);
							//** Set SyncContact Model value to true
							$isSyncDone = true;
						}else{
							/*$colleagueCondition = array();
							if($myColleague > 0){
								$colleagueCondition = array("user_id"=> $userIdSyncBy, "colleague_user_id"=> $userIdToBeSync);
								$checkColleagueStatus = $this->UserColleague->find("first", array("conditions"=> $colleagueCondition));
								if($checkColleagueStatus['UserColleague']['status'] == 0){
									$inviteColleague = $this->UserColleague->updateAll(array("status"=> 2), $colleagueData );
								}
							}*/
							/*else{
								$colleagueCondition = array("user_id"=> $userIdToBeSync, "colleague_user_id"=> $userIdSyncBy);
								$checkColleagueStatus = $this->UserColleague->find("first", array("conditions"=> $colleagueCondition));
								if($checkColleagueStatus['UserColleague']['status'] == 0){
									$inviteColleague = $this->UserColleague->updateAll( array("user_id"=> $userIdToBeSync, "colleague_user_id"=> $userIdSyncBy,"status"=> 2), array("id"=> $checkColleagueStatus['UserColleague']['id']) );
								}
							}*/
							//** Set SyncContact Model value to true
							$isSyncDone = true;
						}

						//** If any user send colleague request also add user follow[START]
							try{
								$conditions = array("followed_by"=> $userIdSyncBy, "followed_to"=> $userIdToBeSync);
								$userFollowCheck = $this->UserFollow->find("count", array("conditions"=> $conditions));
								if($userFollowCheck == 0 ){
									$this->UserFollow->save( array("followed_by"=> $userIdSyncBy, "followed_to"=> $userIdToBeSync, "follow_type"=> 1, "status"=> 1) );
								}else{
									$this->UserFollow->updateAll( array("follow_type"=> 1, "status"=> 1), $conditions );
								}
							}catch( Exception $e ){}
						//** If any user send colleague request also add user follow[END]
					}
					//** Set SyncContact Model value to true
					$isSyncDone = true;
				}
				//** Update SyncContact Model
				if($isSyncDone){
					$this->SyncContact->updateAll(array('is_sync_done' => 1 ), array("id"=> $sData['SyncContact']['id']));
				}
					/* Accept/Remove friend  request Quickblox start */
						/*$senderDetails = $this->User->findById($userIdSyncBy );
						$receiverDetails = $this->User->findById($userIdToBeSync);
						try{ 
							$this->MedicBleep->quickAddRemoveDialog($senderDetails, $receiverDetails, 1);
						}catch( Exception $e ){}*/
					/* Accept/Remove friend request Quickblox end */
			}
		}
		exit;
	}

	/*
	-----------------------------------------------------------------------------------------------
	On: 29-07-2016
	I/P: 
	O/P: 
	Desc: Auto colleague
	-----------------------------------------------------------------------------------------------
	*/
	public function setColleagueFromSyncList(){
		ini_set('max_execution_time', 300);
		ini_set('memory_limit', '256M');
		//** Fetch all sync contacts with email matching[START]
		$conditions = array(
							"SyncContact.is_sync_done"=> 0,
							"User.status"=> 1,
							"User.approved"=> 1,
						);
		$fields = array("User.id, User.email, SyncContact.id, SyncContact.user_id, SyncContact.email, SyncContact.is_sync_done");
		//$totCountFields = array("count(*) AS totRecord");
		$options = array(
								'joins'=>
								array(
									array(
										'table' => 'users',
										'alias' => 'User',
										'type' => 'INNER',
										'conditions'=> array(
											'User.email = SyncContact.email',
											"SyncContact.user_id <> User.id",
											)
									),
								),
								'conditions' => $conditions,
								'fields'=> $fields,
								//'limit'=> $pageSize,
								//'offset'=> $offsetVal,
								'group'=> array('User.id, SyncContact.user_id'), 
								'order'=> 'SyncContact.id DESC'
							);
		$userDataEmailSync = $this->SyncContact->find('all', $options);
		foreach($userDataEmailSync as $sData){

			$userIdSyncBy = ""; $userIdToBeSync = "";
			$userIdSyncBy = $sData['SyncContact']['user_id'];
			$userSyncByDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $userIdSyncBy)));
			//** User to be sync details
			$userData = array();
			$userIdToBeSync = $sData['User']['id'];
			$userData = $this->User->find("first", array("conditions"=> array("User.id"=> $userIdToBeSync)));
			//** If user found from sync list then make colleague
			if(!empty($userData)){
				$isSyncDone = false;
				if($userData['UserProfile']['country_id'] == $userSyncByDetails['UserProfile']['country_id']){
					//** Set Colleague
					$myColleague = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $userIdSyncBy, "colleague_user_id"=> $userIdToBeSync)));
					$meColleague = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $userIdToBeSync, "colleague_user_id"=> $userIdSyncBy)));
					if($userIdSyncBy != $userIdToBeSync){ //** Don't Make colleague to self user
						if($myColleague == 0 && $meColleague == 0){
							$colleagueData = array("user_id"=> $userIdSyncBy, "colleague_user_id"=> $userIdToBeSync, "status"=> 2);
							$this->UserColleague->saveAll($colleagueData);
							//** Set SyncContact Model value to true
							$isSyncDone = true;
						}else{
							//** Set SyncContact Model value to true
							$isSyncDone = true;
						}

						//** If any user send colleague request also add user follow[START]
							try{
								$conditions = array("followed_by"=> $userIdSyncBy, "followed_to"=> $userIdToBeSync);
								$userFollowCheck = $this->UserFollow->find("count", array("conditions"=> $conditions));
								if($userFollowCheck == 0 ){
									$this->UserFollow->save( array("followed_by"=> $userIdSyncBy, "followed_to"=> $userIdToBeSync, "follow_type"=> 1, "status"=> 1) );
								}else{
									$this->UserFollow->updateAll( array("follow_type"=> 1, "status"=> 1), $conditions );
								}
							}catch( Exception $e ){}
						//** If any user send colleague request also add user follow[END]
					}
					//** Set SyncContact Model value to true
					$isSyncDone = true;
				}
				//** Update SyncContact Model
				if($isSyncDone){
					$this->SyncContact->updateAll(array('is_sync_done' => 1 ), array("id"=> $sData['SyncContact']['id']));
				}
			}
		}
		//echo "<pre>"; print_r($userDataEmailSync);
		//** Fetch all sync contacts with email matching[END]									
		

		//** Fetch all sync contacts with Mobile matching[START]
		$conditionsMobileSync = array(
							"SyncContact.is_sync_done"=> 0,
						);
		$fieldsMobileSync = array("UserProfile.user_id, SyncContact.id, SyncContact.user_id, SyncContact.email, SyncContact.is_sync_done");
		$optionsMobileSync = array(
								'joins'=>
								array(
									array(
										'table' => 'user_profiles',
										'alias' => 'UserProfile',
										'type' => 'INNER',
										'conditions'=> array(
											'UserProfile.contact_no = SyncContact.mobile_no',
											"SyncContact.user_id <> UserProfile.user_id",
											)
									),
								),
								'conditions' => $conditionsMobileSync,
								'fields'=> $fieldsMobileSync,
								//'limit'=> $pageSize,
								//'offset'=> $offsetVal,
								'group'=> array('UserProfile.user_id, SyncContact.user_id'), 
								'order'=> 'SyncContact.id DESC'
							);
		$userDataMobileSync = $this->SyncContact->find('all', $optionsMobileSync);
		//echo "<pre>"; print_r($userDataMobileSync);
		foreach($userDataMobileSync as $sData){

			$userIdSyncBy = ""; $userIdToBeSync = ""; $userSyncByDetails = array();
			$userIdSyncBy = $sData['SyncContact']['user_id'];
			$userSyncByDetails = $this->User->find("first", array("conditions"=> array("User.id"=> $userIdSyncBy)));
			//** User to be sync details
			$userData = array();
			$userIdToBeSync = $sData['UserProfile']['user_id'];
			$userData = $this->User->find("first", array("conditions"=> array("User.id"=> $userIdToBeSync)));
			//** If user found from sync list then make colleague
			if(!empty($userData)){
				$isSyncDone = false;
				if($userData['UserProfile']['country_id'] == $userSyncByDetails['UserProfile']['country_id']){
					//** Set Colleague
					$myColleague = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $userIdSyncBy, "colleague_user_id"=> $userIdToBeSync)));
					$meColleague = $this->UserColleague->find("count", array("conditions"=> array("user_id"=> $userIdToBeSync, "colleague_user_id"=> $userIdSyncBy)));
					if($userIdSyncBy != $userIdToBeSync){ //** Don't Make colleague to self user
						if($myColleague == 0 && $meColleague == 0){
							$colleagueData = array("user_id"=> $userIdSyncBy, "colleague_user_id"=> $userIdToBeSync, "status"=> 2);
							$this->UserColleague->saveAll($colleagueData);
							//** Set SyncContact Model value to true
							$isSyncDone = true;
						}else{
							//** Set SyncContact Model value to true
							$isSyncDone = true;
						}

						//** If any user send colleague request also add user follow[START]
							try{
								$conditions = array("followed_by"=> $userIdSyncBy, "followed_to"=> $userIdToBeSync);
								$userFollowCheck = $this->UserFollow->find("count", array("conditions"=> $conditions));
								if($userFollowCheck == 0 ){
									$this->UserFollow->save( array("followed_by"=> $userIdSyncBy, "followed_to"=> $userIdToBeSync, "follow_type"=> 1, "status"=> 1) );
								}else{
									$this->UserFollow->updateAll( array("follow_type"=> 1, "status"=> 1), $conditions );
								}
							}catch( Exception $e ){}
						//** If any user send colleague request also add user follow[END]
					}
					//** Set SyncContact Model value to true
					$isSyncDone = true;
				}
				//** Update SyncContact Model
				if($isSyncDone){
					$this->SyncContact->updateAll(array('is_sync_done' => 1 ), array("id"=> $sData['SyncContact']['id']));
				}
			}
		}
		//echo "<pre>"; print_r($userDataMobileSync);die;
		//** Fetch all sync contacts with mobile matching[END]
		exit;
	}

	/*
	-----------------------------------------------------------------------------------------------
	On: 14-09-2017
	I/P: 
	O/P: 
	Desc: Send reminder mail to free trail user
	-----------------------------------------------------------------------------------------------
	*/

	public function freeTrailReminderMail()
	{
		$freeTrialUser = $this->UserSubscriptionLog->find("all", array("conditions"=> array("subscribe_type"=> 0)));
		foreach ($freeTrialUser AS $user) {
			// $userEmail = $this->User->find("first", array("conditions"=> array("user_id"=> $user['UserSubscriptionLog']['user_id'])));
			$userLists = $this->User->getUserDetail( $user['UserSubscriptionLog']['user_id'] );
			$subscriptionExpiryDate = $user['UserSubscriptionLog']['subscription_expiry_date'];
			$currentDate = date('Y-m-d H:i:s');
			//$subscriptionPerriod = ceil(abs(strtotime($subscriptionExpiryDate) - strtotime($currentDate)) / 86400);
			$subscriptionPerriod = floor((strtotime($subscriptionExpiryDate) - strtotime($currentDate)) / 86400);
			if($subscriptionPerriod == 0 || $subscriptionPerriod == 1 || $subscriptionPerriod == 2 || $subscriptionPerriod == 7 || $subscriptionPerriod ==15 )
			{
				if(isset($userLists[0]['User']['email']))
				{
					try{
						$mailSend = $this->MbEmail->sendFreeSubscriptionMail( $subscriptionPerriod, $userLists[0]['User']['email'], $userLists[0][0]['usrName'] );
					}catch(Exception $e){
						$mailSend = $e->getMessage();
					}
					$this->EmailSmsLog->save( array("type"=> "email", "msg"=> $mailSend, "user_id"=> $user['UserSubscriptionLog']['user_id'], "notify_type"=> "Free Trial Subscription Mail") );
				}
			}
		}
		echo "Done";exit();
	}

	/*
	-----------------------------------------------------------------------------------------------
	On: 13-12-2017
	I/P: 
	O/P: 
	Desc: Check institute and user subscription
	-----------------------------------------------------------------------------------------------
	*/

	public function checkUserAndInstituteSubscription()
	{
		$userLists = $this->User->find("all", array("conditions"=> array("status"=> 1, "approved"=>1)));
		foreach ($userLists as $users) {

			$userCurrentCompany = $this->UserSubscriptionLog->find("first", array("conditions"=> array("user_id"=> $users['User']['id']), "order"=> array("id DESC")));
			if(! empty($userCurrentCompany))
			{
				//********Check user subscription expiry date ********//
				if(strtotime($userCurrentCompany['UserSubscriptionLog']['subscription_expiry_date']) <= strtotime(date('Y-m-d 23:59:59')))
				{
					$conditions = array("user_id"=> $userCurrentCompany['UserSubscriptionLog']['user_id'], "company_id"=> $userCurrentCompany['UserSubscriptionLog']['company_id']);
					$this->UserSubscriptionLog->updateAll( array("subscribe_type"=> 3), $conditions );
				}

				//********Check Institute subscription expiry date ********//
				else if( ($userCurrentCompany['UserSubscriptionLog']['company_id'] != -1) || ($userCurrentCompany['UserSubscriptionLog']['company_id'] != 0) )
				{
					$dataParams['company_id'] = $userCurrentCompany['UserSubscriptionLog']['company_id'];
					$checkInstituteSubscription = $this->CompanyName->getComapanySubscriptionDetails($dataParams);
					if($checkInstituteSubscription == 0)
					{
						$conditions = array("user_id"=> $userCurrentCompany['UserSubscriptionLog']['user_id'], "company_id"=> $dataParams['company_id']);
						$this->UserSubscriptionLog->updateAll( array("subscribe_type"=> 3), $conditions );
					}
				}
			}
		}
		echo "Done";
		exit();
		
	}

}
