<?php
/*
 * Post controller.
 *
 * This file will render views from views/postwebservices/
 *
 
 */

App::uses('AppController', 'Controller');


class MbpagewebservicesController extends AppController {
	public $uses = array('Content');
	public $components = array('Common');
	/*
	On:
	I/P:
	O/P:
	Desc: Page path as a JSON response.
	*/
	public function pageContent(){
		$this->autoRender = false;
		$responseData = array();
		if($this->accesskeyCheck()){
			$dataInput = $this->request->input ( 'json_decode', true) ;
			$encryptedData = $this->Common->decryptData( $dataInput['values'] ); 
			$dataInput = json_decode($encryptedData, true);
			$dataInput['requestFrom'] = 'mobile';
			$requestFrom = isset($dataInput['requestFrom']) ? $dataInput['requestFrom'] : '';
			if( $dataInput['page'] == 'tnc' ){
				//$pageContent = array('PageContent'=> BASE_URL . 'Index/tnc/' . $requestFrom );
				$pageContent = array('PageContent'=> 'http://medicbleep.com/termsMobile.html');
			}elseif( $dataInput['page'] == 'faq' ){
				//$pageContent = array('PageContent'=> BASE_URL . 'Index/faq/' . $requestFrom);
				$pageContent = array('PageContent'=> BASE_URL . 'Index/faqMobile');
			}elseif( $dataInput['page'] == 'privacy' ){
				//$pageContent = array('PageContent'=> BASE_URL . 'Index/privacyPolicy/' . $requestFrom);
				$pageContent = array('PageContent'=> 'http://medicbleep.com/privacyPolicyMobile.html');
			}elseif( $dataInput['page'] == 'about' ){
				//$pageContent = array('PageContent'=> BASE_URL . 'Index/about/' . $requestFrom);
				$pageContent = array('PageContent'=> BASE_URL . 'Index/aboutMobile');
			}elseif( $dataInput['page'] == 'consentTermConditions' ){
				//$pageContent = array('PageContent'=> BASE_URL . 'Index/consentTermConditions/' . $requestFrom);
				$pageContent = array('PageContent'=> BASE_URL . 'Index/consentTermConditionsMobile');
			}
			$responseData = array('method_name'=> 'pageContent', 'status'=>"1", 'response_code'=> '200', 'message'=> ERROR_200, 'data'=> $pageContent);	
		}else{
			$responseData = array('method_name'=> 'pageContent', 'status'=>"0", 'response_code'=> '602', 'message'=> ERROR_602);	
		}
		//echo json_encode($response);
		$encryptedData = $this->Common->encryptData(json_encode($responseData));
		echo json_encode(array("values"=> $encryptedData));
		exit;
	}
	
}
