<?php


App::uses('Model', 'Model');

/**
 * Represents model UserSubscriptionLog
 */
class UserSubscriptionLog extends Model {

	/*
	-------------------------------------------------------------------
	On: 02-06-2017
	I/P: 
	O/P:
	Desc: Check if users subscription expired
	-------------------------------------------------------------------
	*/
	public function checkSusbcriptionExpired($dataParams=array()){
		$isExpired = false; 
		if(!empty($dataParams)){
			$date = date('Y-m-d 23:59:59');
			$expiryDate = $dataParams['subscription_expiry_date'];
			if( strtotime($date) > strtotime($expiryDate) ){
				$isExpired = true;
			}
		}
		return $isExpired;
	}
	
}
