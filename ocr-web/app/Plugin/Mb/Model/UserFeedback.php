<?php 
/*
User feedback model.
*/
App::uses('AppModel', 'Model');
class UserFeedback extends AppModel {

	public $belongsTo = array(
		'User' => array(
            'foreignKey' => false,
             'conditions' => array(
                 'User.id = UserFeedback.user_id'
             ),
        ),
        'UserProfile' => array(
            'foreignKey' => false,
             'conditions' => array(
                 'UserProfile.user_id = UserFeedback.user_id'
             ),
        )
    );

	/*
	------------------------------------------
	ON: 
	I/P: 
	O/P: 
	Desc: Fetches all feedbacks.
	------------------------------------------
	*/
	public function feedbackLists(){
		$feedbackLists = array();

		$options =array(             
			'joins' =>
				array(
					array(
						'table' => 'users',
						'alias' => 'User',
						'type' => 'left',
						'conditions'=> array('UserFeedback.user_id = User.id')
					),
				),
			'fields'=> array("UserFeedback.id, UserFeedback.user_id, UserFeedback.feedback,UserFeedback.status, UserFeedback.created, User.email"),
			'order'=> array("UserFeedback.created DESC")
			);
		$feedbackLists = $this->find('all', $options);
		return $feedbackLists;
	}
}
?>