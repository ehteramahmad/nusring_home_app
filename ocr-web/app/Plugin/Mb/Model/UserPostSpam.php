<?php 
/*
 Represents model for user post beat spam  by users for any Post(Article).
*/
App::uses('AppModel', 'Model');

class UserPostSpam extends AppModel {
	public $name = 'UserPostSpam';

	/*
	--------------------------------------------------------------------------------------------
	On: 17-12-2015
	I/P: 
	O/P: 
	Desc: List of all beats which have to check to mark as a spam beat.
	--------------------------------------------------------------------------------------------
	*/
	public function beatListToBeSpam(){
		$beatToBeSpamList = array();
		$conditions = " UserPostSpam.spam_confirmed = 0 ";
		$options = array(
			'joins'=>
								array(
									array(
										'table' => 'user_posts',
										'alias' => 'UserPost',
										'type' => 'inner',
										'conditions'=> array('UserPost.id = UserPostSpam.user_post_id')
									),
									array(
										'table' => 'users',
										'alias' => 'User',
										'type' => 'inner',
										'conditions'=> array('User.id = UserPost.user_id')
									),
									array(
										'table' => 'user_profiles',
										'alias' => 'UserProfile',
										'type' => 'inner',
										'conditions'=> array('User.id = UserProfile.user_id')
									),
									
								),
			'conditions' => $conditions,
			'fields' => array("UserPostSpam.user_post_id, UserPostSpam.post_spam_content_id,  COUNT(*) AS spamBeatCount, UserPost.title, User.id, User.email, UserProfile.first_name, UserProfile.last_name"),
			'group' => array("UserPostSpam.user_post_id HAVING spamBeatCount > 0")
		);
		$beatToBeSpamList = $this->find("all", $options);
		return $beatToBeSpamList;
	}
}
?>