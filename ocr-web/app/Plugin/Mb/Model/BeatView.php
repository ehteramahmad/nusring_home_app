<?php 
/*
 Represents model for any beat view by visitor.
*/
App::uses('AppModel', 'Model');

class BeatView extends AppModel {
	public $name = 'BeatView';

	/*
	----------------------------------------------------------------------------------------
	On: 19-04-2016
	I/P: $userId, $postId
	O/P: Beat view Count
	Desc: Returns beat view count by any user for any particular beat.
	----------------------------------------------------------------------------------------
	*/
	public function beatViewCount($postId = NULL){
		$beatViewCount = 0;
		if(!empty($postId)){
			$conditions = array("post_id"=> $postId);
			$beatViewCount = $this->find("count", array("conditions"=> $conditions));
		}
		return $beatViewCount;
	}
}
?>