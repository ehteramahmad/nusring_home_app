<?php 
/*
Represent model for company name.
*/
App::uses('AppModel', 'Model');

class CompanyName extends AppModel {
	
	/*
	--------------------------------------------------------------------------
	On: 
	I/P: 
	O/P: 
	Desc: Displays all company names.
	--------------------------------------------------------------------------
	*/
	public function companyLists( $params = array() ){
		$companyLists = array();
		if( !empty($params) ){
			$pageNum = isset($params['page_number']) ? $params['page_number'] : 1;
			$pageSize = isset($params['size']) ? $params['size'] : DEFAULT_PAGE_SIZE;
			$offsetVal = ( $pageNum - 1 ) * $pageSize;
			$conditions = " status = 1 AND created_by = 0 ";
			if(!empty($params['searchText'])){
				if(strlen($conditions) >0){
					$conditions .= ' AND LOWER(company_name) LIKE LOWER("%'.$params['searchText'].'%") ';
				}
			}
			$companyLists = $this->find("all", array(
										"conditions"=> $conditions,
										"order"=> array("company_name"),
										'limit'=> $pageSize,
										'offset'=> $offsetVal
										)
									);
		}
		return $companyLists;
	}


	/*
	--------------------------------------------------------------------------------
	ON: 29-05-2017
	I/P:
	O/P:
	Desc: 
	--------------------------------------------------------------------------------
	*/
	public function getComapanySubscriptionDetails($dataParams=array()){
		$returnData = 0;
		if(! empty($dataParams))
		{
			$companyId = $dataParams['company_id'];
			$returnData=$this->find("count", array("conditions"=> array("id"=>$companyId, "status"=>1,"is_subscribed"=>1, "subscription_expiry_date >=" => date('Y-m-d 23:59:59'))));
		}
		return $returnData;
	}

}
?>