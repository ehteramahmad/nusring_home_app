<?php 
/*
UserDevices model to add, update ,validate user data
*/
App::uses('AppModel', 'Model');

class UserDevice extends AppModel {
	
	/*
	On: 04-08-2015
	I/P: $params = array()
	O/P: User Details
	Desc: 
	*/
	public function tokenDetails( $params = array() ){
		$tokenData = array();
		if( !empty($params) ){
			$tokenData = $this->find('first', array('conditions'=> $params));
		}
		return $tokenData;
	}

}
?>