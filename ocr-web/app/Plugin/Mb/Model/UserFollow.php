<?php 
/*
Add, update, Validate user follow/following details.
*/
App::uses('AppModel', 'Model');

class UserFollow extends AppModel {		
	
	/*
    --------------------------------------------------------------
	On: 12-10-2015
	I/P: 
	O/P: array()
	Desc: Output all follow users lists as an array.
	--------------------------------------------------------------
    */
    public function followLists( $pageSize = DEFAULT_PAGE_SIZE, $pageNum = 1 , $userId = NULL, $followId = NULL, $sortBy = 'latest' ){ 	
		$followListData = array();
		$offsetVal = ( $pageNum - 1 ) * $pageSize;
		
		//** If followId set fetch all follow users > followId
		if( !empty($followId) ){
			if( isset($sortBy) && $sortBy == 'previous'){
				$conditions = array('UserFollow.followed_to'=> $userId, 'UserFollow.follow_type'=> 1, 'UserFollow.status'=> 1,'UserFollow.id < '=> $followId, 'User.status'=> 1, 'User.approved'=> 1);
			}elseif(isset($sortBy) && $sortBy == 'latest'){
				$conditions = array('UserFollow.followed_to'=> $userId, 'UserFollow.follow_type'=> 1, 'UserFollow.status'=> 1,'UserFollow.id > '=> $followId, 'User.status'=> 1, 'User.approved'=> 1);
			}
		}else{
				$conditions = array('UserFollow.followed_to'=> $userId, 'UserFollow.follow_type'=> 1, 'UserFollow.status'=> 1, 'User.status'=> 1, 'User.approved'=> 1);
		}
		$options =array(             
			'joins' =>
				array(
					array(
						'table' => 'users',
						'alias' => 'User',
						'type' => 'left',
						//'foreignKey' => 'user_id',
						'conditions'=> array('UserFollow.followed_by = User.id')
					),
					array(
						'table' => 'user_profiles',
						'alias' => 'UserProfile',
						'type' => 'left',
						'foreignKey' => 'user_id',
						'conditions'=> array('UserFollow.followed_by = UserProfile.user_id')
					),
					array(
						'table' => 'user_institutions',
						'alias' => 'UserInstitution',
						'type' => 'left',
						'conditions'=> array('UserInstitution.user_id = UserProfile.user_id')
					),
				),
			'fields'=> array('UserProfile.user_id, UserProfile.first_name, UserProfile.last_name, UserProfile.profile_img, UserProfile.institution_name, UserProfile.country_id, UserProfile.county, UserProfile.city, UserProfile.profession_id, UserInstitution.institution_id, UserInstitution.education_degree_id, UserInstitution.from_date, UserInstitution.to_date'),
			'conditions'=> $conditions,
			'order'=> array('UserProfile.first_name'),
			'group'=> array('UserProfile.user_id'),
			'limit'=> $pageSize,
			'offset'=> $offsetVal  
		);
		$followListData = $this->find('all', $options);
		return $followListData;		
	}

	/*
    --------------------------------------------------------------
	On: 12-10-2015
	I/P: 
	O/P: array()
	Desc: Output all follow users lists as an array.
	--------------------------------------------------------------
    */
    public function followingLists( $pageSize = DEFAULT_PAGE_SIZE, $pageNum = 1 , $userId = NULL, $followId = NULL, $sortBy = 'latest' ){ 	
		$followingListData = array();
		$offsetVal = ( $pageNum - 1 ) * $pageSize;
		
		//** If followId set fetch all follow users > followId
		if( !empty($followId) ){
			if( isset($sortBy) && $sortBy == 'previous'){
				$conditions = array('UserFollow.followed_by'=> $userId, 'UserFollow.follow_type'=> 1, 'UserFollow.status'=> 1,'UserFollow.id < '=> $followId, 'User.status'=> 1, 'User.approved'=> 1);
			}elseif(isset($sortBy) && $sortBy == 'latest'){
				$conditions = array('UserFollow.followed_by'=> $userId, 'UserFollow.follow_type'=> 1, 'UserFollow.status'=> 1,'UserFollow.id > '=> $followId, 'User.status'=> 1, 'User.approved'=> 1);
			}
		}else{
				$conditions = array('UserFollow.followed_by'=> $userId, 'UserFollow.follow_type'=> 1, 'UserFollow.status'=> 1, 'User.status'=> 1, 'User.approved'=> 1);
		}
		$options =array(             
			'joins' =>
				array(
					array(
						'table' => 'users',
						'alias' => 'User',
						'type' => 'left',
						//'foreignKey' => 'user_id',
						'conditions'=> array('UserFollow.followed_to = User.id')
					),
					array(
						'table' => 'user_profiles',
						'alias' => 'UserProfile',
						'type' => 'left',
						'foreignKey' => 'user_id',
						'conditions'=> array('UserFollow.followed_to = UserProfile.user_id')
					),
					array(
						'table' => 'user_institutions',
						'alias' => 'UserInstitution',
						'type' => 'left',
						'conditions'=> array('UserInstitution.user_id = UserProfile.user_id')
					),
				),
			'fields'=> array('UserProfile.user_id, UserProfile.first_name, UserProfile.last_name, UserProfile.profile_img, UserProfile.institution_name, UserProfile.country_id, UserProfile.county, UserProfile.city, UserProfile.profession_id, UserInstitution.institution_id, UserInstitution.education_degree_id, UserInstitution.from_date, UserInstitution.to_date'),
			'conditions'=> $conditions,
			'order'=> array('UserProfile.first_name'),
			'group'=> array('UserProfile.user_id'),
			'limit'=> $pageSize,
			'offset'=> $offsetVal  
		);
		$followingListData = $this->find('all', $options);
		return $followingListData;		
	}

	/*
	----------------------------------------------------------------------------
	On: 29-03-2016
	I/P: $userId
	O/P:
	Desc: Fetches follow count of user
	----------------------------------------------------------------------------
	*/
	public function followCount( $userId = NULL ){
		$followCount = 0;
		if(!empty($userId)){
			$followCount = $this->find("count", 
								array(
								'joins' =>
								array(
									array(
									'table' => 'users',
									'alias' => 'User',
									'type' => 'left',
									'conditions'=> array('UserFollow.followed_to = User.id')
									),
								),
								"conditions"=> 
									array("UserFollow.followed_by"=> $userId, "UserFollow.follow_type"=> 1, "UserFollow.status"=> 1, 'User.status'=> 1, 'User.approved'=> 1)
								)
								);
		}
		return $followCount;
	}

	/*
	----------------------------------------------------------------------------
	On: 29-03-2016
	I/P: $userId
	O/P:
	Desc: Fetches following count of user
	----------------------------------------------------------------------------
	*/
	public function followingCount( $userId = NULL ){
		$followingCount = 0;
		if(!empty($userId)){
			$followingCount = $this->find("count",
								array(
								'joins' => 
									array(
										array(
										'table' => 'users',
										'alias' => 'User',
										'type' => 'left',
										'conditions'=> array('UserFollow.followed_by = User.id')
										),
									),
								"conditions"=> 
									array("UserFollow.followed_to"=> $userId, "UserFollow.follow_type"=> 1, "UserFollow.status"=> 1, 'User.status'=> 1, 'User.approved'=> 1)
								 )
								);
		}
		return $followingCount;
	}

	/*
    --------------------------------------------------------------
	On: 22-04-2016
	I/P: 
	O/P: 
	Desc: count of follow lists.
	--------------------------------------------------------------
    */
    public function followListsCount( $userId = NULL, $followId = NULL, $sortBy = 'latest' ){ 	
		$followListDataCount = 0;
		//** If followId set fetch all follow users > followId
		if( !empty($followId) ){
			if( isset($sortBy) && $sortBy == 'previous'){
				$conditions = array('UserFollow.followed_to'=> $userId, 'UserFollow.follow_type'=> 1, 'UserFollow.status'=> 1,'UserFollow.id < '=> $followId, 'User.status'=> 1, 'User.approved'=> 1);
			}elseif(isset($sortBy) && $sortBy == 'latest'){
				$conditions = array('UserFollow.followed_to'=> $userId, 'UserFollow.follow_type'=> 1, 'UserFollow.status'=> 1,'UserFollow.id > '=> $followId, 'User.status'=> 1, 'User.approved'=> 1);
			}
		}else{
				$conditions = array('UserFollow.followed_to'=> $userId, 'UserFollow.follow_type'=> 1, 'UserFollow.status'=> 1, 'User.status'=> 1, 'User.approved'=> 1);
		}
		$options =array(             
			'joins' =>
				array(
					array(
						'table' => 'users',
						'alias' => 'User',
						'type' => 'left',
						//'foreignKey' => 'user_id',
						'conditions'=> array('UserFollow.followed_by = User.id')
					),
					array(
						'table' => 'user_profiles',
						'alias' => 'UserProfile',
						'type' => 'left',
						'foreignKey' => 'user_id',
						'conditions'=> array('UserFollow.followed_by = UserProfile.user_id')
					),
					array(
						'table' => 'user_institutions',
						'alias' => 'UserInstitution',
						'type' => 'left',
						'conditions'=> array('UserInstitution.user_id = UserProfile.user_id')
					),
				),
			'conditions'=> $conditions 
		);
		$followListDataCount = $this->find('count', $options);
		return $followListDataCount	;
	}

	/*
    --------------------------------------------------------------
	On: 22-04-2016
	I/P: 
	O/P: 
	Desc: Output all following users count.
	--------------------------------------------------------------
    */
    public function followingListsCount( $userId = NULL, $followId = NULL, $sortBy = 'latest' ){ 	
		$followingListDataCount = 0;
		//** If followId set fetch all follow users > followId
		if( !empty($followId) ){
			if( isset($sortBy) && $sortBy == 'previous'){
				$conditions = array('UserFollow.followed_by'=> $userId, 'UserFollow.follow_type'=> 1, 'UserFollow.status'=> 1,'UserFollow.id < '=> $followId, 'User.status'=> 1, 'User.approved'=> 1);
			}elseif(isset($sortBy) && $sortBy == 'latest'){
				$conditions = array('UserFollow.followed_by'=> $userId, 'UserFollow.follow_type'=> 1, 'UserFollow.status'=> 1,'UserFollow.id > '=> $followId, 'User.status'=> 1, 'User.approved'=> 1);
			}
		}else{
				$conditions = array('UserFollow.followed_by'=> $userId, 'UserFollow.follow_type'=> 1, 'UserFollow.status'=> 1, 'User.status'=> 1, 'User.approved'=> 1);
		}
		$options =array(             
			'joins' =>
				array(
					array(
						'table' => 'users',
						'alias' => 'User',
						'type' => 'left',
						//'foreignKey' => 'user_id',
						'conditions'=> array('UserFollow.followed_to = User.id')
					),
					array(
						'table' => 'user_profiles',
						'alias' => 'UserProfile',
						'type' => 'left',
						'foreignKey' => 'user_id',
						'conditions'=> array('UserFollow.followed_to = UserProfile.user_id')
					),
					array(
						'table' => 'user_institutions',
						'alias' => 'UserInstitution',
						'type' => 'left',
						'conditions'=> array('UserInstitution.user_id = UserProfile.user_id')
					),
				),
			'conditions'=> $conditions,  
		);
		$followingListDataCount = $this->find('count', $options);
		return $followingListDataCount;		
	}
}
?>