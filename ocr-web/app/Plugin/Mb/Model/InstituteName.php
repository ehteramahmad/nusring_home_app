<?php 
/*
Represent model for institute name.
*/
App::uses('AppModel', 'Model');

class InstituteName extends AppModel {
	
	/*
	--------------------------------------------------------------------------
	On: 
	I/P: 
	O/P: 
	Desc: Displays all institute names.
	--------------------------------------------------------------------------
	*/
	public function instituteLists( $params = array() ){
		$instituteLists = array();
		if( !empty($params) ){
			$pageNum = isset($params['page_number']) ? $params['page_number'] : 1;
			$pageSize = isset($params['size']) ? $params['size'] : DEFAULT_PAGE_SIZE;
			$offsetVal = ( $pageNum - 1 ) * $pageSize;
			$conditions = " status = 1 AND created_by = 0 ";
			if(!empty($params['searchText'])){
				if(strlen($conditions) >0){
					$conditions .= ' AND LOWER(institute_name) LIKE mysql_real_escape_string(LOWER("'.$params['searchText'].'%")) ';
				}
			}
			$instituteLists = $this->find("all", array(
										"conditions"=> $conditions,
										"order"=> array("institute_name"),
										'limit'=> $pageSize,
										'offset'=> $offsetVal
										)
									);
		}
		return $instituteLists;
	}

}
?>