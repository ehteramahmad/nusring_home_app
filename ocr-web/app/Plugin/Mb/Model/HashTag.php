<?php 
/*
 Represents model for hash tags.
*/
App::uses('AppModel', 'Model');

class HashTag extends AppModel {
	public $name = 'HashTag';

	/*
	---------------------------------------------------------------------------------
	On: 
	I/P: 
	O/P: 
	Desc: Fetch all hash tag OR matching search.
	---------------------------------------------------------------------------------
	*/
	public function hashLists( $pageSize = DEFAULT_PAGE_SIZE, $pageNum = 1, $params = array() ){
		$hashTags = array();
		$offsetVal = ( $pageNum - 1 ) * $pageSize;
		$conditions = " HashTag.status = 1 ";
		if( !empty($params["searchText"]) ){
			$conditions = ' HashTag.status = 1 AND LOWER(HashTag.name) LIKE LOWER("'.$params['searchText'].'%") ORDER BY HashTag.name ';
		}
		$options = array(
						'conditions'=> $conditions,
						'limit'=> $pageSize,
						'offset'=> $offsetVal 
					);
			$hashTags = $this->find('all', $options);
		return $hashTags;
	}
}
?>