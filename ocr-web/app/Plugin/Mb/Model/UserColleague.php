<?php 
/*
UserColleague model to represent colleague users.
*/
App::uses('AppModel', 'Model');

class UserColleague extends AppModel {

	/*
	---------------------------------------------------------------------------------------
	On:
	I/P:
	O/P:
	Desc:
	---------------------------------------------------------------------------------------
	*/
	public function colleagueLists( $pageSize = DEFAULT_PAGE_SIZE, $pageNum = 1 , $userId = NULL){
		$userColleagues = array();
		$offsetVal = ( $pageNum - 1 ) * $pageSize;
		//** If userId set show all colleagues of the particular user
		$conditions = '';
		if( !empty($userId) ){
			$conditions = array('UserColleague.user_id'=> $userId, 'User.status'=> 1);
		}
		$options =array(             
			'joins' =>
				array(
					array(
						'table' => 'users',
						'alias' => 'User',
						'type' => 'inner',
						'conditions'=> array('User.id = UserColleague.colleague_user_id')
					),
					array(
						'table' => 'user_profiles',
						'alias' => 'UserProfile',
						'type' => 'left',
						'foreignKey' => 'user_id',
						'conditions'=> array('UserColleague.colleague_user_id = UserProfile.user_id')
					),
				),
			'fields'=> array('UserColleague.id, UserColleague.user_id, UserColleague.colleague_user_id, UserColleague.status, UserColleague.created, User.id, User.status, UserProfile.user_id, UserProfile.first_name, UserProfile.last_name, UserProfile.profile_img'),
			'conditions'=> $conditions,
			//'group'=> array('UserColleague.id'),
			'order'=> array('UserColleague.created DESC'),
			'limit'=> $pageSize,
			'offset'=> $offsetVal  
		);
		$userColleagues = $this->find('all', $options);
		return $userColleagues;
	}

	/*
	---------------------------------------------------------------------------------------
	On: 01-08-2016
	I/P: $userId, $colleagueUserId
	O/P: true/false
	Desc: Check if both users are colleague or not
	---------------------------------------------------------------------------------------
	*/
	public function isColleagueCheck($userId = NULL, $colleagueUserId = NULL){
		$iscolleagueCheckCount = 1;
		if(!empty($userId) && !empty($colleagueUserId)){
			$conditions = array(
								"OR"=> array(
									array("user_id"=> $userId, "colleague_user_id"=> $colleagueUserId),
									array("colleague_user_id"=> $userId, "user_id"=> $colleagueUserId)
								)
							);
			$iscolleagueCheckCount = $this->find("count", array("conditions"=> $conditions));
			//if($iscolleagueCheckCount > 0){
				//$retutnVal = true;
			//}
		}
		return $iscolleagueCheckCount;
	}

	/*
	---------------------------------------------------------------------------------------
	On: 15-09-2017
	I/P: $userId
	O/P: Pending Colleague List
	Desc: Pending Coleague List
	---------------------------------------------------------------------------------------
	*/
	public function getPendingColleague($params=array()){
		$pendingColleagueLists = array();
		if(!empty($params['loggedin_user_id'])){
			$loggedinUser = $params['loggedin_user_id'];
			$quer = "SELECT `User`.`id`, `User`.`email`, `UserProfile`.`first_name`, `UserProfile`.`last_name`, 
			 `UserProfile`.`profession_id`, `UserProfile`.`profile_img`, `UserProfile`.`role_status`,`UserDutyLog`.`status`,`UserDutyLog`.`atwork_status`     
			  FROM `user_colleagues` AS `UserColleague` INNER JOIN `users` AS `User` ON `User`.`id`= `UserColleague`.`user_id` INNER JOIN `user_profiles` AS `UserProfile` ON `UserProfile`.`user_id` = `User`.`id` LEFT JOIN  `user_duty_logs` `UserDutyLog` ON `User`.`id` = `UserDutyLog`.`user_id`    WHERE `UserColleague`.`colleague_user_id`= $loggedinUser  AND  `UserColleague`.`status`= 2 AND  `User`.`status` = 1 AND `User`.`approved` = 1 ORDER BY `UserProfile`.`first_name`" ;
			   
			$pendingColleagueLists = $this->query($quer);
		}
		return $pendingColleagueLists;
	}
	
}

?>