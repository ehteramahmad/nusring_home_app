<table class="table stylish-table customPading">
    <thead>
        <tr>
            <th style="width: 5%">Photo </th>
            <th style="width: 20%">Name </th>
            <th style="width: 25%">Email </th>
            <th style="width: 15%">Profession </th>
            <th style="width: 15%">Location</th>
            <th style="width: 10%">Message Delievery Type</th>
            <th style="width: 10%">Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php
            if(isset($data['user_list']) && count($data['user_list']) > 0){
            foreach( $data['user_list'] as $user){
        ?>
        <tr id="<?php echo $user['id']; ?>" data-email="<?php echo $user['email']; ?>">
            <td>
                <span class="round <?php echo $user['cust_class']; ?>">
                  <img src="<?php echo $user['profile_img']; ?>" onerror="this.src='<?php echo BASE_URL ?>institution/img/ava-single.png'" alt="" />
                </span>
            </td>
            <td class="userName"><?php echo $user['UserName']; ?></td>
            <td><?php echo $user['email']; ?></td>
            <td><?php echo $user['profession_type']; ?></td>
            <td><?php echo $user['location']; ?></td>
            <td><?php echo ($user['isBroadcast'] == true) ? 'In App' : 'Sms'; ?></td>
            <td><a class="removeUser" data-id="<?php echo $user['id']; ?>">Remove</a></td>
        </tr>
        <?php }}elseif(isset($tCount) && $tCount > 0){?>

        <tr>
           <td colspan="8" align="center"><font color="green">Please wait while we are loading the list...</font></td>
        </tr>
        <?php }else{ ?>
        <tr>
           <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
        </tr>
        <?php  } ?>
    </tbody>
</table>
<span id="totalCount" style="display:none;"><?php echo $tCount; ?></span>
<span id="AllIds" style="display:none;"><?php echo $data['idsData']; ?></span>
<div class="actions marBottom10 paddingSide15 leftFloat98per">

<?php
    if(count($data['user_list']) > 0){
        echo $this->element('pagination');
    }
?>
</div>
