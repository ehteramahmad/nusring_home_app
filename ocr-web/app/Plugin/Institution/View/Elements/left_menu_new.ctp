<?php
$is_admin = 0;
if($_COOKIE['adminRoleId'] == 0){
  $is_admin = 1;
}
 ?>
<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar" style="position: fixed;">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- User profile -->

        <!-- End User profile text-->
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li id="menu1">
                  <a href="<?php echo BASE_URL.'institution/InstitutionHome/index'; ?>" aria-expanded="false">
                      <span style="margin-left: 3px;" class="hide-menu">DASHBOARD</span>
                  </a>
                </li>
                <li id="broadcastMenu">
                  <a href="<?php echo BASE_URL.'institution/InstitutionBroadcast/index'; ?>" aria-expanded="false">
                      <span style="margin-left: 3px;" class="hide-menu">BROADCAST MESSAGE</span>
                  </a>
                </li>
                <li id="menu2">
                  <a href="javascript:;" class="nav-link nav-toggle" aria-expanded="false">
                      <span class="hide-menu">MANAGE</span>
                      <span class="action-icon"></span>
                  </a>
                  <ul class="sub-menu collapse" aria-expanded="false">
                      <li>
                          <a href="<?php echo BASE_URL.'institution/InstitutionHome/users'; ?>" aria-expanded="false">
                              <span class="title">Users</span>
                          </a>
                      </li>
                      <?php if($is_admin == 1){ ?>
                        <li>
                            <a href="<?php echo BASE_URL.'institution/InstitutionGroup/index'; ?>" aria-expanded="false">
                                <span class="title">Groups</span>
                            </a>
                        </li>
                      <?php } ?>
                  </ul>
                </li>
              <?php if($is_admin == 1){ ?>
                <li id="menu3">
                  <a href="javascript:;" class="nav-link nav-toggle" aria-expanded="false">
                      <span class="hide-menu">AUDIT TRAILS</span>
                      <span class="action-icon"></span>
                  </a>
                  <ul class="sub-menu collapse" aria-expanded="false">
                      <li>
                          <a href="<?php echo BASE_URL.'institution/InstitutionAudit/screenShot'; ?>" aria-expanded="false">
                              <span class="title">User Screenshots</span>
                          </a>
                      </li>
                      <li>
                          <a href="<?php echo BASE_URL.'institution/InstitutionAudit/status'; ?>" aria-expanded="false">
                              <span class="title">Available & On-Call History</span>
                          </a>
                      </li>
                  </ul>
                </li>
              <?php } ?>
                <li id="menu4">
                  <a href="javascript:;" class="nav-link nav-toggle" aria-expanded="false">
                      <span class="hide-menu">SETTINGS</span>
                      <span class="action-icon"></span>
                  </a>
                  <ul class="sub-menu collapse" aria-expanded="false">
                    <?php if($is_admin == 1){ ?>
                      <li>
                          <a href="<?php echo BASE_URL.'institution/InstitutionHome/privilegeList'; ?>" aria-expanded="false">
                              <span class="title">Manage Admin</span>
                          </a>
                      </li>
                    <?php } ?>
                      <li>
                          <a href="<?php echo BASE_URL.'institution/InstitutionUser/index'; ?>" aria-expanded="false">
                              <span class="title">Pre-Approved Emails</span>
                          </a>
                      </li>
                      <?php if($is_admin == 1){ ?>
                      <li>
                          <a href="<?php echo BASE_URL.'institution/InstitutionSetting/number'; ?>" aria-expanded="false">
                              <span class="title">NHS/MRN Number</span>
                          </a>
                      </li>
                      <li>
                          <a href="<?php echo BASE_URL.'institution/InstitutionSetting/session'; ?>" aria-expanded="false">
                              <span class="title">Session Time</span>
                          </a>
                      </li>
                      <li>
                          <a href="<?php echo BASE_URL.'institution/InstitutionSetting/baseLocation'; ?>" aria-expanded="false">
                              <span class="title">Base Location</span>
                          </a>
                      </li>
                      <li>
                          <a href="<?php echo BASE_URL.'institution/InstitutionSetting/profession'; ?>" aria-expanded="false">
                              <span class="title">Profession</span>
                          </a>
                      </li>
                    <?php } ?>
                  </ul>
                </li>
                <li id="menu5">
                  <a href="javascript:;" class="nav-link nav-toggle" aria-expanded="false">
                      <span class="hide-menu">REPORTS</span>
                      <span class="action-icon"></span>
                  </a>
                  <ul class="sub-menu collapse" aria-expanded="false">
                    <?php if($is_admin == 1 && ((string)$_COOKIE['adminInstituteId'] == '336')){ ?>
                      <li>
                          <a href="<?php echo BASE_URL.'institution/InstitutionAnalytic/datastudio'; ?>" aria-expanded="false">
                              <span class="title">Data Studio</span>
                          </a>
                      </li>
                      <li>
                          <a href="<?php echo BASE_URL.'institution/InstitutionAnalytic/powerbi'; ?>" aria-expanded="false">
                              <span class="title">Power BI</span>
                          </a>
                      </li>
                    <?php } ?>
                      <li>
                          <a href="<?php echo BASE_URL.'institution/InstitutionAudit/registrationReport'; ?>" aria-expanded="false">
                              <span class="title">User Registration Report</span>
                          </a>
                      </li>
                  </ul>
                </li>
                <?php if($is_admin == 1){ ?>
                <li id="menu6">
                  <a href="<?php echo BASE_URL.'institution/Switchboard'; ?>" aria-expanded="false">
                      <span class="hide-menu">Switch Board
                          <?php
                              $custBatonClass = 'none';
                                if(isset($baton_count) && $baton_count > 0){
                                  $custBatonClass = 'inline-block';
                                }
                          ?>
                          <span class="badge badge-successNew2 batonBadgeCount" id="baton_request_count_latest" style="display:<?php echo $custBatonClass; ?>"><?php echo $baton_count; ?></span>
                        </span>
                      <!-- arrow-pointing-to-right.svg -->
                      <span class="action-icon switchboard"></span>
                  </a>
                </li>
              <?php } ?>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
