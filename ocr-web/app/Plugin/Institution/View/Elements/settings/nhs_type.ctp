<div id="nhsLoader"></div>
<div class="card">
  <div class="card-block padLeftRight0">
     <div class="floatLeftFull padLeftRight14">
        <div class="tableTop floatLeft">
           <span class="allIconUsers">
             <img src="<?php echo BASE_URL ?>institution/img/sub-patient.svg" alt="">
           </span>
           <span class="card-title">Patient NHS/MRN Type</span>
        </div>
     </div>
     <div class="table-responsive wordBreak">
        <table class="table stylish-table customPading">
           <thead>
              <tr>
                 <th>Current Status</th>
                 <th>Action</th>
              </tr>
           </thead>
           <tbody>
              <tr>
                <td>
                 <?php
                   if($data['CompanyName']['patient_number_type']=='1'){
                     echo "MRN Number";
                   }else{
                     echo "NHS Number";
                   }
                 ?>
                </td>
                <td>
                 <?php
                   $type = array(
                     0=>'NHS Number',
                     1=>'MRN Number'
                   );
                   echo $this->Form->select('Type', $options = $type, $attributes = array('empty'=>'Select','value'=>$data['CompanyName']['patient_number_type'],'id'=>'patient_nType', 'class'=>'bs-select form-control width200 font13'));;
                 ?>
                </td>
              </tr>
           </tbody>
        </table>
     </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  $('#patient_nType').on('change',function(){
    var Current = <?php echo $data['CompanyName']['patient_number_type'] ? $data['CompanyName']['patient_number_type'] : 0; ?>;
    var text = '';
    if($(this).val().toString() == Current.toString()){
      return;
    }
    if($(this).val() == ''){
      var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal" onclick="cancelType();">Close</button>';

      $('#customModalLabel').html('Alert');
      $('#customModalBody').html('Please select NHS or MRN Number.');
      $('#customModalFoot').html(buttonHtml);
      $('#customBox').modal('show');
      return;
    }else{
      if(parseInt($(this).val()) == 1){
        text = 'MRN Number';
      }else{
        text = 'NHS Number';
      }
      var buttonHtml = '<button type="button" class="btn btn-info waves-effect " data-dismiss="modal" onclick="setType('+$(this).val()+');" >Update</button>';
      buttonHtml += '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal" onclick="cancelType();">Cancel</button>';

      $('#customModalLabel').html('Confirmation');
      $('#customModalBody').html('Are you sure you want to set '+text+' as default patient number type?');
      $('#customModalFoot').html(buttonHtml);
      $('#customBox').modal('show');
    }
  });
  function cancelType(){
    var Current = <?php echo $data['CompanyName']['patient_number_type'] ? $data['CompanyName']['patient_number_type'] : 0; ?>;
    $('#patient_nType').val(Current);
  }
});
</script>
