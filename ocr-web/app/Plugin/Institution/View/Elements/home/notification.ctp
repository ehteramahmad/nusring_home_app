<a class="nav-link dropdown-toggle text-muted text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <i class="mdi mdi-bell-outline floatLeft"></i>
    <?php
        if(isset($count) && $count > 0){
    ?>
        <span class="badge badge-successNew2"> <?php echo $count; ?> </span>
    <?php
        }
    ?>
</a>
<div class="dropdown-menu mailbox animated bounceInDown">
    <ul>
        <li>
            <div class="drop-title">Notifications</div>
            <?php
                if(isset($count) && $count > 0){
            ?>
            <div class="drop-title allSelect">
                <span class="newInput">
                    <a id="selectAllNotif">All</a>
                </span>
                <div class="actionBtN floatRight">
                    <button disabled="disabled" id="assignAllNotif" type="button" class="btn btn-outline-primary">Approve Selected</button>
                </div>
            </div>
            <?php } ?>
        </li>
        <li>
            <div class="message-center">
                <?php
                    if(isset($user) && count($user) > 0){
                    foreach( $user as $users){
                ?>
                <!-- Message -->
                <a>
                    <span class="newInputSmall">
                        <input class="assignCheck" data-user="<?php echo $users['id']; ?>" data-email="<?php echo $users['email']; ?>" aria-label="Checkbox for following text input" type="checkbox">
                    </span>
                    <div class="user-img">
                        <span class="round">
                          <img 'width'='50' 'height'='50' src="<?php echo $users['profile_img']; ?>" onerror="this.src='<?php echo BASE_URL ?>institution/img/ava-single.png'" alt="" />
                        </span>
                    </div>
                    <div class="mail-contnet">
                        <h5 style="word-break: break-all;"><?php echo $users['UserName']; ?></h5>
                        <span class="mail-desc"><?php echo $users['email']; ?></span>
                        <span class="time"><?php echo $users['profession_type']; ?></span>
                    </div>
                    <div class="actionBtN">
                      <button onclick="assignUser('<?php echo addslashes($users['email']); ?>', '<?php echo $users['id']; ?>',1);" type="button" class="btn btn-outline-primary">Approve</button>
                    </div>
                </a>
                <?php
                    }}else{
                ?>
                <div class="mail-contnet txtCenter">
                    <?php echo $this->Html->image('Institution.notificationNoRecordFound.png',array('class'=>'verticalCenter')); ?>
                    <span class="txtCenter txtSmallNofication leftFloatFull">No New Notifications!</span>
                </div>
                <?php
                    }
                ?>
            </div>
        </li>
        <li>
            <a class="nav-link text-center" href="<?php echo BASE_URL; ?>institution/InstitutionHome/users/0"> <strong>Check all notifications</strong> <i class="fa fa-angle-right"></i> </a>
        </li>
    </ul>
</div>
