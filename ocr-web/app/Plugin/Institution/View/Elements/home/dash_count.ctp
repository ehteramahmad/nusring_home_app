<div class="col-lg-3 col-md-6">
    <div class="card" data-value="all">
        <div class="card-block" onclick="getListing('all');">
            <!-- Row -->
            <div class="row">
                <div class="col-8"><span class="display-6"><?php echo isset($data['all_users'])?$data['all_users']:0; ?></span>
                    <h6>All Users</h6></div>
                <div class="col-4 align-self-center text-right  p-l-0">
                    <div id="sparklinedash3">
                        <?php echo $this->Html->image('Institution.widget1.svg'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Column -->
<div class="col-lg-3 col-md-6">
    <div class="card" data-value="0">
        <div class="card-block" onclick="getListing(0);">
            <!-- Row -->
            <div class="row">
                <div class="col-8"><span class="display-6"><?php echo isset($data['pending_users'])?$data['pending_users']:0; ?></span>
                    <h6>Awaiting Approval</h6></div>
                <div class="col-4 align-self-center text-right p-l-0">
                    <div id="sparklinedash">
                        <?php echo $this->Html->image('Institution.widget2.svg'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Column -->
<div class="col-lg-3 col-md-6">
    <div class="card" data-value="1">
        <div class="card-block" onclick="getListing(1);">
            <!-- Row -->
            <div class="row">
                <div class="col-8"><span class="display-6"><?php echo isset($data['approved_users'])?$data['approved_users']:0; ?></span>
                    <h6>Approved Users</h6></div>
                <div class="col-4 align-self-center text-right p-l-0">
                    <div id="sparklinedash2">
                        <?php echo $this->Html->image('Institution.widget3.svg'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Column -->
<div class="col-lg-3 col-md-6">
    <div class="card" data-value="7">
        <div class="card-block" onclick="getListing(7);">
            <!-- Row -->
            <div class="row">
                <div class="col-8"><span class="display-6"><?php echo isset($data['new_users'])?$data['new_users']:0; ?></span>
                    <h6>Recently Registered Users</h6></div>
                <div class="col-4 align-self-center text-right p-l-0">
                    <div id="sparklinedash4">
                        <?php echo $this->Html->image('Institution.widget4.svg'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
