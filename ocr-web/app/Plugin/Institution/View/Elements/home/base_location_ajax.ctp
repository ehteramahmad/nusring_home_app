<table class="table stylish-table customPading">
  <thead>
      <tr>
        <th style="width:60%;">Base Location </th>
        <th style="width:30%;">Status</th>
        <th style="width:10%;">Actions</th>
      </tr>
  </thead>
  <tbody>
      <?php
          if(isset($data['base_location_list']) && count($data['base_location_list']) > 0){
          foreach( $data['base_location_list'] as $baseLocation){
      ?>
      <tr>
          <td style="width:25%;" data-id="<?php echo $baseLocation['RoleTag']['id']; ?>"><?php echo $baseLocation['RoleTag']['value']; ?></td>
          <td class="statusBtn" style="width:25%;">
            <?php if($baseLocation['RoleTag']['status']==1){ ?>
                     <button onclick="changeStatus('<?php echo $baseLocation['RoleTag']['id']; ?>', 0);" type="button" class="btn btn-primary"> Active </button>
            <?php }else{ ?>
                       <button onclick="changeStatus('<?php echo $baseLocation['RoleTag']['id']; ?>', 1);" type="button" class="btn btn-outline-primary"> Inactive </button>
            <?php }
              ?>
          </td>
          <td style="text-align: center;">
            <div class="btn-group">
                <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="ti-settings"></i>
                </button>
                <div class="dropdown-menu pad0">
                  <a class="dropdown-item" href="" data-toggle="modal" data-target="#updateBase" data-id="<?php echo $baseLocation['RoleTag']['id']; ?>">Edit Name</a>
                  <a onclick="removeBaseLocation('<?php echo $baseLocation['RoleTag']['id']; ?>')" class="dropdown-item" href="javascript:void(0)">Remove</a>
                </div>
            </div>
          </td>
      </tr>
      <?php }}elseif(isset($tCount) && $tCount > 0){?>

      <tr>
         <td colspan="8" align="center"><font color="green">Please wait while we are loading the list...</font></td>
      </tr>
      <?php }else{ ?>
      <tr>
         <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
      </tr>
      <?php  } ?>
  </tbody>
</table>
<span id="totalCount" class="pull-right" style="display:none;"><?php echo $tCount; ?></span>
<div class="actions marBottom10 paddingSide15 leftFloat98per">
<?php
    if(isset($data['base_location_list']) && count($data['base_location_list']) > 0){
        echo $this->element('pagination');
    }
?>
</div>
