<?php

// print_r($data);
// exit;

$graph1_data = array();
$graph2_data = array();

  ksort($data['professions']);
  foreach ($data['professions'] as $key1 => $value_graph1) {
    $graph1 = array();
    $graph1['y'] = $value_graph1;
    $graph1['legendText'] = $key1." : ".$value_graph1;
    $graph1['label'] = $key1.":".$value_graph1;
    $graph1_data[] = $graph1;
  }
  $graph2_data = array(
    array('y'=>((int)$data['total_users']),'label'=>'All Users',"color"=> "#9fa9ba"),
    array('y'=>(int)$data['available_users'],'label'=>'Available',"color"=> "#76ddfb"),
    array('y'=>(int)$data['oncall_users'],'label'=>'On Call',"color"=> "#b4e18b")
  );

 ?>
<div class="row boaderLines widgetMain dashCounts">
    <!-- Column -->
    <div class="col-lg-3 col-md-6">
        <div class="card" data-value="all">
            <div class="card-block" onclick="getListing('all');">
                <!-- Row -->
                <div class="row">
                    <div class="col-12"><span class="display-6"><?php echo isset($data['all_users'])?$data['all_users']:0; ?></span>
                        <h6>All Users</h6>
                      </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
    <div class="col-lg-3 col-md-6">
        <div class="card" data-value="0">
            <div class="card-block" onclick="getListing(0);">
                <!-- Row -->
                <div class="row">
                    <div class="col-12"><span class="display-6"><?php echo isset($data['pending_users'])?$data['pending_users']:0; ?></span>
                        <h6>Awaiting Approval</h6>
                      </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
    <div class="col-lg-3 col-md-6">
        <div class="card" data-value="1">
            <div class="card-block" onclick="getListing(1);">
                <!-- Row -->
                <div class="row">
                    <div class="col-12"><span class="display-6"><?php echo isset($data['approved_users'])?$data['approved_users']:0; ?></span>
                        <h6>Approved Users</h6>
                      </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
    <div class="col-lg-3 col-md-6">
        <div class="card" data-value="7">
            <div class="card-block" onclick="getListing(7);">
                <!-- Row -->
                <div class="row">
                    <div class="col-12"><span class="display-6"><?php echo isset($data['new_users'])?$data['new_users']:0; ?></span>
                        <h6>Recently Registered Users</h6>
                      </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Row -->
<div class="row boaderLines graph-block">
    <div class="col-md-7 ">
      <div class="card">
          <div class="card-block">
              <div class="col-12 header">
                <h3>Currently Active Professionals: <?php echo (int)$data['total_users']; ?></h3>
              </div>
              <div class="col-12">
                <div id="chartContainer1"></div>
              </div>
          </div>
      </div>
    </div>
    <div class="col-md-5 ">
      <div class="card">
        <div class="card-block">
            <div class="col-12 header">
              <h3>Currently Signed In Users: <?php echo (int)$data['total_users']; ?></h3>
            </div>
            <div class="col-12">
              <div id="chartContainer2"></div>
            </div>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
CanvasJS.addColorSet("blueShades",[
                      "#2078a5",
                      "#2688bd",
                      "#3b9fd5",
                      "#53b5e2",
                      "#10a5da",
                      "#2bb5ff",
                      "#47bae2",
                      "#2dccfa",
                      "#76ddfb",
                      "#d2e3f2",
                      "#3db4f4",
                      "#02bbf1",
                      "#8ddaf6",
                      "#dbecf8",
                      "#2c82be",
                      "#53a8e2",
                      "#76ddfb"
                    ]);
  data1 = <?php echo json_encode($graph1_data); ?>;
  data2 = <?php echo json_encode($graph2_data); ?>;
  console.log(data1.length);
  if(data1.length > 0){
    $('#chartContainer1,#chartContainer2').css('height',(data1.length * 10 + 200)+'px');
    var chart = new CanvasJS.Chart("chartContainer1", {
    animationEnabled: true,
    colorSet: "blueShades",
    legend: {
      horizontalAlign: "right",
      verticalAlign: "center",
      fontSize: 13,
      fontFamily: "'Open Sans', sans-serif",
      fontWeight: 600,
      fontColor: "#485465",
      lineHeight: 19
    },
    toolTip:{
      animationEnabled: true       //enable here
    },
    data: [{
      type: "doughnut",
      theme: "light2",
      startAngle: 270,
      innerRadius: 55,
      indexLabelFontSize: 11,
      indexLabel: "#percent%",
      showInLegend: true,
      indexLabelPlacement: "inside",
      indexLabelFontColor: "white",
      indexLabelFontFamily: "'Open Sans', sans-serif",
      toolTipContent: "<b>{label}:</b> {y} (#percent%)",
      // dataPoints: [
      //   { y: 67, legendText: "Inbox", label: "Inbox",exploded: true },
      //   { y: 28, legendText: "Archives", label: "Archives" },
      //   { y: 10, legendText: "Labels", label: "Labels" },
      //   { y: 60, legendText: "Spam", label: "Spam"},
      //   { y: 72, legendText: "Drafts", label: "Drafts"},
      //   { y: 15, legendText: "shjdvsjhd shjd jhsd hjsd sahjd asjhd jsahd jsa", label: "shjdvsjhd shjd jhsd hjsd sahjd asjhd jsahd jsa"}
      // ]
      dataPoints: data1
    }]
    });
    chart.render();
  }else{
    $('#chartContainer1').html('<img class="pie_def" src="<?php echo BASE_URL ?>institution/img/pie.png" alt="">');
  }

  var chart2 = new CanvasJS.Chart("chartContainer2", {
  animationEnabled: true,
  theme: "theme2", // "light1", "light2", "dark1", "dark2"
  dataPointMaxWidth: 80,
  colorSet: "blueShades",
  toolTip:{
    animationEnabled: true       //enable here
  },
  data: [{
    type: "column",
    indexLabel:  "{y} Users",
    indexLabelMaxWidth: 60,
    indexLabelWrap: true,
    indexLabelPlacement: 'inside',
    indexLabelFontFamily: "'Open Sans', sans-serif",
    indexLabelFontSize: 19,
    indexLabelFontWeight: "bold",
    indexLabelLineDashType: "light",
    indexLabelLineThickness: 0,
    indexLabelFontColor: "white",
    // dataPoints: [
    //   { y: 10, label: "All Users", color: "#9fa9ba" },
    //   { y: 20, label: "Available", color: "#76ddfb" },
    //   { y: 30, label: "On Call", color: "#b4e18b" }
    // ]
    dataPoints: data2
  }]
  });
  chart2.render();
</script>
