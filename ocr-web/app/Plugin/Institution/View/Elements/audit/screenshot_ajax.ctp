<?php
$type = "outer";
$width_per = '20%';
if(isset($user_id) && (int)$user_id > 0){
  $type = "inner";
  $width_per = '33%';
}
$user_country_id = $_COOKIE['adminCountryId'];
 ?>
<table class="table stylish-table customPading">
    <thead>
        <tr>
            <?php if($type == 'outer'){ ?>
              <th style="width:<?php echo $width_per; ?>;">Name</th>
              <th style="width:<?php echo $width_per; ?>;">Email</th>
            <?php } ?>
            <th style="width:<?php echo $width_per; ?>;padding-left:45px;">Chat Name</th>
            <th style="width:<?php echo $width_per; ?>;text-align:center;">Date</th>
            <th style="width:<?php echo $width_per; ?>;text-align:center;">Time</th>
        </tr>
    </thead>
    <tbody>
        <?php
            if(isset($data['user_list']) && count($data['user_list']) > 0){
            foreach( $data['user_list'] as $user){
              date_default_timezone_set('UTC');
              $date = strtotime($user['ScreenShotTransaction']['created']);
              if($user_country_id==226){
                  // date_default_timezone_set('Europe/Amsterdam');
                  date_default_timezone_set("Europe/London");
              }else if($user_country_id==99){
                  date_default_timezone_set('Asia/Kolkata');
              }
        ?>
        <tr>
            <?php if($type == 'outer'){ ?>
              <td style="width:<?php echo $width_per; ?>;"><?php echo $user['userProfiles']['first_name'].' '.$user['userProfiles']['last_name']; ?></td>
              <td style="width:<?php echo $width_per; ?>;"><?php echo $user['Users']['email']; ?></td>
            <?php } ?>
            <td style="width:<?php echo $width_per; ?>;padding-left:45px;"><?php echo $user['ScreenShotTransaction']['dialog_name']; ?></td>
            <td style="width:<?php echo $width_per; ?>;text-align:center;"><?php echo date('d M Y', $date); ?></td>
            <td style="width:<?php echo $width_per; ?>;text-align:center;"><?php echo date('H:i', $date); ?></td>
        </tr>
        <?php }}elseif(isset($tCount) && $tCount > 0){?>

        <tr>
           <td colspan="8" align="center"><font color="green">Please wait while we are loading the list...</font></td>
        </tr>
        <?php }else{ ?>
        <tr>
           <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
        </tr>
        <?php  } ?>
    </tbody>
</table>
<!-- <span class="pull-right">Total Count : <?php //echo isset($tCount)? $tCount:0; ?></span> -->
<?php
  if(isset($section) && $section == 'all'){
    if(isset($data['user_list']) && count($data['user_list']) > 0){
      echo '<div class="actions marBottom10 paddingSide15 leftFloat98per">';
        echo $this->element('pagination');
      echo "</div>";
    }
  }else if(isset($data['user_list']) && count($data['user_list']) > 0){
     ?>
    <div class="card-note">
      <label>Note</label>
      <div class="note-des">
        Click on view all button, to view list of User’s Screenshot Transactions.
      </div>
    </div>
    <button type="button" onclick="window.location.href='<?php echo BASE_URL.'institution/InstitutionAudit/screenShot/'.$user_id; ?>'" name="button" class="btn btn-success">View All</button>
  <?php } ?>
</div>
