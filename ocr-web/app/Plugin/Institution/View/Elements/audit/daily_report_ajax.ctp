<?php
if(!empty($data)){ ?>
  <span class="is-hidden" id="report_count"><?php echo isset($totalCount)?$totalCount:0; ?></span>
  <table class="table stylish-tableNew">
      <thead>
        <tr>
            <th>Base Location </th>
            <th style="text-align:center;">Total Count </th>
        </tr>
      </thead>
      <tbody>
          <?php
              foreach ($data as $key => $list) {
          ?>
          <tr onclick="showData('<?php echo "#".$key; ?>');" data-target="<?php echo "#".$key; ?>">
              <td style="width:50%;"><?php echo $list['key']; ?></span></td>
              <td style="width:50%; text-align:center;"><?php echo $list['count']; ?><span class="openSpan"><i class="demo-icon icon-angle-right"></i></span></td>
          </tr>
          <tr id="<?php echo $key; ?>" class="collapse active transition_div">
            <td colspan="8">
              <div style="padding-left: 0;padding-right:0px;" class="col-12 panel-body">
                    <!-- <div class="card"> -->
                        <!-- <div class="card-block padTop30 padBottom0"> -->
                            <div class="table-responsive wordBreak panel-body">
                                <table class="table stylish-tableNew panel-body">
                                    <thead>
                                      <tr class="panel-body">
                                          <th class="panel-body">Profession </th>
                                          <th style="padding-left:0px; text-align: center;" class="panel-body">Count </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            foreach( $list['list'] as $user){
                                        ?>
                                        <tr class="panel-body">
                                            <td class="panel-body" style="width:50%; border-top: 1px solid #E9E9E9 !important;"><?php echo $user['profession']; ?></span></td>
                                            <td class="panel-body" style="width:40%; border-top: 1px solid #E9E9E9 !important; padding-left:0px; text-align: center;"><?php echo $user['count']; ?></td>
                                        </tr>
                                        <?php }?>
                                    </tbody>
                                </table>
                          <!-- </div> -->
                        <!-- </div> -->
                    </div>
                </div>
            </td>
          </tr>
          <?php }?>
      </tbody>
  </table>
      <?php
}else{ ?>
  <span class="is-hidden" id="report_count"><?php echo isset($totalCount)?$totalCount:0; ?></span>
  <table class="table stylish-tableNew">
      <thead>
        <tr>
            <th>Base Location </th>
            <th style="text-align:center;">Total Count </th>
        </tr>
      </thead>
      <tbody>
        <tr>
            <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
        </tr>
      </tbody>
  </table>
<?php }
?>
<script type="text/javascript">
function exportAll(){
  var list = <?php echo json_encode(serialize($data));  ?>;
  var condition = <?php echo json_encode($condition);  ?>;
  var total = <?php echo $totalCount; ?>;
  var from = to = '';
  if(condition['from_date']){
    from = condition['from_date'];
    to = condition['to'];
  }
  $.ajax({
      url: '<?php echo BASE_URL; ?>institution/InstitutionAudit/exportReport',
      type: 'POST',
      data: {'list':list,'from_date':from,'to':to,'total':total},
      success: function( result ){
        console.log(result);
        window.open(result);
        $('.loader').fadeOut('fast');
      },
      error: function( result ){

      }
  });
}
function showData(id){
  if($('#ajaxContent tr[data-target="'+id+'"]').attr('aria-expanded')=='true'){
    $('#ajaxContent tr').attr('aria-expanded','false');
    $('#ajaxContent tr.transition_div').removeClass('show').attr('aria-expanded','false');
  }else{
    $('#ajaxContent tr').attr('aria-expanded','false');
    $('#ajaxContent tr.transition_div').removeClass('show').attr('aria-expanded','false');
    // id = $(this).data('target');
    $('#ajaxContent tr[data-target="'+id+'"]').attr('aria-expanded','true');
    $(id).addClass('show').attr('aria-expanded','true');
  }
}
</script>
