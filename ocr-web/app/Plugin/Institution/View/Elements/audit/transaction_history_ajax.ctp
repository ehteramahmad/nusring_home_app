<table class="table stylish-table customPading">
    <thead>
        <tr>
            <th style="width:20%; text-align:center;">Type </th>
            <th style="width:20%; text-align:center;">Status </th>
            <th style="width:20%; text-align:center;">Platform </th>
            <th style="width:20%; text-align:center;">Modified Date</th>
            <th style="width:20%; text-align:center;">Modified Time</th>
        </tr>
    </thead>
    <tbody>
        <?php
            if(isset($data['user_list']) && count($data['user_list']) > 0){
            foreach( $data['user_list'] as $user){
            	$device = 'Android';
            	if($user['AvailableAndOncallTransaction']['device_type'] == 'MBWEB'){
            		$device = 'Web';
            	}else{
            		if($user['AvailableAndOncallTransaction']['device_type'] == 'iOS'){
            			$device = 'iOS';
            		}else{
            			$device = 'Android';
            		}
            	}

              $user_country_id = $_COOKIE['adminCountryId'];
              date_default_timezone_set('UTC');
              $date_cust = strtotime($user['AvailableAndOncallTransaction']['created']);
              if($user_country_id==226){
                  date_default_timezone_set("Europe/London");
              }else if($user_country_id==99){
                  date_default_timezone_set('Asia/Kolkata');
              }
        ?>
        <tr>
            <td style="width:20%; text-align:center;"><?php echo $user['AvailableAndOncallTransaction']['type']; ?></td>
            <td style="width:20%; text-align:center;"><?php echo $user['AvailableAndOncallTransaction']['status'] == 1?'<span style="color:#00a777;">On</span>':'<span style="color:#d0021b;">Off</span>'; ?></td>
            <td style="width:20%; text-align:center;"><?php echo $user['AvailableAndOncallTransaction']['platform'] == 'web'?'Admin Pannel':'APP ('.$device.')'; ?></td>
            <td style="width:20%; text-align:center;"><?php echo date("d-m-Y", $date_cust); ?></td>
            <td style="width:20%; text-align:center;"><?php echo date("H:i", $date_cust); ?></td>
        </tr>
        <?php }}elseif(isset($tCount) && $tCount > 0){?>

        <tr>
           <td colspan="8" align="center"><font color="green">Please wait while we are loading the list...</font></td>
        </tr>
        <?php }else{ ?>
        <tr>
           <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
        </tr>
        <?php  } ?>
    </tbody>
</table>
<span id="totalCount" class="pull-right" style="display:none;"><?php echo $tCount; ?></span>
<div class="actions marBottom10 paddingSide15 leftFloat98per">
<?php
  if(isset($section) && $section == 'all'){
    if(isset($data['user_list']) && count($data['user_list']) > 0){
        echo $this->element('pagination');
    }
  }else if(isset($data['user_list']) && count($data['user_list']) > 0){ ?>
    <div class="card-note">
      <label>Note</label>
      <div class="note-des">
        Click on view all button, to view list of User’s OnCall and Available Status.
      </div>
    </div>
    <button type="button" onclick="window.location.href='<?php echo BASE_URL.'institution/InstitutionAudit/getTransaction/'.$user_id; ?>'" name="button" class="btn btn-success">View All</button>
  <?php } ?>
</div>
