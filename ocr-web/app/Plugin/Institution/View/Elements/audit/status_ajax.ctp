<?php
// print_r($data);
 ?>
<table class="table stylish-table customPading">
    <thead>
        <tr>
            <th>Profile </th>
            <th>Name </th>
            <th>E-mail </th>
            <!-- <th>OnCall Status</th> -->
            <!-- <th>Available Status</th> -->
            <th>Modified Date</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
            if(isset($data['user_list']) && count($data['user_list']) > 0){
            foreach( $data['user_list'] as $user){
                $cust_class = 'notAtWork';
                if((string)$user['UserDutyLog']['status'] == '1'){
                    $cust_class = 'onCallMain';
                }else if((string)$user['UserDutyLog']['atwork_status'] == '0'){
                    $cust_class = 'notAtWork';
                }else{
                    $cust_class = '';
                }


                $user_country_id = $_COOKIE['adminCountryId'];
                date_default_timezone_set('UTC');
                $date_cust = $user[0]['date_cust'] ? strtotime($user[0]['date_cust']) : strtotime($user['UserDutyLog']['modified']);
                if($user_country_id==226){
                    // date_default_timezone_set('Europe/Amsterdam');
                    date_default_timezone_set("Europe/London");
                }else if($user_country_id==99){
                    date_default_timezone_set('Asia/Kolkata');
                }
        ?>
        <tr>
            <td style="width:5%;">
                <span data-toggle="modal" data-user="<?php echo $user['User']['id']; ?>" onclick="location.href='<?php echo BASE_URL.'institution/InstitutionUser/userInfo/'.$user['User']['id']; ?>'" class="round <?php echo $cust_class; ?>">
                  <img 'width'='50' 'height'='50' src="<?php echo AMAZON_PATH.$user['User']['id'].'/profile/'.$user['userProfiles']['profile_img']; ?>" onerror="this.src='<?php echo BASE_URL ?>institution/img/ava-single.png'" alt="" />
                  </span>
            </td>
            <td style="width:20%;cursor:pointer;" onclick="location.href='<?php echo BASE_URL.'institution/InstitutionUser/userInfo/'.$user['User']['id']; ?>'"><?php echo $user['userProfiles']['first_name'].' '.$user['userProfiles']['last_name']; ?></td>
            <td style="width:25%;"><?php echo $user['User']['email']; ?></td>
            <td style="width:10%;"><?php echo date("d-m-Y H:i", $date_cust) ; ?></td>
            <td style="width:5%;">
                <div class="btn-group">
                    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="ti-settings"></i>
                    </button>
                    <div class="dropdown-menu">
                        <a onclick="getTransaction('<?php echo $user['User']['id']; ?>');" class="dropdown-item">View Full Transaction History</a>
                    </div>
                </div>
            </td>
        </tr>
        <?php }}elseif(isset($tCount) && $tCount > 0){?>

        <tr>
           <td colspan="8" align="center"><font color="green">Please wait while we are loading the list...</font></td>
        </tr>
        <?php }else{ ?>
        <tr>
           <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
        </tr>
        <?php  } ?>
    </tbody>
</table>
<span id="totalCount" class="pull-right" style="display:none;"><?php echo $tCount; ?></span>
<div class="actions marBottom10 paddingSide15 leftFloat98per">
<?php
    if(isset($data['user_list']) && count($data['user_list']) > 0){
        echo $this->element('pagination');
    }
?>
</div>
