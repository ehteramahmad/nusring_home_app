<table class="table stylish-table customPading">
    <thead>
        <tr>
          <th style="width:40%; padding-left: 25px;">E-mail </th>
          <th style="width:20%;text-align: center;">Status</th>
          <th style="width:20%;text-align: center;">Registered</th>
          <th style="width:20%; text-align: center;">Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php
            if(isset($data['user_list']) && count($data['user_list']) > 0){
            foreach( $data['user_list'] as $user){
        ?>
        <tr>
            <td style="padding-left: 25px;"><?php echo $user['email']; ?></td>
            <td class="statusBtn" style="text-align: center;">
                <?php
                    if($user['status']==2){ ?>
                        <button type="button" class="btn btn-danger btn-grey"> Removed </button>
              <?php }else if($user['status']==1){ ?>
                        <button onclick="changeStatus('<?php echo addslashes($user['email']); ?>',0);" type="button" class="btn btn-primary"> Unapprove </button>
              <?php }else{ ?>
                        <button onclick="changeStatus('<?php echo addslashes($user['email']); ?>',1);" type="button" class="btn btn-outline-primary"> Approve </button>
              <?php }
                ?>
            </td>
            <td style="text-align: center;"><?php echo $user['registered'] == 1? '<span style="color:#00a777;">Yes</span>':'<span style="color:#d0021b;">No</span>'; ?></td>
            <td style="text-align: center;">
            <?php
                if($user['status'] != 2){
            ?>
                <div class="btn-group">
                    <?php if((string)$_COOKIE['adminRoleId'] == '0'){ ?>
                        <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="ti-settings"></i>
                        </button>
                        <div class="dropdown-menu pad0">
                            <a onclick="removeUser('<?php echo addslashes($user['email']); ?>')" class="dropdown-item" href="javascript:void(0)">Remove User</a>
                        </div>
                    <?php }else{ ?>
                        <button disabled="disabled" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="ti-settings"></i>
                        </button>
                    <?php } ?>
                </div>
            <?php
                }else{
            ?>
                    <div class="btn-group text-red">
                        Already removed
                    </div>
            <?php
                }
            ?>
            </td>
        </tr>
        <?php }}elseif(isset($tCount) && $tCount > 0){?>

        <tr>
           <td colspan="8" align="center"><font color="green">Please wait while we are loading the list...</font></td>
        </tr>
        <?php }else{ ?>
        <tr>
           <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
        </tr>
        <?php  } ?>
    </tbody>
</table>
<span id="totalCount" class="pull-right" style="display:none;"><?php echo $tCount; ?></span>
<div class="actions marBottom10 paddingSide15 leftFloat98per">
<?php
    if(isset($data['user_list']) && count($data['user_list']) > 0){
        echo $this->element('pagination');
    }
?>
</div>
