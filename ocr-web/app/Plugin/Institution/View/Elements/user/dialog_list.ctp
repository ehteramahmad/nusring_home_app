<table class="table stylish-table customPading">
    <thead>
        <tr>
          <th>Photo </th>
          <th>Chat Name</th>
          <th>Last Message </th>
          <th>Date</th>
          <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php
          if(isset($data) && count($data) > 0){
          foreach( $data as $dialog){
        ?>
        <tr>
              <td style="width:5%;">
                 <span class="round <?php echo $dialog['pat_class']; ?> <?php echo $dialog['custom_img_class']; ?>">
                   <?php
                   // if(isset($dialog['image']) && (strpos($dialog['image'], 'http') !== false)){
                   //     echo $this->Html->image($dialog['image'],array('width'=>'50!important','height'=>'50!important'));
                   // }else{
                   //     echo strtoupper(substr($dialog['dialog_name'],0,1));
                   // }
                   ?>
                   <img 'width'='50' 'height'='50' src="<?php echo $dialog['image']; ?>" onerror="this.src='<?php echo BASE_URL ?>institution/img/ava-single.png'" alt="" />
                 </span>
             </td>
            <td style="width:20%;" class="dialog_name"><?php echo $dialog['dialog_name']; ?></td>
            <td style="width:25%;"><p class="custom_wrapTxt"><?php echo $dialog['last_message']; ?></p></td>
            <td style="width:10%;"><?php echo date("d M Y",$dialog['message_time']); ?></td>
            <td style="width:5%;">
              <div class="btn-group">
                  <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="ti-settings"></i>
                  </button>
                  <div class="dropdown-menu">
                     <a id="exportuserchat" onclick="showUserChats('<?php echo $dialog['dialog_id'];?>');" class="dropdown-item">Export Chat</a>
                  </div>
               </div>
            </td>
        </tr>
        <tr>
          <td colspan="8" id="emptyData" style="display: none; text-align: center;">
              <font color="red">No Record(s) Found!</font>
          </td>
        </tr>
        <?php }}else{ ?>
        <tr>
           <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
        </tr>
        <?php  } ?>
    </tbody>
</table>
