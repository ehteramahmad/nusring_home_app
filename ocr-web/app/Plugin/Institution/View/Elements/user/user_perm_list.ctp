<?php

if(!empty($perm_list)){
  foreach ($perm_list as $value) {
    $role_name = '';
    $role = json_decode($value['name'],true);
    $role_name .= $role['text'];
    foreach ($role['role'] as $role_data) {
      if(count($role_data['values']) > 0){
        if(count(explode("$",$role_data['values'][0])) > 1){
          $role_name .= ' '.explode("$",$role_data['values'][0])[0];
        }else{
          $role_name .= ' '.explode("$",$role_data['values'][0])[0];
        }
      }
    }
    echo '<li data-id="'.$value['role_id'].'"><span>'.$role_name.'</span></li>';
  }
}else{
  echo '<li class="no_record" data-id=""><span><font color="red">No Permanant Role(s) added by User.</font></span></li>';
} ?>
