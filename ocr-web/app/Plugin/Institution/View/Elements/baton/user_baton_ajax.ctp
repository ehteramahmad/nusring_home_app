<div class="sw-popup-overlay"></div>
<div class="sw-userprofile-popup">
  <div class="sw-close" onclick="closeUserProfile();"></div>
  <div class="sw-userprofile-container">
    <div class="up-userdetail">
      <div class="upu-img">
        <span class="round <?php echo $userdata['cust_class']; ?>">
          <img 'width'="50" 'height'="50" src="<?php echo $userdata['thumbnail_img']; ?>" onerror="this.src='<?php echo BASE_URL ?>institution/img/ava-single.png'" alt="">
        </span>
      </div>
      <div class="upu-content">
        <strong><?php echo $userdata['user_name']; ?></strong><br>
        <?php echo $userdata['email']; ?><br>
        <?php echo $userdata['user_status']; ?>
      </div>
    </div>
    <div class="up-batonroles">
      <?php if(count($userBatonRoles) == 0 || count($userBatonRoles) == 1){ ?>
        <h3>Baton Role(<?php echo count($userBatonRoles); ?>)</h3>
      <?php }else{ ?>
        <h3>Baton Roles (<?php echo count($userBatonRoles); ?>)</h3>
      <?php } ?>
      <?php if($userBatonRoles){ ?>
        <div class="up-br-table">
          <table class="table">
            <?php foreach($userBatonRoles as $batonRole){ ?>
            <tr>
              <td><?php echo $batonRole['role_label']; ?></td>
              <?php if($batonRole['status'] == 1){ ?>
                <td width="110"><a href="javascript: void(0);" onclick="unassignBatonRole('<?php echo $batonRole['role_label']; ?>','<?php echo $batonRole['role_id']; ?>','<?php echo $userdata['user_id']; ?>');" data-toggle="modal" data-target="#unassignBatonPopup">Unassign</a></td>
              <?php }elseif($batonRole['status'] == 3 || $batonRole['status'] == 4 || $batonRole['status'] == 2){ ?>
                <td width="110">Request Awaiting</td>
              <?php }elseif($batonRole['status'] == 5){ ?>
                <td width="110">Transfer Awaiting</td>
              <?php } ?>
            </tr>
            <?php } ?>
          </table>
        </div>
      <?php } ?>
    </div>
  </div>
</div>
