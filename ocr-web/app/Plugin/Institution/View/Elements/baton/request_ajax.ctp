<table class="table stylish-table customPading lineHeight36">
   <thead>
       <tr>
           <th style="width:25%;">User Name</th>
           <th style="width:25%;">Baton Role</th>
           <th style="width:15%; text-align: center;">Request Type</th>
           <th style="width:20%; text-align: center;">Actions</th>
           <th style="width:15%; text-align: center;">Expiring At(Hrs)</th>
       </tr>
   </thead>
   <tbody>
     <?php
         if(isset($data['userList']) && count($data['userList']) > 0){
           $user_country_id = $_COOKIE['adminCountryId'];
           if($user_country_id==226){
             // date_default_timezone_set('Europe/Amsterdam');
             date_default_timezone_set("Europe/London");
           }else if($user_country_id==99){
             date_default_timezone_set('Asia/Kolkata');
           }
           foreach( $data['userList'] as $userRequest){
             ?>
             <tr class="custColorBt" data-id="<?php echo $userRequest['other_user_id']; ?>">
                 <td>
                     <?php //echo $userRequest['thumbnail_img']; ?>
                     <span class="round <?php echo $userRequest['cust_class']; ?>" onclick="userProfile($(this), <?php echo $userRequest['other_user_id']; ?>);">
                       <img 'width'='50' 'height'='50' src="<?php echo $userRequest['thumbnail_img']; ?>" onerror="this.src='<?php echo BASE_URL ?>institution/img/ava-single.png'" alt="" />
                     </span>
                     <?php echo $userRequest['other_user_name']; ?>
                 </td>
                 <td>
                   <?php echo $userRequest['role_name']; ?>
                 </td>
                 <td style="text-align: center;">
                   <?php
                   if($userRequest['type'] == '2'){
                     echo '<span class="txtTakeOn">Take On</span>';
                   }else if($userRequest['type'] == '4'){
                     echo '<span class="txtTransfer">Transfer</span>';
                   }
                  ?>
                 </td>
                 <td style="text-align: center;">
                   <span class="user_note" style="display:none;"><?php echo $userRequest['notes']; ?></span>
                   <?php
                   if($userRequest['type'] == '2'){ ?>
                     <button class="btBatonAccept" type="button" onclick="changeRequest('<?php echo $userRequest['other_user_id']; ?>','<?php echo $userRequest['role_id']; ?>');" name="button">Accept / Reject</button>
                     <!-- <button class="btBatonReject" type="button" onclick="changeRequest('<?php //echo $userRequest['other_user_id']; ?>',0,'<?php //echo $userRequest['role_id']; ?>');"name="button">Reject</button> -->
                  <?php }else if($userRequest['type'] == '4'){ ?>
                    <button class="btBatonAccept" type="button" onclick="changeTransferRequest('<?php echo $userRequest['other_user_id']; ?>','<?php echo $userRequest['role_id']; ?>');" name="button">Accept / Reject</button>
                    <!-- <button class="btBatonReject" type="button" onclick="changeTransferRequest('<?php //echo $userRequest['other_user_id']; ?>',0,'<?php //echo $userRequest['role_id']; ?>');" name="button">Reject</button> -->
                <?php   }
                    ?>
                 </td>
                 <td style="text-align: center;">
                   <?php echo date('H:i', $userRequest['duration']); ?>
                   <?php //echo $userRequest['duration']; ?>
                 </td>
             </tr>
          <?php }}else{ ?>
            <td colspan="8" align="center">
                <font color="red">No Request Pending</font>
            </td>
        <?php  } ?>
   </tbody>
</table>
<span id="totalCount" class="pull-right" style="display:none;"><?php echo $tCount; ?></span>
<span id="tCountTakeOn" class="pull-right" style="display:none;"><?php echo $tCountTakeOn; ?></span>
<span id="tCountTransfer" class="pull-right" style="display:none;"><?php echo $tCountTransfer; ?></span>
<div class="actions marBottom10 paddingSide15 leftFloat98per">

<?php
    if(count($data['userList']) > 0){
        echo $this->element('pagination');
    }
?>
</div>
