<style type="text/css">
.loader {
    position: fixed;
    left:0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('<?php echo BASE_URL; ?>institution/page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
}
#baseLocationLoader {
    position: absolute;
    left:0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('<?php echo BASE_URL; ?>institution/page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
    /* background: url('../page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459); */
}

.modal-backdrop.fade {
  display: none;
}
.modal-open .modal-backdrop.fade {
  display: block;
}
</style>
<div class="container-fluid">
  <!-- <div id="baseLocationLoader"></div> -->
   <!-- ============================================================== -->
   <!-- Bread crumb and right sidebar toggle -->
   <!-- ============================================================== -->
   <div class="row page-titles">
       <div class="col-md-12 col-12 align-self-center">
           <h3 class="text-themecolor m-b-0 m-t-0">Generate QR Code</h3>
       </div>
   </div>
   <!-- ============================================================== -->
   <!-- End Bread crumb and right sidebar toggle -->
   <!-- ============================================================== -->

  <!-- ============================================================== -->
  <!-- Start HTML Content -->
  <!-- ============================================================== -->
  <div class="row boaderLines">
    <div class="loader"></div>
    <div class="col-12">
       <div id="baseLocation" style="position:relative;">
         <div class="card" id="qr_card_form">
            <div class="card-block qr-card-block">
               <div id="select_type">
                <div class="opt-col">
                  <input type="radio" name="qr_type" value="1" id="on_screen">
                  <label for="on_screen">Display on Screen</label>
                </div>
                <div class="opt-col">
                  <input type="radio" name="qr_type" value="2" id="on_email">
                  <label for="on_email">Email for printing</label>
                </div>
               </div>
               <div class="only_one">You can generate only 1 QR code at a time on screen. Please select a subscription period below:</div>
               <div id="qr_request_field" class="disable">
                  <div class="form-group pull-left">
                      <label>Number of QR Cards</label>
                      <div class="number-input disable">
                        <input onkeypress="return (event.charCode==0)||(event.charCode >= 48 &amp;&amp; event.charCode <= 57);" type="text" autocorrect="off" autocapitalize="off" autocomplete="off" class="form-control" id="number_of_qrcode" name="number-of-qr-code" placeholder="00" value="1">
                        <div class="number_controls">
                          <span class="number_increase" onclick="numberInc();"></span>
                          <span class="number_decrease" onclick="numberDec();"></span>
                        </div>
                      </div>
                  </div>
                  <div class="pull-left">
                    <label>Subscription Period</label>
                    <select name="validity" id="validity">
                      <option value="1">1 Day</option>
                      <option value="7">1 Week</option>
                      <option value="14">2 Weeks</option>
                      <option value="30">1 Month</option>
                      <option value="90">3 Months</option>
                      <option value="180">6 Months</option>
                      <option value="365">1 Year</option>
                    </select>
                </div>
              </div>
              <div class="result_block">
                Generated <span id="result_qr_number">1 QR code</span> :
                <?php echo $this->Html->image('Institution.calndr.svg'); ?>
                <span class="span_light">Subscription :</span> <span id="result_qr_validity">3 Days</span>
                <span class="sep"></span>
                <span class="span_light">BATCH NO :</span> <span id="result_batch_no">L16A06 1403715013</span>
              </div>
              <div id="single_qr_img">
                <img src="" alt="">
              </div>
            </div>
            <div class="modal-footer">
              <div class="row showWarning" style="display: block;">
                  <div class="col-md-12 pull-left">
                    <img src="<?php echo BASE_URL ?>institution/img/sub-warning.svg" alt="">
                    <p>Are you sure you want to generate QR codes?</p>
                  <!-- </div>
                  <div class="col-md-7 pull-left"> -->
                    <button type="button" class="btn btn-success waves-effect" onclick="genrateQrCode();" id="qrCodeButton">Yes</button>
                    <!-- <button type="button" class="btn btn-info waves-effect" onclick="disableForm();">Cancel</button> -->
                  </div>
                </div>
                <div class="row showSucess email" style="display: none;">
                    <div class="col-md-12 pull-left">
                      <img src="<?php echo BASE_URL ?>institution/img/sub-sucess.svg" alt="">
                      <p>QR Code has been generated successfully! Generated PDF sent on your email id.</p>
                    <!-- </div>
                    <div class="col-md-4 pull-left"> -->
                      <button type="button" class="btn btn-info waves-effect" onclick="resetAll(2);">Close</button>
                    </div>
                </div>
                <div class="row showSucess screen" style="display: none;">
                    <div class="col-md-12">
                      <img src="<?php echo BASE_URL ?>institution/img/sub-sucess.svg" alt="">
                      <p>QR code has been generated successfully! It will be disappeared once you closed this tab or click somewhere else.</p>
                    <!-- </div>
                    <div class="col-md-4 pull-left"> -->
                      <button type="button" class="btn btn-info waves-effect" onclick="resetAll(1);">Close</button>
                    </div>
                </div>
            </div>
         </div>
       </div>
     </div>
  </div>
</div>

<script type="text/javascript">
$("#sidebarnav>li#menu10").addClass('active').find('a').addClass('active');
$("#sidebarnav>li#menu10").find("li").eq(1).addClass('active').find('a').addClass('active');
$(".loader").fadeOut("fast");
$(window).on('load', function(){
  $('#on_screen').trigger('click');
});

 function numberInc(){
    current = parseInt($('.number-input input').val());
    if(current >= 1){
      if(current <= 19){
        $('.number-input input').val(current + 1);
      }else{
        alert("Maximum limit: 20");
        $('.number-input input').val(20).focus();
      }
    }else{
      $('.number-input input').val(1);
    }
 }

 function numberDec(){
    current = parseInt($('.number-input input').val());
    if(current > 1){
      $('.number-input input').val(current - 1);
    }else{
      $('.number-input input').val(1);
    }
 }

 function resetAll(val){
  var ele = document.getElementsByName('qr_type');
  for(var i=0;i<ele.length;i++)
    ele[i].checked = false;

  $('#qr_request_field').addClass('disable');
  $('.number-input').addClass('disable');
  $('.number-input input').val(1);
  $('#qr_card_single_result, #qr_card_email_result, .card#qr_card_form .modal-footer, .only_one, .card#qr_card_form .showSucess.email, .card#qr_card_form .showSucess.screen, #single_qr_img, .result_block').hide();
  $('#qr_card_form, #qr_request_field .form-group, #qr_request_field label, .card#qr_card_form .showWarning, #qr_request_field').show();
  if(val == 1){
    $('#on_screen').trigger('click');
  }else{
    $('#on_email').trigger('click');
  }
 }

$('#select_type input').change(function(){
  val = $(this).val();
  if(val == 1){
    $('#qr_request_field .form-group, #qr_request_field label').hide();
    $('.only_one').show();
    $('.number-input').addClass('disable');
    $('.number-input input').val(1);
  }else if(val == 2){
    $('.only_one').hide();
    $('#qr_request_field .form-group, #qr_request_field label').show();
    $('.number-input').removeClass('disable');
  }

  $('#qr_request_field').removeClass('disable');
  $('.card#qr_card_form .showSucess.email, .card#qr_card_form .showSucess.screen, #single_qr_img, .result_block').hide();
  $('.card#qr_card_form .modal-footer, .card#qr_card_form .showWarning, #qr_request_field').show();
});

function disableForm(){
  var ele = document.getElementsByName('qr_type');
  for(var i=0;i<ele.length;i++)
    ele[i].checked = false;

  $('#qr_request_field').addClass('disable');
  $('.card#qr_card_form .modal-footer').hide();
  $('.number-input').addClass('disable');
  $('#on_screen').trigger('click');
}


function genrateQrCode(){
    var numberQr = $('#number_of_qrcode').val();
    var qrType = $('#select_type input[name="qr_type"]:checked').val();
    var validity = $('#validity').val();
    if(numberQr == '' || numberQr == 0 ){
        alert("Please enter a valid number of cards.");
        $('.number-input input').val(1).focus();
        return false;
    }
    if(numberQr > 20 ){
        alert("Maximum limit: 20");
        $('.number-input input').val(20).focus();
        return false;
    }
    $(".loader").fadeIn("fast");
    $.ajax({
         url: '<?php echo BASE_URL; ?>institution/Switchboard/genrateQrCodeFunc',
         type: "POST",
         data: {'number_of_qr_codes': $('#number_of_qrcode').val(),'validity':validity,'qr_type':qrType},
         success: function(data) {
          var responseArray = JSON.parse(data);
          if(responseArray['qr_type'] == 1){
            $('#result_qr_validity').html(responseArray['validity']);
            $('#result_batch_no').html(responseArray['batch_number']);
            $('#single_qr_img img').one('load', function() {
              $.ajax({
                 url: '<?php echo BASE_URL; ?>institution/Switchboard/removeQRImage',
                 type: "POST",
                 data: {'qr_img': responseArray['qr_img_name']},
                 success: function(data) {}
               });
            }).attr('src', responseArray['img']);
            $('.card#qr_card_form .showWarning, #qr_request_field, .only_one').hide();
            $('#single_qr_img, .card#qr_card_form .showSucess.screen, .result_block').show();
          }else{
            $('#result_qr_validity').html(responseArray['validity']);
            $('#result_batch_no').html(responseArray['batch_number']);
            $('#result_qr_number').html(responseArray['qr_number']);

            $('.card#qr_card_form .showWarning, #single_qr_img, #qr_request_field, .only_one').hide();
            $('.card#qr_card_form .showSucess.email, .result_block').show();
          }
            $(".loader").fadeOut("fast");
        }
    });
  return false;
}
</script>
