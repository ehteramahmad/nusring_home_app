<?php

$searchdata['status'] = 0;
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
        'update' => '#ajaxContent',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
?>
<div class="loader" style="display: none;"></div>
<style type="text/css">
.loader {
    position: fixed;
    left:0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('<?php echo BASE_URL; ?>institution/page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
}
.calCustom {
    font-size: 1.4em;
    top:8px;
    right:14px;
}

</style>
<div class="container-fluid">
    <div class="row page-titles">
      <div class="col-md-12 align-self-center col-5">
        <h3 class="text-themecolor m-b-0 m-t-0 col-1 window_history"><img src="<?php echo BASE_URL; ?>institution/img/left-arrow.svg" /></h3>
        <h3 class="text-themecolor m-b-0 m-t-0 col-11" style="float: left;">Export Chat</h3>
      </div>
        <!-- <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Export Chat</h3>
        </div> -->
    </div>
    <div class="row breadcrumb-title">
      <ol class="breadcrumb">
          <!-- <li class="breadcrumb-item"><a href="<?php //echo BASE_URL; ?>institution/InstitutionHome/index">Home</a></li>
          <li class="breadcrumb-item"><a href="<?php //echo BASE_URL.'institution/InstitutionUser/exportChat/'.$user_id; ?>">Chat List</a></li>
          <li class="breadcrumb-item active">Export Chat Form</li> -->
      </ol>
    </div>
    <!-- Row -->
    <div id="dialogListView" class="row boaderLines widgetMain">
    <div class="col-12">
        <div class="card">
            <div class="card-block padLeftRight0 pad0">
              <div class="floatLeftFull padLeftRight40 borderBottomExport newListStyle">
                 <div class="tableTop floatLeft">
                    <span class="card-title">Export Chat History</span>
                    <span id="user_id" style="display: none;"><?php echo isset($user_id)? $user_id :'User'; ?></span>
                 </div>
              </div>
                <!-- <p>To export Chat history, you have to enter your profile password due to security reason.</p> -->
                <div class="row floatLeftFull marLeft0">
                    <div class="col-12 padLeftRight0">
                        <form class="exportNewForm">
                            <div class="form-group borderBottomExport" id="passwordBlock">
                                <label for="exampleInputPassword1">Enter Your Password (due to security reason)</label>
                                <input type="text" autocorrect="off" autocapitalize="off" autocomplete="off" class="custom_m_Password form-control" id="exampleInputPassword1" placeholder="Type password…">
                                <div class="form-control-feedback marLeft40px" id="emptyPassword" style="display:none">Please enter password</div>
                                <div class="form-control-feedback marLeft40px" id="passwordError" style="display:none">Please enter correct password</div>
                            </div>
                            <div class="form-group borderBottomExport" id="dateBlock" >
                                <label for="example-date-input">Please Select Date Range For Chat To Export</label>
                                <div class="col-12 pad0">
                                    <span class="form-control col-5 col-form-label">
                                        <input value="" id="datetimepicker6" type="text" autocorrect="off" autocapitalize="off" autocomplete="off">
                                    </span>
                                    <!-- <label for="example-date-input" class="col-1 col-form-label pad0 floatNone txtCenter">To</label> -->
                                    <span class="form-control col-5 col-form-label marLeft20px">
                                        <input value="" id="datetimepicker7" type="text" autocorrect="off" autocapitalize="off" autocomplete="off">
                                    </span>

                                    <div class="form-control-feedback marLeft40px" id="fromDateError" style="display:none">Please enter from date</div>
                                    <div class="form-control-feedback marLeft40px" id="toDateError" style="display:none">Please enter to date</div>
                                    <div class="form-control-feedback marLeft40px" id="incorrectDate" style="display:none">To date must be greater than from date</div>
                                </div>
                            </div>
                            <div class="form-group borderBottomExport" id="reasonBlock" >
                                <label>Mention Reason To Export chat</label>
                                <textarea class="form-control marBottom10px" rows="5" id="reasonToExport"></textarea>
                                <div class="form-control-feedback marLeft40px" id="reasonError" style="display:none">Please enter reason to export chat</div>
                            </div>
                            <div class="col-12 pad0">
                                <div id="dialogListShow1" class="btn btn-primary" onclick="showUserchats('<?php echo $dialogue_id; ?>')">Proceed</div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script type="text/javascript">
    $(function () {
      $('.exportNewForm').trigger("reset");
      // $('.exportNewForm').reset();
        $('.loader').fadeOut('fast');
        $("#sidebarnav>li#menu2").addClass('active').find('a').eq(0).addClass('active');
        $("#sidebarnav>li#menu2").find("li").eq(0).addClass('active').find('a').addClass('active');

        $('#datetimepicker6').datetimepicker({
            format: 'DD/MM/YYYY',
            defaultDate: new Date(),
        });
        $('#datetimepicker7').datetimepicker({
            defaultDate: new Date(),
            format: 'DD/MM/YYYY'
        });

        $('#datetimepicker6').data("DateTimePicker").maxDate(new Date());

        $('#datetimepicker7').data("DateTimePicker").maxDate(new Date());

        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });
        $('#datetimepicker6').parent('span.form-control.col-5.col-form-label').click(function(event){
          event.preventDefault();
          $('#datetimepicker6').focus();
        });
        $('#datetimepicker7').parent('span.form-control.col-5.col-form-label').click(function(event){
          event.preventDefault();
          $('#datetimepicker7').focus();
        });
    });
    function showUserchats($dialog_id){
        var fromDate = $('#datetimepicker6').val();
        var toDate = $('#datetimepicker7').val();
        var password = $('#exampleInputPassword1').val().trim();
        var reason = $('#reasonToExport').val().trim();

        if(password==""){
            $("#passwordBlock").addClass('has-danger');
            $("#emptyPassword").show();
            $("#passwordError").hide();
            return false;
        }

        if(fromDate == ''){
            $("#passwordBlock").removeClass('has-danger');
            $("#passwordError").hide();
            $("#emptyPassword").hide();
            $("#dateBlock").addClass('has-danger');
            $("#fromDateError").show();
            $("#toDateError").hide();
            return false;
        }
        if(toDate == ''){
            $("#dateBlock").addClass('has-danger');
            $("#toDateError").show();
            $("#fromDateError").hide();
            return false;
        }

        if(reason==""){
            $("#reasonBlock").addClass('has-danger');
            $("#dateBlock").removeClass('has-danger');
            $("#passwordBlock").removeClass('has-danger');
            $("#passwordError").hide();
            $("#emptyPassword").hide();
            $("#toDateError").hide();
            $("#fromDateError").hide();
            $("#reasonError").show();
            $("#fromDateError").hide();
            return false;
        }

        $("#reasonBlock").removeClass('has-danger');
        $("#reasonError").hide();
        $("#passwordBlock").removeClass('has-danger');
        $("#emptyPassword").hide();
        $("#passwordError").hide();
        $("#dateBlock").removeClass('has-danger');
        $("#fromDateError").hide();
        $("#dateBlock").removeClass('has-danger');
        $("#toDateError").hide();

        myDate1=fromDate.split("-");
        myDate1=myDate1[0].split("/");
        var newDate1=myDate1[2]+"/"+myDate1[1]+"/"+myDate1[0]+" 00:00:00";
        fromDate=new Date(newDate1).getTime()/1000;

        myDate2=toDate.split("-");
        myDate2=myDate2[0].split("/");
        var newDate2=myDate2[2]+"/"+myDate2[1]+"/"+myDate2[0]+" 23:59:59";
        toDate=new Date(newDate2).getTime()/1000;

        var userid = $('span#user_id').html();

        var expires = "";
        var date = new Date();
        date.setTime(date.getTime()+(10 * 60 * 60 * 1000));
        expires = "; expires="+date.toGMTString();

        $(".loading_overlay").show();
        $('.loader').fadeIn('fast');
        $.ajax({
            url: '<?php echo BASE_URL; ?>institution/InstitutionGroup/checkForm',
            type: "POST",
            data: {'password':password},
            success: function(data) {
                var responseArray = JSON.parse(data);
                if(responseArray['status'] == 1){
                    document.cookie = "from_date="+fromDate+expires+"; path=/";
                    document.cookie = "to_date="+toDate+expires+"; path=/";
                    document.cookie = "id="+userid+expires+"; path=/";
                    document.cookie = "reason="+reason+expires+"; path=/";
                    window.location.href =  "<?php echo BASE_URL; ?>institution/InstitutionUser/showUserChats/" + $dialog_id;
                }else{
                    $('.loader').fadeOut('fast');
                    $("#passwordBlock").addClass('has-danger');
                    $("#passwordError").show();
                    $("#emptyPassword").hide();
                    return false;
                }
            },
            error: function( data ){
                $('.loader').fadeOut('fast');
            }
        });
    }
</script>
