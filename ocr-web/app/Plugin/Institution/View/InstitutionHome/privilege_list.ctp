<?php

$searchdata['status'] = 0;
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
        'update' => '#ajaxContent',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
?>
<div class="loader"></div>
<style type="text/css">
.loader {
    position: fixed;
    left:0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('<?php echo BASE_URL; ?>institution/page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
}
.calCustom {
    font-size: 1.4em;
    top:8px;
    right:14px;
}

</style>
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Settings: <span style="color:#10a5da;">Manage Admin</span></h3>
        </div>
    </div>
    <div class="row breadcrumb-title">
      <ol class="breadcrumb">
          <!-- <li class="breadcrumb-item"><a href="<?php //echo BASE_URL ?>institution/InstitutionHome/index"><i class="demo-icon icon-sub-home"></i></a></li>
          <li class="breadcrumb-item active">Admin Team</li> -->
      </ol>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
<!-- Start HTML Content -->
<!-- ============================================================== -->
<div class="row boaderLines">
   <div class="col-12">
      <div class="card">
         <div class="card-block padLeftRight0 pad0">
            <div class="floatLeftFull padLeftRight14 newListStyle">
              <!-- <div class="tableTop floatLeft marTop10">
                 <span class="card-title">Manage Admin</span>
              </div> -->
              <input class="is-hidden" type="text" id="type_val" name="" value="">
              <div class="tableTop floatLeft">
                 <div id="list-filter-options">
                   <input type="radio" name="filter_option" value="all" id="filter_option_all" checked="checked">
                   <label for="filter_option_all">All (<span id="showTotCount">0</span>)</label>
                   <input type="radio" name="filter_option" value="0" id="filter_option_admin">
                   <label for="filter_option_admin">Admin (<span id="showAdminCount">0</span>)</label>
                   <input type="radio" name="filter_option" value="1" id="filter_option_not_subadmin">
                   <label for="filter_option_not_subadmin">Subadmin (<span id="showSubadminCount">0</span>)</label>
                   <input type="radio" name="filter_option" value="3" id="filter_option_switch_board">
                   <label for="filter_option_switch_board">Switchboard (<span id="showSwitchboardCount">0</span>)</label>
                 </div>
              </div>
               <div id="userlisting_filter" class="dataTables_filter">
                  <label><i class="demo-icon icon-sub-lense"></i></label>
                  <input id="user_search" type="search" class="form-control input-sm" placeholder="Search" aria-controls="userlisting">
               </div>
            </div>
            <div class="table-responsive wordBreak" id="ajaxContent">
              <table class="table stylish-table customPading">
                  <thead>
                      <tr>
                          <th>Photo</th>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Admin Role</th>
                          <th>Actions</th>
                      </tr>
                  </thead>
                  <tbody>
                      <?php
                          if(isset($data['user_list']) && count($data['user_list']) > 0){
                          foreach( $data['user_list'] as $user){
                      ?>
                      <tr>
                          <td style="width:50px;"><span class="round"><?php echo strtoupper(substr($user['AdminUser']['email'],0,1)); ?></span></td>
                          <td><?php echo $user['userProfiles']['first_name'].' '.$user['userProfiles']['last_name']; ?></td>
                          <td><?php echo $user['AdminUser']['email']; ?></td>
                          <td>
                            <?php if($user['AdminUser']['role_id']==1){ ?>
                                      <!-- <button onclick="changeStatus('<?php //echo $user['email']; ?>',0);" type="button" class="btn btn-primary"> Unassign </button> -->
                                      Subadmin
                            <?php }else{ ?>
                                      <!-- <button onclick="changeStatus('<?php //echo $user['email']; ?>',1);" type="button" class="btn btn-outline-primary"> Assign </button> -->
                                      Admin
                            <?php }
                              ?>
                          </td>
                          <td>
                              <div class="btn-group">
                                  <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      <i class="ti-settings"></i>
                                  </button>
                                  <div class="dropdown-menu pad0">
                                      <a class="dropdown-item" href="" data-toggle="modal" data-target="#privilegePopUp" data-whatever="@mdo" data-useremail="<?php echo $user['AdminUser']['email']; ?>" data-userid="<?php echo $user['User']['id']; ?>">Manage Privilege</a>
                                      <!-- <a onclick="removeUser('<?php //echo $user['AdminUser']['email']; ?>')" class="dropdown-item" href="javascript:void(0)">Remove User</a> -->
                                  </div>
                              </div>
                          </td>
                      </tr>
                      <?php }}elseif(isset($tCount) && $tCount > 0){?>

                      <tr>
                         <td colspan="8" align="center"><font color="green">Please wait while we are loading the list...</font></td>
                      </tr>
                      <?php }else{ ?>
                      <tr>
                         <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
                      </tr>
                      <?php  } ?>
                  </tbody>
              </table>
              <span id="totalCount" class="pull-right" style="display:none;"><?php echo $tCount; ?></span>
              <div class="actions marBottom10 paddingSide15 leftFloat98per">
              <?php
                  if(isset($data['user_list']) && count($data['user_list']) > 0){
                      echo $this->element('pagination');
                  }
              ?>
              </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- ============================================================== -->
<!-- End HTML Content -->
<!-- ============================================================== -->




    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <!-- User Privilege -->
    <div class="modal fade" id="privilegePopUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="loader" id="priviligeLoader"></div>
        <div class="modal-dialog" role="document" style="max-width:700px !important;">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Update User Privilege</h4>
                    <p>You can update user privilege by selecting role type below:</p>
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                </div>
                <div class="modal-body">
                    <form>
                        <!-- <span id="subscribtionNote1" style="font-size: 16px;font-weight: normal;">Please select end date of this user’s subscription.</span><br> -->
                        <!-- <label>Select Date: </label> -->
                        <div class="row btCust">
                            <div class="col-md-3">
                                <input type="radio" onclick="setPrivilege(0);" name="privilige" value="0" placeholder="">
                                <label>Admin</label>
                            </div>
                            <div class="col-md-3">
                                <input type="radio" onclick="setPrivilege(1);" name="privilige" value="1" placeholder="">
                                <label>Subadmin</label>
                            </div>
                            <div class="col-md-3">
                                <input type="radio" onclick="setPrivilege(3);" name="privilige" value="3" placeholder="">
                                <label>Switchboard</label>
                            </div>
                            <div class="col-md-3">
                                <input type="radio" onclick="setPrivilege(2);" name="privilige" value="2" placeholder="">
                                <label>None</label>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                  <div class="row showWarning" style="display:block">
                      <div class="col-md-6 pull-left">
                        <img src="<?php echo BASE_URL ?>institution/img/sub-warning.svg" alt="">
                        <p>Are you sure you want to allow this user to access limited features of  Institution Panel?</p>
                      </div>
                      <div class="col-md-6 txtRight pull-right">
                        <button type="button" class="btn btn-success waves-effect" onclick="setPrivilege_final();">Yes</button>
                        <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Cancel</button>
                      </div>
                    </div>
                    <div class="row showSucess" style="display:none">
                        <div class="col-md-8 pull-left">
                          <img src="<?php echo BASE_URL ?>institution/img/sub-sucess.svg" alt="">
                          <p>User Privilege has been updated successfully!</p>
                        </div>
                        <div class="col-md-4 txtRight pull-right">
                          <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ****************  -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->

</div>
<script type="text/javascript">
    // $("#sidebarnav>li#menu3").addClass('active').find('a').addClass('active');
    $("#sidebarnav>li#menu4").addClass('active').find('a').eq(0).addClass('active');
    $("#sidebarnav>li#menu4").find("li").eq(0).addClass('active').find('a').addClass('active');

    //setup before functions
    var typingTimer;                //timer identifier
    var doneTypingInterval = 1000;  //time in ms, 5 second for example
    var $input = $('#user_search');

    //on keyup, start the countdown
    $input.on('keyup', function () {
      clearTimeout(typingTimer);
      typingTimer = setTimeout(searchUser, doneTypingInterval);
    });

    //on keydown, clear the countdown
    $input.on('keydown', function () {
      clearTimeout(typingTimer);
    });

    //user is "finished typing," do something
    function searchUser() {
       $search_text = $input.val().trim();
       var type = $('#type_val').val();
       if($search_text == ''){
           // return false;
       }
       $input.blur();
       $(".loader").fadeIn("fast");
       getAdminCounts();
       $.ajax({
           url:'<?php echo BASE_URL; ?>institution/InstitutionHome/privilegeListFilter',
           type: "POST",
           data: {'search_text':$search_text, 'type':type},
           success: function(data) {
               $('#ajaxContent').html(data);
               $('#showTotCount').html($('#totalCount').html());
               $(".loader").fadeOut("fast");
               $('#assign_btn').hide();
               $('#unassign_btn').hide();
           }
       });
    }
    // --------- INITIAL FUNCTIONS ------------

    $('body').on('click','input[name="filter_option"]',function(){
      $('#type_val').val($(this).val());
      getUserList('1','20',$input.val().trim());
    });

    getUserList('1','20',$input.val().trim());

    function getUserList(page,limit,$search_text){
      var type = $('#type_val').val();
       $(".loader").fadeIn("fast");
       getAdminCounts();
       $.ajax({
         url:'<?php echo BASE_URL; ?>institution/InstitutionHome/privilegeListFilter',
         type: "POST",
         data: {'page_number':page,'limit':limit,'search_text':$search_text, 'type':type},
         success: function(data) {
           $('#ajaxContent').html(data);
           $('#showTotCount').html($('#totalCount').html());
           $(".loader").fadeOut("fast");
         }
       });
    }

    function getAdminCounts(){
      $search_text = $input.val().trim();
      $.ajax({
        url:'<?php echo BASE_URL; ?>institution/InstitutionHome/getAdminCounts',
        type: "POST",
        data: {'search_text':$search_text},
        success: function(data) {
          console.log(data);
          data = JSON.parse(data);
          console.log(data);
          $('#showTotCount').html(data.all_users);
          $('#showAdminCount').html(data.all_admins);
          $('#showSubadminCount').html(data.all_subadmins);
          $('#showSwitchboardCount').html(data.all_switchboards);
        }
      });
    }

    // --------- INITIAL FUNCTIONS ------------

    function chngPage(val,limit) {
        var a=$('tr th.active').attr('id');
        var b=$('tr th.ASC').attr('id');
        if(a==b){
            var order='A';
        }
        else{
            var order='DE';
        }
        var goVal=$("#chngPage").val();
        $search_text = $input.val().trim();
        var type = $('#type_val').val();
        if(parseInt(goVal) <= Math.ceil(val) && Math.ceil(goVal)>0){
            $(".loader").fadeIn("fast");
            $.ajax({
                url:'<?php echo BASE_URL; ?>institution/InstitutionHome/privilegeListFilter',
                type: "POST",
                data: {'page_number':goVal,'limit':limit,'sort':a,'order':order,'search_text':$search_text, 'type':type},
                success: function(data) {
                    $('#ajaxContent').html(data);
                    $('#showTotCount').html($('#totalCount').html());
                    $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                    if(a==b){
                        $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                    }
                    else{
                        $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                    }
                    $(".loader").fadeOut("fast");
                }
            });
        }

    }
    function chngCount(val){
        $(".loader").fadeIn("fast");
        var a=$('tr th.active').attr('id');
        var b=$('tr th.ASC').attr('id');
        if(a==b){
            var order='A';
        }
        else{
            var order='DE';
        }
        $search_text = $input.val().trim();
        var type = $('#type_val').val();
        $.ajax({
            url:'<?php echo BASE_URL; ?>institution/InstitutionHome/privilegeListFilter',
            type: "POST",
            data: {'limit':val,'sort':a,'order':order,'search_text':$search_text, 'type':type},
            success: function(data) {
                $('#ajaxContent').html(data);
                $('#showTotCount').html($('#totalCount').html());
                $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                if(a==b){
                    $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                }
                else{
                    $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                }
                $(".loader").fadeOut("fast");
            }
        });
    }

    function abc(val,limit){
        $(".loader").fadeIn("fast");
        var a=$('tr th.active').attr('id');
        var b=$('tr th.ASC').attr('id');
        if(a==b){
            var order='A';
        }
        else{
            var order='DE';
        }
        $search_text = $input.val().trim();
        var type = $('#type_val').val();
      $.ajax({
           url:'<?php echo BASE_URL; ?>institution/InstitutionHome/privilegeListFilter',
           type: "POST",
           data: {'page_number':val,'limit':limit,'sort':a,'order':order,'search_text':$search_text, 'type':type},
           success: function(data) {
                $('#ajaxContent').html(data);
                $('#showTotCount').html($('#totalCount').html());
               $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
               if(a==b){
               $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
               }
               else{
               $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
               }
               $(".loader").fadeOut("fast");
           }
        });
    }
</script>
<script type="text/javascript">
    $(function () {
      $('#privilegePopUp').on('hide.bs.modal', function (event) {
        if($('#privilegePopUp .showSucess').css('display') == 'block'){
          reload();
        }
      });
      $('#privilegePopUp').on('show.bs.modal', function (event) {

        var button = $(event.relatedTarget); // Button that triggered the modal
        var email = button.data('useremail'); // Extract info from data-* attributes
        var userid = button.data('userid'); // Extract info from data-* attributes
        var adminEmail = "<?php echo (string)$_COOKIE['adminEmail']; ?>";
        $('#privilegePopUp .showWarning').hide();
        $('#privilegePopUp .showSucess').hide();

        if(email == adminEmail){
            var buttonHtml = '<button type="button" onClick="closePop();" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
            $('#customModalLabel').html('Error');
            $('#customModalBody').html('You can not perform this action. Please contact to support@medicbleep.com');
            $('#customModalFoot').html(buttonHtml);
            $('#customBox').modal('show');
            $("#customBox .loaderImg").hide();
            return ;
        }
        $('#priviligeLoader').fadeIn('fast');
        $('#privilegePopUp').attr('data-userEmail',email);
        $('#privilegePopUp input[name="privilige"]').prop('checked', false);;
        $.ajax({
            url:'<?php echo BASE_URL; ?>institution/InstitutionHome/getUserPrivilegeStatus',
            type: 'POST',
            data: {'user_email': email, 'user_id': userid},
            success: function( result ){
                $('#priviligeLoader').fadeOut('fast');
                console.log(result);
                var responseArray = JSON.parse(result);
                if( responseArray['status'] == 1 ){
                    // if(responseArray['data']['role_id'] != 'null'){
                    $('#privilegePopUp input[name="privilige"][value="'+responseArray['data']['role_id']+'"]').prop('checked', true);
                    $('#privilegePopUp').attr('data-privilige',responseArray['data']['role_id']);
                    // }
                }
            },
            error: function( result ){
                $('#priviligeLoader').fadeOut('fast');
            }
        });
      });
    });

    function closePop(){
        $('#privilegePopUp').modal('hide');
    }

    function setPrivilege(role_id){
        var privilige = $('#privilegePopUp').attr('data-privilige');
        if(privilige == role_id){
            return false;
        }
        $('#privilegePopUp .showWarning').show();
        $('#privilegePopUp .showSucess').hide();
    }

    function setPrivilege_final(){
        var email = $('#privilegePopUp').attr('data-userEmail');
        var privilige = $('#privilegePopUp').attr('data-privilige');
        var role_id = $('#privilegePopUp input[name="privilige"]:checked').val();
        $('#priviligeLoader').fadeIn('fast');
        $.ajax({
          url:'<?php echo BASE_URL; ?>institution/InstitutionHome/setUserPrivilege',
          type: "POST",
          data: {'email':email,'role_id':role_id},
          success: function(data) {
            console.log(data);
            var responseArray = JSON.parse(data);
            $('#priviligeLoader').fadeOut('fast');
            // $('#privilegePopUp').modal('toggle');
            // $("#customBox .loaderImg").hide();
            if(responseArray['status'] == 1){
                $('#privilegePopUp .showWarning').hide();
                $('#privilegePopUp .showSucess').show();
                $('#privilegePopUp').attr('data-privilige',role_id);
                // var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                // $('#customModalLabel').html('Confirmation');
                // // $('#customModalBody').html('Please wait while Requesting qr Code');
                // $('#customModalBody').html('User privilege changed successfully');
                // $('#customModalFoot').html(buttonHtml);
                // $('#customBox').modal('show');
            }else{
                var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                $('#customModalLabel').html('Error');
                $('#customModalBody').html(responseArray['message']);
                $('#customModalFoot').html(buttonHtml);
                $('#customBox').modal('show');
                $("#customBox .loaderImg").hide();
            }
          },
            error: function( result ){
                $('#priviligeLoader').fadeOut('fast');
                $("#customBox .loaderImg").hide();
                var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                $('#customModalLabel').html('Error');
                $('#customModalBody').html('An error has occurred. Please try again.');
                $('#customModalFoot').html(buttonHtml);
                $('#customBox').modal('show');
                $("#customBox .loaderImg").hide();
            }
        });
    }

    function reload(){
        var page = parseInt($('#sample_1_paginate ul.pagination li.active.num a #chngPage').val());
        var limit = $('#Data_Count').val();
        var $search_text = $input.val().trim();
        getUserList(page,limit,$search_text);
    }
</script>
