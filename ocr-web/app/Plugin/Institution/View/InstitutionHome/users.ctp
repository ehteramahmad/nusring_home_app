<?php
/*foreach ($country as  $value) {
    // pr($value['countries']);
}*/
// exit();
$searchdata['status'] = 0;
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
        'update' => '#ajaxContent',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
?>
<div id="mainLoader" class="loader"></div>
<style type="text/css">
.loader, .loaderImg {
    position: fixed;
    left:0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('<?php echo BASE_URL; ?>institution/page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
}
.calCustom {
    font-size: 1.4em;
    top:8px;
    right:14px;
}

</style>
<div class="container-fluid" data-type="<?php echo $type; ?>">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-3 align-self-center col-5">
            <h3 class="text-themecolor m-b-0 m-t-0">Manage: <span style="color:#10a5da;">Users</span></h3>
        </div>
        <!-- <div class="buttonTop col-md-9 col-7"> -->
          <div class="col-md-9 assignBtnAll">
            <button id="unassign_btn" disabled="disabled" onclick="changeBulkStatus(0);" style="display: none;" class="btn bulk_action_btn pull-right btn-primary"> + Unapprove Selected</button>
            <button id="assign_btn" disabled="disabled" onclick="changeBulkStatus(1);" style="display: none;" class="btn bulk_action_btn pull-right btn-primary"> + Approve Selected</button>
          </div>
        <!-- </div> -->
    </div>
    <div class="row breadcrumb-title">
      <ol class="breadcrumb">
        <span class="is-hidden" id="type_val"><?php echo $type; ?></span>
          <!-- <li class="breadcrumb-item"><a href="javascript:void(0)"><i class="demo-icon icon-sub-home"></i></a></li>
          <li class="breadcrumb-item active"></li> -->
      </ol>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row boaderLines">
        <div class="col-12">
            <div class="card">
                <div class="card-block padLeftRight0 pad0">
                    <div class="floatLeftFull padLeftRight14 newListStyle">
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                        <div class="loader" style="height: 64%; display:none;" id="adduserLoader"></div>
                        <div class="modal-dialog" role="document">
                            <div class="modal-content uploadFileBlock" id="bulkUploadUser">
                            <div class="loaderImg" style="display:none;">
                                <?php //echo $this->Html->image('Institution.page-loader.gif', array("class"=> "loading-image" )); ?>

                                </div>
                                <div class="modal-header">
                                    <h4 class="modal-title" id="exampleModalLabel1">Add User</h4>
                                    <p>You can add user by providing information below:</p>
                                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                                </div>
                                <div class="modal-body">
                                    <form id="bulkEmailUpload">
                                        <div id="inputEmail" class="form-group">
                                            <input type="email" class="form-control" id="userEmail" name="userEmail" placeholder="Enter user’s email address">
                                            <div class="form-control-feedback" id="emptyEmail" style="display:none">Please Enter Email address</div>
                                            <div class="form-control-feedback" id="invalidEmail" style="display:none">Please enter a valid email address</div>
                                            <div class="form-control-feedback" id="alreadyExist" style="display:none"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12">
                                                <div class="card">
                                                    <div class="">
                                                      <div class="borderSeparation">
                                                        <h4>OR</h4>
                                                      </div>
                                                        <label for="input-file-now" class="nodefault">Add Multiple Users</label>
                                                        <div class="pull-right">
                                                          <span class="sampleTxt">Sample File Formats- </span>
                                                          <a target="_blank" class="downloadLink" href="<?php echo BASE_URL; ?>institution/sample.csv" title="Sample">Download</a>
                                                        </div>
                                                        <input type="file" name="emailfile" id="input-file-now" class="dropify" accept=".csv, .xls, .xlsx, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
                                                        <input type="hidden" name="company_id" type="test" value="<?php echo $_COOKIE['adminInstituteId']; ?>" />
                                                        <div class="form-control-feedback" id="uploadResult" style="display:none">Upload Result:-</div>
                                                        <div class="form-control-feedback" id="uploadSuccess" style="display:none"></div>
                                                        <div class="form-control-feedback" id="uploadError" style="display:none"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                <div class="onCallBtMain txtCenter marBottom30">
                                  <button type="button" style="background-color:#00A777;color:#ffffff;" class="btn btn-onCallBt" onclick="addUser();" id="addUserbtn1">Add User</button>
                                  <button type="submit" style="background-color:#00A777;color:#ffffff;" class="btn btn-onCallBt" id="addUserbtn2" style="display:none;">Upload</button>
                                  <button type="button" class="btn btn-onCallBt" data-dismiss="modal">Cancel</button>
                                </div>
                                <!-- <div style="border-top: 1px solid #eceeef !important;" class="modal-footer">
                                    <button type="button" class="btn btn-success waves-effect" onclick="addUser();" id="addUserbtn1">Save</button>
                                    <button class="btn btn-success waves-effect" id="addUserbtn2" style="display:none;">Upload</button>
                                </div> -->
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="exampleModal2" tabindex="2" role="dialog" aria-labelledby="exampleModalLabel2">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="exampleModalLabel2">Broadcast Message</h4>
                                    <p>Write a message to broadcast in your institution.</p>
                                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                                </div>
                                <div class="modal-body">
                                   <!-- <p>Message:</p> -->
                                    <form>
                                        <div id="inputMessage" class="form-group">
                                            <textarea class="form-control" id="textMessage" placeholder="Write here…" maxlength="500"></textarea>
                                            <p class="txtLeft" id="counter"></p>
                                            <div class="form-control-feedback" id="emptyMessage" style="display:none">Please Enter Message</div>
                                            <div class="form-control-feedback" id="processingMessage" style="display:none;font-size:12px;color:green;font-style:italic">Please wait while broadcasting messege...</div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                  <div class="row showWarning" style="display:block">
                                    <div class="col-md-6 pull-left">
                                      <img src="<?php echo BASE_URL ?>institution/img/sub-warning.svg" alt="">
                                      <p>Are you sure you want to broadcast this message to all users within institution?</p>
                                    </div>
                                    <div class="col-md-6 txtRight pull-right">
                                      <button type="button" class="btn btn-success waves-effect" id="preRequestBtn1">Yes</button>
                                      <button type="button" id="postRequestBtn1" style="display: none;" class="btn btn-success waves-effect" data-toggle="modal" data-target="#exampleModal3"></button>
                                      <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Cancel</button>
                                    </div>
                                  </div>
                                  <div class="row showSucess" style="display:none">
                                      <div class="col-md-8 pull-left">
                                        <img src="<?php echo BASE_URL ?>institution/img/sub-sucess.svg" alt="">
                                        <p>Message Brodacasted successfully to all institute members.</p>
                                      </div>
                                      <div class="col-md-4 txtRight pull-right">
                                        <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
                                      </div>
                                  </div>
                                    <!-- <button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>
                                    <button type="button" id="preRequestBtn1" class="btn btn-success waves-effect">Send Message</button>
                                    <button type="button" id="postRequestBtn1" style="display: none;" class="btn btn-success waves-effect" data-toggle="modal" data-target="#exampleModal3"></button> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="exampleModal3" tabindex="3" role="dialog" aria-labelledby="exampleModalLabel3">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content" style="height:305px;">
                                <div class="modal-header">
                                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <i class="icon-arrow-left-circle"></i>
                                    </button> -->
                                    <h4 class="modal-title" id="exampleModalLabel3">Broadcast Message</h4>
                                </div>
                                <div class="modal-body">
                                   <p>Please confirm your password:</p>
                                   <span class="green">* Please use your Medic Bleep password</span>
                                    <form>
                                        <div id="inputPassword" class="form-group marBottom52px">
                                             <input type="text" autocorrect="off" autocapitalize="off" autocomplete="off" class="custom_m_Password form-control" id="messagePassword">
                                             <div class="form-control-feedback" id="emptyPassword" style="display:none">Please Enter Password</div>
                                             <div class="form-control-feedback" id="incorrectPassword" style="display:none">Password is incorrect</div>
                                             <div class="form-control-feedback" id="responseMessage" style="display:none"></div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Back</button>
                                    <button type="button" id="preConfirmBtn2" class="btn btn-success waves-effect" data-dismiss="modal">Confirm Password</button>
                                    <button type="button" style="display: none;" id="postConfirmBtn2" class="btn btn-success waves-effect" data-dismiss="modal"></button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="exampleModalQr" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content uploadFileBlock" id="bulkUploadUser">
                            <div class="loaderImg" style="display:none;"><?php //echo $this->Html->image('Institution.page-loader.gif', array("class"=> "loading-image" )); ?></div>
                                <div class="modal-header">
                                    <h4 class="modal-title" id="exampleModalLabel1">QR Code Request</h4>
                                    <p>You can request number of QR Codes required:</p>
                                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                                </div>
                                <div class="modal-body">
                                    <form id="requestQrCode">
                                        <div id="inputQrNumber" class="form-group pull-left">
                                            <input onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" type="text" autocorrect="off" autocapitalize="off" autocomplete="off" class="form-control" id="number-of-qr-code" name="number-of-qr-code" placeholder="00">
                                            <div class="form-control-feedback" id="emptyQr" style="display:none">Please Enter Number of qr code</div>
                                        </div>
                                        <!-- <label for="validity">Validity</label> -->
                                        <div class="pull-right">
                                          <select name="validity" id="validity">
                                            <option disabled selected value="0">Validity</option>
                                            <option value="1">1 Day</option>
                                            <option value="7">1 Week</option>
                                            <option value="14">2 Weeks</option>
                                            <option value="30">1 Month</option>
                                            <option value="90">3 Months</option>
                                            <option value="180">6 Months</option>
                                            <option value="365">1 Year</option>
                                          </select>
                                      </div>
                                      <!-- <div style="border-top: 1px solid #eceeef !important;" class="modal-footer">
                                          <button type="button" class="btn btn-success waves-effect" onclick="requestQrCode();" id="qrCodeButton">Send</button>
                                      </div> -->
                                </form>
                            </div>
                            <div class="modal-footer">
                              <div class="row showWarning" style="display:block">
                                  <div class="col-md-6 pull-left">
                                    <img src="<?php echo BASE_URL ?>institution/img/sub-warning.svg" alt="">
                                    <p>Are you sure you want to request QR Code.</p>
                                  </div>
                                  <div class="col-md-6 txtRight pull-right">
                                    <button type="button" class="btn btn-success waves-effect" onclick="requestQrCode();" id="qrCodeButton">Yes</button>
                                    <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Cancel</button>
                                  </div>
                                </div>
                                <div class="row showSucess" style="display:none">
                                    <div class="col-md-8 pull-left">
                                      <img src="<?php echo BASE_URL ?>institution/img/sub-sucess.svg" alt="">
                                      <p>QR Code request sent successfully. Please check your email for further details.</p>
                                    </div>
                                    <div class="col-md-4 txtRight pull-right">
                                      <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>

                    <!-- At work and on call pop up -->
                    <div class="modal fade" id="availableStatusPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                        <div class="loader" style="height: 50%;" id="oncallatworkLoader"></div>
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="loaderImg" style="display:none;">
                                    <?php //echo $this->Html->image('Institution.page-loader.gif', array("class"=> "loading-image" )); ?>
                                </div>
                                <div class="modal-header">
                                    <h4 class="modal-title" id="exampleModalLabel1">Update User Status</h4>
                                    <p>You can update user status by clicking on buttons below:</p>
                                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                                </div>
                                <div class="modal-body">
                                  <div class="onCallBtMain txtCenter">
                                    <button type="button" id="onCllBtn" class="btn btn-onCallBt">Not On Call</button>
                                    <button type="button" id="availableBtn" class="btn btn-onCallBt">Not Available</button>
                                  </div>
                                    <!-- <form id="myForm" class="floatLeftFull borderBottomNew">
                                        <div class="floatLeft">
                                            <p>Available</p>
                                        </div>
                                        <div class="floatRight">
                                            <div class="onoffswitch">
                                                <input type="checkbox" name="onoffswitch1" class="onoffswitch-checkbox" id="atwork" checked>
                                                <label class="onoffswitch-label" for="atwork">
                                                    <span class="onoffswitch-inner"></span>
                                                    <span class="onoffswitch-switch"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </form>
                                    <form id="onCallForm">
                                        <div class="floatLeft">
                                            <p>On Call</p>
                                        </div>
                                        <div class="floatRight">
                                          <div class="onoffswitch">
                                                <input type="checkbox" name="onoffswitch2" class="onoffswitch-checkbox" id="oncall" checked>
                                                <label class="onoffswitch-label" for="oncall">
                                                    <span class="onoffswitch-inner"></span>
                                                    <span class="onoffswitch-switch"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </form> -->

                                    <div class="form-control-feedback errorStatus leftFloatFull" id="onCallError" style="display:none;color: red;text-align: center;"></div>
                                </div>
                                <div class="modal-footer">
                                  <div class="row showWarning" style="display:none">
                                      <div class="col-md-6 pull-left">
                                        <img src="<?php echo BASE_URL ?>institution/img/sub-warning.svg" alt="">
                                        <p>Are you sure you want to update user status?</p>
                                      </div>
                                      <div class="col-md-6 txtRight pull-right">
                                        <button type="button" class="btn btn-success waves-effect" id="updateUserProfile">Yes</button>
                                        <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Cancel</button>
                                      </div>
                                    </div>
                                    <div class="row showSucess" style="display:none">
                                        <div class="col-md-8 pull-left">
                                          <img src="<?php echo BASE_URL ?>institution/img/sub-sucess.svg" alt="">
                                          <p>User Status is updated.</p>
                                        </div>
                                        <div class="col-md-4 txtRight pull-right">
                                          <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ****************  -->

                    <!-- User Custom Subscription -->
                    <div class="modal fade" id="customSubscriptionPopUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="exampleModalLabel1">Update User Subscription</h4>
                                    <p>You can update user’s subscription expiry date by clicking on date below:</p>
                                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                                </div>
                                <div class="modal-body">
                                  <!-- <p>User’s Subscription Current Expiry Date</p> -->
                                    <form>
                                        <!-- <span id="subscribtionNote1" style="font-size: 16px;font-weight: normal;">Please select end date of this user’s subscription.</span><br>
                                        <span id="subscribtionNote2" style="
                                        font-size: 12px;
                                    ">Note: This user will be unsubscribed from your institution after the selected date.</span><br> -->
                                    <span class="dateExpire" id="usersubscriptionexpiryDate"></span>
                                        <div id="datepicker" class="input-group date" data-date-format="dd-mm-yyyy">
                                            <input class="form-control" type="text" autocorrect="off" autocapitalize="off" autocomplete="off" value="" id="subscription_expiry_date"/>
                                            <span class="input-group-addon">
                                              <i class="demo-icon icon-calendar-icon"></i>
                                            </span>
                                        </div>

                                    </form>
                                </div>
                                <div class="modal-footer">
                                  <div class="row showDefault" style="display:block">
                                    <p>Note: This user will be unsubscribed from your institution after the selected date.</p>
                                  </div>
                                  <div class="row showWarning" style="display:none">
                                    <div class="col-md-6 pull-left">
                                      <img src="<?php echo BASE_URL ?>institution/img/sub-warning.svg" alt="">
                                      <p>Are you sure you want to change User Subscription?</p>
                                    </div>
                                    <div class="col-md-6 txtRight pull-right">
                                      <button type="button" class="btn btn-success waves-effect" onclick="customSubscription();">Yes</button>
                                      <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Cancel</button>
                                    </div>
                                  </div>
                                  <div class="row showSucess" style="display:none">
                                      <div class="col-md-8 pull-left">
                                        <img src="<?php echo BASE_URL ?>institution/img/sub-sucess.svg" alt="">
                                        <p>User Subscription is updated.</p>
                                      </div>
                                      <div class="col-md-4 txtRight pull-right">
                                        <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
                                      </div>
                                  </div>
                                <!-- <button type="button" class="btn btn-success waves-effect" onclick="customSubscription();">Save</button> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ****************  -->

                    <!-- User Privilege -->
                    <div class="modal fade" id="privilegePopUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                        <div class="loader" id="priviligeLoader"></div>
                        <div class="modal-dialog" role="document" style="max-width:700px !important;">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="exampleModalLabel1">Update User Privilege</h4>
                                    <p>You can update user privilege by selecting role type below:</p>
                                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                                </div>
                                <div class="modal-body">
                                    <form>
                                        <!-- <span id="subscribtionNote1" style="font-size: 16px;font-weight: normal;">Please select end date of this user’s subscription.</span><br> -->
                                        <!-- <label>Select Date: </label> -->
                                        <div class="row btCust">
                                            <div class="col-md-3">
                                                <input type="radio" onclick="setPrivilege(0);" name="privilige" value="0" placeholder="">
                                                <label>Admin</label>
                                            </div>
                                            <div class="col-md-3">
                                                <input type="radio" onclick="setPrivilege(1);" name="privilige" value="1" placeholder="">
                                                <label>Subadmin</label>
                                            </div>
                                            <div class="col-md-3">
                                                <input type="radio" onclick="setPrivilege(3);" name="privilige" value="3" placeholder="">
                                                <label>Switchboard</label>
                                            </div>
                                            <div class="col-md-3">
                                                <input type="radio" onclick="setPrivilege(2);" name="privilige" value="2" placeholder="">
                                                <label>None</label>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                  <div class="row showWarning" style="display:block">
                                      <div class="col-md-6 pull-left">
                                        <img src="<?php echo BASE_URL ?>institution/img/sub-warning.svg" alt="">
                                        <p>Are you sure you want to allow this user to access limited features of  Institution Panel?</p>
                                      </div>
                                      <div class="col-md-6 txtRight pull-right">
                                        <button type="button" class="btn btn-success waves-effect" onclick="setPrivilege_final();">Yes</button>
                                        <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Cancel</button>
                                      </div>
                                    </div>
                                    <div class="row showSucess" style="display:none">
                                        <div class="col-md-8 pull-left">
                                          <img src="<?php echo BASE_URL ?>institution/img/sub-sucess.svg" alt="">
                                          <p>User Privilege has been updated successfully!</p>
                                        </div>
                                        <div class="col-md-4 txtRight pull-right">
                                          <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ****************  -->

                    <!-- User Privilege -->
                    <div class="modal fade" id="batonRolePopUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                        <div class="loader" id="batonRoleLoader"></div>
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="exampleModalLabel1">Update User Baton Roles</h4>
                                    <p>You can update user Baton Roles below:</p>
                                </div>
                                <div class="modal-body">
                                  <div class="main_content batonUpdateAction leftFloatFull">
                                      <div id="baton_role_content">
                                          <div class="btCust">
                                              <span>Wait while loading ...</span>
                                          </div>
                                      </div>
                                      <div class="assignBox">
                                        <span class="txtComment">You can assign new baton roles to user:</span>
                                        <button id="addBaton" class="btn hidden-sm-down">+ Assign Baton Roles</button>
                                      </div>
                                  </div>
                                  <div class="baton_listing" style="display:none;">
                                      <div class="col-md-12 baton_role_search">
                                        <label><i class="demo-icon icon-sub-lense"></i></label>
                                        <input type="text" placeholder="Search" id="search_unoccupied_role" name="" value="">
                                      </div>
                                      <div id="batonListing" style="display:none;">
                                        <ul id="baton_unoccupied_role"></ul>
                                      </div>
                                  </div>
                                </div>

                                <div class="modal-footer">
                                  <div class="row showWarning" style="display:none">
                                      <div class="col-md-6 pull-left">
                                        <img src="<?php echo BASE_URL ?>institution/img/sub-warning.svg" alt="">
                                        <p>Are you sure you want to update user baton role?</p>
                                      </div>
                                      <div class="col-md-6 txtRight pull-right">
                                        <button type="button" class="btn btn-success waves-effect" onclick="setBatonRole();">Yes</button>
                                        <button type="button" class="btn btn-info waves-effect toMainContent">Cancel</button>
                                      </div>
                                    </div>
                                    <div class="row showSucess" style="display:none">
                                        <div class="col-md-8 pull-left">
                                          <img src="<?php echo BASE_URL ?>institution/img/sub-sucess.svg" alt="">
                                          <p>User Baton role has been updated successfully!</p>
                                        </div>
                                        <div class="col-md-4 txtRight pull-right">
                                          <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    <div class="row showBack" style="display:none">
                                        <div class="col-md-6 txtRight pull-right">
                                          <button type="button" class="btn btn-info waves-effect toMainContent">Cancel</button>
                                        </div>
                                      </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ****************  -->

                    <!-- User Session Timeout -->
                    <div class="modal fade" id="sessionPopUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                        <div class="loader" id="sessionLoader"></div>
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="exampleModalLabel1">Update Session Timeout</h4>
                                    <p>You can update session timeout by providing systems's local IP address.</p>
                                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                                </div>
                                <div class="modal-body">
                                    <form id="ipSettingView">
                                      <div class="row showIPMain">
                                          <div class="col-md-6 pull-left">
                                            <p>Add Multiple IP’s <small>(max 5)</small></p>
                                          </div>
                                          <div class="col-md-6 txtRight pull-right">
                                            <span id="addmore_ip">+ Add More IP</span>
                                          </div>
                                          <div class="col-md-12" style="display:none;" id="sessionError">
                                              <span style="color:red;">Please enter Ip</span>
                                          </div>
                                        </div>
                                        <div class="row btCust boxDash">
                                            <div class="col-md-6 with_data">
                                                <!-- <label>Local Ip: </label> -->
                                                <div id="ipDom"></div>
                                            </div>
                                            <div class="col-md-6 with_data">
                                                <!-- <label>Time For Session (Minutes)</label> -->
                                                <div id="timeDom"></div>
                                            </div>
                                            <div class="col-md-12 without_data" style="display:none;">
                                              No Record's Found !
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                  <div class="row showWarning" style="display:block">
                                    <div class="col-md-6 pull-left">
                                      <img src="<?php echo BASE_URL ?>institution/img/sub-warning.svg" alt="">
                                      <p>Are you sure you want to update session timeout?</p>
                                    </div>
                                    <div class="col-md-6 txtRight pull-right">
                                      <button type="button" class="btn btn-success waves-effect" onclick="setSession();">Yes</button>
                                      <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Cancel</button>
                                    </div>
                                  </div>
                                  <div class="row showSucess" style="display:none">
                                      <div class="col-md-8 pull-left">
                                        <img src="<?php echo BASE_URL ?>institution/img/sub-sucess.svg" alt="">
                                        <p class="lineHeight35">Session Timeout update successfully.</p>
                                      </div>
                                      <div class="col-md-4 txtRight pull-right">
                                        <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
                                      </div>
                                  </div>
                                <!-- <button type="button" class="btn btn-success waves-effect" onclick="setSession();">Save</button> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ****************  -->

                    <!-- User Profile User -->
                    <div class="modal fade" id="profileUserPopUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                        <div class="loader" id="profileLoader"></div>
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="exampleModalLabel1">Contact Info</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                </div>
                                <div class="modal-body pad0">
                                    <div class="containerProfile">
                                      <div class="otherPImg onCallMain imageUser">
                                        <a href="javascript:void(0);">
                                          <img class="img-circle userDetails-avatar" src="<?php echo BASE_URL ?>institution/img/ava-single.png" onerror="this.src='<?php echo BASE_URL ?>institution/img/ava-single.png'" alt="" />
                                          <?php //echo $this->Html->image('images/users/2.jpg',array('class'=>'img-circle userDetails-avatar')); ?>
                                          <!-- <img class="userDetails-avatar profileUserAvatar" src="images/users/2.jpg" alt="user"> -->
                                        </a>
                                      </div>
                                      <div class="userProfile-info padCustom10-20 borderBottom leftFloatFull">
                                        <h3 class="profileUserName"></h3>
                                        <span class="onCallTxt otherProfText"></span>
                                      </div>
                                      <div class="clearFix"></div>
                                      <div class="profile_detail">
                                        <div class="userProfile-field userProfile-field_email borderBottom">
                                          <span class="userDetails-label userDetails-labelEmail">Email:</span>
                                          <span class="userDetails-labelEmail-value">
                                            <span class="userDetails-field-otheruser-email">
                                              <a target="_blank" href=""></a>
                                            </span>
                                          </span>
                                        </div>
                                        <div class="userProfile-field userProfile-field_phone">
                                          <span class="userDetails-label userDetails-labelPhone">Phone:</span>
                                          <span class="userDetails-labelPhone-value">
                                            <span class="userDetails-field-otheruser-phone">
                                              <span></span>
                                            </span>
                                          </span>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-success waves-effect">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ****************  -->

                    <!-- /.modal -->
                    <div class="tableTop floatLeft">
                      <!-- <span class="allIconUsers">
                          <?php //echo $this->Html->image('Institution.allUsers.svg'); ?>
                      </span> -->
                      <!-- <span class="card-title">All Users :</span>
                      <span class="display-6" id="showTotCount"></span> -->
                        <div class="tableTop floatLeft">
                          <div id="list-filter-options">
                            <input type="radio" name="filter_option" value="all" id="filter_option_all" checked="checked">
                            <label for="filter_option_all">All (<span id="showTotCount">0</span>)</label>
                            <input type="radio" name="filter_option" value="0" id="filter_option_await">
                            <label for="filter_option_await">Awaiting Approvals (<span id="showAwaitCount">0</span>)</label>
                            <input type="radio" name="filter_option" value="1" id="filter_option_approved">
                            <label for="filter_option_approved">Approved (<span id="showApprovedCount">0</span>)</label>
                            <input type="radio" name="filter_option" value="7" id="filter_option_new">
                            <label for="filter_option_new">Recently Registered (<span id="showNewCount">0</span>)</label>
                          </div>
                        </div>
                    </div>
                    <div id="userlisting_filter" class="dataTables_filter">
                      <label><i class="demo-icon icon-sub-lense"></i></label>
                        <input id="user_search" type="search" class="form-control input-sm" placeholder="Search" aria-controls="userlisting">
                    </div>
                    </div>
                    <input class="is-hidden" type="text" id="sort_order" name="" value="">
                    <input class="is-hidden" type="text" id="sort_value" name="" value="">
                    <!-- <span class="is-hidden" id="sort_order"></span> -->
                    <!-- <span class="is-hidden" id="sort_value"></span> -->
                    <div class="table-responsive wordBreak" id="ajaxContent">
                        <table class="table stylish-table customPading">
                            <thead>
                                <tr>
                                  <th class="selectiveCheck" style="width:1%; display: none;"><span id="selectAll">All</span></th>
                                  <th style="width:5%;">Photo </th>
                                  <th style="width:20%;" class="sort" data-sort="name"><span>Name</span> <img src="<?php echo BASE_URL; ?>institution/img/sort.svg"></th>
                                  <th style="width:25%;" class="sort" data-sort="email"><span>Email</span> <img src="<?php echo BASE_URL; ?>institution/img/sort.svg"></th>
                                  <th style="width:15%;" class="sort" data-sort="profession"><span>Profession</span> <img src="<?php echo BASE_URL; ?>institution/img/sort.svg"></th>
                                  <th style="width:15%;" class="sort" data-sort="registration_date"><span>Registration Date</span> <img src="<?php echo BASE_URL; ?>institution/img/sort.svg"></th>
                                  <th style="width:15%;" class="sort" data-sort="expiry_date"><span>Subscription Expiry Date</span> <img src="<?php echo BASE_URL; ?>institution/img/sort.svg"></th>
                                  <th style="width:5%;" class="sort" data-sort="status">User Status</th>
                                  <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                              <tr>
                                  <td colspan="8" align="center">
                                      <font color="green">Please wait while we are loading the list...</font>
                                  </td>
                              </tr>
                            </tbody>
                        </table>
                        <span id="totalCount" class="pull-right" style="display:none;"><?php echo $tCount; ?></span>
                        <div class="actions marBottom10 paddingSide15 leftFloat98per">
                        <?php
                            if(isset($data['user_list']) && count($data['user_list']) > 0){
                                echo $this->element('pagination');
                            }
                        ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!--                    End PAge Content                            -->
    <!-- ============================================================== -->

</div>
<?php
        echo $this->Js->writeBuffer();
     ?>
<script>
    $('.onoffswitch').click(function(){
        var chckOnCall=$('#atwork').is(':checked');
    if(chckOnCall==true){
       console.log('1');
       }else{
           console.log('0');
       }
    });
</script>

<script>
    $(document).ready(function () {

        $('body').on('click','th.sort',function(){
          var list_type = $('input[name="filter_option"]:checked').val();
          var limit = $('#Data_Count').val();

          var order = $('#sort_order').val();
          var value = $('#sort_value').val();
          var new_value = $(this).attr('data-sort').trim();

          var new_order = 'DESC';
          if(order == 'DESC'){
            new_order = 'ASC';
          }else{
            new_order = 'DESC';
          }
          if(value != new_value){
            new_order = 'ASC';
          }
          $('#sort_order').val(new_order);
          $('#sort_value').val(new_value);

          getDashboard('1',limit,$input.val().trim(),list_type);
        });

        $('body').on('click','input[name="filter_option"]',function(){
          $('#type_val').html($(this).val());
          getDashboard('1','20',$input.val().trim(),$(this).val());
        });

        $("#sidebarnav>li#menu2").addClass('active').find('a').eq(0).addClass('active');
        $("#sidebarnav>li#menu2").find("li").eq(0).addClass('active').find('a').addClass('active');


        $("li.dropdown .dropdown-menu li a").click(function (e) {
            e.stopPropagation();
        })

        var text_max = 500;
        $('#counter').html(text_max + ' characters remaining');

        $('#textMessage').keyup(function() {

          if($('#counter').css('display') == 'block'){
            $('#exampleModal2 .showSucess').hide();
            var text_length = $('#textMessage').val().length;
            var text_remaining = text_max - text_length;

            $('#counter').html(text_remaining + ' characters remaining');
            if(text_max == text_remaining){
              $('#exampleModal2 .showWarning').hide();
            }else{
              $('#exampleModal2 .showWarning').show();
            }
          }
        });

        $(function () {
          $("#datepicker").datetimepicker({
              format: 'DD-MM-YYYY',
              useCurrent: false
              // defaultDate: new Date(),
          });

          $("#datepicker").on("dp.change", function (e) {
            console.log(e);
              $('#customSubscriptionPopUp .showDefault').hide();
              $('#customSubscriptionPopUp .showWarning').show();
              $('#customSubscriptionPopUp .showSucess').hide();
          });
        });

        $('body').on('input','#sessionPopUp input',function(){
          $('#sessionPopUp .showWarning').show();
          $('#sessionPopUp .showSucess').hide();
        });
        $('body').on('click','#addmore_ip',function(event){
          event.preventDefault();
          event.stopPropagation();
            if($('#ipDom').html() == ''){
              $('#sessionPopUp .with_data').show();
              $('#sessionPopUp .without_data').hide();
            }
            var ip = $('#ipDom input');
            var time = $('#timeDom input');
            var ipReg = new RegExp("^(?!0)(?!.*\\.$)((1?\\d?\\d|25[0-5]|2[0-4]\\d)(\\.|$)){4}$");
            var returnIp = true;
            var returnTime = true;
            var ip_array = [];
            ip.each(function() {
                // if(ip_array.includes($(this).val())){
                if(ip_array.indexOf($(this).val()) > -1){
                    $('#sessionError').show().find('span').html("Duplicate entery not allowed");
                    returnIp = false;
                }else{
                    if($(this).val() == ''){
                      $('#sessionError').show().find('span').html("Please enter IP address");
                      returnIp = false;
                    }else{
                      if(!ipReg.test($(this).val())){
                          $('#sessionError').show().find('span').html("Please enter a valid IP address");
                          returnIp = false;
                      }else{
                          ip_array.push($(this).val());
                      }
                    }
                }
            });

            if(returnIp == true){
              time.each(function() {
                if($(this).val() == ''){
                  $('#sessionError').show().find('span').html("Please enter time");
                  returnTime = false;
                }
              });
            }

            if(returnIp == true && returnTime == true){
              if(ip.length < 5){
                timeHtml = '<div class="session_request_data time_dom_element input'+window.removeIpCount+'">';
                timeHtml += '<div class="time">';
                timeHtml += '<input type="text" class="form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="Time">';
                timeHtml += '</div>';
                timeHtml += '</div>';
                timeHtml += '<div class="remove_ip remove'+(window.removeIpCount)+'" data-input="input'+(window.removeIpCount)+'"><i class="demo-icon icon-close_removefile_icon"></i></div>';

                ipHtml = '<div class="session_request_data ip_dom_element input'+window.removeIpCount+'">';
                ipHtml += '<div class="ipadd">';
                ipHtml += '<input type="text" class="form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57)||(event.charCode==46);" placeholder="IP address">';
                ipHtml += '</div>';
                ipHtml += '</div>';
                window.removeIpCount++;

                $('#ipDom').prepend(ipHtml);
                $('#timeDom').prepend(timeHtml);
                $('#sessionPopUp .showWarning').show();
                $('#sessionPopUp .showSucess').hide();
                $('#sessionError').hide();
              }
              else{
                $('#sessionError').show().find('span').html("Maximum 5 IP's are allowed.");
              }
            }

        });

        $('body').on('click','.remove_ip',function(){
            var cust_class = $(this).attr('data-input');
            $('#ipDom').find('div.'+cust_class).remove();
            $('#timeDom').find('div.'+cust_class).remove();
            $(this).remove();
            if($('#ipDom').html() == ''){
              $('#sessionPopUp .with_data').hide();
              $('#sessionPopUp .without_data').show();
            }
            $('#sessionPopUp .showWarning').show();
            $('#sessionPopUp .showSucess').hide();
        });

        $('#myForm input').on('change', function() {

            $(".modal-content").addClass('loaderBg');

            var atWorkVal = $('#atwork').val();

            if(atWorkVal == 0){
                atWorkStatusVal = 1;
            }else{
                atWorkStatusVal = 0;
                $("#oncall").prop('checked' , false);
            }

            $("#onCallError").hide();
            var userId = $('#availableStatusPopup').attr('data-user');
            $('#oncallatworkLoader').fadeIn('fast');
            $.ajax({
                url:'<?php echo BASE_URL; ?>institution/InstitutionHome/updateAtWorkStatus',
                type: 'POST',
                data: {'user_id': userId, 'status': atWorkStatusVal},
                success: function( result ){

                    $('#oncallatworkLoader').fadeOut('fast');
                    $("#customBox .loaderImg").hide();
                    var responseArray = JSON.parse(result);
                    if( responseArray['status'] == 1 ){
                        // $.toast({
                        //     heading: 'Success',
                        //     text: 'Status changed successfully',
                        //     position: 'top-right',
                        //     loaderBg:'#2c2b2e',
                        //     icon: 'success',
                        //     hideAfter: 3000,
                        //     stack: 6
                        // });
                        var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                        $('#customModalLabel').html('Confirmation');
                        $('#customModalBody').html('Status changed successfully');
                        $('#customModalFoot').html(buttonHtml);
                        $('#customBox').modal('show');

                        $("#atwork").val(responseArray['data']['at_work']);
                        if(responseArray['data']['on_call'] == 0){
                            $("#oncall").prop('checked', false);
                        }
                        $("#oncall").val(responseArray['data']['on_call']);

                        $(".modal-content").removeClass('loaderBg');
                    }else{
                        // $.toast({
                        //     heading: 'Error',
                        //     text: 'An error has occurred. Please try again.',
                        //     position: 'top-right',
                        //     loaderBg:'#2c2b2e',
                        //     icon: 'error',
                        //     hideAfter: 3000,
                        //     stack: 6
                        // });
                        var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                        $('#customModalLabel').html('Error');
                        $('#customModalBody').html('An error has occurred. Please try again.');
                        $('#customModalFoot').html(buttonHtml);
                        $('#customBox').modal('show');
                    }
                },
                error: function( result ){
                    $('#oncallatworkLoader').fadeOut('fast');
                    $("#customBox .loaderImg").hide();
                    // $.toast({
                    //     heading: 'Error',
                    //     text: 'An error has occurred. Please try again.',
                    //     position: 'top-right',
                    //     loaderBg:'#2c2b2e',
                    //     icon: 'error',
                    //     hideAfter: 3000,
                    //     stack: 6
                    // });
                    var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                    $('#customModalLabel').html('Error');
                    $('#customModalBody').html('An error has occurred. Please try again.');
                    $('#customModalFoot').html(buttonHtml);
                    $('#customBox').modal('show');
                }
            });
        });

        $('#availableBtn').on('click',function(){
            var atWorkVal = $('#availableBtn').attr('data-value');
            var totalBaton = $('#availableStatusPopup').attr('data-totalbaton');
            var dnd = $('#availableStatusPopup').attr('data-dnd');
            $("#onCallError").hide();
            if(atWorkVal == 0){
                atWorkStatusVal = 1;
                $('#availableBtn').addClass('blueBt').attr('data-value',1);
                $('#availableBtn').html('Available');
            }else{
              if(totalBaton > 0){
                // alert('User having active Baton Role(s).');
                $("#onCallError").show();
                $("#onCallError").html("User having active Baton Role(s).");
                // $('#onCllBtn').removeClass('greenBt').attr('data-value',0);
                // $('#onCllBtn').html('Not On Call');
                return false;
              }

              if(dnd != 0){
                // alert('User having active Baton Role(s).');
                $("#onCallError").show();
                $("#onCallError").html("User is on Do Not Disturb.");
                // $('#onCllBtn').removeClass('greenBt').attr('data-value',0);
                // $('#onCllBtn').html('Not On Call');
                return false;
              }
                atWorkStatusVal = 0;
                $('#availableBtn').removeClass('blueBt').attr('data-value',0);
                $('#availableBtn').html('Not Available');

                $('#onCllBtn').removeClass('greenBt').attr('data-value',0);
                $('#onCllBtn').html('Not On Call');
            }
            $('#availableStatusPopup .showWarning').show();
            $('#availableStatusPopup .showSucess').hide();
        });

        $('#onCllBtn').on('click',function(){
            var atWorkVal = $('#availableBtn').attr('data-value');
            var subscription_type = $('#availableStatusPopup').attr('data-subsription');
            var professionid = $('#availableStatusPopup').attr('data-profession');
            var userId = $('#availableStatusPopup').attr('data-user');
            var onCallVal = $('#onCllBtn').attr('data-value');
            var atWorkStatusVal = atWorkVal;
            var oncallbaton = $('#availableStatusPopup').attr('data-oncallbaton');

            $("#onCallError").hide();


            if(subscription_type == 2){
                $("#onCallError").show();
                $("#onCallError").html("You can't go 'On Call' status in 'Grace Peroid'.");
                $('#onCllBtn').removeClass('greenBt').attr('data-value',0);
                $('#onCllBtn').html('Not On Call');
                return false;
            }else if(professionid == 4 || professionid == 5 || professionid == 6 || professionid == 7){
                $("#onCallError").show();
                $("#onCallError").html("Student Can't go 'On Call'.");
                $('#onCllBtn').removeClass('greenBt').attr('data-value',0);
                $('#onCllBtn').html('Not On Call');
                return false;
            }else{
              if(onCallVal == 0){
                $('#onCllBtn').addClass('greenBt').attr('data-value',1);
                $('#onCllBtn').html('On Call');
                $('#availableBtn').addClass('blueBt').attr('data-value',1);
                $('#availableBtn').html('Available');
              }else{
                if(oncallbaton > 0){
                  // alert('User having active On Call Baton Role(s).');
                  $("#onCallError").show();
                  $("#onCallError").html("User having active On Call Baton Role(s).");
                  return false;
                }
                $('#onCllBtn').removeClass('greenBt').attr('data-value',0);
                $('#onCllBtn').html('Not On Call');
              }
            }

            $('#availableStatusPopup .showWarning').show();
            $('#availableStatusPopup .showSucess').hide();
        });

        // function updateUserProfile(){
        $('#updateUserProfile').on('click',function(){
            var atWorkVal = $('#availableBtn').attr('data-value');
            var subscription_type = $('#availableStatusPopup').attr('data-subsription');
            var professionid = $('#availableStatusPopup').attr('data-profession');
            var userId = $('#availableStatusPopup').attr('data-user');
            var onCallVal = $('#onCllBtn').attr('data-value');

            $(".modal-content").addClass('loaderBg');

            $('#oncallatworkLoader').fadeIn('fast');
            $.ajax({
                url:'<?php echo BASE_URL; ?>institution/InstitutionHome/updateOnCallStatus',
                type: 'POST',
                data: {'user_id':userId, 'on_status': onCallVal, 'at_status': atWorkVal},
                success: function( result ){
                    $('#oncallatworkLoader').fadeOut('fast');
                    $("#customBox .loaderImg").hide();
                    console.log(result);
                    var responseArray = JSON.parse(result);
                    if( responseArray['status'] == 1 ){
                      $('#availableStatusPopup .showWarning').hide();
                      $('#availableStatusPopup .showSucess').show();

                        // var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                        // $('#customModalLabel').html('Confirmation');
                        // $('#customModalBody').html('Status changed successfully');
                        // $('#customModalFoot').html(buttonHtml);
                        // $('#customBox').modal('show');
                        $('#availableBtn').attr('data-value',responseArray['data']['at_work']);
                        $('#onCllBtn').attr('data-value',responseArray['data']['on_call']);

                        $(".modal-content").removeClass('loaderBg');
                    }else{

                        var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                        $('#customModalLabel').html('Error');
                        $('#customModalBody').html('An error has occurred. Please try again.');
                        $('#customModalFoot').html(buttonHtml);
                        $('#customBox').modal('show');

                    }
                },
                error: function( result ){
                    $('#oncallatworkLoader').fadeOut('fast');
                    $("#customBox .loaderImg").hide();

                    var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                    $('#customModalLabel').html('Error');
                    $('#customModalBody').html('An error has occurred. Please try again.');
                    $('#customModalFoot').html(buttonHtml);
                    $('#customBox').modal('show');

                }
            });
        });

        $('#onCallForm input').on('change', function() {
            var atWorkVal = $('#atwork').val();
            var subscription_type = $('#availableStatusPopup').attr('data-subsription');
            var professionid = $('#availableStatusPopup').attr('data-profession');
            var userId = $('#availableStatusPopup').attr('data-user');
            var onCallVal = $('#oncall').val();
            var atWorkStatusVal = atWorkVal;
            // if(atWorkVal == 0){
            //     // $("#onCallError").show();
            //     // $("#onCallError").html("Please select 'Available' before setting 'On Call status'.");
            //     // $("#oncall").prop('checked' , false);
            //     // return false;
            //     atWorkStatusVal = 1;
            // }else
            if(subscription_type == 2){
                $("#onCallError").show();
                $("#onCallError").html("You can't go 'On Call' status in 'Grace Peroid'.");
                $("#oncall").prop('checked' , false);
                return false;
            }else if(professionid == 4 || professionid == 5 || professionid == 6 || professionid == 7){
                $("#onCallError").show();
                $("#onCallError").html("Student Can't go 'On Call'.");
                $("#oncall").prop('checked' , false);
                return false;
            }else{
                $(".modal-content").addClass('loaderBg');

                atWorkStatusVal = 1;
                $("#atwork").prop('checked', true);

                if(onCallVal == 0){
                    onCallStatusVal = 1;
                }else{
                    onCallStatusVal = 0;
                }

                $('#oncallatworkLoader').fadeIn('fast');
                $.ajax({
                    url:'<?php echo BASE_URL; ?>institution/InstitutionHome/updateOnCallStatus',
                    type: 'POST',
                    data: {'user_id':userId, 'on_status': onCallStatusVal, 'at_status': atWorkStatusVal},
                    success: function( result ){
                        $('#oncallatworkLoader').fadeOut('fast');
                        $("#customBox .loaderImg").hide();
                        console.log(result);
                        var responseArray = JSON.parse(result);
                        if( responseArray['status'] == 1 ){
                            // $.toast({
                            //     heading: 'Success',
                            //     text: 'Status Changed successfully',
                            //     position: 'top-right',
                            //     loaderBg:'#2c2b2e',
                            //     icon: 'success',
                            //     hideAfter: 3000,
                            //     stack: 6
                            // });
                            var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                            $('#customModalLabel').html('Confirmation');
                            $('#customModalBody').html('Status changed successfully');
                            $('#customModalFoot').html(buttonHtml);
                            $('#customBox').modal('show');

                            $("#atwork").val(responseArray['data']['at_work']);
                            $("#oncall").val(responseArray['data']['on_call']);

                            $(".modal-content").removeClass('loaderBg');
                        }else{
                            // $.toast({
                            //     heading: 'Error',
                            //     text: 'An error has occurred. Please try again.',
                            //     position: 'top-right',
                            //     loaderBg:'#2c2b2e',
                            //     icon: 'error',
                            //     hideAfter: 3000,
                            //     stack: 6
                            // });
                            var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                            $('#customModalLabel').html('Error');
                            $('#customModalBody').html('An error has occurred. Please try again.');
                            $('#customModalFoot').html(buttonHtml);
                            $('#customBox').modal('show');

                        }
                    },
                    error: function( result ){
                        $('#oncallatworkLoader').fadeOut('fast');
                        $("#customBox .loaderImg").hide();
                        // $.toast({
                        //     heading: 'Error',
                        //     text: 'An error has occurred. Please try again.',
                        //     position: 'top-right',
                        //     loaderBg:'#2c2b2e',
                        //     icon: 'error',
                        //     hideAfter: 3000,
                        //     stack: 6
                        // });
                        var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                        $('#customModalLabel').html('Error');
                        $('#customModalBody').html('An error has occurred. Please try again.');
                        $('#customModalFoot').html(buttonHtml);
                        $('#customBox').modal('show');

                    }
                });
            }
        });
    });

    $(function () {
      $('#profileUserPopUp').on('show.bs.modal', function (event) {
        $(".modal-content").addClass('loaderBg');
        $('#profileLoader').fadeIn('fast');

        $('.imageUser img').attr('src','<?php echo BASE_URL; ?>institution/img/ava-single.png');
        $('.userProfile-info .profileUserName').html('');
        $('.userDetails-field-otheruser-email a').attr('href','mailto:'+'').html('');
        $('.userDetails-field-otheruser-phone').html('');
        $('.imageUser').removeClass('onCallMain').removeClass('notAtWork');
        $('.onCallTxt.otherProfText').html('');

        var button = $(event.relatedTarget);
        var userid = button.data('user');

        $.ajax({
            url:'<?php echo BASE_URL; ?>institution/InstitutionHome/getUserProfile',
            type: 'POST',
            data: {'user_id': userid},
            success: function( result ){
                $('#profileLoader').fadeOut('fast');
                var responseArray = JSON.parse(result);
                if( responseArray['status'] == 1 ){

                    var id = responseArray['data']['id'];
                    var email = responseArray['data']['email'];
                    var phone = responseArray['data']['phone'];
                    var UserName = responseArray['data']['UserName'];
                    var profile_img = responseArray['data']['profile_img'] !=""?responseArray['data']['profile_img']: '<?php echo BASE_URL; ?>institution/img/ava-single.png';
                    var role_status = responseArray['data']['role_status'];
                    var AtWorkStatus = responseArray['data']['AtWorkStatus'];
                    var OnCallStatus = responseArray['data']['OnCallStatus'];
                    var cust_class = responseArray['data']['cust_class'];
                    $('.imageUser').addClass(cust_class);

                    $('.imageUser img').attr('src',profile_img);
                    $('.userProfile-info .profileUserName').html(UserName);
                    $('.userDetails-field-otheruser-email a').attr('href','mailto:'+email).html(email);
                    $('.userDetails-field-otheruser-phone').html(phone);
                    $('.onCallTxt.otherProfText').html(role_status);

                    $(".modal-content").removeClass('loaderBg');
                }
            },
            error: function( result ){
                $('#profileLoader').fadeOut('fast');
            }
        });
      });
    });
    $(function () {
      $('#privilegePopUp').on('show.bs.modal', function (event) {

        var button = $(event.relatedTarget); // Button that triggered the modal
        var email = button.data('useremail'); // Extract info from data-* attributes
        var userid = button.data('userid'); // Extract info from data-* attributes
        var adminEmail = "<?php echo (string)$_COOKIE['adminEmail']; ?>";
        $('#privilegePopUp .showWarning').hide();
        $('#privilegePopUp .showSucess').hide();

        if(email == adminEmail){
            var buttonHtml = '<button type="button" onClick="closePop();" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
            $('#customModalLabel').html('Error');
            $('#customModalBody').html('You can not perform this action. Please contact to support@medicbleep.com');
            $('#customModalFoot').html(buttonHtml);
            $('#customBox').modal('show');
            $("#customBox .loaderImg").hide();
            return ;
        }
        $('#priviligeLoader').fadeIn('fast');
        $('#privilegePopUp').attr('data-userEmail',email);
        $('#privilegePopUp input[name="privilige"]').prop('checked', false);;
        $.ajax({
            url:'<?php echo BASE_URL; ?>institution/InstitutionHome/getUserPrivilegeStatus',
            type: 'POST',
            data: {'user_email': email, 'user_id': userid},
            success: function( result ){
                $('#priviligeLoader').fadeOut('fast');
                console.log(result);
                var responseArray = JSON.parse(result);
                if( responseArray['status'] == 1 ){
                    // if(responseArray['data']['role_id'] != 'null'){
                    $('#privilegePopUp input[name="privilige"][value="'+responseArray['data']['role_id']+'"]').prop('checked', true);
                    $('#privilegePopUp').attr('data-privilige',responseArray['data']['role_id']);
                    // }
                }
            },
            error: function( result ){
                $('#priviligeLoader').fadeOut('fast');
            }
        });
      });
    });


    $(function () {
      $('#exampleModalQr').on('show.bs.modal', function (event) {
        $('#exampleModalQr .showWarning').hide();
        $('#exampleModalQr .showSucess').hide();
      });

      $('#exampleModal2').on('show.bs.modal', function (event) {
        $('#exampleModal2 .showWarning').hide();
        $('#exampleModal2 .showSucess').hide();
        $('#textMessage').attr('readonly',false);
        $('#counter').show();
        $("#preRequestBtn1").css('background-color','#10a5da','border','#10a5da');
      });
      $('#sessionPopUp').on('show.bs.modal', function (event) {
        $('#sessionLoader').fadeIn('fast');
        window.removeIpCount = 1;
        var ipHtml = '';
        var timeHtml = '';

        var button = $(event.relatedTarget); // Button that triggered the modal
        var email = button.data('useremail'); // Extract info from data-* attributes
        var userid = button.data('userid'); // Extract info from data-* attributes
        $('.session_request_data').removeClass('new').removeClass('old');
        $('#sessionError').hide();
        $('#sessionPopUp .showWarning').hide();
        $('#sessionPopUp .showSucess').hide();

        $('#ipDom, #timeDom').removeClass('has-danger');
        $('#sessionLoader').fadeIn('fast');
        $('#sessionPopUp').attr('data-userId',userid);
        $('#sessionPopUp').attr('data-userEmail',email);
        $('#sessionPopUp input').val('');

        $('#ipDom').html('');
        $('#timeDom').html('');
        $('.with_data').hide();
        $('.without_data').show();

        window.removeIpCount++;

        $.ajax({
            url:'<?php echo BASE_URL; ?>institution/InstitutionHome/getUserSessionTimeout',
            type: 'POST',
            data: {'user_email': email, 'user_id': userid},
            success: function( result ){
                $('#sessionLoader').fadeOut('fast');
                var responseArray = JSON.parse(result);

                if( responseArray['status'] == 1 ){
                  $('.session_request_data').addClass('old');
                  if(responseArray['data'].length > 0){
                    $('.with_data').show();
                    $('.without_data').hide();
                      window.removeIpCount = 1;
                      ipHtml = '';
                      timeHtml = '';
                      $.each(responseArray['data'], function(index,ipsetting) {
                          timeHtml += '<div class="session_request_data time_dom_element input'+window.removeIpCount+'">';
                          timeHtml += '<div class="time">';
                          timeHtml += '<input type="text" value="'+ipsetting['UserIpSetting']['time_span']+'" class="form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="Time">';
                          timeHtml += '</div>';
                          timeHtml += '</div>';
                          timeHtml += '<div class="remove_ip remove'+(window.removeIpCount)+'" data-input="input'+(window.removeIpCount)+'"><i class="demo-icon icon-close_removefile_icon"></i></div>';

                          ipHtml += '<div class="session_request_data ip_dom_element input'+window.removeIpCount+'">';
                          ipHtml += '<div class="ipadd">';
                          ipHtml += '<input type="text" value="'+ipsetting['UserIpSetting']['ip']+'" class="form-control " onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57) || (event.charCode==46);" placeholder="IP address">';
                          ipHtml += '</div>';
                          ipHtml += '</div>';
                          window.removeIpCount++;
                      });
                      $('#ipDom').html(ipHtml);
                      $('#timeDom').html(timeHtml);
                    }
                }else{
                    $('.session_request_data').addClass('new');
                }
            },
            error: function( result ){
                $('#sessionLoader').fadeOut('fast');
            }
        });
      });
    });

    function setSession(){
        var ip = $('#ipDom input');
        var time = $('#timeDom input');
        var ipReg = new RegExp("^(?!0)(?!.*\\.$)((1?\\d?\\d|25[0-5]|2[0-4]\\d)(\\.|$)){4}$");
        var returnIp = true;
        var returnTime = true;
        var ip_array = [];
        var user_id = $('#sessionPopUp').attr('data-userId');

        ip.each(function() {
          // .indexOf(value) > -1
            // if(ip_array.includes($(this).val())){
            if(ip_array.indexOf($(this).val()) > -1){
                $('#sessionError').show().find('span').html("Duplicate entery not allowed");
                returnIp = false;
            }else{
                if($(this).val() == ''){
                  $('#sessionError').show().find('span').html("Please enter IP address");
                  returnIp = false;
                }else{
                  if(!ipReg.test($(this).val())){
                      $('#sessionError').show().find('span').html("Please enter a valid IP address");
                      returnIp = false;
                  }else{
                      ip_array.push($(this).val());
                  }
                }
            }
        });

        if(returnIp == true){
          time.each(function() {
            if($(this).val() == ''){
              $('#sessionError').show().find('span').html("Please enter time");
              returnTime = false;
            }
          });
        }


        if(returnIp == true && returnTime == true){
          $('#sessionError').hide();
            setSessionFinal(user_id);
        }
    }
    // function setSession(){
    //     var ip = $('#ipDom input').val();
    //     var time = $('#timeDom input').val();
    //     var ipReg = new RegExp("^(?!0)(?!.*\\.$)((1?\\d?\\d|25[0-5]|2[0-4]\\d)(\\.|$)){4}$");
    //     var returnIp = false;
    //     var returnTime = false;
    //     var user_id = $('#sessionPopUp').attr('data-userId');

    //     $('#ipDom, #timeDom').removeClass('has-danger');
    //     if(!ipReg.test(ip)){
    //         $('#ipDom').addClass('has-danger');
    //     }else{
    //         $('#ipDom').removeClass('has-danger');
    //         returnIp = true;
    //     }

    //     if(time == ""){
    //         $('#timeDom').addClass('has-danger');
    //     }else{
    //         $('#timeDom').removeClass('has-danger');
    //         returnTime = true;
    //     }

    //     if(returnIp == true && returnTime == true){
    //         var buttonHtml = '<button type="button" class="btn btn-info waves-effect " onclick="setSessionFinal(\''+ip+'\', '+time+','+user_id+');" >Save</button>';
    //         buttonHtml += '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Cancel</button>';

    //         $('#customModalLabel').html('Remove User');
    //         $('#customModalBody').html('Are you sure you want to save this session time for this IP address?');
    //         $('#customModalFoot').html(buttonHtml);
    //         $('#customBox').modal('show');
    //     }
    // }

    function setSessionFinal(user_id){

        var userId = user_id;
        var ip = $('#ipDom input');
        var time = $('#timeDom input');
        var ip_array = [];
        var time_array = [];
        var finalData = [];
        ip.each(function() {
            ip_array.push($(this).val());
        });

        time.each(function() {
          time_array.push($(this).val());
        });

        for (var i = 0; i < ip_array.length; i++) {
            var jsonData = {};
            jsonData.ip = ip_array[i];
            jsonData.time = time_array[i];
            finalData.push(jsonData);
        }
        // $('#sessionPopUp').modal('hide');
        $("#mainLoader.loader").fadeIn("fast");
        $.ajax({
            url: '<?php echo BASE_URL; ?>institution/InstitutionHome/setSessionTimeout',
            type: 'POST',
            data: {'ip_time_data': JSON.stringify(finalData), 'user_id': userId},
            success: function( result ){
                $("#mainLoader.loader").fadeOut("fast");
                console.log(result);
                var responseArray = JSON.parse(result);
                if( responseArray['status'] == 1 ){
                  $('#sessionPopUp .showWarning').hide();
                  $('#sessionPopUp .showSucess').show();
                    // var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                    // $('#customModalLabel').html('Confirmation');
                    // $('#customModalBody').html('Session time updated successfully');
                    // $('#customModalFoot').html(buttonHtml);
                    // $('#customBox').modal('show');
                    //
                    // $("#customBox .loaderImg").hide();
                }else{

                   var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                   $('#customModalLabel').html('Error');
                   $('#customModalBody').html('An error has occurred. Please try again');
                   $('#customModalFoot').html(buttonHtml);
                   $('#customBox').modal('show');

                   $("#customBox .loaderImg").hide();
                }
            },
            error: function( result ){
                $("#mainLoader.loader").fadeOut("fast");

                var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                $('#customModalLabel').html('Error');
                $('#customModalBody').html('An error has occurred. Please try again');
                $('#customModalFoot').html(buttonHtml);
                $('#customBox').modal('show');

                $("#customBox .loaderImg").hide();
            }
        });
    }

    function closePop(){
        $('#privilegePopUp').modal('hide');
    }

    function setPrivilege(role_id){
        var privilige = $('#privilegePopUp').attr('data-privilige');
        if(privilige == role_id){
            return false;
        }
        $('#privilegePopUp .showWarning').show();
        $('#privilegePopUp .showSucess').hide();
    }
    function setPrivilege_final(){
        var email = $('#privilegePopUp').attr('data-userEmail');
        var privilige = $('#privilegePopUp').attr('data-privilige');
        var role_id = $('#privilegePopUp input[name="privilige"]:checked').val();
        $('#priviligeLoader').fadeIn('fast');
        $.ajax({
          url:'<?php echo BASE_URL; ?>institution/InstitutionHome/setUserPrivilege',
          type: "POST",
          data: {'email':email,'role_id':role_id},
          success: function(data) {
            console.log(data);
            var responseArray = JSON.parse(data);
            $('#priviligeLoader').fadeOut('fast');
            // $('#privilegePopUp').modal('toggle');
            // $("#customBox .loaderImg").hide();
            if(responseArray['status'] == 1){
                $('#privilegePopUp .showWarning').hide();
                $('#privilegePopUp .showSucess').show();
                $('#privilegePopUp').attr('data-privilige',role_id);
                // var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                // $('#customModalLabel').html('Confirmation');
                // // $('#customModalBody').html('Please wait while Requesting qr Code');
                // $('#customModalBody').html('User privilege changed successfully');
                // $('#customModalFoot').html(buttonHtml);
                // $('#customBox').modal('show');
            }else{
                var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                $('#customModalLabel').html('Error');
                $('#customModalBody').html(responseArray['message']);
                $('#customModalFoot').html(buttonHtml);
                $('#customBox').modal('show');
                $("#customBox .loaderImg").hide();
            }
          },
            error: function( result ){
                $('#priviligeLoader').fadeOut('fast');
                $("#customBox .loaderImg").hide();
                var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                $('#customModalLabel').html('Error');
                $('#customModalBody').html('An error has occurred. Please try again.');
                $('#customModalFoot').html(buttonHtml);
                $('#customBox').modal('show');
                $("#customBox .loaderImg").hide();
            }
        });
    }

    $(function () {
      $('#availableStatusPopup').on('hide.bs.modal', function (event) {
        if($('#availableStatusPopup .showSucess').css('display') == 'block'){
          reload();
        }
      });
      $('#availableStatusPopup').on('show.bs.modal', function (event) {
        $('#availableStatusPopup .showWarning').hide();
        $('#availableStatusPopup .showSucess').hide();
        $('#oncallatworkLoader').fadeIn('fast');
        $(".modal-content").addClass('loaderBg');
        $("#onCallError").hide();
        var button = $(event.relatedTarget); // Button that triggered the modal
        var subsriptiontype = button.data('subsriptiontype'); // Extract info from data-* attributes
        var professionid = button.data('professionid'); // Extract info from data-* attributes
        var userid = button.data('userid'); // Extract info from data-* attributes

        $.ajax({
            url:'<?php echo BASE_URL; ?>institution/InstitutionHome/getUserAvailableStatus',
            type: 'POST',
            data: {'user_id': userid},
            success: function( result ){
                $('#oncallatworkLoader').fadeOut('fast');
                console.log(result);
                var responseArray = JSON.parse(result);
                console.log(responseArray);
                if( responseArray['status'] == 1 ){

                    $('#availableStatusPopup').attr('data-user',userid);
                    $('#availableStatusPopup').attr('data-subsription',subsriptiontype);
                    $('#availableStatusPopup').attr('data-profession',professionid);

                    $('#availableStatusPopup').attr('data-totalbaton',responseArray['data']['total_baton']);
                    $('#availableStatusPopup').attr('data-oncallbaton',responseArray['data']['On_call_batons']);

                    $('#availableStatusPopup').attr('data-dnd',responseArray['data']['dnd']);

                    atwork = responseArray['data']['at_work'];
                    oncall = responseArray['data']['on_call'];

                    var modal = $(this);
                    if(atwork == 1){

                        modal.find('#availableBtn').addClass('blueBt').attr('data-value',atwork);
                        $('#availableBtn').addClass('blueBt').attr('data-value',atwork);
                        // $("#atwork").prop('checked', true);
                        $('#availableBtn').html('Available');
                    }else{
                        modal.find('#availableBtn').removeClass('blueBt').attr('data-value',atwork);
                        $('#availableBtn').removeClass('blueBt').attr('data-value',atwork);
                        // $("#atwork").prop('checked', false);
                        $('#availableBtn').html('Not Available');
                    }

                    if(oncall == 1){
                        modal.find('#onCllBtn').addClass('greenBt').attr('data-value',oncall);
                        $('#onCllBtn').addClass('greenBt').attr('data-value',oncall);
                        // $("#oncall").prop('checked', true);
                        $('#onCllBtn').html('On Call');
                    }else{
                        modal.find('#onCllBtn').removeClass('greenBt').attr('data-value',oncall);
                        $('#onCllBtn').removeClass('greenBt').attr('data-value',oncall);
                        // $("#oncall").prop('checked', false);
                        $('#onCllBtn').html('Not On Call');
                    }

                    $(".modal-content").removeClass('loaderBg');

                }
            },
            error: function( result ){
                $('#oncallatworkLoader').fadeOut('fast');
            }
        });
      });
    });
    $(function () {
      $('#customSubscriptionPopUp').on('hide.bs.modal', function (event) {
        if($('#customSubscriptionPopUp .showSucess').css('display') == 'block'){
          reload();
        }
      });
      $('#customSubscriptionPopUp').on('show.bs.modal', function (event) {
        if (event.namespace === 'bs.modal') {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var useremail = button.data('useremail'); // Extract info from data-* attributes
            var userid = button.data('userid'); // Extract info from data-* attributes
            var userSubscription = button.data('usersubscriptionexpiry'); // Extract info from data-* attributes
            var newdate = userSubscription.split(" ");
            var newdate1 = newdate[0].split("-").reverse().join("-");
            var companyExpiryDate = "<?php echo $expiry; ?>";
            $('#customSubscriptionPopUp').attr('data-subscription',newdate1);
            $('#customSubscriptionPopUp').attr('data-user',userid);
            $('#customSubscriptionPopUp').attr('data-userEmail',useremail);
            $("#usersubscriptionexpiryDate").html('User Subscription Expiry Date: ' + newdate1);

            $('#datepicker').data("DateTimePicker").date(new Date());
            $('#datepicker').data("DateTimePicker").minDate(moment().subtract(1,'d'));
            $('#datepicker').data("DateTimePicker").maxDate(new Date(parseInt(companyExpiryDate)));
            $('#customSubscriptionPopUp .showDefault').show();
            $('#customSubscriptionPopUp .showWarning').hide();
            $('#customSubscriptionPopUp .showSucess').hide();
        }

        //var modal = $(this);
        //modal.find('#expirydate').val(expirydate);
      });
    });
    $(function () {
      $('#customSubscriptionPopUp').on('hide.bs.modal', function (event) {
        if($('#customSubscriptionPopUp .showSucess').css('display') == 'block'){
          reload();
        }
      });
      $('#batonRolePopUp').on('show.bs.modal', function (event) {
        if (event.namespace === 'bs.modal') {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var userid = button.data('userid'); // Extract info from data-* attributes
            $('#batonRolePopUp').attr('data-id',userid);
            $('#batonRolePopUp .main_content').show();
            $('#batonRolePopUp .showWarning').hide();
            $('#batonRolePopUp .showSucess').hide();
            $('#batonRolePopUp .showBack').hide();
            $('#batonRolePopUp .baton_listing').hide();
            $('#batonRolePopUp #batonListing').hide();
            $('#batonRolePopUp #search_unoccupied_role').val('');
            var html = '';
            var status2 = 'ROLES';
            var status3 = 'STATUS';
            html += '<div class="col-md-12 batonRoleDiv">';
            html += '<div class="col-md-8">';
            html += '<span class="title">'+status2+'</span>';
            html += '</div>';
            html += '<div class="col-md-4">';
            html += '<span class="title">'+status3+'</span>';
            html += '</div>';
            html += '</div>';
            html += '<div class="col-md-12 batonRoleDiv">';
            html += '<div class="col-md-12">';
            html += '<span style="text-align: center;display: block;color:green;">Wait while loading ...</span>';
            html += '</div>';
            html += '</div>';
            $('#baton_role_content').html(html);
            manageBatonRoleList(userid);
        }
      });
    });

    function manageBatonRoleList(userid){
      var html = '';
      var status2 = 'ROLES';
      var status3 = 'STATUS';
      $('#batonRoleLoader').show();
      getUserBatonRole(userid,function(baton_data){
        html = '';
        html += '<div class="col-md-12 batonRoleDiv">';
        html += '<div class="col-md-8">';
        html += '<span class="title">'+status2+'</span>';
        html += '</div>';
        html += '<div class="col-md-4">';
        html += '<span class="title">'+status3+'</span>';
        html += '</div>';
        html += '</div>';
        if(baton_data.length > 0){
            for (var i = 0; i < baton_data.length; i++) {
              disabled_condition = baton_data[i].type != '0' ? 'disabled="true"' : '';
              if(baton_data[i].other_user_name.trim() == ''){
                baton_data[i].other_user_name = "Switchboard";
              }
              status =  baton_data[i].type == '1' ?
                        'Assigned' : baton_data[i].type == '2' ?
                        'Requested from '+baton_data[i].other_user_name : baton_data[i].type == '3' ?
                        'Request awaiting' : baton_data[i].type == '4' ?
                        'Transfer awaiting' : baton_data[i].type == '5' ?
                        'Transfer to '+baton_data[i].other_user_name : baton_data[i].type == '6' ?
                        'Request rejected' : baton_data[i].type == '7' ?
                        'Transfer rejected' : baton_data[i].type == '8' ?
                        'Request timeout' : baton_data[i].type == '9' ?
                        'Transfer rejected' : 'None';

              html += '<div class="col-md-12 batonRoleDiv">';
              html += '<div class="col-md-8">';
              html += '<span>'+baton_data[i].role_name+'</span>';
              html += '</div>';
              html += '<div class="col-md-4">';
              if(baton_data[i].type == '0'){
                html += '<span class="unassignBatonBt" onClick="unassignBaton('+baton_data[i].role_id+');">'+status+'</span>';
              }else{
                html += '<span class="unassignBatonTxt" '+disabled_condition+'>'+status+'</span>';
              }
              html += '</div>';
              html += '</div>';
            }
        }else{
          html += '<div class="col-md-12 batonRoleDiv">';
          html += '<div class="col-md-12">';
          html += '<span style="text-align: center;display: block;color:red;">No Baton Role Selected</span>';
          html += '</div>';
          html += '</div>';
        }
          $('#batonRoleLoader').hide();
          $('#baton_role_content').html(html);
      });
    }

    function unassignBaton(role_id){
      var user_id = $('#batonRolePopUp').attr('data-id');
      $('#batonRolePopUp').attr('data-status',0);
      $('#batonRolePopUp').attr('data-roleid',role_id);
      $('#batonRolePopUp .showWarning').show();
      $('#batonRolePopUp .showSucess').hide();
    }

    function setBatonRole(){
      var user_id = $('#batonRolePopUp').attr('data-id');
      // var role_id = $('#baton_unoccupied_role div.selected li').data('id');
      var role_id = $('#batonRolePopUp').attr('data-roleid');
      var status = $('#batonRolePopUp').attr('data-status');
      changeUserBatonStatus(user_id,role_id,status);
    }

    function changeUserBatonStatus(user_id,role_id,status){
      $('#batonRoleLoader').show();
      $.ajax({
          url:'<?php echo BASE_URL; ?>institution/InstitutionBaton/assignUnassignBatonRole',
          type: "POST",
          data: {'user_id':user_id, 'role_id':role_id,'status':status},
          success: function(data) {
            console.log(data);
            var responseArray = JSON.parse(data);
            if(responseArray['status'] == 1){
              $role_input.val('');
              $('#batonListing').hide();
              $('#batonRolePopUp .showWarning').hide();
              $('#batonRolePopUp .showSucess').show();
              $('#batonRolePopUp .main_content').show();
              $('#batonRolePopUp .baton_listing').hide();
            }else{
              alert(responseArray['message']);
              searchRole();
            }
            $('#batonRoleLoader').hide();
            manageBatonRoleList(user_id);
          }
      });
    }
</script>
<script type="text/javascript">

 //setup before functions
 var typingTimer;                //timer identifier
 var typingTimerRole;                //timer identifier
 var doneTypingInterval = 1000;  //time in ms, 5 second for example
 var doneTypingIntervalBaton = 1500;  //time in ms, 5 second for example
 var $input = $('#user_search');
 var $role_input = $('#search_unoccupied_role');

 //on keyup, start the countdown
 $input.on('keyup', function () {
   clearTimeout(typingTimer);
   typingTimer = setTimeout(searchUser, doneTypingInterval);
 });
 $role_input.on('keyup', function () {
   clearTimeout(typingTimerRole);
   typingTimerRole = setTimeout(searchRole, doneTypingIntervalBaton);
 });

 //on keydown, clear the countdown
 $input.on('keydown', function () {
   clearTimeout(typingTimer);
 });
 $role_input.on('keydown', function () {
   clearTimeout(typingTimerRole);
 });

 //user is "finished typing," do something
 function searchUser() {
    $type = $('input[name="filter_option"]:checked').val();
    $search_text = $input.val().trim();
    if($search_text == ''){
        // return false;
    }
    $input.blur();
    getDashCount($search_text);
    $("#mainLoader.loader").fadeIn("fast");
    var sort_order = $('#sort_order').val().trim();
    var sort_value = $('#sort_value').val().trim();

    $('th.sort').removeClass('active');
    $('th.sort[data-sort="'+sort_value+'"]').addClass('active');
    $.ajax({
        url:'<?php echo BASE_URL; ?>institution/InstitutionHome/dashboardFilter',
        type: "POST",
        data: {'type':$type, 'search_text':$search_text, 'sort_order':sort_order, 'sort_value':sort_value},
        success: function(data) {
            $('#ajaxContent').html(data);
            $('#showTotCount').html($('#totalCount').html());
            $("#mainLoader.loader").fadeOut("fast");
            $('#assign_btn').hide();
            $('#unassign_btn').hide();

            $('th.sort').removeClass('active');
            $('th.sort[data-sort="'+sort_value+'"]').addClass('active');

            if($type == 0 || $type == 1){
                $('.selectiveCheck').show();
                if($type == 0){
                    $('#assign_btn').show();
                }else if($type == 1){
                    $('#unassign_btn').show();
                }
            }else{
                $('.selectiveCheck').hide();
            }
        }
    });
 }

 function searchRole() {
   try {window.searchBaton.abort();} catch (e) {}
   $('#batonRolePopUp .showWarning').hide();
   $('#batonRolePopUp .showSucess').hide();
    $search_text = $role_input.val().trim();
    if($search_text == ''){
      $('#batonListing').hide();
      return false;
    }
    $('#batonRoleLoader').show();
     window.searchBaton = $.ajax({
       url:'<?php echo BASE_URL; ?>institution/InstitutionHome/getUnoccupiedBatonRole',
       type: "POST",
       data: {'search_text':$search_text},
       success: function(data) {
         console.log(data);
         var responseArray = JSON.parse(data);
         if( responseArray['status'] == 1 ){
           var html = '';
           var list = responseArray['data'];
           var oncall = 'No';
           if(list.length > 0){
             html += '<div><span class="role_name head">Role Name</span><span class="role_type head">On Call</span></div>';
             for (var i = 0; i < list.length; i++) {
               if(list[i].on_call_value == 1){
                 oncall = 'Yes';
               }else{
                 oncall = 'No';
               }
               html +='<div><li class="role_name" data-id="'+list[i].id+'" data-oncall="'+list[i].on_call_value+'">'+list[i].name+'</li><li class="role_type">'+oncall+'</li></div>';
             }
           }else{
             html +='<div style="text-align: center;color: red;">No Record(s) Found!</div>';
           }
           $('#batonListing').show();
           $('#batonRoleLoader').hide();
           $('#baton_unoccupied_role').html(html);
         }
       }
     });
 }
 // --------- INITIAL FUNCTIONS ------------
$(document).ready(function(){
  $type = $('#type_val').html();
  $('input[name="filter_option"]').attr('checked',false);
  $('input[name="filter_option"][value="'+$type.trim()+'"]').attr('checked',true);
  var list_type = $('input[name="filter_option"]:checked').val();
  getDashboard('1','20',$input.val().trim(),list_type);
});

 // --------- INITIAL FUNCTIONS ------------

 function getDashboard(page,limit,$search_text,$type){
    getDashCount($search_text);
    // var $type = $('.container-fluid').attr('data-type');
    $("#mainLoader.loader").fadeIn("fast");
    $('input[name="filter_option"]').attr('checked',false);
    $('input[name="filter_option"][value="'+$type.trim()+'"]').attr('checked',true);

    var sort_order = $('#sort_order').val().trim();
    var sort_value = $('#sort_value').val().trim();
    $('th.sort').removeClass('active');
    $('th.sort[data-sort="'+sort_value+'"]').addClass('active');
    $.ajax({
      url:'<?php echo BASE_URL; ?>institution/InstitutionHome/dashboardFilter',
      type: "POST",
      data: {'type':$type.trim(), 'page_number':page, 'limit':limit, 'search_text':$search_text, 'sort_order':sort_order, 'sort_value':sort_value},
      success: function(data) {
        $('#ajaxContent').html(data);
        // $('#showTotCount').html($('#totalCount').html());
        $("#mainLoader.loader").fadeOut("fast");
        $(".loaderImg").fadeOut("fast");
        $('#assign_btn').hide();
        $('#unassign_btn').hide();

        $('th.sort').removeClass('active');
        if(sort_value == ''){
          $('th.sort[data-sort="registration_date"]').addClass('active');
        }else{
            $('th.sort[data-sort="'+sort_value+'"]').addClass('active');
        }

        if($type == 0 || $type == 1){
            $('.selectiveCheck').show();
            if($type == 0){
                $('#assign_btn').show();
            }else if($type == 1){
                $('#unassign_btn').show();
            }
        }else{
            $('.selectiveCheck').hide();
        }
      }
    });
 }

 function getUserBatonRole(user_id,callBack){
   var returnval = [];
    $.ajax({
      url:'<?php echo BASE_URL; ?>institution/InstitutionHome/getUserBatonRole',
      type: "POST",
      data: {'user_id':user_id},
      success: function(data) {
        console.log(data);
        var responseArray = JSON.parse(data);
        if( responseArray['status'] == 1 ){
          console.log(responseArray['data']);
          returnval = callBack(responseArray['data']);
        }
      }
    });
    return returnval;
 }

 // function getUnoccupiedBatonRole(callBack){
 //   var returnval = [];
 //    $.ajax({
 //      url:'<?php //echo BASE_URL; ?>institution/InstitutionHome/getUnoccupiedBatonRole',
 //      type: "POST",
 //      success: function(data) {
 //        console.log(data);
 //        var responseArray = JSON.parse(data);
 //        if( responseArray['status'] == 1 ){
 //          returnval = callBack(responseArray['data']);
 //        }
 //      }
 //    });
 //    return returnval;
 // }

 $('#addBaton').on('click',function(){
   $('#batonRolePopUp .showWarning').hide();
   $('#batonRolePopUp .showSucess').hide();
   $('#batonRolePopUp .main_content').hide();
   $('#batonRolePopUp .baton_listing').show();
   $('#batonRolePopUp .showBack').show();
   $('#batonRolePopUp #search_unoccupied_role').val('');
   $('#batonRolePopUp #batonListing').hide();
   $('#batonRolePopUp').attr('data-status',1);
 });
$('body').on('click','#batonRolePopUp .showBack button',function(){
  $('#batonRolePopUp .main_content').show();
  $('#batonRolePopUp .baton_listing').hide();
  $('#batonRolePopUp .showBack').hide();
});
$('body').on('click','#batonRolePopUp #baton_unoccupied_role li',function(){
// $('#batonRolePopUp #baton_unoccupied_role li').on('click',function(){
  $('#batonRolePopUp #baton_unoccupied_role div').removeClass('selected');
  $(this).parent('div').addClass('selected');
  $('#batonRolePopUp .showBack').hide();
  $('#batonRolePopUp .showSucess').hide();
  $('#batonRolePopUp .showWarning').show();
  $('#batonRolePopUp').attr('data-roleid',$(this).data('id'));
});
$('#batonRolePopUp .toMainContent').on('click',function(){
  $('#batonRolePopUp .showBack').hide();
  $('#batonRolePopUp .showWarning').hide();
  $('#batonRolePopUp .showSucess').hide();
  $('#batonRolePopUp .main_content').show();
  $('#batonRolePopUp .baton_listing').hide();
});

 function getListing($type){
    if($('.card[data-value="'+$type+'"]').hasClass('selected')){
        return false;
    }
    if($type == 'all'){
        custom_val = '';
    }else{
        custom_val = '/'+$type;
    }
    window.location.href =  "<?php echo BASE_URL; ?>institution/InstitutionHome/index" + custom_val;
 }

 function chngPage(val,limit) {
    var list_type = $('input[name="filter_option"]:checked').val();
    $search_text = $input.val().trim();
    var goVal=$("#chngPage").val();

    var sort_order = $('#sort_order').val().trim();
    var sort_value = $('#sort_value').val().trim();

    if(parseInt(goVal) <= Math.ceil(val) && Math.ceil(goVal)>0){
        $("#mainLoader.loader").fadeIn("fast");
        $.ajax({
            url:'<?php echo BASE_URL; ?>institution/InstitutionHome/dashboardFilter',
            type: "POST",
            data: {'page_number':goVal,'limit':limit,'type':list_type,'search_text':$search_text, 'sort_order':sort_order, 'sort_value':sort_value},
            success: function(data) {
                $('#ajaxContent').html(data);
                $('#showTotCount').html($('#totalCount').html());
                if(list_type == 0 || list_type == 1){
                    $('.selectiveCheck').show();
                }else{
                    $('.selectiveCheck').hide();
                }
                $("#mainLoader.loader").fadeOut("fast");
            }
        });
    }

 }
 function chngCount(val){
    $("#mainLoader.loader").fadeIn("fast");
    var sort_order = $('#sort_order').val().trim();
    var sort_value = $('#sort_value').val().trim();

    var list_type = $('input[name="filter_option"]:checked').val();
    $search_text = $input.val().trim();
  $.ajax({
      url:'<?php echo BASE_URL; ?>institution/InstitutionHome/dashboardFilter',
      type: "POST",
      data: {'limit':val, 'sort_order':sort_order, 'sort_value':sort_value,'type':list_type,'search_text':$search_text},
      success: function(data) {
          $('#ajaxContent').html(data);
          $('#showTotCount').html($('#totalCount').html());
          if(list_type == 0 || list_type == 1){
              $('.selectiveCheck').show();
          }else{
              $('.selectiveCheck').hide();
          }
          $("#mainLoader.loader").fadeOut("fast");
      }
  });
 }

function abc(val,limit){
    $("#mainLoader.loader").fadeIn("fast");
    var sort_order = $('#sort_order').val().trim();
    var sort_value = $('#sort_value').val().trim();

    var list_type = $('input[name="filter_option"]:checked').val();
    $search_text = $input.val().trim();
    console.log(list_type);
  $.ajax({
       url:'<?php echo BASE_URL; ?>institution/InstitutionHome/dashboardFilter',
       type: "POST",
       data: {'page_number':val,'limit':limit, 'sort_order':sort_order, 'sort_value':sort_value, 'type':list_type,'search_text':$search_text},
       success: function(data) {
            $('#ajaxContent').html(data);
            $('#showTotCount').html($('#totalCount').html());
            if(list_type == 0 || list_type == 1){
                $('.selectiveCheck').show();
            }else{
                $('.selectiveCheck').hide();
            }
           $("#mainLoader.loader").fadeOut("fast");
       }
    });
}

$('#input-file-now').change(function(){
    $("#userEmail").attr("disabled", "disabled");
    $("#addUserbtn1").hide();
    $("#addUserbtn2").show();

    $("#inputEmail").removeClass('has-danger');
    $("#emptyEmail").hide();
    $("#invalidEmail").hide();

    $("#uploadResult").hide();
    $("#uploadSuccess").hide();
    $("#uploadError").hide();

});

$("#userEmail").keyup(function() {
    var input = $(this);

    if( input.val() == "" ) {
      $("#addUserbtn1").show();
      $("#addUserbtn2").hide();
      $(".dropify-wrapper").removeClass('disable');
      $("#input-file-now").removeClass('disable');
    }
    else{
        $(".dropify-wrapper").addClass('disable');
        $("#input-file-now").addClass('disable');
        $("#addUserbtn1").show();
        $("#addUserbtn2").hide();
    }
});

$("#addUserButton").click(function(){
    $("#userEmail").val('');
    $("#inputEmail").removeClass('has-danger');
    $("#alreadyExist").hide();
    $("#emptyEmail").hide();
    $("#invalidEmail").hide();
    $(".dropify-wrapper").removeClass('disable');
    $("#input-file-now").removeClass('disable');
    $("#userEmail").attr("disabled", false);

    $(".dropify-clear").trigger("click");

});
$("#broadcastMessage").click(function(){
    $("#textMessage").val('');
    $("#messagePassword").val('');
    $("#inputMessage").removeClass('has-danger');
    $("#inputPassword").removeClass('has-danger');
    $("#emptyMessage").hide();
    $("#incorrectPassword").hide();
    var text_max = 500;
    $('#counter').html(text_max + ' characters remaining');
    $("#processingMessage").hide();
    $("#preRequestBtn1").removeAttr('disabled');
});
$('#preRequestBtn1').click(function() {
 $("#processingMessage").html('');
 var message = $('#textMessage').val().trim();
 if(message==""){
     $("#inputMessage").addClass('has-danger');
     $("#emptyMessage").show();
     return false;
 }else{
     $("#emptyMessage").hide();
     $("#inputPassword").removeClass('has-danger');
     $("#incorrectPassword").hide();
     $("#emptyPassword").hide();
     $("#messagePassword").val('');
     $("#inputMessage").removeClass('has-danger');
 }
 $('#postRequestBtn1').click();
});

$('#preConfirmBtn2').click(function() {
 var email= "<?php echo $_COOKIE['adminEmail']; ?>";
 var trsutId= "<?php echo $_COOKIE['adminInstituteId']; ?>";
 var message = $('#textMessage').val().trim();
 var password = $('#messagePassword').val().trim();
    if(password == ""){
        $("#inputPassword").addClass('has-danger');
        $("#emptyPassword").show();
        $("#incorrectPassword").hide();
        return false;
    }else{
        $("#processingMessage").show();
        $("#processingMessage").css('color','green').html('Please wait while broadcasting message...');
        $("#preRequestBtn1").attr('disabled','disabled');
        $("#preRequestBtn1").css('background-color','grey','border','grey');
        // $('#exampleModal2 .showWarning').hide();
        var subsIds = [];
        var companydetail = {company_id: trsutId};
        var companydetailJson = JSON.stringify(companydetail);
        console.log(password);
        $.ajax({
            url: '<?php echo BASE_URL; ?>institution/InstitutionHome/broadCastMessage',
            type: 'POST',
            data: {'password':password,'message':message},
            success: function( result ){
                $("#customBox .loaderImg").hide();
                var responseArray = JSON.parse(result);
                if( responseArray['status'] == 1 ){
                  if($('#preRequestBtn1').attr('disabled')){
                    $('#exampleModal2 #textMessage').attr('readonly',true);
                    $('#exampleModal2 #counter').hide();
                    $('#exampleModal2 .showSucess').show();
                    $('#exampleModal2 .showWarning').hide();
                    // $('#textMessage').val('');
                    $('#counter').html('500 characters remaining');
                    $("#processingMessage").hide();
                  }

                     // $.toast({
                     //    heading: 'Success',
                     //    text: 'Message Sent successfully',
                     //    position: 'top-right',
                     //    loaderBg:'#2c2b2e',
                     //    icon: 'success',
                     //    hideAfter: 3000,
                     //    stack: 6
                     //  });

                     // var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                     // $('#customModalLabel').html('Confirmation');
                     // $('#customModalBody').html('Message Sent successfully');
                     // $('#customModalFoot').html(buttonHtml);
                     // $('#customBox').modal('show');


                    $("#preRequestBtn1").removeAttr('disabled');
                    // $("#processingMessage").html("Message Sent.");
                    $("#preRequestBtn1").css('background-color','#10a5da','border','#10a5da');
                  }else if( responseArray['status'] == 2 ){
                    if($('#exampleModal2').hasClass('show')){
                      $('#exampleModal2').modal('hide');
                    }
                    var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                    $('#customModalLabel').html('Alert');
                    $('#customModalBody').html(responseArray['message']);
                    $('#customModalFoot').html(buttonHtml);
                    $('#customBox').modal('show');
                    $("#customBox .loaderImg").hide();
                  }else{
                    $("#processingMessage").css('color','red').html(responseArray['message']).show();
                    $("#preRequestBtn1").removeAttr('disabled');
                    $("#preRequestBtn1").css('background-color','#10a5da','border','#10a5da');
                }
            }
        });
    }
});
 $('#messagePassword').keypress(function(e){
     if(e.which == 13){//Enter key pressed
         $('#preConfirmBtn2').click();
         return false;
     }
 });

 function exportuserChat(userId){
   $("#mainLoader.loader").fadeIn("fast");
    window.location.href =  "<?php echo BASE_URL; ?>institution/InstitutionUser/exportChat/" + userId;
 }


 $("form#bulkEmailUpload").submit(function(){
     var formData = new FormData(this);
     $('#adduserLoader').fadeIn('fast');

    $.ajax({
        url: '<?php echo BASE_URL; ?>institution/InstitutionHome/addUserFileUpload',
        type: 'POST',
        data: formData,
        success: function (data) {
            console.log(data);
            $('#adduserLoader').fadeOut('fast');
            var responseArray = JSON.parse(data);
            if( responseArray['status'] == 1 ){
                $(".uploadFileBlock .loaderImg").css("display", "none");
                $("#uploadResult").show();
                $("#uploadSuccess").show();
                $("#uploadError").show();
                $("#uploadSuccess").html("Success: " + responseArray['data']['addUserData']['valid_email_count']);
                $("#uploadError").html("Error: " + responseArray['data']['addUserData']['invalid_email_count']);
            }
        },
        cache: false,
        contentType: false,
        processData: false
    });

     return false;
 });

 $('#requestQrCode').on('click',function(){
    $("#number-of-qr-code").val('');
    $("#validity").val(0);
 });

 $('body').on('input change','#number-of-qr-code, #validity',function(){
   $('#exampleModalQr .showWarning').show();
   $('#exampleModalQr .showSucess').hide();
 });

function requestQrCode(){
    var numberOfQrCode = $("#number-of-qr-code").val();
    var validity = $("#validity option:selected").val();
    if(numberOfQrCode == '' || numberOfQrCode == 0){
        alert('Enter a valid number.');
        return false;
    }
    if(numberOfQrCode > 1000){
        alert('Maximum limit is 1000.');
        return false;
    }
    if(validity == '' || validity == 0){
        alert('Select validity.');
        return false;
    }
    $(".loaderImg").show();
    $.ajax({
        url: '<?php echo BASE_URL; ?>institution/InstitutionQr/requestQrCode',
        type: 'POST',
        data: {'quantity': numberOfQrCode, 'validity': validity},
        success: function( result ){
            $(".loaderImg").hide();
            $("#customBox .loaderImg").hide();
            var responseArray = JSON.parse(result);
            if( responseArray['status'] == 1 ){
                // $.toast({
                //     heading: 'Requesting...',
                //     text: 'Please wait while Requesting qr Code',
                //     position: 'top-right',
                //     loaderBg:'#2c2b2e',
                //     icon: 'success',
                //     hideAfter: 3000,
                //     stack: 6
                // });

                // var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                // $('#customModalLabel').html('Confirmation');
                // // $('#customModalBody').html('Please wait while Requesting qr Code');
                // $('#customModalBody').html('QR code requested successfully');
                // $('#customModalFoot').html(buttonHtml);
                // $('#customBox').modal('show');
                $('#exampleModalQr .showWarning').hide();
                $('#exampleModalQr .showSucess').show();
                $("#number-of-qr-code").val('');
                $("#validity").val(0);

                // $('#exampleModalQr').modal('toggle');
            }else{
                var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                $('#customModalLabel').html('Error');
                $('#customModalBody').html('An error has occurred. Please try again.');
                $('#customModalFoot').html(buttonHtml);
                $('#customBox').modal('show');
                $('#exampleModalQr').modal('toggle');
            }
        },
        error: function( result ){
            $(".loaderImg").hide();
            var responseArray = JSON.parse(result);
                $("#inputEmail").addClass('has-danger');
                $("#alreadyExist").show();
                $("#alreadyExist").html('* '+responseArray['values']['message']+'.' );
                $("#emptyEmail").hide();
                $("#invalidEmail").hide();

        }
    });
}

function changeStatus(email, userId, status){
    $("#mainLoader.loader").fadeIn("fast");
    $.ajax({
        url: '<?php echo BASE_URL; ?>institution/InstitutionHome/changeAssignStatus',
        type: 'POST',
        data: {'email':email, 'user_id': userId, 'status': status},
        success: function( result ){
            $("#mainLoader.loader").fadeOut("fast");
            $("#customBox .loaderImg").hide();

            var responseArray = JSON.parse(result);
            if( responseArray['status'] == 1 ){
                // $.toast({
                //     heading: 'Success',
                //     text: responseArray['message'],
                //     position: 'top-right',
                //     loaderBg:'#2c2b2e',
                //     icon: 'success',
                //     hideAfter: 3000,
                //     stack: 6
                // });
                // location.reload();
                var buttonHtml = '<button type="button" onClick="reload();" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                $('#customModalLabel').html('Confirmation');
                $('#customModalBody').html(responseArray['message']);
                $('#customModalFoot').html(buttonHtml);
                $('#customBox').modal('show');

            }else if(responseArray['status'] == 2){
              var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
              $('#customModalLabel').html('Error');
              $('#customModalBody').html(responseArray['message']);
              $('#customModalFoot').html(buttonHtml);
              $('#customBox').modal('show');

              $(".loading_overlay1").hide();
            }else{

                // $.toast({
                //     heading: 'Error',
                //     text: 'An error has occurred. Please try again.',
                //     position: 'top-right',
                //     loaderBg:'#2c2b2e',
                //     icon: 'error',
                //     hideAfter: 3000,
                //     stack: 6
                //   });
                var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                $('#customModalLabel').html('Error');
                $('#customModalBody').html('An error has occurred. Please try again.');
                $('#customModalFoot').html(buttonHtml);
                $('#customBox').modal('show');

                $(".loading_overlay1").hide();
            }
        },
        error: function( result ){
            $("#mainLoader.loader").fadeOut("fast");
            $("#customBox .loaderImg").hide();
             // $.toast({
             //        heading: 'Error',
             //        text: 'An error has occurred. Please try again.',
             //        position: 'top-right',
             //        loaderBg:'#2c2b2e',
             //        icon: 'error',
             //        hideAfter: 3000,
             //        stack: 6
             //      });
             var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
             $('#customModalLabel').html('Error');
             $('#customModalBody').html('An error has occurred. Please try again.');
             $('#customModalFoot').html(buttonHtml);
             $('#customBox').modal('show');

             $(".loading_overlay1").hide();

        }
    });
}

function changeBulkStatus(status) {
    $("#mainLoader.loader").fadeIn("fast");
    var $data = [];
    var $type = $('input[name="filter_option"]:checked').val();
    $('.selectiveCheck input[type="checkbox"]:not(#selectAll):checked').each(function(){
        var details = {};
        var userId = $(this).parents('tr').attr('id');
        var email = $(this).parents('tr').attr('data-email');
        details.user_id = userId;
        details.email = email;
        details.status = status;
        $data.push(details);
    });

    $.ajax({
        url: '<?php echo BASE_URL; ?>institution/InstitutionHome/changeBulkAssignStatus',
        type: 'POST',
        data: {'user_data' : JSON.stringify($data)},
        success: function( result ){
            $("#mainLoader.loader").fadeOut("fast");
            $("#customBox .loaderImg").hide();

            var responseArray = JSON.parse(result);
            if( responseArray['status'] == 1 ){

                var buttonHtml = '<button type="button" onClick="reload();" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                $('#customModalLabel').html('Confirmation');
                $('#customModalBody').html(responseArray['message']);
                $('#customModalFoot').html(buttonHtml);
                $('#customBox').modal('show');
                if($type == 0){
                    $('#assign_btn').attr('disabled',true);
                }else{
                    $('#unassign_btn').attr('disabled',true);
                }

                // $.toast({
                //     heading: 'Success',
                //     text: responseArray['message'],
                //     position: 'top-right',
                //     loaderBg:'#2c2b2e',
                //     icon: 'success',
                //     hideAfter: 3000,
                //     stack: 6
                // });
                // location.reload();
            }else{

                // $.toast({
                //     heading: 'Error',
                //     text: 'An error has occurred. Please try again.',
                //     position: 'top-right',
                //     loaderBg:'#2c2b2e',
                //     icon: 'error',
                //     hideAfter: 3000,
                //     stack: 6
                //   });
                var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                $('#customModalLabel').html('Error');
                $('#customModalBody').html('An error has occurred. Please try again.');
                $('#customModalFoot').html(buttonHtml);
                $('#customBox').modal('show');

                $(".loading_overlay1").hide();
            }
        },
        error: function( result ){
            $("#customBox .loaderImg").hide();
             // $.toast({
             //        heading: 'Error',
             //        text: 'An error has occurred. Please try again.',
             //        position: 'top-right',
             //        loaderBg:'#2c2b2e',
             //        icon: 'error',
             //        hideAfter: 3000,
             //        stack: 6
             //      });
             var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                $('#customModalLabel').html('Error');
                $('#customModalBody').html('An error has occurred. Please try again.');
                $('#customModalFoot').html(buttonHtml);
                $('#customBox').modal('show');
             $(".loading_overlay1").hide();

        }
    });

}

function addUser(){
    var userEmail = $("#userEmail").val().trim();
    var regex = /^[_A-Za-z0-9-']+(\.[_A-Za-z0-9-']+)*@[A-Za-z0-9-]+(\.[a-z0-9-]+)*(\.[A-Za-z]{2,4})$/;
    if(userEmail == '' || userEmail == null){
        $("#inputEmail").addClass('has-danger');
        $("#emptyEmail").show();
        $("#invalidEmail").hide();
    }else if(!regex.test(userEmail)){
         $("#inputEmail").addClass('has-danger');
         $("#invalidEmail").show();
         $("#emptyEmail").hide();
    }else{
        $('#adduserLoader').fadeIn('fast');
        $.ajax({
        url: '<?php echo BASE_URL; ?>institution/InstitutionHome/addUser',
        type: 'POST',
        data: { email: userEmail },
        success: function( result ){
            $('#adduserLoader').fadeOut('fast');
            var responseArray = JSON.parse(result);
            if( responseArray['status'] == 1 ){
                // $.toast({
                //     heading: 'New User Adding',
                //     text: 'Please wait while a new user added.',
                //     position: 'top-right',
                //     loaderBg:'#2c2b2e',
                //     icon: 'success',
                //     hideAfter: 3000,
                //     stack: 6
                // });
                var buttonHtml = '<button type="button"  class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                $('#customModalLabel').html('Confirmation');
                // $('#customModalBody').html('Please wait while a new user added');
                $('#customModalBody').html('New User added successfully');
                $('#customModalFoot').html(buttonHtml);
                $('#customBox').modal('show');

                $("#customBox .loaderImg").hide();

                $('#exampleModal').modal('toggle');
            }else{
                $("#inputEmail").addClass('has-danger');
                $("#alreadyExist").show();
                $("#alreadyExist").html('* '+responseArray['message']+'.' );
                $("#emptyEmail").hide();
                $("#invalidEmail").hide();

            }
        },
        error: function( result ){
            $('#adduserLoader').fadeOut('fast');
            var responseArray = JSON.parse(result);
                $("#inputEmail").addClass('has-danger');
                $("#alreadyExist").show();
                $("#alreadyExist").html('* '+responseArray['message']+'.' );
                $("#emptyEmail").hide();
                $("#invalidEmail").hide();

        }
    });
    }
}

function removeUser(userEmail, userId){
    var buttonHtml = '<div class="row mainDiv" style="text-align: center;">';
        buttonHtml += '<button type="button" class="btn btn-info waves-effect redBtCustom" id="removeReqBtn">Remove</button>';
        buttonHtml += '<button type="button" onClick="removeClass();" class="btn btn-info waves-effect whiteBtCustom" data-dismiss="modal">Cancel</button>';
        buttonHtml += '</div>';
        buttonHtml += '<div class="row showWarning" style="display:none">';
        buttonHtml += '<div class="col-md-6 pull-left">';
        buttonHtml += '<img src="<?php echo BASE_URL ?>institution/img/sub-warning.svg" alt="">';
        buttonHtml += '<p>Are you sure you want to Remove User from your Institution?</p>';
        buttonHtml += '</div>';
        buttonHtml += '<div class="col-md-6 txtRight pull-right">';
        buttonHtml += '<button type="button" class="btn btn-success waves-effect" onclick="removeUserFinal(\''+userEmail+'\', '+userId+');">Yes</button>';
        buttonHtml += '<button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Cancel</button>';
        buttonHtml += '</div>';
        buttonHtml += '</div>';
        buttonHtml += '<div class="row showSucess" style="display:none">';
        buttonHtml += '<div class="col-md-8 pull-left">';
        buttonHtml += '<img src="<?php echo BASE_URL ?>institution/img/sub-sucess.svg" alt="">';
        buttonHtml += '<p style="margin-top: 7px;">User has been removed successfully.</p>';
        buttonHtml += '</div>';
        buttonHtml += '<div class="col-md-4 txtRight pull-right">';
        buttonHtml += '<button type="button" class="btn btn-info waves-effect" onClick="reload();" data-dismiss="modal">Close</button>';
        buttonHtml += '</div>';
        buttonHtml += '</div>';


    $('#customModalLabel').html('Remove User<p>Remove user from your institution.</p>');
    $('#customModalBody').addClass('bodyCustom').html('<span><img src="<?php echo BASE_URL ?>institution/img/sub-removeUser.svg" alt=""></span><p>It can’t be undone . User will be removed from your institution immediately and would not be able to access the Directory.</p>');
    $('#customModalFoot').css('text-align','left').html(buttonHtml);
    $('#customBox').modal('show');
}

$('body').on('click','#removeReqBtn',function(){
  $('#customBox .row.mainDiv').hide();
  $('#customBox .row.showWarning').show();
  $('#customBox .row.showSucess').hide();
});

function removeClass(){
  $('#customModalBody').removeClass('bodyCustom');
  $('#customModalFoot').css('text-align','center');
}

function removeUserFinal(userEmail, userId){
    // $('#customBox').modal('hide');
    // $('#customModalBody').removeClass('bodyCustom');
    var email = userEmail;
    var userId = userId;
    $("#customBox .loaderImg").show();
    $.ajax({
        url: '<?php echo BASE_URL; ?>institution/InstitutionHome/removeUser',
        type: 'POST',
        data: {'email': email, 'user_id': userId},
        success: function( result ){
            console.log(result);
            var responseArray = JSON.parse(result);
            if( responseArray['status'] == 1 ){
                $("#customBox .loaderImg").hide();
                $('#customBox .row.mainDiv').hide();
                $('#customBox .row.showWarning').hide();
                $('#customBox .row.showSucess').show();
                // var buttonHtml = '<button type="button" onClick="reload();" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                // $('#customModalLabel').html('Confirmation');
                // $('#customModalBody').html('User removed successfully');
                // $('#customModalFoot').html(buttonHtml);
                // $('#customBox').modal('show');
                // window.location.reload();
            }else if(responseArray['status'] == 2){
              var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
              $('#customModalLabel').html('Error');
              $('#customModalBody').removeClass('bodyCustom').html(responseArray['message']);
              $('#customModalFoot').html(buttonHtml);
              $('#customModalFoot').css('text-align','center');

              $("#customBox .loaderImg").hide();
            }else{
               var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
               $('#customModalLabel').html('Error');
               $('#customModalBody').removeClass('bodyCustom').html('An error has occurred. Please try again');
               $('#customModalFoot').html(buttonHtml);
               $('#customModalFoot').css('text-align','center');
               // $('#customBox').modal('show');

               $("#customBox .loaderImg").hide();
            }
        },
        error: function( result ){
            // $.toast({
            //     heading: 'Error',
            //     text: 'An error has occurred. Please try again.',
            //     position: 'top-right',
            //     loaderBg:'#2c2b2e',
            //     icon: 'error',
            //     hideAfter: 3000,
            //     stack: 6
            // });
            var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
            $('#customModalLabel').html('Error');
            $('#customModalBody').removeClass('bodyCustom').html('An error has occurred. Please try again');
            $('#customModalFoot').html(buttonHtml);
            $('#customModalFoot').css('text-align','center');
            // $('#customBox').modal('show');

            $("#customBox .loaderImg").hide();
        }
    });

}

function customSubscription(){
    var email = $('#customSubscriptionPopUp').attr('data-userEmail');
    var userId = $('#customSubscriptionPopUp').attr('data-user');
    var exdate = $("#subscription_expiry_date").val();
    var userSubscription = $('#customSubscriptionPopUp').attr('data-subscription');
    $('#mainLoader').fadeIn('fast');

    $.ajax({
        url: '<?php echo BASE_URL; ?>institution/InstitutionHome/updateSubscription',
        type: 'POST',
        data: {'email' : email,'user_id' : userId,'custom_date' : exdate},
        success: function( result ){
          $('#mainLoader').fadeOut('fast');
            // $('#customSubscriptionPopUp').modal('hide');
            var responseArray = JSON.parse(result);
            if( responseArray['status'] == 1 ){
              $("#usersubscriptionexpiryDate").html('User Subscription Expiry Date: ' + exdate);

                // var buttonHtml = '<button type="button" onClick="reload();" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                // $('#customModalLabel').html('Confirmation');
                // $('#customModalBody').html('Subscription period is updated');
                // $('#customModalFoot').html(buttonHtml);
                // $('#customBox').modal('show');
                // $("#customBox .loaderImg").hide();
                $('#customSubscriptionPopUp .showDefault').hide();
                $('#customSubscriptionPopUp .showWarning').hide();
                $('#customSubscriptionPopUp .showSucess').show();

                $(".loading_overlay").hide();
                // location.reload();
            }else{
                // $.toast({
                //     heading: 'Error',
                //     text: 'An error has occurred. Please try again.',
                //     position: 'top-right',
                //     loaderBg:'#2c2b2e',
                //     icon: 'error',
                //     hideAfter: 3000,
                //     stack: 6
                // });
                var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                $('#customModalLabel').html('Error');
                $('#customModalBody').html('An error has occurred. Please try again');
                $('#customModalFoot').html(buttonHtml);
                $('#customBox').modal('show');
                $("#customBox .loaderImg").hide();

                $(".loading_overlay").hide();
            }
        },
        error: function( result ){
            // $('#customSubscriptionPopUp').modal('hide');
            $('#mainLoader').fadeOut('fast');
             // $.toast({
             //        heading: 'Error',
             //        text: 'An error has occurred. Please try again.',
             //        position: 'top-right',
             //        loaderBg:'#2c2b2e',
             //        icon: 'error',
             //        hideAfter: 3000,
             //        stack: 6
             //    });
             var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
             $('#customModalLabel').html('Error');
             $('#customModalBody').html('An error has occurred. Please try again');
             $('#customModalFoot').html(buttonHtml);
             $('#customBox').modal('show');
             $("#customBox .loaderImg").hide();

            $(".loading_overlay").hide();
        }
    });
}

function reload(){
    $('#customModalBody').removeClass('bodyCustom');
    $('#customModalFoot').css('text-align','center');
    var page = parseInt($('#sample_1_paginate ul.pagination li.active.num a #chngPage').val());
    var limit = $('#Data_Count').val();
    var $search_text = $input.val().trim();
    var list_type = $('input[name="filter_option"]:checked').val();
    getDashCount($search_text);
    getDashboard(page,limit,$search_text,list_type);
    notifications();
}

function getDashCount($search_text){
    $.ajax({
      url:'<?php echo BASE_URL; ?>institution/InstitutionHome/getDashboardCounts',
      type: "POST",
      data: {'search_text':$search_text},
      success: function(data) {
        console.log(data);
        data = JSON.parse(data);
        console.log(data);
        $('#showTotCount').html(data.all_users);
        $('#showAwaitCount').html(data.pending_users);
        $('#showApprovedCount').html(data.approved_users);
        $('#showNewCount').html(data.new_users);
      }
    });
}

</script>
