<div id="mainLoader" class="loader"></div>
<style type="text/css">
    .loader, .loaderImg {
        position: fixed;
        left:0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('<?php echo BASE_URL; ?>institution/page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
    }
</style>
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
        <div class="row page-titles">
          <div class="col-md-6 col-6 align-self-center">
              <!-- <h3 class="text-themecolor m-b-0 m-t-0"><span style="color:#10a5da;">Broadcast Message</span></h3> -->
              <h3 class="text-themecolor m-b-0 m-t-0">Broadcast Message</h3>
          </div>
        </div>
        <div class="row breadcrumb-title">
          <ol class="breadcrumb">
          </ol>
        </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->


    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->

    <div class="row boaderLines">
        <div class="col-12">
          <div id="ipRange" style="position:relative;">
            <div class="card">
                <div class="card-block padLeftRight0 pad0" id="broadCastCateogary">
                    <div class="floatLeftFull padLeftRight40 groupCustom newListStyle">
                        <div class="tableTop floatLeft marTop10">
                            <span class="card-title">Select User Groups to Broadcast Message:</span>
                            <div class="font14">You can filter by profession ("nurse", "doctor"), location ("F7", "Emergency Department") and Availability. Once you have selected the user groups, click next to see and edit the list of users.</div>
                        </div>
                    </div>
                    <div id="cateogaryContainer">
                        <div class="card-note">
                            <div class="card-title">Professions
                                <button data-target="professionBlock" class="Btn-Select-All"><img src="<?php echo BASE_URL; ?>institution/img/select-all-dark.svg" />Select All</button>
                            </div>
                        </div>
                        <div class="table-responsive wordBreak cateogaryList" id="professionBlock">
                            <span>Loading Please Wait...</span>
                        </div>

                        <div class="card-note">
                            <div class="card-title">Location
                                <button data-target="locationBlock" class="Btn-Select-All"><img src="<?php echo BASE_URL; ?>institution/img/select-all-dark.svg" />Select All</button>
                            </div>
                        </div>
                        <div class="table-responsive wordBreak cateogaryList" id="locationBlock">
                            <span>Loading Please Wait...</span>
                        </div>

                        <div class="card-note">
                            <div class="card-title">Availability
                            <button data-target="availabilityBlock" class="Btn-Select-All"><img src="<?php echo BASE_URL; ?>institution/img/select-all-dark.svg" />Select All</button>
                            </div>
                        </div>
                        <div class="table-responsive wordBreak cateogaryList" id="availabilityBlock">
                            <div class="cateogaryCell" data-id="1">Available</div>
                            <div class="cateogaryCell" data-id="0">Not Available</div>
                        </div>
                    </div>
                    <div id="actionContainer">
                        <button id="selectAll" class="">
                            <img src="<?php echo BASE_URL; ?>institution/img/select-all-green.svg" />
                            Select All
                        </button>
                        <button class="eventOff" id="next">Next</button>  
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>
    <!-- Row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
<script type="text/javascript">
    $body = $('body');
    $("#sidebarnav>li#broadcastMenu").addClass('active').find('a').eq(0).addClass('active');
    $('document').ready(function(){
        sessionStorage.clear();
        getAllProfessionList();
        getAllLocationList();

    });

    function getAllProfessionList(){
        $.ajax({
            url:'<?php echo BASE_URL; ?>institution/InstitutionBroadcast/getProfession',
            type: "POST",
            success: function(data) {
                $('#professionBlock').html(data);
                $('#next').removeClass('eventOff');
            }
        });
    }

    function getAllLocationList(){
        $.ajax({
            url:'<?php echo BASE_URL; ?>institution/InstitutionBroadcast/getLocation',
            type: "POST",
            success: function(data) {
                $('#locationBlock').html(data);
                $('#next').removeClass('eventOff');
                $("#mainLoader.loader").fadeOut("fast");
            }
        });
    }

    $body.on('click','#selectAll', function() {
        if($(this).hasClass('selected')){
            $('.Btn-Select-All').removeClass('selected');
            $('.cateogaryCell').removeClass('selected');
            $(this).removeClass('selected');
        }else{
            $('.Btn-Select-All').addClass('selected');
            $('.cateogaryCell').addClass('selected');
            $(this).addClass('selected');
        }
    });

    $body.on('click', '.Btn-Select-All', function(){
        $target = $(this).attr('data-target');
        if($(this).hasClass('selected')){
            $('#'+$target+' .cateogaryCell').removeClass('selected');
            $(this).removeClass('selected');
        }else{
            $('#'+$target+' .cateogaryCell').addClass('selected');
            $(this).addClass('selected');
        }
        if(isAllCateogarySelected() === true){
            $('#selectAll').addClass('selected');
        }else{
            $('#selectAll').removeClass('selected');
        }
    });

    $body.on('click', '.cateogaryCell', function(){
        var target = $(this).parent().attr('id');
        $(this).toggleClass('selected');
        if(isAllCellSelected(target) === true){
            $('button[data-target='+target+']').addClass('selected');
        }else{
            $('button[data-target='+target+']').removeClass('selected');
        }

        if(isAllCateogarySelected() === true){
            $('#selectAll').addClass('selected');
        }else{
            $('#selectAll').removeClass('selected');
        }
    });

    $body.on('click','#next', function(){
        event.preventDefault();
        if($('.cateogaryCell.selected').length == 0){
            alert('Please select a filter to proceed');
            return false;
        }
        var professionArray = [];
        var professionNameArray = [];
        var locationArray = [];
        var locationNameArray = [];
        var availabilityArray = [];
        var availabilityNameArray = [];
        $("#professionBlock .cateogaryCell.selected").each(function(){
            professionNameArray.push($(this).html().trim());
            professionArray.push($(this).attr('data-id'));
        });
        $("#locationBlock .cateogaryCell.selected").each(function(){
            locationNameArray.push($(this).html().trim());
            locationArray.push($(this).html().trim());
        });
        $("#availabilityBlock .cateogaryCell.selected").each(function(){
            availabilityNameArray.push($(this).html().trim());
            availabilityArray.push($(this).attr('data-id'));
        });
        var finalFilters = {
            professions: professionArray,
            professionsName: professionNameArray,
            locations: locationArray,
            locationsName: locationNameArray,
            availableStatus: availabilityArray,
            availableStatusName: availabilityNameArray
        };
        sessionStorage.setItem('filters',JSON.stringify(finalFilters));
        $("#mainLoader.loader").fadeIn("fast");
        window.location.href =  "<?php echo BASE_URL; ?>institution/InstitutionBroadcast/userList";
    });

    function isAllCateogarySelected(){
        var selectedCount = $('.Btn-Select-All').length;
        var returnVal = false;
        if(selectedCount !== 0 && (selectedCount === $('.Btn-Select-All.selected').length)){
            returnVal = true;
        }
        return returnVal;
    }
    function isAllCellSelected(id){
        var selectedCount = $('#'+id+' .cateogaryCell').length;
        var returnVal = false;
        if(selectedCount !== 0 && (selectedCount === $('#'+id+' .cateogaryCell.selected').length)){
            returnVal = true;
        }
        return returnVal;
    }
</script>