
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
        <div class="row page-titles">
          <div class="col-md-6 col-6 align-self-center">
              <!-- <h3 class="text-themecolor m-b-0 m-t-0"><span style="color:#10a5da;">Broadcast Message</span></h3> -->
              <h3 class="text-themecolor m-b-0 m-t-0">Broadcast Message</h3>
          </div>
        </div>
        <div class="row breadcrumb-title">
          <ol class="breadcrumb">
          </ol>
        </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->


    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->

    <div class="row boaderLines">
        <div class="col-12">
          <div id="ipRange" style="position:relative;">
            <div class="card">
                <div class="card-block padLeftRight0 pad0" id="broadCastCateogary">
                    <div class="floatLeftFull padLeftRight40 groupCustom newListStyle">
                        <div class="tableTop floatLeft marTop10">
                            <span class="card-title">Broadcast Message Sent</span>
                        </div>
                    </div>
                    <div class="bmSentTitle">
                        <img src="<?php echo BASE_URL ?>institution/img/sent-icon.svg" />
                        <div>Your message broadcast to <text id="totalUsers">0</text> users.</div>
                    </div>
                    <div class="bmSentDetail">
                        <div class="titleWithBtn">
                            <strong>User List</strong>
                        </div>
                        <div data-target="professionBlock" class="card-title" style="display:none;">Professions</div>
                        <p id="professionBlock" style="display:none;"></p>
                        <div data-target="locationBlock" class="card-title" style="display:none;">Location</div>
                        <p id="locationBlock" style="display:none;"></p>
                        <div data-target="availabilityBlock" class="card-title" style="display:none;">Availability</div>
                        <p id="availabilityBlock" style="display:none;"></p>
                        <div class="titleWithBtn marginTop">
                            <strong>Broadcast Message</strong>
                        </div>
                        <p id="broadcastMessage">The health of underserved patients has been our top priority since CareMessage was founded in 2012. With COVID-19 impacting Safety Net Providers and the patients they serve the most, we continue to be here to help. See the free COVID-19 messaging resources we've developed to help support Health Centers.</p>
                    </div>
                    <div id="actionContainer" class="bmSentFooter">
                        <button class="bmGreenBtn" onclick="sendNewBroadcast();">Start a New Broadcast Message</button>  
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>
    <!-- Row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>

<script type="text/javascript">
    $("#sidebarnav>li#broadcastMenu").addClass('active').find('a').eq(0).addClass('active');
    $('document').ready(function(){
        var filter = sessionStorage.getItem('filters');
        if(!filter){
            window.location.href =  "<?php echo BASE_URL; ?>institution/InstitutionBroadcast/";
            return false;
        }
        showCategoriesSelected();
    });
    function showCategoriesSelected(){
        var categoriesselected = sessionStorage.getItem('filters');
        var totalUsers = sessionStorage.getItem('totalUsers');
        var broadcastMessage = sessionStorage.getItem('broadcastMessage');

        $('#broadcastMessage').html(broadcastMessage);
        $('#totalUsers').html(totalUsers);

        categoriesselected = JSON.parse(categoriesselected);
        const {professionsName, locationsName, availableStatusName} = categoriesselected;
        if(professionsName.length > 0){
            $('#professionBlock').html(professionsName.join(', '));
            $('#professionBlock').show();
            $('div[data-target="professionBlock"]').show();
        }
        if(locationsName.length > 0){
            $('#locationBlock').html(locationsName.join(', '));
            $('#locationBlock').show();
            $('div[data-target="locationBlock"]').show();
        }
        if(availableStatusName.length > 0){
            $('#availabilityBlock').html(availableStatusName.join(', '));
            $('#availabilityBlock').show();
            $('div[data-target="availabilityBlock"]').show();
        }
    }
    function sendNewBroadcast(){
        sessionStorage.clear();
        window.location.href =  "<?php echo BASE_URL; ?>institution/InstitutionBroadcast/";
    }
</script>