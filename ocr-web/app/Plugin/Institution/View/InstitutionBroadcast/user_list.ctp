<div id="mainLoader" class="loader"></div>
<style type="text/css">
    .loader, .loaderImg {
        position: fixed;
        left:0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('<?php echo BASE_URL; ?>institution/page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
    }
</style>
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
        <div class="row page-titles">
          <div class="col-md-6 col-6 align-self-center">
              <!-- <h3 class="text-themecolor m-b-0 m-t-0"><span style="color:#10a5da;">Broadcast Message</span></h3> -->
              <h3 class="text-themecolor m-b-0 m-t-0">Broadcast Message</h3>
          </div>
        </div>
        <div class="row breadcrumb-title">
          <ol class="breadcrumb">
          </ol>
        </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->


    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->

    <div class="row boaderLines">
        <div class="col-12">
          <div id="ipRange" style="position:relative;">
            <div class="card">
                <div class="card-block padLeftRight0 pad0" id="broadCastUserList">
                    <div class="floatLeftFull padLeftRight40 groupCustom newListStyle">
                        <div class="tableTop floatLeft marTop10">
                            <span class="card-title">User Broadcast List</span>
                            <div class="font14">You can edit the user list or click Next to write the broadcast message.</div>
                        </div>
                        <div id="userlisting_filter" class="dataTables_filter">
                            <label><i class="demo-icon icon-sub-lense"></i></label>
                            <input id="user_search" type="search" class="form-control input-sm" placeholder="Search" aria-controls="userlisting">
                        </div>
                    </div>
                    <div class="table-responsive wordBreak" id="ajaxContent">
                        <table class="table stylish-table customPading">
                            <thead>
                                <tr>
                                    <th style="width: 5%">Photo </th>
                                    <th style="width: 20%">Name </th>
                                    <th style="width: 25%">Email </th>
                                    <th style="width: 15%">Profession </th>
                                    <th style="width: 15%">Location</th>
                                    <th style="width: 10%">Message Delievery Type</th>
                                    <th style="width: 10%">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                <td colspan="8" align="center"><font color="green">Please wait while we are loading the list...</font></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div id="actionContainer">
                        <button id="next">Next</button>  
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>
    <!-- Row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>

<script type="text/javascript">
    $("#sidebarnav>li#broadcastMenu").addClass('active').find('a').eq(0).addClass('active');
    var $body = $('body');
    var typingTimer;
    var $input = $('#user_search');
    var doneTypingInterval = 1000;
    $('document').ready(function(){
        var filter = sessionStorage.getItem('filters');
        if(!filter){
            window.location.href =  "<?php echo BASE_URL; ?>institution/InstitutionBroadcast/";
            return false;
        }
        getUserList('1','20','');
    });

    //on keyup, start the countdown
    $input.on('keyup', function () {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(reload, doneTypingInterval);
    });

    //on keydown, clear the countdown
    $input.on('keydown', function () {
        clearTimeout(typingTimer);
    });

    function getUserList(page, limit, searchText){
        var filter = sessionStorage.getItem('filters');
        var deletedUsers = sessionStorage.getItem('removeUsers');
        $("#mainLoader.loader").fadeIn("fast");
        $.ajax({
            url:'<?php echo BASE_URL; ?>institution/InstitutionBroadcast/getUserList',
            type: "POST",
            data: {filters: filter, deletedUsers: deletedUsers,'page_number':page,'limit':limit,'search_text':searchText},
            success: function(data) {
                console.log(data);
                $('#ajaxContent').html(data);
                var userCount = $('#totalCount').html();
                if(userCount == 0){
                    alert("No user to send Broadcast.");
                    window.location.href =  "<?php echo BASE_URL; ?>institution/InstitutionBroadcast/";
                    return false;
                }
                $("#mainLoader.loader").fadeOut("fast");
            }
        });
    }
    function chngPage(val,limit) {
        var list_type = $('input[name="filter_option"]:checked').val();
        $search_text = $input.val().trim();
        var goVal=$("#chngPage").val();
        var filter = sessionStorage.getItem('filters');
        var deletedUsers = sessionStorage.getItem('removeUsers');

        if(parseInt(goVal) <= Math.ceil(val) && Math.ceil(goVal)>0){
            $("#mainLoader.loader").fadeIn("fast");
            $.ajax({
                url:'<?php echo BASE_URL; ?>institution/InstitutionBroadcast/getUserList',
                type: "POST",
                data: {filters: filter, deletedUsers: deletedUsers,'page_number':goVal,'limit':limit,'search_text':$search_text, 'sort_order':sort_order},
                success: function(data) {
                    $('#ajaxContent').html(data);
                    $("#mainLoader.loader").fadeOut("fast");
                }
            });
        }
    }
    function chngCount(val){
        $("#mainLoader.loader").fadeIn("fast");
        $search_text = $input.val().trim();
        var filter = sessionStorage.getItem('filters');
        var deletedUsers = sessionStorage.getItem('removeUsers');
        $.ajax({
            url:'<?php echo BASE_URL; ?>institution/InstitutionBroadcast/getUserList',
            type: "POST",
            data: {filters: filter, deletedUsers: deletedUsers,'limit':val,'search_text':$search_text},
            success: function(data) {
                $('#ajaxContent').html(data);
                $("#mainLoader.loader").fadeOut("fast");
            }
        });
    }

    function abc(val,limit){
        $("#mainLoader.loader").fadeIn("fast");
        $search_text = $input.val().trim();
        var filter = sessionStorage.getItem('filters');
        var deletedUsers = sessionStorage.getItem('removeUsers');
        $.ajax({
            url:'<?php echo BASE_URL; ?>institution/InstitutionBroadcast/getUserList',
            type: "POST",
            data: {filters: filter, deletedUsers: deletedUsers,'page_number':val,'limit':limit,'search_text':$search_text},
            success: function(data) {
                $('#ajaxContent').html(data);
                $("#mainLoader.loader").fadeOut("fast");
            }
        });
    }
    function reload(){
        $input.blur();
        var page = parseInt($('#sample_1_paginate ul.pagination li.active.num a #chngPage').val());
        var limit = $('#Data_Count').val();
        var search_text = $input.val().trim();
        getUserList(page, limit,search_text);
    }
    $body.on('click', '.removeUser', function(){
        var userId = $(this).attr('data-id');
        var $removedUsers = sessionStorage.getItem('removeUsers');
        if($removedUsers){
            $removedUsers = JSON.parse($removedUsers);
            $removedUsers.push(userId);
        }else{
            $removedUsers = [userId];
        }
        $removedUsers = JSON.stringify($removedUsers);
        sessionStorage.setItem('removeUsers', $removedUsers);
        reload();
    });

    $body.on('click','#next',function(){
        $("#mainLoader.loader").fadeIn("fast");
        var usersCount = $('#totalCount').html();
        var AllIds = $('#AllIds').html();
        sessionStorage.setItem('totalUsers',usersCount);
        sessionStorage.setItem('AllIds',AllIds);
        window.location.href =  "<?php echo BASE_URL; ?>institution/InstitutionBroadcast/broadcastMessage";
    });
</script>