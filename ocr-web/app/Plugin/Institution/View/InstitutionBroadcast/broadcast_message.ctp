<div id="mainLoader" class="loader"></div>
<style type="text/css">
    .loader, .loaderImg {
        position: fixed;
        left:0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('<?php echo BASE_URL; ?>institution/page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
    }
</style>
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
        <div class="row page-titles">
          <div class="col-md-6 col-6 align-self-center">
              <!-- <h3 class="text-themecolor m-b-0 m-t-0"><span style="color:#10a5da;">Broadcast Message</span></h3> -->
              <h3 class="text-themecolor m-b-0 m-t-0">Broadcast Message</h3>
          </div>
        </div>
        <div class="row breadcrumb-title">
          <ol class="breadcrumb">
          </ol>
        </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->


    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->

    <div class="row boaderLines">
        <div class="col-12">
          <div id="ipRange" style="position:relative;">
            <div class="card">
                <div class="card-block padLeftRight0 pad0" id="broadCastMessage">
                    <div class="floatLeftFull padLeftRight40 groupCustom newListStyle">
                        <div class="tableTop floatLeft marTop10">
                            <span class="card-title">Write a short message to Broadcast:</span>
                        </div>
                    </div>
                    <div id="messageContainer">
                        <textarea name="" id="" placeholder="Write here..." rows="6" maxlength="500" spellcheck="true"></textarea>
                        <span>Character Remaining <text id="remainingCount">500</text>/500</span>
                    </div>
                    <div id="actionContainer">
                        <button id="next">Next</button>  
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>
    <!-- Row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
<script type="text/javascript">
    var $body = $('body');
    $("#sidebarnav>li#broadcastMenu").addClass('active').find('a').eq(0).addClass('active');
    $('document').ready(function(){
        var filter = sessionStorage.getItem('filters');
        if(!filter){
            window.location.href =  "<?php echo BASE_URL; ?>institution/InstitutionBroadcast/";
            return false;
        }
        setMessageBody();
        $("#mainLoader.loader").fadeOut("fast");
    });
    function setMessageBody(){
        var broadcastMessage = sessionStorage.getItem('broadcastMessage');
        if(broadcastMessage){
            $('#messageContainer textarea').val(broadcastMessage);
            var count = broadcastMessage.length;
            $('#remainingCount').html(500 - parseInt(count));
        }
    }
    $body.on('input', '#messageContainer textarea', function(event){
        var count = event.target.value.length;
        $('#remainingCount').html(500 - parseInt(count));
    });
    $body.on('click', '#next', function(){
        var message = $('#messageContainer textarea').val().trim();
        if(message == ''){
            alert('Please enter message.');
            return false;
        }
        sessionStorage.setItem('broadcastMessage', message);
        $("#mainLoader.loader").fadeIn("fast");
        window.location.href =  "<?php echo BASE_URL; ?>institution/InstitutionBroadcast/confirmMessage";
    });
</script>