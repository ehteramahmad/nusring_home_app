<div id="mainLoader" class="loader"></div>
<style type="text/css">
    .loader, .loaderImg {
        position: fixed;
        left:0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('<?php echo BASE_URL; ?>institution/page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
    }
</style>
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
        <div class="row page-titles">
          <div class="col-md-6 col-6 align-self-center">
              <!-- <h3 class="text-themecolor m-b-0 m-t-0"><span style="color:#10a5da;">Broadcast Message</span></h3> -->
              <h3 class="text-themecolor m-b-0 m-t-0">Broadcast Message</h3>
          </div>
        </div>
        <div class="row breadcrumb-title">
          <ol class="breadcrumb">
          </ol>
        </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->


    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->

    <div class="row boaderLines">
        <div class="col-12">
          <div id="ipRange" style="position:relative;">
            <div class="card">
                <div class="card-block padLeftRight0 pad0" id="broadCastCateogary">
                    <div class="floatLeftFull padLeftRight40 groupCustom newListStyle">
                        <div class="tableTop floatLeft marTop10">
                            <span class="card-title">Confirm Broadcast Message</span>
                        </div>
                    </div>
                    <div class="titleWithBtn">
                        <strong>User List</strong>
                        <span>: You are sending to <text id="totalUsers">0</text> users.</span>
                        <button class="blueBtn" onclick="gotoPage('userList');">
                            <img src="<?php echo BASE_URL ?>institution/img/left-arrow-icon.svg" />
                            Edit User List
                        </button>
                    </div>
                    <div id="cateogaryContainer" class="confirmMsgSection">
                        <div data-target="professionBlock" class="card-note" style="display:none;">
                            <div class="card-title">Professions</div>
                        </div>
                        <div class="table-responsive wordBreak cateogaryList" id="professionBlock" style="display:none;"></div>

                        <div data-target="locationBlock" class="card-note" style="display:none;">
                            <div class="card-title">Location</div>
                        </div>
                        <div class="table-responsive wordBreak cateogaryList" id="locationBlock" style="display:none;"></div>

                        <div data-target="availabilityBlock" class="card-note" style="display:none;">
                            <div class="card-title">Availability</div>
                        </div>
                        <div class="table-responsive wordBreak cateogaryList" id="availabilityBlock" style="display:none;"></div>
                    </div>
                    <div class="titleWithBtn">
                        <strong>Broadcast Message</strong>
                        <button class="blueBtn" onclick="gotoPage('broadcastMessage');">
                            <img src="<?php echo BASE_URL ?>institution/img/edit-icon.svg" />
                            Edit Message
                        </button>
                    </div>
                    <div class="confirmMsgText">
                    The health of underserved patients has been our top priority since CareMessage was founded in 2012. With COVID-19 impacting Safety Net Providers and the patients they serve the most, we continue to be here to help. See the free COVID-19 messaging resources we've developed to help support Health Centers.
                    </div>
                    <div class="confirmPass">
                        <div class="titleWithBtn">
                            <strong>Please enter your login password</strong>
                        </div>
                        <input class="form-control" id="userPassword" type="password" placeholder="Password…" />
                    </div>
                    <div id="actionContainer">
                        <button class="bmCancelBtn" onClick="backToMain();">
                            Cancel
                        </button>
                        <button class="bmGreenBtn" onClick="sendBroadcastMessage();">Send Broadcast Message</button>  
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>
    <div class="bmSendingLoader">
        <div class="bmLoaderContainer">
            <img src="<?php echo BASE_URL ?>institution/img/1-loader.gif" />
            <div>Please wait while we are sending your message…</div>
        </div>
    </div>
    <!-- Row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
<script type="text/javascript">
    $("#sidebarnav>li#broadcastMenu").addClass('active').find('a').eq(0).addClass('active');
    $('document').ready(function(){
        var filter = sessionStorage.getItem('filters');
        if(!filter){
            window.location.href =  "<?php echo BASE_URL; ?>institution/InstitutionBroadcast/";
            return false;
        }
        showCategoriesSelected();
        $("#mainLoader.loader").fadeOut("fast");
    });

    function backToMain(){
        window.location.href =  "<?php echo BASE_URL; ?>institution/InstitutionBroadcast/";
    }

    function sendBroadcastMessage(){
        var password = $('#userPassword').val().trim();
        if(password == ''){
            alert('Please enter your password.');
            return false;
        }
        var filter = sessionStorage.getItem('filters');
        var deletedUsers = sessionStorage.getItem('removeUsers');
        var broadcastMessage = sessionStorage.getItem('broadcastMessage');
        var AllIds = sessionStorage.getItem('AllIds');
        $('.bmSendingLoader').show();
        $.ajax({
            url:'<?php echo BASE_URL; ?>institution/InstitutionBroadcast/sendBroadcastMessage',
            type: "POST",
            data: {filters: filter, deletedUsers: deletedUsers,message: broadcastMessage, password: password, AllIds: AllIds},
            success: function(result) {
                console.log(result);
                $("#customBox .loaderImg").hide();
                var responseArray = JSON.parse(result);
                if( responseArray['status'] == 1 ){
                    window.location.href =  "<?php echo BASE_URL; ?>institution/InstitutionBroadcast/broadcastMessageSent";
                }else{
                    $('.bmSendingLoader').hide();
                    if($('#exampleModal2').hasClass('show')){
                        $('#exampleModal2').modal('hide');
                    }
                    var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                    $('#customModalLabel').html('Alert');
                    $('#customModalBody').html(responseArray['message']);
                    $('#customModalFoot').html(buttonHtml);
                    $('#customBox').modal('show');
                    $("#customBox .loaderImg").hide();
                }
            }
        });
    }

    function showCategoriesSelected(){
        var categoriesselected = sessionStorage.getItem('filters');
        var totalUsers = sessionStorage.getItem('totalUsers');
        var broadcastMessage = sessionStorage.getItem('broadcastMessage');

        $('.confirmMsgText').html(broadcastMessage);
        $('#totalUsers').html(totalUsers);

        categoriesselected = JSON.parse(categoriesselected);
        const {professionsName, locationsName, availableStatusName} = categoriesselected;
        if(professionsName.length > 0){
            professionsName.forEach(element => {
                $('#professionBlock').append(categoryCell(element));
            });
            $('#professionBlock').show();
            $('div[data-target="professionBlock"]').show();
        }
        if(locationsName.length > 0){
            locationsName.forEach(element => {
                $('#locationBlock').append(categoryCell(element));
            });
            $('#locationBlock').show();
            $('div[data-target="locationBlock"]').show();
        }
        if(availableStatusName.length > 0){
            availableStatusName.forEach(element => {
                $('#availabilityBlock').append(categoryCell(element));
            });
            $('#availabilityBlock').show();
            $('div[data-target="availabilityBlock"]').show();
        }
    }
    function gotoPage(page){
        window.location.href =  "<?php echo BASE_URL; ?>institution/InstitutionBroadcast/"+page;
    }
    function categoryCell(name){
        return(
            `<div class="cateogaryCell">${name}</div>`
        );
    }
</script>