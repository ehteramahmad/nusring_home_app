<?php

$searchdata['status'] = 0;
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
        'update' => '#ajaxContent',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
?>
<!-- <div class="loader"></div> -->
<style type="text/css">
.loader {
    position: fixed;
    left:0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('<?php echo BASE_URL; ?>institution/page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
}
.calCustom {
    font-size: 1.4em;
    top:8px;
    right:14px;
}

</style>
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Baton Roles</h3>
        </div>
    </div>
    <div class="row breadcrumb-title">
      <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo BASE_URL ?>institution/InstitutionHome/index"><i class="demo-icon icon-sub-home"></i></a></li>
          <li class="breadcrumb-item active">Baton Roles</li>
      </ol>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
<!-- Start HTML Content -->
<!-- ============================================================== -->
<div class="row boaderLines">
   <div class="col-12">
     <div id="batonListLoader" class="loader"></div>
      <div class="card">
         <div class="card-block padLeftRight0">
            <div class="floatLeftFull padLeftRight14">
               <div class="tableTop floatLeft">
                  <span class="allIconUsers">
                  <img src="<?php echo BASE_URL ?>institution/img/batonRoleIcon.svg" alt=""></span>
                  <span class="card-title">Total Baton Roles:</span>
                  <span class="display-6" id="showTotCount">0</span>
                  <div class="assignBtnAll">
                     <button data-toggle="modal" data-target="#addBatonPopup" class="btn bulk_action_btn pull-right hidden-sm-down btn-primary-green"> + Add Baton Roles</button>
                  </div>
               </div>
               <div id="userlisting_filter" class="dataTables_filter">
                  <label><i class="demo-icon icon-sub-lense"></i></label>
                  <input id="user_search" type="search" class="form-control input-sm" placeholder="Search" aria-controls="userlisting">
               </div>
            </div>
            <div class="table-responsive wordBreak" id="ajaxContent">
              <table class="table stylish-table customPading lineHeight36">
                  <thead>
                      <tr>
                          <th>Baton Roles </th>
                          <th>On Call </th>
                          <th>Status</th>
                          <!-- <th>Action</th> -->
                      </tr>
                  </thead>
                  <tbody>
                    <tr>
                        <td colspan="8" align="center">
                            <font color="green">Please wait while we are loading the list...</font>
                        </td>
                    </tr>
                  </tbody>
              </table>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- ============================================================== -->
<!-- End HTML Content -->
<!-- ============================================================== -->




    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <!-- User Privilege -->
    <div class="modal fade" id="addBatonPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="loader" id="addBatonLoader"></div>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Add Baton Role</h4>
                    <p>You can add baton role by creating new role below:</p>
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                </div>
                <div class="modal-body">
                    <form class="addBatonForm">
                      <div class="floatLeftFull txtCenter">
                        <textarea id="batonRoleText" placeholder="Write here.."></textarea>
                      </div>
                      <div class="floatLeftFull optionOnCall">
                        <span class="leftFloat"><input id="baton_is_oncall" type="checkbox" /></span>
                        <p> This is On Call Role</p>
                      </div>
                    </form>
                </div>
                <div class="modal-footer">
                  <!-- <div style="margin:0 auto;" class="onCallBtMain txtCenter">
                    <button type="button" id="btBatonAccept" class="btn btModelBatonAccept" onclick="changeRequestFinal(20,1,8);">Add</button>
                    <button type="button" id="btBatonCancel" class="btn btModelBatonCancel" data-dismiss="modal">Cancel</button>
                  </div> -->
                    <div style="display:none" class="row showWarning">
                      <div class="col-md-6 pull-left">
                        <img src="<?php echo BASE_URL ?>institution/img/sub-warning.svg" alt="">
                        <p>Are you sure you want to add this Baton Role.?</p>
                      </div>
                      <div class="col-md-6 txtRight pull-right">
                        <button type="button" class="btn btn-success waves-effect" onclick="addBatonRole();">Add</button>
                        <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Cancel</button>
                      </div>
                    </div>
                    <div class="row showSucess" style="display:none">
                        <div class="col-md-8 pull-left">
                          <img src="<?php echo BASE_URL ?>institution/img/sub-sucess.svg" alt="">
                          <p>Baton Role has been added successfully!</p>
                        </div>
                        <div class="col-md-4 txtRight pull-right">
                          <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ****************  -->
    <!-- User Assign -->
    <div class="modal fade" id="userAssignPopUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Update User Baton Role</h4>
                    <p>You can update user baton roles below:</p>
                </div>
                <div class="modal-body">
                    <div class="col-md-12 baton_role_search">
                        <label><i class="demo-icon icon-sub-lense"></i></label>
                        <input type="text" placeholder="Search" id="search_unoccupied_role" name="" value="">
                    </div>
                    <div id="batonListing" style="display:block;">
                      <div id="batonUserListLoader" class="loader"></div>
                      <ul id="batonRoleList">
                      </ul>
                    </div>
                </div>
                <div class="modal-footer">
                  <div class="row showWarning" style="display:none">
                    <div class="col-md-6 pull-left">
                      <img src="<?php echo BASE_URL ?>institution/img/sub-warning.svg" alt="">
                      <p>Are you sure you want to update user baton role?</p>
                    </div>
                    <div class="col-md-6 txtRight pull-right">
                      <button type="button" class="btn btn-success waves-effect" onclick="assignBatonRole();">Yes</button>
                      <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Cancel</button>
                    </div>
                  </div>
                  <div class="row showSucess" style="display:none">
                        <div class="col-md-8 pull-left">
                          <img src="<?php echo BASE_URL ?>institution/img/sub-sucess.svg" alt="">
                          <p>User baton role has been updated successfully!</p>
                        </div>
                        <div class="col-md-4 txtRight pull-right">
                          <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->

</div>
<script type="text/javascript">
$("#sidebarnav>li#menu10").addClass('active').find('a').addClass('active');
$("#sidebarnav>li#menu10").find("li").eq(0).addClass('active').find('a').addClass('active');
//setup before functions
var typingTimer;                //timer identifier
var UsertypingTimer;                //timer identifier
var doneTypingInterval = 1000;  //time in ms, 5 second for example
var $input = $('#user_search');
var $userInput = $('#search_unoccupied_role');
var $batonInput = $('#batonRoleText');
var $batonInputCheck = $('#baton_is_oncall');

//on keyup, start the countdown
$input.on('keyup', function () {
  clearTimeout(typingTimer);
  typingTimer = setTimeout(searchBaton, doneTypingInterval);
});

//on keydown, clear the countdown
$input.on('keydown', function () {
  clearTimeout(typingTimer);
});

//on keyup, start the countdown
$userInput.on('keyup', function () {
  clearTimeout(UsertypingTimer);
  UsertypingTimer = setTimeout(searchUserList, doneTypingInterval);
});

//on keydown, clear the countdown
$userInput.on('keydown', function () {
  clearTimeout(UsertypingTimer);
});


$batonInput.on('keyup', function () {
  $text = $batonInput.val().trim();

  $('#addBatonPopup .showSucess').hide();
  if($text == ''){
    $('#addBatonPopup .showWarning').hide();
  }else{
    $('#addBatonPopup .showWarning').show();
  }
});

$(document).ready(function() {
  getBatonList('1','20',$input.val().trim());
});

  function getBatonList(page,limit,$search_text){
    $("#batonListLoader").fadeIn("fast");
    $.ajax({
      url:'<?php echo BASE_URL; ?>institution/InstitutionBaton/batonFilter',
      type: "POST",
      data: {'page_number':page,'limit':limit,'search_text':$search_text},
      success: function(data) {
        $('#ajaxContent').html(data);
        $("#batonListLoader").fadeOut("fast");
        $('#showTotCount').html($('#totalCount').html());
      }
    });
  }
// });
function searchBaton() {
   $search_text = $input.val().trim();
   $input.blur();
   $("#batonListLoader").fadeIn("fast");
   $.ajax({
       url:'<?php echo BASE_URL; ?>institution/InstitutionBaton/batonFilter',
       type: "POST",
       data: {'search_text':$search_text},
       success: function(data) {
           $('#ajaxContent').html(data);
           $('#showTotCount').html($('#totalCount').html());
           $("#batonListLoader").fadeOut("fast");
       }
   });
}
function searchUserList() {
   $search_text = $userInput.val().trim();
   if($search_text == ''){
    $('#batonRoleList').html('');
    $('#userAssignPopUp .showWarning').hide();
    $('#userAssignPopUp .showSucess').hide();
    return false;
   }
   $userInput.blur();
   $("#batonUserListLoader").fadeIn("fast");
   $.ajax({
       url:'<?php echo BASE_URL; ?>institution/InstitutionBaton/getUserList',
       type: "POST",
       data: {'search_text':$search_text},
       success: function(data) {
           $('#batonListing').scrollTop(0);
           $('#batonRoleList').html(data);
           $('#userAssignPopUp .showWarning').hide();
           $('#userAssignPopUp .showSucess').hide();
           $("#batonUserListLoader").fadeOut("fast");
       }
   });
}

$('body').on('click','#batonRoleList li.assignUser',function(event){
  event.preventDefault();
  $('#batonRoleList li.assignUser').removeClass('selected');
  $(this).addClass('selected');
  $('#userAssignPopUp .showWarning').show();
  $('#userAssignPopUp .showSucess').hide();
});

function assignBatonRole(){
  var role_id = $('#userAssignPopUp').attr('data-roleid');
  var user_id = $('#batonRoleList li.assignUser.selected').attr('data-user');
  $("#batonUserListLoader").fadeIn("fast");
  $.ajax({
      url:'<?php echo BASE_URL; ?>institution/InstitutionBaton/assignUnassignBatonRole',
      type: "POST",
      data: {'user_id':user_id, 'role_id':role_id,'status':1},
      success: function(data) {
        console.log(data);
        var responseArray = JSON.parse(data);
        if(responseArray['status'] == 1){
          $userInput.val('');
          $userInput.attr('disabled',true);
          $('#batonRoleList').empty()
          $('#userAssignPopUp .showWarning').hide();
          $('#userAssignPopUp .showSucess').show();
        }else{
          alert(responseArray['message']);
        }
        $("#batonUserListLoader").fadeOut("fast");
      }
  });

}

function addBatonRole(){
  $text = $batonInput.val().trim();
  $check = $batonInputCheck.is(':checked');
  $on_call_val = $check == true ? 1 : 0;
  $("#addBatonLoader").fadeIn("fast");
  $.ajax({
      url:'<?php echo BASE_URL; ?>institution/InstitutionBaton/addBatonRole',
      type: "POST",
      data: {'role_name':$text, 'on_call_value':$on_call_val},
      success: function(data) {
        console.log(data);
        var responseArray = JSON.parse(data);
        if(responseArray['status'] == 1){
          $batonInput.val('');
          $batonInputCheck.prop('checked', false);
          $('#batonRoleList').empty()
          $('#addBatonPopup .showWarning').hide();
          $('#addBatonPopup .showSucess').show();
        }else{
          alert(responseArray['message']);
        }
        $("#addBatonLoader").fadeOut("fast");
      }
  });
}

function chngPage(val,limit) {
   var a=$('tr th.active').attr('id');
   var b=$('tr th.ASC').attr('id');
   if(a==b){
       var order='A';
   }
   else{
       var order='DE';
   }
   $search_text = $input.val().trim();
   var goVal=$("#chngPage").val();
   if(parseInt(goVal) <= Math.ceil(val) && Math.ceil(goVal)>0){
       $("#batonListLoader").fadeIn("fast");
       $.ajax({
           url:'<?php echo BASE_URL; ?>institution/InstitutionBaton/batonFilter',
           type: "POST",
           data: {'page_number':goVal,'limit':limit,'sort':a,'order':order,'search_text':$search_text},
           success: function(data) {
               $('#ajaxContent').html(data);
               $('#showTotCount').html($('#totalCount').html());
               $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
               if(a==b){
                   $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
               }
               else{
                   $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
               }
               $("#batonListLoader").fadeOut("fast");
           }
       });
   }

}
function chngCount(val){
   $("#batonListLoader").fadeIn("fast");
   var a=$('tr th.active').attr('id');
   var b=$('tr th.ASC').attr('id');
   if(a==b){
       var order='A';
   }
   else{
       var order='DE';
   }
   $search_text = $input.val().trim();
 $.ajax({
     url:'<?php echo BASE_URL; ?>institution/InstitutionBaton/batonFilter',
     type: "POST",
     data: {'limit':val,'sort':a,'order':order,'search_text':$search_text},
     success: function(data) {
         $('#ajaxContent').html(data);
         $('#showTotCount').html($('#totalCount').html());
         $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
         if(a==b){
         $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
         }
         else{
         $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
         }
         $("#batonListLoader").fadeOut("fast");
     }
 });
}

function abc(val,limit){
   $("#batonListLoader").fadeIn("fast");
   var a=$('tr th.active').attr('id');
   var b=$('tr th.ASC').attr('id');
   if(a==b){
       var order='A';
   }
   else{
       var order='DE';
   }
   $search_text = $input.val().trim();
 $.ajax({
      url:'<?php echo BASE_URL; ?>institution/InstitutionBaton/batonFilter',
      type: "POST",
      data: {'page_number':val,'limit':limit,'sort':a,'order':order,'search_text':$search_text},
      success: function(data) {
           $('#ajaxContent').html(data);
           $('#showTotCount').html($('#totalCount').html());
          $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
          if(a==b){
          $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
          }
          else{
          $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
          }
          $("#batonListLoader").fadeOut("fast");
      }
   });
}
</script>
<script type="text/javascript">
$(function () {
  $('#userAssignPopUp').on('hide.bs.modal', function (event) {
    $('#search_unoccupied_role').val('');
    $('#batonRoleList').empty();
    if($('#userAssignPopUp .showSucess').css('display') == 'block'){
      reload();
    }
  });
  $('#userAssignPopUp').on('show.bs.modal', function (event) {
    if (event.namespace === 'bs.modal') {
        var button = $(event.relatedTarget);
        var role_id = button.data('roleid');
        var on_call_value = button.data('oncall');
        $('#userAssignPopUp').attr('data-roleid',role_id);
        $('#userAssignPopUp').attr('data-oncall',on_call_value);

        $('#userAssignPopUp .showWarning').hide();
        $('#userAssignPopUp .showSucess').hide();
        $('#batonUserListLoader').hide();
        $userInput.attr('disabled',false);
    }
  });

  $('#addBatonPopup').on('hide.bs.modal', function (event) {
    if($('#addBatonPopup .showSucess').css('display') == 'block'){
      reload();
    }
  });
  $('#addBatonPopup').on('show.bs.modal', function (event) {
    if (event.namespace === 'bs.modal') {
        $batonInput.val('');
        $batonInputCheck.prop('checked', false);
        $('#addBatonLoader').hide();
        $('#addBatonPopup .showWarning').hide();
        $('#addBatonPopup .showSucess').hide();
    }
  });
});
function reload(){
    // $('#customModalBody').removeClass('bodyCustom');
    // $('#customModalFoot').css('text-align','center');
    var page = parseInt($('#sample_1_paginate ul.pagination li.active.num a #chngPage').val());
    var limit = $('#Data_Count').val();
    var $search_text = $input.val().trim();
    getBatonList(page,limit,$search_text);
}
</script>
