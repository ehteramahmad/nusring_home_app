<?php

$searchdata['status'] = 0;
//** AJAX Pagination
$this->Js->JqueryEngine->jQueryObject = 'jQuery';
// Paginator options
$this->Paginator->options(array(
    'update' => '#ajaxContent',
    'evalScripts' => true,
    'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
    'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
    )
);
?>
<div id="professionLoader" class="loader"></div>
<style type="text/css">
.loader {
  position: fixed;
  left:0px;
  top: 0px;
  width: 100%;
  height: 100%;
  z-index: 9999;
  background: url('<?php echo BASE_URL; ?>institution/page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
}
.calCustom {
    font-size: 1.4em;
    top:8px;
    right:14px;
}

</style>
<div class="container-fluid" id="registration_report">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-6 col-6 align-self-center">
          <h3 class="text-themecolor m-b-0 m-t-0">Settings: <span style="color:#10a5da;">Profession</span></h3>
            <!-- <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php // echo BASE_URL ?>institution/InstitutionHome/index">Home</a></li>
                <li class="breadcrumb-item active">Available / OnCall Transactions</li>
            </ol> -->
        </div>
    </div>
    <div class="row breadcrumb-title">
      <ol class="breadcrumb">
          <!-- <li class="breadcrumb-item"><a href="<?php //echo BASE_URL ?>institution/InstitutionHome/index"><i class="demo-icon icon-sub-home"></i></a></li>
          <li class="breadcrumb-item active">User Registration Report</li> -->
      </ol>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->


    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row boaderLines" id="profession_block">
      <div class="col-12">
          <div class="card">
              <div class="card-block padLeftRight0 pad0">
                <div class="floatLeftFull padLeftRight14 newListStyle">
                   <div class="tableTop floatLeft">
                     <div id="list-filter-options">
                       <input type="radio" name="filter_option" value="all" id="filter_option_all" checked="checked">
                       <label for="filter_option_all">All (<span id="showTotCount">0</span>)</label>
                       <input type="radio" name="filter_option" value="1" id="filter_option_await">
                       <label for="filter_option_await">Active (<span id="showActiveCount">0</span>)</label>
                       <input type="radio" name="filter_option" value="0" id="filter_option_approved">
                       <label for="filter_option_approved">Inactive (<span id="showInactiveCount">0</span>)</label>
                       <button style="" class="btn add_new_prof" onclick="location.href='<?php echo BASE_URL.'institution/InstitutionSetting/addProfession'; ?>'">+ Add New</button>
                     </div>
                   </div>
                   <div id="userlisting_filter" class="dataTables_filter">
                      <label><i class="demo-icon icon-sub-lense"></i></label>
                      <input id="user_search" type="search" class="form-control input-sm" placeholder="Search" aria-controls="userlisting">
                   </div>
                </div>
                <input class="is-hidden" type="text" id="type_val" name="" value="">
                <div class="table-responsive wordBreak" id="professionContent">
                  <table class="table stylish-table customPading lineHeight36">
                     <thead>
                         <tr>
                             <th style="width:45%; padding-left:20px">Profession</th>
                             <th style="width:20%; text-align:center;">Status</th>
                             <th style="width:15%; text-align:center;">Edit</th>
                             <th style="width:20%;">Actions</th>
                         </tr>
                     </thead>
                     <tbody>
                       <tr>
                         <td colspan="8" align="center">
                             <font color="green">Please wait while we are loading the list...</font>
                         </td>
                       </tr>
                     </tbody>
                  </table>
                </div>
              </div>
          </div>
      </div>
    </div>
    <!-- Row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!---Update Profession Start[Start]-->
    <div class="modal fade" id="updateProf" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2">
      <div class="loader" id="updateLoader"></div>
      <div class="modal-dialog" role="document">
        <div class="modal-content uploadFileBlock" id="bulkUploadBase">
          <div class="modal-header">
            <!-- <h4 class="modal-title" id="exampleModalLabel2">Add Base Location</h4> -->
            <h4 class="modal-title" id="exampleModalLabel2">Update Profession</h4>
            <p>You can update Profession by providing information below:</p>
          </div>
          <div class="modal-body">
            <form id="bulkEmailUpdate">
              <div id="inputUpdateProfession" class="form-group">
                <input type="baseLocation" class="form-control" id="updateProfessionTxt" name="profession" placeholder="Enter Profession">
                <div class="form-control-feedback" id="emptyUpdateProfession" style="display:none">Please Enter Profession</div>
                <div class="form-control-feedback" id="alreadyUpdateProfession" style="display:none"></div>
              </div>
              <div class="onCallBtMain txtCenter marBottom30">
                <button type="button" style="background-color:#00A777;color:#ffffff;" class="btn btn-onCallBt" onclick="updateProfession();" id="updateBasebtn">Update Profession</button>
                <button type="button" class="btn btn-onCallBt" data-dismiss="modal">Cancel</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!---Update Profession Start[END]-->

</div>
<script type="text/javascript">

</script>
<script type="text/javascript">
$("#sidebarnav>li#menu4").addClass('active').find('a').eq(0).addClass('active');
$("#sidebarnav>li#menu4").find("li").eq(5).addClass('active').find('a').addClass('active');

$(document).ready(function(){
  $type = $('#type_val').val();
  if($type == ''){
    $type = 'all';
  }
  $('input[name="filter_option"]').attr('checked',false);
  $('input[name="filter_option"][value="'+$type.trim()+'"]').attr('checked',true);
  var list_type = $('input[name="filter_option"]:checked').val();
  getProfession('1','20',$input.val().trim(),list_type);
});
$('body').on('click','input[name="filter_option"]',function(){
  $('#type_val').html($(this).val());
  getProfession('1','20',$input.val().trim(),$(this).val());
});
// --------- INITIAL FUNCTIONS ------------
  //setup before functions
  var typingTimer;                //timer identifier
  var doneTypingInterval = 1000;  //time in ms, 5 second for example
  var $input = $('#user_search');

  //on keyup, start the countdown
  $input.on('keyup', function () {
    clearTimeout(typingTimer);
    typingTimer = setTimeout(searchProfession, doneTypingInterval);
  });
  //on keydown, clear the countdown
  $input.on('keydown', function () {
    clearTimeout(typingTimer);
  });
// --------- INITIAL FUNCTIONS ------------
  function getProfession(page,limit,$search_text,$type){
    $("#professionLoader").fadeIn("fast");
    getProfDashCount($search_text);
    $.ajax({
      url:'<?php echo BASE_URL; ?>institution/InstitutionSetting/getProfession',
      type: "POST",
      data: {'page_number':page,'limit':limit,'search_text':$search_text,'type':$type},
      success: function(data) {
        console.log(data);
        $("#professionLoader").fadeOut("fast");
        $('#professionContent').html(data);
      }
    });
  }

  function getProfDashCount($search_text){
    $.ajax({
      url:'<?php echo BASE_URL; ?>institution/InstitutionSetting/getProfDashCount',
      type: "POST",
      data: {'search_text':$search_text},
      success: function(data) {
        console.log(data);
        data = JSON.parse(data);
        console.log(data);
        $('#showTotCount').html(data.all);
        $('#showActiveCount').html(data.active);
        $('#showInactiveCount').html(data.inactive);
      }
    });
  }

  //user is "finished typing," do something
  function searchProfession() {
     $type = $('input[name="filter_option"]:checked').val();
     $search_text = $input.val().trim();
     if($search_text == ''){
         // return false;
     }
     $input.blur();
     $("#professionLoader").fadeIn("fast");
     getProfDashCount($search_text);
     $.ajax({
         url:'<?php echo BASE_URL; ?>institution/InstitutionSetting/getProfession',
         type: "POST",
         data: {'type':$type, 'search_text':$search_text},
         success: function(data) {
           $("#professionLoader").fadeOut("fast");
           $('#professionContent').html(data);
         }
     });
  }

  function showData(id){
    if($('#professionContent tr[data-target="'+id+'"]').attr('aria-expanded')=='true'){
      $('#professionContent tr').attr('aria-expanded','false');
      $('#professionContent tr.transition_div').removeClass('show').attr('aria-expanded','false');
      $('.editBlock_main img').attr('src',"<?php echo BASE_URL ?>institution/img/edit.svg");
    }else{
      $('#professionContent tr').attr('aria-expanded','false');
      $('#professionContent tr.transition_div').removeClass('show').attr('aria-expanded','false');
      // id = $(this).data('target');
      $('.editBlock_main img').attr('src',"<?php echo BASE_URL ?>institution/img/edit.svg");
      $('#professionContent tr[data-target="'+id+'"]').attr('aria-expanded','true');
      $('#professionContent tr[data-target="'+id+'"] .editBlock_main img').attr('src',"<?php echo BASE_URL ?>institution/img/edit_white.svg");
      $(id).addClass('show').attr('aria-expanded','true');
    }
  }

  $(function () {
    $('#updateProf').on('show.bs.modal', function (event) {
      $("#updateLoader").fadeOut("fast");
      var button = $(event.relatedTarget);
      var id = button.data('id');
      var value = button.data('name');
      console.log($(event.relatedTarget));
      console.log(id);
      // var value = $('#baseLocationContent td[data-id="'+id+'"]').html();
      $('#updateProfessionTxt').val(value);
      $('#updateProf #updateBasebtn').attr('data-id',id);
      $("#emptyUpdateProfession").hide();
      $("#alreadyUpdateProfession").hide();
      $("#inputUpdateProfession").removeClass('has-danger');
    });
  });

  function updateProfession(){
    var name = $('#updateProfessionTxt').val();
    var id = $('#updateProf #updateBasebtn').attr('data-id');

    $("#emptyUpdateProfession").hide();
    $("#inputUpdateProfession").removeClass('has-danger');
    if(name.trim() == ''){
      $("#inputUpdateProfession").addClass('has-danger');
      $("#emptyUpdateProfession").show();
    }else{
      changeStatus(id,1,name,'update');
    }
  }

  function changeStatus(id,status,value,request_type){
    $("#professionLoader").fadeIn("fast");
    $.ajax({
        url:'<?php echo BASE_URL; ?>institution/InstitutionSetting/updateProfState',
        type: "POST",
        data: {'id':id, 'status':status, 'value':value, 'request_type':request_type},
        success: function(data) {
          $("#professionLoader").fadeOut("fast");
          var responseArray = JSON.parse(data);
          if(request_type == 'update'){
            $('#updateProf').modal('toggle');
          }
          if( responseArray['status'] == 1 ){
            var buttonHtml = '<button type="button" onClick="reload();" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
            $('#customModalLabel').html('Confirmation');
            $('#customModalBody').html(responseArray['message']);
            $('#customModalFoot').html(buttonHtml);
            $('#customBox').modal('show');
            $("#customBox .loaderImg").hide();
            $(".loading_overlay").hide();
          }else{
            var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
            $('#customModalLabel').html('Error');
            $('#customModalBody').html(responseArray['message']);
            $('#customModalFoot').html(buttonHtml);
            $('#customBox').modal('show');
            $("#customBox .loaderImg").hide();

            $(".loading_overlay").hide();
          }
        },
        error: function( result ){
          $("#professionLoader").fadeOut("fast");
          var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
          $('#customModalLabel').html('Error');
          $('#customModalBody').html('An error has occurred. Please try again');
          $('#customModalFoot').html(buttonHtml);
          $('#customBox').modal('show');
          $("#customBox .loaderImg").hide();
          $(".loading_overlay").hide();
        }
    });
  }

  function reload(){
    var page = parseInt($('#sample_1_paginate ul.pagination li.active.num a #chngPage').val());
    var limit = $('#Data_Count').val();
    var $search_text = $input.val().trim();
    var list_type = $('input[name="filter_option"]:checked').val();
    getProfession(page,limit,$search_text,list_type);
  }
  function chngPage(val,limit) {
     var list_type = $('input[name="filter_option"]:checked').val();
     $search_text = $input.val().trim();
     var goVal=$("#chngPage").val();

     if(parseInt(goVal) <= Math.ceil(val) && Math.ceil(goVal)>0){
         $("#professionLoader").fadeIn("fast");
         $.ajax({
             url:'<?php echo BASE_URL; ?>institution/InstitutionSetting/getProfession',
             type: "POST",
             data: {'page_number':goVal,'limit':limit,'type':list_type,'search_text':$search_text},
             success: function(data) {
                 $('#professionContent').html(data);
                 $("#professionLoader").fadeOut("fast");
             }
         });
     }

  }
  function chngCount(val){
     $("#professionLoader").fadeIn("fast");
     var list_type = $('input[name="filter_option"]:checked').val();
     $search_text = $input.val().trim();
   $.ajax({
       url:'<?php echo BASE_URL; ?>institution/InstitutionSetting/getProfession',
       type: "POST",
       data: {'limit':val,'type':list_type,'search_text':$search_text},
       success: function(data) {
           $('#professionContent').html(data);
           $("#professionLoader").fadeOut("fast");
       }
   });
  }

 function abc(val,limit){
     $("#professionLoader").fadeIn("fast");
     var list_type = $('input[name="filter_option"]:checked').val();
     $search_text = $input.val().trim();
     console.log(list_type);
   $.ajax({
        url:'<?php echo BASE_URL; ?>institution/InstitutionSetting/getProfession',
        type: "POST",
        data: {'page_number':val,'limit':limit, 'type':list_type,'search_text':$search_text},
        success: function(data) {
            $('#professionContent').html(data);
            $("#professionLoader").fadeOut("fast");
        }
     });
 }
</script>
