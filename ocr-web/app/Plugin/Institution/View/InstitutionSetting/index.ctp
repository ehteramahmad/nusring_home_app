<?php
//print_r($data);
?>
<!-- <div id="nhsLoader" class="loader"></div> -->
<style type="text/css">
.loader {
    position: fixed;
    left:0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('<?php echo BASE_URL; ?>institution/page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
}
#nhsLoader,#ipLoader,#baseLocationLoader {
    position: absolute;
    left:0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('../page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
}

.modal-backdrop.fade {
	display: none;
}
.modal-open .modal-backdrop.fade {
	display: block;
}
</style>
<div class="container-fluid">
	 <!-- ============================================================== -->
	 <!-- Bread crumb and right sidebar toggle -->
	 <!-- ============================================================== -->
	 <div class="row page-titles">
	     <div class="col-md-6 col-6 align-self-center">
	         <h3 class="text-themecolor m-b-0 m-t-0">Settings</h3>
	         <!-- <ol class="breadcrumb">
	             <li class="breadcrumb-item"><a href="<?php // echo BASE_URL ?>institution/InstitutionHome/index">Home</a></li>
	             <li class="breadcrumb-item active">Settings</li>
	         </ol> -->
	     </div>
	 </div>
   <div class="row breadcrumb-title">
     <ol class="breadcrumb">
         <li class="breadcrumb-item"><a href="<?php echo BASE_URL ?>institution/InstitutionHome/index"><i class="demo-icon icon-sub-home"></i></a></li>
         <li class="breadcrumb-item active">Settings</li>
     </ol>
   </div>
	 <!-- ============================================================== -->
	 <!-- End Bread crumb and right sidebar toggle -->
	 <!-- ============================================================== -->

<!-- ============================================================== -->
<!-- Start HTML Content -->
<!-- ============================================================== -->
<div class="row boaderLines">
   <div class="col-12">
       <div id="nhsNType" style="position:relative;">
         <div id="nhsLoader"></div>
         <div class="card">
           <div class="card-block padLeftRight0">
            <div class="floatLeftFull padLeftRight14">
               <div class="tableTop floatLeft">
                  <span class="allIconUsers">
                    <img src="<?php echo BASE_URL ?>institution/img/sub-patient.svg" alt="">
                  </span>
                  <span class="card-title">Patient NHS/MRN Type</span>
               </div>
            </div>
            <div class="table-responsive wordBreak">
               <table class="table stylish-table customPading">
                  <thead>
                     <tr>
                        <th>Current Status</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                       <td>
                        <?php
                            echo "NHS Number";
                        ?>
                       </td>
                       <td>
                        <?php
                          $type = array(
                            0=>'NHS Number',
                            1=>'MRN Number'
                          );
                          echo $this->Form->select('Type', $options = $type, $attributes = array('empty'=>'Select','value'=>0,'id'=>'patient_nType', 'class'=>'bs-select form-control width200 font13'));;
                        ?>
                       </td>
                     </tr>
                  </tbody>
               </table>
            </div>
         </div>
          </div>
       </div>
       <div id="ipRange" style="position:relative;">
         <div id="ipLoader"></div>
         <div class="card">
           <div class="card-block padLeftRight0">
              <div class="floatLeftFull padLeftRight14 borderBottomNew">
                 <div class="tableTop floatLeft">
                    <span class="allIconUsers">
                      <img src="<?php echo BASE_URL ?>institution/img/sub-session.svg" alt="">
                    </span>
                    <span class="card-title">Update Session Time Out</span>
                 </div>
              </div>
              <div class="table-responsive wordBreak">
                <form id="ipSettingView">
                  <div class="row showIPMain padLeftRight14">
                      <div class="col-md-8 pull-left">
                        <p>Please enter a range of IP's to set Session Time out for Medic Bleep web application</p>
                      </div>
                      <div class="col-md-4 pull-right">
                        <span id="addmore_ip_range" class="addMoreBt">+ Add IP Range</span>
                      </div>
                      <div class="col-md-12" style="display:none;" id="sessionError">
                          <span style="color:red;">Please enter Ip</span>
                      </div>
                    </div>
                    <div class="row btCust boxDash" id="ipRangeContent">
                        <div class="col-md-12 without_data" style="display:none;">
                          No Record's Found !
                        </div>
                    </div>
                </form>
              </div>
              <div class="table-responsive wordBreak">
                <form id="ipSettingViewCust">
                  <div class="row showIPMain padLeftRight14">
                      <div class="col-md-8 pull-left">
                        <p>You can enter individual system IP's also.</p>
                      </div>
                      <div class="col-md-4 pull-right">
                        <span id="addmore_ip" class="addMoreBt">+ Add IP</span>
                      </div>
                      <div class="col-md-12" style="display:none;" id="sessionErrorCust">
                          <span style="color:red;">Please enter Ip</span>
                      </div>
                    </div>
                    <div class="row btCust boxDash" id="ipContent">
                        <div class="col-md-12 without_data" style="display:none;">
                          No Record's Found !
                        </div>
                    </div>
                </form>
              </div>
              <div class="col-md-12 padZero">
                <div class="session_request_data col-md-12 pull-left">
                  <div class="row showIPMain padLeftRight14">
                    <div style="display:none;" id="timeError">
                        <span style="color:red; float:left; margin-bottom:10px;">Please enter Time Span in Min</span>
                    </div>
                      <div class="col-md-6 col-lg-5 col-xl-4 pull-left padZero">
                        <p class="leftFloat marTop3">Session Timeout Minutes : </p>
                        <input id="time_range" type="text" class="cusInputTimeBox form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="Time">
                        <span class="custSpan marTop3">Min</span>
                      </div>
                      <div class="col-md-6 col-lg-7 col-xl-8 pull-right txtLeft">
                        <button type="button" class="addMoreBt" onclick="setSession();">Save</button>
                      </div>
                    </div>
                  <!-- <div class="centerField">
                    <div style="display:none;" id="timeError">
                        <span style="color:red; float:left; margin-bottom:10px;">Please enter Time Span in Min</span>
                    </div>
                    <div class="time noDash time_dom_element">
                      <input id="time_range" type="text" class="form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="Time">
                    </div>
                    <button type="button" class="addMoreBt" onclick="setSession();">Save</button>
                    <p>*Session time out in minutes.</p>
                  </div> -->
                </div>
              </div>
           </div>
         </div>
       </div>
       <div id="baseLocation" style="position:relative;">
         <div id="baseLocationLoader"></div>
         <div class="card">
            <div class="card-block padLeftRight0">
               <div class="floatLeftFull padLeftRight14">
                  <div class="tableTop floatLeft">
                     <span class="allIconUsers">
                     <img src="<?php echo BASE_URL ?>institution/img/base-location.svg" alt=""></span>
                     <span class="card-title">Total Base Location:</span>
                     <span class="display-6" id="showTotCount"></span>
                     <div class="assignBtnAll marTop7">
                        <button style="" class="btn bulk_action_btn pull-right btn-primary-green" data-toggle="modal" data-target="#exampleModal">+ Add Base Location</button>
                        <!-- <button class="btn bulk_action_btn pull-right btn-primary-green" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo" id="addUserButton"><i class="mdi mdi-plus-circle"></i> Add Base Location</button> -->
                     </div>
                  </div>
                  <div id="userlisting_filter" class="dataTables_filter">
                     <label><i class="demo-icon icon-sub-lense"></i></label>
                     <input id="user_search" type="search" class="form-control input-sm" placeholder="Search" aria-controls="userlisting">
                  </div>
               </div>
               <div class="table-responsive wordBreak" id="baseLocationContent">
                 <table class="table stylish-table customPading lineHeight36">
                    <thead>
                        <tr>
                            <th style="width:25%;">Base Location </th>
                            <th style="width:25%;">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                      <tr>
                          <td colspan="8" align="center">
                              <font color="green">Please wait while we are loading the list...</font>
                          </td>
                      </tr>
                    </tbody>
                 </table>
               </div>
            </div>
         </div>
       </div>
   </div>
</div>
<!---Add  Base Location Start[Start]-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
  <div class="loader" style="height: 64%; display:none;" id="adduserLoader"></div>
  <div class="modal-dialog" role="document">
    <div class="modal-content uploadFileBlock" id="bulkUploadUser">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel1">Add Base Location</h4>
        <p>You can add base location by providing information below:</p>
      </div>
      <div class="modal-body">
        <form id="bulkEmailUpload">
          <div id="inputBaseLocation" class="form-group">
            <input type="baseLocation" class="form-control" id="baseLocationTxt" name="baseLocation" placeholder="Enter Base Location">
            <div class="form-control-feedback" id="emptyBaseLocation" style="display:none">Please Enter Base Location</div>
            <div class="form-control-feedback" id="alreadyBaseLocation" style="display:none"></div>
          </div>
          <div class="onCallBtMain txtCenter marBottom30">
            <button type="button" style="background-color:#00A777;color:#ffffff;" class="btn btn-onCallBt" onclick="addBaseLocation();" id="addUserbtn1">Add Base Location</button>
            <button type="button" class="btn btn-onCallBt" data-dismiss="modal">Cancel</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!---Add  Base Location Start[END]-->
<!-- ============================================================== -->
<!-- End HTML Content -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- Start HTML Content -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- End HTML Content -->
<!-- ============================================================== -->


</div>
<script type="text/javascript">
$("#sidebarnav>li#menu6").addClass('active').find('a').addClass('active');
//setup before functions
var typingTimer;                //timer identifier
var doneTypingInterval = 1000;  //time in ms, 5 second for example
var $input = $('#user_search');

//on keyup, start the countdown
$input.on('keyup', function () {
  clearTimeout(typingTimer);
  typingTimer = setTimeout(searchUser, doneTypingInterval);
});

//on keydown, clear the countdown
$input.on('keydown', function () {
  clearTimeout(typingTimer);
});

//user is "finished typing," do something
function searchUser() {
   $type = $('.card.selected').attr('data-value');
   $search_text = $input.val().trim();
   if($search_text == ''){
       // return false;
   }
   $input.blur();
   $("#baseLocationLoader").fadeIn("fast");
   $.ajax({
       url:'<?php echo BASE_URL; ?>institution/InstitutionSetting/getBaseLocation',
       type: "POST",
       data: {'type':$type, 'search_text':$search_text},
       success: function(data) {
           $('#baseLocationContent').html(data);
           $('#showTotCount').html($('#totalCount').html());
           $("#baseLocationLoader").fadeOut("fast");
       }
   });
}
$(document).ready(function() {
  getNhsType();
  getIpTimeout();
  getBaseLocation('1','20',$input.val().trim());
  window.removeIpCount = 1;
});

  function getNhsType(){
    $("#nhsLoader").fadeIn("fast");
    $.ajax({
      url:'<?php echo BASE_URL; ?>institution/InstitutionSetting/getNhsType',
      type: "POST",
      success: function(data) {
        $('#nhsNType').html(data);
        $("#nhsLoader").fadeOut("fast");
      }
    });
  }

  function getIpTimeout(){
    $("#ipLoader").fadeIn("fast");
    $.ajax({
      url:'<?php echo BASE_URL; ?>institution/InstitutionSetting/getIpTimeout',
      type: "POST",
      success: function(result) {
        $("#ipLoader").fadeOut("fast");
        var responseArray = JSON.parse(result);
        if( responseArray['status'] == 1 ){
          window.removeIpCount = 1;
          ipHtml_list = '';
          ipHtml_range = '';
          time = '';
          if(responseArray['data']['ip_range'].length > 0){
            for (var i = 0; i < responseArray['data']['ip_range'].length; i++) {

              ip_array_from = responseArray['data']['ip_range'][i]['from'].split('.');
              ip_array_to = responseArray['data']['ip_range'][i]['to'].split('.');
              time = responseArray['data']['ip_range'][i]['time'];

              ipHtml_range += '<div class="col-md-12 with_data">';
              ipHtml_range += '<div class="ipDom">';
              ipHtml_range += '<div class="session_request_data input'+window.removeIpCount+'">';
              ipHtml_range += '<div class="ipadd">';
              ipHtml_range += '<span class="toSize marRight10">Start IP : </span><input type="text" value="'+ip_array_from[0]+'" class="ip1 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
              ipHtml_range += '<input type="text" value="'+ip_array_from[1]+'" class="ip2 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
              ipHtml_range += '<input type="text" value="'+ip_array_from[2]+'" class="ip3 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
              ipHtml_range += '<input type="text" value="'+ip_array_from[3]+'" class="ip4 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="toSize"> To </span>';
              ipHtml_range += '<input type="text" value="'+ip_array_to[3]+'" class="ip5 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000">';
              ipHtml_range += '</div>';
              ipHtml_range += '</div>';
              ipHtml_range += '<div class="remove_ip remove'+(window.removeIpCount)+'" data-input="input'+(window.removeIpCount)+'"><i class="demo-icon icon-close_removefile_icon"></i></div>';
              ipHtml_range += '</div>';
              ipHtml_range += '</div>';
              window.removeIpCount++;
            }
          }else{
            ipHtml_range += '<div class="col-md-12 with_data">';
            ipHtml_range += '<div class="ipDom">';
            ipHtml_range += '<div class="session_request_data input'+window.removeIpCount+'">';
            ipHtml_range += '<div class="ipadd">';
            ipHtml_range += '<span class="toSize marRight10">Start IP : </span><input type="text" value="" class="ip1 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
            ipHtml_range += '<input type="text" value="" class="ip2 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
            ipHtml_range += '<input type="text" value="" class="ip3 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
            ipHtml_range += '<input type="text" value="" class="ip4 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="toSize"> To </span>';
            ipHtml_range += '<input type="text" value="" class="ip5 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000">';
            ipHtml_range += '</div>';
            ipHtml_range += '</div>';
            ipHtml_range += '<div class="remove_ip remove'+(window.removeIpCount)+'" data-input="input'+(window.removeIpCount)+'"><i class="demo-icon icon-close_removefile_icon"></i></div>';
            ipHtml_range += '</div>';
            ipHtml_range += '</div>';
            window.removeIpCount++;
          }
          if(responseArray['data']['ip_list'].length > 0){
            for (var i = 0; i < responseArray['data']['ip_list'].length; i++) {

              ip_array = responseArray['data']['ip_list'][i]['ip'].split('.');
              time = responseArray['data']['ip_list'][i]['time'];

              ipHtml_list += '<div class="col-md-12 with_data">';
              ipHtml_list += '<div class="ipDom">';
              ipHtml_list += '<div class="session_request_data input'+window.removeIpCount+'">';
              ipHtml_list += '<div class="ipadd">';
              ipHtml_list += '<span class="toSize marRight44">IP : </span><input type="text" value="'+ip_array[0]+'" class="ip1 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
              ipHtml_list += '<input type="text" value="'+ip_array[1]+'" class="ip2 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
              ipHtml_list += '<input type="text" value="'+ip_array[2]+'" class="ip3 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
              ipHtml_list += '<input type="text" value="'+ip_array[3]+'" class="ip4 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000">';
              ipHtml_list += '</div>';
              ipHtml_list += '</div>';
              ipHtml_list += '<div class="remove_ip remove'+(window.removeIpCount)+'" data-input="input'+(window.removeIpCount)+'"><i class="demo-icon icon-close_removefile_icon"></i></div>';
              ipHtml_list += '</div>';
              ipHtml_list += '</div>';
              window.removeIpCount++;
            }
          }else{
            ipHtml_list += '<div class="col-md-12 with_data">';
            ipHtml_list += '<div class="ipDom">';
            ipHtml_list += '<div class="session_request_data input'+window.removeIpCount+'">';
            ipHtml_list += '<div class="ipadd">';
            ipHtml_list += '<span class="toSize marRight44">IP : </span><input type="text" value="" class="ip1 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
            ipHtml_list += '<input type="text" value="" class="ip2 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
            ipHtml_list += '<input type="text" value="" class="ip3 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
            ipHtml_list += '<input type="text" value="" class="ip4 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000">';
            ipHtml_list += '</div>';
            ipHtml_list += '</div>';
            ipHtml_list += '<div class="remove_ip remove'+(window.removeIpCount)+'" data-input="input'+(window.removeIpCount)+'"><i class="demo-icon icon-close_removefile_icon"></i></div>';
            ipHtml_list += '</div>';
            ipHtml_list += '</div>';
            window.removeIpCount++;
          }
          console.log(ipHtml_range);
          console.log(ipHtml_list);
          $('#ipRangeContent').append(ipHtml_range);
          $('#ipContent').append(ipHtml_list);
          $('#time_range').val(time);
        }
      }
    });
  }

  function getBaseLocation(page,limit,$search_text){
    $("#baseLocationLoader").fadeIn("fast");
    $.ajax({
      url:'<?php echo BASE_URL; ?>institution/InstitutionSetting/getBaseLocation',
      type: "POST",
      data: {'page_number':page,'limit':limit,'search_text':$search_text},
      success: function(data) {
        $('#baseLocationContent').html(data);
        $("#baseLocationLoader").fadeOut("fast");
        $('#baseLocationContent').html(data);
        $('#showTotCount').html($('#totalCount').html());
      }
    });
  }

  function chngPage(val,limit) {
     var a=$('tr th.active').attr('id');
     var b=$('tr th.ASC').attr('id');
     if(a==b){
         var order='A';
     }
     else{
         var order='DE';
     }
     var list_type = $('.card.selected').attr('data-value');
     $search_text = $input.val().trim();
     var goVal=$("#chngPage").val();
     if(parseInt(goVal) <= Math.ceil(val) && Math.ceil(goVal)>0){
         $("#baseLocationLoader").fadeIn("fast");
         $.ajax({
             url:'<?php echo BASE_URL; ?>institution/InstitutionSetting/getBaseLocation',
             type: "POST",
             data: {'page_number':goVal,'limit':limit,'sort':a,'order':order,'search_text':$search_text},
             success: function(data) {
                 $('#baseLocationContent').html(data);
                 $('#showTotCount').html($('#totalCount').html());
                 $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                 if(a==b){
                     $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                 }
                 else{
                     $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                 }
                 $("#baseLocationLoader").fadeOut("fast");
             }
         });
     }

  }
  function chngCount(val){
     $("#baseLocationLoader").fadeIn("fast");
     var a=$('tr th.active').attr('id');
     var b=$('tr th.ASC').attr('id');
     if(a==b){
         var order='A';
     }
     else{
         var order='DE';
     }
     $search_text = $input.val().trim();
   $.ajax({
       url:'<?php echo BASE_URL; ?>institution/InstitutionSetting/getBaseLocation',
       type: "POST",
       data: {'limit':val,'sort':a,'order':order,'search_text':$search_text},
       success: function(data) {
           $('#baseLocationContent').html(data);
           $('#showTotCount').html($('#totalCount').html());
           $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
           if(a==b){
             $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
           }
           else{
             $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
           }
           $("#baseLocationLoader").fadeOut("fast");
       }
   });
  }

 function abc(val,limit){
     $("#baseLocationLoader").fadeIn("fast");
     var a=$('tr th.active').attr('id');
     var b=$('tr th.ASC').attr('id');
     if(a==b){
         var order='A';
     }
     else{
         var order='DE';
     }
     $search_text = $input.val().trim();
   $.ajax({
        url:'<?php echo BASE_URL; ?>institution/InstitutionSetting/getBaseLocation',
        type: "POST",
        data: {'page_number':val,'limit':limit,'sort':a,'order':order, 'search_text':$search_text},
        success: function(data) {
            $('#baseLocationContent').html(data);
            $('#showTotCount').html($('#totalCount').html());
            $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
            if(a==b){
              $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
            }
            else{
              $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
            }
            $("#baseLocationLoader").fadeOut("fast");
        }
     });
 }

 function reload(){
     $('#customModalBody').removeClass('bodyCustom');
     $('#customModalFoot').css('text-align','center');
     var page = parseInt($('#sample_1_paginate ul.pagination li.active.num a #chngPage').val());
     var limit = $('#Data_Count').val();
     var $search_text = $input.val().trim();
     getBaseLocation(page,limit,$search_text);
 }

 function changeStatus(baseLocationId, status){
   $("#baseLocationLoader").fadeIn("fast");
   $.ajax({
       url: '<?php echo BASE_URL; ?>institution/InstitutionSetting/changeBaseLocationStatus',
       type: 'POST',
       data: {'baseLocationId':baseLocationId, 'status': status},
       success: function( result ){
           $("#baseLocationLoader").fadeOut("fast");
           var responseArray = JSON.parse(result);
           if( responseArray['status'] == 1 ){
               var buttonHtml = '<button type="button" onClick="reload();" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
               $('#customModalLabel').html('Confirmation');
               $('#customModalBody').html(responseArray['message']);
               $('#customModalFoot').html(buttonHtml);
               $('#customBox').modal('show');

           }else{
               var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
               $('#customModalLabel').html('Error');
               $('#customModalBody').html(responseArray['message']);
               $('#customModalFoot').html(buttonHtml);
               $('#customBox').modal('show');

               $(".loading_overlay1").hide();
           }
       },
       error: function( result ){
           $("#baseLocationLoader").fadeOut("fast");
           $("#customBox .loaderImg").hide();
            var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
            $('#customModalLabel').html('Error');
            $('#customModalBody').html('An error has occurred. Please try again.');
            $('#customModalFoot').html(buttonHtml);
            $('#customBox').modal('show');

            $(".loading_overlay1").hide();

       }
   });
  }

  $(function () {
    $('#exampleModal').on('show.bs.modal', function (event) {
      $('#exampleModal input').val('');
      $("#emptyBaseLocation").hide();
      $("#alreadyBaseLocation").hide();
      $("#inputBaseLocation").removeClass('has-danger');
    });
  });
  function addBaseLocation(){
      var baseLocation = $("#baseLocationTxt").val().trim();
      $("#inputBaseLocation").removeClass('has-danger');
      $("#emptyBaseLocation").hide();
      $("#alreadyBaseLocation").hide();
      if(baseLocation == '' || baseLocation == null){
          $("#inputBaseLocation").addClass('has-danger');
          $("#emptyBaseLocation").show();
      }else{
          $('#adduserLoader').fadeIn('fast');
          $.ajax({
          url: '<?php echo BASE_URL; ?>institution/InstitutionSetting/addBaseLocation',
          type: 'POST',
          data: { value: baseLocation },
          success: function( result ){
              $('#adduserLoader').fadeOut('fast');
              var responseArray = JSON.parse(result);
              if( responseArray['status'] == 1 ){
                  var buttonHtml = '<button type="button"  onClick="reload();" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                  $('#customModalLabel').html('Confirmation');
                  $('#customModalBody').html('Base Location added successfully');
                  $('#customModalFoot').html(buttonHtml);
                  $('#customBox').modal('show');

                  $("#customBox .loaderImg").hide();

                  $('#exampleModal').modal('toggle');
              }else{
                  $("#inputBaseLocation").addClass('has-danger');
                  $("#alreadyBaseLocation").show();
                  $("#alreadyBaseLocation").html('* '+responseArray['message']+'.' );
                  $("#emptyBaseLocation").hide();

              }
          },
          error: function( result ){
              $('#adduserLoader').fadeOut('fast');
              var responseArray = JSON.parse(result);
                  $("#inputBaseLocation").addClass('has-danger');
                  $("#alreadyBaseLocation").show();
                  $("#alreadyBaseLocation").html('* '+responseArray['message']+'.' );
                  $("#emptyEmail").hide();
                  $("#emptyBaseLocation").hide();

          }
      });
    }
  }

	function setType(value){
		$("#nhsLoader").fadeIn("fast");
		// $('#customBox').modal('hide');
		var type = value;
        $.ajax({
	        url: '<?php echo BASE_URL; ?>institution/InstitutionSetting/updatePatientType',
	        type: 'POST',
	        data: { type: type },
	        success: function( result ){
	        	$("#nhsLoader").fadeOut("fast");
	            var responseArray = JSON.parse(result);
	            if( responseArray['status'] == 1 ){
	                var buttonHtml = '<button type="button" onClick="getNhsType()" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
	                $('#customModalLabel').html('Confirmation');
	                // $('#customModalBody').html('Please wait while a new user added');
	                $('#customModalBody').html('Updated successfully');
	                $('#customModalFoot').html(buttonHtml);
	                $('#customBox').modal('show');

	                $("#customBox .loaderImg").hide();

	                // location.reload();
	            }else{
	            	var buttonHtml = '<button type="button" onClick="location.reload()" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
	            	$('#customModalLabel').html('Confirmation');
	            	// $('#customModalBody').html('Please wait while a new user added');
	            	$('#customModalBody').html(responseArray['message']);
	            	$('#customModalFoot').html(buttonHtml);
	            	$('#customBox').modal('show');

	            	$("#customBox .loaderImg").hide();

	            }
	        },
	        error: function( result ){
	        	$('.loader').hide();
	            var responseArray = JSON.parse(result);
	                var buttonHtml = '<button type="button" onClick="location.reload()" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
	            	$('#customModalLabel').html('Confirmation');
	            	// $('#customModalBody').html('Please wait while a new user added');
	            	$('#customModalBody').html(responseArray['message']);
	            	$('#customModalFoot').html(buttonHtml);
	            	$('#customBox').modal('show');

	            	$("#customBox .loaderImg").hide();

	        }
	    });
	}
  $('body').on('click','#addmore_ip',function(event){
    event.preventDefault();
    event.stopPropagation();
      if($('#ipContent .ipDom').length == 0){
        // $('#sessionPopUp .with_data').show();
        $('#ipContent .without_data').hide();
      }
      $('#sessionErrorCust').hide();
      var ip = $('#ipContent .ipDom');
      var ipReg = new RegExp("^(?!0)(?!.*\\.$)((1?\\d?\\d|25[0-5]|2[0-4]\\d)(\\.|$)){4}$");
      var returnIp = true;
      var ip_array = [];
      var val1 = '';
      ip.each(function() {
          val1 = $(this).find('input').eq(0).val()+'.'+$(this).find('input').eq(1).val()+'.'+$(this).find('input').eq(2).val()+'.'+$(this).find('input').eq(3).val();
          if(ip_array.indexOf(val1) > -1){
              $('#sessionErrorCust').show().find('span').html("Duplicate entery not allowed");
              returnIp = false;
          }else{
              if($(this).find('input').eq(0).val() == ''){
                $('#sessionErrorCust').show().find('span').html("Please enter IP address");
                returnIp = false;
              }else{
                if(!ipReg.test(val1)){
                    $('#sessionErrorCust').show().find('span').html("Please enter a valid IP address");
                    returnIp = false;
                }else{
                    ip_array.push(val1);
                }
              }
          }
      });

      if(returnIp == true ){

          ipHtml = '<div class="col-md-12 with_data">';
          ipHtml += '<div class="ipDom">';
          ipHtml += '<div class="session_request_data input'+window.removeIpCount+'">';
          ipHtml += '<div class="ipadd">';
          ipHtml += '<span class="toSize marRight44">IP : </span><input type="text" class="ip1 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
          ipHtml += '<input type="text" class="ip2 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
          ipHtml += '<input type="text" class="ip3 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
          ipHtml += '<input type="text" class="ip4 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000">';
          ipHtml += '</div>';
          ipHtml += '</div>';
          ipHtml += '<div class="remove_ip remove'+(window.removeIpCount)+'" data-input="input'+(window.removeIpCount)+'"><i class="demo-icon icon-close_removefile_icon"></i></div>';
          ipHtml += '</div>';
          ipHtml += '</div>';

          window.removeIpCount++;

          $('#ipContent').append(ipHtml);
      }
  });
  $('body').on('click','#addmore_ip_range',function(event){
    event.preventDefault();
    event.stopPropagation();
      if($('#ipRangeContent .ipDom').length == 0){
        $('#ipRangeContent .without_data').hide();
      }
      $('#sessionError').hide();
      var ip = $('#ipRangeContent .ipDom');
      var ipReg = new RegExp("^(?!0)(?!.*\\.$)((1?\\d?\\d|25[0-5]|2[0-4]\\d)(\\.|$)){4}$");
      var returnIp = true;
      var ip_array = [];
      var val1 = '';
      var val2 = '';
      ip.each(function() {
        val1 = $(this).find('input').eq(0).val()+'.'+$(this).find('input').eq(1).val()+'.'+$(this).find('input').eq(2).val()+'.'+$(this).find('input').eq(3).val();
        val2 = $(this).find('input').eq(0).val()+'.'+$(this).find('input').eq(1).val()+'.'+$(this).find('input').eq(2).val()+'.'+$(this).find('input').eq(4).val();
          if(ip_array.indexOf(val1) > -1){
              $('#sessionError').show().find('span').html("Duplicate entery not allowed");
              returnIp = false;
          }else{
              if($(this).find('input').eq(0).val() == '' || $(this).find('input').eq(4).val() == ''){
                $('#sessionError').show().find('span').html("Please enter IP address");
                returnIp = false;
              }else{
                if(!ipReg.test(val1) || !ipReg.test(val2)){
                    $('#sessionError').show().find('span').html("Please enter a valid IP address");
                    returnIp = false;
                }else{
                    ip_array.push(val1);
                    ip_array.push(val2);
                }
              }
          }
      });

      if(returnIp == true ){

          ipHtml = '<div class="col-md-12 with_data">';
          ipHtml += '<div class="ipDom">';
          ipHtml += '<div class="session_request_data input'+window.removeIpCount+'">';
          ipHtml += '<div class="ipadd">';
          ipHtml += '<span class="toSize marRight10">Start IP : </span><input type="text" class="ip1 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
          ipHtml += '<input type="text" class="ip2 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
          ipHtml += '<input type="text" class="ip3 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
          ipHtml += '<input type="text" class="ip4 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="toSize"> To </span> ';
          ipHtml += '<input type="text" class="ip5 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000">';
          ipHtml += '</div>';
          ipHtml += '</div>';
          ipHtml += '<div class="remove_ip remove'+(window.removeIpCount)+'" data-input="input'+(window.removeIpCount)+'"><i class="demo-icon icon-close_removefile_icon"></i></div>';
          ipHtml += '</div>';
          ipHtml += '</div>';
          window.removeIpCount++;

          $('#ipRangeContent').append(ipHtml);
      }

  });
  $('body').on('click','.remove_ip',function(){
      var cust_class = $(this).attr('data-input');
      $('.ipDom').find('div.'+cust_class).parents('.with_data').remove();
      $('.timeDom').find('div.'+cust_class).parents('.with_data').remove();
      $(this).remove();
      if($('#ipRangeContent .ipDom').length == 0){
        $('#ipRangeContent .without_data').show();
      }
      if($('#ipContent .ipDom').length == 0){
        $('#ipContent .without_data').show();
      }
  });

  function setSession(){
    var ip1 = $('#ipContent .ipDom');
    var ip2 = $('#ipRangeContent .ipDom');
    var time = $('#time_range').val().trim();

    var ipReg = new RegExp("^(?!0)(?!.*\\.$)((1?\\d?\\d|25[0-5]|2[0-4]\\d)(\\.|$)){4}$");

    var returnIp1 = true;
    var returnIp2 = true;
    var returnTime = true;

    var ip_array1 = [];
    var ip_array2 = [];
    var ip_range = [];

    var val1 = '';
    var val2 = '';
    var val3 = '';

    $('#timeError').hide();
    $('#sessionErrorCust').hide();
    $('#sessionError').hide();

    ip1.each(function() {
        val1 = $(this).find('input').eq(0).val()+'.'+$(this).find('input').eq(1).val()+'.'+$(this).find('input').eq(2).val()+'.'+$(this).find('input').eq(3).val();
        if(val1 != '...'){
          if(ip_array1.indexOf(val1) > -1){
              $('#sessionErrorCust').show().find('span').html("Duplicate entery not allowed");
              returnIp1 = false;
          }else{
            if($(this).find('input').eq(0).val() == ''){
              $('#sessionErrorCust').show().find('span').html("Please enter IP address");
              returnIp1 = false;
            }else{
              if(!ipReg.test(val1)){
                  $('#sessionErrorCust').show().find('span').html("Please enter a valid IP address");
                  returnIp1 = false;
              }else{
                  ip_array1.push(val1);
              }
            }
          }
        }
    });

    ip2.each(function() {
      val2 = $(this).find('input').eq(0).val()+'.'+$(this).find('input').eq(1).val()+'.'+$(this).find('input').eq(2).val()+'.'+$(this).find('input').eq(3).val();
      val3 = $(this).find('input').eq(0).val()+'.'+$(this).find('input').eq(1).val()+'.'+$(this).find('input').eq(2).val()+'.'+$(this).find('input').eq(4).val();
        if(val2 != '...' && val3 != '...'){
          if(ip_array2.indexOf(val2) > -1){
              $('#sessionError').show().find('span').html("Duplicate entery not allowed");
              returnIp2 = false;
          }else{
              if($(this).find('input').eq(0).val() == '' || $(this).find('input').eq(4).val() == ''){
                $('#sessionError').show().find('span').html("Please enter IP address");
                returnIp2 = false;
              }else{
                if(!ipReg.test(val2) || !ipReg.test(val3)){
                    $('#sessionError').show().find('span').html("Please enter a valid IP address");
                    returnIp2 = false;
                }else{
                    ip_array2.push(val2);
                    ip_array2.push(val3);
                    var range = {};
                    range.from = val2;
                    range.to = val3;
                    ip_range.push(range);
                }
              }
          }
        }
    });

    console.log($('#ipRangeContent .ipDom').length);
    console.log($('#ipContent .ipDom').length);
    console.log(time);

    if(time == ''){
      $('#timeError').show().find('span').html("Please enter Time in Min.");
      returnTime = false;
    }

    if(returnIp1 == true && returnIp2 == true && returnTime == true){
      $("#ipLoader").fadeIn("fast");
      $.ajax({
        url:'<?php echo BASE_URL; ?>institution/InstitutionSetting/setIpTimeout',
        type: "POST",
        data: {'ip_range':JSON.stringify(ip_range),'ips':JSON.stringify(ip_array1),'time':time},
        success: function(data) {
          $("#ipLoader").fadeOut("fast");
          var responseArray = JSON.parse(data);
          if( responseArray['status'] == 1 ){
              var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
              $('#customModalLabel').html('Confirmation');
              $('#customModalBody').html('Session time updated successfully');
              $('#customModalFoot').html(buttonHtml);
              $('#customBox').modal('show');

              $("#customBox .loaderImg").hide();
          }else{

             var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
             $('#customModalLabel').html('Error');
             $('#customModalBody').html('An error has occurred. Please try again');
             $('#customModalFoot').html(buttonHtml);
             $('#customBox').modal('show');

             $("#customBox .loaderImg").hide();
          }
        }
      });
    }
  }
</script>
