<?php
$searchdata['status'] = 0;
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
        'update' => '#ajaxContent',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
?>
<div id="ipLoader" class="loader"></div>
<style type="text/css">
#ipLoader {
    position: absolute;
    left:0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('../page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
}
.modal-backdrop.fade {
	display: none;
}
.modal-open .modal-backdrop.fade {
	display: block;
}
</style>
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
        <div class="row page-titles">
          <div class="col-md-6 col-6 align-self-center">
              <h3 class="text-themecolor m-b-0 m-t-0">Settings: <span style="color:#10a5da;">Session Time</span></h3>
          </div>
        </div>
        <div class="row breadcrumb-title">
          <ol class="breadcrumb">
              <!-- <li class="breadcrumb-item"><a href="<?php //echo BASE_URL ?>institution/InstitutionHome/index"><i class="demo-icon icon-sub-home"></i></a></li>
              <li class="breadcrumb-item active">Screen Shot Transactions</li> -->
          </ol>
        </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->


    <!-- ============================================================== -->
<!-- Start HTML Content -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- End HTML Content -->
<!-- ============================================================== -->


    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->

    <div class="row boaderLines">
        <div class="col-12">
          <div id="ipRange" style="position:relative;">
            <div class="card">
                <div class="card-block padLeftRight0 pad0">
                  <div class="floatLeftFull padLeftRight40 groupCustom newListStyle">
                     <div class="tableTop floatLeft marTop10">
                        <span class="card-title">Please enter a range of IP's to set Session Time out for Medic Bleep web application</span>
                     </div>
                  </div>
                  <div class="card-note">
                    <label>Note: </label>
                    <div class="note-des">
                      You can add multiple IP Range(s).
                    </div>
                  </div>
                  <div class="table-responsive wordBreak">
                    <form id="ipSettingView">
                        <div class="row btCust boxDash" id="ipRangeContent">
                            <div class="col-md-12 without_data" style="display:none;">
                              No Record's Found !
                            </div>
                            <div class="col-md-12 with_data">
                             <div class="ipDom">
                                <div class="session_request_data input1">
                                   <div class="ipadd">
                                     <span class="toSize marRight10">IP Range : </span>
                                     <input type="text" value="" class="ip1 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 &amp;&amp; event.charCode <= 57);" placeholder="000">
                                     <span class="dotSize">.</span>
                                     <input type="text" value="" class="ip2 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 &amp;&amp; event.charCode <= 57);" placeholder="000">
                                     <span class="dotSize">.</span>
                                     <input type="text" value="" class="ip3 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 &amp;&amp; event.charCode <= 57);" placeholder="000">
                                     <span class="dotSize">.</span>
                                     <input type="text" value="" class="ip4 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 &amp;&amp; event.charCode <= 57);" placeholder="000">
                                     <span class="toSize"> To </span>
                                     <input type="text" value="" class="ip5 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 &amp;&amp; event.charCode <= 57);" placeholder="000">
                                   </div>
                                </div>
                                <div class="remove_ip remove1" data-input="input1"><i class="demo-icon icon-close_removefile_icon"></i></div>
                             </div>
                          </div>
                        </div>
                    </form>
                  </div>
                  <div class="row showIPMain padLeftRight14">
                    <div class="col-md-12" style="display:none;" id="sessionError">
                        <span style="color:red;">Please enter Ip</span>
                    </div>
                    <div class="col-md-7 border-line"></div>
                    <div class="col-md-2 pull-right">
                      <span id="addmore_ip_range" class="addMoreBt">+ Add IP Range</span>
                    </div>
                  </div>
                  <div class="card-note">
                    <label>Note: </label>
                    <div class="note-des">
                      You can add multiple IP(s).
                    </div>
                  </div>
                  <div class="table-responsive wordBreak">
                    <form id="ipSettingViewCust">
                        <div class="row btCust boxDash" id="ipContent">
                            <div class="col-md-12 without_data" style="display:none;">
                              No Record's Found !
                            </div>
                            <div class="col-md-12 with_data">
                               <div class="ipDom">
                                  <div class="session_request_data input2">
                                     <div class="ipadd">
                                       <span class="toSize marRight44">IP : </span>
                                       <input type="text" value="" class="ip1 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 &amp;&amp; event.charCode <= 57);" placeholder="000">
                                       <span class="dotSize">.</span>
                                       <input type="text" value="" class="ip2 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 &amp;&amp; event.charCode <= 57);" placeholder="000">
                                       <span class="dotSize">.</span>
                                       <input type="text" value="" class="ip3 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 &amp;&amp; event.charCode <= 57);" placeholder="000">
                                       <span class="dotSize">.</span>
                                       <input type="text" value="" class="ip4 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 &amp;&amp; event.charCode <= 57);" placeholder="000">
                                     </div>
                                  </div>
                                  <div class="remove_ip remove2" data-input="input2"><i class="demo-icon icon-close_removefile_icon"></i></div>
                               </div>
                            </div>
                        </div>
                    </form>
                  </div>
                  <div class="row showIPMain padLeftRight14">
                      <div class="col-md-12" style="display:none;" id="sessionErrorCust">
                          <span style="color:red;">Please enter Ip</span>
                      </div>
                      <div class="col-md-7 border-line"></div>
                      <div class="col-md-4 pull-right">
                        <span id="addmore_ip" class="addMoreBt">+ Add IP</span>
                      </div>
                    </div>
                  <div class="col-md-12">
                    <div class="session_request_data col-md-12 pull-left">
                      <div class="row showIPMain padLeftRight14">
                        <div class="col-12" style="display:none;" id="timeError">
                            <span style="color:red; float:left; margin-bottom:10px;">Please enter Time Span in Min</span>
                        </div>
                          <div class="col-md-6 col-lg-5 col-xl-4 pull-left padZero">
                            <p class="leftFloat marTop3">Session Timeout : </p>
                            <input id="time_range" type="text" class="cusInputTimeBox form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="Time">
                            <span class="custSpan marTop3">Min</span>
                          </div>
                          <div class="col-md-6 col-lg-7 col-xl-8 pull-right txtLeft">
                            <button type="button" class="addMoreBt" onclick="setSession();">Save</button>
                          </div>
                        </div>
                    </div>
                  </div>
                </div>
            </div>
          </div>
        </div>
    </div>
    <!-- Row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->

</div>
<script type="text/javascript">
$("#sidebarnav>li#menu4").addClass('active').find('a').eq(0).addClass('active');
$("#sidebarnav>li#menu4").find("li").eq(3).addClass('active').find('a').addClass('active');
  $(document).ready(function() {
    getIpTimeout();
    window.removeIpCount = 1;
  });
  function getIpTimeout(){
    $("#ipLoader").fadeIn("fast");
    $.ajax({
      url:'<?php echo BASE_URL; ?>institution/InstitutionSetting/getIpTimeout',
      type: "POST",
      success: function(result) {
        $("#ipLoader").fadeOut("fast");
        var responseArray = JSON.parse(result);
        if( responseArray['status'] == 1 ){
          window.removeIpCount = 1;
          ipHtml_list = '';
          ipHtml_range = '';
          time = '';
          if(responseArray['data']['ip_range'].length > 0){
            for (var i = 0; i < responseArray['data']['ip_range'].length; i++) {

              ip_array_from = responseArray['data']['ip_range'][i]['from'].split('.');
              ip_array_to = responseArray['data']['ip_range'][i]['to'].split('.');
              time = responseArray['data']['ip_range'][i]['time'];

              ipHtml_range += '<div class="col-md-12 with_data">';
              ipHtml_range += '<div class="ipDom">';
              ipHtml_range += '<div class="session_request_data input'+window.removeIpCount+'">';
              ipHtml_range += '<div class="ipadd">';
              ipHtml_range += '<span class="toSize marRight10">IP Range : </span><input type="text" value="'+ip_array_from[0]+'" class="ip1 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
              ipHtml_range += '<input type="text" value="'+ip_array_from[1]+'" class="ip2 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
              ipHtml_range += '<input type="text" value="'+ip_array_from[2]+'" class="ip3 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
              ipHtml_range += '<input type="text" value="'+ip_array_from[3]+'" class="ip4 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="toSize"> To </span>';
              ipHtml_range += '<input type="text" value="'+ip_array_to[3]+'" class="ip5 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000">';
              ipHtml_range += '</div>';
              ipHtml_range += '</div>';
              ipHtml_range += '<div class="remove_ip remove'+(window.removeIpCount)+'" data-input="input'+(window.removeIpCount)+'"><i class="demo-icon icon-close_removefile_icon"></i></div>';
              ipHtml_range += '</div>';
              ipHtml_range += '</div>';
              window.removeIpCount++;
            }
          }else{
            ipHtml_range += '<div class="col-md-12 with_data">';
            ipHtml_range += '<div class="ipDom">';
            ipHtml_range += '<div class="session_request_data input'+window.removeIpCount+'">';
            ipHtml_range += '<div class="ipadd">';
            ipHtml_range += '<span class="toSize marRight10">IP Range : </span><input type="text" value="" class="ip1 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
            ipHtml_range += '<input type="text" value="" class="ip2 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
            ipHtml_range += '<input type="text" value="" class="ip3 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
            ipHtml_range += '<input type="text" value="" class="ip4 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="toSize"> To </span>';
            ipHtml_range += '<input type="text" value="" class="ip5 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000">';
            ipHtml_range += '</div>';
            ipHtml_range += '</div>';
            ipHtml_range += '<div class="remove_ip remove'+(window.removeIpCount)+'" data-input="input'+(window.removeIpCount)+'"><i class="demo-icon icon-close_removefile_icon"></i></div>';
            ipHtml_range += '</div>';
            ipHtml_range += '</div>';
            window.removeIpCount++;
          }
          if(responseArray['data']['ip_list'].length > 0){
            for (var i = 0; i < responseArray['data']['ip_list'].length; i++) {

              ip_array = responseArray['data']['ip_list'][i]['ip'].split('.');
              time = responseArray['data']['ip_list'][i]['time'];

              ipHtml_list += '<div class="col-md-12 with_data">';
              ipHtml_list += '<div class="ipDom">';
              ipHtml_list += '<div class="session_request_data input'+window.removeIpCount+'">';
              ipHtml_list += '<div class="ipadd">';
              ipHtml_list += '<span class="toSize marRight44">IP : </span><input type="text" value="'+ip_array[0]+'" class="ip1 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
              ipHtml_list += '<input type="text" value="'+ip_array[1]+'" class="ip2 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
              ipHtml_list += '<input type="text" value="'+ip_array[2]+'" class="ip3 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
              ipHtml_list += '<input type="text" value="'+ip_array[3]+'" class="ip4 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000">';
              ipHtml_list += '</div>';
              ipHtml_list += '</div>';
              ipHtml_list += '<div class="remove_ip remove'+(window.removeIpCount)+'" data-input="input'+(window.removeIpCount)+'"><i class="demo-icon icon-close_removefile_icon"></i></div>';
              ipHtml_list += '</div>';
              ipHtml_list += '</div>';
              window.removeIpCount++;
            }
          }else{
            ipHtml_list += '<div class="col-md-12 with_data">';
            ipHtml_list += '<div class="ipDom">';
            ipHtml_list += '<div class="session_request_data input'+window.removeIpCount+'">';
            ipHtml_list += '<div class="ipadd">';
            ipHtml_list += '<span class="toSize marRight44">IP : </span><input type="text" value="" class="ip1 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
            ipHtml_list += '<input type="text" value="" class="ip2 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
            ipHtml_list += '<input type="text" value="" class="ip3 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
            ipHtml_list += '<input type="text" value="" class="ip4 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000">';
            ipHtml_list += '</div>';
            ipHtml_list += '</div>';
            ipHtml_list += '<div class="remove_ip remove'+(window.removeIpCount)+'" data-input="input'+(window.removeIpCount)+'"><i class="demo-icon icon-close_removefile_icon"></i></div>';
            ipHtml_list += '</div>';
            ipHtml_list += '</div>';
            window.removeIpCount++;
          }
          console.log(ipHtml_range);
          console.log(ipHtml_list);

          $('#ipRangeContent').find('.with_data').remove();
          $('#ipContent').find('.with_data').remove();

          $('#ipRangeContent').append(ipHtml_range);
          $('#ipContent').append(ipHtml_list);

          $('#time_range').val(time);
        }
      }
    });
  }
  $('body').on('click','#addmore_ip',function(event){
    event.preventDefault();
    event.stopPropagation();
      if($('#ipContent .ipDom').length == 0){
        // $('#sessionPopUp .with_data').show();
        $('#ipContent .without_data').hide();
      }
      $('#sessionErrorCust').hide();
      var ip = $('#ipContent .ipDom');
      var ipReg = new RegExp("^(?!0)(?!.*\\.$)((1?\\d?\\d|25[0-5]|2[0-4]\\d)(\\.|$)){4}$");
      var returnIp = true;
      var ip_array = [];
      var val1 = '';
      ip.each(function() {
          val1 = $(this).find('input').eq(0).val()+'.'+$(this).find('input').eq(1).val()+'.'+$(this).find('input').eq(2).val()+'.'+$(this).find('input').eq(3).val();
          if(ip_array.indexOf(val1) > -1){
              $('#sessionErrorCust').show().find('span').html("Duplicate entery not allowed");
              returnIp = false;
          }else{
              if($(this).find('input').eq(0).val() == ''){
                $('#sessionErrorCust').show().find('span').html("Please enter IP Address");
                returnIp = false;
              }else{
                if(!ipReg.test(val1)){
                    $('#sessionErrorCust').show().find('span').html("Please enter a valid IP Address");
                    returnIp = false;
                }else{
                    ip_array.push(val1);
                }
              }
          }
      });

      if(returnIp == true ){

          ipHtml = '<div class="col-md-12 with_data">';
          ipHtml += '<div class="ipDom">';
          ipHtml += '<div class="session_request_data input'+window.removeIpCount+'">';
          ipHtml += '<div class="ipadd">';
          ipHtml += '<span class="toSize marRight44">IP : </span><input type="text" class="ip1 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
          ipHtml += '<input type="text" class="ip2 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
          ipHtml += '<input type="text" class="ip3 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
          ipHtml += '<input type="text" class="ip4 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000">';
          ipHtml += '</div>';
          ipHtml += '</div>';
          ipHtml += '<div class="remove_ip remove'+(window.removeIpCount)+'" data-input="input'+(window.removeIpCount)+'"><i class="demo-icon icon-close_removefile_icon"></i></div>';
          ipHtml += '</div>';
          ipHtml += '</div>';

          window.removeIpCount++;

          $('#ipContent').append(ipHtml);
      }
  });
  $('body').on('click','#addmore_ip_range',function(event){
    event.preventDefault();
    event.stopPropagation();
      if($('#ipRangeContent .ipDom').length == 0){
        $('#ipRangeContent .without_data').hide();
      }
      $('#sessionError').hide();
      var ip = $('#ipRangeContent .ipDom');
      var ipReg = new RegExp("^(?!0)(?!.*\\.$)((1?\\d?\\d|25[0-5]|2[0-4]\\d)(\\.|$)){4}$");
      var returnIp = true;
      var ip_array = [];
      var val1 = '';
      var val2 = '';
      ip.each(function() {
        val1 = $(this).find('input').eq(0).val()+'.'+$(this).find('input').eq(1).val()+'.'+$(this).find('input').eq(2).val()+'.'+$(this).find('input').eq(3).val();
        val2 = $(this).find('input').eq(0).val()+'.'+$(this).find('input').eq(1).val()+'.'+$(this).find('input').eq(2).val()+'.'+$(this).find('input').eq(4).val();
          if(ip_array.indexOf(val1) > -1){
              $('#sessionError').show().find('span').html("Duplicate entery not allowed");
              returnIp = false;
          }else{
              if($(this).find('input').eq(0).val() == '' || $(this).find('input').eq(4).val() == ''){
                $('#sessionError').show().find('span').html("Please enter IP Range");
                returnIp = false;
              }else{
                if(!ipReg.test(val1) || !ipReg.test(val2)){
                    $('#sessionError').show().find('span').html("Please enter a valid IP Range");
                    returnIp = false;
                }else{
                    ip_array.push(val1);
                    ip_array.push(val2);
                }
              }
          }
      });

      if(returnIp == true ){

          ipHtml = '<div class="col-md-12 with_data">';
          ipHtml += '<div class="ipDom">';
          ipHtml += '<div class="session_request_data input'+window.removeIpCount+'">';
          ipHtml += '<div class="ipadd">';
          ipHtml += '<span class="toSize marRight10">IP Range : </span><input type="text" class="ip1 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
          ipHtml += '<input type="text" class="ip2 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
          ipHtml += '<input type="text" class="ip3 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="dotSize">.</span>';
          ipHtml += '<input type="text" class="ip4 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000"><span class="toSize"> To </span> ';
          ipHtml += '<input type="text" class="ip5 col-md-1 form-control" onkeypress="return (event.charCode==0)||(event.charCode >= 48 && event.charCode <= 57);" placeholder="000">';
          ipHtml += '</div>';
          ipHtml += '</div>';
          ipHtml += '<div class="remove_ip remove'+(window.removeIpCount)+'" data-input="input'+(window.removeIpCount)+'"><i class="demo-icon icon-close_removefile_icon"></i></div>';
          ipHtml += '</div>';
          ipHtml += '</div>';
          window.removeIpCount++;

          $('#ipRangeContent').append(ipHtml);
      }

  });
  $('body').on('click','.remove_ip',function(){
      var cust_class = $(this).attr('data-input');
      $('.ipDom').find('div.'+cust_class).parents('.with_data').remove();
      $('.timeDom').find('div.'+cust_class).parents('.with_data').remove();
      $(this).remove();
      if($('#ipRangeContent .ipDom').length == 0){
        $('#ipRangeContent .without_data').show();
      }
      if($('#ipContent .ipDom').length == 0){
        $('#ipContent .without_data').show();
      }
  });

  function setSession(){
    var ip1 = $('#ipContent .ipDom');
    var ip2 = $('#ipRangeContent .ipDom');
    var time = $('#time_range').val().trim();

    var ipReg = new RegExp("^(?!0)(?!.*\\.$)((1?\\d?\\d|25[0-5]|2[0-4]\\d)(\\.|$)){4}$");

    var returnIp1 = true;
    var returnIp2 = true;
    var returnTime = true;

    var ip_array1 = [];
    var ip_array2 = [];
    var ip_range = [];

    var val1 = '';
    var val2 = '';
    var val3 = '';

    $('#timeError').hide();
    $('#sessionErrorCust').hide();
    $('#sessionError').hide();

    ip1.each(function() {
        val1 = $(this).find('input').eq(0).val()+'.'+$(this).find('input').eq(1).val()+'.'+$(this).find('input').eq(2).val()+'.'+$(this).find('input').eq(3).val();
        if(val1 != '...'){
          if(ip_array1.indexOf(val1) > -1){
              $('#sessionErrorCust').show().find('span').html("Duplicate entery not allowed");
              returnIp1 = false;
          }else{
            if($(this).find('input').eq(0).val() == ''){
              $('#sessionErrorCust').show().find('span').html("Please enter IP Address");
              returnIp1 = false;
            }else{
              if(!ipReg.test(val1)){
                  $('#sessionErrorCust').show().find('span').html("Please enter a valid IP Address");
                  returnIp1 = false;
              }else{
                  ip_array1.push(val1);
              }
            }
          }
        }
    });

    ip2.each(function() {
      val2 = $(this).find('input').eq(0).val()+'.'+$(this).find('input').eq(1).val()+'.'+$(this).find('input').eq(2).val()+'.'+$(this).find('input').eq(3).val();
      val3 = $(this).find('input').eq(0).val()+'.'+$(this).find('input').eq(1).val()+'.'+$(this).find('input').eq(2).val()+'.'+$(this).find('input').eq(4).val();
        if(val2 != '...' && val3 != '...'){
          if(ip_array2.indexOf(val2) > -1){
              $('#sessionError').show().find('span').html("Duplicate entery not allowed");
              returnIp2 = false;
          }else{
              if($(this).find('input').eq(0).val() == '' || $(this).find('input').eq(4).val() == ''){
                $('#sessionError').show().find('span').html("Please enter IP Range");
                returnIp2 = false;
              }else{
                if(!ipReg.test(val2) || !ipReg.test(val3)){
                    $('#sessionError').show().find('span').html("Please enter a valid IP Range");
                    returnIp2 = false;
                }else{
                    ip_array2.push(val2);
                    ip_array2.push(val3);
                    var range = {};
                    range.from = val2;
                    range.to = val3;
                    ip_range.push(range);
                }
              }
          }
        }
    });

    console.log($('#ipRangeContent .ipDom').length);
    console.log($('#ipContent .ipDom').length);
    console.log(time);

    if(time == ''){
      $('#timeError').show().find('span').html("Please enter Time in Min.");
      returnTime = false;
    }

    if(returnIp1 == true && returnIp2 == true && returnTime == true){
      $("#ipLoader").fadeIn("fast");
      $.ajax({
        url:'<?php echo BASE_URL; ?>institution/InstitutionSetting/setIpTimeout',
        type: "POST",
        data: {'ip_range':JSON.stringify(ip_range),'ips':JSON.stringify(ip_array1),'time':time},
        success: function(data) {
          $("#ipLoader").fadeOut("fast");
          var responseArray = JSON.parse(data);
          if( responseArray['status'] == 1 ){
              var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
              $('#customModalLabel').html('Confirmation');
              $('#customModalBody').html('Session time updated successfully');
              $('#customModalFoot').html(buttonHtml);
              $('#customBox').modal('show');

              $("#customBox .loaderImg").hide();
          }else{

             var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
             $('#customModalLabel').html('Error');
             $('#customModalBody').html('An error has occurred. Please try again');
             $('#customModalFoot').html(buttonHtml);
             $('#customBox').modal('show');

             $("#customBox .loaderImg").hide();
          }
        }
      });
    }
  }
</script>
