<?php
// print_r($data);
// exit;
$searchdata['status'] = 0;
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
        'update' => '#ajaxContent',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
?>
<div id="professionDetailLoader" class="loader"></div>
<style type="text/css">
.loader {
    position: fixed;
    left:0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('<?php echo BASE_URL; ?>institution/page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
}
.calCustom {
    font-size: 1.4em;
    top:8px;
    right:14px;
}

</style>
<div class="container-fluid" id="prof_detail">
  <!-- ============================================================== -->
  <!-- Bread crumb and right sidebar toggle -->
  <!-- ============================================================== -->
  <div class="row page-titles">
    <div class="col-md-12 align-self-center col-5">
        <h3 class="text-themecolor m-b-0 m-t-0 col-1 window_history"><img src="<?php echo BASE_URL; ?>institution/img/left-arrow.svg" /></h3>
        <h3 class="text-themecolor m-b-0 m-t-0 col-11" style="float: left;">Settings: <span style="color:#10a5da;">Profession</span></h3>
    </div>
  </div>
  <div class="row breadcrumb-title">
    <ol class="breadcrumb">
    </ol>
  </div>
  <!-- ============================================================== -->
  <!-- End Bread crumb and right sidebar toggle -->
  <!-- ============================================================== -->


  <!-- ============================================================== -->
  <!-- Start HTML Content -->
  <!-- ============================================================== -->
  <div class="row boaderLines">
     <div class="col-12">
        <div class="card">
           <div class="card-block padLeftRight0 pad0">
             <div class="floatLeftFull padLeftRight14 groupCustom newListStyle">
                <div class="tableTop floatLeft marTop10">
                   <span class="card-title"><?php echo isset($data['name']) ? $data['name'] : 'Profession' ?></span>
                </div>
                <div id="userlisting_filter" class="dataTables_filter">
                   <label><i class="demo-icon icon-sub-lense"></i></label>
                   <input id="user_search" type="search" class="form-control input-sm" placeholder="Search" aria-controls="userlisting">
                </div>
             </div>
              <div class="table-responsive wordBreak" id="ajaxContent">
                <table class="table stylish-tableNew">
                    <tbody>
                      <?php
                      if(isset($data['tags']) && count($data['tags']) > 0){
                        foreach ($data['tags'] as $key => $list) {
                      ?>
                      <tr data-target="<?php echo "#".$key; ?>">
                        <td style="width:30%;">
                          <span onclick="showData('<?php echo $key; ?>','<?php echo $data['id']; ?>');" class="tag_title"><?php echo $key; ?></span>
                          <button data-toggle="modal" data-target="#addTag" data-key="<?php echo $key; ?>" class="btn add_new_role btn-outline-primary" type="button" name="button">+ Add New</button>
                        </td>
                        <td onclick="showData('<?php echo $key; ?>','<?php echo $data['id']; ?>');">
                          <span class="openSpan"><i class="demo-icon icon-angle-right"></i></span>
                        </td>
                      </tr>
                      <tr id="<?php echo $key; ?>" class="collapse active transition_div">
                        <td colspan="8">
                          <div style="padding-left: 0;padding-right:0px;" class="col-12 panel-body">
                              <div class="table-responsive wordBreak panel-body">
                                  <table class="table stylish-tableNew panel-body">
                                      <tbody class="finalListContent">
                                      </tbody>
                                  </table>
                                </div>
                            </div>
                        </td>
                      </tr>
                    <?php }}else{ ?>
                    <tr>
                       <td colspan="8" align="center"><font color="red">No Record(s) Found.</font></td>
                    </tr>
                  <?php } ?>
                    </tbody>
                </table>
              </div>
           </div>
        </div>
     </div>
  </div>
  <!---Add Role Tag Start[Start]-->
  <div class="modal fade" id="addTag" data-profId="<?php echo $data['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2">
    <div class="loader" id="addTagLoader"></div>
    <div class="modal-dialog" role="document">
      <div class="modal-content uploadFileBlock" id="bulkUploadBase">
        <div class="modal-header">
          <!-- <h4 class="modal-title" id="exampleModalLabel2">Add Base Location</h4> -->
          <h4 class="modal-title" id="addTagLabel">Create</h4>
          <p>You can add tag by providing information below:</p>
        </div>
        <div class="modal-body">
          <form id="bulkEmailUpdate">
            <div id="addTagInput" class="form-group">
              <input type="baseLocation" class="form-control" id="tagInputTxt" name="roleTag" placeholder="Enter Name">
              <div class="form-control-feedback" id="emptyAddTag" style="display:none">Please Enter Name</div>
              <div class="form-control-feedback" id="alreadyAddTag" style="display:none"></div>
            </div>
            <div class="onCallBtMain txtCenter marBottom30">
              <button type="button" style="background-color:#00A777;color:#ffffff;" class="btn btn-onCallBt" id="addTagBTN">Add</button>
              <button type="button" class="btn btn-onCallBt" data-dismiss="modal">Cancel</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!---Add Role Tag Start[END]-->
  <!---Edit Role Tag Start[Start]-->
  <div class="modal fade" id="editTag" data-profId="<?php echo $data['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2">
    <div class="loader" id="editTagLoader"></div>
    <div class="modal-dialog" role="document">
      <div class="modal-content uploadFileBlock" id="bulkUploadBase">
        <div class="modal-header">
          <!-- <h4 class="modal-title" id="exampleModalLabel2">Add Base Location</h4> -->
          <h4 class="modal-title" id="editTagLabel">Create</h4>
          <p>You can edit tag by providing information below:</p>
        </div>
        <div class="modal-body">
          <form id="bulkEmailUpdate">
            <div id="editTagInput" class="form-group">
              <input type="baseLocation" class="form-control" id="editTagInputTxt" name="roleTag" placeholder="Enter Name">
              <div class="form-control-feedback" id="emptyEditTag" style="display:none">Please Enter Name</div>
              <div class="form-control-feedback" id="alreadyEditTag" style="display:none"></div>
            </div>
            <div class="onCallBtMain txtCenter marBottom30">
              <button type="button" style="background-color:#00A777;color:#ffffff;" class="btn btn-onCallBt" id="editTagBTN">Update</button>
              <button type="button" class="btn btn-onCallBt" data-dismiss="modal">Cancel</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!---Edit Role Tag Start[END]-->
  <!-- ============================================================== -->
  <!-- End HTML Content -->
  <!-- ============================================================== -->
</div>
<script type="text/javascript">
$("#sidebarnav>li#menu4").addClass('active').find('a').eq(0).addClass('active');
$("#sidebarnav>li#menu4").find("li").eq(5).addClass('active').find('a').addClass('active');
window.history.pushState('','','<?php echo BASE_URL.'institution/InstitutionSetting/professionDetail/'.$data['id']; ?>');
$("#professionDetailLoader").fadeOut("fast");
var callOnce = true;
<?php if(isset($_REQUEST['action'])){ ?>
  if(callOnce == true){
    showData('<?php echo $_REQUEST['action']; ?>','<?php echo $data['id']; ?>');
    callOnce = false;
  }
<?php } ?>
//setup before functions
var typingTimer;                //timer identifier
var doneTypingInterval = 1000;  //time in ms, 5 second for example
var $input = $('#user_search');

//on keyup, start the countdown
$input.on('keyup', function () {
  clearTimeout(typingTimer);
  typingTimer = setTimeout(searchTag, doneTypingInterval);
});
//on keydown, clear the countdown
$input.on('keydown', function () {
  clearTimeout(typingTimer);
});

$(function () {
  $('#addTag').on('show.bs.modal', function (event) {
    $("#addTagLoader").fadeOut("fast");
    var element = $('#addTag');
    var button = $(event.relatedTarget);
    var key = button.attr('data-key');

    element.find('#addTagLabel').html('Add New '+key);
    element.find('#emptyAddTag').html('Please Enter '+key);
    element.find('#tagInputTxt').val('').attr('placeholder','Enter '+key);
    element.attr('data-key',key);
    $("#emptyAddTag").hide();
    $("#alreadyAddTag").hide();
    $("#addTagInput").removeClass('has-danger');
  });
  $('#editTag').on('show.bs.modal', function (event) {
    $("#editTagLoader").fadeOut("fast");
    var element = $('#editTag');
    var button = $(event.relatedTarget);
    var key = button.attr('data-key');
    var value = button.attr('data-value');
    var id = button.attr('data-id');

    element.find('#editTagLabel').html('Edit '+key);
    element.find('#emptyEditTag').html('Please Enter '+key);
    element.find('#editTagInputTxt').val(value).attr('placeholder','Enter '+key);
    element.attr('data-key',key);
    element.attr('data-tagId',id);
    $("#emptyEditTag").hide();
    $("#alreadyEditTag").hide();
    $("#editTagInput").removeClass('has-danger');
  });
});

function getTagData(id){
  $('#professionDetailLoader').fadeIn('fast');
  $.ajax({
      url:'<?php echo BASE_URL; ?>institution/InstitutionSetting/getTagData',
      type: "POST",
      data: {'id':id},
      success: function(data) {
        $("#professionDetailLoader").fadeOut("fast");
        data = JSON.parse(data);
        // $.each(data.tags, function( key, value ) {
        //   $("#"+key+"_title").html(key+' ('+value+')');
        // });
      }
  });
}

function searchTag(){
  $("#professionDetailLoader").fadeIn("fast");
  var $search_text = $input.val().trim();
  var elem = '';
  $('.emptyTagList').addClass('is-hidden');
  if($('#ajaxContent tr').attr('aria-expanded')=='true'){
    elem = $('tr.transition_div .tagName');
    elem.each(function () {
      $(this).parents('tr.tagList').show();
      if($(this).html().toLowerCase().indexOf($search_text.toLowerCase()) == -1){
          $(this).parents('tr.tagList').hide();
      }
    });
    if($('tr.transition_div tr.tagList:visible').length == 0){
      $('.emptyTagList').removeClass('is-hidden');
    }
  }else{
    elem = $('tr .tag_title');
    elem.each(function () {
      $(this).parents('tr').show();
      if($(this).html().toLowerCase().indexOf($search_text.toLowerCase()) == -1){
          $(this).parents('tr').hide();
      }
    });
  }
  // $('.transition_div').css('height',$('.transition_div').find('.finalListContent').css('height'));
  $("#professionDetailLoader").fadeOut("fast");
}

$('body').on('click','#addTagBTN',function(){
  var name = $('#tagInputTxt').val().trim();
  var key = $('#addTag').attr('data-key');
  var profId = $('#addTag').attr('data-profid');

  if(name == ''){
    $("#emptyAddTag").show();
    $("#addTagInput").addClass('has-danger');
  }else{
    addRoleTag(name,key,profId);
  }
});

$('body').on('click','#editTagBTN',function(){
  var name = $('#editTagInputTxt').val().trim();
  var key = $('#editTag').attr('data-key');
  var profId = $('#editTag').attr('data-profid');
  var tagId = $('#editTag').attr('data-tagid');

  if(name == ''){
    $("#emptyEditTag").show();
    $("#editTagInput").addClass('has-danger');
  }else{
    updateRoleTag(name,key,tagId);
  }
});

function showData(tag,profId){
  if($('#ajaxContent tr[data-target="#'+tag+'"]').attr('aria-expanded')=='true'){
    $('#ajaxContent tr').attr('aria-expanded','false');
    $('#ajaxContent tr.transition_div').removeClass('show').attr('aria-expanded','false');
  }else{
    $('#ajaxContent tr').attr('aria-expanded','false');
    $('#ajaxContent tr.transition_div').removeClass('show').attr('aria-expanded','false');
    $('#ajaxContent tr[data-target="#'+tag+'"]').attr('aria-expanded','true');
    $("#"+tag).addClass('show').attr('aria-expanded','true');
    $("#"+tag).find('.finalListContent').html('<tr><td colspan="8" align="center"><font color="green">Please wait while we are loading the list...</font></td></tr>');
    $("#"+tag).css('height','50px');
    getTagList(tag,profId,function(returnVal){
      $("#"+tag).find('.finalListContent').html(returnVal);
      var height = $("#"+tag).find('.finalListContent').css('height');
      if(height > 200){
        height = 200;
      }
      $("#"+tag).css('height',height+' px');
    });
  }
}

function getTagList(tag,profId,callback){
  $('#professionDetailLoader').fadeIn('fast');
  $.ajax({
      url:'<?php echo BASE_URL; ?>institution/InstitutionSetting/getTagDataList',
      type: "POST",
      data: {'key':tag,'prof_id':profId},
      success: function(data) {
        $('#professionDetailLoader').fadeOut('fast');
          data = JSON.parse(data);
          console.log(data);
          var list = '';
          var hidenClass = 'is-hidden';
          if(data.length > 0){
            data.forEach(function(element){
              console.log(element);
              list += '<tr class="tagList">';
              list += '<td class="tagName" style="padding-left:20px">'+element.name+'</td>';
              list += '<td style="text-align:center;">';
              if(element.status == 1){
                list += '<button onclick="changeStatus('+element.id+',0,\''+tag+'\');" type="button" class="btn btn-primary"> Active</button>';
              }else{
                list += '<button onclick="changeStatus('+element.id+',1,\''+tag+'\');" type="button" class="btn btn-outline-primary">Inactive</button>';
              }
              list += '</td>';
              list += '<td style="text-align:center;" class="editBlock_main">';
              list += '<img data-toggle="modal" data-id="'+element.id+'" data-target="#editTag" data-key="'+tag+'" data-value="'+element.name+'" style="width: 15px;" src="<?php echo BASE_URL ?>institution/img/edit.svg" alt="">';
              list += '</td>';
              list += '</tr>';
            });
          }else{
            hidenClass = '';
          }
          list += '<tr class="emptyTagList '+hidenClass+'">';
          list += '<td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>';
          list += '</tr>';
          // $("#"+tag+"_title").html(tag+' ('+data.length+')');
          return callback(list);
      }
  });
}

function addRoleTag(name, key, profId) {
  $("#addTagLoader").fadeIn("fast");
  $.ajax({
      url:'<?php echo BASE_URL; ?>institution/InstitutionSetting/addRoleTag',
      type: "POST",
      data: {'key':key,'name':name.trim(), 'profession_id': profId},
      success: function(data) {
        $("#addTagLoader").fadeOut("fast");
        console.log(data);
        data = JSON.parse(data);
        $('#addTag').modal('hide');
        if(data.status == 1){
          var buttonHtml = '<button type="button" onClick="reload(\''+key+'\');" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
          $('#customModalLabel').html('Confirmation');
          $('#customModalBody').html(data['message']);
          $('#customModalFoot').html(buttonHtml);
          $('#customBox').modal('show');
          $("#customBox .loaderImg").hide();
          $(".loading_overlay").hide();
        }else{
          var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
          $('#customModalLabel').html('Error');
          $('#customModalBody').html('An error has occurred. Please try again');
          $('#customModalFoot').html(buttonHtml);
          $('#customBox').modal('show');
          $("#customBox .loaderImg").hide();

          $(".loading_overlay").hide();
        }
      },
      error: function( result ){
        $("#addTagLoader").fadeOut("fast");
        var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
        $('#customModalLabel').html('Error');
        $('#customModalBody').html('An error has occurred. Please try again');
        $('#customModalFoot').html(buttonHtml);
        $('#customBox').modal('show');
        $("#customBox .loaderImg").hide();
        $(".loading_overlay").hide();
      }
  });
}
function updateRoleTag(name, key, id) {
  $("#editTagLoader").fadeIn("fast");
  $.ajax({
      url:'<?php echo BASE_URL; ?>institution/InstitutionSetting/updateRoleTag',
      type: "POST",
      data: {'key':key,'name':name.trim(), 'tag_id': id},
      success: function(data) {
        $("#editTagLoader").fadeOut("fast");
        console.log(data);
        data = JSON.parse(data);
        $('#editTag').modal('hide');
        if(data.status == 1){
          var buttonHtml = '<button type="button" onClick="reload(\''+key+'\');" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
          $('#customModalLabel').html('Confirmation');
          $('#customModalBody').html(data['message']);
          $('#customModalFoot').html(buttonHtml);
          $('#customBox').modal('show');
          $("#customBox .loaderImg").hide();
          $(".loading_overlay").hide();
        }else{
          var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
          $('#customModalLabel').html('Error');
          $('#customModalBody').html(data['message']);
          $('#customModalFoot').html(buttonHtml);
          $('#customBox').modal('show');
          $("#customBox .loaderImg").hide();

          $(".loading_overlay").hide();
        }
      },
      error: function( result ){
        $('#editTag').modal('hide');
        $("#editTagLoader").fadeOut("fast");
        var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
        $('#customModalLabel').html('Error');
        $('#customModalBody').html('An error has occurred. Please try again');
        $('#customModalFoot').html(buttonHtml);
        $('#customBox').modal('show');
        $("#customBox .loaderImg").hide();
        $(".loading_overlay").hide();
      }
  });
}

function changeStatus(id, status, tag){
  $("#professionDetailLoader").fadeIn("fast");
  $.ajax({
      url:'<?php echo BASE_URL; ?>institution/InstitutionSetting/changeTagStatus',
      type: "POST",
      data: {'tag_id':id, 'status':status, 'tag_key':tag},
      success: function(data) {
        $("#professionDetailLoader").fadeOut("fast");
        data = JSON.parse(data);
        if(data.status == 1){
          var buttonHtml = '<button type="button" onClick="reload(\''+tag+'\');" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
          $('#customModalLabel').html('Confirmation');
          $('#customModalBody').html(data['message']);
          $('#customModalFoot').html(buttonHtml);
          $('#customBox').modal('show');
          $("#customBox .loaderImg").hide();
          $(".loading_overlay").hide();
        }else{
          var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
          $('#customModalLabel').html('Error');
          $('#customModalBody').html(data['message']);
          $('#customModalFoot').html(buttonHtml);
          $('#customBox').modal('show');
          $("#customBox .loaderImg").hide();

          $(".loading_overlay").hide();
        }
      }
  });
}

function reload(key) {
  var profId = $('#addTag').data('profid');
  if($('#ajaxContent tr[data-target="#'+key+'"]').attr('aria-expanded')=='true'){
    getTagList(key,profId,function(returnVal){
      $("#"+key).find('.finalListContent').html(returnVal);
      // $("#"+key).css('height',$("#"+key).find('.finalListContent').css('height'));
    });
  }else{
    getTagData(profId);
  }
}

$('body').on('click','.text-themecolor.window_history',function(){
  window.history.back();
});
</script>
