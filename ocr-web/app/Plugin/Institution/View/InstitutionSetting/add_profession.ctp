<?php
$searchdata['status'] = 0;
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
        'update' => '#ajaxContent',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
?>
<div id="profLoader" class="loader"></div>
<style type="text/css">
#profLoader {
    position: fixed;
    left:0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('../page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
}
.modal-backdrop.fade {
	display: none;
}
.modal-open .modal-backdrop.fade {
	display: block;
}
</style>
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
      <div class="col-md-6 col-6 align-self-center">
          <h3 class="text-themecolor m-b-0 m-t-0">Settings: <span style="color:#10a5da;">Profession</span></h3>
      </div>
    </div>
    <span class="is-hidden" id="number_val"></span>
    <div class="row breadcrumb-title">
      <ol class="breadcrumb">
      </ol>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->

    <div class="row boaderLines">
      <div class="col-12" id="addProfBlock">
        <form id="bulkTagUpload" method="post" enctype="multipart/form-data">
        <!-- Form Steps  -->
        <div class="card prof_name">
          <div class="card-block padLeftRight0 pad0">
            <div class="floatLeftFull padLeftRight40">
              <div class="tableTop floatLeft">
                <span class="card-title">Add Professions</span>
              </div>
            </div>
            <div class="number_option floatLeftFull tableTop floatLeft padLeftRight40">
              <div id="list-filter-options1">
                <input type="text" name="profession_name" id="professionTxt" placeholder="Enter Profession" class="form-control col-6" value="">
                <input type="checkbox" name="student" value="1" id="is_student">
                <label for="is_student">Is Student</label>
                <button type="button" onclick="getCateogaries();" class="btn success_btn" name="button">Next</button>
              </div>
            </div>
          </div>
        </div>
        <!-- Form Steps  -->
        <!-- Form Steps  -->
        <div class="card cateogary_card is-hidden">
          <div class="card-block padLeftRight0 pad0">
            <div class="floatLeftFull padLeftRight40">
              <div class="tableTop floatLeft">
                <span class="card-title">Select Tags for Permanent Role</span> OR
                <a onclick="skip();" class="btn skip_btn" name="button">Skip..</a>
              </div>
            </div>
            <div class="number_option floatLeftFull tableTop floatLeft padLeftRight40">
              <div id="list-filter-options">
              </div>
            </div>
          </div>
        </div>
        <!-- Form Steps  -->
        <!-- Form Steps  -->
        <div class="card tag_form_card is-hidden">
          <div class="card-block padLeftRight0 pad0">
            <div class="floatLeftFull padLeftRight40">
              <div class="tableTop floatLeft">
                <span class="card-title">Add Tag Values.</span>
              </div>
            </div>
            <div class="number_option floatLeftFull tableTop floatLeft padLeftRight40">
                <div id="tag_form_list">
                </div>
            </div>
          </div>
        </div>
        <!-- Form Steps  -->
        <!-- Form Steps  -->
        <div class="card final_step is-hidden">
          <button class="btn" id="finalSave" type="submit" name="button">Save</button>
        </div>
        <!-- Form Steps  -->
        </form>
      </div>
    </div>
    <!-- Row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->

</div>
<script type="text/javascript">
  $("#sidebarnav>li#menu4").addClass('active').find('a').eq(0).addClass('active');
  $("#sidebarnav>li#menu4").find("li").eq(5).addClass('active').find('a').addClass('active');
  $('#profLoader').fadeOut('fast');
  $('body').on('click','#finalSave',function(){
    $('#form_submit').click();
  });
  $('body').on('click','.add_multi.single_sec',function(){
    var elem = $(this).parents('.row.tag_row').addClass('multi_part');
    $(this).parents('.row.tag_row').find('input').val('');
  });
  $('body').on('click','.cancel_multi.multi_sec',function(){
    var elem = $(this).parents('.row.tag_row').removeClass('multi_part');
    $(".dropify-clear").trigger("click");
  });
  $('body').on('keyup','#professionTxt',function(){
    var value = $('#professionTxt').val().trim();
    console.log(value);
    if(value != ''){
      $('.card.prof_name').addClass('active');
    }else{
      $('.card.prof_name').removeClass('active');
    }
  });

  function skip(){
    $('input[name="tag_cateogary[]"]').prop('checked', false);
    $('.cateogary_card').removeClass('active').addClass('done');
    $('.final_step').removeClass('is-hidden');
  }
  function getCateogaries(){
    $('#profLoader').fadeIn('fast');
    var value = $('#professionTxt').val().trim();
    var is_student = $('#is_student').prop('checked');

    $.ajax({
       url:'<?php echo BASE_URL; ?>institution/InstitutionProfession/getCateogaries',
       type: "POST",
       data: {'profession_name':value},
       success: function(data) {
         $('#profLoader').fadeOut('fast');
         console.log(data);
         data = JSON.parse(data);
         if(data.status == 1){
           if(is_student == true){
             $('.card').eq(0).removeClass('active').addClass('done');
             $('.final_step').removeClass('is-hidden');
           }else{
             cateogary = data.data;
             var list = '';
             cateogary.forEach(function(element){
               list += '<input type="checkbox" name="tag_cateogary[]" value="'+element.name+'" id="tag_cateogary_'+element.id+'">';
               list += '<label for="tag_cateogary_'+element.id+'">'+element.name+'</label>';
             });
             list += '<button type="button" onclick="tagManagment();" class="btn success_btn" name="button">Next</button>';
             $('#list-filter-options').html(list);
             $('.card').eq(0).removeClass('active').addClass('done');
             $('.cateogary_card').removeClass('is-hidden');
           }
         }else{
           var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
           $('#customModalLabel').html('Error');
           $('#customModalBody').html(data['message']);
           $('#customModalFoot').html(buttonHtml);
           $('#customBox').modal('show');
           $("#customBox .loaderImg").hide();
         }
       }
    });
  }

  $('body').on('change','input[name="tag_cateogary[]"]',function(){
    var tags = $('input[name="tag_cateogary[]"]:checked').length;
    if(tags > 0){
      $('.card.cateogary_card').addClass('active');
    }else{
      $('.card.cateogary_card').removeClass('active');
    }
  });

  function tagManagment(){
    var element = $('input[name="tag_cateogary[]"]:checked');
    var list = '';
    var params = {};
    element.each(function () {
      params = {};
      params.name = $(this).next("label").text().trim();
      params.id = $(this).val();
      list += getTagFormHtml(params);
    });

    $('.card.cateogary_card').removeClass('active').addClass('done');
    $('.tag_form_card').removeClass('is-hidden');
    $('.final_step').removeClass('is-hidden');
    $('#tag_form_list').html(list);
    $('.dropify').dropify();
  }

  $("form#bulkTagUpload").submit(function(){
      var formData = new FormData(this);
      var datastring = $("#bulkTagUpload").serialize();
      $('#profLoader').fadeIn('fast');
      var name = $('#professionTxt').val().trim();
      console.log(datastring);
     $.ajax({
         url: '<?php echo BASE_URL; ?>institution/InstitutionProfession/addTagFileUpload',
         type: 'POST',
         data: formData,
         success: function (data) {
             console.log(data);
             $('#profLoader').fadeOut('fast');
             var responseArray = JSON.parse(data);
             if( responseArray['status'] == 1 ){
               window.location.href = '<?php echo BASE_URL ?>institution/InstitutionSetting/profession';
             }
         },
         cache: false,
         contentType: false,
         processData: false
     });

      return false;
  });

  function getTagFormHtml(params){
    var html = '';
    html += '<div class="row tag_row">';
    html += '<span class="card-title col-12">Add '+params.name+'</span>';
    html += '<input type="text" class="form-control col-6 single_sec" placeholder="Enter '+params.name+'" name="'+params.name+'" value="">';
    html += '<span class="divider single_sec"></span>';
    html += '<a class="add_multi single_sec">+ Add Multiple</a>';
    html += '<a class="add_multi_txt multi_sec">+ Add Multiple</a>';
    html += '<span class="divider_txt multi_sec"></span>';
    html += '<span class="cancel_multi multi_sec">x</span>';
    html += '<div class="multi_sec col-12">';
    html += '<input type="file" name="'+params.name+'" class="dropify" accept=".csv, .xls, .xlsx, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />';
    html += '</div>';
    html += '</div>';
    return html;
  }
</script>
