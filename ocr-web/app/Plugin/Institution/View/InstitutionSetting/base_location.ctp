<?php
$searchdata['status'] = 0;
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
        'update' => '#ajaxContent',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
?>
<div id="ipLoader" class="loader"></div>
<style type="text/css">
#baseLocationLoader,#updateLoader,#adduserLoader {
    position: absolute;
    left:0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('../page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
}
.modal-backdrop.fade {
	display: none;
}
.modal-open .modal-backdrop.fade {
	display: block;
}
</style>
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
        <div class="row page-titles">
          <div class="col-md-6 col-6 align-self-center">
              <h3 class="text-themecolor m-b-0 m-t-0">Settings: <span style="color:#10a5da;">Base Location</span></h3>
          </div>
        </div>
        <div class="row breadcrumb-title">
          <ol class="breadcrumb">
              <!-- <li class="breadcrumb-item"><a href="<?php //echo BASE_URL ?>institution/InstitutionHome/index"><i class="demo-icon icon-sub-home"></i></a></li>
              <li class="breadcrumb-item active">Screen Shot Transactions</li> -->
          </ol>
        </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->


    <!-- ============================================================== -->
<!-- Start HTML Content -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- End HTML Content -->
<!-- ============================================================== -->


    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->

    <div class="row boaderLines">
        <div class="col-12">
          <div id="baseLocation" style="position:relative;">
            <div class="loader" id="baseLocationLoader"></div>
            <div class="card">
               <div class="card-block padLeftRight0 pad0">
                  <div class="floatLeftFull padLeftRight14 newListStyle">
                     <div class="tableTop floatLeft">
                       <div id="list-filter-options">
                         <input type="radio" name="filter_option" value="all" id="filter_option_all" checked="checked">
                         <label for="filter_option_all">All (<span id="showTotCount">0</span>)</label>
                         <input type="radio" name="filter_option" value="1" id="filter_option_await">
                         <label for="filter_option_await">Active (<span id="showActiveCount">0</span>)</label>
                         <input type="radio" name="filter_option" value="0" id="filter_option_approved">
                         <label for="filter_option_approved">Inactive (<span id="showInactiveCount">0</span>)</label>
                         <button style="" class="btn add_new_base" data-toggle="modal" data-target="#exampleModal">+ Add New</button>
                       </div>
                     </div>
                     <div id="userlisting_filter" class="dataTables_filter">
                        <label><i class="demo-icon icon-sub-lense"></i></label>
                        <input id="user_search" type="search" class="form-control input-sm" placeholder="Search" aria-controls="userlisting">
                     </div>
                  </div>
                  <div class="table-responsive wordBreak" id="baseLocationContent">
                    <table class="table stylish-table customPading lineHeight36">
                       <thead>
                           <tr>
                               <th style="width:60%;">Base Location </th>
                               <th style="width:30%;">Status</th>
                               <th style="width:10%;">Actions</th>
                           </tr>
                       </thead>
                       <tbody>
                         <tr>
                             <td colspan="8" align="center">
                                 <font color="green">Please wait while we are loading the list...</font>
                             </td>
                         </tr>
                       </tbody>
                    </table>
                  </div>
               </div>
            </div>
          </div>
        </div>
    </div>
    <!-- Row -->
    <!---Add Base Location Start[Start]-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
      <div class="loader" id="adduserLoader"></div>
      <div class="modal-dialog" role="document">
        <div class="modal-content uploadFileBlock" id="bulkUploadUser">
          <div class="modal-header">
            <h4 class="modal-title" id="exampleModalLabel1">Add Base Location</h4>
            <p>You can add base location by providing information below:</p>
          </div>
          <div class="modal-body">
            <form id="bulkEmailUpload">
              <div id="inputBaseLocation" class="form-group">
                <input type="baseLocation" class="form-control" id="baseLocationTxt" name="baseLocation" placeholder="Enter Base Location">
                <div class="form-control-feedback" id="emptyBaseLocation" style="display:none">Please Enter Base Location</div>
                <div class="form-control-feedback" id="alreadyBaseLocation" style="display:none"></div>
              </div>
              <div class="onCallBtMain txtCenter marBottom30">
                <button type="button" style="background-color:#00A777;color:#ffffff;" class="btn btn-onCallBt" onclick="addBaseLocation();" id="addUserbtn1">Add Base Location</button>
                <button type="button" class="btn btn-onCallBt" data-dismiss="modal">Cancel</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!---Add Base Location Start[END]-->
    <!---Update Base Location Start[Start]-->
    <div class="modal fade" id="updateBase" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2">
      <div class="loader" id="updateLoader"></div>
      <div class="modal-dialog" role="document">
        <div class="modal-content uploadFileBlock" id="bulkUploadBase">
          <div class="modal-header">
            <!-- <h4 class="modal-title" id="exampleModalLabel2">Add Base Location</h4> -->
            <h4 class="modal-title" id="exampleModalLabel2">Update Base Location</h4>
            <p>You can update base location by providing information below:</p>
          </div>
          <div class="modal-body">
            <form id="bulkEmailUpdate">
              <div id="inputUpdateBaseLocation" class="form-group">
                <input type="baseLocation" class="form-control" id="updateBaseLocationTxt" name="baseLocation" placeholder="Enter Base Location">
                <div class="form-control-feedback" id="emptyUpdateBaseLocation" style="display:none">Please Enter Base Location</div>
                <div class="form-control-feedback" id="alreadyUpdateBaseLocation" style="display:none"></div>
              </div>
              <div class="onCallBtMain txtCenter marBottom30">
                <button type="button" style="background-color:#00A777;color:#ffffff;" class="btn btn-onCallBt" onclick="updateBaseLocation('update');" id="updateBasebtn">Update Base Location</button>
                <button type="button" class="btn btn-onCallBt" data-dismiss="modal">Cancel</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!---Update Base Location Start[END]-->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->

</div>
<script type="text/javascript">
$("#sidebarnav>li#menu4").addClass('active').find('a').eq(0).addClass('active');
$("#sidebarnav>li#menu4").find("li").eq(4).addClass('active').find('a').addClass('active');
//setup before functions
var typingTimer;                //timer identifier
var doneTypingInterval = 1000;  //time in ms, 5 second for example
var $input = $('#user_search');

//on keyup, start the countdown
$input.on('keyup', function () {
  clearTimeout(typingTimer);
  typingTimer = setTimeout(searchUser, doneTypingInterval);
});

//on keydown, clear the countdown
$input.on('keydown', function () {
  clearTimeout(typingTimer);
});

$(document).ready(function() {
  $('input[name="filter_option"]').attr('checked',false);
  $('input[name="filter_option"][value="all"]').attr('checked',true);
  getBaseLocation('1','20',$input.val().trim());
});

function getBaseLocation(page,limit,$search_text){
  $("#baseLocationLoader").fadeIn("fast");
  $type = $('input[name="filter_option"]:checked').val();
  getDashCount($search_text);
  $.ajax({
    url:'<?php echo BASE_URL; ?>institution/InstitutionSetting/getBaseLocation',
    type: "POST",
    data: {'page_number':page,'limit':limit,'search_text':$search_text,'type':$type},
    success: function(data) {
      $("#baseLocationLoader").fadeOut("fast");
      $('#baseLocationContent').html(data);
      // $('#showTotCount').html($('#totalCount').html());
    }
  });
}
//user is "finished typing," do something
function searchUser() {
   $type = $('input[name="filter_option"]:checked').val();
   $search_text = $input.val().trim();
   if($search_text == ''){
       // return false;
   }
   $input.blur();
   $("#baseLocationLoader").fadeIn("fast");
   getDashCount($search_text);
   $.ajax({
       url:'<?php echo BASE_URL; ?>institution/InstitutionSetting/getBaseLocation',
       type: "POST",
       data: {'type':$type, 'search_text':$search_text},
       success: function(data) {
           $('#baseLocationContent').html(data);
           $("#baseLocationLoader").fadeOut("fast");
       }
   });
}
function getDashCount($search_text){
  $.ajax({
    url:'<?php echo BASE_URL; ?>institution/InstitutionSetting/getDashboardCounts',
    type: "POST",
    data: {'search_text':$search_text},
    success: function(data) {
      data = JSON.parse(data);
      $('#showTotCount').html(parseInt(data.all));
      $('#showActiveCount').html(parseInt(data.active));
      $('#showInactiveCount').html(parseInt(data.inactive));
    }
  });
}
$('body').on('click','input[name="filter_option"]',function(){
  $('#type_val').html($(this).val());
  getBaseLocation('1','20',$input.val().trim());
});
function chngPage(val,limit) {
   var a=$('tr th.active').attr('id');
   var b=$('tr th.ASC').attr('id');
   if(a==b){
       var order='A';
   }
   else{
       var order='DE';
   }
   $type = $('input[name="filter_option"]:checked').val();
   $search_text = $input.val().trim();
   var goVal=$("#chngPage").val();
   if(parseInt(goVal) <= Math.ceil(val) && Math.ceil(goVal)>0){
       $("#baseLocationLoader").fadeIn("fast");
       $.ajax({
           url:'<?php echo BASE_URL; ?>institution/InstitutionSetting/getBaseLocation',
           type: "POST",
           data: {'page_number':goVal,'limit':limit,'sort':a,'order':order,'search_text':$search_text,'type':$type},
           success: function(data) {
               $('#baseLocationContent').html(data);
               $('#showTotCount').html($('#totalCount').html());
               $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
               if(a==b){
                   $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
               }
               else{
                   $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
               }
               $("#baseLocationLoader").fadeOut("fast");
           }
       });
   }

}
function chngCount(val){
   $("#baseLocationLoader").fadeIn("fast");
   var a=$('tr th.active').attr('id');
   var b=$('tr th.ASC').attr('id');
   if(a==b){
       var order='A';
   }
   else{
       var order='DE';
   }
   $search_text = $input.val().trim();
   $type = $('input[name="filter_option"]:checked').val();
 $.ajax({
     url:'<?php echo BASE_URL; ?>institution/InstitutionSetting/getBaseLocation',
     type: "POST",
     data: {'limit':val,'sort':a,'order':order,'search_text':$search_text,'type':$type},
     success: function(data) {
         $('#baseLocationContent').html(data);
         $('#showTotCount').html($('#totalCount').html());
         $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
         if(a==b){
           $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
         }
         else{
           $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
         }
         $("#baseLocationLoader").fadeOut("fast");
     }
 });
}

function abc(val,limit){
   $("#baseLocationLoader").fadeIn("fast");
   var a=$('tr th.active').attr('id');
   var b=$('tr th.ASC').attr('id');
   if(a==b){
       var order='A';
   }
   else{
       var order='DE';
   }
   $search_text = $input.val().trim();
   $type = $('input[name="filter_option"]:checked').val();
 $.ajax({
      url:'<?php echo BASE_URL; ?>institution/InstitutionSetting/getBaseLocation',
      type: "POST",
      data: {'page_number':val,'limit':limit,'sort':a,'order':order, 'search_text':$search_text,'type':$type},
      success: function(data) {
          $('#baseLocationContent').html(data);
          $('#showTotCount').html($('#totalCount').html());
          $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
          if(a==b){
            $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
          }
          else{
            $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
          }
          $("#baseLocationLoader").fadeOut("fast");
      }
   });
}

function reload(){
   $('#customModalBody').removeClass('bodyCustom');
   $('#customModalFoot').css('text-align','center');
   var page = parseInt($('#sample_1_paginate ul.pagination li.active.num a #chngPage').val());
   var limit = $('#Data_Count').val();
   var $search_text = $input.val().trim();
   getBaseLocation(page,limit,$search_text);
}

function changeStatus(baseLocationId, status){
 $("#baseLocationLoader").fadeIn("fast");
 $.ajax({
     url: '<?php echo BASE_URL; ?>institution/InstitutionSetting/changeBaseLocationStatus',
     type: 'POST',
     data: {'baseLocationId':baseLocationId, 'status': status},
     success: function( result ){
         $("#baseLocationLoader").fadeOut("fast");
         var responseArray = JSON.parse(result);
         if( responseArray['status'] == 1 ){
             var buttonHtml = '<button type="button" onClick="reload();" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
             $('#customModalLabel').html('Confirmation');
             $('#customModalBody').html(responseArray['message']);
             $('#customModalFoot').html(buttonHtml);
             $('#customBox').modal('show');

         }else{
             var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
             $('#customModalLabel').html('Error');
             $('#customModalBody').html(responseArray['message']);
             $('#customModalFoot').html(buttonHtml);
             $('#customBox').modal('show');

             $(".loading_overlay1").hide();
         }
     },
     error: function( result ){
         $("#baseLocationLoader").fadeOut("fast");
         $("#customBox .loaderImg").hide();
          var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
          $('#customModalLabel').html('Error');
          $('#customModalBody').html('An error has occurred. Please try again.');
          $('#customModalFoot').html(buttonHtml);
          $('#customBox').modal('show');

          $(".loading_overlay1").hide();

     }
 });
}

$(function () {
  $('#exampleModal').on('show.bs.modal', function (event) {
    $('.loader').fadeOut('fast');
    $('#exampleModal input').val('');
    $("#emptyBaseLocation").hide();
    $("#alreadyBaseLocation").hide();
    $("#inputBaseLocation").removeClass('has-danger');
  });
});

$(function () {
  $('#updateBase').on('show.bs.modal', function (event) {
    $('.loader').fadeOut('fast');
    var button = $(event.relatedTarget);
    var id = button.data('id');
    var value = $('#baseLocationContent td[data-id="'+id+'"]').html();
    $('#updateBase input').val(value);
    $('#updateBase #updateBasebtn').attr('data-id',id);
    $("#emptyUpdateBaseLocation").hide();
    $("#alreadyUpdateBaseLocation").hide();
    $("#inputUpdateBaseLocation").removeClass('has-danger');
  });
});

function removeBaseLocation(id){
  var value = $('#baseLocationContent td[data-id="'+id+'"]').html();
  // var buttonHtml = '<button type="button"  onClick="updateBaseLocation(\'delete\');" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
  var buttonHtml = '<button type="button" onClick="updateBaseLocation(\'delete\');" class="btn btn-info waves-effect redBtCustom" data-id="'+id+'" id="removeReqBtn">Remove</button>';
  buttonHtml += '<button type="button" class="btn btn-info waves-effect whiteBtCustom" data-dismiss="modal">Cancel</button>';
  $('#customModalLabel').html('Confirmation');
  $('#customModalBody').html('Are you sure you want to remove <h4>"'+value+'"</h4>');
  $('#customModalFoot').html(buttonHtml);
  $('#customBox').modal('show');
  $("#customBox .loaderImg").hide();
}
function updateBaseLocation(type){
  var baseLocation = '';
  var baseLocationId = '';
  if(type == 'update'){
    baseLocation = $("#updateBaseLocationTxt").val().trim();
    baseLocationId = $("#updateBasebtn").attr('data-id');

    $("#inputUpdateBaseLocation").removeClass('has-danger');
    $("#emptyUpdateBaseLocation").hide();
    $("#alreadyUpdateBaseLocation").hide();
    if(baseLocation == '' || baseLocation == null){
      $("#inputUpdateBaseLocation").addClass('has-danger');
      $("#emptyUpdateBaseLocation").show();
      return false;
    }else{
      $('#updateLoader').fadeIn('fast');
    }
  }else if(type == 'delete'){
    baseLocationId = $("#removeReqBtn").attr('data-id');
    baseLocation = $('#baseLocationContent td[data-id="'+baseLocationId+'"]').html();
    $("#baseLocationLoader").fadeIn("fast");
  }
  $.ajax({
    url: '<?php echo BASE_URL; ?>institution/InstitutionSetting/updateBaseLocation',
    type: 'POST',
    data: { 'value': baseLocation, 'baseLocationId':baseLocationId, 'type':type },
    success: function( result ){
      $('.loader').fadeOut('fast');
      $("#baseLocationLoader").fadeOut("fast");
      var responseArray = JSON.parse(result);
      if( responseArray['status'] == 1 ){
        if(type == 'update'){
          $('#updateBase').modal('toggle');
        }
        var buttonHtml = '<button type="button" onClick="reload();" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
        $('#customModalLabel').html('Confirmation');
        $('#customModalBody').html(responseArray['message']);
        $('#customModalFoot').html(buttonHtml);
        if(type != 'delete'){
          $('#customBox').modal('show');
        }
        $("#customBox .loaderImg").hide();
      }else{
        if(type == 'update'){
          $("#inputUpdateBaseLocation").addClass('has-danger');
          $("#alreadyUpdateBaseLocation").show();
          $("#alreadyUpdateBaseLocation").html('* '+responseArray['message']+'.' );
          $("#emptyUpdateBaseLocation").hide();
        }else if(type == 'delete'){
          var buttonHtml = '<button type="button" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
          $('#customModalLabel').html('Error');
          $('#customModalBody').html(responseArray['message']);
          $('#customModalFoot').html(buttonHtml);
        }
      }
    },
    error: function( result ){
      $('.loader').fadeOut('fast');
      var responseArray = JSON.parse(result);
      $("#inputUpdateBaseLocation").addClass('has-danger');
      $("#alreadyUpdateBaseLocation").show();
      $("#alreadyUpdateBaseLocation").html('* '+responseArray['message']+'.' );
      $("#emptyUpdateBaseLocation").hide();
    }
  });
}

function addBaseLocation(){
    var baseLocation = $("#baseLocationTxt").val().trim();
    $("#inputBaseLocation").removeClass('has-danger');
    $("#emptyBaseLocation").hide();
    $("#alreadyBaseLocation").hide();
    if(baseLocation == '' || baseLocation == null){
        $("#inputBaseLocation").addClass('has-danger');
        $("#emptyBaseLocation").show();
    }else{
        $('#adduserLoader').fadeIn('fast');
      $.ajax({
        url: '<?php echo BASE_URL; ?>institution/InstitutionSetting/addBaseLocation',
        type: 'POST',
        data: { 'value': baseLocation },
        success: function( result ){
            $('.loader').fadeOut('fast');
            var responseArray = JSON.parse(result);
            if( responseArray['status'] == 1 ){
                var buttonHtml = '<button type="button"  onClick="reload();" class="btn btn-info waves-effect greyBt" data-dismiss="modal">Close</button>';
                $('#customModalLabel').html('Confirmation');
                $('#customModalBody').html('Base Location added successfully');
                $('#customModalFoot').html(buttonHtml);
                $('#customBox').modal('show');

                $("#customBox .loaderImg").hide();

                $('#exampleModal').modal('toggle');
            }else{
                $("#inputBaseLocation").addClass('has-danger');
                $("#alreadyBaseLocation").show();
                $("#alreadyBaseLocation").html('* '+responseArray['message']+'.' );
                $("#emptyBaseLocation").hide();

            }
        },
        error: function( result ){
            $('.loader').fadeOut('fast');
            var responseArray = JSON.parse(result);
                $("#inputBaseLocation").addClass('has-danger');
                $("#alreadyBaseLocation").show();
                $("#alreadyBaseLocation").html('* '+responseArray['message']+'.' );
                $("#emptyEmail").hide();
                $("#emptyBaseLocation").hide();

        }
      });
    }
  }
</script>
