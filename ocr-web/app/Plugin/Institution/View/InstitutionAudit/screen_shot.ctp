<?php
$type = "outer";
$width_per = '20%';
if(isset($user_id) && (int)$user_id > 0){
  $type = "inner";
  $width_per = '33%';
}
$searchdata['status'] = 0;
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
        'update' => '#ajaxContent',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
?>
<div class="loader"></div>
<style type="text/css">
.loader {
    position: fixed;
    left:0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('<?php echo BASE_URL; ?>institution/page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
}
.calCustom {
    font-size: 1.4em;
    top:8px;
    right:14px;
}

</style>
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
      <?php if($type == 'inner'){ ?>
        <div class="row page-titles">
          <div class="col-md-12 align-self-center col-5">
              <h3 class="text-themecolor m-b-0 m-t-0 col-1 window_history"><img src="<?php echo BASE_URL; ?>institution/img/left-arrow.svg" /></h3>
              <h3 class="text-themecolor m-b-0 m-t-0 col-11" style="float: left;">Screen Shot Transactions</h3>
          </div>
        </div>
      <?php }else{ ?>
        <div class="row page-titles">
          <div class="col-md-6 col-6 align-self-center">
              <h3 class="text-themecolor m-b-0 m-t-0">Audit Trails: <span style="color:#10a5da;">User Screenshots</span></h3>
          </div>
        </div>
      <?php } ?>
      <div class="row breadcrumb-title date_range">
        <div class="floatRight dashboardBtn">
          <button class="date-range-selection selected" data-value="0" type="button" name="button">All</button>
          <button id="dashboard-report-range" type="button" name="button"><span>Select Date Range</span> <i class="fa fa-sort-down"></i></button>
          <span class="is-hidden" id="start_date"></span>
          <span class="is-hidden" id="end_date"></span>
          <span class="is-hidden" id="type">all</span>
        </div>
      </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->


    <!-- ============================================================== -->
<!-- Start HTML Content -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- End HTML Content -->
<!-- ============================================================== -->


    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->

    <div class="row boaderLines">
        <div class="col-12">
            <div class="card">
                <div class="card-block padLeftRight0 pad0">
                  <div class="floatLeftFull padLeftRight14 groupCustom newListStyle">
                     <div class="tableTop floatLeft marTop10">
                        <span class="card-title">App Screenshots: <small>Taken by Users </small></span>
                     </div>
                     <div id="userlisting_filter" class="dataTables_filter">
                        <label><i class="demo-icon icon-sub-lense"></i></label>
                        <input id="user_search" type="search" class="form-control input-sm" placeholder="Search" aria-controls="userlisting">
                     </div>
                  </div>
                    <div class="table-responsive wordBreak" id="ajaxContent">
                        <table class="table stylish-table customPading">
                            <thead>
                                <tr>
                                    <?php if($type == 'outer'){ ?>
                                      <th style="width:<?php echo $width_per; ?>;">Name</th>
                                      <th style="width:<?php echo $width_per; ?>;">Email</th>
                                    <?php } ?>
                                    <th style="width:<?php echo $width_per; ?>;padding-left:45px;">Chat Name</th>
                                    <th style="width:<?php echo $width_per; ?>;text-align:center;">Date</th>
                                    <th style="width:<?php echo $width_per; ?>;text-align:center;">Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    if(isset($data['user_list']) && count($data['user_list']) > 0){
                                    foreach( $data['user_list'] as $user){
                                ?>
                                <tr>
                                    <?php if($type == 'outer'){ ?>
                                      <td style="width:20%;"><?php echo $user['userProfiles']['first_name'].' '.$user['userProfiles']['last_name']; ?></td>
                                      <td style="width:25%;"><?php echo $user['Users']['email']; ?></td>
                                    <?php } ?>
                                    <td style="width:50px;"><span class="round"><?php echo strtoupper(substr($user['email'],0,1)); ?></span></td>
                                    <td><?php echo $user['email']; ?></td>
                                    <td>
                                        <?php
                                            if($user['status']==2){ ?>
                                                <button type="button" class="btn btn-danger"> Removed </button>
                                      <?php }else if($user['status']==1){ ?>
                                                <button onclick="changeStatus('<?php echo addslashes($user['email']); ?>',0);" type="button" class="btn btn-primary"> Unassign </button>
                                      <?php }else{ ?>
                                                <button onclick="changeStatus('<?php echo addslashes($user['email']); ?>',1);" type="button" class="btn btn-outline-primary"> Assign </button>
                                      <?php }
                                        ?>
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="ti-settings"></i>
                                            </button>
                                            <div class="dropdown-menu pad0">
                                                <a onclick="removeUser('<?php echo $user['email']; ?>')" class="dropdown-item" href="javascript:void(0)">Remove</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <?php }}elseif(isset($tCount) && $tCount > 0){?>

                                <tr>
                                   <td colspan="8" align="center"><font color="green">Please wait while we are loading the list...</font></td>
                                </tr>
                                <?php }else{ ?>
                                <tr>
                                   <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
                                </tr>
                                <?php  } ?>
                            </tbody>
                        </table>
                        <!-- <span class="pull-right">Total Count : <?php //echo isset($tCount)? $tCount:0; ?></span> -->
                        <div class="actions marBottom10 paddingSide15 leftFloat98per">
                        <?php
                            if(isset($data['user_list']) && count($data['user_list']) > 0){
                                echo $this->element('pagination');
                            }
                        ?>
                        </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->

</div>
<script type="text/javascript">

</script>
<script type="text/javascript">
    var user_id = "<?php echo $user_id; ?>";
    var type = "<?php echo $type; ?>";

    var typingTimer;                //timer identifier
    var typingTimerRole;                //timer identifier
    var doneTypingInterval = 1000;  //time in ms, 5 second for example
    var doneTypingIntervalBaton = 1500;  //time in ms, 5 second for example
    var $input = $('#user_search');

    //on keyup, start the countdown
    $input.on('keyup', function () {
      clearTimeout(typingTimer);
      typingTimer = setTimeout(getList, doneTypingInterval);
    });

    //on keydown, clear the countdown
    $input.on('keydown', function () {
      clearTimeout(typingTimer);
    });

    if(type == 'outer'){
      $("#sidebarnav>li#menu3").addClass('active').find('a').eq(0).addClass('active');
      $("#sidebarnav>li#menu3").find("li").eq(0).addClass('active').find('a').addClass('active');
    }else{
      $("#sidebarnav>li#menu1").find("li").eq(0).addClass('active').find('a').addClass('active');
    }
    // --------- INITIAL FUNCTIONS ------------
    getList();
    // $(".loader").fadeOut("fast");

    // --------- INITIAL FUNCTIONS ------------
    function getList(){
       $(".loader").fadeIn("fast");
       $search_text = $input.val().trim();
       var start_date = $('.left .input-mini').val()+' 00:00:00';
       var end_date = $('.right .input-mini').val()+' 23:59:59';
       if($('#type').html() == 'all'){
         type = 'all';
       }else{
         type = 'range';
       }
       $.ajax({
         url:'<?php echo BASE_URL; ?>institution/InstitutionAudit/screenShotFilter',
         type: "POST",
         data: {'user_id': user_id, 'search_text':$search_text,'start_date':start_date,'end_date':end_date,'type':type},
         success: function(data) {
            console.log(data);
           $('#ajaxContent').html(data);
           $(".loader").fadeOut("fast");
         }
       });
    }
     function chngPage(val,limit) {
        var a=$('tr th.active').attr('id');
        var b=$('tr th.ASC').attr('id');
        if(a==b){
            var order='A';
        }
        else{
            var order='DE';
        }
        var goVal=$("#chngPage").val();
        $search_text = $input.val().trim();
        if(parseInt(goVal) <= Math.ceil(val) && Math.ceil(goVal)>0){
            $(".loader").fadeIn("fast");
            $.ajax({
                url:'<?php echo BASE_URL; ?>institution/InstitutionAudit/screenShotFilter',
                type: "POST",
                data: {'page_number':goVal,'limit':limit,'sort':a,'order':order, 'search_text':$search_text},
                success: function(data) {
                    $('#ajaxContent').html(data);
                    $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                    if(a==b){
                        $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                    }
                    else{
                        $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                    }
                    $(".loader").fadeOut("fast");
                }
            });
        }

     }
     function chngCount(val){
        $(".loader").fadeIn("fast");
        var a=$('tr th.active').attr('id');
        var b=$('tr th.ASC').attr('id');
        if(a==b){
            var order='A';
        }
        else{
            var order='DE';
        }
        $search_text = $input.val().trim();
      $.ajax({
          url:'<?php echo BASE_URL; ?>institution/InstitutionAudit/screenShotFilter',
          type: "POST",
          data: {'limit':val,'sort':a,'order':order, 'search_text':$search_text},
          success: function(data) {
              $('#ajaxContent').html(data);
              $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
              if(a==b){
              $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
              }
              else{
              $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
              }
              $(".loader").fadeOut("fast");
          }
      });
     }

    function abc(val,limit){
        $(".loader").fadeIn("fast");
        var a=$('tr th.active').attr('id');
        var b=$('tr th.ASC').attr('id');
        if(a==b){
            var order='A';
        }
        else{
            var order='DE';
        }
        $search_text = $input.val().trim();
      $.ajax({
           url:'<?php echo BASE_URL; ?>institution/InstitutionAudit/screenShotFilter',
           type: "POST",
           data: {'page_number':val,'limit':limit,'sort':a,'order':order, 'search_text':$search_text},
           success: function(data) {
                $('#ajaxContent').html(data);
               $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
               if(a==b){
               $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
               }
               else{
               $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
               }
               $(".loader").fadeOut("fast");
           }
        });
    }
    $('#dashboard-report-range').daterangepicker({
        "locale": {
            "format": "DD-MM-YYYY",
            "separator": " - ",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "fromLabel": "From",
            "toLabel": "To",
            "customRangeLabel": "Custom",
            "daysOfWeek": [
                "Su",
                "Mo",
                "Tu",
                "We",
                "Th",
                "Fr",
                "Sa"
            ],
            "monthNames": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            "firstDay": 1
        },
        "maxDate": new Date()
        //"startDate": "11/08/2015",
        //"endDate": "11/14/2015",
        // opens: (App.isRTL() ? 'right' : 'left'),
    }, function(start, end, label) {
      // 05/08/2019, 10:36:57
      console.log(start.format('DD/MM/YYYY, H:i:s'));
      console.log(end.format('DD/MM/YYYY, H:i:s'));
      $('#type').html('range')
      $('#dashboard-report-range span').html(start.format('DD/MM/YYYY')+' - '+end.format('DD/MM/YYYY'));
      $('.dashboardBtn button').removeClass("selected");
      $('#dashboard-report-range').addClass("selected");
      $('#start_date').html(start.format('DD/MM/YYYY'));
      $('#end_date').html(end.format('DD/MM/YYYY'));
      getList();
      // getDashboardGraph('range',$('.left .input-mini').val()+' 00:00:00',$('.right .input-mini').val()+' 23:59:59');
    });
    $('body').on('click','.date-range-selection',function(){
      $('#type').html('all');
      $('#dashboard-report-range span').html('Select Date Range');
      getList();
      $('.dashboardBtn button').removeClass("selected");
      $(this).addClass("selected");
      $('#dashboard-report-range').data('daterangepicker').setStartDate(new Date());
      $('#dashboard-report-range').data('daterangepicker').setEndDate(new Date());
    });
</script>
