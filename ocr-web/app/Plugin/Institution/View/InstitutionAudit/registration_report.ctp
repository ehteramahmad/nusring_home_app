<?php

$searchdata['status'] = 0;
//** AJAX Pagination
$this->Js->JqueryEngine->jQueryObject = 'jQuery';
// Paginator options
$this->Paginator->options(array(
    'update' => '#ajaxContent',
    'evalScripts' => true,
    'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
    'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
    )
);
?>
<div class="loader"></div>
<style type="text/css">
.loader {
    position: fixed;
    left:0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('<?php echo BASE_URL; ?>institution/page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
}
.calCustom {
    font-size: 1.4em;
    top:8px;
    right:14px;
}

</style>
<div class="container-fluid" id="registration_report">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-6 col-6 align-self-center">
          <h3 class="text-themecolor m-b-0 m-t-0">Reports: <span style="color:#10a5da;">User Registration Report</span></h3>
            <!-- <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php // echo BASE_URL ?>institution/InstitutionHome/index">Home</a></li>
                <li class="breadcrumb-item active">Available / OnCall Transactions</li>
            </ol> -->
        </div>
    </div>
    <div class="row breadcrumb-title">
      <ol class="breadcrumb">
          <!-- <li class="breadcrumb-item"><a href="<?php //echo BASE_URL ?>institution/InstitutionHome/index"><i class="demo-icon icon-sub-home"></i></a></li>
          <li class="breadcrumb-item active">User Registration Report</li> -->
      </ol>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->


    <!-- ============================================================== -->
<!-- Start HTML Content -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- End HTML Content -->
<!-- ============================================================== -->


    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->

    <!-- <div class="row boaderLines">
      <div class="col-12">
        <div class="card">
          <div class="card-block padTop30 padBottom0">
            <div class="row floatLeftFull marLeft0">
                <div class="col-12 padLeftRight0">
                    <form class="exportNewForm">
                        <div class="form-group borderBottomExport" id="dateBlock" >
                            <div class="col-12 pad0">
                              <div class="row btCust">
                                  <div class="col-md-4">
                                      <input type="radio" checked="checked" name="privilige" value="0" placeholder="">
                                      <label>All</label>
                                  </div>
                                  <div class="col-md-4">
                                      <input type="radio" name="privilige" value="1" placeholder="">
                                      <span class="form-control col-5 col-form-label">
                                          <input value="" id="datetimepicker6" type="text" autocorrect="off" autocapitalize="off" autocomplete="off">
                                      </span>
                                      <span class="form-control col-5 col-form-label marLeft20px">
                                          <input value="" id="datetimepicker7" type="text" autocorrect="off" autocapitalize="off" autocomplete="off">
                                      </span>
                                  </div>
                              </div>
                            </div>
                        </div>
                        <div class="col-12 pad0 text-center">
                            <div id="dialogListShow1" class="btn btn-primary" onclick="getList()">Proceed</div>
                        </div>
                    </form>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div> -->
    <div class="row boaderLines">
      <div class="col-12">
          <div class="card">
              <div class="card-block padLeftRight0 pad0">
                <div class="padLeftRight14 newListStyle">
                  <div class="tableTop floatLeft">
                    <span class="card-title">Total Base Location (<bold id="rep_count">0</bold>)</span>
                    <div class="assignBtnAll">
                      <button id="unassign_btn" onclick="exportAll();" class="btn"> Export All</button>
                    </div>
                  </div>
                   <div class="btCust">
                       <div class="allMainRadioBt">
                           <input class="marTop23" type="radio" checked="checked" name="privilige" value="0" placeholder="">
                           <label>All</label>
                       </div>
                       <div class="custom_date marTop23 inactive">
                         <div class="boaderLeftRight">
                             <input type="radio" name="privilige" value="1" placeholder="">
                             <div class="input-group date" data-date-format="dd-mm-yyyy">
                                 <input class="form-control" type="text" autocorrect="off" autocapitalize="off" autocomplete="off" value="" id="datetimepicker6">
                                 <span class="input-group-addon">
                                   <i class="demo-icon icon-calendar-icon"></i>
                                 </span>
                             </div>
                             <div class="input-group date" data-date-format="dd-mm-yyyy">
                                 <input class="form-control" type="text" autocorrect="off" autocapitalize="off" autocomplete="off" value="" id="datetimepicker7">
                                 <span class="input-group-addon">
                                   <i class="demo-icon icon-calendar-icon"></i>
                                 </span>
                             </div>
                         </div>
                         <div class="minTop4px">
                             <div id="dialogListShow1" class="btn btn-primary" onclick="getList()">Proceed</div>
                         </div>
                     </div>
                   </div>
                </div>
                  <div class="table-responsive wordBreak" id="ajaxContent">
                </div>
              </div>
          </div>
      </div>
    </div>
    <!-- Row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->

</div>
<script type="text/javascript">

</script>
<script type="text/javascript">
    $("#sidebarnav>li#menu9").addClass('active').find('a').addClass('active');
    // --------- INITIAL FUNCTIONS ------------
    $('#datetimepicker6').datetimepicker({
        format: 'DD-MM-YYYY',
        defaultDate: new Date(),
    });
    $('#datetimepicker7').datetimepicker({
        defaultDate: new Date(),
        format: 'DD-MM-YYYY'
    });

    $('body').on('click','input[name="privilige"]',function(){
      // $('#type_val').html($(this).val());
      // getList($input.val().trim(),$(this).val());
      if(parseInt($(this).val()) == 0){
        $('.custom_date').addClass('inactive');
        getList();
      }else{
        $('.custom_date').removeClass('inactive');
      }
    });

    $('#datetimepicker6').data("DateTimePicker").maxDate(new Date());

    $('#datetimepicker7').data("DateTimePicker").maxDate(new Date());

    $("#datetimepicker6").on("dp.change", function (e) {
      $('input[type="radio"][name="privilige"][value="1"]').prop('checked',true);
      $('input[type="radio"][name="privilige"][value="0"]').prop('checked',false);
      $('.custom_date').removeClass('inactive');
      $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
    });
    $("#datetimepicker7").on("dp.change", function (e) {
      $('input[type="radio"][name="privilige"][value="1"]').prop('checked',true);
      $('input[type="radio"][name="privilige"][value="0"]').prop('checked',false);
      $('.custom_date').removeClass('inactive');
      $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
    });
    $('#datetimepicker6').next('span.input-group-addon').click(function(event){
      event.preventDefault();
      $('#datetimepicker6').focus();
    });
    $('#datetimepicker7').next('span.input-group-addon').click(function(event){
      event.preventDefault();
      $('#datetimepicker7').focus();
    });
    getList();

    // --------- INITIAL FUNCTIONS ------------
    function getList(){
       $(".loader").fadeIn("fast");
       var type = $('input[name="privilige"]:checked').val();
       var from = $('#datetimepicker6').val();
       var to = $('#datetimepicker7').val();
       if(type == 0){
         from = to = '';
       }

       $.ajax({
         url:'<?php echo BASE_URL; ?>institution/InstitutionAudit/report_filter',
         type: "POST",
         data: {'from_date':from,'to':to},
         success: function(data) {
            console.log(data);
           $('#ajaxContent').html(data);
           $(".loader").fadeOut("fast");
           $('#rep_count').html($('#report_count').html())
         }
       });
    }
</script>
