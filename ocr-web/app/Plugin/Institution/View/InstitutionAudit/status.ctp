<?php

$searchdata['status'] = 0;
    //** AJAX Pagination
    $this->Js->JqueryEngine->jQueryObject = 'jQuery';
    // Paginator options
    $this->Paginator->options(array(
        'update' => '#ajaxContent',
        'evalScripts' => true,
        'before' => $this->Js->get('#spinner')->effect('fadeIn', array('buffer' => false)),
        'complete' => $this->Js->get('#spinner')->effect('fadeOut', array('buffer' => false))
        )
    );
?>
<div class="loader"></div>
<style type="text/css">
.loader {
    position: fixed;
    left:0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('<?php echo BASE_URL; ?>institution/page-loader.gif') 50% 50% no-repeat rgba(79,74,74,0.459);
}
.calCustom {
    font-size: 1.4em;
    top:8px;
    right:14px;
}

</style>
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-6 col-6 align-self-center">
          <h3 class="text-themecolor m-b-0 m-t-0">Audit Trails: <span style="color:#10a5da;">Available & On-Call History</span></h3>
          <span class="is-hidden" id="type_val">all</span>
            <!-- <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php // echo BASE_URL ?>institution/InstitutionHome/index">Home</a></li>
                <li class="breadcrumb-item active">Available / OnCall Transactions</li>
            </ol> -->
        </div>
    </div>
    <div class="row breadcrumb-title">
      <ol class="breadcrumb">
          <!-- <li class="breadcrumb-item"><a href="<?php //echo BASE_URL ?>institution/InstitutionHome/index"><i class="demo-icon icon-sub-home"></i></a></li>
          <li class="breadcrumb-item active">Available / OnCall Transactions</li> -->
      </ol>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
<!-- Start HTML Content -->
<!-- ============================================================== -->
<div class="row boaderLines">
   <div class="col-12">
      <div class="card">
         <div class="card-block padLeftRight0 pad0">
            <div class="floatLeftFull padLeftRight14 newListStyle">
               <div class="tableTop floatLeft">
                  <div id="list-filter-options">
                    <input type="radio" name="filter_option" value="all" id="filter_option_all" checked="checked">
                    <label for="filter_option_all">All (<span id="showTotCount">0</span>)</label>
                    <input type="radio" name="filter_option" value="1" id="filter_option_available">
                    <label for="filter_option_available">Available (<span id="showAvailableCount">0</span>)</label>
                    <input type="radio" name="filter_option" value="2" id="filter_option_not_available">
                    <label for="filter_option_not_available">Not Available (<span id="showNotAvailableCount">0</span>)</label>
                    <input type="radio" name="filter_option" value="0" id="filter_option_on_call">
                    <label for="filter_option_on_call">On Call (<span id="showOnCallCount">0</span>)</label>
                  </div>
               </div>
               <div id="userlisting_filter" class="dataTables_filter">
                  <label><i class="demo-icon icon-sub-lense"></i></label>
                  <input id="user_search" type="search" class="form-control input-sm" placeholder="Search" aria-controls="userlisting">
               </div>
            </div>
            <div class="table-responsive wordBreak" id="ajaxContent">
              <table class="table stylish-table customPading">
                  <thead>
                      <tr>
                          <th>Profile </th>
                          <th>Name </th>
                          <th>E-mail </th>
                          <th>Modified Date</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>
                      <?php
                          if(isset($data['user_list']) && count($data['user_list']) > 0){
                          foreach( $data['user_list'] as $user){
                      ?>
                      <tr>
                          <td><?php echo $user['userProfiles']['first_name'].' '.$user['userProfiles']['last_name']; ?></td>
                          <td><?php echo $user['User']['email']; ?></td>
                          <td><?php echo $user['UserDutyLog']['status'] == 1 ?'OnCall':'Not OnCall'; ?></td>
                          <td><?php echo $user['UserDutyLog']['atwork_status'] == 1 ?'Available':'Not Available'; ?></td>
                          <td><?php echo date("d-m-Y H:i", strtotime($user['UserDutyLog']['modified'])); ?></td>
                          <td>Action</td>
                      </tr>
                      <?php }}elseif(isset($tCount) && $tCount > 0){?>

                      <tr>
                         <td colspan="8" align="center"><font color="green">Please wait while we are loading the list...</font></td>
                      </tr>
                      <?php }else{ ?>
                      <tr>
                         <td colspan="8" align="center"><font color="red">No Record(s) Found!</font></td>
                      </tr>
                      <?php  } ?>
                  </tbody>
              </table>
              <span id="totalCount" class="pull-right" style="display:none;"><?php echo $tCount; ?></span>
              <div class="actions marBottom10 paddingSide15 leftFloat98per">
              <?php
                  if(isset($data['user_list']) && count($data['user_list']) > 0){
                      echo $this->element('pagination');
                  }
              ?>
              </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- ============================================================== -->
<!-- End HTML Content -->
<!-- ============================================================== -->



    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->

    <!-- Row -->
    <!-- User Profile User -->
    <div class="modal fade" id="profileUserPopUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="loader" id="profileLoader"></div>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Contact Info</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body pad0">
                    <div class="containerProfile">
                      <div class="otherPImg onCallMain imageUser">
                        <a href="javascript:void(0);">
                          <img class="img-circle userDetails-avatar" src="<?php echo BASE_URL ?>institution/img/ava-single.png" onerror="this.src='<?php echo BASE_URL ?>institution/img/ava-single.png'" alt="" />
                          <?php //echo $this->Html->image('institution/img/ava-single.png',array('class'=>'img-circle userDetails-avatar')); ?>
                          <!-- <img class="userDetails-avatar profileUserAvatar" src="images/users/2.jpg" alt="user"> -->
                        </a>
                      </div>
                      <div class="userProfile-info padCustom10-20 borderBottom leftFloatFull">
                        <h3 class="profileUserName"></h3>
                        <span class="onCallTxt otherProfText"></span>
                      </div>
                      <div class="clearFix"></div>
                      <div class="profile_detail">
                        <div class="userProfile-field userProfile-field_email borderBottom">
                          <span class="userDetails-label userDetails-labelEmail">Email:</span>
                          <span class="userDetails-labelEmail-value">
                            <span class="userDetails-field-otheruser-email">
                              <a target="_blank" href=""></a>
                            </span>
                          </span>
                        </div>
                        <div class="userProfile-field userProfile-field_phone">
                          <span class="userDetails-label userDetails-labelPhone">Phone:</span>
                          <span class="userDetails-labelPhone-value">
                            <span class="userDetails-field-otheruser-phone">
                              <span></span>
                            </span>
                          </span>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="modal-footer">
                <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-success waves-effect">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- ****************  -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->

</div>
<script type="text/javascript">

</script>
<script type="text/javascript">
    // $("#sidebarnav>li#menu5").addClass('active').find('a').addClass('active');
    // $("#sidebarnav>li#menu5").find("li").eq(1).addClass('active').find('a').addClass('active');
    $("#sidebarnav>li#menu3").addClass('active').find('a').eq(0).addClass('active');
    $("#sidebarnav>li#menu3").find("li").eq(1).addClass('active').find('a').addClass('active');

    //setup before functions
    var typingTimer;                //timer identifier
    var doneTypingInterval = 1000;  //time in ms, 5 second for example
    var $input = $('#user_search');

    //on keyup, start the countdown
    $input.on('keyup', function () {
      clearTimeout(typingTimer);
      typingTimer = setTimeout(searchUser, doneTypingInterval);
    });

    //on keydown, clear the countdown
    $input.on('keydown', function () {
      clearTimeout(typingTimer);
    });

    //user is "finished typing," do something
    function searchUser() {
       $search_text = $input.val().trim();
       if($search_text == ''){
           // return false;
       }
       $input.blur();
       $(".loader").fadeIn("fast");
       getDashCount($search_text);
       var list_type = $('input[name="filter_option"]:checked').val();
       $.ajax({
           url:'<?php echo BASE_URL; ?>institution/InstitutionAudit/statusFilter',
           type: "POST",
           data: {'search_text':$search_text,'type':list_type},
           success: function(data) {
               $('#ajaxContent').html(data);
               $('#showTotCount').html($('#totalCount').html());
               $(".loader").fadeOut("fast");
               $('#assign_btn').hide();
               $('#unassign_btn').hide();
               if($type == 0 || $type == 1){
                   $('.selectiveCheck').show();
                   if($type == 0){
                       $('#assign_btn').show();
                   }else if($type == 1){
                       $('#unassign_btn').show();
                   }
               }else{
                   $('.selectiveCheck').hide();
               }
           }
       });
    }
    // --------- INITIAL FUNCTIONS ------------
    $(document).ready(function(){
      $type = $('#type_val').html();
      $('input[name="filter_option"]').attr('checked',false);
      $('input[name="filter_option"][value="'+$type.trim()+'"]').attr('checked',true);
      var list_type = $('input[name="filter_option"]:checked').val();
      getList($input.val().trim(),list_type);
    });
    // $(".loader").fadeOut("fast");

    $('body').on('click','input[name="filter_option"]',function(){
      $('#type_val').html($(this).val());
      getList($input.val().trim(),$(this).val());
    });

    // --------- INITIAL FUNCTIONS ------------
    function getList($search_text,type){
       $(".loader").fadeIn("fast");
       getDashCount($search_text);
       $('input[name="filter_option"]').attr('checked',false);
       $('input[name="filter_option"][value="'+type+'"]').attr('checked',true);
       $.ajax({
         url:'<?php echo BASE_URL; ?>institution/InstitutionAudit/statusFilter',
         type: "POST",
         data: {'search_text':$search_text,'type':type},
         success: function(data) {
            console.log(data);
           $('#ajaxContent').html(data);
           $('#showTotCount').html($('#totalCount').html());
           $(".loader").fadeOut("fast");
         }
       });
    }

    function getDashCount($search_text){
        $.ajax({
          url:'<?php echo BASE_URL; ?>institution/InstitutionAudit/getDashboardCounts',
          type: "POST",
          data: {'search_text':$search_text},
          success: function(data) {
            console.log(data);
            data = JSON.parse(data);
            console.log(data);
            $('#showTotCount').html(data.all_users);
            $('#showNotAvailableCount').html(data.not_available_users);
            $('#showAvailableCount').html(data.available_users);
            $('#showOnCallCount').html(data.on_call_users);
          }
        });
    }

     function chngPage(val,limit) {
        var a=$('tr th.active').attr('id');
        var b=$('tr th.ASC').attr('id');
        if(a==b){
            var order='A';
        }
        else{
            var order='DE';
        }
        var goVal=$("#chngPage").val();
        var list_type = $('input[name="filter_option"]:checked').val();
        if(parseInt(goVal) <= Math.ceil(val) && Math.ceil(goVal)>0){
            $(".loader").fadeIn("fast");
            $.ajax({
                url:'<?php echo BASE_URL; ?>institution/InstitutionAudit/statusFilter',
                type: "POST",
                data: {'page_number':goVal,'limit':limit,'sort':a,'order':order,'type':list_type},
                success: function(data) {
                    $('#ajaxContent').html(data);
                    $('#showTotCount').html($('#totalCount').html());
                    $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
                    if(a==b){
                        $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
                    }
                    else{
                        $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
                    }
                    $(".loader").fadeOut("fast");
                }
            });
        }

     }
     function chngCount(val){
        $(".loader").fadeIn("fast");
        var a=$('tr th.active').attr('id');
        var b=$('tr th.ASC').attr('id');
        if(a==b){
            var order='A';
        }
        else{
            var order='DE';
        }
        var list_type = $('input[name="filter_option"]:checked').val();
      $.ajax({
          url:'<?php echo BASE_URL; ?>institution/InstitutionAudit/statusFilter',
          type: "POST",
          data: {'limit':val,'sort':a,'order':order,'type':list_type},
          success: function(data) {
              $('#ajaxContent').html(data);
              $('#showTotCount').html($('#totalCount').html());
              $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
              if(a==b){
              $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
              }
              else{
              $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
              }
              $(".loader").fadeOut("fast");
          }
      });
     }

    function abc(val,limit){
        $(".loader").fadeIn("fast");
        var a=$('tr th.active').attr('id');
        var b=$('tr th.ASC').attr('id');
        if(a==b){
            var order='A';
        }
        else{
            var order='DE';
        }
        var list_type = $('input[name="filter_option"]:checked').val();
      $.ajax({
           url:'<?php echo BASE_URL; ?>institution/InstitutionAudit/statusFilter',
           type: "POST",
           data: {'page_number':val,'limit':limit,'sort':a,'order':order,'type':list_type},
           success: function(data) {
                $('#ajaxContent').html(data);
                $('#showTotCount').html($('#totalCount').html());
               $("tr th").removeClass("active").removeClass("ASC").addClass("DESC");
               if(a==b){
               $('#'+a).removeClass("DESC").addClass("active").addClass("ASC");
               }
               else{
               $('#'+a).removeClass("ASC").addClass("active").addClass("DESC");
               }
               $(".loader").fadeOut("fast");
           }
        });
    }

    function getTransaction($user_id){
      // --- Active menu in Available and Oncall Transaction [START]
      var date = new Date();
      date.setTime(date.getTime()+(10 * 60 * 1000));
      var expires = "; expires="+date.toGMTString();
      document.cookie = "av/on=audit"+expires+"; path=/";
      // --- Active menu in Available and Oncall Transaction [END]
      window.location.href =  "<?php echo BASE_URL; ?>institution/InstitutionAudit/getTransaction/" + $user_id;
    }
    $(function () {
      $('#profileUserPopUp').on('show.bs.modal', function (event) {
        $(".modal-content").addClass('loaderBg');
        $('#profileLoader').fadeIn('fast');

        $('.imageUser img').attr('src','<?php echo BASE_URL; ?>institution/img/ava-single.png');
        $('.userProfile-info .profileUserName').html('');
        $('.userDetails-field-otheruser-email a').attr('href','mailto:'+'').html('');
        $('.userDetails-field-otheruser-phone').html('');
        $('.imageUser').removeClass('onCallMain').removeClass('notAtWork');
        $('.onCallTxt.otherProfText').html('');

        var button = $(event.relatedTarget);
        var userid = button.data('user');

        $.ajax({
            url:'<?php echo BASE_URL; ?>institution/InstitutionHome/getUserProfile',
            type: 'POST',
            data: {'user_id': userid},
            success: function( result ){
                $('#profileLoader').fadeOut('fast');
                var responseArray = JSON.parse(result);
                if( responseArray['status'] == 1 ){

                    var id = responseArray['data']['id'];
                    var email = responseArray['data']['email'];
                    var phone = responseArray['data']['phone'];
                    var UserName = responseArray['data']['UserName'];
                    var profile_img = responseArray['data']['profile_img'] !=""?responseArray['data']['profile_img']: '<?php echo BASE_URL; ?>institution/img/ava-single.png';
                    var role_status = responseArray['data']['role_status'];
                    var AtWorkStatus = responseArray['data']['AtWorkStatus'];
                    var OnCallStatus = responseArray['data']['OnCallStatus'];
                    var cust_class = responseArray['data']['cust_class'];
                    $('.imageUser').addClass(cust_class);

                    $('.imageUser img').attr('src',profile_img);
                    $('.userProfile-info .profileUserName').html(UserName);
                    $('.userDetails-field-otheruser-email a').attr('href','mailto:'+email).html(email);
                    $('.userDetails-field-otheruser-phone').html(phone);
                    $('.onCallTxt.otherProfText').html(role_status);

                    $(".modal-content").removeClass('loaderBg');
                }
            },
            error: function( result ){
                $('#profileLoader').fadeOut('fast');
            }
        });
      });
    });
</script>
