<?php

/*
 * Email Component
 *
 * Contains all related function for email sending
 *
 */

App::uses('CakeEmail', 'Network/Email');

class InsEmailComponent extends Component{
	private $mandrillKey = 'QcLlXKhzVK6oIr0o4U5gkw';

	/*
	------------------------------------------------------------------------------------------
	On: 23-01-2018
	I/P:
	O/P:
	Desc: Sending Chat History to InstitutionAdmin
	------------------------------------------------------------------------------------------
	*/
	public function sendChatHistory($params=array()){
	 	App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
		$mandrill = new Mandrill($this->mandrillKey);
		//$template = "mb_chat_history";
		$template = "MB_Exported_PDF";
		$template_content = array(
		array(
		'name' => $template,
		'content' => 'Chat History'
		)
		);

		$to[]=array('email'=> $params['toMail'],
		'name' => $params['name'],
		'type' => 'to');


		$var[]=array(
			'rcpt' => $params['toMail'],
			'vars' => array(
			array(
				'name' => 'NAME',
				'content' => $params['name']
			),
			array(
				'name' => 'CONVERSION_NAME',
				'content' => $params['conversationName']
			),
			array(
				'name' => 'FROM_DATE',
				'content' => $params['fromDate']
			),
			array(
				'name' => 'TO_DATE',
				'content' => $params['toDate']
			),
			)
		);
		$attachments[]=
                array(
                	"type" => "application/pdf",
                	"name"=> $params['chatHistoryFileName'],
                	"content"=> $params['chatHistoryFilePath']

            );


		$message = array(
		'html' => 'Chat History',
		'text' => 'Chat History',
		'subject' => $params['emailSubject'],
		'from_email' => 'support@medicbleep.com',//'medicbleep@theoncallroom.com',
		'from_name' => 'Medic Bleep',
		'to' => $to,
		"merge_vars" => $var,
		"subaccount" => MANDRILL_SUBACCOUNT,
		"attachments"=> $attachments
		);
		$async = false;
		$ip_pool = 'Main Pool';
		$send_at = date('d-m-Y');
		$result = $mandrill->messages->sendTemplate($template, $template_content, $message, $async, $ip_pool, $send_at);
	 	return json_encode($result);
	 }

 	public function sendEmailOnQrCodeRequest( $params = array()){
 		if( !empty($params) ){
 			try {
 				App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
 				$mandrill = new Mandrill('QcLlXKhzVK6oIr0o4U5gkw');
 				$fromName = '';

 				$template = "MB_Order_Received";

 			    $template_content = array(
 			        array(
 			            'name' => $template,
 			            'content' => 'MB Order Recieved'
 			        )
 			    );
 				$to[]=array(
 					'email'=> $params['to_email'],
 					'name' => $params['to_email'],
 					'type' => 'to'
 				);


 				$var[]=array(
 					'rcpt' => $params['to_email'],
 					'vars' => array(
 						array(
 							'name' => 'FNAME',
 							'content' => $params['fname']
 						),
 						array(
 							'name' => 'QUANTITY',
 							'content' => $params['quantity']
 						),
 						array(
 							'name' => 'DELIVERYDATE',
 							'content' => $params['delivery_date']
 						),
 					)
 				);

 				$message = array(
 				'html' => 'MB Order Recieved',
 				'text' => 'MB Order Recieved',
 				'subject' => 'MB Order Recieved',
 				//'from_email' => 'medicbleep@theoncallroom.com',
 				'from_email' => 'support@medicbleep.com',
 				'from_name' => 'Medic Bleep',
 				'to' => $to,
 				"merge_vars" => $var,
 				);
 				$async = false;
 				$ip_pool = 'Main Pool';
 				$send_at = date('d-m-Y');
 				$result = $mandrill->messages->sendTemplate($template, $template_content, $message, $async, $ip_pool, $send_at);
 				$msg = $result;
     			//print_r($result);

 			}
 			catch(Mandrill_Error $e) {
 			    // Mandrill errors are thrown as exceptions
 			    echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
 			    // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
 			    throw $e;
 			}
 		}
 	}

 	public function sendEmailOnQrCodeRequestToAdmin( $params = array()){
 		if( !empty($params) ){
 			try {
 				App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
 				$mandrill = new Mandrill('QcLlXKhzVK6oIr0o4U5gkw');
 				$fromName = '';

 				$template = "MB_RequestedQR_Internal";

 			    $template_content = array(
 			        array(
 			            'name' => $template,
 			            'content' => 'MB_RequestedQR_Internal'
 			        )
 			    );
 				$to[]=array(
 					'email'=> $params['to_email'],
 					'name' => $params['to_email'],
 					'type' => 'to'
 				);


 				$var[]=array(
 					'rcpt' => $params['to_email'],
 					'vars' => array(
 						array(
 							'name' => 'FNAME',
 							'content' => $params['fname']
 						),
 						array(
 							'name' => 'QUANTITY',
 							'content' => $params['quantity']
 						),
 						array(
 							'name' => 'DATETIME',
 							'content' => $params['date']
 						),
						array(
 							'name' => 'TRUSTNAME',
 							'content' => $params['trust']
 						),
 					)
 				);

 				$message = array(
 				'html' => 'Order Requested',
 				'text' => 'Order Requested',
 				'subject' => 'Order Requested',
 				//'from_email' => 'medicbleep@theoncallroom.com',
 				'from_email' => 'support@medicbleep.com',
 				'from_name' => 'Medic Bleep',
 				'to' => $to,
 				"merge_vars" => $var,
 				);
 				$async = false;
 				$ip_pool = 'Main Pool';
 				$send_at = date('d-m-Y');
 				$result = $mandrill->messages->sendTemplate($template, $template_content, $message, $async, $ip_pool, $send_at);
 				$msg = $result;
     			//print_r($result);

 			}
 			catch(Mandrill_Error $e) {
 			    // Mandrill errors are thrown as exceptions
 			    echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
 			    // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
 			    throw $e;
 			}
 		}
 	}


 	/*
	------------------------------------------------------------------------------------------
	On: 18-12-2018
	I/P:
	O/P:
	Desc: Sending Mail on role assing
	------------------------------------------------------------------------------------------
	*/

	public function assignRole($params=array()){
	 	App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
		$mandrill = new Mandrill($this->mandrillKey);

		if((string)$params['status'] == '1'){
			$template = "MB_Role_Assign";
			$nameTxt = 'Role Updated';
		}else{
			$template = "MB_Role_Removed";
			$nameTxt = 'Role Removed';
		}
		$template_content = array(
			array(
				'name' => $template,
				'content' => $nameTxt
			)
		);

		$to[]=array('email'=> $params['toMail'],
			'name' => $params['f_name'],
			'type' => 'to'
		);


		$var[]=array(
			'rcpt' => $params['toMail'],
			'vars' => array(
				array(
					'name' => 'FNAME',
					'content' => $params['f_name']
				),
				array(
					'name' => 'ROLE',
					'content' => $params['role']
				),
				array(
					'name' => 'TRUSTNAME',
					'content' => $params['trust_name']
				),
			)
		);


		$message = array(
			'html' => $nameTxt,
			'text' => $nameTxt,
			'subject' => $nameTxt,
			'from_email' => 'support@medicbleep.com',
			'from_name' => 'Medic Bleep',
			'to' => $to,
			"merge_vars" => $var,
		);
		$async = false;
		$ip_pool = 'Main Pool';
		$send_at = date('d-m-Y');
		$result = $mandrill->messages->sendTemplate($template, $template_content, $message, $async, $ip_pool, $send_at);
		$msg = $result;
		return $msg;
	}

	public function sendQRCode( $params = array()){

		try{
			App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
			$mandrill = new Mandrill('QcLlXKhzVK6oIr0o4U5gkw');
			//$template = "VERIFY_LINK";
			$template = 'MB_QR_Switchboard_Batch_Attached';

			$template_content = array(
				array(
					'name' => $template,
					'content' => 'MB_QR_Switchboard_Batch_Attached'
				)
			);

			$to[]=array('email'=> $params['to_email'],
			'name' => $params['to_email'],
			'type' => 'to');


			$var[]=array(
				'rcpt' => $params['to_email'],
				'vars' => array(
					array(
						'name' => 'FNAME',
						'content' => $params['fname']
					),
					array(
						'name' => 'BATCH',
						'content' => $params['batch']
					),
				)
			);
			$attachments[]=
	            array(
	            	"type" => $params['file_type'],
	            	"name"=> $params['file_name'],
	            	"content"=> $params['file_path']

	        );
			$message = array(
			'html' => 'QR Code Order Processed',
			'text' => 'QR Code Order Processed',
			'subject' => 'QR Code Order Processed',
			//'from_email' => 'medicbleep@theoncallroom.com',
			'from_email' => 'support@medicbleep.com',
			'from_name' => 'Medic Bleep',
			'to' => $to,
			"merge_vars" => $var,
			// "subaccount" => MANDRILL_SUBACCOUNT,
			"attachments"=> $attachments
			);
			$async = false;
			$ip_pool = 'Main Pool';
			$send_at = date('d-m-Y');
			$result = $mandrill->messages->sendTemplate($template, $template_content, $message, $async, $ip_pool, $send_at);
			$msg = $result;
		}
		catch(Exception $e) {
		    $msg = $e->getMessage();
		}
	 	return $msg;
	}

	public function sendQRCodeToAdmin( $params = array()){

		try{
			App::import('Vendor','MANDRILL',array('file' => 'mandrill/Mandrill.php'));
			$mandrill = new Mandrill('QcLlXKhzVK6oIr0o4U5gkw');
			//$template = "VERIFY_LINK";
			$template = 'MB_ProcessedQR_Internal';

			$template_content = array(
			array(
			'name' => $template,
			'content' => 'MB_ProcessedQR_Internal'
			)
			);

			$to[]=array('email'=> $params['to_email'],
			'name' => $params['to_email'],
			'type' => 'to');


			$var[]=array(
				'rcpt' => $params['to_email'],
				'vars' => array(
					array(
						'name' => 'FNAME',
						'content' => $params['fname']
					),
					array(
						'name' => 'QUANTITY',
						'content' => $params['quantity']
					),
					array(
						'name' => 'BATCH',
						'content' => $params['batch']
					),
					array(
						'name' => 'TRUSTNAME',
						'content' => $params['trust']
					),
					array(
						'name' => 'DATETIME',
						'content' => $params['date']
					),
				)
			);
			$attachments[]=
	            array(
	            	"type" => $params['file_type'],
	            	"name"=> $params['file_name'],
	            	"content"=> $params['file_path']

	        );
			$message = array(
			'html' => 'QR Code Generated',
			'text' => 'QR Code Generated',
			'subject' => 'QR Code Generated',
			//'from_email' => 'medicbleep@theoncallroom.com',
			'from_email' => 'support@medicbleep.com',
			'from_name' => 'Medic Bleep',
			'to' => $to,
			"merge_vars" => $var,
			// "subaccount" => MANDRILL_SUBACCOUNT,
			"attachments"=> $attachments
			);
			$async = false;
			$ip_pool = 'Main Pool';
			$send_at = date('d-m-Y');
			$result = $mandrill->messages->sendTemplate($template, $template_content, $message, $async, $ip_pool, $send_at);
			$msg = $result;
		}
		catch(Exception $e) {
		    $msg = $e->getMessage();
		}
	 	return $msg;
	}
}
?>
