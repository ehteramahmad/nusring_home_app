<?php

class SwitchboardController extends InstitutionAppController {
	public $components = array(
		'RequestHandler',
		'Paginator',
		'Session',
		'Institution.InsCommon',
		'Institution.InsEmail',
		'Image',
		'Quickblox',
		//'Admin.AdminEmail',
		'Cache'
	);

	public $uses = array(
		'EmailTemplate',
		'Institution.User',
		'Institution.EnterpriseUserList',
		'Institution.UserEmployment',
		'Institution.AdminUser',
		'Institution.CompanyName',
		'Institution.UserSubscriptionLog',
		'Institution.NotificationBroadcastMessage',
		'Institution.UserQbDetail',
		'Institution.UserDutyLog',
		'Institution.UserOneTimeToken',
		'Institution.CacheLastModifiedUser',
		'Institution.ExportChatTransaction',
		'Institution.UserProfile',
		'Institution.QrCodeRequest',
		'Institution.QrCodeDetail',
		'Institution.Profession',
		'Institution.RoleTag',
		'Institution.AvailableAndOncallTransaction',
		'Institution.TrustAdminActivityLog',
		'Institution.UserIpSetting',
		'Institution.UserBatonRole',
		'Institution.DepartmentsBatonRole',
		'Institution.BatonRole'
	);
	public $helpers = array('Form', 'Html', 'Js', 'Time','Paginator');

	public function index(){
		try{
				// Admin activity logs [START]
				$activityData = array();
				$activityData['company_id'] = $_COOKIE['adminInstituteId'];
				$activityData['admin_id'] = $_COOKIE['adminUserId'];
				$activityData['action'] = 'Switchboard Dashboard';
				$activityData['custom_data'] = 'View';
				$this->TrustAdminActivityLog->addActivityLog($activityData);
				// Admin activity logs [END]
		} catch (Exception $e) {

		}
	}

	public function batonFilter(){
		$this->autoRender = false;
		$returnVal = '';
		// $idata = array();
		// $total_batons = 0;
		// $occupied_batons = 0;
		// $idata['tCount'] = $total_batons;
		// $idata['tAssigned'] = $occupied_batons;
		if(isset($_COOKIE['adminInstituteId']) ){
			$companyId = $_COOKIE['adminInstituteId'];

			$total_batons = $this->DepartmentsBatonRole->find('count',array(
				'conditions'=>array(
					'DepartmentsBatonRole.is_active' => 1,
					'DepartmentsBatonRole.institute_id' => $companyId
				)
			));

			$occupied_batons = $this->DepartmentsBatonRole->find('count',array(
				'conditions'=>array(
					'DepartmentsBatonRole.is_active' => 1,
					'DepartmentsBatonRole.is_occupied' => 1,
					'DepartmentsBatonRole.institute_id' => $companyId
				)
			));

			$idata['tCount'] = (int)$total_batons;
			$idata['tAssigned'] = (int)$occupied_batons;
			$returnVal = json_encode($idata);
		}
		echo $returnVal;
		// else{
		// 	$this->redirect(BASE_URL . 'institution/Switchboard/index');
		// }

		try{
				$activityData = array();
				$activityData['company_id'] = $_COOKIE['adminInstituteId'];
				$activityData['admin_id'] = $_COOKIE['adminUserId'];
				$activityData['action'] = 'BatonRole Counter - Total and Assigned';
				$activityData['custom_data'] = 'Get';
				$this->TrustAdminActivityLog->addActivityLog($activityData);
				// Admin activity logs [END]
		} catch (Exception $e) {

		}
	}

	public function addBatonRole(){
		$this->autoRender = false;
		$responseData = array();
		if(isset($_COOKIE['adminInstituteId']) && $this->request->is('post')) {
			$companyId = $_COOKIE['adminInstituteId'];
				$batonRoleName = $this->request->data['role_name'];
				$on_call_value = $this->request->data['on_call_value'];
				$createdDate = date("Y-m-d H:i:s");
				if( !empty($batonRoleName) && !empty($companyId) ){
						$roleTagData = $this->BatonRole->find('first',array(
							'conditions'=>array(
								'LOWER(BatonRole.baton_roles)' => strtolower($batonRoleName),
								'BatonRole.is_active' => 1,
								'DepartmentsBatonRole.institute_id' => $companyId
							),
							'joins'=>array(
								array(
				          'table' => 'departments_baton_roles',
				          'alias' => 'DepartmentsBatonRole',
				          'type' => 'left',
				          'conditions'=> array('BatonRole.id = DepartmentsBatonRole.role_id')
				        )
							)
						));
						if( !empty($roleTagData) ){
							$DeptroleTagData = $this->DepartmentsBatonRole->find('first',array('conditions'=>array('DepartmentsBatonRole.role_id'=> $roleTagData['BatonRole']['id'],'DepartmentsBatonRole.is_active'=>1)));
							if($roleTagData['BatonRole']['is_active'] == 1 && $DeptroleTagData['DepartmentsBatonRole']['is_active'] == 1){
								$responseData['status'] = 0;
								$responseData['message'] = 'Already Exists';
								echo json_encode($responseData);
							}else{
								$this->BatonRole->updateAll(array('is_active' => 1),array('id'=> $roleTagData['BatonRole']['id']));
								$this->DepartmentsBatonRole->updateAll(array('is_active' => 1),array('role_id'=> $roleTagData['BatonRole']['id']));
								$responseData['status'] = 1;
								$responseData['message'] = 'Baton Role Added Successfully';
								echo json_encode($responseData);
							}
						}else{
								$this->BatonRole->saveAll(
									array(
										'baton_roles'=> $batonRoleName,
										"is_active"=> 1,
										'created_at' => $createdDate
									)
								);
								$roleId = $this->BatonRole->getLastInsertID();
								$this->DepartmentsBatonRole->saveAll(
									array(
										'department_id' => 1,
										"role_id" => $roleId,
										'is_occupied' => 0,
										'is_active' => 1,
										// 'timeout' => ,
										'institute_id' => $companyId,
										'on_call_value' => $on_call_value,
										'created_at' => $createdDate
									)
								);
								$responseData['status'] = 1;
								$responseData['message'] = 'Baton Role Added Successfully';
								echo json_encode($responseData);
						}
						try{
								// Admin activity logs [START]
								$activityData = array();
								$activityData['company_id'] = $_COOKIE['adminInstituteId'];
								$activityData['admin_id'] = $_COOKIE['adminUserId'];
								$activityData['action'] = $responseData['message'];
								$activityData['custom_data'] = '{"role_name":"'.$batonRoleName.'","on_call_value":"'.$on_call_value.'"}';
								$this->TrustAdminActivityLog->addActivityLog($activityData);
								// Admin activity logs [END]
						} catch (Exception $e) {

						}
				}else{
					$responseData['status'] = 0;
					$responseData['message'] = 'Empty Email Or Company Id.';
					echo json_encode($responseData);
				}
		}else{
			$responseData['status'] = 0;
			$responseData['message'] = 'Information not provided.';
			echo json_encode($responseData);
		}
	}

	public function assignUserSearch(){
		// baton_user_assign_ajax
		if(isset($_COOKIE['adminInstituteId']) && null !== $_COOKIE['adminInstituteId'] && $_COOKIE['adminInstituteId'] !='' ){
				$companyId = $_COOKIE['adminInstituteId'];
				$groupAdmin = $_COOKIE['instituteGroupAdminId'];
		}

	}

  public function getRequest(){
		// $this->checkAdminRole();
		try{
				// Admin activity logs [START]
				$activityData = array();
				$activityData['company_id'] = $_COOKIE['adminInstituteId'];
				$activityData['admin_id'] = $_COOKIE['adminUserId'];
				$activityData['action'] = 'Baton Role Request List';
				$activityData['custom_data'] = 'View';
				$this->TrustAdminActivityLog->addActivityLog($activityData);
				// Admin activity logs [END]
		} catch (Exception $e) {

		}
  }
  public function getRequestFilter(){
		$this->autoRender = false;
		$total_count = '';
    // if( null !== $_COOKIE['adminInstituteId'] && $_COOKIE['adminInstituteId'] !='' ){
			$companyId = $_COOKIE['adminInstituteId'];
			$groupAdmin = $_COOKIE['instituteGroupAdminId'];
      $userId = 0;

      $conditions = array(
          "UserBatonRole.from_user"=> $userId,
          "UserBatonRole.status"=> array(2,4),
          "UserBatonRole.is_active"=> 1,
					"DepartmentsBatonRole.institute_id" => $companyId
        );

      $joins = array(
        array(
          'table' => 'departments_baton_roles',
          'alias' => 'DepartmentsBatonRole',
          'type' => 'left',
          'conditions'=> array('UserBatonRole.role_id = DepartmentsBatonRole.role_id')
        )
      );

      $options = array(
        'conditions' => $conditions,
        'joins' => $joins,
				'group' => array('UserBatonRole.role_id')
      );

			$total_count = $this->UserBatonRole->find('count',$options);

			return (int)$total_count;

		try{
				$activityData = array();
				$activityData['company_id'] = $_COOKIE['adminInstituteId'];
				$activityData['admin_id'] = $_COOKIE['adminUserId'];
				$activityData['action'] = 'BatonRole Requested Counter';
				$activityData['custom_data'] = 'Get';
				$this->TrustAdminActivityLog->addActivityLog($activityData);
				// Admin activity logs [END]
		} catch (Exception $e) {

		}
  }

  public function getLatestRequest(){
		$this->autoRender = false;
    if(isset($_COOKIE['adminInstituteId']) && null !== $_COOKIE['adminInstituteId'] && $_COOKIE['adminInstituteId'] !='' ){
			$companyId = $_COOKIE['adminInstituteId'];
			$groupAdmin = $_COOKIE['instituteGroupAdminId'];
      $userId = 0;
      $fields = array('
        UserBatonRole.role_id,
        UserBatonRole.from_user,
        UserBatonRole.to_user,
        UserBatonRole.role_id,
        UserBatonRole.status,
        UserBatonRole.notes,
        UserBatonRole.updated_at,
				UserProfile.user_id,
        UserProfile.first_name,
        UserProfile.last_name,
        UserProfile.profile_img,
        UserProfile.thumbnail_img,
        UserProfiles.user_id,
        UserProfiles.first_name,
        UserProfiles.last_name,
        UserProfiles.profile_img,
        UserProfiles.thumbnail_img,
				UserProfiles.profession_id,
				UserProfiles.role_status,
				Professions.profession_type,
				User.email,
        BatonRole.id,
        BatonRole.baton_roles,
        DepartmentsBatonRole.timeout,
        DepartmentsBatonRole.on_call_value,
				UserDutyLog.atwork_status,
				UserDutyLog.status
      ');
      $conditions = array(
          "UserBatonRole.from_user !="=> $userId,
          "UserBatonRole.status"=> array(1,2,5,7),
          "UserBatonRole.is_active"=> 1,
		  		"DepartmentsBatonRole.institute_id" => $companyId
        );
      $joins = array(

				array(
          'table' => 'user_profiles',
          'alias' => 'UserProfile',
          'type' => 'left',
          'conditions'=> array('UserBatonRole.from_user = UserProfile.user_id')
        ),
        array(
          'table' => 'user_profiles',
          'alias' => 'UserProfiles',
          'type' => 'left',
          'conditions'=> array('UserBatonRole.to_user = UserProfiles.user_id')
        ),
				array(
					'table' => 'users',
					'alias' => 'User',
					'type' => 'left',
					'conditions'=> array('User.id = UserProfile.user_id')
				),
				array(
					'table' => 'professions',
					'alias' => 'Professions',
					'type' => 'left',
					'conditions'=> array('UserProfile.profession_id = Professions.id')
				),
        array(
          'table' => 'departments_baton_roles',
          'alias' => 'DepartmentsBatonRole',
          'type' => 'left',
          'conditions'=> array('UserBatonRole.role_id = DepartmentsBatonRole.role_id')
        ),
        array(
          'table' => 'baton_roles',
          'alias' => 'BatonRole',
          'type' => 'left',
          'conditions'=> array('DepartmentsBatonRole.role_id = BatonRole.id'),
          'group'=> array('UserBatonRole.role_id')
        ),
				array(
					'table' => 'user_duty_logs',
					'alias' => 'UserDutyLog',
					'type' => 'left',
					'conditions'=> array('User.id = UserDutyLog.user_id')
				)
      );

      $options = array(
        'conditions' => $conditions,
        'joins' => $joins,
        'fields' => $fields,
				'limit' => 5,
				'group'=> 'UserBatonRole.role_id',
				'order' => array('UserBatonRole.updated_at DESC')
      );

      $userList = $this->UserBatonRole->find('all',$options);
			$final_data = array();
			foreach ($userList as $user) {
				if($user){
					$data = array();
					$data['user_name'] = $user['UserProfile']['first_name']." ".$user['UserProfile']['last_name'];
					$data['user_id'] = isset($user['UserProfile']['user_id']) ? $user['UserProfile']['user_id'] : 0;
					$data['profile_img'] =	isset($user['UserProfile']['profile_img']) ? AMAZON_PATH.$user['UserProfile']['user_id'].'/profile/'.$user['UserProfile']['profile_img']:'';
					$data['thumbnail_img'] = isset($user['UserProfile']['thumbnail_img']) ? $user['UserProfile']['thumbnail_img'] : "";
					$data['role_name'] = isset($user['BatonRole']['baton_roles']) ? $user['BatonRole']['baton_roles'] : "";
					$data['notes'] = isset($user['UserBatonRole']['notes']) ? $user['UserBatonRole']['notes'] : "";
					$data['role_id'] = isset($user['UserBatonRole']['role_id']) ? $user['UserBatonRole']['role_id'] : 0;
					$data['type'] = isset($user['UserBatonRole']['status']) ? $user['UserBatonRole']['status'] : 0;
					$data['created'] = $user['UserBatonRole']['updated_at'];
					$data['on_call_value'] = isset($user['DepartmentsBatonRole']['on_call_value']) ? $user['DepartmentsBatonRole']['on_call_value'] : 0;
					if(isset($user['UserProfile']['role_status'])){
						$data['user_role'] = $user['UserProfile']['role_status'];
					}else{
						$data['user_role'] = $user['Professions']['profession_type'];
					}
					$data['user_email'] = $user['User']['email'];

					$cust_class = 'notAtWork';
					if((string)$user['UserDutyLog']['status'] == '1'){
						$cust_class = 'onCallMain';
					}else if((string)$user['UserDutyLog']['atwork_status'] == '0'){
						$cust_class = 'notAtWork';
					}else{
						$cust_class = '';
					}

					$data['cust_class'] = $cust_class;
					$final_data[] = $data;
				}
			}

			$this->set(array("data"=>$final_data));
			// $this->render('/Elements/baton/latest_request_ajax');
			$this->render('/Elements/baton/latest_assigned_ajax');

			try{
					$activityData = array();
					$activityData['company_id'] = $_COOKIE['adminInstituteId'];
					$activityData['admin_id'] = $_COOKIE['adminUserId'];
					$activityData['action'] = 'Latest BatonRole Assigned';
					$activityData['custom_data'] = 'Get';
					$this->TrustAdminActivityLog->addActivityLog($activityData);
					// Admin activity logs [END]
			} catch (Exception $e) {

			}
  	}
  }

  public function assignUnassignBatonRole(){
    $this->autoRender = false;
    $responseData = array();
    if($this->request->is('post')) {
      $userId = $this->request->data['user_id'];
      $status = $this->request->data['status'];
      $companyId = $_COOKIE['adminInstituteId'];
      $roleId = $this->request->data['role_id'];
			$role_details = $this->DepartmentsBatonRole->find('first',array('conditions'=>array('role_id'=>$roleId)));
			$oncallValue = $role_details['DepartmentsBatonRole']['on_call_value'];
			$createdDate = date("Y-m-d H:i:s");

      if( !empty($companyId) && !empty($userId) ){

        if($status == 1){

          $this->UserBatonRole->saveAll(array('from_user'=>$userId,'to_user'=>0,'role_id'=>$roleId,'status'=>4,'is_active'=>1,'created_at'=>$createdDate,'updated_at'=>$createdDate,'deleted_at'=>$createdDate));
          $this->UserBatonRole->saveAll(array('to_user'=>$userId,'from_user'=>0,'role_id'=>$roleId,'status'=>5,'is_active'=>1,'created_at'=>$createdDate,'updated_at'=>$createdDate,'deleted_at'=>$createdDate));
          $this->DepartmentsBatonRole->updateAll(array('DepartmentsBatonRole.is_occupied'=>1),array('DepartmentsBatonRole.role_id'=>$roleId,'DepartmentsBatonRole.institute_id'=>$companyId));

					// $paramsVal['user_id'] = $userId;
					// $paramsVal['on_call_value'] = $oncallValue;
					// $paramsVal['company_id'] = $companyId;
					// $this->updateOnCallStatusRoleExchange($paramsVal);
					// $this->updateCacheOnStatusChange($userId,$companyId);

					$params['request_type'] = "swichborad_transfered";
					$params['user_id'] = $userId;
					$params['role_id'] = $roleId;
					$params['from_user_id'] = 0;
					$params['push_on'] = 0;
					$sendRoleExchangePushNotification = $this->sendRoleExchangePushNotification($params);

        }else{
          $this->DepartmentsBatonRole->updateAll(array('DepartmentsBatonRole.is_occupied'=>1),array('DepartmentsBatonRole.role_id'=>$roleId,'DepartmentsBatonRole.institute_id'=>$companyId));
          $this->UserBatonRole->updateAll(array('UserBatonRole.is_active'=>0,'UserBatonRole.updated_at'=>"'" . $createdDate . "'"),array('UserBatonRole.from_user'=>$userId,'UserBatonRole.status'=>1,'UserBatonRole.role_id'=>$roleId));
					$this->updateCacheOnStatusChange($userId,$companyId);

					$params['request_type'] = "swichborad_removed";
					$params['user_id'] = $userId;
					$params['role_id'] = $roleId;
					$params['from_user_id'] = 0;
					$params['push_on'] = 0;
					$sendRoleExchangePushNotification = $this->sendRoleExchangePushNotification($params);

        }

  			$responseData['status'] = 1;
        $responseData['message'] = 'ok.';
        echo json_encode($responseData);
				try{
						// Admin activity logs [START]
						$activityData = array();
						$activityData['company_id'] = $_COOKIE['adminInstituteId'];
						$activityData['admin_id'] = $_COOKIE['adminUserId'];
						$activityData['user_id'] = $userId;
						$activityData['action'] = $params['request_type'];
						$activityData['custom_data'] = '{"role_id":"'.$roleId.'"}';
						$this->TrustAdminActivityLog->addActivityLog($activityData);
						// Admin activity logs [END]
				} catch (Exception $e) {

				}

      }else{
        $responseData['status'] = 0;
        $responseData['message'] = 'Empty Email Or Company Id.';
        echo json_encode($responseData);
      }
    }else{
      $responseData['status'] = 0;
      $responseData['message'] = 'Information not provided.';
      echo json_encode($responseData);
    }
  }

  public function changeBatonRequestStatus(){
    $this->autoRender = false;
    $responseData = array();
    if($this->request->is('post')) {
      $userId = $this->request->data['user_id'];
      $status = $this->request->data['status'];
      $companyId = $_COOKIE['adminInstituteId'];
      $roleId = $this->request->data['role_id'];
			$createdDate = date("Y-m-d H:i:s");
      if( !empty($companyId) && !empty($userId) ){
				$role_details = $this->DepartmentsBatonRole->find('first',array('conditions'=>array('role_id'=>$roleId)));
				$oncallValue = $role_details['DepartmentsBatonRole']['on_call_value'];
        if($status == 1){
          // try {
              $this->UserBatonRole->updateAll(array('UserBatonRole.status'=>1,'UserBatonRole.updated_at'=>"'" . $createdDate . "'"),array('UserBatonRole.from_user'=>$userId,'UserBatonRole.status'=>3,'UserBatonRole.role_id'=>$roleId));

							$paramsVal['user_id'] = $userId;
							$paramsVal['on_call_value'] = $oncallValue;
							$paramsVal['company_id'] = $companyId;

							$this->updateOnCallStatusRoleExchange($paramsVal);
							$this->updateCacheOnStatusChange($userId,$companyId);

							$params['request_type'] = "swichborad_accept";
            	$params['user_id'] = $userId;
							$params['role_id'] = $roleId;
            	$params['from_user_id'] = 0;
            	$params['push_on'] = 1;

							$sendRoleExchangePushNotification = $this->sendRoleExchangePushNotification($params);

          // } catch (\Exception $e) {}
          $this->DepartmentsBatonRole->updateAll(array('DepartmentsBatonRole.is_occupied'=>1),array('DepartmentsBatonRole.role_id'=>$roleId,'DepartmentsBatonRole.institute_id'=>$companyId));
        }else{
					$this->updateCacheOnStatusChange($userId,$companyId);

					$params['request_type'] = "swichborad_rejected";
					$params['user_id'] = $userId;
					$params['role_id'] = $roleId;
					$params['from_user_id'] = 0;
					$params['push_on'] = 0;
					$sendRoleExchangePushNotification = $this->sendRoleExchangePushNotification($params);

          $this->UserBatonRole->updateAll(array('UserBatonRole.is_active'=>0,'UserBatonRole.updated_at'=>"'" . $createdDate . "'"),array('UserBatonRole.from_user'=>$userId,'UserBatonRole.status'=>3,'UserBatonRole.role_id'=>$roleId));
					$this->DepartmentsBatonRole->updateAll(array('DepartmentsBatonRole.is_occupied'=>0),array('DepartmentsBatonRole.role_id'=>$roleId,'DepartmentsBatonRole.institute_id'=>$companyId));
        }

        // try {
          $this->UserBatonRole->updateAll(array('UserBatonRole.is_active'=>0,'UserBatonRole.updated_at'=>"'" . $createdDate . "'"),array('UserBatonRole.from_user'=>0,'UserBatonRole.status'=>2,'UserBatonRole.role_id'=>$roleId));
        // } catch (\Exception $e2) {}

  			$responseData['status'] = 1;
        $responseData['message'] = 'ok.';
        echo json_encode($responseData);
				// ----- Update in user profile for directory [START] ----
				$update_params = array();
				$update_params['to_user'] = 0;
				$update_params['from_user'] = $userId;
				$this->getUserBatonRolesData($update_params);
				// ----- Update in user profile for directory [END] ----

				try{
						// Admin activity logs [START]
						$activityData = array();
						$activityData['company_id'] = $_COOKIE['adminInstituteId'];
						$activityData['admin_id'] = $_COOKIE['adminUserId'];
						$activityData['user_id'] = $userId;
						$activityData['action'] = $params['request_type'];
						$activityData['custom_data'] = '{"role_id":"'.$roleId.'","type":"Take On"}';
						$this->TrustAdminActivityLog->addActivityLog($activityData);
						// Admin activity logs [END]
				} catch (Exception $e) {

				}

      }else{
        $responseData['status'] = 0;
        $responseData['message'] = 'Empty Email Or Company Id.';
        echo json_encode($responseData);
      }
    }else{
      $responseData['status'] = 0;
      $responseData['message'] = 'Information not provided.';
      echo json_encode($responseData);
    }
  }


  public function changeBatonTransferRequestStatus(){
    $this->autoRender = false;
    $responseData = array();
    if($this->request->is('post')) {
      $userId = $this->request->data['user_id'];
      $status = $this->request->data['status'];
      $companyId = $_COOKIE['adminInstituteId'];
      $roleId = $this->request->data['role_id'];
			$createdDate = date("Y-m-d H:i:s");
      if( !empty($companyId) && !empty($userId) ){
				$role_details = $this->DepartmentsBatonRole->find('first',array('conditions'=>array('role_id'=>$roleId)));
				$oncallValue = $role_details['DepartmentsBatonRole']['on_call_value'];
        if($status == 1){
          $this->UserBatonRole->updateAll(array('UserBatonRole.is_active'=>0,'UserBatonRole.updated_at'=>"'" . $createdDate . "'"),array('UserBatonRole.from_user'=>$userId,'UserBatonRole.status'=>5,'UserBatonRole.role_id'=>$roleId));
          $this->DepartmentsBatonRole->updateAll(array('DepartmentsBatonRole.is_occupied'=>0),array('DepartmentsBatonRole.role_id'=>$roleId,'DepartmentsBatonRole.institute_id'=>$companyId));

					$paramsVal['user_id'] = $userId;
					$paramsVal['on_call_value'] = $oncallValue;
					$paramsVal['company_id'] = $companyId;

					$this->updateCacheOnStatusChange($userId,$companyId);

					$params['request_type'] = "swichborad_transfer_accept";
					$params['user_id'] = $userId;
					$params['role_id'] = $roleId;
					$params['from_user_id'] = 0;
					$params['push_on'] = 0;

					$sendRoleExchangePushNotification = $this->sendRoleExchangePushNotification($params);

        }else{

          $this->UserBatonRole->updateAll(array('UserBatonRole.is_active'=>1,'UserBatonRole.updated_at'=>"'" . $createdDate . "'"),array('UserBatonRole.from_user'=>$userId,'UserBatonRole.status'=>1,'UserBatonRole.role_id'=>$roleId));
					$paramsVal['user_id'] = $userId;
					$paramsVal['on_call_value'] = $oncallValue;
					$paramsVal['company_id'] = $companyId;

					$this->updateCacheOnStatusChange($userId,$companyId);

					$params['request_type'] = "swichborad_transfer_rejected";
					$params['user_id'] = $userId;
					$params['role_id'] = $roleId;
					$params['from_user_id'] = 0;
					$params['push_on'] = 1;

					$sendRoleExchangePushNotification = $this->sendRoleExchangePushNotification($params);
        }
        $this->UserBatonRole->updateAll(array('UserBatonRole.is_active'=>0),array('UserBatonRole.from_user'=>0,'UserBatonRole.status'=>4,'UserBatonRole.role_id'=>$roleId));

  			$responseData['status'] = 1;
        $responseData['message'] = 'ok.';
        echo json_encode($responseData);

				// ----- Update in user profile for directory [START] ----
				$update_params = array();
				$update_params['to_user'] = 0;
				$update_params['from_user'] = $userId;
				$this->getUserBatonRolesData($update_params);
				// ----- Update in user profile for directory [END] ----

				try{
						// Admin activity logs [START]
						$activityData = array();
						$activityData['company_id'] = $_COOKIE['adminInstituteId'];
						$activityData['admin_id'] = $_COOKIE['adminUserId'];
						$activityData['user_id'] = $userId;
						$activityData['action'] = $params['request_type'];
						$activityData['custom_data'] = '{"role_id":"'.$roleId.'","type":"Transfer"}';
						$this->TrustAdminActivityLog->addActivityLog($activityData);
						// Admin activity logs [END]
				} catch (Exception $e) {

				}
      }else{
        $responseData['status'] = 0;
        $responseData['message'] = 'Empty Email Or Company Id.';
        echo json_encode($responseData);
      }
    }else{
      $responseData['status'] = 0;
      $responseData['message'] = 'Information not provided.';
      echo json_encode($responseData);
    }
  }

	public function updateCacheOnStatusChange($userId, $companyId){
	    //** update dynamic cache[STATRT]
	    $lastModified = date('Y-m-d H:i:s');
	    if(! empty($userId)){
	        $getDynamicChangedUser = $this->CacheLastModifiedUser->find("count", array("conditions"=> array("user_id"=> $userId, "company_id"=> $companyId, "type"=>"dynamic_directory_changed")));
	        // return $getDynamicChangedUser;
	        if($getDynamicChangedUser > 0){
	            $this->CacheLastModifiedUser->updateLastModifiedDate($userId, $companyId, 'dynamic_directory_changed');
	        }else{
	            $saveCacheLastModifiedData = array("user_id"=> $userId, "company_id"=> $companyId, "type"=>"dynamic_directory_changed", "status"=>"1", "last_modified"=>$lastModified);
	            $this->CacheLastModifiedUser->saveAll($saveCacheLastModifiedData);
	        }

	        //** Get last modified date dynamic directory[START]
	        $getDynamicDirectoryLastModified = $this->CacheLastModifiedUser->getLastModifiedDateDynamicDirectory($companyId);
	        if(!empty($getDynamicDirectoryLastModified)){
	            $paramsDynamicEtagUpdate['lastmodified'] = $getDynamicDirectoryLastModified[0]['cache_last_modified_users']['last_modified'];
	            $paramsDynamicEtagUpdate['key'] = 'dynamicDirectoryEtag:'.$companyId;
	            $this->updateDynamicEtagData($paramsDynamicEtagUpdate);
	        }
	        //** Get last modified date dynamic directory[END]
	    }
	    //** update dynamic cache[END]
	}

	public function manageBatonRoles(){
		// $this->checkAdminRole();
		try{
				// Admin activity logs [START]
				$activityData = array();
				$activityData['company_id'] = $_COOKIE['adminInstituteId'];
				$activityData['admin_id'] = $_COOKIE['adminUserId'];
				$activityData['action'] = 'Baton Role List';
				$activityData['custom_data'] = 'View';
				$this->TrustAdminActivityLog->addActivityLog($activityData);
				// Admin activity logs [END]
		} catch (Exception $e) {

		}
	}

	public function generateQrCode(){
		// $this->checkAdminRole();
		try{
				// Admin activity logs [START]
				$activityData = array();
				$activityData['company_id'] = $_COOKIE['adminInstituteId'];
				$activityData['admin_id'] = $_COOKIE['adminUserId'];
				$activityData['action'] = 'Generate QR Code';
				$activityData['custom_data'] = 'View';
				$this->TrustAdminActivityLog->addActivityLog($activityData);
				// Admin activity logs [END]
		} catch (Exception $e) {

		}
	}

	public function genrateQrCodeFunc()
    {
    	$this->autoRender = false;

        App::import('Vendor','QRcode',array('file' => 'phpqrcode/qrlib.php'));
        App::import('Vendor','TCPDF',array('file' => 'tcpdf/tcpdf.php'));
        App::import('Vendor','FPDI',array('file' => 'fpdi/fpdi.php'));
        App::import('Vendor','FPDF',array('file' => 'fpdf/fpdf.php'));
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $fpdi = new FPDI();
        $fpdf = new FPDF('P', 'mm', 'A4');

        $fpdf->AddPage();
        $numberOfQrCode = $this->request->data['number_of_qr_codes'];
        $validity = $this->request->data['validity'];
        $instituteId = $_COOKIE['adminInstituteId'];
        $qr_type =  $this->request->data['qr_type'];
        //$requestId = $this->QrCodeRequest->getLastInsertId();
        //$lastCreated = $this->QrCodeRequest->find('first', array('order' => array('QrCodeRequest.id' =>'desc')));
        //$requestId = $lastCreated['QrCodeRequest']['id'] + 1;
        //print_r($requestId);

        $imgrandom =  $instituteId.rand(1000,100000);//$this->request->data['request_id'];

        $batchNumber = "MB-${instituteId}-".strtotime(date('Y-m-d H:i:s'));

        $calValidity = '';
        switch ($validity) {
        	case 1:
        		$calValidity = '1 Day';
        		break;
        	case 7:
        		$calValidity = '1 Week';
        		break;
        	case 14:
        		$calValidity = '2 Weeks';
        		break;
        	case 30:
        		$calValidity = '1 Month';
        		break;
        	case 90:
        		$calValidity = '3 Months';
        		break;
        	case 180:
        		$calValidity = '6 Months';
        		break;
        	case 365:
        		$calValidity = '1 Year';
        		break;

        	default:
        		# code...
        		break;
        }

        if($qr_type == 1){
        	$qrcodenumber = $instituteId.rand(1000,100000);
            $arr = array("batch_number" => $batchNumber, "qr_code_number"=> $qrcodenumber,"institute_id" => $instituteId);
            $json_data = json_encode($arr);
            // echo "$json_data";exit();
            $text= $json_data;
            $folder = APP.'webroot/qrCode/';
            $file_name= "qrcode-$imgrandom.png";
            $file_name2=$folder.$file_name;
            QRcode::png($text,$file_name2);
            //echo $file_name;
            $data['qr_type'] = 1;
            $data['batch_number'] = $batchNumber;
            $data['validity'] = $calValidity;
            $data['img'] = BASE_URL.'app/webroot/qrCode/'.$file_name;
						$data['qr_img_name'] = $file_name;
            echo json_encode($data);

        	$companyId = $_COOKIE['adminInstituteId'];
			$userEmail = $_COOKIE['adminEmail'];
			$requestId = '';
			if(!empty($companyId) ){
			    $user = $this->User->find('first',array('conditions'=>array('email'=>$userEmail)));
			    if(!empty($user)){
			        $this->QrCodeRequest->save(array('institute_id'=> $companyId,'user_id'=>$user['User']['id'] , "number_of_qr_codes"=> $numberOfQrCode, 'validity'=> $validity, 'status' => "1"));
			        $requestId = $this->QrCodeRequest->getLastInsertId();
			        //echo $requestId;
			    }
			}

			$qrCodeData=array('card_id'=>"${instituteId}1",'request_id'=>$requestId,'batch_number'=>$batchNumber,'qr_code_number'=>$qrcodenumber,'validity'=>$validity, "institute_id"=> $instituteId, "is_used"=>"1", "is_valid"=> "1");

	        $this->QrCodeDetail->saveAll($qrCodeData);
        }else{
        	$textHtml = '';

			$qrArray = [];
	        for($i= 1; $i <= $numberOfQrCode; $i++){
	            $qrcodenumber = $instituteId.rand(1000,100000).$i;
	            $qrArray[$i] = $qrcodenumber;
	            $arr = array("batch_number" => $batchNumber, "qr_code_number"=> $qrcodenumber,"institute_id" => $instituteId);
	            $json_data = json_encode($arr);

	            $text= $json_data;
	            $folder = APP.'webroot/qrCode/';
	            $file_name= "qrcode-$qrcodenumber.png";
	            $file_name2=$folder.$file_name;
	            QRcode::png($text,$file_name2);

							$textHtml .= '<table cellspacing="0" cellpadding="2" color="#1B3B6A" bgcolor="#FFFFFF"><tr><td width="570px" bgcolor="#FFFFFF">';
							$textHtml .= '<table  width="570px" cellspacing="0" cellpadding="2" color="#1B3B6A" bgcolor="#FFFFFF"><tr><td width="230px"><img src="'.APP.'Plugin/Institution/webroot/img/medicbleep-logo-web-HIghres.png" alt="" width="140px" /></td><td width="" align="right" bgcolor="#FFFFFF"><img src="'.APP.'Plugin/Institution/webroot/img/trustlogo/logo_'.$_COOKIE['adminInstituteId'].'.png" onerror="this.src=\''.APP.'Plugin/Institution/webroot/img/trustlogo/logo_default.png\'" alt="" height="27px" /></td></tr></table>';
							$textHtml .= '</td></tr>';
							$textHtml .= '<tr width="570px;"><td>&nbsp;</td></tr>';

							if($numberOfQrCode == 1){
								$numCodeTxt = '1 QR code';
							}else{
								$numCodeTxt = $numberOfQrCode . ' QR codes';
							}
							$textHtml .= '<tr><td width="570px;"><table cellspacing="0" cellpadding="2" color="#1B3B6A" bgcolor="#FFFFFF">';
							$textHtml .= '<tr><td width="370px">Generated '.$numCodeTxt.' : Subscription : '.$calValidity.'</td><td width="200px;" align="right"> BATCH NO : '.$batchNumber.'</td></tr></table></td></tr>';
							$textHtml .= '<tr><td width="570px;"><hr></td></tr>';
							$textHtml .= '<tr><td width="570px;">Scan QR Code</td></tr>';
							$textHtml .= '<tr><td width="570px;"><small>Please scan QR code to provide access to your institution.</small></td></tr>';
										$textHtml .= '<tr>';
										$textHtml .= '<td width="570px" height="600px" align="center">';
				            $textHtml .= '<div><br><br><br><br><br><br><br><br><br><br><br><br></div><img src="'.$file_name2.'" width="150px" height="150px" alt="">';
				            $textHtml .= '<div><small>QR-'.$qrcodenumber.'</small></div>';
				            $textHtml .= '</td>';
										$textHtml .= '</tr>';
										$textHtml .= '</table>';


	            if($i == 1){
		            $companyId = $_COOKIE['adminInstituteId'];
					$userEmail = $_COOKIE['adminEmail'];
					if(!empty($companyId) ){
					    $user = $this->User->find('first',array('conditions'=>array('email'=>$userEmail)));
					    if(!empty($user)){
					        $this->QrCodeRequest->save(array('institute_id'=> $companyId,'user_id'=>$user['User']['id'] , "number_of_qr_codes"=> $numberOfQrCode, 'validity'=> $validity, 'status' => "1"));
					        $requestId = $this->QrCodeRequest->getLastInsertId();
					        //echo $requestId;
					    }
					}
				}

	            $qrCodeData=array('card_id'=>"${instituteId}1",'request_id'=>$requestId,'batch_number'=>$batchNumber,'qr_code_number'=>$qrcodenumber,'validity'=>$validity, "institute_id"=> $instituteId, "is_used"=>"1", "is_valid"=> "1");

	            $this->QrCodeDetail->saveAll($qrCodeData);
	        }
	        //print_r($textHtml);exit();
	        $fpdi->AddPage('P');
	        $fpdi->SetXY(5, 5);
	        $fpdi->setPrintHeader(false);
	    		$fpdi->setPrintFooter(false);
	        $fpdi->writeHTML($textHtml, true, true, true, 0);

	        $finalFileName = "QrCode"."-". strtotime(date('Y-m-d H:i:s')) . '.pdf';
        	$output = $fpdi->Output(APP . 'webroot/qrCode/'.$finalFileName, 'F');



	         if(file_exists(APP . 'webroot/qrCode/'.$finalFileName)){
	            $userDetails = $this->QrCodeRequest->find('first',array('conditions'=>array('QrCodeRequest.id'=>$requestId)));
	            if(!empty($userDetails)){
	                if(isset($userDetails['QrCodeRequest']['user_id'])){
	                    if($userDetails['QrCodeRequest']['user_id'] == 0){
	                        $email = 'ehteram@mediccreations.com';
	                        $name = 'Admin';
	                    }else{
	                        $user = $this->User->find('first',array('conditions'=>array('User.id'=>$userDetails['QrCodeRequest']['user_id'])));
	                        $email = $user['User']['email'];
	                        $name = $user['UserProfile']['first_name'];
	                    }
	                    //** Send Consent mail to patient
	                    chmod(APP . 'webroot/qrCode/'.$finalFileName, 0777);

	                    $client_params = array();
	                    $client_params['file_name'] = $finalFileName;
	                    $client_params['file_path']= base64_encode(file_get_contents(APP . 'webroot/qrCode/'.$finalFileName));
	                    // $client_params['file_path'] = APP . 'webroot/qrCode/'.$finalFileName;
	                    $client_params['file_type'] = 'application/pdf';
	                    $client_params['to_email'] = $email;
	                    $client_params['fname'] = $name;
	                    $client_params['quantity'] = $numberOfQrCode;
	                    $client_params['batch'] = $batchNumber;


	                    $sendMail = $this->InsEmail->sendQRCode( $client_params );
	                    // ** If mail send successfully, update table
	                    if(!empty($sendMail[0])){
	                        $company = $this->CompanyName->find('first',array('conditions'=>array('CompanyName.id'=>$userDetails['QrCodeRequest']['institute_id'])));
	                        $admin_params = array();
	                        $admin_params['file_name'] = $finalFileName;
	                        $admin_params['file_path']= base64_encode(file_get_contents(APP . 'webroot/qrCode/'.$finalFileName));
	                        // $admin_params['file_path'] = APP . 'webroot/qrCode/'.$finalFileName;
	                        $admin_params['file_type'] = 'application/pdf';
	                        $admin_params['to_email'] = 'ehteram@mediccreations.com';
	                        $admin_params['fname'] = 'Admin';
	                        $admin_params['quantity'] = $numberOfQrCode;
	                        $admin_params['batch'] = $batchNumber;
	                        $admin_params['trust'] = $company['CompanyName']['company_name'];
	                        $admin_params['date'] = date('d M Y');
	                        $sendMail = $this->InsEmail->sendQRCodeToAdmin( $admin_params );

	                        $this->QrCodeRequest->updateAll(array("status"=> 1), array("id"=> $requestId));
	                        $filePath = APP . 'webroot/qrCode/'.$finalFileName;
	                        if( !empty($filePath) && file_exists( $filePath )){
	                            chmod($filePath, 777);
	                            unlink( $filePath );
	                            $return = true;
	                        }
	                    }
	                }
	            }
	            for($j= 1; $j <= $numberOfQrCode; $j++){

	                $filePath = APP."webroot/qrCode/qrcode-$qrArray[$j].png";

	                if( !empty($filePath) && file_exists( $filePath )){
	                    chmod($filePath, 777);
	                    unlink( $filePath );
	                    $return = true;
	                }
	            }

	            $data['qr_type'] = 2;
	            $data['batch_number'] = $batchNumber;
	            $data['validity'] = $calValidity;
	            $data['qr_number'] = $numCodeTxt;
	            echo json_encode($data);
	        }
	    }

			try{
					// Admin activity logs [START]
					if($qr_type == 1){
						$generate_type = 'Display on Screen';
					}else{
						$generate_type = 'Email for printing';
					}
					$activityData = array();
					$activityData['company_id'] = $_COOKIE['adminInstituteId'];
					$activityData['admin_id'] = $_COOKIE['adminUserId'];
					$activityData['action'] = 'Generate QR Code';
					$activityData['custom_data'] = '{"generate_type" :"'.$generate_type.'","Cards" :"'.$numberOfQrCode.'","Validity" :"'.$calValidity.'"}';
					$this->TrustAdminActivityLog->addActivityLog($activityData);
					// Admin activity logs [END]
			} catch (Exception $e) {

			}
        exit();
    }

		public function removeQRImage(){
			$qr_img_name =  $this->request->data['qr_img'];
			$filePath = APP."webroot/qrCode/$qr_img_name";

			if( !empty($filePath) && file_exists( $filePath )){
					chmod($filePath, 777);
					unlink( $filePath );
					$return = true;
			}
			exit();
		}

		public function logout(){
      $this->layout = null;
      // $this->layout = 'InstitutionLogin';
      $this->autoRender = false;
        try{
            // Admin activity logs [START]
            $activityData = array();
            $activityData['company_id'] = $_COOKIE['adminInstituteId'];
            $activityData['admin_id'] = $_COOKIE['adminUserId'];
            $activityData['action'] = 'Logout';
            $this->TrustAdminActivityLog->addActivityLog($activityData);
            // Admin activity logs [END]
        } catch (Exception $e) {

        }
        $this->Session->destroy('adminEmail');
        $this->Session->destroy('adminInstituteId');
        $this->Session-> destroy();
				if (isset($_SERVER['HTTP_COOKIE'])) {
				    $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
				    foreach($cookies as $cookie) {
				        $parts = explode('=', $cookie);
				        $name = trim($parts[0]);
				        setcookie($name, '', time()-1000);
				        setcookie($name, '', time()-1000, '/');
				    }
				}
        $this->redirect('/institution');
        exit;
    }

		public function getUserProfileDetail(){
			$this->autoRender = false;
			if(isset($_COOKIE['adminInstituteId']) && null !== $_COOKIE['adminInstituteId'] && $_COOKIE['adminInstituteId'] !='' ){
				$companyId = $_COOKIE['adminInstituteId'];
				$groupAdmin = $_COOKIE['instituteGroupAdminId'];
				$userId = $this->request->data['user_id'];

				if(isset($this->data['layout'])){
				    $layout_view = $this->data['layout'];
				}else{
				    $layout_view = 'switch';
				}

				$options = array(
					'conditions' => array(
						'User.id' => $userId,
						'User.status' => 1,
						'User.approved' =>1,
						'UserDutyLog.user_id' => $userId,
					),
					'joins' => array(
						array(
							'table' => 'user_profiles',
							'alias' => 'userProfiles',
							'type' => 'left',
							'conditions'=> array('userProfiles.user_id = User.id')
						),
						array(
							'table' => 'user_duty_logs',
							'alias' => 'UserDutyLog',
							'type' => 'left',
							'conditions'=> array('User.id = UserDutyLog.user_id')
						),
						array(
							'table' => 'professions',
							'alias' => 'Professions',
							'type' => 'left',
							'conditions'=> array('userProfiles.profession_id = Professions.id')
						),
					),
					'fields' => array('
						userProfiles.user_id,
						User.email,
						userProfiles.first_name,
						userProfiles.last_name,
						userProfiles.profile_img,
						userProfiles.role_status,
						userProfiles.custom_role,
						userProfiles.custom_baton_roles,
						userProfiles.custom_permanent_role,
						userProfiles.profession_id,
						userProfiles.contact_no,
						UserDutyLog.atwork_status,
						UserDutyLog.status,
						Professions.profession_type
					'),
				);

				$userDetail = $this->User->find('first',$options);

				$options2 = array(
					'conditions' => array(
						"userBatonRoles.from_user"=> $userId,
						'userBatonRoles.is_active' => 1,
						// "userBatonRoles.status"=> array(1,3,5),
						"DepartmentsBatonRole.institute_id" => $companyId
					),
					'joins' => array(
						array(
							'table' => 'user_baton_roles',
							'alias' => 'userBatonRoles',
							'type' => 'left',
							'conditions'=> array('userBatonRoles.from_user = User.id')
						),
						array(
		          'table' => 'departments_baton_roles',
		          'alias' => 'DepartmentsBatonRole',
		          'type' => 'left',
		          'conditions'=> array('userBatonRoles.role_id = DepartmentsBatonRole.role_id')
		        ),
		        array(
		          'table' => 'baton_roles',
		          'alias' => 'BatonRole',
		          'type' => 'left',
		          'conditions'=> array('DepartmentsBatonRole.role_id = BatonRole.id'),
		          'group'=> array('userBatonRoles.role_id')
		        )
					),
					'fields' => array('
						userBatonRoles.id,
						userBatonRoles.status,
						userBatonRoles.role_id,
						BatonRole.baton_roles
					'),
					'group'=> 'userBatonRoles.role_id'
				);

				$batonRoleDetails = $this->User->find('all',$options2);
				//print_r($batonRoleDetails);

				if(!empty($userDetail)){
					$userdata['user_id'] = $userDetail['userProfiles']['user_id'];
					$userdata['user_name'] = $userDetail['userProfiles']['first_name'].' '.$userDetail['userProfiles']['last_name'];
					$userdata['email'] = $userDetail['User']['email'];
					$userdata['thumbnail_img'] = isset($userDetail['userProfiles']['profile_img'])?AMAZON_PATH.$userDetail['userProfiles']['user_id'].'/profile/'.$userDetail['userProfiles']['profile_img']:'';

					$status = '';

					if($userDetail['UserDutyLog']['status'] == 1){
						$status .= 'OnCall, ';
					}

					if(trim($userDetail['userProfiles']['role_status']) == ''){
						$status .= $userDetail['Professions']['profession_type'];
					}else{
						$status .= $userDetail['userProfiles']['role_status'];
					}

					$userBatonRoles = json_decode($userDetail['userProfiles']['custom_baton_roles'],true);
					if(!empty($userBatonRoles)){
						foreach ($userBatonRoles as $role1) {
							$status .= ', '.$role1['role_name'];
						}
					}

					$userRoles = json_decode($userDetail['userProfiles']['custom_role']);
					foreach ($userRoles as $role) {
						foreach ($role->value as $value) {
							if(in_array($value, array('--N/A--','-N/A','- Not Applicable'))){
								$str = explode("$",$value);
								$status .= ', ' . $str[0];
							}
						}
					}

					$userdata['user_status'] = $status;

					$cust_class = 'notAtWork';
					if((string)$userDetail['UserDutyLog']['status'] == '1'){
						$cust_class = 'onCallMain';
					}else if((string)$userDetail['UserDutyLog']['atwork_status'] == '0'){
						$cust_class = 'notAtWork';
					}else{
						$cust_class = '';
					}

					$userdata['cust_class'] = $cust_class;
				}

				$userBatonroleAssigned = array();
				$userBatonroleReqAwait = array();
				$userBatonroleTransOut = array();
				foreach ($batonRoleDetails as  $batonRole) {
					if($batonRole['userBatonRoles']['status'] == 1){ // Assigned
						$userBatonroleAssigned[] = array(
							'role_id' => $batonRole['userBatonRoles']['role_id'],
							'role_label' => $batonRole['BatonRole']['baton_roles'],
							'status' => $batonRole['userBatonRoles']['status'],
							'user_role_id' => $batonRole['userBatonRoles']['id']
						);
					}
					if($batonRole['userBatonRoles']['status'] == 3 || $batonRole['userBatonRoles']['status'] == 4 || $batonRole['userBatonRoles']['status'] == 2){ // Request Awaiting
						$userBatonroleReqAwait[] = array(
							'role_id' => $batonRole['userBatonRoles']['role_id'],
							'role_label' => $batonRole['BatonRole']['baton_roles'],
							'status' => $batonRole['userBatonRoles']['status'],
							'user_role_id' => $batonRole['userBatonRoles']['id']
						);
					}
					if($batonRole['userBatonRoles']['status'] == 5){ // Transfering Out
						$userBatonroleTransOut[] = array(
							'role_id' => $batonRole['userBatonRoles']['role_id'],
							'role_label' => $batonRole['BatonRole']['baton_roles'],
							'status' => $batonRole['userBatonRoles']['status'],
							'user_role_id' => $batonRole['userBatonRoles']['id']
						);
					}
				}

				$userBatonRoles = array_merge($userBatonroleAssigned, $userBatonroleReqAwait, $userBatonroleTransOut);

				if($layout_view == 'switch'){
					$this->set(array("userdata"=>$userdata,"userBatonRoles"=>$userBatonRoles));
					$this->render('/Elements/baton/user_baton_ajax');
				}else{
					$profile = array(
						'id' =>  $userDetail['userProfiles']['user_id'],
						'email' =>  $userDetail['User']['email'],
						'phone' =>  isset($userDetail['userProfiles']['contact_no']) ? $userDetail['userProfiles']['contact_no'] : 'Not Available',
						'UserName' => $userDetail['userProfiles']['first_name'].' '.$userDetail['userProfiles']['last_name'],
						'profile_img' => isset($userDetail['userProfiles']['profile_img'])?AMAZON_PATH.$userDetail['userProfiles']['user_id'].'/profile/'.$userDetail['userProfiles']['profile_img']:'',
						'role_status' => $status,
						'AtWorkStatus' =>  $userDetail['UserDutyLog']['atwork_status'],
						'OnCallStatus' =>  $userDetail['UserDutyLog']['status'],
						'cust_class' => $cust_class
					);
					$responseData['data'] = $profile;
					$responseData['status'] = 1;
					$responseData['message'] = 'ok.';
					echo json_encode($responseData);
				}

				try{
						$activityData = array();
						$activityData['company_id'] = $_COOKIE['adminInstituteId'];
						$activityData['admin_id'] = $_COOKIE['adminUserId'];
						$activityData['action'] = 'User Profile Detail Popup';
						$activityData['custom_data'] = 'Get';
						$this->TrustAdminActivityLog->addActivityLog($activityData);
						// Admin activity logs [END]
				} catch (Exception $e) {

				}

			}
		}

		public function addBatonRoleFileUpload(){
					App::import('Vendor','PHPExcel',array('file' => 'PHPExcel/Classes/PHPExcel.php'));
					App::import('Vendor','PHPExcel',array('file' => 'PHPExcel/Classes/IOFactory.php'));
					PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
	        $this->autoRender = false;
	        $responseData = array();
	        $response = array();
	        $validEmail = array();
	        $invalidEmail = array();
	        $createdDate = date("Y-m-d H:i:s");
					$batonRoleOnCall = '';
	        if($this->request->is('post')) {
	            $paramsInput  = $this->request->params;
	            $companyId = $_COOKIE['adminInstituteId'];
	            $file_name  = $paramsInput['form']['bantonfile']['name'];
	            $file_temp  = $paramsInput['form']['bantonfile']['tmp_name'];
	            $file_size  = $paramsInput['form']['bantonfile']['size'];
	            $file_error = $paramsInput['form']['bantonfile']['error'];
	            $file_ext = explode('.', $file_name);
	            $file_ext = strtolower(end($file_ext));
	            $allowed = array('csv', 'xls', 'xlsx');
	            if(in_array($file_ext, $allowed)){
	                if($file_error === 0){
	                    $file_new_name = uniqid('',true). '.' .$file_ext;
	                    $file_destination = APP .'/webroot/files/'.$file_new_name;
	                    if(move_uploaded_file($file_temp, $file_destination)){
	                       $fileName = $file_destination;
	                       if(!empty($fileName)){
	                            $row = 1;
	                            if (($handle = fopen($fileName , "r")) !== FALSE) {
																	$fieldEmpty = 0;

																	// xlsx and xls process
																	if($file_ext == 'xlsx' || $file_ext == 'xls'){
																		$objPHPExcel = PHPExcel_IOFactory::load($file_destination);
																		$sheet = $objPHPExcel->getSheet(0);
																		$highestRow = $sheet->getHighestRow();
																		$highestColumn = $sheet->getHighestColumn();
																		for ($row = 1; $row <= $highestRow; $row++){
																		    //  Read a row of data into an array
																		    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

																				if($row == 1) continue;

																				//$batonRoleName = $rowData[0][0];
																				$batonRoleNameOriginal = $rowData[0][0];
																				$batonRoleNameModified =  strip_tags($batonRoleNameOriginal);
																				$batonRoleName = trim(preg_replace("/(\r|\n)/", " ", $batonRoleNameModified));
																				$errorVal = false;
																				if(strtolower($rowData[0][1]) == 'yes'){
																					$batonRoleOnCall = 1;
																				}elseif(strtolower($rowData[0][1]) == 'no'){
																					$batonRoleOnCall = 0;
																				}else{
																					$errorVal = true;
																				}

																				if( !empty($batonRoleName) && $errorVal == false){
																					if( !empty($batonRoleName) && ($batonRoleOnCall == 1 || $batonRoleOnCall == 0) && !empty($companyId) ){
																						$roleTagData = $this->BatonRole->find('first',array(
																							'conditions'=>array(
																								'LOWER(BatonRole.baton_roles)' => strtolower($batonRoleName),
																								'BatonRole.is_active' => 1,
																								'DepartmentsBatonRole.institute_id' => $companyId
																							),
																							'joins'=>array(
																								array(
																									'table' => 'departments_baton_roles',
																									'alias' => 'DepartmentsBatonRole',
																									'type' => 'left',
																									'conditions'=> array('BatonRole.id = DepartmentsBatonRole.role_id')
																								)
																							)
																						));
																						if( !empty($roleTagData) ){
																							$DeptroleTagData = $this->DepartmentsBatonRole->find('first',array('conditions'=>array('DepartmentsBatonRole.role_id'=> $roleTagData['BatonRole']['id'],'DepartmentsBatonRole.is_active'=>1)));
																							if($roleTagData['BatonRole']['is_active'] == 1 && $DeptroleTagData['DepartmentsBatonRole']['is_active'] == 1){
																								$responseData['status'] = 0;
																								$responseData['message'] = 'Already Exists';
																								$alreadyExist[] = $batonRoleName;
																								//echo json_encode($responseData);
																							}else{
																								$this->BatonRole->updateAll(array('is_active' => 1),array('id'=> $roleTagData['BatonRole']['id']));
																								$this->DepartmentsBatonRole->updateAll(array('is_active' => 1),array('role_id'=> $roleTagData['BatonRole']['id']));
																								$responseData['status'] = 1;
																								$responseData['message'] = 'Baton Role Added Successfully';
																								//echo json_encode($responseData);
																								$addedSuccessfully[] = $batonRoleName;
																							}
																						}else{
																								$this->BatonRole->saveAll(
																									array(
																										'baton_roles'=> $batonRoleName,
																										"is_active"=> 1,
																										'created_at' => $createdDate
																									)
																								);
																								$roleId = $this->BatonRole->getLastInsertID();
																								$this->DepartmentsBatonRole->saveAll(
																									array(
																										'department_id' => 1,
																										"role_id" => $roleId,
																										'is_occupied' => 0,
																										'is_active' => 1,
																										// 'timeout' => ,
																										'institute_id' => $companyId,
																										'on_call_value' => $batonRoleOnCall,
																										'created_at' => $createdDate
																									)
																								);
																								$responseData['status'] = 1;
																								$responseData['message'] = 'Baton Role Added Successfully';
																								//echo json_encode($responseData);
																								$addedSuccessfully[] = $batonRoleName;
																						}
			                                    }
																				}else{
																					$fieldEmpty++;
																				}
		                                    if($row % 100 == 0) { sleep(1); }

																		}

																	}else{
		                                while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
		                                    $num = count($data);
		                                    $row++;
		                                    if($row == 2) continue;
		                                    //$batonRoleName = $data[0];
																				$batonRoleNameOriginal = $data[0];
																				$batonRoleNameModified =  strip_tags($batonRoleNameOriginal);
																				$batonRoleName = trim(preg_replace("/(\r|\n)/", " ", $batonRoleNameModified));
																				$errorVal = false;
																				if(strtolower($data[1]) == 'yes'){
																					$batonRoleOnCall = 1;
																				}elseif(strtolower($data[1]) == 'no'){
																					$batonRoleOnCall = 0;
																				}else{
																					$errorVal = true;
																				}

		                                    //check email exist
																				if( !empty($batonRoleName) && $errorVal == false){
																					if( !empty($batonRoleName) && ($batonRoleOnCall == 1 || $batonRoleOnCall == 0) && !empty($companyId) ){
																						$roleTagData = $this->BatonRole->find('first',array(
																							'conditions'=>array(
																								'LOWER(BatonRole.baton_roles)' => strtolower($batonRoleName),
																								'BatonRole.is_active' => 1,
																								'DepartmentsBatonRole.institute_id' => $companyId
																							),
																							'joins'=>array(
																								array(
																									'table' => 'departments_baton_roles',
																									'alias' => 'DepartmentsBatonRole',
																									'type' => 'left',
																									'conditions'=> array('BatonRole.id = DepartmentsBatonRole.role_id')
																								)
																							)
																						));
																						if( !empty($roleTagData) ){
																							$DeptroleTagData = $this->DepartmentsBatonRole->find('first',array('conditions'=>array('DepartmentsBatonRole.role_id'=> $roleTagData['BatonRole']['id'],'DepartmentsBatonRole.is_active'=>1)));
																							if($roleTagData['BatonRole']['is_active'] == 1 && $DeptroleTagData['DepartmentsBatonRole']['is_active'] == 1){
																								$responseData['status'] = 0;
																								$responseData['message'] = 'Already Exists';
																								$alreadyExist[] = $batonRoleName;
																								//echo json_encode($responseData);
																							}else{
																								$this->BatonRole->updateAll(array('is_active' => 1),array('id'=> $roleTagData['BatonRole']['id']));
																								$this->DepartmentsBatonRole->updateAll(array('is_active' => 1),array('role_id'=> $roleTagData['BatonRole']['id']));
																								$responseData['status'] = 1;
																								$responseData['message'] = 'Baton Role Added Successfully';
																								//echo json_encode($responseData);
																								$addedSuccessfully[] = $batonRoleName;
																							}
																						}else{
																								$this->BatonRole->saveAll(
																									array(
																										'baton_roles'=> $batonRoleName,
																										"is_active"=> 1,
																										'created_at' => $createdDate
																									)
																								);
																								$roleId = $this->BatonRole->getLastInsertID();
																								$this->DepartmentsBatonRole->saveAll(
																									array(
																										'department_id' => 1,
																										"role_id" => $roleId,
																										'is_occupied' => 0,
																										'is_active' => 1,
																										// 'timeout' => ,
																										'institute_id' => $companyId,
																										'on_call_value' => $batonRoleOnCall,
																										'created_at' => $createdDate
																									)
																								);
																								$responseData['status'] = 1;
																								$responseData['message'] = 'Baton Role Added Successfully';
																								//echo json_encode($responseData);
																								$addedSuccessfully[] = $batonRoleName;
																						}
			                                    }
																				}else{
																					$fieldEmpty++;
																				}
		                                    if($row % 100 == 0) { sleep(1); }
		                                }
																	}
	                                fclose($handle);
	                                $response['success'] = "File Uploaded Successfully";
	                                //$response['valid_email'] = $validEmail;
	                                $response['addedSuccessfully'] = count($addedSuccessfully);
	                                //$response['invalid_email'] = $invalidEmail;
	                                $response['alreadyExist'] = count($alreadyExist);
																	$response['emptyFields'] = $fieldEmpty;

	                    	    			$responseData['status'] = 1;
	                                $responseData['message'] = 'File Uploaded Successfully.';
	                                $responseData['data'] = array('addUserData'=> $response);
	                    			echo json_encode($responseData);

	                                try{
		                                // Admin activity logs [START]
		                	            $activityData = array();
		                	            $activityData['company_id'] = $_COOKIE['adminInstituteId'];
		                	            $activityData['admin_id'] = $_COOKIE['adminUserId'];
		                	            $activityData['action'] = 'Add BatonRole (Bulk)';
		                	            $activityData['custom_data'] = '{"addedSuccessfully" :"'.json_encode($addedSuccessfully).'","alreadyExist" :"'.json_encode($alreadyExist).'","emptyFields" :"'.$fieldEmpty.'"}';
		                	            $this->TrustAdminActivityLog->addActivityLog($activityData);
		                	            // Admin activity logs [END]
		                	        } catch (Exception $e) {

		                	        }
	                            }
	                       }else{
	               		    	$responseData['status'] = 0;
	               	            $responseData['message'] = 'Please Upload Valid File.';
	               	            echo json_encode($responseData);
	                       }

									 			//$filePath = APP."webroot/qrCode/$qr_img_name";

									 			if( !empty($file_destination) && file_exists( $file_destination )){
									 					chmod($file_destination, 777);
									 					unlink( $file_destination );
									 					$return = true;
									 			}

	                    }else{
	        		    	$responseData['status'] = 0;
	        	            $responseData['message'] = 'Some Error Occured In Uploading File. Please Try Again.';
	        	            echo json_encode($responseData);
	                    }
	                }else{
	                	$responseData['status'] = 0;
	    	            $responseData['message'] = 'Please Upload Valid File.';
	    	            echo json_encode($responseData);
	                }
	            }else{
		        	$responseData['status'] = 0;
		            $responseData['message'] = 'Please Upload Valid File.';
		            echo json_encode($responseData);
	            }
	        }else{
	        	$responseData['status'] = 0;
	            $responseData['message'] = 'Information not provided.';
	            echo json_encode($responseData);
	        }
	    }
}
