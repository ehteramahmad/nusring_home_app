<?php

class InstitutionGroupController extends InstitutionAppController {
    public $components = array('RequestHandler','Paginator','Session', 'Institution.InsCommon', 'Institution.InsEmail', 'Image', 'Quickblox', 'Cache');

    public $uses = array('EmailTemplate','Institution.User','Institution.EnterpriseUserList','Institution.UserEmployment','Institution.AdminUser','Institution.CompanyName','Institution.UserSubscriptionLog','Institution.NotificationBroadcastMessage','Institution.UserQbDetail','Institution.UserDutyLog','Institution.UserOneTimeToken','Institution.CacheLastModifiedUser','Institution.ExportChatTransaction','Institution.UserProfile','Institution.QrCodeRequest','Institution.Profession','Institution.RoleTag','Institution.AvailableAndOncallTransaction','Institution.TrustAdminActivityLog');
    public $helpers = array('Form', 'Html', 'Js', 'Time','Paginator');

    public function index(){
      $this->checkAdminRole();
        // Admin activity logs [START]
        $activityData = array();
        $activityData['company_id'] = $_COOKIE['adminInstituteId'];
        $activityData['admin_id'] = $_COOKIE['adminUserId'];
        $activityData['action'] = 'Admin Group Dialog';
        $activityData['custom_data'] = 'view';
        $this->TrustAdminActivityLog->addActivityLog($activityData);
        // Admin activity logs [END]
    }

    public function showDialogues() {
        $this->autoRender = false;
        $responseData = array();
        $customData = array();
        $is_import = "true";
        if($this->request->is('post')) {

            $companyId = $_COOKIE['adminInstituteId'];
            $adminEmail = $_COOKIE['adminEmail'];
            $userId = $_COOKIE['instituteGroupAdminId'];

            // $inputFromDate = $this->request->data['fromDate'];
            // $inputToDate = $this->request->data['toDate'];
            // $reason = $this->request->data['reason'];

            $userData = $this->User->find("first", array("conditions"=> array("User.id"=> $userId)));
            $loggedinUserCountryId = $userData['UserProfile']['country_id'];
            if($loggedinUserCountryId==226){
                // date_default_timezone_set("Europe/London");
                date_default_timezone_set("Europe/London");
            }else if($loggedinUserCountryId==99){
                date_default_timezone_set('Asia/Kolkata');
            }

            if(!empty($userData['User']['email'])){

                $oneTimeToken = $this->InsCommon->genrateRandomToken($userId);
                $oneTimeTokenData = array("user_id"=> $userId, "token"=> $oneTimeToken);
                $this->UserOneTimeToken->saveAll($oneTimeTokenData);

                $tokenDetails = $this->Quickblox->quickLogin($userData['User']['email'], $oneTimeToken);
                // $tokenDetails = $this->Quickblox->quickLogin($userData['User']['email'], '12345678');
                $token = $tokenDetails->session->token;
                $user_id = $tokenDetails->session->user_id;

                $dialogs = $this->Quickblox->quickGetDialogLite($token, '', '');
                $dialog_data = array();
                foreach ($dialogs->items AS  $dialog) {
                  if($dialog->last_message != ''){
                    $custom_class = 'normalMain';
                    $image_url = '';
                    $is_image = '';

                    if(isset($dialog->photo) && (strpos($dialog->photo, 'http') !== false)){
                        $image_url = $dialog->photo;
                        $is_image = 'image';
                    }
                    if($dialog->type == '2'){
                        if(!empty($dialog->data) && $dialog->data != null && isset($dialog->data->Patient_name)){
                            $custom_class = 'patientMain';
                        }
                        if(isset($dialog->photo) && (strpos($dialog->photo, 'http') !== false)){
                            $image_url = $dialog->photo;
                            $is_image = 'image';
                        }else{
                            if($custom_class == 'patientMain'){
                                $image_url = BASE_URL.'institution/img/patientDefaultIcon.svg';
                                $is_image = 'image';
                            }else{
                                $image_url = BASE_URL.'institution/img/ava-group.png';
                                $is_image = 'image';
                            }
                        }
                    }
                    $dialog_data[] = array(
                        'dialog_name' => $dialog->name,
                        'image' => $image_url,
                        'custom_img_class' => $is_image,
                        'last_message' => $this->InsCommon->decryptData($dialog->last_message),
                        // 'last_message' => $dialog->last_message,
                        'message_time' => $dialog->last_message_date_sent,
                        'occupants' => (string)join(',',$dialog->occupants_ids),
                        'dialog_id' => (string)$dialog->_id,
                        'created_by' => (string)$dialog->user_id,
                        'pat_class' => $custom_class,
                    );
                  }
                }
                $this->set(array("data"=>$dialog_data));
                $this->render('/Elements/user/dialog_list_custom');


            }else{
                $responseData['status'] = 0;
                $responseData['message'] = 'User is not active.';
                echo json_encode($responseData);

            }
        }else{
            $responseData['status'] = 0;
            $responseData['message'] = 'Some technical error occurred. Please try again.';
            echo json_encode($responseData);

        }
    }

    public function exportForm($dialog_id){
      $this->checkAdminRole();
        // Admin activity logs [START]
        $activityData = array();
        $activityData['company_id'] = $_COOKIE['adminInstituteId'];
        $activityData['admin_id'] = $_COOKIE['adminUserId'];
        $activityData['action'] = 'Dialog click to export (Export form)';
        $activityData['custom_data'] = '{"dialog_id" :"'.$dialog_id.'"}';
        $this->TrustAdminActivityLog->addActivityLog($activityData);
        // Admin activity logs [END]

        $this->set(array('dialogue_id'=>$dialog_id));
    }

    public function exportFormPdf($dialog_id){
        if(isset($dialog_id) && $_COOKIE['from_date']){

            $inputFromDate = $_COOKIE['from_date'];
            $inputToDate = $_COOKIE['to_date'];
            $reason = (string)$_COOKIE['reason'];

            $companyId = $_COOKIE['adminInstituteId'];
            $adminEmail = $_COOKIE['adminEmail'];
            $userId = $_COOKIE['instituteGroupAdminId'];

            $userData = $this->User->find("first", array("conditions"=> array("User.id"=> $userId)));
            if(!empty($userData['User']['email'])){
                $userDetails = $this->User->find("first", array("conditions"=> array("User.email"=> $adminEmail)));

                $title = $userDetails['UserProfile']['first_name'];
                // $title = $userData['UserProfile']['first_name'].' '.$userData['UserProfile']['last_name'];
                $chat_user_id = $userData['UserProfile']['user_id'];
                // $user_country_id = $userData['UserProfile']['country_id'];
                $user_country_id = $_COOKIE['adminCountryId'];
                $oneTimeToken = $this->InsCommon->genrateRandomToken($userId);
                $oneTimeTokenData = array("user_id"=> $userId, "token"=> $oneTimeToken);
                $this->UserOneTimeToken->saveAll($oneTimeTokenData);

                $tokenDetails = $this->Quickblox->quickLogin($userData['User']['email'], $oneTimeToken);
                // $tokenDetails = $this->Quickblox->quickLogin($userData['User']['email'], '12345678');
                $token = $tokenDetails->session->token;
                $user_id = $tokenDetails->session->user_id;

                $dialogs = $this->Quickblox->quickGetDialogData($token,$dialog_id);
                $dialog_data = array();
                $occupantsId = '';
                $chatCreatedBy = '';
                $dialogue_cust_data = [];
                foreach ($dialogs->items AS  $dialog) {
                    $dialogue_name = $dialog->name;
                    $occupantsId = $dialog->occupants_ids;
                    $chatCreatedBy = (string)$dialog->user_id;
                    if(isset($dialog->data)){
                        $dialogue_cust_data = $dialog->data;
                    }
                }

                $patient_details = $this->formatPatientdata($dialogue_cust_data);

                $response_data = $this->getuserMessage($chatCreatedBy,$occupantsId,$token,$dialog_id,$inputFromDate, $inputToDate,$patient_details,$user_country_id);

                $transactionData = array(
                    'user_id_from'=>$userId,
                    'user_id_to'=>$userId,
                    'company_id'=>$companyId,
                    'from_date'=>date('y-m-d H:i:s',$inputFromDate),
                    'to_date'=>date('y-m-d H:i:s',$inputToDate),
                    'dialog_id'=>$dialog_id,
                    'export_type'=>1,
                    'reason'=>$reason,
                    'mail_sent'=>0
                );
                $transaction = $this->ExportChatTransaction->saveAll($transactionData);
                $transaction_id = $this->ExportChatTransaction->getLastInsertId();

                // Admin activity logs [START]
                $activityData = array();
                $activityData['company_id'] = $_COOKIE['adminInstituteId'];
                $activityData['admin_id'] = $_COOKIE['adminUserId'];
                $activityData['action'] = 'Chat messages display';
                $activityData['custom_data'] = '{"dialog_id" :"'.$dialog_id.'","from" :"'.date('y-m-d H:i:s',$inputFromDate).'","to" :"'.date('y-m-d H:i:s',$inputToDate).'"}';
                $this->TrustAdminActivityLog->addActivityLog($activityData);
                // Admin activity logs [END]

                $this->set(array("data"=>$response_data,'title'=>$title,'user_id'=>$chat_user_id,'from_date' =>$inputFromDate,'to_date' => $inputToDate,'dialogue_name' =>$dialogue_name,'dialog_id'=>$dialog_id,'transaction_id'=>$transaction_id));
            }else{
                $this->redirect(BASE_URL . 'institution/InstitutionGroup/exportForm/'.$dialog_id);
            }
        }else{
            $this->redirect(BASE_URL . 'institution/InstitutionGroup/exportForm/'.$dialog_id);
        }
    }

    public function getuserMessage($chatCreatedBy,$occupantsId,$token,$dialogue_id,$inputFromDate, $inputToDate,$patient_details,$loggedinUserCountryId){
        //** Chat Created By Id[START]
        $companyId = $_COOKIE['adminInstituteId'];
        $groupAdmin = $_COOKIE['instituteGroupAdminId'];
        $userByQbId = $this->UserQbDetail->find("first", array("conditions"=> array("qb_id"=> $chatCreatedBy)));
            if(!empty($userByQbId)){
                $chatCreatedByUserId = $userByQbId["UserQbDetail"]['user_id'];
            }
        //** Chat Created By Id[END]
            if($loggedinUserCountryId==226){
                // date_default_timezone_set('Europe/Amsterdam');
                date_default_timezone_set("Europe/London");
            }else if($loggedinUserCountryId==99){
                date_default_timezone_set('Asia/Kolkata');
            }

        //** Chat Participants details[START]
        $participantIdsData = $this->UserQbDetail->find("list", array("fields"=>array("user_id"),"conditions"=> array("qb_id"=>$occupantsId)));

        // return $participantIdsData;

        $participantIds = array_unique($participantIdsData);

        //** Created By Institution[START]
        $userCurrentCompanyName = "";
        if($companyId == -1){
            $userCurrentCompanyName = "None";
        }else if(!empty($companyId)){
            $userCompanyDetails = $this->CompanyName->find("first", array("conditions"=> array("id"=> $companyId, "status"=> 1, "created_by"=> 0)));
            if(!empty($userCompanyDetails)){
                $userCurrentCompanyName = $userCompanyDetails['CompanyName']['company_name'];
            }
        }
        //** Created By Institution[END]

        $roleStatus = '';
        $count = 1;
        $participantIdsArr = [];
        $newArr = array();
        foreach ($participantIds AS  $participantId) {
            if($groupAdmin != $participantId){
                $chatParticipantsDetails = $this->getParticipentDetail($participantId);
                if(!empty($chatParticipantsDetails)){
                    if(!empty($chatParticipantsDetails['UserProfile']['role_status']))
                    {
                        $roleStatus = $chatParticipantsDetails['UserProfile']['role_status'];
                    }
                    else if(!empty($chatParticipantsDetails['Profession']['profession_type']))
                    {
                        $roleStatus = $chatParticipantsDetails['Profession']['profession_type'];
                    }
                    else
                    {
                        $roleStatus = "NA";
                    }
                }
                $newArr[] = array('first_name'=> $chatParticipantsDetails['UserProfile']['first_name'], 'last_name'=> $chatParticipantsDetails['UserProfile']['last_name'],'role_status'=>$roleStatus);
            }
        }

        $chatMessageDetail = $this->Quickblox->quickGetMessageLite($token, $dialogue_id, $inputFromDate, $inputToDate);
        $messageResult = json_decode(json_encode($chatMessageDetail));
        $messageArr = array();
        // print_r($messageResult->items);
        foreach ($messageResult->items as  $message) {
            date_default_timezone_set('UTC');
            $created_at = strtotime($message->created_at);
            if($loggedinUserCountryId==226){
                // date_default_timezone_set('Europe/Amsterdam');
                date_default_timezone_set("Europe/London");
            }else if($loggedinUserCountryId==99){
                date_default_timezone_set('Asia/Kolkata');
            }
            $created_at = date("d-m-Y", $created_at);
            $senderName = "";
            $userDetailsSender = $this->userDetailsByQbId($message->sender_id);
            if(!empty($userDetailsSender)){
                $senderName = $userDetailsSender["UserProfile"]['first_name'];
            }

            $messages = "";
            if( !empty($message->message)){
                if(!empty($message->notification_type)){
                    $messages = $this->formatNotification($message);
                }else{
                    $messages = $this->formatMessage($message);
                }
                $timeStamp = date("H:i", $message->date_sent);
                $messageArr[] = array('id'=>$message->sender_id,'created_date'=> $created_at, 'sender_name'=> $senderName,'messages'=>$messages,'timeStamp'=>$timeStamp);
            }
        }

        $responseData = array('message_data'=> $messageArr,'occupants_data'=>$newArr, 'current_company'=>$userCurrentCompanyName,'patient_data'=>$patient_details);
        return $responseData;
    }

    public function userDetailsByQbId($qbId=NULL){
        $userDetails = array();
        if(!empty($qbId)){
            $userByQbId = $this->UserQbDetail->find("first", array("conditions"=> array("qb_id"=> $qbId)));
            if(!empty($userByQbId['UserQbDetail']['user_id'])){
                $userDetails = $this->User->userDetailsById($userByQbId['UserQbDetail']['user_id']);
            }
        }
        return $userDetails;
    }

    public function formatPatientdata($cust_data){
        $patientData = array();
        if(!empty($cust_data) && $cust_data != null && isset($cust_data->Patient_name)){
            $patientData = array(
                'name' => $this->InsCommon->decryptData($cust_data->Patient_name),
                'nhs_number' => $this->InsCommon->decryptData($cust_data->Patient_nhs_number),
                'age' => $this->InsCommon->decryptData($cust_data->Patient_age),
                'gender' => $this->InsCommon->decryptData($cust_data->Patient_gender),
                'notes' => isset($cust_data->Patient_condition)?$this->InsCommon->decryptData($cust_data->Patient_condition):'',
                'patient_number_type' => isset($cust_data->Patient_number_type)?$cust_data->Patient_number_type:0,
                'patient_dob' => isset($cust_data->Patient_dob)?$this->InsCommon->decryptData($cust_data->Patient_dob):''

                // 'name' => $cust_data->Patient_name,
                // 'nhs_number' => $cust_data->Patient_nhs_number,
                // 'age' => $cust_data->Patient_age,
                // 'gender' => $cust_data->Patient_gender,
                // 'notes' => isset($cust_data->Patient_condition)?$cust_data->Patient_condition:''
            );
        }
        return $patientData;
    }

    public function formatNotification($message=NULL){
        $returnMsg = "Notification Message";
        $contact_custom_msg = '';
        $notifyType = $message->notification_type;
        if(isset($message->contact_custom_msg) && $message->contact_custom_msg != ''){
            $contact_custom_msg = $message->contact_custom_msg;
        }
        if(!empty($notifyType)){
            switch($notifyType){
                case 1:
                        $returnMsg = "Group Created";
                        break;
                case 2:
                        $returnMsg = "Group Updated";
                        break;
                case 4:
                        $returnMsg = "Contact Request<br />".$contact_custom_msg;
                        break;
                case 5:
                        $returnMsg = "Contact Request Accepted";
                        break;
                case 6:
                        $returnMsg = "Contact Request Rejected";
                        break;
                case 7:
                        $returnMsg = "Contact Request Removed";
                        break;
                case 101:
                        $returnMsg = "Missed Call";
                        break;
                case 102:
                        $returnMsg = "Call Not Responded";
                        break;
                case 103:
                        $returnMsg = "Call Rejected";
                        break;
                case 104:
                        $returnMsg = "Missed Audio Call";
                        break;
                case 105:
                        $returnMsg = "Audio Call Not Responded";
                        break;
                case 106:
                        $returnMsg = "Missed Video Call";
                        break;
                case 107:
                        $returnMsg = "Video Call Not Responded";
                        break;
                case 108:
                        $returnMsg = "Took a screenshot of this chat.";
                        break;
                default:
                        $returnMsg = "Notification Message";
                        break;
            }
        }
        return $returnMsg;
    }

    public function formatMessage($message = null) {
        $forward_message_data = array();
        $reply_message_data = array();
        $senderName = '';
        $forward_user = '';
        $forward_attac = '';
        $messages = "";

        $userDetailsSender = $this->userDetailsByQbId($message->sender_id);
        if(!empty($userDetailsSender)){
            $senderName = $userDetailsSender["UserProfile"]['first_name'];
        }

        if(isset($message->is_forwarded_msg) && $message->is_forwarded_msg == 'Yes'){
            $forward_message_data = json_decode($message->forwarded_msg_user_info,true);
        }

        if(isset($message->is_reply_msg) && $message->is_reply_msg == 'Yes'){
            $reply_message_data = json_decode($message->reply_msg_user_info,true);
            if(isset($reply_message_data['reply_attachment']) && gettype($reply_message_data['reply_attachment'])=='string'){
                $reply_message_data['reply_attachment'] = json_decode($reply_message_data['reply_attachment'],true);
            }
        }

        if(!empty($forward_message_data)){
            if((string)$message->sender_id == (string)$forward_message_data['forward_msg_user_id']){
                $forward_user = 'own';
            }else{
                $forward_user = $forward_message_data['forward_msg_sender_name']."'s";
            }

            if(count($message->attachments) > 0){
                $forward_attac_type = $this->getAttachmentType($message->attachments[0]->type);
                $forward_attac_url = QB_API_ENDPOINT.'blobs/'.$message->attachments[0]->id;
                $forward_message = '<a target="_blank" href="'.$forward_attac_url.'">'.$forward_attac_type.'</a>';
            }else{
                $forward_message = $this->InsCommon->decryptData($forward_message_data['forward_msg_message']);
                // $forward_message = $forward_message_data['forward_msg_message'];
            }

            if(isset($message->isEncrypted) && $message->isEncrypted=='Yes'){
                $main_message = $this->InsCommon->decryptData($message->message);
                // $main_message = $message->message;
            }else{
                $main_message = $message->message;
            }

            $messages = $senderName.' forwarded '.$forward_user." message<br>".$forward_message."<br><br>".$senderName."'s Message: ".$main_message;
        }else if(!empty($reply_message_data)){
            if((string)$message->sender_id == (string)$reply_message_data['reply_msg_user_id']){
                $reply_user = 'own';
            }else{
                $reply_user = $reply_message_data['reply_msg_sender_name']."'s";
            }

            if(count($message->attachments) > 0){
                $reply_main_attac_type = $this->getAttachmentType($message->attachments[0]->type);
                $reply_main_attac_url = QB_API_ENDPOINT.'blobs/'.$message->attachments[0]->id;
                $main_message = '<a target="_blank" href="'.$reply_main_attac_url.'">'.$reply_main_attac_type.'</a>';
            }else{
                if(isset($message->isEncrypted) && $message->isEncrypted=='Yes'){
                    $main_message = $this->InsCommon->decryptData($message->message);
                    // $main_message = $message->message;
                }else{
                    $main_message = $message->message;
                }
            }

            if(isset($reply_message_data['reply_attachment']) && $reply_message_data['reply_is_media'] == 'Yes'){
                $reply_attac_type = $this->getAttachmentType($reply_message_data['reply_attachment']['type']);
                $reply_attac_url = QB_API_ENDPOINT.'blobs/'.$reply_message_data['reply_attachment']['id'];
                $reply_message = '<a target="_blank" href="'.$reply_attac_url.'">'.$reply_attac_type.'</a>';
                // $reply_message = '';
            }else{
                $reply_message = $this->InsCommon->decryptData($reply_message_data['reply_msg_message']);
                // $reply_message = $reply_message_data['reply_msg_message'];
            }

            $messages = $senderName.' replied '.$reply_user." message<br>".$reply_message."<br><br>".$senderName."'s Reply: ".$main_message;
        }else{
            if(count($message->attachments) > 0){
                $attac_type = $this->getAttachmentType($message->attachments[0]->type);
                $attac_url = QB_API_ENDPOINT.'blobs/'.$message->attachments[0]->id;
                $messages = '<a target="_blank" href="'.$attac_url.'">'.$attac_type.'</a>';
            }else{
                if(isset($message->isEncrypted) && $message->isEncrypted=='Yes'){
                    $messages = $this->InsCommon->decryptData($message->message);
                    // $messages = $message->message;
                }else{
                    $messages = $message->message;
                }
            }
        }
        return $messages;
    }

    public function getAttachmentType($attachment_type){

        $attachment_type = strtolower($attachment_type);

        if($attachment_type=="video"){
            $attachmentName = "Video Attachment";
        }else if($attachment_type=="mp4"){
            $attachmentName = "Video Attachment";
        }else if($attachment_type=="aac"){
            $attachmentName = "Audio Attachment";
        }else if($attachment_type=="mp3"){
            $attachmentName = "Audio Attachment";
        }else if($attachment_type=="audio"){
            $attachmentName = "Audio Attachment";
        }else if($attachment_type=="photo"){
            $attachmentName = "Image Attachment";
        }else if($attachment_type=="image"){
            $attachmentName = "Image Attachment";
        }else if($attachment_type=="attachment image"){
            $attachmentName = "Image Attachment";
        }else if(in_array($attachment_type,array("pdf","doc","docx","xls","xlsx","ppt","pptx"))){
            $attachmentName = "Document Attachment";
        }else{
            $attachmentName = "Attachment";
        }
        return $attachmentName;
    }

    public function getParticipentDetail($participantId){
        $conditions = "UserProfile.user_id = $participantId";
        $userLists = $this->UserProfile->find('first',
                        array(
                            'joins'=>
                            array(
                                array(
                                    'table' => 'professions',
                                    'alias' => 'Profession',
                                    'type' => 'left',
                                    'conditions'=> array('UserProfile.profession_id = Profession.id')
                                ),
                            ),
                            'conditions' => $conditions,
                            'fields' => array('UserProfile.first_name', 'UserProfile.last_name','UserProfile.role_status', 'Profession.profession_type')
                        )
                    );
        return  $userLists;
    }

    public function checkForm(){
        $this->autoRender = false;
        $responseData = array();
        $adminEmail = $_COOKIE['adminEmail'];
        $password = $this->request->data['password'];
        $params = array();
        $params['userName'] = strtolower($adminEmail);
        $params['password'] = $password;
        $adminData = $this->AdminUser->checkLogin($params);

        // $adminData = $this->AdminUser->find('first', array('conditions'=> array('username'=> $adminEmail,'password'=> md5($password),'status'=> 1)));
        if(!empty($adminData)){
            $responseData['status'] = 1;
            $responseData['message'] = 'ok.';
            echo json_encode($responseData);
        }else{
            $responseData['status'] = 0;
            $responseData['message'] = 'Incorrect password.';
            echo json_encode($responseData);
        }
    }

    public function broadCastMessage() {
        $this->autoRender = false;
        $responseData = array();

        $companyId = $_COOKIE['adminInstituteId'];
        $userId = $_COOKIE['instituteGroupAdminId'];
        $email = $_COOKIE['adminEmail'];
        $password = $this->request->data['password'];
        $message = $this->request->data['message'];
        $dialog_id = (string)$this->request->data['dialogue_id'];
        $dialog_name = $this->request->data['dialogue_name'];

        $serviceId = 'MG0490a00ed855239165604a73dda63967';

        $params = array();
        $params['userName'] = strtolower($email);
        $params['password'] = $password;
        $userData = $this->AdminUser->checkLogin($params);

        if(!empty($userData)){
            $userDetails = $this->User->find('first',array('conditions'=>array('User.id'=>$userId)));
            $oneTimeToken = $this->InsCommon->genrateRandomToken($userId);
            $oneTimeTokenData = array("user_id"=> $userId, "token"=> $oneTimeToken);
            $this->UserOneTimeToken->saveAll($oneTimeTokenData);

            $tokenDetails = $this->Quickblox->quickLogin($userDetails['User']['email'], $oneTimeToken);
            $token = $tokenDetails->session->token;
            $user_id = $tokenDetails->session->user_id;

            $dialogs = $this->Quickblox->quickGetDialogData($token,$dialog_id);
            $occupantsId = array();
            foreach ($dialogs->items AS  $dialog) {
                $occupantsId = $dialog->occupants_ids;
            }

            if(count($occupantsId) < 2){
               $responseData['status'] = 2;
               $responseData['message'] = 'No members to send Broadcast message.';
               // echo json_encode($responseData);
            }else{
                $conditions = array(
                    'UserQbDetail.qb_id' => $occupantsId,
                    'UserEmployment.company_id' => $companyId,
                    'UserEmployment.is_current' => 1,
                    'UserEmployment.status' => 1,
                    'User.status' => 1,
                    'User.approved' => 1,
                    'LOWER(VoipPushNotification.application_type)' => 'mb',
                    'VoipPushNotification.status' => 1,
                    'EnterpriseUserList.status' => array(1)
                );

                $joins = array(
                    array(
                        'table' => 'user_employments',
                        'alias' => 'UserEmployment',
                        'type' => 'left',
                        'conditions'=> array('UserEmployment.user_id = User.id')
                    ),
                    array(
                        'table' => 'user_qb_details',
                        'alias' => 'UserQbDetail',
                        'type' => 'left',
                        'conditions'=> array('User.id = UserQbDetail.user_id')
                    ),
                    array(
                        'table' => 'user_profiles',
                        'alias' => 'userProfiles',
                        'type' => 'left',
                        'conditions'=> array('userProfiles.user_id = User.id')
                    ),
                    array(
                        'table' => 'professions',
                        'alias' => 'Professions',
                        'type' => 'left',
                        'conditions'=> array('userProfiles.profession_id = Professions.id')
                    ),
                    array(
                        'table' => 'enterprise_user_lists',
                        'alias' => 'EnterpriseUserList',
                        'type' => 'left',
                        'conditions'=> array('User.email = EnterpriseUserList.email', 'UserEmployment.company_id = EnterpriseUserList.company_id')
                    ),
                    array(
                        'table' => 'voip_push_notifications',
                        'alias' => 'VoipPushNotification',
                        'type' => 'left',
                        'conditions'=> array('User.id = VoipPushNotification.user_id')
                    )
                );

                $options = array(
                    'conditions' => $conditions,
                    'joins' => $joins,
                    'fields' => array('User.id','UserQbDetail.qb_id','VoipPushNotification.device_token','VoipPushNotification.device_token'),
                    'group' => array('UserEmployment.user_id'),
                );

                $id_data = $this->User->find('all',$options);
                $broadcastIds = array();
                $broadcastUsers = array();
                $token_data = array();
                $smsIds = array();
                if(empty($id_data)){
                  $responseData['status'] = 2;
                  $responseData['message'] = 'No members to send Broadcast message.';
                }
                foreach ($id_data AS  $ids) {
                    if(!empty($ids['VoipPushNotification']) && isset($ids['VoipPushNotification']['device_token'])){
                        if($ids['UserQbDetail']['qb_id'] != "" && $ids['UserQbDetail']['qb_id'] != null){
                            $broadcastIds[] = $ids['UserQbDetail']['qb_id'];
                            $broadcastUsers[] = $ids['User']['id'];
                            $token_data[] = $ids['VoipPushNotification'];
                        }
                    }else{
                        $smsIds[] = $ids['User']['id'];
                    }
                }

                if(count($broadcastUsers) > 0){
                  foreach ($token_data as $value) {
                    $params = array();

                    $params['notificationMessage'] = $message;
                    $params['voip_tokens']['VoipPushNotification'] = $value;
                    $this->sendCustomVoipPush($params);

                  }
                  $responseData['status'] = 1;
                  $responseData['message'] = 'OK';

                  $sentFrom['dialog_id'] = $dialog_id;
                  $sentFrom['dialog_name'] = $dialog_name;

                  $notificationSendResponse = array(
                    "sender"=>$email,
                    "send_to"=>serialize($broadcastUsers),
                    "trust_id"=>$companyId,
                    "sent_messages"=>$message,
                    "type"=>"broadcast",
                    "sent_from"=>json_encode($sentFrom),
                    "response"=>'Broadcast sent'
                  );

                  try{
                   $this->NotificationBroadcastMessage->saveAll($notificationSendResponse);
                  }catch(Exception $e){}
                }
// ------ Response when SMS service are not available --------
                else{
                  $responseData['status'] = 2;
                  $responseData['message'] = 'No members to send Broadcast message.';
                }
// ------ Response when SMS service are not available --------
                if(count($smsIds) > 1){
                    $numbers = $this->UserProfile->find('all',array('conditions'=>array('UserProfile.user_id' => $smsIds),'fields'=>array('UserProfile.contact_no','UserProfile.user_id')));

                    $deliver_status['success'] = 0;
                    $deliver_status['success_users'] = array();
                    $deliver_status['failure'] = 0;
                    $deliver_status['failure_users'] = array();

                    foreach ($numbers as $value) {
                        if($value['UserProfile']['contact_no'] != ''){
                            $smsUsers[] = $value['UserProfile']['user_id'];
                            $data = json_decode(json_decode($this->sendSms($value['UserProfile']['contact_no'], $message, $serviceId)));
                            if($data->status == 'accepted'){
                                $response = json_decode(json_decode($this->getSmsResponse($data->sid)));
                                if($response->status != 'failed'){
                                  $deliver_status['success'] += 1;
                                  $deliver_status['success_users'][] = $value['UserProfile']['user_id'];
                                }else{
                                  $deliver_status['failure'] += 1;
                                  $deliver_status['failure_users'][] = $value['UserProfile']['user_id'];
                                }
                            }else{
                                $deliver_status['failure'] += 1;
                                $deliver_status['failure_users'][] = $value['UserProfile']['user_id'];
                            }
                        }
                    }

                    $sentFrom['dialog_id'] = $dialog_id;
                    $sentFrom['dialog_name'] = $dialog_name;

                    $messageResponse = array("sender"=>$email,"send_to"=>serialize($smsUsers),"trust_id"=>$companyId,"sent_messages"=>$message,"type"=>"message","sent_from"=>json_encode($sentFrom),"deliver_status"=>json_encode($deliver_status),"response"=>"Message sent");
                    try{
                     $this->NotificationBroadcastMessage->saveAll($messageResponse);
                    }catch(Exception $e){}

                    $responseData['status'] = 1;
                    $responseData['message'] = 'OK';
                }

                echo json_encode($responseData);

                try {
                    // Admin activity logs [START]
                    $activityData = array();
                    $activityData['company_id'] = $_COOKIE['adminInstituteId'];
                    $activityData['admin_id'] = $_COOKIE['adminUserId'];
                    $activityData['action'] = 'Broadcast Message';
                    $activityData['custom_data'] = '{"dialog_id" :"'.$dialog_id.'"}';
                    $this->TrustAdminActivityLog->addActivityLog($activityData);
                    // Admin activity logs [END]
                } catch (Exception $e) {

                }

            }
        }else{
            $responseData['status'] = 0;
            $responseData['message'] = 'Invalid Credentials.';
            echo json_encode($responseData);
        }

    }

    public function sendNotification($params = array()){
      $messageArr = array(
                            "message"=> $params['notificationMessage'],
                            "aps"=> array("alert"=> $params['notificationMessage'], "sound"=> "notification_sound.mp3"),
                            "is_broadcast"=> 1,
                            "show_alert"=>1,
                            "tap_to_reply" => 0,
                        );
      $messageEncoded = base64_encode(json_encode($messageArr));
      $postData=json_encode(array("event"=> array("notification_type"=> "push", "environment"=> (ENVIRONMENT == "live" ? "production" : "development"), "user"=> $params['recipients'], "message"=> $messageEncoded)));
      $curl = curl_init();
       curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT.'events.json');
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'QuickBlox-REST-API-Version: 0.1.0',
                    'QB-Token: ' . $params['qbtoken']
                ));
        $response = curl_exec($curl);
        if (!empty($response)) {
            return $response;
        } else {
            return false;
        }
        curl_close($curl);
        return $response;
    }
}
