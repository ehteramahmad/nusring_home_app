<?php

class InstitutionAnalyticController extends InstitutionAppController {
    public $components = array('RequestHandler','Paginator','Session', 'Institution.InsCommon', 'Institution.InsEmail', 'Image', 'Quickblox', 'Cache');

    public $uses = array('EmailTemplate','Institution.User','Institution.EnterpriseUserList','Institution.UserEmployment','Institution.AdminUser','Institution.CompanyName','Institution.UserSubscriptionLog','Institution.NotificationBroadcastMessage','Institution.UserQbDetail','Institution.UserDutyLog','Institution.UserOneTimeToken','Institution.CacheLastModifiedUser','Institution.ExportChatTransaction','Institution.UserProfile','Institution.QrCodeRequest','Institution.Profession','Institution.RoleTag','Institution.AvailableAndOncallTransaction','Institution.ScreenShotTransaction','Institution.TrustAdminActivityLog');
    public $helpers = array('Form', 'Html', 'Js', 'Time','Paginator');

    public function datastudio(){
      $this->checkAdminRole();
        // Admin activity logs [START]
        $activityData = array();
        $activityData['company_id'] = $_COOKIE['adminInstituteId'];
        $activityData['admin_id'] = $_COOKIE['adminUserId'];
        $activityData['action'] = 'Data-Studio Report';
        $activityData['custom_data'] = 'view';
        $this->TrustAdminActivityLog->addActivityLog($activityData);
        // Admin activity logs [END]

        $this->set(array('url'=>'https://datastudio.google.com/embed/reporting/1OXIHPcl5n10Wo3GiZvtd84hkBwmv0yzQ/page/E49E'));
    }

    public function dataStudioFilter(){
        $this->autoRender = false;
        if(isset($this->data['page_number'])){
            $page_number=$this->data['page_number'];
        }else{
            $page_number=1;
        }
        switch ($page_number) {
            case 1:
                $url = 'https://datastudio.google.com/embed/u/0/reporting/1OXIHPcl5n10Wo3GiZvtd84hkBwmv0yzQ/page/E49E';
                break;
            case 2:
                $url = 'https://datastudio.google.com/embed/u/0/reporting/1OXIHPcl5n10Wo3GiZvtd84hkBwmv0yzQ/page/1M';
                break;
            case 3:
                $url = 'https://datastudio.google.com/embed/u/0/reporting/1OXIHPcl5n10Wo3GiZvtd84hkBwmv0yzQ/page/hm3W';
                break;
            default :
                $url = 'https://datastudio.google.com/embed/u/0/reporting/1OXIHPcl5n10Wo3GiZvtd84hkBwmv0yzQ/page/E49E';
        }
        $this->set(array('url'=>$url,'page'=>$page_number));
        $this->render('/Elements/analytic/data_studio_filter');
    }

    public function powerbi(){
      $this->checkAdminRole();
    	// Admin activity logs [START]
        $activityData = array();
        $activityData['company_id'] = $_COOKIE['adminInstituteId'];
        $activityData['admin_id'] = $_COOKIE['adminUserId'];
        $activityData['action'] = 'Power-Bi Report';
        $activityData['custom_data'] = 'view';
        $this->TrustAdminActivityLog->addActivityLog($activityData);
    	// Admin activity logs [END]

    	$this->set(array(
        // 'url'=>'https://app.powerbi.com/view?r=eyJrIjoiNTU3ZGJmMmEtNTFiZS00MzJhLWEzZjEtNzBhZjBmMDQ3OGJkIiwidCI6ImQ1MmViODExLTk4YjAtNGU2OC1iNWVjLTI4Yjk3ZmJkOTNjZCJ9'
        'main_url' => BASE_URL.'mbapiv2/mbapiv2userwebservices/getPowerBiToken',
        'sub_url' => 'https://api.powerbi.com/v1.0/myorg/reports/ced8b517-5fb2-4ad3-a23c-f25852fda9f3'
        // 'sub_url' => 'https://api.powerbi.com/v1.0/myorg/reports/3a1897b3-3c73-4d60-a0f3-7c080bb09d30'
      ));
    }
}
