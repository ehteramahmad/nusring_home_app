<?php

class InstitutionBroadcastController extends InstitutionAppController {
	public $components = array(
		'RequestHandler',
		'Paginator',
		'Session',
		'Institution.InsCommon',
		'Institution.InsEmail',
		'Image',
		'Quickblox',
		'Cache'
	);
	
	public $uses = array(
		'EmailTemplate',
		'Institution.User',
		'Institution.EnterpriseUserList',
		'Institution.UserEmployment',
		'Institution.AdminUser',
		'Institution.CompanyName',
		'Institution.UserSubscriptionLog',
		'Institution.NotificationBroadcastMessage',
		'Institution.UserQbDetail',
		'Institution.UserDutyLog',
		'Institution.UserOneTimeToken',
		'Institution.CacheLastModifiedUser',
		'Institution.ExportChatTransaction',
		'Institution.UserProfile',
		'Institution.QrCodeRequest',
		'Institution.Profession',
		'Institution.RoleTag',
		'Institution.AvailableAndOncallTransaction',
		'Institution.ScreenShotTransaction',
		'Institution.TrustAdminActivityLog',
		'Institution.SessionTimeIp',
		'Institution.SessionTimeIpRange',
		'Institution.UserRoleTag',
		'Institution.UserPermanentRole'
	);
	public $helpers = array('Form', 'Html', 'Js', 'Time','Paginator');
    
	public function index(){
		$this->checkAdminRole();
	}
	
	public function getProfession(){
		$companyId = $_COOKIE['adminInstituteId'];
		$user_country_id = $_COOKIE['adminCountryId'];

		$custom_condition = array('Profession.status' => 1);
		$condition = array_merge(
			array('Profession.country_id'=>$user_country_id),
			$custom_condition
		);

		$fields = array('
			Profession.id,
			Profession.profession_type
		');

		$options = array(
			'conditions' => $condition,
			'fields' => $fields,
			'group' => array('Profession.id'),
			'order' => 'Profession.profession_type ASC'
		);

		$allProfessions = $this->Profession->find('all',$options);

		$final_list = array();
		foreach ($allProfessions as $prof) {
			$id = $prof['Profession']['id'];
			$name = $prof['Profession']['profession_type'];
			$final_list[] = array('name' => $name, 'id' => $id);
		}
		$this->set(array("data"=>$final_list));
		$this->render('/Elements/broadcast/cateogaryList');
	}

	public function getLocation(){
		$companyId = $_COOKIE['adminInstituteId'];
		$user_country_id = $_COOKIE['adminCountryId'];

		$custom_condition = array('RoleTag.status' => 1);

		$fields = array('
			RoleTag.id,
			RoleTag.value
		');
  		$conditions = array(
			'RoleTag.trust_id' => array($companyId, 0),
			'RoleTag.key' => "Base Location",
			'RoleTag.status' => 1
		);
  		$options = array(
  			'conditions' => $conditions,
  			'fields' => $fields,
  			'group' => array('RoleTag.id'),
  			'order' => 'RoleTag.value ASC'
  		);

		$allCateogary = $this->RoleTag->find('all',$options);

		$final_list = array();
		foreach ($allCateogary as $cateogary) {
			$id = $cateogary['RoleTag']['id'];
			$name = $cateogary['RoleTag']['value'];
			$final_list[] = array('name' => $name, 'id' => $id);
		}
		$this->set(array("data"=>$final_list));
		$this->render('/Elements/broadcast/cateogaryList');
	}

	public function userList(){
		$this->checkAdminRole();
	}

	public function getUserList(){
		$companyId = $_COOKIE['adminInstituteId'];
		$filters = json_decode($this->request->data['filters'], true);
		if(isset($this->request->data['deletedUsers']) && $this->request->data['deletedUsers'] != ''){
			$removedUsers = json_decode($this->request->data['deletedUsers'], true);
		}else{
			$removedUsers = array();
		}
		if(isset($this->request->data['search_text']) && $this->request->data['search_text'] != ''){
			$text=$this->data['search_text'];
		}else{
			$text = '';
		}

		if(isset($this->request->data['page_number'])){
		    $page_number=$this->request->data['page_number'];
		}
		else{
		    $page_number=1;
		}

		if(isset($this->request->data['limit'])){
			$limit=$this->request->data['limit'];
		}
		else{
			$limit=20;
		}

		$fields = array('
			User.id,
			User.email,
			UserProfile.profile_img,
			UserProfile.first_name,
			UserProfile.last_name,
			UserProfile.custom_role,
			RoleTag.value,
			Profession.profession_type,
			UserDutyLog.status,
			UserDutyLog.atwork_status,
			UserQbDetail.qb_id,
			VoipPushNotification.device_token,
			VoipPushNotification.device_token
		');

		$userData = $this->getFilteredUsers($filters, $removedUsers, $text, $page_number, $limit, $fields);
		$userIds = $this->getFilteredUsers($filters, $removedUsers, '', 0, '', array('User.id'));
		$userIds = $userIds['userList'];

		$userList = $userData['userList'];
		$tCount = $userData['tCount'];

		$idsArray = array();
		$userArray = array();
		foreach ($userIds AS  $id) {
			array_push($idsArray,$id['User']['id']);
		}

		foreach ($userList AS  $userData) {
			$cust_class = 'notAtWork';
			if((string)$userData['UserDutyLog']['status'] == '1'){
				$cust_class = 'onCallMain';
			}else if((string)$userData['UserDutyLog']['atwork_status'] == '0'){
				$cust_class = 'notAtWork';
			}else{
				$cust_class = '';
			}
			$isBroadcast = false;
			if(!empty($userData['VoipPushNotification']) && isset($userData['VoipPushNotification']['device_token'])){
				if($userData['UserQbDetail']['qb_id'] != "" && $userData['UserQbDetail']['qb_id'] != null){
					$isBroadcast = true;
				}
			}

			$roleTagData = json_decode($userData['UserProfile']['custom_role'],true);
			$baselocation = 'Not Selected';
			if(isset($roleTagData) && !empty($roleTagData)){
				foreach ($roleTagData as $value) {
					if($value['key'] == 'Base Location' && count($value['value']) > 0){
						$baselocation = $value['value'][0];
					}
				}
			}
			$userArray[] = array(
				'id' => $userData['User']['id'],
				'email' => $userData['User']['email'],
				'UserName' => $userData['UserProfile']['first_name'].' '.$userData['UserProfile']['last_name'],
				'profile_img' => isset($userData['UserProfile']['profile_img']) ? AMAZON_PATH.$userData['User']['id'].'/profile/'.$userData['UserProfile']['profile_img']:'',
				'profession_type' => $userData['Profession']['profession_type'],
				'location' => $baselocation,
				'cust_class' => $cust_class,
				'isBroadcast' => $isBroadcast
			);
		}

		$data['user_list'] = $userArray;
		$data['idsData'] = json_encode($idsArray);

		$this->set(array("data"=>$data,"condition"=>$condition,"limit"=>$limit,'tCount'=>$tCount));
		$this->render('/Elements/broadcast/userList');
	}

	public function broadcastMessage(){
		$this->checkAdminRole();
	}

	public function confirmMessage(){
		$this->checkAdminRole();
	}

	public function broadcastMessageSent(){
		$this->checkAdminRole();
	}

	public function sendBroadcastMessage(){
		$this->autoRender = false;
		$responseData = array();
		$companyId = $_COOKIE['adminInstituteId'];
		$email = $_COOKIE['adminEmail'];
		$password = $this->request->data['password'];
		$message = $this->request->data['message'];
		$AllIds = json_decode($this->request->data['AllIds'],true);

		$params = array();
		$params['userName'] = strtolower($email);
		$params['password'] = $password;
		$userData = $this->AdminUser->checkLogin($params);
		if(!empty($userData)){
			$filters = json_decode($this->request->data['filters'], true);
			if(isset($this->request->data['deletedUsers']) && $this->request->data['deletedUsers'] != ''){
				$removedUsers = json_decode($this->request->data['deletedUsers'], true);
			}else{
				$removedUsers = array();
			}

			$condition = array(
				'User.id' => $AllIds
			);

			$fields = array('
				User.id,
				UserQbDetail.qb_id,
				VoipPushNotification.device_token,
				VoipPushNotification.fcm_voip_token
			');
			$joins = array(
				array(
					'table' => 'user_qb_details',
					'alias' => 'UserQbDetail',
					'type' => 'left',
					'conditions'=> array('User.id = UserQbDetail.user_id')
				),
				array(
					'table' => 'voip_push_notifications',
					'alias' => 'VoipPushNotification',
					'type' => 'left',
					'conditions'=> array('User.id = VoipPushNotification.user_id')
				)
			);
			$options = array(
				'conditions' => $condition,
				'fields' => $fields,
				'joins' => $joins,
				'group' => array('User.id'),
			);

			$userList = $this->User->find('all',$options);
			$this->sendBroadcastFinal($userList, $message);
		}else{
            $responseData['status'] = 0;
            $responseData['message'] = 'Invalid Credentials.';
			echo json_encode($responseData);
		}
	}

	public function getFilteredUsers($filters, $removedUsers, $text, $page_number, $limit, $fields){
		$companyId = $_COOKIE['adminInstituteId'];
		if(isset($removedUsers) && !empty($removedUsers)){
			$deletedUsers = $removedUsers;
		}else{
			$deletedUsers = array();
		}

		$professionArray = $filters['professions'];
		$baseLocationArray = $filters['locationsName'];
		$availableStatus = $filters['availableStatus'];

		if(isset($text) && $text != ''){
			$searchCondition = array(
				'OR'=>array(
					'LOWER(RoleTag.value) LIKE'=>strtolower('%'.$text.'%'),
					'LOWER(Profession.profession_type) LIKE'=>strtolower('%'.$text.'%'),
					'LOWER(User.email) LIKE'=>strtolower('%'.$text.'%'),
					'LOWER(CONCAT(UserProfile.first_name, " ", UserProfile.last_name)) LIKE'=>strtolower('%'.$text.'%')
				)
			);
		}else{
			$searchCondition = array();
		}

		if(!empty($deletedUsers)){
			$removedCondition = array('User.id !=' => $deletedUsers, 'UserEmployment.user_id !=' => $deletedUsers);
		}else{
			$removedCondition = array();
		}
		
		if(!empty($baseLocationArray)){

			foreach ($baseLocationArray as $name) {
				$locationCondition['or'][] = array('UserProfile.custom_role LIKE' => "%$name%");
			}
		}else{
			$locationCondition = array();
		}
		
		if(!empty($professionArray)){
			$professionCondition = array('UserProfile.profession_id' => $professionArray);
		}else{
			$professionCondition = array();
		}
		
		if(!empty($availableStatus)){
			$availableCondition = array('UserDutyLog.atwork_status' => $availableStatus);
		}else{
			$availableCondition = array();
		}

		$mainConditions = array(
			'UserEmployment.company_id' => $companyId,
			'UserEmployment.is_current' => 1,
			'UserEmployment.status' => 1,
			'User.status' => 1,
			'User.approved' => 1,
			// 'LOWER(VoipPushNotification.application_type)' => 'mb',
			// 'VoipPushNotification.status' => 1,
			'EnterpriseUserList.status' => 1
		);

		$condition = array_merge(
			$searchCondition,
			$removedCondition,
			$locationCondition,
			$professionCondition,
			$availableCondition,
			$mainConditions
		);
		
		$joins = array(
			array(
				'table' => 'professions',
				'alias' => 'Profession',
				'type' => 'left',
				'conditions'=> array('UserProfile.profession_id = Profession.id')
			),
			array(
				'table' => 'user_qb_details',
				'alias' => 'UserQbDetail',
				'type' => 'left',
				'conditions'=> array('User.id = UserQbDetail.user_id')
			),
			array(
				'table' => 'user_duty_logs',
				'alias' => 'UserDutyLog',
				'type' => 'left',
				'conditions'=> array('UserDutyLog.user_id = User.id')
			),
			array(
				'table' => 'user_employments',
				'alias' => 'UserEmployment',
				'type' => 'left',
				'conditions'=> array('UserEmployment.user_id = User.id')
			),
			array(
				'table' => 'user_subscription_logs',
				'alias' => 'UserSubscriptionLog',
				'type' => 'left',
				'conditions'=> array('User.id = UserSubscriptionLog.user_id')
			),
			array(
				'table' => 'enterprise_user_lists',
				'alias' => 'EnterpriseUserList',
				'type' => 'left',
				'conditions'=> array('User.email = EnterpriseUserList.email', 'UserEmployment.company_id = EnterpriseUserList.company_id')
			),
			array(
				'table' => 'user_role_tags',
				'alias' => 'UserRoleTag',
				'type' => 'left',
				'conditions'=> array('UserRoleTag.user_id = User.id')
			),
			array(
				'table' => 'role_tags',
				'alias' => 'RoleTag',
				'type' => 'left',
				'conditions'=> array('RoleTag.id = UserRoleTag.role_tag_id')
			),
			array(
				'table' => 'voip_push_notifications',
				'alias' => 'VoipPushNotification',
				'type' => 'left',
				'conditions'=> array('User.id = VoipPushNotification.user_id')
			)
		);

  		$options = array(
			'conditions' => $condition,
			'fields' => $fields,
			'joins' => $joins,
			'group' => array('UserEmployment.user_id'),
			'order' => "CONCAT(UserProfile.first_name, ' ', UserProfile.last_name) ASC"
		);

		// print_r($options);
		// exit;
		if($limit != ''){
			$paginationArray = array(
				'limit' => $limit,
				'page'=> $page_number
			);
			$finalOptions = array_merge($paginationArray, $options);
			$this->Paginator->settings = $finalOptions;
			$userList = $this->Paginator->paginate('User');
			$total = $this->request->params;
			$tCount = $total['paging']['User']['count'];
		}else{
			$userList = $this->User->find('all', $options);
			$tCount = count($userList);
		}
		return array('userList' => $userList, 'tCount' => $tCount);
	}

	public function sendBroadcastFinal($id_data, $message){
		$email = $_COOKIE['adminEmail'];
		$companyId = $_COOKIE['adminInstituteId'];
		$serviceId = 'MG0490a00ed855239165604a73dda63967';
		$broadcastIds = array();
		$broadcastUsers = array();
		$smsIds = array();
		if(empty($id_data)){
			$responseData['status'] = 2;
			$responseData['message'] = 'No members to send Broadcast message.';
			echo json_encode($responseData);
		}else{
			foreach ($id_data AS  $ids) {
				if(!empty($ids['VoipPushNotification']) && isset($ids['VoipPushNotification']['device_token'])){
					if($ids['UserQbDetail']['qb_id'] != "" && $ids['UserQbDetail']['qb_id'] != null){
							$broadcastIds[] = $ids['UserQbDetail']['qb_id'];
							$broadcastUsers[] = $ids['User']['id'];
							$token_data[] = $ids['VoipPushNotification'];
					}
				}else{
						$smsIds[] = $ids['User']['id'];
				}
			}
	
			if(count($broadcastUsers) > 0){
				foreach ($token_data as $value) {
					$params = array();
	
					$params['notificationMessage'] = $message;
					$params['voip_tokens']['VoipPushNotification'] = $value;
					$this->sendCustomVoipPush($params);
	
				}
				$responseData['status'] = 1;
				$responseData['message'] = 'OK';
	
				$notificationSendResponse = array(
					"sender"=>$email,
					"send_to"=>serialize($broadcastUsers),
					"trust_id"=>$companyId,
					"sent_messages"=>$message,
					"type"=>"broadcast",
					"sent_from"=>"Broadcast Message",
					"response"=>'Broadcast sent'
				);
	
				try{
					$this->NotificationBroadcastMessage->saveAll($notificationSendResponse);
				}catch(Exception $e){}
			}
	// ------ Response when SMS service are not available --------
			else{
				$responseData['status'] = 2;
				$responseData['message'] = 'No members to send Broadcast message.';
				// echo json_encode($responseData);
			}
	// ------ Response when SMS service are not available --------
	
			if(count($smsIds) > 1){
				$numbers = $this->UserProfile->find('all',array('conditions'=>array('UserProfile.user_id' => $smsIds),'fields'=>array('UserProfile.contact_no','UserProfile.user_id')));
	
					$deliver_status['success'] = 0;
					$deliver_status['success_users'] = array();
					$deliver_status['failure'] = 0;
					$deliver_status['failure_users'] = array();
	
				foreach ($numbers as $value) {
						if($value['UserProfile']['contact_no'] != ''){
								$smsUsers[] = $value['UserProfile']['user_id'];
								$data = json_decode(json_decode($this->sendSms($value['UserProfile']['contact_no'], $message, $serviceId)));
								if($data->status == 'accepted'){
										$response = json_decode(json_decode($this->getSmsResponse($data->sid)));
										if($response->status != 'failed'){
											$deliver_status['success'] += 1;
											$deliver_status['success_users'][] = $value['UserProfile']['user_id'];
										}else{
											$deliver_status['failure'] += 1;
											$deliver_status['failure_users'][] = $value['UserProfile']['user_id'];
										}
								}else{
										$deliver_status['failure'] += 1;
										$deliver_status['failure_users'][] = $value['UserProfile']['user_id'];
								}
						}
				}
	
				$messageResponse = array(
					"sender"=>$email,
					"send_to"=>serialize($smsUsers),
					"trust_id"=>$companyId,
					"sent_messages"=>$message,
					"type"=>"message",
					"sent_from"=>"Broadcast Message",
					"deliver_status"=>json_encode($deliver_status),
					"response"=>"Message sent"
				);
				try{
					$this->NotificationBroadcastMessage->saveAll($messageResponse);
				}catch(Exception $e){}
	
				$responseData['status'] = 1;
				$responseData['message'] = 'OK';
			}
			echo json_encode($responseData);
		}
		try{
			// Admin activity logs [START]
			$activityData = array();
			$activityData['company_id'] = $_COOKIE['adminInstituteId'];
			$activityData['admin_id'] = $_COOKIE['adminUserId'];
			$activityData['action'] = 'Broadcast Message';
			$activityData['custom_data'] = '{"company_id" :"'.$companyId.'","message":"'.$message.'"}';
			$this->TrustAdminActivityLog->addActivityLog($activityData);
			// Admin activity logs [END]
		} catch (Exception $e) {

		}
	}
}
?>