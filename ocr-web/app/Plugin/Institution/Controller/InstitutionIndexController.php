<?php

class InstitutionIndexController extends InstitutionAppController {

    public $uses = array('Institution.AdminUser','EmailTemplate','Institution.InstitutionGroupAdmin','Institution.TrustAdminActivityLog');
    public $helpers = array('Form', 'Html', 'Js', 'Time');

    public function index() {

    }

    public function login() {
      $this->autoRender = false;
      $responseData = array();
      if( !empty($this->request->data['userName']) && !empty($this->request->data['password']) ){
          $this->request->data['UserAdmin']['adminEmail'] = $this->request->data['userName'];
          $this->request->data['UserAdmin']['password'] = $this->request->data['password'];

          $params = array();
          $params['userName'] = strtolower($this->request->data['userName']);
          $params['password'] = $this->request->data['password'];

          $checkAdmin = $this->AdminUser->checkAdmin($params);

          if( !empty($checkAdmin) ){
              $userData = $this->AdminUser->checkLogin($params);

              if( !empty($userData) ){
                $admin_name = $userData['UserProfile']['first_name'].' '.$userData['UserProfile']['last_name'];
                $user_country_id = $userData['UserProfile']['country_id'];
                if($user_country_id==226){
                    // date_default_timezone_set('Europe/Amsterdam');
                    date_default_timezone_set("Europe/London");
                }else if($user_country_id==99){
                    date_default_timezone_set('Asia/Kolkata');
                }
                $institution_group_admin_id = 0;
                $institution_group_admin_qbId = 0;

                $responseData['status'] = 1;
                $responseData['UserName'] = $userData['AdminUser']['username'];
                $responseData['CompanyId'] = $userData['AdminUser']['company_id'];

                $companyData = $this->CompanyName->find('first',array('conditions'=>array('CompanyName.id'=>$userData['AdminUser']['company_id'])));
                $companySubscriptionPeriod = isset($companyData['CompanyName']['subscription_expiry_date']) ? $companyData['CompanyName']['subscription_expiry_date'] : 0;
                if( strtotime($companySubscriptionPeriod) > strtotime('now')  || $userData['AdminUser']['company_id'] == 0) {
                  $group_admin = $this->InstitutionGroupAdmin->find('first',array('conditions'=>array('institute_id'=>$userData['AdminUser']['company_id'],'status'=>1)));

                  if(!empty($group_admin)){
                      $institution_group_admin_id = $group_admin['InstitutionGroupAdmin']['user_id'];
                      $institution_group_admin_qbId = $group_admin['InstitutionGroupAdmin']['qb_id'];
                  }else{
                      $institution_group_admin_id = 0;
                      $institution_group_admin_qbId = 0;
                  }
                  if($userData['AdminUser']['company_id'] == 0){
                    $companyList = $this->CompanyName->find('all',array(
                      'conditions'=>array(
                        'CompanyName.is_subscribed'=>1,
                        'CompanyName.status'=>1
                      ),
                      'joins'=>array(
                        array(
              	          'table' => 'institution_group_admins',
              	          'alias' => 'InstitutionGroupAdmin',
              	          'type' => 'left',
              	          'conditions'=> array('InstitutionGroupAdmin.institute_id = CompanyName.id')
              	        )
                      ),
                      'fields'=>array('InstitutionGroupAdmin.user_id,InstitutionGroupAdmin.qb_id,CompanyName.company_name,CompanyName.id,CompanyName.subscription_expiry_date'),
                      'order'=>'CompanyName.company_name ASC'
                    ));
                    foreach ($companyList as $comp) {
                      $comp_data[] = array(
                        'id'=>$comp['CompanyName']['id'],
                        'name'=>$comp['CompanyName']['company_name'],
                        'expiry'=>$comp['CompanyName']['subscription_expiry_date'],
                        'group_admin_id'=>$comp['InstitutionGroupAdmin']['user_id'],
                        'group_admin_qb_id'=>$comp['InstitutionGroupAdmin']['qb_id']
                      );
                    }
                    ////--------Set localstorage [START]
                    setcookie('all_company_list', json_encode($comp_data), time() + (86400 * 30), "/");
                    ////--------Set localstorage [END]
                  }


                  setcookie('adminEmail', strtolower($this->request->data['userName']), time() + (86400 * 30), "/");
                  setcookie('adminName', ucfirst($admin_name), time() + (86400 * 30), "/");
                  setcookie('adminInstituteId', $userData['AdminUser']['company_id'], time() + (86400 * 30), "/");
                  setcookie('adminRoleId', $userData['AdminUser']['role_id'], time() + (86400 * 30), "/");
                  setcookie('adminUserId', $userData['User']['id'], time() + (86400 * 30), "/");
                  setcookie('adminCountryId', $user_country_id, time() + (86400 * 30), "/");
                  setcookie('institutionExpiryDate', $companySubscriptionPeriod, time() + (86400 * 30), "/");
                  setcookie('instituteGroupAdminId', $institution_group_admin_id, time() + (86400 * 30), "/");
                  setcookie('instituteGroupAdminQbId', $institution_group_admin_qbId, time() + (86400 * 30), "/");

                  if($userData['AdminUser']['company_id'] == 0){
                    setcookie('adminAccessId', 0, time() + (86400 * 30), "/");
                  }

                  echo json_encode($responseData);
                  try{
                      // Admin activity logs [START]
                      $activityData = array();
                      $activityData['company_id'] = $userData['AdminUser']['company_id'];
                      $activityData['admin_id'] = $userData['User']['id'];
                      $activityData['action'] = 'Login';
                      $this->TrustAdminActivityLog->addActivityLog($activityData);
                      // Admin activity logs [END]
                  } catch (Exception $e) {

                  }
                }else{
                  $responseData['status'] = 0;
                  $responseData['message'] = '* Subscription of your Institution has been expired.';
                  echo json_encode($responseData);
                }
              }else{
                  $responseData['status'] = 0;
                  $responseData['message'] = '* Invalid login credentials! Please check entered Username / Password and try again.';
                  echo json_encode($responseData);
              }
          }else{
              $responseData['status'] = 0;
              $responseData['message'] = '* You are not authorized to access this Panel. For any help, please contact support@medicbleep.com';
              echo json_encode($responseData);
          }
      }else{
          echo ERROR_603;
      }
      exit;
    }

    public function renewSession(){
        $this->autoRender = false;
        $all_sessions = $_SESION;
        foreach ($_SESION as $key => $value) {
          if($key != 'Config'){
            $this->Session->write($key, $value);
          }
        }
    }
    public function destroySession(){
        $this->layout = null;
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->Session->destroy();
        return false;
    }

    public function logout(){
      $this->layout = null;
      // $this->layout = 'InstitutionLogin';
      $this->autoRender = false;
        try{
            // Admin activity logs [START]
            $activityData = array();
            $activityData['company_id'] = $_COOKIE['adminInstituteId'];
            $activityData['admin_id'] = $_COOKIE['adminUserId'];
            $activityData['action'] = 'Logout';
            $this->TrustAdminActivityLog->addActivityLog($activityData);
            // Admin activity logs [END]
        } catch (Exception $e) {

        }
        $this->Session->destroy('adminEmail');
        $this->Session->destroy('adminInstituteId');
        $this->Session-> destroy();
        if (isset($_SERVER['HTTP_COOKIE'])) {
            $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
            foreach($cookies as $cookie) {
                $parts = explode('=', $cookie);
                $name = trim($parts[0]);
                setcookie($name, '', time()-1000);
                setcookie($name, '', time()-1000, '/');
            }
        }
        $this->redirect('/institution');
        exit;
    }
}
