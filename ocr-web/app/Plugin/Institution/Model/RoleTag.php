<?php


App::uses('AppModel', 'Model');

/**
 * Represents model role_tags
 */
class RoleTag extends AppModel {

	public function userRoleTags($params=array()){
		$roleTag = array();
		if(!empty($params)){
			$userId = $params['user_id'];
			$quer = "SELECT rt.id,urt.id,rt.`key`,rt.value FROM `role_tags` rt LEFT JOIN user_role_tags urt ON rt.id=urt.role_tag_id WHERE urt.user_id =$userId AND rt.key='".$params['key']."'";
			$roleTag = $this->query($quer);
		}
		return $roleTag;
	}
}
