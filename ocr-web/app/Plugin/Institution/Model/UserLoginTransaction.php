<?php


App::uses('AppModel', 'Model');

/**
	* Represents model UserLoginTransaction
*/
class UserLoginTransaction extends AppModel {

	public function getTransaction($user_id) {
		$data = array();
		if( $user_id ){
			$data['Web'] = $this->find('first',array('conditions'=>array('user_id'=>$user_id,'application_type'=>'MB-WEB'),'order'=>'id DESC'));
			$data['App'] = $this->find('first',array('conditions'=>array('user_id'=>$user_id,'application_type'=>'MB'),'order'=>'id DESC'));
		}
		return $data;
	}

}
