<?php
/*
User model to add, update ,validate user data
*/
App::uses('AppModel', 'Model');
// App::uses('InstitutionAppModel', 'AppModel');

class User extends AppModel {

    var $hasOne = array(
        'UserProfile' => array(
            'className' => 'UserProfile',
            'foreignKey' => 'user_id',
        )
    );
    // var $hasMany = array(
    //     'UserSpecilities' => array(
    //         'className' => 'UserSpecilities',
    //         'foreignKey' => 'user_id',
    //         'conditions' => array('UserSpecilities.status' => 1)
    //     )
    // );


    public function getCount($comp_Id = NULL,$group_admin = NULL) {
        if(isset($comp_Id)){
            $activeUser = 0;
            $pendingUser = 0;
            $newUsers = 0;
            $custom_condition = array();

            if($groupAdmin != 0){
                $custom_condition = array('User.id !=' => $groupAdmin);
            }

            $conditions = array(
                    'UserEmployment.company_id' => $comp_Id,
                    'User.status' => 1,
                    'User.approved' => 1,
                    'DATE(User.registration_date) > DATE_SUB(NOW(), INTERVAL 7 DAY)'
                );

            $conditions_final = array_merge($custom_condition,$conditions);

            //*********** New Users Registred Within last seven days     **************//
            $newUsers = $this->find('count',array(
                'conditions' => $conditions_final,
                'joins'=> array(
                    array(
                        'table' => 'user_employments',
                        'alias' => 'UserEmployment',
                        'type' => 'left',
                        'conditions'=> array('UserEmployment.user_id = User.id')
                    )
                ),
            ));

            $data['new_users'] = $newUsers;

            return $data;
        }
    }

    public function userDetailsById( $userId = NULL , $loggedinUser = NULL ){
        $userLists = array();
        if( !empty($userId) ){
            if(!empty($loggedinUser)){
                $conditions = "  User.id !=" . $loggedinUser . " AND User.id = $userId  ";
            }else{
                $conditions = "  User.id = $userId ";
            }
            $userLists = $this->find('first',
                            array(
                                'joins'=>
                                array(
                                    array(
                                        'table' => 'countries',
                                        'alias' => 'Country',
                                        'type' => 'left',
                                        'conditions'=> array('UserProfile.country_id = Country.id')
                                    ),
                                    array(
                                        'table'=> 'professions',
                                        'alias' => 'Profession',
                                        'type' => 'left',
                                        'conditions'=> array('UserProfile.profession_id = Profession.id')
                                    ),
                                    array(
                                        'table'=> 'user_specilities',
                                        'alias' => 'UserSpecility',
                                        'type' => 'left',
                                        'conditions'=> array('User.id = UserSpecility.user_id')
                                    ),
                                    array(
                                        'table'=> 'specilities',
                                        'alias' => 'Specility',
                                        'type' => 'left',
                                        'conditions'=> array('Specility.id = UserSpecility.specilities_id')
                                    ),
                                ),
                                'conditions' => $conditions,
                                //'group'=> array('User.id'),
                                //'order'=> 'UserProfile.first_name'
                            )
                        );
        }
        return $userLists;
    }



}
