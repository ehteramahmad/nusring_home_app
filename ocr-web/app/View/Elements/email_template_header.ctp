<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Newsletter</title>
</head>

<body>
<table width="535" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:'Segoe UI', Arial;font-size:14px;line-height:18px;color:#3a3a3a;text-decoration:none;" >
  <tbody >
    <tr>
      <td align="left" valign="top" style="font-size:0%;"><img src="https://s3-us-west-1.amazonaws.com/theoncallroom/latest_theme_assets/images/356991_Spacer.gif" alt="" border="0"></td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF" style="border:1px solid #e6e6e6;" ><table width="535" border="0" cellspacing="0" cellpadding="0" >
          <tbody>
            <tr>
              <td align="left" valign="top" style="background-color:#fff; padding: 5px;"><img src="https://s3-us-west-1.amazonaws.com/theoncallroom/latest_theme_assets/images/logo-new.png" alt="" width="123" height="63" style="border:0px;"></td>
              <td align="left" valign="top" style="background-color:#fff; vertical-align: middle;"><div class="social-links">
                  <ul style="float:right; margin-right:6px;">
                    <li style="list-style:none; float:left; margin-right:6px;">
                    <a href="https://www.linkedin.com/company/4842839?trk=tyah&amp;trkInfo=idx%3A1-1-1%2CtarId%3A1425467003079%2Ctas%3Athe+on+call+room" title="LinkedIn" target="_blank"><img src="https://s3-us-west-1.amazonaws.com/theoncallroom/latest_theme_assets/images/linked-icon.png" alt="LinkedIn"></a>
                    </li>
                    <li style="list-style:none; float:left; margin-right:6px;">
                    <a href="https://www.facebook.com/TheOnCallRoom?ref=br_rs" title="Facebook" target="_blank"><img src="https://s3-us-west-1.amazonaws.com/theoncallroom/latest_theme_assets/images/fb-icon.png" alt="Facebook"></a>
                    </li>
                    <li style="list-style:none; float:left; margin-right:6px;">
                    <a href="https://twitter.com/theoncallroom" title="Twitter" target="_blank"><img src="https://s3-us-west-1.amazonaws.com/theoncallroom/latest_theme_assets/images/tweets.png" alt="Twitter"></a>
                    </li>
                  </ul>
                </div></td>
            </tr>
            <tr >
              <td colspan="2" align="left" valign="top"><a rel="nofollow" target="_blank" href="https://theoncallroom.com/" ><img src="https://s3-us-west-1.amazonaws.com/theoncallroom/latest_theme_assets/images/newsletter-new2.jpg" alt=""></a></td>
            </tr>
            <tr style="height: 10px; float: left;">
              <td height="2" colspan="2" align="left" valign="top">&nbsp;</td>
            </tr>
            <tr >
              <td colspan="2" align="center" valign="top" >
                