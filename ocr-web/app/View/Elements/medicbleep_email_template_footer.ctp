<!--</td> 
            </tr>
          </tbody>
        </table></td>
    </tr>
    <tr>
      <td height="3" align="left" valign="middle" bgcolor="#10a5da" style="font-family:'Segoe UI', Arial;font-size:9px;color:#ffffff;text-decoration:none;line-height:0px;">&nbsp;</td>
    </tr>
    <tr>
      <td height="59" align="center" valign="top" bgcolor="#ffffff" style="font-family:'Segoe UI', Arial;font-size:9px;color:#000;text-decoration:none; border: 1px solid #d2d2d2;">
      <p style=" margin-top:18px; width:100%; text-align: center; font-family:'Segoe UI', Arial;font-size:13px;">Copyright &copy; <a style="color:#000; text-decoration:underline;" target="_blank" href="http://mediccreations.com/"><img width="14" height="14" src="https://s3-us-west-1.amazonaws.com/theoncallroom/latest_theme_assets/images/medic-logo-small.png" alt="Logo"> Medic Creations</a> All Rights Reserved. </p>
      </td>
    </tr>
  </tbody>
</table>
</body>
</html>-->

</td> 
            </tr>
          </tbody>
        </table></td>
    </tr>
   <tr>
      <td height="1" align="left" valign="middle" bgcolor="#d5d5d5" style="font-family:'Segoe UI', Arial;font-size:1px;color:#ffffff;text-decoration:none;line-height:0px;">&nbsp;</td>
    </tr>
    <tr>
      <td align="center" valign="top" bgcolor="#ffffff" style="font-family:'Segoe UI', Arial;font-size:9px;color:#000;text-decoration:none; border: 1px solid #d2d2d2; padding-top: 20px;">
        <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" class="m_6145364116186641551mcnTextContentContainer" style="border-collapse:collapse;max-width:100%;min-width:100%">
                                            <tbody>
                                              <tr>
                                                <td valign="top" class="m_6145364116186641551mcnTextContent" style="word-break:break-word;color:rgb(128,128,128);font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;font-size:10px;line-height:12.5px;text-align:left;padding:0px 18px 9px"><em>Copyright © 2017 Medic Creations, All rights reserved.</em><span class="m_6145364116186641551Apple-converted-space">&nbsp;</span><br>
                                                  You signed up on The On Call Room via the App or Joined the waiting list of MedicBleep or Entered King's Fund Contest<span class="m_6145364116186641551Apple-converted-space">&nbsp;</span><br>
                                                  <br>
                                                  <strong>Our mailing address is:</strong><span class="m_6145364116186641551Apple-converted-space">&nbsp;</span><br>
                                                  <div class="m_6145364116186641551vcard"><span class="m_6145364116186641551org m_6145364116186641551fn">Medic Creations</span>
                                                    <div class="m_6145364116186641551adr">
                                                      <div class="m_6145364116186641551street-address">Oakdale</div>
                                                      <div class="m_6145364116186641551extended-address">Royal Oak Hill</div>
                                                      <span class="m_6145364116186641551locality">Newport</span>,<span class="m_6145364116186641551Apple-converted-space">&nbsp;</span><span class="m_6145364116186641551region">Gwent</span><span class="m_6145364116186641551Apple-converted-space">&nbsp;</span> <span class="m_6145364116186641551postal-code">NP18 1JF</span>
                                                      <div class="m_6145364116186641551country-name">United Kingdom</div>
                                                    </div>
                                                    <br>
                                                    <a href="http://mediccreations.us12.list-manage.com/vcard?u=4f8f2caf54859b1e05155fb68&amp;id=d83d76884e" class="m_6145364116186641551hcard-download" style="color:rgb(197,46,38);font-weight:bold;text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://mediccreations.us12.list-manage.com/vcard?u%3D4f8f2caf54859b1e05155fb68%26id%3Dd83d76884e&amp;source=gmail&amp;ust=1483151065894000&amp;usg=AFQjCNEOtoHdvxraHJzY2rtB0L538I4P9g">Add us to your address book</a></div>
                                                  <br>
                                                  <br>
                                                  Want to change how you receive these emails?<br>
                                                  You can<span class="m_6145364116186641551Apple-converted-space">&nbsp;</span><a href="http://mediccreations.us12.list-manage1.com/profile?u=4f8f2caf54859b1e05155fb68&amp;id=d83d76884e&amp;e=25168b7811" style="color:rgb(197,46,38);font-weight:bold;text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://mediccreations.us12.list-manage1.com/profile?u%3D4f8f2caf54859b1e05155fb68%26id%3Dd83d76884e%26e%3D25168b7811&amp;source=gmail&amp;ust=1483151065894000&amp;usg=AFQjCNHSN1wHCpMkHvdRIKvmOBq-Loiluw">update your preferences</a><span class="m_6145364116186641551Apple-converted-space">&nbsp;</span>or<span class="m_6145364116186641551Apple-converted-space">&nbsp;</span><a href="http://mediccreations.us12.list-manage.com/unsubscribe?u=4f8f2caf54859b1e05155fb68&amp;id=d83d76884e&amp;e=25168b7811&amp;c=5014839cdb" style="color:rgb(197,46,38);font-weight:bold;text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://mediccreations.us12.list-manage.com/unsubscribe?u%3D4f8f2caf54859b1e05155fb68%26id%3Dd83d76884e%26e%3D25168b7811%26c%3D5014839cdb&amp;source=gmail&amp;ust=1483151065894000&amp;usg=AFQjCNHN5p2w0nmKdSVdMlCstrMZcTjBBg">unsubscribe from this list</a><span class="m_6145364116186641551Apple-converted-space">&nbsp;</span><br>
                                                  <br></td>
                                              </tr>
                                            </tbody>
                                          </table>
      </td>
    </tr>
  </tbody>
</table>
</body>
</html>