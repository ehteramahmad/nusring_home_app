<?php
	// Set email template header
	echo $this->element('medicbleep_email_template_header_only_icon');
	// Email template body part
 	echo isset($msg) ? $msg : '';
 	// Set email template footer
 	echo $this->element('medicbleep_email_template_footer');