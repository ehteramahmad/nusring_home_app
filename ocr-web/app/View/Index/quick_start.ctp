<section class="inner-content">
<!--Inner page-->
  <div class="container">
    <div class="row">
      <div class="howOCR">
        <div class="col-sm-12">
        <div class="bs-example">
        <?php if(isset($pageTitle) && $pageTitle != ''){ ?> <h1><?php echo $pageTitle; ?></h1> <?php } ?>
              <p class="text-center">The On Call Room has been designed to be easy and intuitive to use. We’ve put this quick start guide together to point you in the right direction during the sign up process and answer any questions you might have.</p>
<p class="text-center"><strong>Download the OCR app</strong></p>
<p class="text-center">If you have an iPhone you should download the app from the iOS app store. (works with iOS 8.2 onwards)</p>
<p class="text-center marBottom20 width20 marAuto"><a title="Download on the App Store" target="_blank" href="https://itunes.apple.com/us/app/the-on-call-room-ocr/id975310441?ls=1&amp;mt=8" onclick="ga('send', 'event', 'app', 'download','appstore');">
  <?php echo $this->Html->image('app-store-black.png', array("alt"=> "Download on the App Store", "title"=> "Download on the App Store", "height"=> "70", "border"=> "0", "width"=> "229")); ?>
</a>
</p>
<p class="text-center">Alternatively, if you have an Android phone you should download OCR from the Google Play app store. (works with Android 4.3 'Jellybean' onwards)</p>
<p class="text-center marBottom20 width20 marAuto"><a title="Get it on Google play" target="_blank" href="https://play.google.com/store/apps/details?id=com.ocr.theoncallroom&amp;hl=en" onclick="ga('send', 'event', 'app', 'download','googleplay');">
  <?php echo $this->Html->image('google-play-black.png', array("alt"=> "Get it on Google play", "title"=> "Get it on Google play", "height"=> "70", "width"=> "229")); ?>
</a>
</p>
      <h3 class="marTop40">Open The OCR app for the First Time.</h3>
            <ul class="quickStart">
                <li>  
                  <div class="leftFloat quickStartWidth">
                      <ul class="aboutTab padTop30"><li>Tap the OCR app icon on your phone:</li></ul>
                    </div>
                    <div class="text-center">
                      <?php echo $this->Html->image('start01.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>  
                  <div class="leftFloat quickStartWidth">
                      <ul class="aboutTab padTop20"><li>Take a look through the introductory screens, then hit sign up</li></ul>
                    </div>
                    <div class="text-center">
                      <?php echo $this->Html->image('start02.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>  
                  <div class="leftFloat quickStartWidth">
                      <ul class="aboutTab padTop20">
                          <li>Type in your name and profession and we’ll search for you against our comprehensive database of medical professionals.</li>
                            <li>If you’re there, you can start using OCR immediately.</li>
                        </ul>
                    </div>
                    <div class="text-center">
                      <?php echo $this->Html->image('start03.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>  
                  <div class="leftFloat quickStartWidth">
                      <ul class="aboutTab padTop50">
                          <li>  If not, we’ll need to verify your identity. We do this to make sure that only medical professionals use the OCR. We’ll ask you for some contact details as well as to upload some ID. Don’t worry, we work in accordance with stringent UK data protection laws, so your ID is safe with us.</li>
                            <li>Once we’ve verified your identity as a medical professional, you’re good to go.</li>
                        </ul>
                    </div>
                    <div class="text-center">
                      <?php echo $this->Html->image('start04.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>  
                  <div class="leftFloat quickStartWidth">
                      <ul class="aboutTab padTop10">
                          <li>  While you’re waiting for your ID to be verified, you can take a sneak peek at OCR by using the app as a guest.</li>
                       </ul>
                    </div>
                    <div class="text-center">
                      <?php echo $this->Html->image('start05.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>  
                  <div class="leftFloat quickStartWidth">
                      <p><strong>Logging in for the First Time</strong></p>
                        <p>Once your ID has been verified, you’ll be free to use the app for the first time. Don’t be surprised by alerts asking you to approve notification and location settings. This is completely normal, and it just allows us to keep you in the loop and bring you geographic specific information where possible.</p>
                    </div>
                    <div class="text-center">
                      <?php echo $this->Html->image('start06.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>  
                  <div class="leftFloat quickStartWidth">
                      <p><strong>Setting Up Your Profile</strong></p>
                        <p>When you’re in the app for the first time, you’ll be asked to highlight your particular areas of interest. This is so we can personalize your OCR experience. We know that if you’re a cardiothoracic surgeon, you’re not going to be as interested in seeing advancements in skin diseases as a dermatologist would be.</p>
                    </div>
                    <div class="text-center">
                      <?php echo $this->Html->image('start07.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>  
                  <div class="leftFloat quickStartWidth">
                      <p><strong>Setting Up Your Profile</strong></p>
                        <p>You’ll also get the opportunity to fine tune the details in your profile by adding contact information including phone number and email. As this is an app purely for medical professionals, your details are safe. You won’t get headhunters contacting you or sales people ‘reaching out’ to you.</p>
                    </div>
                    <div class="text-center">
                      <?php echo $this->Html->image('start08.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>  
                  <div class="leftFloat quickStartWidth">
                      <p><strong>Posting Your First ‘Beat’</strong></p>
                        <p>Simply hit this icon in the bottom right hand corner of the app to make your first post.</p>
                    </div>
                    <div class="text-center">
                      <?php echo $this->Html->image('start09.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>  
                  <div class="leftFloat quickStartWidth">
                      <p><strong>Posting Your First ‘Beat’</strong></p>
                        <p>You’ll then be asked to add a title for your post, the areas of interest (e.g. Cardiothoracic Surgery) and get the chance to type some information about your post.</p>
                    </div>
                    <div class="text-center">
                      <?php echo $this->Html->image('start10.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>  
                  <div class="leftFloat quickStartWidth">
                        <p>You can also upload content (photo, video, document or URL link) to support your post.</p>
                    </div>
                    <div class="text-center">
                      <?php echo $this->Html->image('start11.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>  
                  <div class="leftFloat quickStartWidth">
                        <p>Should you want it, you’ll also get the option to post anonymously.</p>
                    </div>
                    <div class="text-center">
                      <?php echo $this->Html->image('start12.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>  
                  <div class="leftFloat quickStartWidth">
                      <p><strong>Commenting For the First Time</strong></p>
                        <p>Adding a comment to someone else’s post is really easy. Simply hit the comment button underneath the post…</p>
                    </div>
                    <div class="text-center">
                      <?php echo $this->Html->image('start13.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>  
                  <div class="leftFloat quickStartWidth">
                        <p>…type your message and tap the send button and you’re done.</p>
                    </div>
                    <div class="text-center">
                      <?php echo $this->Html->image('start14.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>  
                  <div class="leftFloat quickStartWidth">
                      <p><strong>Sharing for the First Time</strong></p>
                        <p>If you’ve seen a post your followers would love, you can share it by simply tapping the ‘share’ button. You’ll also have the option to add your own message before you share it.</p>
                    </div>
                    <div class="text-center">
                      <?php echo $this->Html->image('start15.png', array("alt"=> "")); ?>
                    </div>
                    <div class="clear"></div>
                </li>
            </ul>
            </div>
             </div>
             <p class="text-center marTop20 leftFloat width100">Still got questions? Check out our <a href="<?php echo BASE_URL . 'product-tour'; ?>">product tour</a> or our <a href="<?php echo BASE_URL . 'support'; ?>">FAQs</a></p>
      </div>
    </div>
  </div>
</section>