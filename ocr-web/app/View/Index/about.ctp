<?php 
	//echo !empty($about['Content']['content_text']) ? $about['Content']['content_text'] : '';
?>
<section class="inner-content">
<!--Inner page-->
  <div class="container">
    <div class="row">
      <div class="faq">
        <div class="col-sm-12">
        <div class="bs-example">
         <!-- tabs left -->
      <div class="tabbable tabs-left">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#a" data-toggle="tab" class="about-link">About</a></li>
          <li><a href="#b" data-toggle="tab" class="commitment-link">Our Commitment</a></li>
          <li><a href="#c" data-toggle="tab" class="mission-link">Our Mission</a></li>
          <li><a href="#d" data-toggle="tab" class="people-link">Our People</a></li>
        </ul>
        <div class="tab-content">
         <div class="tab-pane active" id="a">
         <h1><?php if(isset($pageTitle) && $pageTitle != ''){ ?> <h1><?php echo $pageTitle; ?></h1> <?php } ?></h1>
         	<p>The On Call Room (The OCR) is a knowledge sharing  & social networking app for the entire medical team. </p>
<p>Consultants, doctors, nurses, pharmacists, dentists and students can share best practise, help diagnose unusual symptoms and offer support to other professionals around the world. </p>
<p>Clinicians can use our app to quickly access up to the minute research, findings and discussions, wherever they are to help them improve patient care. Say goodbye to googling and hello to The OCR.</p>
<p><strong>A Medical Community at Your Fingertips</strong><br />We're on a mission to put medical knowledge, wisdom and advice in the hands of every medical professional around the world.</p>
<p>Do you sometimes need help quickly diagnosing unusual symptoms? </p>
<p>Ever need to chat to other medics and decompress after a difficult shift?</p>
<p>Want to find out how to better treat patients &amp; discover new medical advancements?</p>
<p>Then the OCR is for you. </p>
<p><strong>Make an Impact on Medicine</strong><br />
The OCR was created by doctors for doctors. We’ve been in the medical game a long time and experience tells us that by providing collaborative learning tools to healthcare professionals , we can help you make a greater impact on the world of medicine. This is why we started The On Call Room.</p>
<p>The OCR is the brainchild of Dr Sandeep Bansal. He believes that there is a better way to share and manage medical knowledge outside of journals and hospital notice boards. Since starting The On Call Room in February 2014, Sandeep has grown his idea into a team of 22, who are all striving to help expand the minds of the medical sector.</p>
<p><strong>Safe, Secure &amp; Easy to Use</strong><br />
We’ve made sure that The OCR app is safe, secure and confidential, so you can tap into a global network of medical professionals, whilst sticking to your Hippocratic Oath and keeping your patient’s best interests at heart.</p>
<p>It’s easy to get started, visit either the <a href="https://itunes.apple.com/us/app/the-on-call-room-ocr/id975310441?ls=1&mt=8" target="_blank">iOS app store</a> (if you have an iPhone) or the <a href="https://play.google.com/store/apps/details?id=com.ocr.theoncallroom&hl=en" target="_blank">Google Play store</a> (if you have an Android phone) and download The OCR. We’ll ask you a couple of questions to verify your identity against our databases and then you’re good to go.</p>
<p>If you’re stuck at any point, or want to find out more about using The OCR for the first time, check out our <a href="<?php echo BASE_URL . 'quick-start'; ?>" >getting started guide</a> to find out everything you need to know.</p>
<div class="navmenu centerAlign">
        <h3>Make Your Impact on Medicine</h3>
        <h4>Download The OCR now</h4>
        <div class="app-on-stores2">
                <ul>
                  <li><a title="Download on the App Store" target="_blank" href="https://itunes.apple.com/us/app/the-on-call-room-ocr/id975310441?ls=1&amp;mt=8" onclick="ga('send', 'event', 'app', 'download','appstore');">
                  	<?php echo $this->Html->image('app-store.png', array("alt"=> "Download on the App Store", "title"=> "Download on the App Store", "height"=> "71",  "border"=> "0" , "width"=> "228")); ?>
                  </a></li>
                  <li><a title="Get it on Google play" target="_blank" href="https://play.google.com/store/apps/details?id=com.ocr.theoncallroom&amp;hl=en" onclick="ga('send', 'event', 'app', 'download','googleplay');">
                  	<?php echo $this->Html->image('google-play.png', array("alt"=> "Get it on Google play", "title"=> "Get it on Google play", "height"=>"70", "width"=>"229")); ?>
                  </a></li>
                </ul>
              </div>
      </div>
         </div>
         <div class="tab-pane" id="b">
         <h1>Our Commitment</h1>
         <p>At The OCR we’ve put together our very own version of the Hippocratic Oath as a set of guiding principles.</p>
<p><strong>Privacy –</strong> We know that patient privacy is a priority for healthcare professionals. With this in mind, we’ve built a number of features into The OCR to help you edit out the identifiable parts of patient X-Rays and symptom photos. We’ve also included an anonymous posting feature, to help you discuss difficult or sensitive topics without the fear of being identified.</p>
<p><strong>Security –</strong> We use multilayer encryption and SSL certification to make sure that ‘The On Call Room’ app is completely secure. Both your details and your patient’s information are kept safely in accordance with UK and international data protection laws.</p>
<p><strong>Quality –</strong> We’ve made sure that only vetted medical professionals are able to use our app and only respected medical publications can share content. You won’t find any crackpots or keyboard warriors on The OCR, just real, educated and experienced doctors, nurses, pharmacists and dentists. This means that the information you see, the network you build and the support you receive is of the highest possible quality. </p>
         </div>
         <div class="tab-pane" id="c">
         <h1>Our Mission</h1>
         <p>You’re on a mission to save lives and help make your patients healthier and happier. We’re on a mission to collect and share the combined knowledge of the entire medical profession and offer you a support network, but what does that really mean?</p>
<p class="marBottom10"><strong>The On Call Room Will:</strong></p>
<ul class="aboutTab">
    <li>Empower medical professionals to share knowledge and learn new insights.</li>
    <li>Improve patient care by raising standards and encouraging new ways of working.</li>
    <li>Build up a portfolio of cases for medical professionals to reference at a glance.</li>
    <li>Make a living, breathing, ever evolving medical textbook based on real life experiences, not academic theories.</li>
    <li>Quickly disseminate the latest guidelines and best practices to help combat outbreaks, epidemics and pandemics.</li>
    <li>Take teaching and professional development online –You’ll now be able to attend grand rounds wherever you are in the world, using our app.</li>
    <li>Help you collaborate on research online.  You can now audit and support the research work others are doing in your field.</li>
    <li>Help clinicians discuss new medical findings and offer their own take on discoveries.</li>
</ul>
         </div>
         <div class="tab-pane" id="d"><h1>Our People</h1>
         <p>The OCR was founded by Dr. Sandeep Bansal in 2014. He has one vision – to make the collective knowledge of the medical community and accessible to individual clinicians wherever they are in the world.</p>
<p>Sandeep is a fully trained MD turned entrepreneur with over 10 years of medical experience. He trained in the UK as well as the United States and India, so he understands medicine on a truly global scale.</p>
<p>Working alongside Sandeep are several other high level professionals who help form our executive board:</p>
<p><strong>Dr. Ashok Bansal –</strong> CFO. Ashok has over 20 years of frontline experience in the medical and social care sectors. As well as his role with The OCR, Ashok is the CEO of Comfort Care Homes Ltd an elderly care home group in Wales.</p>
<!--<p><strong>Udai Deo –</strong> COO. Udai is an investment banker with experience in investment management auditing within a top tier firms. His wealth of knowledge in areas like Business Information Management &amp; Computer Science has been an invaluable part of the OCR journey.</p>
<p><strong>Deepak Bhavra –</strong> Head of Recruitment. Deepak has lent us his 10 years of experience working in Recruitment &amp; Procurement for IBM in the USA and India to help grow the business.</p>-->
<p>Our C level executives call the shots on building the business, but there wouldn’t be a business without our product team. We proudly employ 22 highly skilled developers and designers who work tirelessly to make sure The OCR makes a huge impact on the medical world and helps patients live healthier and happier lives.</p>
<p>We’re always on the lookout for talented designers, developers and marketers. <a href="<?php echo BASE_URL . 'join-the-team'; ?>" >Want to join the team?</a></p>
<p>Or, if you’re a medical student and want to help us spread the word, we’re looking for student brand ambassadors.</p></div>
        </div>
      </div>
      <!-- /tabs -->
         </div>
         </div>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('#tabs').tab();
        $(".submenu li a").attr("data-toggle","tab");
    });
  $(".user-nav li a").click(function() {
    $(".nav li").removeClass("active");
    $($(this).attr("data-toggle-tab")).parent("li").addClass("active");
  });
  $(function () {
      var hash = window.location.hash;
      hash && $('ul.nav a[href="' + hash + '"]').tab('show');
    });
</script>