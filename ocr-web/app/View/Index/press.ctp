<section class="inner-content">
<!--Inner page-->
  <div class="container">
    <div class="row">
      <div class="howOCR">
        <div class="col-sm-12">
        <?php if(isset($pageTitle) && $pageTitle != ''){ ?> <h1><?php echo $pageTitle; ?></h1> <?php } ?>
              <p class="text-center">We want our medical apps to go viral faster than Influenza so let’s talk.</p>
              <p><strong>About:</strong>
The On Call Room (The OCR) is a knowledge sharing app exclusively for medical professionals. We help the whole medical team (consultants, doctors, nurses, pharmacists & dentists) to share knowledge, learning and best practice in a supportive and collaborative environment.</p>
                <p class="text-center pressLink">
                    <a href="<?php echo BASE_URL . 'about'; ?>">About The OCR</a>
                    <a href="<?php echo BASE_URL . 'about'; ?>#c">Discover our Mission</a>
                    <a href="<?php echo BASE_URL . 'about'; ?>#d">Meet our Team</a>
                </p>
                <div class="clear"></div><br />
                <p><strong>Our Key People</strong><br /></p>
                <div id="screenshots-slider" class="owl-carousel"> <a class="item" title="Dr Ashok Bansal">
                  <?php echo $this->Html->image('ashok.png', array("alt"=> "ashok", "width"=> "200", "height"=> "220")); ?>
                  <span>
              <p><strong>Dr Ashok Bansal</strong><br />
                MD<br />
                Doctor by profession. Passionate about healthcare. Nursing home operator. Investor.</p>
              </span></a> <a class="item" title="Dr Sandeep Bansal">
              <?php echo $this->Html->image('sunny.png', array("alt"=> "sunny", "width"=> "200", "height"=> "220")); ?>
              <span>
              <p><strong>Dr Sandeep Bansal</strong><br />
                Founder and CEO<br />
                Doctor turned entrepreneur. Passionate about healthcare and technology.</p>
              </span></a> 
              <a class="item" title="Anil Kumar">
                <?php echo $this->Html->image('anil.png', array("alt"=> "anil", "width"=> "200", "height"=> "220")); ?>
                <span>
              <p><strong>Anil Kumar</strong><br />
                COO<br />
                Start-up specialist. Building delivery excellence. Three decades of professional experience spread across sectors. </p>
              </span></a>
              
              <a class="item" title="Rakesh Biyani">
                <?php echo $this->Html->image('rakesh_uncle.png', array("alt"=> "Rakesh Biyani", "width"=> "200", "height"=> "220")); ?>
                <span>
              <p><strong>Rakesh Biyani</strong><br />
                Future Group Joint MD<br />
                Business Guru. </p>
              </span></a>
              </div>
                <!--<ul class="pressLink">
                    <li>
                      <div>
                          <p>Dr. Sandeep Bansal
                    <em>Founder and CEO</em></p>
                    <ul class="aboutTab">
                      <li>Doctor with 10 years experience.</li>
                        <li>GP Trainee in Oxford University Deanery</li>
                        <li>Trained in India, UK and USA.</li>
                    </ul>
                        </div>
                    </li>
                    <li>
                      <div>
                          <p>Dr. Ashok Bansal
                    <em>CFO</em></p>
                    <ul class="aboutTab">
                      <li>Current CEO of comfort care homes</li>
                        <li>Ex Doctor with 20+ years of experience on the frontline of medicine and social care.</li>
                    </ul>
                        </div>
                    </li>
                    <li>
                      <div>
                          <p>Udai Deo
                    <em>COO</em></p>
                    <ul class="aboutTab">
                      <li>Currently working as a investment management auditor with a tier 1 investment bank</li>
                        <li>A total of 10 years of experience in Computer Science, Information Management and Information Security.</li>
                    </ul>
                        </div>
                    </li>
                    <li>
                      <div>
                          <p>Deepak Bhavra
                    <em>Head Of Recruitment and Procurement</em></p>
                    <ul class="aboutTab">
                      <li>10 years experience in IBM India and USA in procurement and recruitment</li>
                    </ul>
                        </div>
                    </li>
                </ul>-->
                <p><strong>Want to Book an Interview?</strong></p>
                <p>CEO: Dr. Sandeep Bansal</p>
                <p>Email: <a href="mailto:sandeep@mediccreations.com">Sandeep@mediccreations.com</a></p>
                <p>Tel: +44(0)1234 567890</p>
                <p><strong>Read all about it:</strong> List all future press releases listed here</p>
             </div>
    <div class="col-xs-12 col-sm-12 col-md-6 pressLink2">
                <p><strong>We've already been Featured in:</strong><a target="_blank" href="http://www.joomag.com/magazine/the-journal-of-mhealth-vol-2-issue-6-dec-2015/0902490001450798164/p10?short">
                  <?php echo $this->Html->image('news-icon2.png', array("alt"=> "")); ?>
                </a></p>
                <!-- <p><strong>Watch Our Video &amp; Discover The OCR:</strong><a target="_blank" href="#"><i class="fa fa-video-camera"></i></a></p> -->
                <!--<p><strong>Downloadable Goodies:</strong><a target="_blank" href="#"><i class="fa fa-cloud-download"></i>
                <i class="fa fa-file-pdf-o"></i></a></p>-->
                </div>
      </div>
    </div>
  </div>
</section>

<?php echo $this->Html->script(array('owl.carousel.min')); ?>
<script>
 var owl = $("#screenshots-slider");
    owl.owlCarousel({
        items : 5, 
        itemsDesktop : [1400,4], 
        itemsDesktopSmall : [1200,3], 
        itemsTablet: [900,2], 
        itemsMobile : [600,1],
    stopOnHover:true
    });
</script>