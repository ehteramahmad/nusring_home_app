<section class="inner-content mobile">
<!--Inner page-->
  <div class="container">
    <div class="row">
      <div class="howOCR">
        <div class="col-sm-12">
			  <!-- <h1>About</h1> -->
              <p>The On Call Room (The OCR) is a knowledge sharing  & social networking app for the entire medical team. </p>
<p>Consultants, doctors, nurses, pharmacists, dentists and students can share best practise, help diagnose unusual symptoms and offer support to other professionals around the world. </p>
<p>Clinicians can use our app to quickly access up to the minute research, findings and discussions, wherever they are to help them improve patient care. Say goodbye to googling and hello to The OCR.</p>
<p><strong>A Medical Community at Your Fingertips</strong><br />We're on a mission to put medical knowledge, wisdom and advice in the hands of every medical professional around the world.</p>
<p>Do you sometimes need help quickly diagnosing unusual symptoms? </p>
<p>Ever need to chat to other medics and decompress after a difficult shift?</p>
<p>Want to find out how to better treat patients &amp; discover new medical advancements?</p>
<p>Then the OCR is for you. </p>
<p><strong>Make an Impact on Medicine</strong><br />
The OCR was created by doctors for doctors. We’ve been in the medical game a long time and experience tells us that by providing collaborative learning tools to healthcare professionals , we can help you make a greater impact on the world of medicine. This is why we started The On Call Room.</p>
<p>The OCR is the brainchild of Dr Sandeep Bansal. He believes that there is a better way to share and manage medical knowledge outside of journals and hospital notice boards. Since starting The On Call Room in February 2014, Sandeep has grown his idea into a team of 22, who are all striving to help expand the minds of the medical sector.</p>
<p><strong>Safe, Secure &amp; Easy to Use</strong><br />
We’ve made sure that The OCR app is safe, secure and confidential, so you can tap into a global network of medical professionals, whilst sticking to your Hippocratic Oath and keeping your patient’s best interests at heart.</p>
<p>It’s easy to get started, visit either the <a href="https://itunes.apple.com/us/app/the-on-call-room-ocr/id975310441?ls=1&mt=8" target="_blank">iOS app store</a> (if you have an iPhone) or the <a href="https://play.google.com/store/apps/details?id=com.ocr.theoncallroom&hl=en" target="_blank">Google Play store</a> (if you have an Android phone) and download The OCR. We’ll ask you a couple of questions to verify your identity against our databases and then you’re good to go.</p>
<!-- <p>If you’re stuck at any point, or want to find out more about using The OCR for the first time, check out our <a href="quickStart.html">getting started guide</a> to find out everything you need to know.</p> -->
             </div>
    
      </div>
    </div>
  </div>
</section>