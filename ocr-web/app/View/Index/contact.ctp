<section class="inner-content">
<!--Inner page-->
  <div class="container">
    <div class="row">
      <div class="faq">
        <div class="col-sm-12">
        <?php if(isset($pageTitle) && $pageTitle != ''){ ?> <h1><?php echo $pageTitle; ?></h1> <?php } ?>
              <p class="text-center">Got a question about The On Call Room or need help using the app? We’d love to hear from you.  Email is the best way to reach us and we’ll get back to you within 24 hours.</p>
              <!--signup form -->
          <div class="account-section">
            <div class="col-xs-12 col-sm-12 col-md-6 signup-form">
            <h3 class="text-center marBottom10">Send us a Message</h3>
            <?php 

           
            if(isset($_REQUEST['captcha']) && $_REQUEST['captcha']==1){
               echo '<span style="color:red; padding-left: 25%; margin-bottom:5px; float: left;">Please enter right captch value</span>';
            } ?>
                <form novalidate action="<?php echo BASE_URL . 'Index/sendContactUs'; ?>" class="" id="contact-form" method="post" accept-charset="utf-8"><div style="display:none;"><input type="hidden" name="_method" value="POST"></div>  
         <fieldset class="form-group">
                 <label for="name">Your Name</label>
                 <input type="text" name="data[contact][name]" id="name" class="form-control" maxlength="50" value="">
                </fieldset>
                <fieldset class="form-group">
                  <label for="email">Your Email</label>
                   <input type="text" name="data[contact][email]" id="email" class="form-control" maxlength="250" value="">              
                </fieldset>
                <fieldset class="form-group">
                  <label for="subject">Subject</label>
                  <select form="contact-form" name="data[contact][subject]" id="subject" class="form-control" required>
                     <option value=""></option>
                    <option value="Technical Support">Technical Support </option>
                    <option value="Questions about The OCR">Questions about The OCR </option>
                    <option value="Press Enquiry">Press Enquiry </option>
                    <option value="Verify my ID">Verify my ID </option>
                    <option value="Careers @ The OCR">Careers @ The OCR </option>
                  </select>
                </fieldset>
                <fieldset class="form-group">
                  <label for="message">Your Message</label>
                  <textarea name="data[contact][message]" id="message" class="form-control" rows="4" cols="50" maxlength="500"></textarea>
                </fieldset>
                <fieldset class="form-group">
                  <label for="Captcha"></label>
                   <img id="captcha_code" src="<?php echo BASE_URL; ?>captcha_code.php" />
                   <a style="cursor:pointer; color: #10a5da;"  class="btnRefresh" onClick="refreshCaptcha();">Refresh</a>            
                </fieldset>
                <fieldset class="form-group">
                 <label for="name">Captcha</label>
                 <input type="text" name="data[contact][captcha]" id="captcha" class="form-control" maxlength="50" value="">
                </fieldset>
                
                <fieldset class="form-group">
                  <input type="submit" class="btn btn-signup" id="" name="" value="Send Message">
                </fieldset>
              </form>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 signup-form padLeft40">
            <h3 class="text-center marBottom10">Our Contact Details</h3>
                <p><strong>Email:</strong> <a href="mailto:support@theoncallroom.com">support@theoncallroom.com</a> </p>
                <p><strong>Phone:</strong> 07985 569774 </p>
                <p><strong>Address:</strong> Oakdale, Coldra Mill Park, Royal Oak Hill, Newport, Gwent, NP18 1JF, UK</p>
                <p>Need help using the app? Check out our <a href="<?php echo BASE_URL . 'quick-start'; ?>">get started guide</a> and our <a href="<?php echo BASE_URL . 'support'; ?>">support section.</a></p>
                <p>Want to become a <a href="<?php echo BASE_URL . 'student-ambassador'; ?>">student ambassador?</a></p>
            </div>
          </div>    
          <!--//signup form --> 
    </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  function refreshCaptcha() {
  $("#captcha_code").attr('src','<?php echo BASE_URL; ?>/captcha_code.php');
}

</script>