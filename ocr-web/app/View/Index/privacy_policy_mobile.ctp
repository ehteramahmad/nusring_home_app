<section class="inner-content mobile">
	<div class="container">
    <div class="row">
      <div class="howOCR">
        <div class="col-sm-12">
        <div class="bs-example">
 <h3>1. Effective as of May 28 2014</h3>

<p>This privacy policy governs your use of The On Call Room and Medic Bleep, software applications ("Application") that was created by The On Call Room Ltd. The Application includes a method for viewing medical cases,&nbsp;medical journals, a database of medical journals, and mechanisms for users to upload medical journals to the database or send them to each other.</p>

<p><strong>What information does the Application obtain and how is it used?</strong></p>

<p><strong>User Provided Information</strong></p>

<p>The Application obtains the information you provide when you download and register the Application.</p>

<h3>2. Part 1: Personal User Information</h3>

<p>When you register with us and use the Application, you provide:</p>

<ul class="aboutTab padLeft40">
	<li>Your Name</li>
	<li>Your Username</li>
	<li>Your Password</li>
	<li>Your Institution Email Address</li>
	<li>Your Medical ID or Professional Registration Number</li>
</ul>

<p>Your password is cryptographically hashed and your email address is encrypted. These information elements are referred to collectively as your "Personal User Information." We collect and hold this information for the purpose of administering your use of the Application.<br>
You are solely responsible for (1) maintaining the strict confidentiality of your Personal User Information, (2) not allowing another person to use your Personal User Information to access the Services, (3) any and all damages or losses that may be incurred or suffered as a result of any activities that occur under your Personal User Information. You agree to immediately notify The On Call Room Ltd. in writing by email to <a href="mailto:privacy@theoncallroom.com">privacy@theoncallroom.com</a> of any unauthorized use of your Personal User Information or any other breach of security. The On Call Room Ltd, is not and shall not be liable for any harm arising from or relating to the theft of your Personal User Information, your disclosure of your Personal User Information, or the use of your Personal User Information by another person or entity.</p>

<h3>3. Part 2: User-Provided Case Information</h3>

<p>(a) When you use the application, you may:<br>
i &nbsp;Upload articles<br>
ii Upload media/images<br>
iii Share an article<br>
We will store your uploaded articles, article descriptions, date created, common variations of image tags (such as misspellings) and information on whether the tag appears in searches. The Application will only stores de-identified articles, not the original unedited articles, which may contain personally identifying features. <strong>We do not store IP addresses or location information relating to images uploaded by users.</strong></p>

<p>(b) When you use the application, you may provide comments. We will store your comments made, the date of comment and the username connected to the comment.</p>

<p>(c) When you use the application, you may 'mark as hot' for easy reference later. We will store which articles on The On Call Room you have marked as 'hot', as well as the date and the username connected.</p>

<p>(d) When you use the application, you may flag articles provided by other users (e.g., to indicate that identifiable, incorrect or offensive information may have been improperly included in an article, caption, or comment). We will store the articles that you flag, the subcategory of the flagged image, the date flagged and the username referenced to the flagged image.</p>

<p>(e) We will also store the date your account was created, the Application version, and the last login date.<br>
These informational elements shall be referred to as "User-Provided Case Information.". All User-Provided Case Information that you generate in the Application must comply with local, provincial, state, and federal privacy legislation and best practices. Identifying information must be removed from any uploaded content and should not be included in any descriptions or comments.</p>

<p>(f) User Provided Information<br>
i You have to be verified as a licensed healthcare professional. If you choose this option, you will be asked to provide us with information, which we will cross-reference with publicly available data to ensure that you actually are a licensed healthcare professional.</p>

<p>ii <strong>You also have the option to invite colleagues to join The On Call Room so that they can use the Application too. If you choose to do this, the Application will access the address book on your mobile device to enable you to choose which of your colleagues you would like to invite to the Application.</strong> We only store the email addresses of those colleagues you invite to join the Application for the purpose of sending them an invitation email from our server. We do not store your complete address book.</p>

<p>(g) Automatically Collected Information<br>
In addition, the Application may collect certain information automatically, such as the type of mobile device you use, the IP address of your mobile device, your mobile operating system and information about the way you use the Application in order to improve the Application and deliver the services.<br>
All information stored on our server will not be accessible by third parties. We do not collect user level search activity or viewing activity. This is compiled only at an aggregate, in order to help us better understand how users are using our Application, so that we can optimize your experiences.<br>
We may also use the information provided by you to contact you from time to time to provide you with important information, push notifications and marketing promotions. You will be given the option to opt-out of these notifications.</p>

<p>Does the Application collect precise real time location information of the device?</p>

<p>This Application does not collect precise information about the location of your mobile device.</p>

<p>Do third parties see and/or have access to information obtained by the Application?</p>

<p>We will disclose User Provided and Automatically Collected Information as described above in the following circumstances:</p>

<ul class="aboutTab padLeft40">
	<li>as required by law, such as to comply with a subpoena, or similar legal process;</li>
	<li>when we believe in good faith that disclosure is necessary to protect our rights, protect your safety or the safety of others, investigate fraud, or respond to a government request;</li>
	<li>with our trusted services providers who work on our behalf, do not have an independent use of the information we disclose to them, and have agreed to adhere to the rules set forth in this privacy statement; and</li>
	<li>if The On Call Room Ltd is involved in a merger, acquisition, or sale of all or a portion of its assets, you will be notified via email and/or a prominent notice on our Web site of any change in ownership or uses of this information, as well as any choices you may have regarding this information</li>
	<li>we disclose user email addresses to third party email service providers in order to deliver users Figure1 informational, service and content emails.</li>
</ul>

<p>What are my opt-out rights?</p>

<p>You can stop all collection of information by the Application easily by uninstalling the Application. You may use the standard uninstall processes as may be available as part of your mobile device or via the mobile application marketplace or network.<br>
However, the Application will not allow you to opt out of any announcements and messages related to the implementation of this Policy and your obligations thereunder. As such, should your uploaded articles, media, or comments contain identifying information about a patient (as described in the <a href="http://theoncallroom.com/terms-and-conditions">Terms of Service</a>, you will receive messages from The On Call Room notifying you of a potential privacy violation associated with this content.<br>
You may also delete your account by emailing <a href="mailto:privacy@theoncallroom.com">privacy@theoncallroom.com</a>. You will be asked to confirm that you would like to delete your account. If you confirm, your profile information and all images associated with your profile will be automatically deleted from the The On Call Room server. We cannot guarantee, however, that we will be able to recall images that have been provided to third-parties, such as medical journals or medical education websites.</p>

<h3>4. Data Retention Policy, Managing Your Information</h3>

<p>We will retain User Provided Information as described above for as long as you use the Application, and will delete it if you delete your account, which can be done easily on the profile tab in the app or by contacting us via <a href="mailto:privacy@theoncallroom.com">privacy@theoncallroom.com</a> You may also permanently delete articles you have submitted from the Application. Once an article is deleted by the user, it will be deleted from our server along with all references to the image, such as all associated comments or favorites. We cannot guarantee, however, that we will be able to recall articles that have been provided to third-parties.<br>
If you contact The On Call Room Ltd. to delete your account, the change will be processed within seven (7) calendar days.</p>

<h3>5. Security</h3>

<p>We are concerned about safeguarding the confidentiality of your information. We provide physical, electronic and procedural safeguards to protect information we process and maintain. For example, your password is cryptographically hashed and your email address is encrypted.<br>
We limit access to your information to authorized employees and contractors who need to know that information in order to operate, develop or improve the Application. Please be aware that, although we will take reasonable steps to safeguard and maintain security of Personal User Information, User-Provided Information and Automatically Collected Information that we process and maintain, no security system can prevent all potential security breaches. Please refer to the Terms of Service for more details about The On Call Room Ltd and your obligations with respect to the proper use of the Application and notification obligations thereunder</p>

<h3>6. Changes</h3>

<p>This Privacy Policy may be updated from time to time for any reason. Each time you use the Application, the most current version of the Privacy Policy will apply. We will notify you of any changes to our Privacy Policy on this page. You are advised to consult this Privacy Policy regularly for any changes. Unless stated otherwise, the most current version of the Privacy Policy applies to all information that we have about you. We will not materially change our policies and practices to make them less protective of your privacy without the consent of affected users.</p>

<h3>7. Your Consent</h3>

<p>By using the Services, you are consenting to our processing of Personal User Information, User-Provided information, and Automatically Collected Information as set forth in this Privacy Policy now and as amended by us. "Processing" means using cookies on a computer/hand held device or using or touching information in any way, including, but not limited to, collecting, storing, deleting, using, combining and disclosing information.</p>

<p>Contact us &ndash; If you have any questions regarding privacy while using the Application, have questions about our practices, or wish to make a complaint about our handling of your personal data, please contact us via <a href="mailto:contact@theoncallroom.com">email</a>. We will make every effort to investigate and respond to your complaint in a timely way.</p>

<p>Any and all good-faith disclosures of privacy concerns under this Policy will not be used to restrict or prohibit you from continuing to use the Application to the extent permitted by law. However, disclosure of any unlawful practices implicating the Privacy Policy to The On Call Room Ltd does not release you from your obligations to notify local, provincial, state, and federal authorities of any violation of law related to your use of the Application.</p>

<h3>8. Notice to California Residents</h3>

<p>If you are a California resident, California Civil Code Section 1798.83 permits you to request information regarding the disclosure of your personal information by us to third parties for the third parties' direct marketing purposes. With respect to these entities, this Policy applies only to their activities within the State of California. To make such a request, please send an email to <a href="mailto:privacy@theoncallroom.com">privacy@theoncallroom.com</a> or write to us at:<br>
The On Call Room Ltd.<br>
Oakdale, Royal Oak Hill,<br>
Newport, UK<br>
NP18 1JF</p>

<h3>9. ADDITIONAL TERMS FOR UNITED KINGDOM RESIDENTS</h3>

<p>If you are a resident of the United Kingdom, the Data Protection Act 1998 permits you to request that we tell you what personal information we hold about you and provide you with a copy of that information. We may ask you to provide further information to confirm your identity before considering your request. To make such a request, please send an email to <a href="mailto:privacy@theoncallroom.com">privacy@theoncallroom.com</a> or write to us at:<br>
The On Call Room Ltd.<br>
Oakdale, Royal Oak Hill,<br>
Newport, UK<br>
NP18 1JF</p>

<p>If you use the Application to invite colleagues to join The On Call Room or to share articles with a colleague, you must first have their consent to use their e-mail address for this purpose. The On Call Room will rely on your obtaining that consent.</p>

<p>The On Call Room may transfer your personal data to servers located in the United States or other countries outside of the European Economic Area for purposes of providing the Application. By using, and continuing to use the Application, you agree to that transfer.</p>

<h3>10. ADDITIONAL TERMS FOR AUSTRALIAN, NEW ZEALAND AND SOUTH AFRICAN RESIDENTS</h3>

<p>If you are a resident of the Australia, New Zealand, or South Africa privacy legislation permits you to request that we tell you what personal information we hold about you and provide you with a copy of that information. You also have the right to request that we correct your personal information if it is inaccurate, out-of-date or incomplete. We may ask you to provide further information to confirm your identity before considering your request. To make an access and/or correction request, please send an email to <a href="mailto:privacy@theoncallroom.com">privacy@theoncallroom.com</a> or write to us at:<br>
The On Call Room Ltd.<br>
Oakdale, Royal Oak Hill,<br>
Newport, UK<br>
NP18 1JF</p>

<p>If you use the Application to invite colleagues to join The On Call Room or to share journals with a colleague, you must first have their consent to use their e-mail address for this purpose. The On Call Room will rely on your obtaining that consent.</p>

<p>It is not mandatory for you to provide us with any of your personal information. However, if you do not provide the information that we ask for, you may not be able to access and use all of the Application's features available to users.</p>

<p>The On Call Room may transfer your personal data to servers located in other countries for purposes of providing the Application. By using, and continuing to use the Application, you agree to that transfer. If you are an Australian resident, you acknowledge that by providing your consent to the transfer to servers located outside Australia, we are not required to take reasonable steps to ensure your personal information is handled in accordance with Australian privacy law.</p>
	</div>
 </div>
      </div>
    </div>
  </div>
</section>

