<?php 
/*
Add, update, Validate user profile.
*/
App::uses('AppModel', 'Model');

class UserProfile extends AppModel {


/*public $validate = array(		
        'first_name' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message'  => 'First name is Required.'
            ),
			'characters' => array(
				'rule' => array('custom', '/^[a-z0-9. ]*$/i'),
				'message'  => 'Alphanumeric characters with spaces only'
			)
        ),
        'last_name' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message'  => 'Last name is Required.'
            ),
			'characters' => array(
				'rule' => array('custom', '/^[a-z0-9 ]*$/i'),
				'message'  => 'Alphanumeric characters with spaces only'
			)
        ),
    );*/

/*
    On: 06-08-2015
    I/P: $fields = array(), $conditions = array()
    O/P: true/false
    Desc: 
    */
    public function updateFields( $fields = array(), $conditions = array() ){
        $return = 0;
        if( !empty($fields) && !empty($conditions) ){
            $this->updateAll( $fields, $conditions );

            if( $this->getAffectedRows() ){
                $return = 1;
            }
        }
        return $return;
    }			
	
}
?>