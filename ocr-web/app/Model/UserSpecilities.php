<?php 
/*
User Specilities.
*/
App::uses('AppModel', 'Model');

class UserSpecilities extends AppModel {			
	public $name = 'UserSpecilities';

	var $belongsTo = array(
        'Specilities' => array(
            'className' => 'Specilities',
            'conditions'=> 'Specilities.id = UserSpecilities.specilities_id'
            )
    );
}
?>