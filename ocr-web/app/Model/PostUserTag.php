<?php 
/*
 Represents model for post user tags.
*/
App::uses('AppModel', 'Model');

class PostUserTag extends AppModel {
	public $name = 'PostUserTag';

	/*
	---------------------------------------------------------------------------------------------
	On: 
	I/P:
	O/P:
	Desc: Getting information of tagged users with post(beat)
	---------------------------------------------------------------------------------------------
	*/
	public function postTaggedUsers( $postId = NULL ){
		$taggedUsers = array();
		if( !empty($postId) ){
			$conditions = array('PostUserTag.post_id'=> $postId, 'User.status'=> 1);
			$options =array(             
				'joins' =>
					array(
						array(
							'table' => 'users',
							'alias' => 'User',
							'type' => 'inner',
							'conditions'=> array('User.id = PostUserTag.user_id')
							),
						array(
							'table' => 'user_profiles',
							'alias' => 'UserProfile',
							'type' => 'left',
							'foreignKey' => 'user_id',
							'conditions'=> array('PostUserTag.user_id = UserProfile.user_id')
						),
					),
				'fields'=> array('User.id, User.status, UserProfile.user_id, UserProfile.first_name, UserProfile.last_name, UserProfile.profile_img'),
				'conditions'=> $conditions
				);
			$taggedUsers = $this->find('all', $options);
		}
		return $taggedUsers;
	}
	
}
?>