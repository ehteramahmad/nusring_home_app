<?php 
/*
 Represents model for Notification User.
*/
App::uses('AppModel', 'Model');

class NotificationUser extends AppModel {
	public $name = 'NotificationUser';

	/*
	---------------------------------------------------------------------------------------
	On: 21-07-2016
	I/P: $userId (valid user id)
	O/P: $userRegisteredDevices (All registered device id on GCM)
	Desc: 
	--------------------------------------------------------------------------------------
	*/
	public function getUserGcmRegisteredDevices($userId = NULL){
		$userRegisteredDevices = array();
		if(!empty($userId)){
			$userDevices = $this->find("all", array("conditions"=> array("user_id"=> $userId, "status"=> 1)));
			foreach( $userDevices as $userDevice ){
				$userRegisteredDevices[] = $userDevice['NotificationUser']['device_reg_id'];
			}
		}
		return $userRegisteredDevices;
	}
	
}
?>