<?php 
/*
User model to add, update ,validate user data
*/
App::uses('AppModel', 'Model');

class User extends AppModel {
	var $hasOne = array(
        'UserProfile' => array(
            'className' => 'UserProfile',
            'foreignKey' => 'user_id',
        )
    );

    var $hasMany = array(
        'UserSpecilities' => array(
            'className' => 'UserSpecilities',
            'foreignKey' => 'user_id',
            'conditions' => array('UserSpecilities.status' => 1)
        )
    );

    public $validate = array(
		'email' => array( 
              'rule' => '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/', 
              'message' => 'Please provide a valid email address.' 
         ),
		/*'first_name' => array(
			'characters' => array(
				'rule' => array('custom', '/^[a-z ]*$/i'),
				'message'  => 'First Name Should be Characters with spaces only'
			)
		),
		'last_name' => array(
			'characters' => array(
				'rule' => array('custom', '/^[a-z ]*$/i'),
				'message'  => 'Last name Should be Characters with spaces only'
			)
		),*/
        
        
    );

	/*
	On: 23-07-2015
	I/P: $modelName = string, $fieldName = string, $fieldVals = array()
	O/P: true/false
	Desc: 
	*/
	public function checkValueExists( $fieldName = '', $fieldVal = ''){
		
		$countdata = $this->find('count', array('conditions' => array( $fieldName => $fieldVal )));
		if($countdata > 0){
			return true;
		}else{
			return false;
		}
	}

	/*
	On: 04-07-2015
	I/P: $activationKey
	O/P: $data = array()
	Desc: 
	*/
	public function activationKeyStatus( $activationKey = NULL ){
		$data = array(); 
		if( !empty($activationKey) ){
			$data = $this->find('first', array('fields'=> array('User.status, User.approved'), 'conditions'=> array('activation_key'=> $activationKey)));
		}
		return $data;
	}

	/*
	On: 04-08-2015
	I/P: $activationKey
	O/P: 0 OR 1 
	Desc:
	*/
	function activateKey( $activationKey = NULL ){
		$return = 0;
		if( !empty($activationKey) ){
			//$this->updateAll( array('status'=> 1, 'approved'=> 1), array('activation_key'=> $activationKey) );
			$this->updateAll( array('status'=> 1), array('activation_key'=> $activationKey) );
			if( $this->getAffectedRows() ){
				$return = 1;
			}
		}
		return $return;
	}

	/*
	On: 04-08-2015
	I/P: $params = array()
	O/P: User Details
	Desc: 
	*/
	public function userDetails( $params = array() ){
		$userData = array();
		if( !empty($params) ){
			$userData = $this->find('first', array('conditions'=> $params));
		}
		return $userData;
	}		
	
	/*
	On: 05-08-2015
	I/P: 
	O/P:
	Desc: 
	*/
	public function updateFields( $fields = array(), $conditions = array() ){
		$return = 0;
		if( !empty($fields) && !empty($conditions) ){
			$this->updateAll( $fields, $conditions );

			if( $this->getAffectedRows() ){
				$return = 1;
			}
		}
		return $return;
	}

	/*
	On: 03-11-2015
	I/P: $params = array()
	O/P: array() users lists
	Desc: Fetches user lists with pagination
	*/
	public function userSearch( $params = array() , $loggedinUser = NULL ){
		$userLists = array();
		if( !empty($params) ){ 
			$pageNum = isset($params['page_number']) ? $params['page_number'] : 1;
			$pageSize = isset($params['size']) ? $params['size'] : DEFAULT_PAGE_SIZE;
			$offsetVal = ( $pageNum - 1 ) * $pageSize;
			if(!empty($loggedinUser)){
				$conditions = " User.status = 1 AND User.approved = 1 AND User.id !=" . $loggedinUser . " ";
			}else{
				$conditions = " User.status = 1 AND User.approved = 1 ";
			}
			if(!empty($params['first_name'])){
				if(strlen($conditions) >0){
					//$conditions .= ' AND ( LOWER(UserProfile.first_name) LIKE LOWER("'.$params['first_name'].'%") OR LOWER(UserProfile.last_name) LIKE LOWER("'.$params['first_name'].'%") OR LOWER(CONCAT(UserProfile.first_name," ", UserProfile.last_name)) LIKE LOWER("'.$params['first_name'].'%") OR ( LOWER(Specility.name) LIKE LOWER("'.$params['first_name'].'%") AND UserSpecility.status =1 ) ) ';
					$conditions .= ' AND ( LOWER(UserProfile.first_name) LIKE LOWER("'.$params['first_name'].'%") OR LOWER(UserProfile.last_name) LIKE LOWER("'.$params['first_name'].'%") OR LOWER(CONCAT(UserProfile.first_name," ", UserProfile.last_name)) LIKE LOWER("'.$params['first_name'].'%") OR  LOWER(UserProfile.role_status) LIKE LOWER("%'.$params['first_name'].'%") OR  LOWER(Profession.profession_type) LIKE LOWER("'.$params['first_name'].'%") OR  LOWER(User.email) LIKE LOWER("'.$params['first_name'].'%") OR  LOWER(Country.country_name) LIKE LOWER("'.$params['first_name'].'%") OR  LOWER(CompanyName.company_name) LIKE LOWER("%'.$params['first_name'].'%") OR  LOWER(InstituteName.institute_name) LIKE LOWER("%'.$params['first_name'].'%") ) ';
				}else{
					//$conditions .= ' ( LOWER(UserProfile.first_name) LIKE LOWER("'.$params['first_name'].'%") OR LOWER(UserProfile.last_name) LIKE LOWER("'.$params['first_name'].'%") OR LOWER(CONCAT(UserProfile.first_name," ", UserProfile.last_name)) LIKE LOWER("'.$params['first_name'].'%") OR ( LOWER(Specilitiy.name) LIKE LOWER("'.$params['first_name'].'%") AND AND UserSpecility.status =1 ) ) ';
					$conditions .= ' ( LOWER(UserProfile.first_name) LIKE LOWER("'.$params['first_name'].'%") OR LOWER(UserProfile.last_name) LIKE LOWER("'.$params['first_name'].'%") OR LOWER(CONCAT(UserProfile.first_name," ", UserProfile.last_name)) LIKE LOWER("'.$params['first_name'].'%") OR LOWER(UserProfile.role_status) LIKE LOWER("%'.$params['first_name'].'%") OR  LOWER(Profession.profession_type) LIKE LOWER("'.$params['first_name'].'%") OR  LOWER(User.email) LIKE LOWER("'.$params['first_name'].'%") OR  LOWER(Country.country_name) LIKE LOWER("'.$params['first_name'].'%") OR  LOWER(CompanyName.company_name) LIKE LOWER("%'.$params['first_name'].'%") OR  LOWER(InstituteName.institute_name) LIKE LOWER("%'.$params['first_name'].'%") ) ';
				}
			}
			if(!empty($params['last_name'])){
				if(strlen($conditions) >0){
						$conditions .= ' AND LOWER(UserProfile.last_name) LIKE LOWER("%'.$params['last_name'].'%") ';
					}else{
						$conditions .= ' LOWER(UserProfile.last_name) LIKE LOWER("%'.$params['last_name'].'%") ';
					}
			}
			if(!empty($params['email'])){
				if(strlen($conditions) >0){
					$conditions .= ' AND LOWER(User.email) LIKE LOWER("%'.$params['email'].'%") ';
				}else{
					$conditions .= ' LOWER(User.email) LIKE LOWER("%'.$params['email'].'%") ';
				}
			}
			$userLists = $this->find('all', 
							array(
								'joins'=>
								array(
									array(
										'table' => 'countries',
										'alias' => 'Country',
										'type' => 'left',
										'conditions'=> array('UserProfile.country_id = Country.id')
									),
									array(
										'table'=> 'professions',
										'alias' => 'Profession',
										'type' => 'left',
										'conditions'=> array('UserProfile.profession_id = Profession.id')
									),
									array(
										'table'=> 'user_specilities',
										'alias' => 'UserSpecility',
										'type' => 'left',
										'conditions'=> array('User.id = UserSpecility.user_id')
									),
									array(
										'table'=> 'specilities',
										'alias' => 'Specility',
										'type' => 'left',
										'conditions'=> array('Specility.id = UserSpecility.specilities_id')
									),
									array(
										'table'=> 'user_institutions',
										'alias' => 'UserInstitution',
										'type' => 'left',
										'conditions'=> array('User.id = UserInstitution.user_id')
									),
									array(
										'table'=> 'institute_names',
										'alias' => 'InstituteName',
										'type' => 'left',
										'conditions'=> array('InstituteName.id = UserInstitution.institution_id')
									),
									array(
										'table'=> 'user_employments',
										'alias' => 'UserEmployment',
										'type' => 'left',
										'conditions'=> array('User.id = UserEmployment.user_id')
									),
									array(
										'table'=> 'company_names',
										'alias' => 'CompanyName',
										'type' => 'left',
										'conditions'=> array('CompanyName.id = UserEmployment.company_id')
									),
								),
								'conditions' => $conditions,
								'limit'=> $pageSize,
								'offset'=> $offsetVal,
								'group'=> array('User.id'), 
								'order'=> 'UserProfile.first_name'
							)
						);
		}
		return $userLists;
	}

	/*
	On: 25-04-2016
	I/P: $params = array()
	O/P: array() users lists
	Desc: Fetches user details by user id
	*/
	public function userDetailsByUserId( $userId = NULL , $loggedinUser = NULL ){ 
		$userLists = array();
		if( !empty($userId) ){ 
			if(!empty($loggedinUser)){
				$conditions = " User.status = 1 AND User.approved = 1 AND User.id !=" . $loggedinUser . " AND User.id = $userId  ";
			}else{
				$conditions = " User.status = 1 AND User.approved = 1 AND User.id = $userId "; 
			}
			$userLists = $this->find('first', 
							array(
								'joins'=>
								array(
									array(
										'table' => 'countries',
										'alias' => 'Country',
										'type' => 'left',
										'conditions'=> array('UserProfile.country_id = Country.id')
									),
									array(
										'table'=> 'professions',
										'alias' => 'Profession',
										'type' => 'left',
										'conditions'=> array('UserProfile.profession_id = Profession.id')
									),
									array(
										'table'=> 'user_specilities',
										'alias' => 'UserSpecility',
										'type' => 'left',
										'conditions'=> array('User.id = UserSpecility.user_id')
									),
									array(
										'table'=> 'specilities',
										'alias' => 'Specility',
										'type' => 'left',
										'conditions'=> array('Specility.id = UserSpecility.specilities_id')
									),
								),
								'conditions' => $conditions,
								//'group'=> array('User.id'),
								//'order'=> 'UserProfile.first_name'
							)
						);
		}
		return $userLists;
	}

}
?>